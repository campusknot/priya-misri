$(document).ready(function () {


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    // $( document ).ajaxSuccess(function( event, xhr, settings, data ) {
    //     if(data.msg != undefined){
    //         showToast(data.msg, 'success');
    //     }
    // });
    // $( document ).ajaxError()(function( event, xhr, settings, data ) {
    //     if(data.msg != undefined){
    //         showToast(data.msg, 'success');
    //     }
    // });
});

function loadNewData(sDivId, sUrl, nCurrentPage, sAdditionalParams)
{

    if (!sAdditionalParams)
        sAdditionalParams = '';

    $.ajax({
        type: "GET",
        url: siteUrl + sUrl + '?page=' + nCurrentPage + sAdditionalParams,
        success: function (data) {
            $('#' + sDivId).html(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend: function () {
            nLoadNewDataStatus = 1; // status set if ajax call is active
        },
        complete: function () {
            nLoadNewDataStatus = 0; // status set if ajax call is complete
        }
    });
}
/*style function*/
function showToast(message, messageType) {

    var toastDiv = '<div id="toast-container" class="toast-top-center toast-wrapper" aria-live="polite" role="alert">' +
    '<div class="toast  ck-bg-'+messageType+'" style="display: block;">' +
    '<div class="toast-message">'+ message + 
    '</div></div></div>';

    $(toastDiv).appendTo('body');

    $('#toast-container').slideDown('slow');

    setTimeout(function () {

        $("#toast-container").fadeOut("slow");
        $('#toast-container').remove();
    }, 3000);
}

function hideElement(el) {
    $(el).addClass("hideElement");
}
function showElement(el) {
    $(el).addClass("showElement");
}
function removeElement(el) {
    $(el).remove();
}

function showAddExtensionLightBox(idCampus) {
    $.ajax({
        type: "GET",
        url: siteUrl + '/campus/' + idCampus + '/add-extension',
        success: function (data) {
            openLightBox(data);
            $('input[name="campus_extension"]').focus();
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
        }
    });
}

function openLightBox(showHtml)
{

    $('#light_box').css('display', 'block');
    $('#lightbox_content').html(showHtml);

    $('#lightbox_content').children().click(function (event) {
        event.stopPropagation();
    });
    nLighboxStatus = 1;

    //this is to stop background body scroll
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () {
        window.scrollTo(x, y);
    };
}

function closeLightBox()
{
    $('#lightbox_content').html('');
    $('#light_box').css('display', 'none');
    $("body").removeClass("modal-open");
    //this is to enable scrolling
    window.onscroll = function () {};
    nLighboxStatus = 0;
}

function showAddNewUniversity() {
    $('#add_new_university').toggleClass('hide');
    $('#university, #university_name, #university_url').val('');
    if ($('#add_new_university').hasClass('hide')) {
        $('#university').attr('disabled', false);
        $('#university_name, #university_url').attr('disabled', true);
    } else {
        $('#university').attr('disabled', true);
        $('#university_name, #university_url').attr('disabled', false);
    }

}



function showVerifyUser(idUser) {
    var res = confirm("Are you sure you want to verify this user?");
    if (res == true) {
        $.ajax({
            type: "GET",
            url: siteUrl + '/user/verified/' + idUser,
            success: function (data) {
                showToast(data.msg, data.status);
                callUserListing();
            },
            error: function (data) {
                var response = $.parseJSON(data.responseText);
            }
        });
    } else {
        return false;
    }
}

function sendVerificationLink(idUser) {
    var res = confirm("Are you sure to send verification Email?");
    if (res == true) {
        $.ajax({
            type: "GET",
            url: siteUrl + '/user/verification/' + idUser,
            success: function (data) {
                showToast(data.msg, data.status);
                callUserListing();
            },
            error: function (data) {
                var response = $.parseJSON(data.responseText);
                console.log(response);
            }
        });
    } else {
        return false;
    }
}

function isCharcter(event){
    if(event.which >= 48 && event.which <= 90){
        return true;
    }
    return false;
}

function getSearchString(){
    return $("input[name='search_str']").val();
}

function getOrderField(){
    return $("input[name='order_field']").val();
}

function getSortingOrder(){
    return $("input[name='order_by']").val();
}


function callSearchUser(searchStr, event){
    // if(!isCharcter(event))
    //     return false;

    var search_str = searchStr;

    callUserListing(search_str);
}

function callSortUser(orderField) {
    var search_str = getSearchString();
    var order_field = orderField;
    var prevOrderField = $("input[name='order_field']").val();
    if(order_field == prevOrderField){
        var order_by = ($("input[name='order_by']").val() == 'asc') ? 'desc' : 'asc';
    }else{
        var order_by = 'asc';
    }

    callUserListing(search_str, order_field, order_by);
}

function callUserListing(){

    var searchStr = arguments.length == 0 || arguments[0] === undefined ? getSearchString() : arguments[0];
    var orderField = arguments.length <= 1 || arguments[1] === undefined ? getOrderField() : arguments[1];
    var orderBy = arguments.length <= 2 || arguments[2] === undefined ? getSortingOrder() : arguments[2];

    $.ajax({
        type: "POST",
        url: siteUrl + '/user/list',
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy},
        success: function (data) {
            $('div.user_data').html(data);
            callHeighlightSearchString(searchStr);
        },
        error: function (data) {
        }
    });
}

function callSearchExtension(searchStr, event){
    // if(!isCharcter(event))
    //     return false;

    var search_str = searchStr;
    
    callExtensionListing(search_str);
}

function callSortExtension(orderField) {
    var search_str = getSearchString();
    var order_field = orderField;
    var prevOrderField = $("input[name='order_field']").val();
    if(order_field == prevOrderField){
        var order_by = ($("input[name='order_by']").val() == 'asc') ? 'desc' : 'asc';
    }else{
        var order_by = 'asc';
    }

    callExtensionListing(search_str, order_field, order_by);
}

function callExtensionListing(){

    var searchStr = arguments.length == 0 || arguments[0] === undefined ? getSearchString() : arguments[0];
    var orderField = arguments.length <= 1 || arguments[1] === undefined ? getOrderField() : arguments[1];
    var orderBy = arguments.length <= 2 || arguments[2] === undefined ? getSortingOrder() : arguments[2];

    $.ajax({
        type: "POST",
        url: siteUrl + '/extension/list',
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy},
        success: function (data) {
            $('div.extension_data').html(data);
            callHeighlightSearchString(searchStr);
        },
        error: function (data) {
        }
    });
}

function callSearchCampus(searchStr, event){
    // if(!isCharcter(event))
    //     return false;

    var search_str = searchStr;
    
    callCampusListing(search_str);
}

function callSortCampus(orderField) {
    var search_str = getSearchString();
    var order_field = orderField;
    var prevOrderField = $("input[name='order_field']").val();
    if(order_field == prevOrderField){
        var order_by = ($("input[name='order_by']").val() == 'asc') ? 'desc' : 'asc';
    }else{
        var order_by = 'asc';
    }

    callCampusListing(search_str, order_field, order_by);
}


function callCampusListing(){

    var searchStr = arguments.length == 0 || arguments[0] === undefined ? getSearchString() : arguments[0];
    var orderField = arguments.length <= 1 || arguments[1] === undefined ? getOrderField() : arguments[1];
    var orderBy = arguments.length <= 2 || arguments[2] === undefined ? getSortingOrder() : arguments[2];

    $.ajax({
        type: "POST",
        url: siteUrl + '/campus/list',
        data: {search_str: searchStr, order_field: orderField, order_by: orderBy},
        success: function (data) {
            $('div.campus_data').html(data);
            callHeighlightSearchString(searchStr);
        },
        error: function (data) {
        }
    });
}

function callHeighlightSearchString(searcStr){
    console.log(searcStr);
    if(searcStr != ''){
        $('td:not(.no-heighlight) span').each(function(i, row) 
        {
            console.log(this);
            data = $(this).text();
            searcStr = searcStr.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");

            var pattern = new RegExp("("+searcStr+")", "gi");

            data = data.replace(pattern, "<mark>$1</mark>");
            $(this).html(data);
        });
    }
}

function callDeleteUser(idUser) {
    var res = confirm("Are you sure you to remove this user?");
    if (res == true) {
        $.ajax({
            type: "GET",
            url: siteUrl + '/user/delete-user/' + idUser,
            success: function (data) {
                callUserListing();
            },
            error: function (data) {
                var response = $.parseJSON(data.responseText);
            }
        });
    } else {
        return false;
    }
}


function callExtensionDelete(idExtension) {
    var res = confirm("Are you sure you to remove this Extension?");
    if (res == true) {
        $.ajax({
            type: "GET",
            url: siteUrl + '/campus/delete-extension/' + idExtension,
            success: function (data) {
                callExtensionListing();
                showToast(data.msg, data.status);
            },
            error: function (data) {
                var response = $.parseJSON(data.responseText);
            }
        });
    } else {
        return false;
    }
}


function callUpdateExtension(nIdExtension, nExtension) {
    $.ajax({
        type: "POST",
        url: siteUrl + '/campus/update-extension/' + nIdExtension,
        data: {campus_extension: nExtension},
        success: function (data) {
            //$('ul.mode-edit').find('li.campus_extension .processing').hide();
            $('tr.mode-edit').find('td.campus_extension').html('<span>'+nExtension+'</span>');
            showToast(data.msg, 'success');
        },
        error:function(jqXHR, exception){
            $('tr.mode-edit').find('td.campus_extension .processing').remove();
            // $('tr.mode-edit').find('td.campus_extension .processing').remove();
            $('tr td.campus_extension input').show().focus();
            var Response = jqXHR.responseText;
            Response = $.parseJSON(Response);
            showToast(Response.campus_extension, 'danger');
        }
    }); 
    //$('ul.mode-edit').find('li.campus_extension .processing').hide();
}   

function callShowError(jqXHR) {

    var nIdForm = arguments.length <= 1 || arguments[1] === undefined ? undefined : arguments[1];

    var response = jqXHR.responseText;
    response = $.parseJSON(response);
    var errors = "";
    $.each(response, function(index, element) 
    {

        var nInputField =  nIdForm != undefined ? nIdForm +' [name="'+index+'"]' : '[name="'+index+'"]';
        $(nInputField).parent('.form-group').addClass('has-error');
        $('<span class="help-block">'+element+'</span>').insertAfter( nInputField );
        // $('[name="'+index+'"]').parent('.form-group').addClass('has-error');
    });
}

function callHideError() {
    $('.help-block').remove();
    $('.form-group').removeClass('has-error');
}