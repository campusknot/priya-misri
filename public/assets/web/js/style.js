
/*Browse button change style*/
$(function() {
    // We can attach the `fileselect` event to all file inputs on the page
    $(document).on('change', ':file', function() {
        var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
    $(':file').on('fileselect', function(event, numFiles, label) {
       //this function is defined in common.js
        showCustomBrowseButton(event, numFiles, label, this);
    });
    showTooltip();
//    callTabDrop('group-tabs-dropdown');
//    callTabDrop('docs-top-heading-bar');
    
   /*header drop down top position*/
    $(".ddlink").click(function(){
        var hheight=$("header").height();
          $('.profile-edit').css('top',hheight);
          $('.profile-edit').css('margin','0px');
    });
     $('.mightOverflow').bind('mouseenter', function(){
        var $this = $(this);
        if(this.offsetWidth < this.scrollWidth && !$this.attr('title')){
            $this.attr('title', $this.text());
        }
    });
    
    
    /*sticky header*/
    var stickyOffset = $('header').offset().top;
    var stckyeleheight= $('header').height;
        
    $(window).scroll(function(){
        var isQuizLayout = $('header').hasClass("quiz-tab");
        
        var sticky = $('header'),
        scroll = $(window).scrollTop();
        var a=$(".name-width p").text().length  ;
        if(!nLighboxStatus){
            if (scroll > stickyOffset){ 
                sticky.addClass('fixed');
                $('.logo').addClass('sml-logo');
                $('.notification .padding-tb-25').addClass('sml-notification');
                $('.header-global-form').addClass('sml-search-padding');
                $('.profile-pic').addClass('sml-profile-pic');
                $('.navigation').addClass('top-56');
                $('.header-container').addClass('header-container-bg');
                $('.navbar-brand').addClass('sml-navbar-brand');
                if(a<=13)
                {
                    $(".name-width p").css('padding-top','10px');
                }
                else{
                    $(".name-width p").css('padding-top','5px');
                }
            }
            else{
                if(!isQuizLayout){
                    sticky.removeClass('fixed');
                    $('.logo').removeClass('sml-logo');
                    $('.notification .padding-tb-25').removeClass('sml-notification');
                    $('.profile-pic').removeClass('sml-profile-pic');
                    $('.header-global-form').removeClass('sml-search-padding');
                    $('.navigation').removeClass('top-56');
                    $('.header-container').removeClass('header-container-bg');
                    $('.navbar-brand').removeClass('sml-navbar-brand');
                    if(a<=13)
                    {
                        $(".name-width p").css('padding-top','15px');
                    }
                    else{
                        $(".name-width p").css('padding-top','5px');
                    }
                }
            }
        }
        
        
    });
  });
  
});

/*tabdrop function call*/
//function callTabDrop(classname){
//    $("." + classname).tabdrop();
//}

/*Auto size text area js call*/
function callAutoSizeTextArea(){
    $('.animated').autosize({append: "\n"});
}
/*Auto size text area css height set call this when comment is submited*/
function setAutosizeTextAreaHeight(height1){
    $('.animated').css('height',height1);
}
/*Show tooltip */
function showTooltip(){
    $('[data-toggle="tooltip"]').tooltip();      
}


/* STICKY HEADER HORIZONTAL SCROLLIG*/	
$(window).scroll(function(){
    $('.header-container').css('left',-$(window).scrollLeft());
});

/*make visible top group tabs  */
$(window).load(function() {
    $(".group-tabs-dropdown").css("overflow","visible");
    $(".document-tabs").css("overflow","visible");
    
    
});

/* temporary message show*/
function showTemporaryMessage(){
    $(".temp-message").show( );
    setTimeout(function(){
        $(".temp-message").hide();
    },10*1000);
}

/*open success pop up url attribute fetch*/
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
/*show more data js*/
 
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    function showMoreAmountdata(limit,className){
        
        $('.'+className).each(function() {
            var content = $(this).html();
            if(content.length > limit) {
                var c = content.substr(0, limit);
                var h = content.substr(limit, content.length - limit);
                var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<label class="morelink">' + moretext + '</label></span>';
                $(this).html(html);
            }
        });
    }
    function setMoreAmountDataLabel(ele){
        if($(ele).hasClass("less")) {
            $(ele).removeClass("less");
            $(ele).html(moretext);
        } else {
            $(ele).addClass("less");
            $(ele).html(lesstext);
        }
        $(ele).parent().prev().toggle();
        $(ele).prev().toggle();
        return false;

    }
