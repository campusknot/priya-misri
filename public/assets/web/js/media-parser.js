
// Simple social media parser
// scans the document to parse the social media urls
// and appends the respective social media's embeded content

/*
// Regex for popular social media's urls

// 1. Youtube
// ^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$
// Source: http://stackoverflow.com/a/10315969/509482

function getChannelName(url){

	var channels = [{
		name: 'youtube',
		pattern: /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/
	},
	{
		name: 'vimeo',
		pattern: /(http|https)?:\/\/(www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|)(\d+)(?:|\/\?)/
	}];
}*/

// source: https://noembed.com/providers
var NOEMBED_PROVIDERS = [{"name":"Clickhole","patterns":["http://www\\.clickhole\\.com/[^/]+/[^/]?"]},{"patterns":["https?://(?:www\\.)?twitter\\.com/(?:#!/)?[^/]+/status(?:es)?/(\\d+)/photo/\\d+(?:/large|/)?$","https?://pic\\.twitter\\.com/.+"],"name":"Twitter"},{"name":"CloudApp","patterns":["http://cl\\.ly/(?:image/)?[0-9a-zA-Z]+/?$"]},{"name":"Boing Boing","patterns":["http://boingboing\\.net/\\d{4}/\\d{2}/\\d{2}/[^/]+\\.html"]},{"name":"Path","patterns":["https?://path\\.com/p/([0-9a-zA-Z]+)$"]},{"name":"The AV Club","patterns":["https?://(?:www\\.)?avclub\\.com/article/[^/]+/?$"]},{"name":"Nooledge","patterns":["https?://www\\.nooledge\\.com/\\!/Vid/.+","https?://v\\.nldg\\.me/.+"]},{"name":"SoundCloud","patterns":["https?://soundcloud.com/.*/.*"]},{"patterns":["https?://(?:www|mobile\\.)?twitter\\.com/(?:#!/)?[^/]+/status(?:es)?/(\\d+)","https?://t\\.co/[a-zA-Z0-9]+"],"name":"Twitter"},{"name":"Urban Dictionary","patterns":["http://www\\.urbandictionary\\.com/define\\.php\\?term=.+"]},{"patterns":["https?://(?:www\\.)?vimeo\\.com/.+"],"name":"Vimeo"},{"name":"iTunes Movie Trailers","patterns":["http://trailers\\.apple\\.com/trailers/[^/]+/[^/]+"]},{"name":"ASCII Art Farts","patterns":["http://www\\.asciiartfarts\\.com/[0-9]+\\.html"]},{"patterns":["https?://[^\\.]+\\.wikipedia\\.org/wiki/(?!Talk:)[^#]+(?:#(.+))?"],"name":"Wikipedia"},{"name":"Monoprice","patterns":["http://www\\.monoprice\\.com/products/product\\.asp\\?.*p_id=\\d+"]},{"name":"The Verge","patterns":["http://(?:www\\.)?theverge\\.com/\\d{4}/\\d{1,2}/\\d{1,2}/\\d+/[^/]+/?$"]},{"patterns":["http://www.traileraddict.com/trailer/[^/]+/trailer"],"name":"TrailerAddict"},{"patterns":["http://www\\.duffelblog\\.com/\\d{4}/\\d{1,2}/[^/]+/?$"],"name":"The Duffel Blog"},{"name":"Wired","patterns":["https?://(?:www\\.)?wired\\.com/([^/]+/)?\\d+/\\d+/[^/]+/?$"]},{"patterns":["https?://(?:www\\.)?vice\\.com/[^/]+/?"],"name":"VICE"},{"name":"Qik","patterns":["http://qik\\.com/video/.*"]},{"name":"Spotify","patterns":["https?://open\\.spotify\\.com/(track|album)/([0-9a-zA-Z]{22})"]},{"patterns":["http://(?:www\\.)?xkcd\\.com/\\d+/?"],"name":"XKCD"},{"name":"Skitch","patterns":["https?://(?:www.)?skitch.com/([^/]+)/[^/]+/.+","http://skit.ch/[^/]+"]},{"patterns":["http://lockerz\\.com/[sd]/\\d+"],"name":"Lockerz"},{"name":"Gfycat","patterns":["http://gfycat\\.com/([a-zA-Z]+)"]},{"patterns":["http://(?:www\\.)?beeradvocate\\.com/beer/profile/\\d+/\\d+"],"name":"Beer Advocate"},{"name":"Picplz","patterns":["http://picplz\\.com/user/[^/]+/pic/[^/]+"]},{"name":"Twitpic","patterns":["http://(?:www\\.)?twitpic\\.com/([^/]+)"]},{"patterns":["http://www\\.twitlonger\\.com/show/[a-zA-Z0-9]+","http://tl\\.gd/[^/]+"],"name":"Twitlonger"},{"patterns":["http://arstechnica\\.com/[^/]+/\\d+/\\d+/[^/]+/?$"],"name":"Ars Technica"},{"name":"The Onion","patterns":["http://www\\.theonion\\.com/articles/[^/]+/?"]},{"patterns":["https?://news.vice\\.com/[^/]+/?"],"name":"VICE News"},{"patterns":["https?://www\\.23hq\\.com/.*/photo/.*","https?://alpha\\.app\\.net/.*/post/.*","https?://photos\\.app\\.net/.*/.*","https?://live\\.amcharts\\.com/.*","https?://www\\.animatron\\.com/project/.*","https?://animatron\\.com/project/.*","https?://animoto\\.com/play/.*","https?://audiosnaps\\.com/k/.*","https?://blackfire\\.io/profiles/.*/graph","https?://blackfire\\.io/profiles/compare/.*/graph","https?://cacoo\\.com/diagrams/.*","https?://img\\.catbo\\.at/.*","https?://public\\.chartblocks\\.com/c/.*","https?://chirb\\.it/.*","https?://www\\.circuitlab\\.com/circuit/.*","https?://www\\.clipland\\.com/v/.*","https?://www\\.clipland\\.com/v/.*","https?://clyp\\.it/.*","https?://clyp\\.it/playlist/.*","https?://codepen\\.io/.*","https?://codepen\\.io/.*","https?://codepoints\\.net/.*","https?://codepoints\\.net/.*","https?://www\\.codepoints\\.net/.*","https?://www\\.codepoints\\.net/.*","https?://www\\.collegehumor\\.com/video/.*","https?://coub\\.com/view/.*","https?://coub\\.com/embed/.*","https?://crowdranking\\.com/.*/.*","https?://www\\.dailymile\\.com/people/.*/entries/.*","https?://www\\.dailymotion\\.com/video/.*","https?://.*\\.deviantart\\.com/art/.*","https?://.*\\.deviantart\\.com/.*#/d.*","https?://fav\\.me/.*","https?://sta\\.sh/.*","https?://.*\\.didacte\\.com/a/course/.*","https?://www\\.dipity\\.com/.*/.*/","https?://docs\\.com/.*","https?://www\\.docs\\.com/.*","https?://dotsub\\.com/view/.*","https?://edocr\\.com/docs/.*","https?://egliseinfo\\.catholique\\.fr/.*","https?://embedarticles\\.com/.*","https?://.*\\.flickr\\.com/photos/.*","https?://flic\\.kr/p/.*","https?://fiso\\.foxsports\\.com\\.au/isomorphic-widget/.*","https?://fiso\\.foxsports\\.com\\.au/isomorphic-widget/.*","https?://www\\.funnyordie\\.com/videos/.*","https?://.*\\.geograph\\.org\\.uk/.*","https?://.*\\.geograph\\.co\\.uk/.*","https?://.*\\.geograph\\.ie/.*","https?://.*\\.wikimedia\\.org/.*_geograph\\.org\\.uk_.*","https?://.*\\.geograph\\.org\\.gg/.*","https?://.*\\.geograph\\.org\\.je/.*","https?://channel-islands\\.geograph\\.org/.*","https?://channel-islands\\.geographs\\.org/.*","https?://.*\\.channel\\.geographs\\.org/.*","https?://geo-en\\.hlipp\\.de/.*","https?://geo\\.hlipp\\.de/.*","https?://germany\\.geograph\\.org/.*","https?://gty\\.im/.*","https?://gfycat\\.com/.*","https?://www\\.gfycat\\.com/.*","https?://gfycat\\.com/.*","https?://www\\.gfycat\\.com/.*","https?://huffduffer\\.com/.*/.*","https?://www\\.hulu\\.com/watch/.*","https?://www\\.ifixit\\.com/Guide/View/.*","https?://ifttt\\.com/recipes/.*","https?://infogr\\.am/.*","https?://instagram\\.com/p/.*","https?://instagr\\.am/p/.*","https?://instagram\\.com/p/.*","https?://instagr\\.am/p/.*","https?://www\\.isnare\\.com/.*","https?://www\\.kickstarter\\.com/projects/.*","https?://www\\.kitchenbowl\\.com/recipe/.*","https?://learningapps\\.org/.*","https?://mathembed\\.com/latex\\?inputText=.*","https?://mathembed\\.com/latex\\?inputText=.*","https?://meetup\\.com/.*","https?://meetu\\.ps/.*","https?://www\\.mixcloud\\.com/.*/.*/","https?://www\\.mobypicture\\.com/user/.*/view/.*","https?://moby\\.to/.*","https?://.*\\.nfb\\.ca/film/.*","https?://mix\\.office\\.com/watch/.*","https?://mix\\.office\\.com/embed/.*","https?://official\\.fm/tracks/.*","https?://official\\.fm/playlists/.*","https?://on\\.aol\\.com/video/.*","https?://www\\.oumy\\.com/v/.*","https?://pastery\\.net/.*","https?://pastery\\.net/.*","https?://www\\.pastery\\.net/.*","https?://www\\.pastery\\.net/.*","https?://.*\\.polldaddy\\.com/s/.*","https?://.*\\.polldaddy\\.com/poll/.*","https?://.*\\.polldaddy\\.com/ratings/.*","https?://portfolium\\.com/entry/.*","https?://www\\.quiz\\.biz/quizz-.*\\.html","https?://www\\.quizz\\.biz/quizz-.*\\.html","https?://rapidengage\\.com/s/.*","https?://reddit\\.com/r/.*/comments/.*/.*","https?://rwire\\.com/.*","https?://repubhub\\.icopyright\\.net/freePost\\.act\\?.*","https?://www\\.reverbnation\\.com/.*","https?://www\\.reverbnation\\.com/.*/songs/.*","https?://roomshare\\.jp/post/.*","https?://roomshare\\.jp/en/post/.*","https?://videos\\.sapo\\.pt/.*","https?://www\\.screenr\\.com/.*/","https?://www\\.scribd\\.com/doc/.*","https?://www\\.shortnote\\.jp/view/notes/.*","https?://shoudio\\.com/.*","https?://shoud\\.io/.*","https?://showtheway\\.io/to/.*","https?://.*\\.silk\\.co/explore/.*","https?://.*\\.silk\\.co/explore/.*","https?://.*\\.silk\\.co/s/embed/.*","https?://.*\\.silk\\.co/s/embed/.*","https?://sketchfab\\.com/models/.*","https?://sketchfab\\.com/models/.*","https?://sketchfab\\.com/.*/folders/.*","https?://www\\.slideshare\\.net/.*/.*","https?://fr\\.slideshare\\.net/.*/.*","https?://de\\.slideshare\\.net/.*/.*","https?://es\\.slideshare\\.net/.*/.*","https?://pt\\.slideshare\\.net/.*/.*","https?://.*\\.smugmug\\.com/.*","https?://soundcloud\\.com/.*","https?://speakerdeck\\.com/.*/.*","https?://speakerdeck\\.com/.*/.*","https?://streamable\\.com/.*","https?://streamable\\.com/.*","https?://content\\.streamonecloud\\.net/embed/.*","https?://sway\\.com/.*","https?://www\\.sway\\.com/.*","https?://ted\\.com/talks/.*","https?://www\\.nytimes\\.com/svc/oembed","https?://theysaidso\\.com/image/.*","https?://www\\.topy\\.se/image/.*","https?://.*\\.ustream\\.tv/.*","https?://.*\\.ustream\\.com/.*","https?://uttles\\.com/uttle/.*","https?://www\\.vevo\\.com/.*","https?://www\\.vevo\\.com/.*","https?://www\\.videojug\\.com/film/.*","https?://www\\.videojug\\.com/interview/.*","https?://vidl\\.it/.*","https?://vimeo\\.com/.*","https?://vimeo\\.com/album/.*/video/.*","https?://vimeo\\.com/channels/.*/.*","https?://vimeo\\.com/groups/.*/videos/.*","https?://vimeo\\.com/ondemand/.*/.*","https?://player\\.vimeo\\.com/video/.*","https?://vine\\.co/v/.*","https?://vine\\.co/v/.*","https?://.*\\.wiredrive\\.com/.*","https?://.*\\.yfrog\\.com/.*","https?://yfrog\\.us/.*"],"name":"oEmbed"},{"name":"IMDB","patterns":["http://(?:www\\.)?imdb.com/title/(tt\\d+)"]},{"name":"Vine","patterns":["https?://vine.co/v/[a-zA-Z0-9]+"]},{"name":"Imgur","patterns":["http://imgur\\.com/([0-9a-zA-Z]+)$"]},{"name":"Github Commit","patterns":["https?://github\\.com/([^/]+)/([^/]+)/commit/(.+)","http://git\\.io/[_0-9a-zA-Z]+"]},{"name":"Dropbox","patterns":["https?://www\\.(dropbox\\.com/s/.+\\.(?:jpg|png|gif))","https?://db\\.tt/[a-zA-Z0-9]+"]},{"patterns":["https://tube.switch.ch/videos/([a-z0-9]+)"],"name":"SWITCHTube"},{"patterns":["http://www\\.amazon\\.com/(?:.+/)?[gd]p/(?:product/)?(?:tags-on-product/)?([a-zA-Z0-9]+)","http://amzn\\.com/([^/]+)"],"name":"Amazon"},{"name":"Bash.org","patterns":["http://bash\\.org/\\?(\\d+)"]},{"name":"GiantBomb","patterns":["https?://www\\.giantbomb\\.com/videos/[^/]+/\\d+-\\d+/?"]},{"name":"Imgur","patterns":["http://imgur\\.com/gallery/[0-9a-zA-Z]+"]},{"patterns":["https?://gist\\.github\\.com/(?:[-0-9a-zA-Z]+/)?([0-9a-fA-f]+)"],"name":"Gist"},{"patterns":["https?://www\\.globalgiving\\.org/((micro)?projects|funds)/.*"],"name":"GlobalGiving"},{"name":"YouTube","patterns":["https?://(?:[^\\.]+\\.)?youtube\\.com/watch/?\\?(?:.+&)?v=([^&]+)","https?://(?:[^\\.]+\\.)?(?:youtu\\.be|youtube\\.com/embed)/([a-zA-Z0-9_-]+)"]}];

function setEmbedHTML(url, post) {
	$.ajax({
		type: 'GET',
		url: siteUrl + '/utility/media-parser?url=' + url,
		success: function(response){
			var data = JSON.parse(response);
			var postContent = $(post).find('.post-text');
                        
                        if($(postContent).find('.embed-content').length == 0)
                                $(postContent).append('<div class="embed-content">' + data.html + '</div>');
		},
		error: function(jqXHR, textStatus, error){
			console.log(error);
		}
	});
}

function hasNoembedSupport(url) {

	for ( var i = 0; i < NOEMBED_PROVIDERS.length; i++ ) {
		
		var channel 	= 	NOEMBED_PROVIDERS[i];
		var patterns 	= 	channel.patterns || [];
		var patCount	= 	patterns.length;

		for ( var j = 0; j < patCount; j++ ) {

			if ( new RegExp(patterns[j]).test(url) ) {
				return true;
			}
		}
	}

	return false;
}

function findUrlThumbnails()
{
    var $posts = $('.post');

    for( var i = 0; i < $posts.length; i++)
    {
        var post = $posts[i];
        var urlPath = '.post-text > p a:nth-of-type(1)';
        var $url = $(post).find(urlPath);

        if( $url.length == 1){
            var url = $url[0].href;
            // find social media based on url pattern
            if ( url ) {
                setEmbedHTML(url, post);
            }
        }
    }

}