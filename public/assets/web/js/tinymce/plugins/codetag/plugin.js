tinymce.PluginManager.add('customem', function(editor, url) {
    // Add a button that opens a window
    editor.addButton('customEmElementButton', {
        text: 'Custom EM',
        icon: false,
        onclick: function() {
            // Open window
            editor.windowManager.open({
                title: 'Please input text',
                body: [
                    {type: 'textbox', name: 'description', label: 'Text'}
                ],
                onsubmit: function(e) {
                    // Insert content when the window form is submitted
                    editor.insertContent('<emstart>EM Start</emstart><p>' + e.data.description + '</p><emend>EM End</emend>');
                }
            });
        }
    });

    // Adds a menu item to the tools menu
    editor.addMenuItem('customEmElementMenuItem', {
        text: 'Custom EM Element',
        context: 'tools',
        onclick: function() {
            editor.insertContent('<emstart>EM Start</emstart><p>Example text!</p><emend>EM End</emend>');
        }
    });
});