/* 
 * This is file for common javascript related functions that use in website
 */

//$(function() {
//    $(window).focus(function() {
//        console.log('Focus');
//    });
//ChangeEventStatus
//    $(window).blur(function() {
//        console.log('Blur');
//    });
//});

var nLoadNewDataStatus = 0; // status variable for ajax new load data if 1 = ajax already active 0 = inactive
var nLighboxStatus = 0; // status variable to check lightbox is open or not  0 = closed, 1=open
var currentRequest = null; //abort previous ajax request 
$.fn.preload = function () {
    //debugger;
    this.each(function () {
        $('<img/>')[0].src = this;
    });
}

// Usage:

$(['https://s3.amazonaws.com/campusknot/file/icon/banner-bg.jpg']).preload();

$(document).ready(function () {

    /* to call flash msg*/
    var msg = $('#flash_msg').text();
    if ($.trim(msg) != '') {
        $(".temp-message").show( );
        setTimeout(function () {
            $(".temp-message").hide();
        }, 10 * 1000);
    }
    $('#lightbox_content').click(function () {
        closeLightBox()
    });
    $('#slider_black_overlay').click(function () {
        hideSlider()
    });
});
function sendGoogleAnalytic(sUrl) {
    ga('send', 'pageview', sUrl);
}
function capitalizeFirstLetter(sString) {
    return sString.charAt(0).toUpperCase() + sString.slice(1);
}

function applyCapitalization(oInput) {
    $(oInput).val(capitalizeFirstLetter($(oInput).val()));
}

function clearInputs(formId)
{
    $("#" + formId)[0].reset();
}

function clearFieldInviteGroup()
{
    clearInputs('invite_user');
    $('#selected_emails').html('');
}

function openLightBox(showHtml)
{
    //$("body").addClass("modal-open");

    $('#light_box').css('display', 'block');
    $('#lightbox_content').html(showHtml);
    $('#lightbox_content').css('display', 'table');

    $('#lightbox_content').children().click(function (event) {
        event.stopPropagation();
    });
    nLighboxStatus = 1;

    //this is to stop background body scroll
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () {
        window.scrollTo(x, y);
    };
}

function closeLightBox()
{
    $('#lightbox_content').css('display', 'none');
    $('#lightbox_content').html('');
    $('#light_box').css('display', 'none');
    $("body").removeClass("modal-open");
    //this is to enable scrolling
    window.onscroll = function () {};
    nLighboxStatus = 0;
}

function showSlider()
{
    $("#slider_light_box_container").addClass('col-lg-5 col-md-5 col-sm-5 col-xs-5');
    $("#slider_light_box_container").removeClass('col-lg-9 col-md-9 col-sm-9 col-xs-9');
    $(".slider_loader").removeClass("hidden");
    $('#slider_light_box').css('display', 'block');
    $('#slider_light_box_container').animate({left: 0});
    //this is to stop background body scroll
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () {
        window.scrollTo(x, y);
    };
    $("header").css("position", "fixed");
    $(".navigation").addClass("sidebarFix");
    nLighboxStatus = 1;
}

function hideSlider()
{
    $("body").removeClass("modal-open");

    $('#slider_light_box_container').animate({left: 100 + '%'}, function () {
        $('#slider_light_box_content').html('');
        $('#slider_light_box').css('display', 'none');
    });
    $('#slider_light_box_content').animate({left: 100 + '%'}, function () {
        $('#slider_light_box_content').html('');
        $('#slider_light_box').css('display', 'none');
    });
    //this is to enable background body scroll
    window.onscroll = function () {};
    //console.log("Caled");
    $(".navigation").removeClass("sidebarFix");
    nLighboxStatus = 0;
}

function getCampuses(sEmail, sDiv)
{
    $.ajax({
        type: "GET",
        url: siteUrl + '/getCampus?email=' + sEmail,
        success: function (data) {
            $('#' + sDiv).html(data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function getProfileData(sUrl, sBrowserUrl)
{
    $.ajax({
        type: "GET",
        url: sUrl,
        success: function (data) {
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('.profile-Content').html(data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function changeButtonAbility(oElement, sIdToEnable)
{
    if ($(oElement).is(':checked')) {
        $('#' + sIdToEnable).removeAttr('disabled');
    } else {
        $('#' + sIdToEnable).attr('disabled', 'disabled');
    }
}

function showAddGroupLightBox()
{
    sendGoogleAnalytic(siteUrl + '/group/add-group');
    $.ajax({
        type: "GET",
        url: siteUrl + '/group/add-group',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showAddCourseGroupLightBox()
{
    sendGoogleAnalytic(siteUrl + '/group/add-course-group');
    $.ajax({
        type: "GET",
        url: siteUrl + '/group/add-course-group',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showProfileImageLightBox(sImageSrc)
{
    var data = '<div class="clearfix modal-content photo-detail"><div class="photo-detail-left"><button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">X</span></button><img src="' + sImageSrc + '" /></div></div>';
    openLightBox(data);
}

function showAddEventLightBox()
{
    sendGoogleAnalytic(siteUrl + '/event/add-event');
    $.ajax({
        type: "GET",
        url: siteUrl + '/event/add-event',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}


function showLoader(id, img) {
    //alert(img);
    $('#' + id).hide();
    $('#' + id).html('555');
}
function submitAjaxForm(sFormId, sUrl, oClickedElement)
{
    $(oClickedElement).prop('disabled', true);

    var formData = new FormData($('#' + sFormId)[0]);
    $.ajax({
        type: "POST",
        url: sUrl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $(oClickedElement).prop('disabled', false);
            var parsedData = data.split(":to:");

            if (parsedData.length == 2)
            {
                window.location = parsedData[1];
            } else {
                $('#lightbox_content').html(data);
                $('#lightbox_content').children().click(function (event) {
                    event.stopPropagation();
                });
            }
        },
        error: function (data) {
            $(oClickedElement).prop('disabled', false);
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function submitReport(sFormId, sUrl, oClickedElement)
{
    var formData = new FormData($('#'+sFormId)[0]);
    $.ajax({
        headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
        type: "POST",
        url: sUrl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $('.report-error').remove();
           $(oClickedElement).siblings('.remove-form').trigger('click');
        },
        error: function (data) {
            $(oClickedElement).prop('disabled', false);
            if(data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            else
            {
                var error = data.responseText;
                var result = $.parseJSON(error);
                $('.report-error').remove();
                $(oClickedElement).before("<span class='ck-error report-error'>"+result.report_text+"</span>");
            }
            console.log('Error:', data);
        }
    });
}

function openEditInfo(sEditDivId, sInfoDivId)
{
    $('#' + sEditDivId).css('display', 'block');
    $('#' + sInfoDivId).css('display', 'none');
}

function hideEditInfo(sEditDivId, sInfoDivId)
{
    $('#' + sEditDivId).css('display', 'none');
    $('#' + sInfoDivId).css('display', 'block');
}

function showEditGroupImageLightBox(nIdGroup)
{
    sendGoogleAnalytic(siteUrl + '/group/edit-group-image?id_group=' + nIdGroup);
    $.ajax({
        type: "GET",
        url: siteUrl + '/group/edit-group-image?id_group=' + nIdGroup,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

//Priya ::image upload for profile 
//not in use
//function showEditProfileImageLightBox()
//{
//    sendGoogleAnalytic(siteUrl + '/user/edit-profile-image');
//    $.ajax({
//        type: "GET",
//        url: siteUrl + '/user/edit-profile-image',
//        success: function (data) {
//            openLightBox(data);
//        },
//        error: function (data) {
//            if(data.status == 401)
//            {
//                window.location = siteUrl + '/home';
//            }
//            console.log('Error:', data);
//        }
//    });
//}

function leaveGroup(sConfirmationMessage, nIdGroup)
{

    swal({
        title: "",
        text: sConfirmationMessage, //"You will not be able to recover this imaginary file!",
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    sendGoogleAnalytic(siteUrl + '/group/leave-group/' + nIdGroup);
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/group/leave-group/' + nIdGroup,
                        success: function (data) {
                            var parsedData = data.split(":to:");

                            if (parsedData.length == 2)
                            {
                                window.location = parsedData[1];
                            } else
                            {
                                openLightBox(data);
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                } else {
                    //swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
}

function leaveOptionClicked(oSelectedOption)
{
    if ($(oSelectedOption).val() === "CM")
    {
        $('#existing_members').removeClass('hidden');
    } else
    {
        $('#existing_members').addClass('hidden');
    }
}

function showRemoveLink(nIdGroupMember)
{
    $('#remove_' + nIdGroupMember).removeClass('hidden');
}

function hideRemoveLink(nIdGroupMember)
{
    $('#remove_' + nIdGroupMember).addClass('hidden');
}

function removeMemberFromGroup(sConfirmationMessage, nIdGroup, nIdUser)
{

    swal({
        title: "",
        text: sConfirmationMessage,
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    sendGoogleAnalytic(siteUrl + '/group/remove-member/' + nIdGroup + '/' + nIdUser);
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/group/remove-member/' + nIdGroup + '/' + nIdUser,
                        success: function (data) {
                            var parsedData = data.split(":to:");

                            if (parsedData.length == 2)
                            {
                                window.location = parsedData[1];
                            } else
                            {
                                openLightBox(data);
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                } else {
                    //swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
}

/* Functions below used in user-profile */
function isNumberKey(evt) {

    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46) {
        return false;
    }
    if (charCode != 46) {
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            return true;
        }
    }
}

function year_must_low(end_year, statrt_year)
{
    if (end_year >= statrt_year)
    {
        return true;
    } else
    {
        return false;
    }
}

function year_vs_crnt_year(field_year) {
    var d = new Date();
    var crnt_year = d.getFullYear();

    if (field_year <= crnt_year)
    {
        return true;
    } else
    {
        return false;
    }
}

function displaySuccessNotification(msg)
{
    $('#success_msg').html(msg);
    $("#success-alert").show();
    $("#success-alert").alert();
    $("#success-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#success-alert").hide();
    });
}

function displayWarningNotification(msg)
{
    $('#warning_msg').html(msg);
    $("#warning-alert").show();
    $("#warning-alert").alert();
    $("#warning-alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#warning-alert").hide();
    });
}

function setActiveTab(tab_name)
{
    $('#profile_modal_tab a[href = "#' + tab_name + '"]').tab('show');
    resetEducationModal();
    resetWorkExpModal();
    resetOrganizationModal();
    resetAwardHonorModal();
    resetAspirationsModal();
    resetJournalArticleModal();
    resetResearchWorkModal();
    resetPublicationsModal();
}

function cancelAllTab()
{
    $('#Modal_Profile_Detail').modal('hide');
}

function showIconAii(s_id)
{
    $('#' + s_id + '').css('visibility', 'visible');
}

function hideIconAii(s_id)
{
    $('#' + s_id + '').css('visibility', 'hidden');
}

function setVisibleUnvisible(c_id, p_id, txt_id)
{
    if ($('#' + c_id + '').is(":checked")) {
        $('#' + p_id + '').hide();
    } else {
        $('#' + p_id + '').show();
    }
}

function setVisibleUnvisibleByValue(c_id, p_id, c_val)
{
    if (c_val != '' && c_val != 0 && c_val != null) {
        $('#' + c_id + '').attr("checked", true);
        $('#' + p_id + '').hide();
    } else {
        $('#' + c_id + '').attr("checked", false);
        $('#' + p_id + '').show();
    }
}


function setNoDataValue(p_id, display_txt)
{

    UI = '<div class="pro_info" style="text-align: center; margin: 20px 0px; font-size: 25px; color: rgb(167, 164, 167);">' +
            '    <label>' + display_txt + '</label>' +
            '</div>';
    $('#' + p_id + '').html(UI);
}
/* Functions above used in user-profile */

function searchGroup(Url,cUrl)
{
    if($('#search_group').val() !='')
        chengeGroupListing('group_list', siteUrl + Url + $('#search_group').val(), siteUrl + cUrl);
}

function setPostType(sPostType)
{
    $('#post_type').val(sPostType);
    if (sPostType == "T")
    {
        $('#post_text').removeClass('hidden');
        $('#add_file').addClass('hidden');
        $('#add_code').addClass('hidden');
        $('#add_poll').addClass('hidden');
    } else if (sPostType == "C") {
        $('#add_code').removeClass('hidden');
        $('#add_file').addClass('hidden');
        $('#post_text').addClass('hidden');
        $('#add_poll').addClass('hidden');
    } else
    {
        $('#post_text').removeClass('hidden');
        $('#add_file').removeClass('hidden');
        $('#add_code').addClass('hidden');
        //$("#add_file .file-attachment-cancel").removeClass('hidden');

        if (sPostType == "D") {
            $('#attachment_file').attr('accept', '.doc,.docx,.pages,.rtf,.txt,.wp,.numbers,.xls,.xlsx,.csv,.key,.ppt,.pptx,.pps,.mdb,.acc,.accdb,.pdf,.zip,.rar');
            $('#add_poll').addClass('hidden');
        } else {
            $('#attachment_file').attr('accept', '.bmp,.jpg,.jpeg,.png,.svg');
            $('#add_poll').addClass('hidden');
        }

        if (sPostType == "P") {
            $('#add_poll').removeClass('hidden');
            // $(" #add_file .file-attachment-cancel").addClass('hidden');
        }
    }
}

function setPostCommentType(sPostType, nIdPost)
{
    $('#comment_type_' + nIdPost).val(sPostType);
}

function editPoll(nIdPost,sType)
{ 
    //sType is used to differenciate copy and edit functionality 
    $.ajax({
            type: "GET",
            url: siteUrl + '/post/add-poll/'+nIdPost+'?poll_type='+sType,
            success: function (data) {
                if(data.success)
                    var nData = "<div class='edit-poll'>" + data.html+ "</div>";
                    openLightBox(nData);
            },
            error: function (data) {
                if(data.status == 401)
                {
                    window.location = siteUrl + '/home';
                }
                console.log('Error:', data);    
            }
    });
}

function deletePost(sConfirmationMessage, nIdPost)
{
    swal({
        title: "",
        text: sConfirmationMessage, //"You will not be able to recover this imaginary file!",
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/post/delete-post/' + nIdPost,
                        success: function (data) {
                            $('#' + nIdPost).remove();
                            $('#flash_msg').text(data.msg);
                            showTemporaryMessage();

                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                } else {
                    //swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
}

function setCommentType(sPostId, sCommentType)
{
    $('#comment_type_' + sPostId).val(sCommentType);
    if (sCommentType == "T" || sCommentType == "C")
    {
        $('#add_file_' + sPostId).addClass('hidden');
    } else
    {
        $('#add_file_' + sPostId).removeClass('hidden');
    }
}

function updatePostLike(sDivId, nIdPost)
{
    $.ajax({
        type: "GET",
        url: siteUrl + '/post/like-post/' + nIdPost,
        success: function (data) {
            // change id to class for multiple count chng. $('#'+sDivId).html(data);
            $('.' + sDivId).html(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function loadNewData(sDivId, sUrl, nCurrentPage, sAdditionalParams)
{
    if (!sAdditionalParams)
        sAdditionalParams = '';
    $('.more-data-loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: sUrl + '?page=' + nCurrentPage + sAdditionalParams,
        success: function (data) {
            $('.more-data-loader').addClass('hidden');
            $('#' + sDivId).append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend: function () {
            nLoadNewDataStatus = 1; // status set if ajax call is active
        },
        complete: function () {
            nLoadNewDataStatus = 0; // status set if ajax call is complete
        }
    });
}

function showNotificationSlider()
{
    $('#notification-count-lable').remove();
    showSlider();
    getNotifications(1);
}

function getNotifications(nCurrentPage)
{
    $.ajax({
        type: "GET",
        dataType: 'html',
        url: siteUrl + '/notification/notification-listing?page=' + nCurrentPage,
        success: function (data) {
            $('.slider_loader').addClass('hidden');
            $('#slider_light_box_content').append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function removeInvity(oElement)
{
    $(oElement).parents('.vR').remove();
}

function showPhotoDetails(nIdPost, nIdGroup)
{
    sendGoogleAnalytic(siteUrl + '/post/photo-detail/' + nIdPost + '/' + nIdGroup);
    if (typeof nIdGroup == 'undefined')
        nIdGroup = '';
    $.ajax({
        type: "GET",
        url: siteUrl + '/post/photo-detail/' + nIdPost + '/' + nIdGroup,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showPollDetail(nIdPost)
{
    showSlider();
    $.ajax({
        type: "GET",
        url: siteUrl + '/post/poll-detail/' + nIdPost,
        success: function (data) {
            $('.slider_loader').addClass('hidden');
            $('#slider_light_box_content').append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function changePollListing(sIdDiv, sUrl, sBrowserUrl)
{
    $('.switch-tab-loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: sUrl,
        success: function (data) {
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('#' + sIdDiv).html(data);
            $('#validation-errors').html('');
            $('#add_poll').removeClass('in');
            $('.switch-tab-loader').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function UpdateNotificationStatus(ChangedStatus, nid, nType)
{

    $.ajax({
        type: "GET",
        url: siteUrl + '/notification/update-notification-status/' + ChangedStatus + '/' + nid,
        success: function (data) {
            if (ChangedStatus == 'A')
                var status = "Accepted";
            else
                var status = "Rejected";

            $('#group_' + nid).html('<span class="status-' + ChangedStatus + '">' + status + '</span>');

            if (nType == 'GM_LIST')
            {
                //window.location.reload();
            }

        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function ChangeEventStatus(eid) {
    var eStatus = document.getElementById("id_status_" + eid);
    var Status = eStatus.options[eStatus.selectedIndex].value;
    var text = 'I am : ' + $("#id_status_" + eid + " option:selected").text();
    $.ajax({
        type: "GET",
        url: siteUrl + '/notification/update-event-status/' + Status + '/' + eid,
        success: function (data) {
            if ($("#id_status_" + eid).hasClass('change_status'))
                $("#id_status_" + eid).parent().html(text);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showEducationLightbox() {
    sendGoogleAnalytic(siteUrl + '/user/add-education');
    $.ajax({
        type: "GET",
        url: siteUrl + '/user/add-education',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showWorkExpLightbox() {
    sendGoogleAnalytic(siteUrl + '/user/add-workexperience');
    $.ajax({
        type: "GET",
        url: siteUrl + '/user/add-workexperience',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showOrgLightbox() {
    sendGoogleAnalytic(siteUrl + '/user/add-organization');
    $.ajax({
        type: "GET",
        url: siteUrl + '/user/add-organization',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showAwardLightBox() {
    sendGoogleAnalytic(siteUrl + '/user/add-award');
    $.ajax({
        type: "GET",
        url: siteUrl + '/user/add-award',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showAspirationLightBox() {
    sendGoogleAnalytic(siteUrl + '/user/add-aspiration');
    $.ajax({
        type: "GET",
        url: siteUrl + '/user/add-aspiration',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showJournalLightBox() {
    sendGoogleAnalytic(siteUrl + '/user/add-journal');
    $.ajax({
        type: "get",
        url: siteUrl + '/user/add-journal',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showResearchLightBox() {
    sendGoogleAnalytic(siteUrl + '/user/add-research');
    $.ajax({
        type: "get",
        url: siteUrl + '/user/add-research',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showPublicationLightBox() {
    sendGoogleAnalytic(siteUrl + '/user/add-publication');
    $.ajax({
        type: "get",
        url: siteUrl + '/user/add-publication',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function chengeGroupListing(sIdDiv, sUrl, sBrowserUrl)
{
    $('.switch-tab-loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: sUrl,
        success: function (data) {
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('#search_group').val('');  //clear search bar
            $('#' + sIdDiv).html(data);
            $('.switch-tab-loader').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getGroupDetailPage(sIdDiv, sUrl, sBrowserUrl)
{
    $('.switch-tab-loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: sUrl,
        async: false,
        success: function (data) {
            var response = data.html;
            $('.switch-tab-loader').addClass('hidden');
            
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('#right_sidebar').html(response['right_sidebar']);
            $('#' + sIdDiv).html(response["content"]);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showAddCourseLightBox()
{
    sendGoogleAnalytic(siteUrl + '/user/add-user-course');
    $.ajax({
        type: "GET",
        url: siteUrl + '/user/add-user-course',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getGroupRequestStatusData(nIdGroup, nPageNumber)
{
    $.ajax({
        type: "GET",
        url: siteUrl + '/group/group-request-status/' + nIdGroup + '?page=' + nPageNumber,
        success: function (data) {
            $('#invited_member_status').append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend: function () {
            nLoadNewDataStatus = 1; // status set if ajax call is active
        },
        complete: function () {
            nLoadNewDataStatus = 0; // status set if ajax call is complete
        }
    });
}

function showEventInviteLightBox($IdEvent)
{
    sendGoogleAnalytic(siteUrl + '/event/invite-member?id_event=' + $IdEvent);
    hideSlider();
    $.ajax({
        type: "GET",
        url: siteUrl + '/event/invite-member?id_event=' + $IdEvent,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getSingleEvent(nIdEvent)
{
    showSlider();
    $.ajax({
        type: 'GET',
        url: siteUrl + '/event/detail/' + nIdEvent,
        success: function (data) {
            $('.slider_loader').addClass('hidden');
            $('#slider_light_box_content').append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function editEvent(nIdEvent)
{
    sendGoogleAnalytic(siteUrl + '/event/add-event?id_event=' + nIdEvent);
    hideSlider();
    $.ajax({
        type: "GET",
        url: siteUrl + '/event/add-event?id_event=' + nIdEvent,
        success: function (data) {
            if (typeof (data.success) != "undefined" && data.success == false)
            {
                $('#flash_msg').text(data.msg);
                showTemporaryMessage();
            } else
                openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function deleteEvent(sConfirmationMessage, nIdEvent)
{
    swal({
        title: "",
        text: sConfirmationMessage, //"You will not be able to recover this imaginary file!",
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm)
            {
                if (isConfirm) {
                    $.ajax({
                        type: 'GET',
                        url: siteUrl + '/event/delete-event/' + nIdEvent,
                        success: function (data) {
                            var parsedData = data.split(":to:");

                            if (parsedData.length == 2)
                            {
                                window.location = parsedData[1];
                            } else
                            {
                                alert(data);
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                } else {
                    //swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
}

function getEventFilterByGroup(sClickedElementId, nIdGroup)
{
    var grp = $("#" + sClickedElementId);
    if ($("#" + sClickedElementId).parent().hasClass('dropdown-menu')) {
        var swap = $('.group-tabs-dropdown > li:eq(3)');
        tdiv1 = grp.clone();
        tdiv2 = swap.clone();
        grp.replaceWith(tdiv2);
        swap.replaceWith(tdiv1);
    }
    $('.group-tabs  li').removeClass('active');
    $("#" + sClickedElementId).addClass('active');

    if ($("#" + sClickedElementId).parent().hasClass('dropdown-menu')) {
        var other = $("#" + sClickedElementId);
        var ele = $(".group-tabs .nav-tabs li:eq(1)");
        ele.after(other.clone());
    }

    //event search str is clear when select a grp
    $('#event_filter_search_str').val('');
    $('#search_event').val('');

    $('#event_filter_id_group').val(nIdGroup);
    filterEventList('', nIdGroup);
}

function getEventFilterBySearchStr(sIdStr)
{
    if ($('#' + sIdStr).val() != '')
    {
        $('#event_filter_search_str').val($('#' + sIdStr).val());
        filterEventList();
    }
}
function filterEventList()
{
    $('.switch-tab-loader').removeClass('hidden');
    $.ajax({
         type: "GET",
         url: siteUrl + '/event/filter-event-listing',
         data: $('#event_filter_form').serialize(),
         success: function (data) {
            $('#event_list').html(data);
            $('#event_list_all').hide();
            $('#event_list').show();
            $('.switch-tab-loader').addClass('hidden');
         },
         error: function (data) {
            if(data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
         },
        beforeSend: function(){
            nLoadNewDataStatus = 1; // status set if ajax call is active
        },
        complete: function(){
            nLoadNewDataStatus = 0; // status set if ajax call is complete
        }
      });
}

function getEventMembers(nIdEvent, sUserStatus, sIdDiv, nPage)
{
    $('.tab-content  div').removeClass('active');
    $('#' + sIdDiv).addClass('active');
    $('#event_status').val(sUserStatus);

    $.ajax({
        type: 'GET',
        url: siteUrl + '/event/event-members-by-status/' + nIdEvent + '/' + sUserStatus + '?page=' + nPage,
        success: function (data) {
            if (nPage == 1)
                $('#' + sIdDiv).html(data);
            else
                $('#' + sIdDiv).append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

/* profile edu oraganization put form user_profile_content_edit_blade @priya*/
function getEducationInfo(idEducation)
{
    sendGoogleAnalytic(siteUrl + '/user/add-education/' + idEducation);
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-education/' + idEducation,
        success: function (data) {
            if (typeof (data.success) != "undefined" && data.success == false)
            {
                $('#flash_msg').text(data.msg);
                showTemporaryMessage();
            } else
                openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend: function () {
            // loader_start();
        }
    });
}

function getWorkExpLightbox(id) {
    sendGoogleAnalytic(siteUrl + '/user/add-workexperience/' + id);
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-workexperience/' + id,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getOrgInfoLightbox(id) {
    sendGoogleAnalytic(siteUrl + '/user/add-organization/' + id);
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-organization/' + id,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getAwardHonorInfoLightBox(id) {
    sendGoogleAnalytic(siteUrl + '/user/add-award/' + id);
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-award/' + id,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getJournalArticleInfo(id) {
    sendGoogleAnalytic(siteUrl + '/user/add-journal/' + id);
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-journal/' + id,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getResearchWorkInfo(id) {
    sendGoogleAnalytic(siteUrl + '/user/add-research/' + id);
    FormId = 'research_' + id;
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-research/' + id,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getPublicationsInfo(id) {
    sendGoogleAnalytic(siteUrl + '/user/add-publication/' + id);
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/add-publication/' + id,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}





// #################### START:-  EVENT SCROLL  ###################//


// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {

    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}
/* profile aspiration */
function getRemainingChar() {
    var text_max = 250;
    var text_length = $('#aspirations_desc_update').val().length;
    var text_remaining = text_max - text_length;

    $('#textarea_feedback').html(text_remaining);
}
// #################### END:-  EVENT SCROLL  ###################//

// #################### START:-  Fancy Alert / Confirmation    ###################//


function alartFancy(title, text)
{
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
            function (isConfirm) {
                if (isConfirm) {
                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
                } else {
                    swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });

}



function deleteUserProfileDetail(text, url, confirmButtonText, cancelButtonText)
{
    swal({
        title: "",
        text: text, //"You will not be able to recover this imaginary file!",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: confirmButtonText,
        cancelButtonText: cancelButtonText,
        closeOnConfirm: false,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    //alert(isConfirm);
                    window.location.href = url;
                }
            });
}
function getTime(seconds) {
    //how many seconds are left


    //how many minutes fits in the amount of leftover seconds
    var minutes = Math.floor(seconds / 60);
    var leftover = seconds - (minutes * 60);
    if (leftover < 10)
        leftover = '0' + leftover;
    //how many seconds are left
    //leftover = leftover - (minutes * 60);
    var leftTime = minutes + ':' + leftover;
    return leftTime;
}
// #################### END:-  EVENT SCROLL  ###################//
//not in use ...use for faculty acc suggetion
function getSectionFromCourse(course_id)
{
    $.ajax({
        type: 'GET',
        url: siteUrl + '/user/section/' + course_id,
        dataType: 'html',
        success: function (data) {
            $(".section input[name='section']").replaceWith(data);
            $(".section select[name='section']").replaceWith(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showAttendance(Show, sAttendanceType)
{
    if (sAttendanceType == 'submit')
    {
        $('.submit_atten').removeClass('hidden');
        $('.take_atten').addClass('hidden');
    } else if (sAttendanceType == 'take')
    {
        $('.submit_atten').addClass('hidden');
        $('.take_atten').removeClass('hidden');
    }
    if (Show == 1) {
        $(".attendance").slideDown();
        $('.attendance-bg-black').css('display', 'block');
        $('html').css('overflow', 'hidden');
        window.scrollTo(0, 0);
    } else {
        $(".attendance").slideUp();
        //take course attendace form reset
        $("#attendance")[0].reset();
        $("#attendance").find('.error').removeClass('error');
        $("#errors-attendance").html('');
        //take Group attendace form reset
        $("#group-attendance")[0].reset();
        $("#group-attendance").find('.error').removeClass('error');
        $("#errors-group-attendance").html('');
        //submit Group attendace form reset
        $("#group-attendance-submit")[0].reset();
        $("#group-attendance-submit").find('.error').removeClass('error');
        $("#errors-attendance-group").html('');
        $('.attendance-bg-black').css('display', 'none');
        $('html').css('overflow', 'auto');

    }
}

function changeStudentAttendance(nIduser, nIdLecture, sAttendanceType)
{
    var sAttenText = $('input[name=attendance_' + nIduser + '_' + nIdLecture + ']:checked').val();
    $.ajax({
        type: 'GET',
        url: siteUrl + '/' + sAttendanceType + '/change-attendance',
        data: {'id_user': nIduser, 'id_lecture': nIdLecture, 'sAttenText': sAttenText},
        success: function (data) {
            var response = data.html;
            var msg = data.message;
            $('#' + nIduser + '_' + nIdLecture).html(response);
            $('#btn-atten').trigger("click");
            $('#flash_msg').text(msg);
            showTemporaryMessage();
        },
    });
}


function getUserAttendanceLog(sUrl, sIds, el)
{
    sendGoogleAnalytic(sUrl);
//    $(".attendance-log-show").removeClass("show");
//    $(el).siblings(".attendance-log-show").toggleClass("show");
    $(".attendance-log-show").addClass("show");

    $.ajax({
        type: 'GET',
        url: sUrl,
        data: {'sIds': sIds},
        success: function (data) {
            $(".attendance-log-show").html(data);
        },
    });
}
//delete lecture
function removeAttenColumn(nTd,nLectureId,sMessage)
{
    swal(
        {
            title: "",
            text: sMessage,//"You will not be able to recover this imaginary file!",
            //  type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "Do not delete!",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm)
        {
            if (isConfirm) {
                $.ajax({
                    type: 'GET',
                    url: siteUrl + '/group/delete-lecture/'+ nLectureId ,
                    success: function (data) {
                        var from = $('#attendqance_from_dt').val();
                        var to = $('#attendqance_to_dt').val();
                        if(from == to)
                        {
                            $('#slider_light_box_content').html('');
                            showAttendaceDataSlider(nLectureId,siteUrl+'/group/admin-attendance-table','f');
                        }
                        else
                            $("#btn-atten").trigger("click");
                    },
                });
            }
        }        
    );
}
function showAddSellBookLightBox()
{
    sendGoogleAnalytic(siteUrl + '/book/book-sell');
    $.ajax({
        type: "GET",
        url: siteUrl + '/book/book-sell',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}
function showAddToWishlistLightBox()
{
    sendGoogleAnalytic(siteUrl + '/book/book-wishlist');
    $.ajax({
        type: "GET",
        url: siteUrl + '/book/book-wishlist',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function GoogleDisconnect(sConfirmationMessage)
{

    swal({
        title: "",
        text: sConfirmationMessage, //"You will not be able to recover this imaginary file!",
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    sendGoogleAnalytic(siteUrl + '/event/event-google-disconnect');
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/event/event-google-disconnect',
                        success: function (data) {
                            var parsedData = data.split(":to:");

                            if (parsedData.length == 2)
                            {
                                window.location = parsedData[1];
                            } else
                            {
                                openLightBox(data);
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                } else {
                    //swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
}
function searchdata(sUrl,sBrowserUrl,sId,sPutDataId)
{
    var sSearchStr = $('#'+sId).val();
    if(sSearchStr != '')
    {
        $.ajax({
            type: "GET",
            url: sUrl+'/'+sSearchStr ,
            success: function (data) {
                var response = data.html;
                $('#'+sPutDataId).html(response['content']);
                $('#right_sidebar').html(response['right_sidebar']);
                var nCurrentPage = data.nCurrentPage;
                var nLastPage = data.nLastPage;
                var strLength= $('#'+sId).val().length;
                $('#'+sId).focus();
                $('#'+sId)[0].setSelectionRange(strLength, strLength);

                if(sBrowserUrl+'/'+sSearchStr !=window.location){
                    //window.history.pushState({path:sBrowserUrl+'/'+sSearchStr},'',sBrowserUrl+'/'+sSearchStr);
                }
            }
        });
    }
}

function callGoogleCron(sUrl, nLastPage)
{

    $.ajax({
        type: "GET",
        url: siteUrl + sUrl + nLastPage,
        success: function (data) {

        }
    });
}

function deleteUserImage(nIdImage, sConfirmationMessage)
{
    swal({
        title: "",
        text: sConfirmationMessage, //"You will not be able to recover this imaginary file!",
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        cancelButtonText: "Cancel",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/user/delete-user-image/' + nIdImage,
                        success: function (data) {
                            var parsedData = data.split(":to:");

                            if (parsedData.length == 2)
                            {
                                window.location = parsedData[1];
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                }
            }
    );
}

function showDocsShareLightBox(nDocId)
{
    sendGoogleAnalytic(siteUrl + '/documents/document-share?id_document=' + nDocId);
    $.ajax({
        type: "GET",
        url: siteUrl + '/documents/document-share?id_document=' + nDocId,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function copyDocument(nDocId)
{
    sendGoogleAnalytic(siteUrl + '/documents/document-copy/' + nDocId);
    $.ajax({
        type: "GET",
        url: siteUrl + '/documents/document-copy/' + nDocId,
        success: function (data) {
            openLightBox(data.html);

        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function createFolder(sUrl, data)
{
    $('.file-upload-loader').css('display', 'block');
    $.ajax({
        type: "POST",
        url: sUrl,
        data: data,
        processData: false,
        contentType: false,
        success: function (data) {

            location.reload();
        },
        error: function (data) {
            $('.file-upload-loader').css('display', 'none');
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            var arr = JSON.parse(data.responseText);

            $('.file-upload-error').css('display', 'block');
            $.each(arr, function (index, value)
            {
                $('.error-message').append('<span>' + value + '</span>');
                console.log('Error:', value);

            });
            setTimeout(function () {
                $('.file-upload-error').css('display', 'none');
                $('.error-message').html('');
            }, 3000);

        }
    });
}

function htmlSpecialChars(text) {

    return text
            .replace(/&/g, "&amp;")
            .replace(/"/g, "&quot;")
            .replace(/'/g, "&#039;")
            .replace(/</g, "&lt")
            .replace(/>/g, "&gt");

}

function getDocumentData(sUrl, sBrowserUrl)
{
    $(".loader_ajax").removeClass("hidden");
    $('#document_list').html('');
    $.ajax({
        headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
        url: sUrl,
        processData: false,
        contentType: false,
        success: function (data) {
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $(".loader_ajax").addClass("hidden");
            var response = data.html;
            $('#document_list').html(response['DocumentList']);
            $('.add-new-document-bar').html(response['CreateFolderHtml']);
            setNotClickable();
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function deleteDocument(nIdDoc, sConfirmationMessage)
{
    swal({
        title: "",
        text: sConfirmationMessage,
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        headers:
                                {
                                    'X-CSRF-Token': $('input[name="_token"]').val()
                                },
                        url: '/documents/document-delete/' + nIdDoc,
                        processData: false,
                        contentType: false,
                        success: function () {
                            $('#' + nIdDoc).remove();
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                }
            });
}

function deleteAccessRight(oElement,sConfirmMessage)
{
    var sId = $(oElement).children('form').attr('id');
    var formData = new FormData($('#'+sId)[0]);
    
    swal({
        title: "",
        text: sConfirmMessage,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
    function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                headers:
                        {
                            'X-CSRF-Token': $('input[name="_token"]').val()
                        },
                url: '/documents/document-revoke-access',
                data: formData,
                type: "POST",
                processData: false,
                contentType: false,
                success: function () {
                    window.location.reload();
                },
                error: function (data) {
                    if (data.status == 401)
                    {
                        window.location = siteUrl + '/home';
                    }
                    console.log('Error:', data);
                }
            });
        }
    });
}
//show alert box on error if no need to perform action.
function displayAlertBox(sConfirmationmsg)
{
    swal({
        title: "",
        text: sConfirmationmsg,
        //  type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: true,
    });
}
/*document breadcrumb last child set not clickable*/
function setNotClickable() {
    $breadcrumb_last_li_selector = $(".docs-top-heading-bar li").last().children();
    $breadcrumb_last_li_selector.removeAttr("onClick");
    callTabDrop('docs-top-heading-bar');
}

//  show more user listing when clicked on shared with + icon but now it is disable
/*function showDocsShareUserList($list,nIdDocument){
 
 $($list).siblings(".more-shared-with-user-listing").toggleClass('show');
 var isData = $($list).siblings(".more-shared-with-user-listing").html();
 
 if(isData.trim() == '')
 {
 var loader='<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
 $($list).siblings(".more-shared-with-user-listing").html(loader);
 $.ajax({
 url: siteUrl + '/documents/document-shared-userlist/'+nIdDocument,
 processData: false,
 contentType: false,
 success: function(data) {
 //            $($list).siblings(".more-shared-with-user-listing").css('display', 'block');
 
 $($list).siblings(".more-shared-with-user-listing").html(data);
 },
 error: function (data) {
 if(data.status == 401)
 {
 window.location = siteUrl + '/home';
 }
 console.log('Error:', data);
 }
 });
 }
 }*/

//serach document group module
function searchDocument(sUrl, sBrowserUrl, sId, sPutDataId)
{
    var sSearchStr = $('#' + sId).val();
    if (sSearchStr != '')
    {
        $('.loader_ajax').removeClass('hidden');
        $.ajax({
            type: "GET",
            url: sUrl + '?search_str=' + sSearchStr,
            success: function (data) {
                var response = data.html;
                $('#' + sPutDataId).html(response['content']);
                $('#right_sidebar').html(response['right_sidebar']);

                var strLength = $('#' + sId).val().length;
                $('#' + sId).focus();
                $('#' + sId)[0].setSelectionRange(strLength, strLength);
                if (sBrowserUrl + '?search_str=' + sSearchStr != window.location) {
                    window.history.pushState({path: sBrowserUrl + '?search_str=' + sSearchStr}, '', sBrowserUrl + '?search_str=' + sSearchStr);
                }
                //$('#'+sPutDataId).html(data);
            }
        });
    }
}

//sort document by name ,by owner name or by date in group module
function getDocumentSort(sSortBy, sUrl, sIdData)
{
    $('#document_list').addClass('hidden');
    $('.loader_ajax').removeClass('hidden');
    if ($('.' + sSortBy + '> .asc').hasClass('active'))
    {
        var order_by = 'desc';
        var order_field = sSortBy;
        $('.column-sorting-options').children().removeClass('active');
        $('.' + sSortBy).children('.desc').addClass('active');
    } else
    {
        var order_by = 'asc';
        var order_field = sSortBy;
        $('.column-sorting-options').children().removeClass('active');
        $('.' + sSortBy).children('.asc').addClass('active')
    }

    $.ajax({
        url: sUrl + '?order_by=' + order_by + '&order_field=' + order_field,
        processData: false,
        contentType: false,
        success: function (data) {
            var response = data.html;
            $('#' + sIdData).html(response['content']);
            $('#right_sidebar').html(response['right_sidebar']);
            $('#document_list').removeClass('hidden');
            $('.loader_ajax').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });

}
//used for document module
function getDocsSort(sSortBy, sUrl, sIdData)
{
    $('.loader_ajax').removeClass('hidden');
    if ($('.' + sSortBy + '> .asc').hasClass('active'))
    {
        var order_by = 'desc';
        var order_field = sSortBy;
        $('.column-sorting-options').children().removeClass('active');
        $('.' + sSortBy).children('.desc').addClass('active');
    } else
    {
        var order_by = 'asc';
        var order_field = sSortBy;
        $('.column-sorting-options').children().removeClass('active');
        $('.' + sSortBy).children('.asc').addClass('active')
    }

    $.ajax({
        url: sUrl + '?order_by=' + order_by + '&order_field=' + order_field,
        processData: false,
        contentType: false,
        success: function (data) {
            var response = data.html;
            $('#' + sIdData).html(response['DocumentList']);
            $('.loader_ajax').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });

}

//serach document document module
function searchDocs(sUrl, sBrowserUrl, sId, sPutDataId)
{
    var sSearchStr = $('#' + sId).val();
    if (sSearchStr != '')
    {
        $('.loader_ajax').removeClass('hidden');
        $.ajax({
            type: "GET",
            url: sUrl + '?search_str=' + sSearchStr,
            success: function (data) {
                var response = data.html;
                $('#' + sPutDataId).html(response['DocumentList']);
                $('.loader_ajax').addClass('hidden');
                /*if(sBrowserUrl+'?search_str='+sSearchStr  !=window.location){
                 window.history.pushState({path:sBrowserUrl+'?search_str='+sSearchStr },'',sBrowserUrl+'?search_str='+sSearchStr );
                 }*/
            }
        });
    }
}
//hide docs share listing when clicked anywhere in screen 
$(document).click(function () {
//  $(".more-shared-with-user-listing").css('display', 'none'); //hide the button
});

function showCustomBrowseButton(event, numFiles, label, $this)
{
    var input = $($this).parents('.input-group').find(" .file-attachment-name"),
            log = numFiles > 1 ? numFiles + ' files selected' : label;
    if (input.length) {
        input.val(log);
    } else {
        if (log)
            alert(log);
    }

}

function showAttendaceDataSlider(nIdLecture, sUrl, uType)
{
    if (uType == 'f')
    {
        $("#slider_light_box_container").addClass('col-lg-9 col-md-9 col-sm-9 col-xs-9');
        $("#slider_light_box_container").removeClass('col-lg-5 col-md-5 col-sm-5 col-xs-5');
    } else {
        $("#slider_light_box_container").removeClass('col-lg-9 col-md-9 col-sm-9 col-xs-9');
        $("#slider_light_box_container").addClass('col-lg-5 col-md-5 col-sm-5 col-xs-5');
    }
    $(".slider_loader").removeClass("hidden");
    $('#slider_light_box').css('display', 'block');
    $('#slider_light_box_container').animate({left: 0});
    $.ajax({
        type: "GET",
        url: sUrl + '/' + nIdLecture,
        success: function (data) {
            $('.slider_loader').addClass('hidden');
            $('#slider_light_box_content').html('');
            $('#slider_light_box_content').append(data.html);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend :function(){
            if(currentRequest != null )
            {
                currentRequest.abort();
            }
        }
    });
    //this is to stop background body scroll
    event.stopImmediatePropagation();
    var x = window.scrollX;
    var y = window.scrollY;
    window.onscroll = function () {
        window.scrollTo(x, y);
    };
    $("header").css("position", "fixed");
    $(".navigation").addClass("sidebarFix");
    nLighboxStatus = 1;
}

function loadAttendanceData(sUrl, nCurrentPage, sAdditionalParams)
{
    if (!sAdditionalParams)
        sAdditionalParams = '';
    $('.more-data-loader').removeClass('hidden');
    currentRequest = $.ajax({
        type: "GET",
        url: sUrl + '?page=' + nCurrentPage + sAdditionalParams,
        success: function (data) {
            $('.more-data-loader').addClass('hidden');
            $('.fht-fixed-column .fancyTable tbody').append(data.rowview);
            $('.fht-fixed-body .fht-tbody tbody').append(data.html);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend: function () {
            nLoadNewDataStatus = 1; // status set if ajax call is active
        },
        complete: function () {
            nLoadNewDataStatus = 0; // status set if ajax call is complete
        }
    });
}

function getLikeUserList(nIdPost)
{
    $.ajax({
        type: "GET",
        url: siteUrl + '/post/post-Like-user/' + nIdPost,
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function callFollowUser(ele, sUrl, nFollowFlag, nIdUser)
{
    $.ajax({
        type: "GET",
        url: sUrl,
        success: function (data) {
            if (nFollowFlag == 1)
                var html = '<a href="javascript:void(0);" class="follow" onclick="callFollowUser(this,\'' + siteUrl + '/user/follow/' + nIdUser + '\',0)">Follow</a>';
            else
                var html = '<a href="javascript:void(0);" class="follow" onclick="callFollowUser(this,\'' + siteUrl + '/user/follow/' + nIdUser + '/unfollow\',1)">Following</a>';

            $(ele).parent().html(html);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}
// added by misri
function loadCourseAttendanceData(sIdDiv, sUrl, sBrowserUrl)
{
    $('.switch-tab-loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: sUrl,
        async: false,
        success: function (data) {
            var response = data.html;
            $('#' + sIdDiv).html(response);
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('.switch-tab-loader').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function loadGroupAttendanceData(sIdDiv, sUrl, sBrowserUrl)
{
    $('.switch-tab-loader').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: sUrl,
        async: false,
        success: function (data) {
            var response = data.html;
            $('#' + sIdDiv).html(response);
            if (sBrowserUrl != window.location) {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('.switch-tab-loader').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function showAddQuizLightBox()
{
    sendGoogleAnalytic(siteUrl + '/quiz/quiz-add/');
    $.ajax({
        type: "GET",
        url: siteUrl + '/quiz/quiz-add/',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function addQuizQuestion(nCount, nQueId)
{
    sendGoogleAnalytic(siteUrl + '/quiz/quiz-question-add');
    var url = siteUrl + '/quiz/quiz-question-add/' + nQueId;

    $.ajax({
        type: "GET",
        data: {'question_count': nCount, 'id_quiz': $('#id_quiz_add').val()},
        url: url,
        success: function (data) {
            if (nQueId == '') {
                $("#quiz_questions").append(data);
            } else
                $("#show_question_" + nCount).replaceWith(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function submitFormQuiz(sFormId, sUrl, oClickedElement,sFrom)
{
    var formData = new FormData($('#'+sFormId)[0]);
    $.ajax({
        type: "POST",
        url: sUrl,
        data: formData,
        processData: false,
        contentType: false,
        success: function(response) {
            if(response.success == true && sFrom == 'question')
            {
                $('#'+sFormId).parent().replaceWith(response.html);
                $('.question_total_mark').text(response.nQuizQuestionTotal);
                questionCount++;
                if($(oClickedElement).val() != 'save')
                    addQuizQuestion(questionCount,'');
                if($('.add-quiz').parent().hasClass(' slider_light_box_content'))
                    $('.show-total-points').removeClass('hidden');
                else
                    $('.show-total-points-header').removeClass('hidden');
                return 1;
            }
            else if(response.success == true && sFrom == 'quiz')
            {
                $('#id_quiz_add').val(response.nIdQuiz);
                $('#'+sFormId).find("*").removeClass("has-error");
                $('#'+sFormId).find("*").removeClass("error");
                $('.new-question').removeClass('hidden');
                $('.close-popup').removeClass('hidden');
                $('.or-text').removeClass('hidden');
                $('.quiz_total_mark').text(response.nQuizTotal);
                showQuizSavedLabel('form_add_quiz');
            }
            else if(response.success == true && sFrom == 'editPoint')
            {
                $('.quiz_total_mark').text(response.marks);
                showQuizSavedLabel('form_add_quiz');
            }
            else 
            {
                
                $('#'+sFormId).parent().replaceWith(response);
                return 0;
            }
        },
        error: function (data) {
            var error = data.responseText;
            var result = $.parseJSON(error);
            if(sFrom == 'editPoint')
            {
                $('#marks').parent().addClass('error');
                $('#marks').attr('title',result.marks);
            }
            if(data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function openQuizSlider(sUrl)
{
    showSlider();
    $.ajax({
        type: "GET",
        url: sUrl,
        success: function (data) {
            $('.slider_loader').addClass('hidden');
            $('#slider_light_box_content').append(data);
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            } else
            {
                $('.slider_loader').addClass('hidden');
                $('#slider_light_box_content').html('<div class="no-data">There is no data found.</div>');
            }

            console.log('Error:', data);
        }
    });
}

function changeQuizList(sIdDiv, sUrl, sBrowserUrl, sClickedElementId)
{
    $('.switch-tab-loader').removeClass('hidden');
    if (sClickedElementId != '')
    {
        $('.group-tabs > li').removeClass('active');
        $("#" + sClickedElementId).addClass('active');

//        if($("#"+sClickedElementId).parent().hasClass('dropdown-menu')){
//            var other = $("#"+sClickedElementId);
//            var ele = $(".ck-top-tabs > li:eq(2)");
//            ele.after(other.clone());
//            other.after(ele).remove();
//        }
    }
    $.ajax({
        type: "GET",
        url: sUrl,
        success: function (data) {
            if (sBrowserUrl != window.location)
            {
                window.history.pushState({path: sBrowserUrl}, '', sBrowserUrl);
            }
            $('#' + sIdDiv).html(data);
            $('.switch-tab-loader').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function getUserInvitedList(nIdQuiz, sId, nPage)
{
    if (nPage == 1)
        $('#' + sId + '_' + nIdQuiz).html('');

    $('#user_list_' + nIdQuiz).siblings('.spinner').removeClass('hidden');
    $.ajax({
        type: "GET",
        url: siteUrl + '/quiz/quiz-user-list/' + nIdQuiz + '?page=' + nPage,
        success: function (data) {
            if (nPage == 1)
                $('#' + sId + '_' + nIdQuiz).html(data.html);
            else
                $('#' + sId + '_' + nIdQuiz).append(data.html);
            $('#user_list_' + nIdQuiz).siblings('.spinner').addClass('hidden');
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        },
        beforeSend: function () {
            nLoadNewDataStatus = 1; // status set if ajax call is active
        },
        complete: function () {
            nLoadNewDataStatus = 0; // status set if ajax call is complete
        }
    });
}

function StartQuiz(nIdQuizUserInvite)
{
    $.ajax({
        type: "GET",
        url: siteUrl + '/quiz/quiz-start/' + nIdQuizUserInvite,
        success: function (data) {
            if (data == 'Success')
            {
                autoPaginate();
            }
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function openDraftedQuiz(nIdQuiz)
{
    $.ajax({
        type: "GET",
        url: siteUrl + '/quiz/quiz-publish/'+nIdQuiz,
        success: function (data) {
            window.location.reload();
        },
        error: function (data) {
            if(data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
            console.log('Error:', data);
        }
    });
}

function addMarks(formId,element)
{
    event.preventDefault();
    var formData = new FormData($('#form_'+formId)[0]);
    $.ajax({
        headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
        type: 'POST',
        url: siteUrl + '/quiz/add-student-marks',
        data: formData,
        processData: false,
        contentType: false,
        success: function(){
            var marks = $(element).children('.marks').val();
            $(element).parent().siblings('.alloted_marks').text(marks);
            $(element).parent(".allot-marks-input").css("display","none");
        },
        error: function(data){
            var result = data.responseText
            var response = jQuery.parseJSON(result);
            $(element).find('.ck-error').remove();
            $.each(response, function(index, value)
            {
                if (value.length != 0)
                {
                    $(element).prepend('<div class="ck-error text-left">'+ value +'</div>');
                }
            });
        }
    });
}

function submitUserAnswer(idForm, event)
{
    event.preventDefault();
    var formData = new FormData($('#q_' + idForm)[0]);
    $.ajax({
        type: 'POST',
        url: siteUrl + '/quiz/add-student-answer',
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            var response = jQuery.parseJSON(data);
            if (response.response_status == 0)
            {
                $.each(response.errors, function (index, value)
                {
                    if (value.length != 0)
                    {
                        $('#error_' + idForm).remove();
                        $('#q_' + idForm).prepend('<div id="error_' + idForm + '" class="ck-error text-left">' + value + '</div>');
                    }
                });
            } else
            {
                $('#error_' + idForm).remove();
                showQuizSavedLabel('q_' + idForm);
                $('#index_' + idForm).addClass('attempted');
            }
        }
    });
}

function showQuizSavedLabel(targetParent)
{
    var targetDiv = '#' + targetParent;
    $(targetDiv).parent().siblings(".saved-answer-label").css("display", "block");
    $(targetDiv).parent().siblings(".saved-answer-label").delay(2000).fadeOut();
}

function deleteQuestion(ncount, nIdQuestion, sConfirmationMessage)
{
    swal({
        title: "",
        text: sConfirmationMessage,
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm)
            {
                if (isConfirm) {
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/quiz/quiz-question-delete/' + nIdQuestion,
                        success: function (data) {
                            if (data == 'success')
                            {
                                $('#show_question_' + ncount).remove();
                                var ncounter = 1;
                                $(".counter_cal").each(function (i) {
                                    $(this).text(ncounter);
                                    ncounter++;
                                });
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                }
            }
    );
}

function deleteQuiz(nIdQuiz, sConfirmationMessage)
{
    swal({
        title: "",
        text: sConfirmationMessage,
        //  type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "Do not delete!",
        closeOnConfirm: true,
        closeOnCancel: true
    },
            function (isConfirm)
            {
                if (isConfirm) {
                    $.ajax({
                        type: "GET",
                        url: siteUrl + '/quiz/quiz-delete/' + nIdQuiz,
                        success: function (data) {
                            if (data == 'success')
                            {
                                $('#quiz_' + nIdQuiz).remove();
                            }
                        },
                        error: function (data) {
                            if (data.status == 401)
                            {
                                window.location = siteUrl + '/home';
                            }
                            console.log('Error:', data);
                        }
                    });
                }
            }
    );
}

function deleteThisOption(el, nIdAnswerOption)
{

    if (nIdAnswerOption != '')
    {
        $.ajax({
            type: "GET",
            url: siteUrl + '/quiz/answer-option-delete/' + nIdAnswerOption,
            success: function (data) {
                if (data == 'success')
                {
                    $('#show_question_' + ncount).remove();
                    var ncounter = 1;
                    $(".counter_cal").each(function (i) {
                        $(this).text(ncounter);
                        ncounter++;
                    });
                }
            }
        });
    }

    var counter = $(el).parent().parent().children().size();
    console.log(counter);
    if (counter > 2) {
        $(el).parent(".single-mcq-answer").fadeOut("normal", function () {
            $(el).parent().remove();
        });
    }
}

function addQuestionMarks(formId, sUrl, element)
{
    element.preventDefault();
    var formData = new FormData($('#question_' + formId)[0]);
    $.ajax({
        headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
        type: 'POST',
        url: sUrl,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $('.allot-marks-input').css('display', 'none');
            $('#edit_marks_' + formId).text(data.marks);
            $('.question_total_mark').text(data.nQuizQuestionTotal);
            $(element).parent(".allot-marks-input").css("display", "none");
            var ele = $('#question_' + formId).closest('.question-wrapper');
            $(ele).siblings(".saved-answer-label").css("display", "block");
            $(ele).siblings(".saved-answer-label").delay(2000).fadeOut();
        },
        error: function (data) {
            var result = data.responseText
            var response = jQuery.parseJSON(result);
            $.each(response, function (index, value)
            {
                if (value.length != 0)
                {
                    $(element).html('<div class="ck-error text-left">' + value + '</div>');
                }
            });
        }
    });
}

function activatePoll(button, nIdPoll)
{
    $(button).button('loading');
    $.ajax({
        type: "GET",
        url: siteUrl + '/post/activate-poll/' + nIdPoll,
        success: function (response) {
            if (response.success == true)
            {
                if (response.redirect)
                    window.location.href = response.redirect;
                else
                    window.location.reload(true);
            } else {
                $('#flash_msg').text(response.error);
                showTemporaryMessage();
                $(button).button('reset');
            }
        },
        error: function (data) {
            if (data.status == 401)
            {
                window.location = siteUrl + '/home';
            }
        }
    });
}