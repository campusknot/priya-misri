var VueRouter = require('vue-router');
Vue.use(VueRouter);

import example from './components/Example.vue';
import home from './components/Home.vue';
import login from './components/Login.vue';
import feeds from './components/user/Feeds.vue';




const routes = [
   
        { path: '/home', component: example,
            children:[
                { path: '', component: home},
                { path: 'login', component: login}
            ]
        }
        
    
]

const router = new VueRouter({
    mode: 'history',
    base: __dirname,
    routes 
});

new Vue({
  router
}).$mount('#ckLandingPage');


//const routesApp = [
//   { path: '/', component: feeds }
//]


//
//const routerApp = new VueRouter({
//    mode: 'history',
//    routesApp // short for routes: routes
//});



//new Vue({
//  routerApp
//}).$mount('#ckWebApp');