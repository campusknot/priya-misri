<?php

return [

        /*
        |--------------------------------------------------------------------------
        | General Messages Language Lines
        |--------------------------------------------------------------------------
        |
        | The following language lines are used for various general
        | messages that we need to display to the user. You are free to modify
        | these language lines according to your application's requirements.
        |
        */

        // Common phrases
        'no_record_found' => 'There is no such data found that you are looking for.',
        'click_here' => 'Click :click_hint',
        'post_time' => ':time',
        'just_now' => 'Just now',
        'need_to_login' => 'You need to log in to access this page.',
        'all' => 'All',
        'action' => 'Action',
        'delete_record_alert' => 'Are you sure, you would like to delete the selected :record_name history?',
        'delete_confirm_button_text' => 'Yes, delete it!',
        'delete_cancel_button_text' => 'Do not delete!',
        'loading' => 'Loading...',
        'new' => 'New',
        'add' => 'Add', //new
        'browse' => 'Browse', //new
    
        //Email translations
        'dear' => 'Dear',
        'cheers' => 'Cheers,',
        'campusknot' => 'Campusknot',
        'on_campusknot' => 'on Campusknot',
        'privacy_policy' => 'Privacy Policy',
        'terms_of_use' => 'Terms of use ',
        'verification_mail_heading' => 'Verification',
        'verification_mail_welcome' => 'Welcome to :sCampusName ! ',
        'verification_mail_text' => 'Get connected with your classmates and professors by clicking on the link below',
        'login_mail_heading' => 'Welcome',
        'login_mail_text' => 'Welcome',
        'verification_mail_link' => 'Verify Now',
        'verification_link_send_successfully' => 'Verification link send successfully',//new
    
        'reset_password_mail_text' =>'We have received a request to reset the password for your Campusknot Account.',
        'reset_password_link' => 'Reset now',
    
        'group_invitation_heading' => 'Group Invitation',
        'group_invitation_text' => 'has invited you to join the group',
        'group_invitation_link' => 'Join Now',
    
        'new_group_creator_mail_heading' => 'Admin rights',
        'new_group_creator_mail_text' => 'has given you the admin rights for group ',
        'new_group_creator_mail_message' =>'You can now invite other users, share events and documents to the group.',
    
        'remove_member_heading' => 'Group membership',
        'remove_member_text' => 'You have been removed from',
        'remove_member_by' => 'by' ,
        
        'group_join_request_subject' => 'Campusknot-Group join request',
        'group_join_request_mail_heading' => 'Group Request',
        'group_join_request_mail_text' => 'wants to join group',
        'group_join_request_mail_link' => 'Accept',
    
        'group_request_approved_mail_heading' => 'Group joining request approved',
        'group_request_approved_mail_text' => 'has approved your request to join the group',
        'group_request_approved_mail_message' => 'You can now post questions, share notes, upload documents',
    
        'verification_mail_title' => 'Welcome to Campusknot',
        'verification_mail_subject' => 'Campusknot Email verification.',
        'verification_mail_body' => "To complete your registration, please click on the link below<br \> :verification_link",
        'contact_send' =>'Query from Contact Us Page',
        'subject_reply_contact' => 'Request received',
    
        'confirm_email' => 'Confirm Email',
        'confirm_email_text' => 'A confirmation email has been sent to',
        'confirm_email_click' => 'Click on the confirmation link to activate your account.',
        'confirmation_email' => 'Confirmation Email',

        'group_invitation_subject' => 'Campusknot Group invitation',
        'group_accept_invitation_subject' => 'Group joining request approved',
        'hello_user' => 'Hello :name,',
        'group_invitaion_mail_title' => 'Group invitaion',
        //'group_invite_mail_body' => ":invite_by has invited you to join  group in Campusknot. Please check out the link below to join this group.",
        'new_group_creator_mail_body' => 'I have given you the creator\'s right for :group_name. Please click on the link below.',
        
        'remove_group_member_subject' => 'You have been removed from the group',
        'remove_group_member_mail_title' => 'Admin has removed you.',
        'remove_group_member_mail_body' => 'Sorry!! :removed_by has removed you from :group_name group.',
        
        'event_invitation_subject' => 'Campusknot Event invitation',
        'event_invitaion_mail_title' => 'Event invitation',
        'event_invite_mail_body' => ":invite_by has invited you to join the event :event_name.",
        'event_invitation_link' => "RSVP",
        
        'event_cancelation_subject' => 'Campusknot Event cancelled',
        'event_cancelation_mail_title' => 'Event cancelled',
        'event_cancel_mail_body' => ":cancel_by has cancelled :event_name event from Campusknot.",
        
        'event_edit_subject' => 'Campusknot Event edited',
        'event_edit_mail_title' => 'Event edited',
        'event_edit_mail_body' => ":edit_by has edited :event_name event in Campusknot.",
    
        'password_reset' => 'Password reset', //new
        'send_newsletter' => 'New Features', //new
        
        // Home page
        'about' => 'About',
        'blog' => 'Blog',
        'mobile_app' => 'Mobile App',
        'login' => 'Log In',
        'contact' => 'Contact',
        'contact' => 'Contact Us',
        'send_message' => 'Send Message',
        'contact_send_successfully' =>'Send Your Information Successfully',//new
        'mobile_app_text' => 'Engage at your Fingertips',
        'slider_text' => 'Engage with your Classroom',
        'anytime' => 'Anytime,',
        'anywhere' => 'Anywhere!!!',
        'get_mobile_app' => 'Engage with your Classroom <br/> at your Fingertips!',
        'download_now' => 'Download Now!',
        'media_coverage' => 'Media Coverage',
        'feeds' => 'Feeds',
        'feeds_text' => 'Post your campus updates, Course queries and polls',
        'groups' => 'Groups',
        'user_list' => 'User List',
        'groups_text' => 'Create and manage groups for your classes',
        'attendance' => 'Attendance',
        'attendance_text' => 'Create and manage Attendance for your classes',
        'today_attendance_taken'=> 'Today\'s Lecture attendance already taken.',
        'member_exists_list'=> 'Member already in list.',
        'member_exists_list_successfully'=> 'Member is added in list successfully.',
        'docs' => 'Docs',
        'docs_text' => 'Create and manage Docs for your classes',
        'planner' => 'Planner',
        'browse' => 'Browse..',
        'compatible_browser' => 'Compatible Browser:',
        'edge' => 'Edge',
        'mozila' => 'Mozila',
        'chrome' => 'Chrome',
        'safari' => 'Safari',
    
        'planner_text' => 'Create and manage Planner for your classes',
        'Social_academic_management_system' => 'Social Academic Management System',
        'campusknot_groups_feature' => 'Create and manage groups for your classes, student organization, Greek organizations, and more.',
        'campusknot_feeds_feature' => 'Share updates, Post media content, and participate in discussions and polls.',
        'campusknot_docs_feature' => 'Upload, Organize, and Share Documents.',
        'campusknot_attendance_feature' => 'Take, Submit, and Track Attendance.  ',
        'campusknot_events_feature' => 'Keep up with Due Dates and Events.',
        'campusknot_notifications_feature' => 'Receive notifications for new course materials and updates from professors on the go.',
    
        'testimonial' => 'Testimonial',
        'testimonial_text' => 'Know what the professors at Mississippi State University have to say about Campusknot!',
        'sign_up' => 'Sign Up for Free',
        'get_in_touch' => 'Get in Touch',
        'All_rights_reserved' => '2017 All rights reserved',
//        'reply_contact'=>'<span>Thanks for writing to Customer Service at Campusknot.</span><br/> '
//        //                . 'We have created a service request ticket and your ticket reference number is mentioned in the subject line of this email. <br />'
//                        . 'Should you need any further assistance, please write to us at <a href="mailto:info@campusknot.com" target="_blank">info@campusknot.com</a>. ',
        
        'reply_contact'=>'<span>Thank you for contacting Campusknot. </span><br/> '
        //                . 'We have created a service request ticket and your ticket reference number is mentioned in the subject line of this email. <br />'
                        . 'We will reply back to you within 24 business hours of receiving your request. ',
        'contact_us' => 'Contact Us',
        
        // About page
        'about_text' => 'Campusknot is a technology company that <br> enhances overall learning experience of <br> College Students and Faculty Members!',
        
        //Add-course page
        'course_name' => 'Course name:',
        'course_code' => 'Course code:',
        'university' => 'University',
        'select_university' => 'Select University',
        'import_via_csv' => 'Import via csv',
        'become_member_confirmation' => 'Yes, I want to connect with my classmates.',
        'user_course_added_successful' => 'User courses added successfully.',
        
        //Send-verification-mail
        'verification_key' => 'Verification key',
        'user_already_verified' => 'User is already verified this account',
        'resend_verification_mail' => 'Resend Mail',
    
        //User registration page
        'signup_title_text' => 'Nice to meet you!',
        'first_name' => 'First name',
        'last_name' => 'Last name',
        'technical_support' => 'Technical Support',
        'business_licensing' => 'Business & Licensing',
        'press' => 'Press',
        'feedback' => 'Feedback',
        'email' => 'Email',
        'phone_number' => 'Please enter your phone number',
        'enter_phone_number' => 'Phone number',//newß
        'message_placeholder' => 'Message for Us',
        'xyz@university.edu' => 'xyz@university.edu',
        'signup_as_student' => 'Student',
        'signup_as_faculty' => 'Faculty',
        'password' => 'Password',
        'change_password' => 'Change Password',
        'confirm_password' => 'Confirm password',
        'select_campus' => 'Select campus',
        'terms_and_condition' => "*I have read and agree to the <a href=':terms' target='_blank'>Terms</a> and <a href=':privacy' target='_blank'>Privacy</a>.",
        'user_verification_fail' => 'User verification fails as the verification link might already be used once or tampered with.',
        'user_verification_success' => 'Your email has been successfully verified. Explore your campus by using your credentials!',
        'user_registored_success' => 'User registration has been completed. Please check your registered mail id for verification.',

        //Login page
        'login_page_title' => 'Nice to see you again!',
        'remember_me' => 'Remember me',
        'forgot_password' => 'Forgot your password?',
        'sign_up_free' => 'Sign up for Campusknot',
        'take_a_tour' => 'Take a tour',
        'incorrect_credentials' => 'Your username and password do not match.',
        'resend_mail_info' => 'Your account is not verified yet. Please click the link below if you have not received the verification email.',//new
        'resend_mail_button' => 'Resend Verification Email',//new
        
        //Forgot password page
        'forgot_password_page_title' => 'Forgot password',
        'or' => 'Or',
        'request_new_password' => 'Request new password',
        'not_verified_user' => 'Email ID that you provided has not been verified as yet.',
        'forgot_password_success' => 'Please click on the password reset link sent to your registered email to proceed to password reset.',
        'forgot_password_mail_subject' => 'Campusknot Reset password.',
        'forgot_password_mail_title' => 'Reset your password',
        'forgot_password_mail_body' => "To reset your password, please click on the link below<br \> :reset_password_link",

        //Reset password page
        'reset_password_page_title' => 'Reset password',
        'reset_password_fail' => 'Something went wrong in the reset password proccess. Please try again.',
        'reset_password_link_expired' => 'Reset password link has been expired.',//new
        'reset_password_link_param_missing' => 'Missing expected parameter to reset password.',
        'password_reset_success' => 'Your password has successfully been reset. Please log in using the new password.',

        //Main header
        'add_group' => 'Create Group',
        'add_event' => 'Add Event',
        'account_settings' => 'Account settings',
        'edit_profile' => 'Edit profile',
        'logout' => 'Logout',
    
        //Add group 
        'group_info_text' => '<span>Public</span>: All users can join this group <div><span>Private</span>: All users can request to join this group or wait for an Admin\'s invite</div> <div><span>Secret</span>: User requires an Admin invite to join this group and this group will not be visible to any other user except the invitees</div>',

        //Subheader
        'home' => 'Home',
        'groups' => 'Groups',
        'events' => 'Planner',
        'profile' => 'Profile',
        'search_user' => 'Search user by name',
        'search_group' => 'Search recommended group by name',
        'search_event' => 'Search event by name',
        'search' => 'Search',
        'polls' => 'Polls',
        'global_search_placeholder' => 'Search username or .edu Id',

    
    
        //Account settings
        'account' => 'Account',
        'name' => 'Name',
        //'change_account_settings' => 'Change your account settings.',
        'alternate_email' => 'Alternate Email',
        'current_password' => 'Current Password',
        'new_password' => 'New Password',
        'repeat_new_password' => 'Repeat new Password',
        'save' => 'Save',
        'cancel' => 'Cancel',
        'incorrect_current_password' => 'Password does not match with current password.',
        'current_password_and_new_password_same' => 'Current password and New password should not be the same.',
        'account_settings_change_success' => 'Your account settings has changed successfully.',
        
        'report_added_successful' => 'Your report has been registor successfully. We will take necessary action on that soon.',
        
        //Add course popup
        'add_course' => 'Add Course',
        'select_course' => 'Search via course name or code',
        'add_corse_group' =>'Create course group',
        'add_course_info' =>'This is a public group meant for students to connect, collaborate, and share.',
        'add_course_group_info' =>'This is a secret group, only visible to the admin and the invitees.',
    
    
        
        //Group pages
        'no_admin_groups' => 'You are currently not an admin for any group.<br /> Create some groups, engage the world! What say?.',
        'group_categories' => 'Group categories',
        'select_group_category' => 'Select group category',
        'select_group' => 'Select group',
        'group_name' => 'Group name',
        'PUB' => 'Public',
        'PVT' => 'Private',
        'SEC' => 'Secret',
        'about_group_text' => 'About group text',
        'create_group' => 'Create Group',
        'creator' => 'Creator :- ',
        'category' => 'Category :- ',
        'type' => 'Type : ',
        'group_feeds' => 'Group feeds',
        'members' => ':members_count Members',
        'member_removed_success'=>'Group member removed successfully',
        'group_members_list' => 'List of Group members',
        'photos_count' => ':photos_count Photos',
        'group_photos_list' => 'List of Group photos',
        'documents' => 'Documents',
        'documents_count' => ':documents_count Documents',
        'group_documents_list' => 'List of Group documents ',
        'events_count' => ':events_count Events',
        'group_events_list' => 'List of Group events',
        'about_group' => 'About Group',
        'group_settings' => 'Group settings',
        'group_settings_edit' => 'Edit group settings',
        'admin_remove_confirmation' => 'Are you sure you would like to delete this admin?',
        'invite_users' => 'Invite users',
        'invite_users_csv' => 'Invite via csv',
        'download_csv_sample'=> 'Download sample csv file: sample.csv',
        'leave_group' => 'Leave group',
        'join_group' => 'Join group',
        'join' => 'Join',
        'ask_join' => 'Request to Join',
        'recomended' => 'Recommended',
        'not_invited_to_join' => 'Sorry, You have not been invited to join this group as yet.',
        'new_member_of_group' => 'You are now a member of this group.',
        'request_registored' => 'Your request to join this group is registered.',
        'user_name' => 'Enter \'Name of the User\' or \'Email ID\' to Invite',//'Enter email id for user to invite',
        'invalid_and_out_campus_emails' => 'Of all the users you have invited, the following email IDs are either invalid or not from your campus! Valid email IDs are: xyz@msstate.edu. <br \>:out_campus_email <br \> :in_valid_email',
        'invalid_emails' => 'Emails you entered :in_valid_email are not valid.',
        'out_campus_emails' => 'Emails you entered :out_campus_email are not from your campus.',
        'invite_group_successful' => 'Your group invite has been sent successfully.',
        'about_group' => 'About group',
        'no_group_info_available' => 'No info available for this group.',
        'edit_group_image' => 'Edit group image',
        'group_image_change_success' => 'Group icon is changed successfully.',
        'upload' => 'Upload',
        'leave_group_confirmation' => 'Do you really want to leave this group?',
        'leave_group_success' => 'You have left the group.',
        'admin' => 'Admin',
        'member' => 'Member',
        'deactivated' => 'Deactivated',
        'member_count' => ':member_count members',
        'image_upload_successful' => 'Image uploaded successfully.',
        'change_icon' => 'Change Icon',
        'accept' => 'Accept',
        'decline' => 'Decline',
        'group_request_deleted' => 'This request has been deleted by admin.',
        'group_deleted' => 'This group no longer exists.',
        'download_csv' => 'Downlad csv',
    
        //Group creator leave group pop-up
        'creator_leave_group_title' => 'Leave group',
        'leave_group_info' => 'As a creator of this group, we recommend you not to leave. But still, if you would like to continue then please transfer your creator rights to a group member.',
        'select_from_options' => 'Please select one of the options below:',
        'choose_member' => 'Choose from existing group members',
        'random_member' => 'Choose any other user on Campusknot. (not recommended)',
        'choose_from_existing_user' => 'Choose user from below list.',
        'proceed' => 'Proceed',
        'search_group' => 'Search Group',

        //Group members page
        'members_page_title' => 'Group members',
        'remove' => 'Remove',
        'remove_user_confirmation' => 'Do you really want to remove :member from this group?',
        'search_group_member' =>'Search Group Member',

        //Group Events page
        'events_page_title' => 'Group events',
        'no_group_events' => 'No events have been created for this group as yet',
        'search_group_event' => 'Search Group Event',
        'search_group_document' => 'Search Group Document',
        
        //Group Photos page
        'upload_photo' => 'Upload Photo',
        'photos'=>'Photos',
    
        //Group poll
        'add_poll' => 'Add Poll',
    
        //Group invite page
        'all_request_status' => 'Invite request status',
        'status' => 'Status',
        'date' => 'Status updated',
        'invite_again' => 'Invite again',
        'request_pending' => 'Request Pending',
        'request_status_P' => 'Pending',
        'request_status_A' => 'Joined',
        'request_status_R' => 'Rejected',
        'request_status_' => '',
        'invite' => 'Invite',
        'group_request_rejected' => 'Group request declined successfully.',
        'invite_again_success' => 'Invitation sent again to :name.',
    
        //Group settings page
        'add_new_admin' => 'Add new Admin', //new
        'group_creator' => 'Creator', //new
        'enter_admin_name' => 'Enter Admin Name', //new
    
    
        //User Events page
        'events_page_title' => 'Events',
        'google_caledar' => 'Sync Google Calendar',
        'google_caledar_connect' =>'Connect',
        'google_caledar_disconnect' => 'Disconnect',
        'sync_calendar' => 'Google Calendar',
        'google_caledar_disconnect_msg' => 'Are you sure, you would like to disconnect from your Google Account?',
    
        
        //Post page
        'post' => 'Post',
        'edit' => 'Edit',
        'delete' => 'Delete',
        //'person_grade' =>'Person graded it',
        //'people_grade' => 'People graded it',
        'like' => 'A+ :like_count',
        'a'=> 'A',
        'plus' => '+',
        'non_member_like_alert' => 'You are not a member of this group. Please join the group to like the post.',
        'view' => 'View',
        'download' => 'Download',
        'add_comment' => 'Add a comment',
        'send' => 'Send',
        'post_placeholder' => 'Share with :name',
        'post_pollplaceholder' => 'Ask your query to :name',
        'post_img' => ' Share your photos ',
        'post_doc' => ' Share your documents ',
        'post_poll' => ' Share your pictorial query here ',
        'comments' => '<span id="count_:post">:comment_count</span><span> Comment(s)<span>',
        'post_delete_confirmation' => 'Are you sure you would like to delete this post?',
        'poll_delete_confirmation' => 'Are you sure you would like to delete this poll?',
        'day' => 'Day',
        'hour' => 'Hour',
        'minute' => 'Min',
        'second' => 'Sec',
        'post_deleted' => 'This post no longer exists.',
        'post_deleted_successfully' => 'This post deleted successfully.',//new
        'post_delete_successful' => 'Your post has been deleted successfully.',
        'user_graded it' => 'User graded it',//new
        'users_graded it' => 'Users graded it',//new
        'post_not_found' => 'This post is not found.',
        'not_allowed_to_delete_post' => 'you are not allowed to delete this post.',//new
        'not_allowed_to_delete_post_comment' => 'you are not allowed to delete this post comment.',//new
        'poll_deleted' => 'This poll no longer exists.',
        'poll_deleted_successfully' => 'This poll deleted successfully.',//new
        'poll_not_found' => 'This poll is not found.',
        'not_allowed_to_activate_poll' => 'You are not allowed to activate this poll.',
        'poll_activated_successful' => 'Poll activated successfully.',
        'not_allowed_to_delete_poll' => 'You are not allowed to delete this poll.',//new
        'poll_delete_successful' => 'Your poll has been deleted successfully.',
        'comment_not_found' => 'This comment is not found.',
        'not_allowed_to_delete_comment' => 'you are not allowed to delete this comment.',//new
        'comment_delete_successful' => 'Your comment has been deleted successfully.',
        'share_with_followers' => 'Share with Followers',//new
        'follow_faculty' => 'Follow Faculty',//new
        'report' => 'Report',//new
        'report_heading' => 'Report Post',//new
        'report_comment_heading' => 'Report Comment',//new
        'report_placeholder' => 'Type your reason for report...',//new
        'submit' => 'Submit',
        

        //User Profile Page
        'color_count'=>5,
        'change_avatar' => 'Change avatar',
        'unfollow' => 'Following',
        'follow' => 'Follow',
        'followers' => ' Followers',
        'following' => ' Following',
        'education' => 'Education',
        'work_experience' => 'Work Experience',
        'organization' => 'Organization',
        'awards_honors' => 'Awards & Honors',
        'aspirations' => 'Aspirations',
        'aspirations_placeholder' => 'What are your career aspirations?',
        'journal_article' => 'Journal Article',
        'research_work' => 'Research Work',
        'publication' => 'Publications',
        'date_of_attendance' => 'Dates Attended',
        'present' => 'Present',
        'done' => 'Done',
        'publication_date' => 'Publication Date',
        'research_date'=>'Research Date',
        'journal_date' =>'Journal Date',
        'user_profile_images' =>'User Profile Images',
        'time_of_winning'=>'Year of Achievement',
        'edit_profile_image'=>'Change Profile Image',
        'by' => 'By:',
        'change_profile_icon'=>'Change Profile Image',
        'upload_image'=>'Choose an image from computer',//new
        'upload_image_tagline'=>'Select a photo so your peers can recognize you arround',//new
        'change_group_icon'=>'Change Group Icon',
        'my_poll'=>'My Polls',
        'delete_image_confirmation' => 'Are you sure you would like to delete this image?',
        'user_image_change_success' => 'Your profile pic is changed successfully.',
        
        //User add education
        'degree' => 'Degree',
        'major' => 'Major',
        'classification' => 'Classification',
        'start_month' => 'Start month',
        'end_month' => 'End month',
        'year' => 'Year',
        'authentication_User_education_error' =>'You are not valid user to edit this education.',
        'default_education' => 'Set as default Major',
        'delete_education_success' => 'Education deleted Successfully',//new
        'default_education' => 'Set as default Major',
        'invalide_user_delete_item' =>'You are not the right person to delete this Information.',//new
        
        //Attendance
        'attendance' => 'Attendance',
        'attendance_taken' => 'Dear Faculty, You have already taken today\'s attendance. It cannot be re-taken today.',
        'attendance_not_allowed' => 'Only a faculty member can take an attendance.',
        'attendance_code_not_valid' => 'The attendance code does not match with the Course Code and Section. Please check your inputs and try again.',
        'group_attendance_code_not_valid' => 'The attendance code does not match with your selected group. Please check your inputs and try again.',
        'attendance_submitted' => 'Attendance submitted successfully.',
        'student_added_successfully' =>'Student added in attendance list successfully.',
        'student_already_exist' =>'Student already exists in attendance list.',
        'mark_as_absent' => 'Mark as Absent',
        'mark_as_paresent' => 'Mark as Present',
        'mark_as_sick_leave' => 'Mark as Excuse Absence',
        'attendace_log' => 'Attendance Log', //new
        'log' => 'Log',
        'sick_leave' => 'Excused Absence',
        'present' => 'Present',
        'absent' => 'Absent',
        'change_log_message' => 'Atendace change from <span class ="stu-:from">:from</span> to <span class ="stu-:to">:to</span>',
        'submit_attendance' =>'Attendance successfully submited.', //new
        'submit_attendance_error' =>'Code doese not belong to your course or section.', //new
        'lecture_date' =>'Lecture Date', //new
        'chnaged_date' =>'Changed Date', //new
        'action_performed' =>'Action Performed', //new
        'download_excel' =>'Download Excel', //new
        'search_student' =>'Search Sudent', //new
        'student_name' =>'Sudent Name', //new
        'add_student' =>'Add Sudent', //new
        'not_allowed_to_see_attendance' => 'You are not allowed to see this attendance.',
        'sick_leave_short' => 'EA',
        'present_short' => 'P',
        'absent_short' => 'A',
        'time' => 'Time',//new
    
        //User work experience
        'company_name' => 'Company name',
        'job_title' => 'Job title',
        'job_description' => 'Job description',
        'delete_workexp_success' => 'Work Experience deleted Successfully',//new
        'authentication_User_Work_error' =>'You are not valid user to edit this Work Experience.',//new
    
        //User organization
        'organization_name' => 'Organization name',
        'position_held' => 'Position held',
        'delete_organization_success' => 'Organization deleted Successfully',//new
        'authentication_User_organization_error' =>'You are not valid user to edit this organization.',//new
    
        //User awards
        'award_title' => 'Award title',
        'award_issuer' => 'Award issuer',
        'award_description' => 'Award description',
        'delete_award_success' => 'Award deleted Successfully',//new
        'authentication_User_award_error' =>'You are not valid user to edit this Awars.',//new
    
        //User Journal artical
        'journal_title' => 'Title',
        'article_url' => 'Article URL',
        'authors' => 'Name of Author(s)',
        'journal_description' => 'Description',
        'delete_journal_success' => 'Journal deleted Successfully',//new
        'authentication_User_journal_error' =>'You are not valid user to edit this Journal artical.',//new
    
        //User research
        'research_title' => 'Title',
        'research_work_url' => 'Research work URL',
        'research_authors' => 'Author(s)',
        'research_description' => 'Description',
        'delete_research_success' => 'Research deleted Successfully',//new
        'authentication_User_research_error' =>'You are not valid user to edit this Research Work.',//new
        
        //User publication
        'publication_title' => 'Title',
        'publication_url' => 'Publication URL',
        'publication_authors' => 'Author(s)',
        'publication_description' => 'Description',
        'delete_user_publication_success' => 'User Publication deleted Successfully',//new
        'authentication_User_publication_error' =>'You are not valid user to edit this Publication.',//new
    
        //Event Page
        'add_event' => 'Add Event',
        'edit_event_title' => 'Edit Event',
        //'creating_event' => 'Creating event',
        'invite_groups_or_users' => 'Invite groups or users',
        'cancel_event_title' => 'Cancel Event',
        'invite_to_event' => 'Event Invitation',
        'invite_members' => 'Invite Members',
        'event_detail' => 'Event Detail',
        'create_event'=>'Create Event',
        'description' => 'Description',
        'feeds' => 'Feeds',
        'going' => 'Going',
        'may_be' => 'Maybe',
        'not_going' => 'Not Going',
        'event_title' => 'Event title',
        'event_description' => 'Event description',
        'start_date' => 'Select start date',
        'end_date' => 'Select end date',
        'hh' => 'hh',
        'mm' => 'mm',
        'am' => 'AM',
        'pm' => 'PM',
        'add_end_date' =>'Add End Date',
        'add_address' => 'Add address',
        'add_detail_address' => 'Add detail address',
        'update' => 'Update',
        'create_event' => 'Create Event',
        'update_event' =>'Update Event',
        'colon' => ':',
        'event_request_not_found' => 'Sorry you have not been invited to this event as yet.',
        'event_response_registered' => 'Thank you. Your response has been registered.',
        'event_invite_member' => 'Your event request has been sent successfully.',
        'event_delete'=>'The event has been deleted successfully.',
        'no_events_found' => 'No Events have been created as yet.',
        'cancel_event_confirmation' => "Are you sure you would like to cancel this event?",
        'not_allow_to_delete_event' => 'You are not allowed to delete this event.',
        'invite_event_text'=> 'Invite members to ',
        'user_event_response_G' => 'I am going.',
        'user_event_response_M' => 'I am maybe.',
        'user_event_response_NG' => 'I am not going.',
        'i_am' => 'I am:',
        'event_deleted' => 'This event no longer exists.',
        'close' => 'Close.',
        'add_end_time' => '+ Add End Time',
        'authentication_event_error' =>'You are not creator of this event.You can\'t edit this event.',//new
        
        
        //notification 
        'notification' => 'Notifications',
        'group_requests' => 'Group request',
        'event_requests' => 'Event request',
        'others' => 'Others',
        'approve' => 'Approve',
        'dis_approve' => 'Disapprove',
        'refuse' => 'Refuse',
    
        
        //Group request notification texts
        'request_invite_user' => ':user has invited you to join :group group.',
        'request_join_user' => ':user wants to join :group group.',
        
        //Notification texts
        'user_follow' => ':user is now following you.',
        'comment_on_event' => ':user has commented on :event event.',
        'edit_event' => ':user has edited :event event.',
        'cancel_event' => ':user has canceled :event event.',
        'new_group_post' => ':user has created a new post in :group group.',
        'new_group_poll' => ':user has created a new poll in :group group.',
        'comment_on_post' => ':user has commented on ":post" post.',
        'like_on_post' => ':user has graded ":post" post.',
        'new_group_admin' => ':user has given you the admin rights of :group Group.',
        'new_group_document' => ':user has shared the document :document with the group :group.',
        'new_group_document_folder' => ':user has shared the folder :document with the group :group.',
        'new_user_document' => ':user has shared the document :document with you.',
        'new_user_document_folder' => ':user has shared the folder :document with you.',
        'user_attendance_change' => ':user has change your attendance of :course from :from to :to', //new
        'group_quiz' => ':name has created :quiz quiz in :group.', //new
        'group_quiz_delete' => ':name has deleted :quiz quiz from :group.', //new
        'scheduled_for_me' => 'Scheduled Quiz', //new
        'created_by_me' => 'My Quiz', //new
    
        //no data texts
        'no_group_photos' => 'Share any group photo, event photo or your art work.',
        'no_group_docs'  => 'Share your documents here.',
        'no_documents'  => 'Upload your assignments, presentations, spreadsheets and research papers! You may share it with your Professors and fellow students.',
        'no__group_events' => 'Create events for this group ',
        'no_events' => 'No Events have been created as yet.',
        'no_followers' => 'Update your profile and increase your followers. ',
        'no_following' => 'Follow your Faculty and Classmates. ',
        'no_admin_group' => 'Create your Interest Group and invite members.  ',
        'private_group_member' => "This group is 'Private'. Please click on 'Request to join' button or request the Admin to send you an invite.",
        'secret_group_member' => 'This group is secret. Please ask the admin to send you an invite. ',
        'file_preview_not_available' => 'File preview is not available.',
    
    
        //poll messages
        'view_poll_answers' => 'View Poll Answers',
        'poll_votes' => 'Poll votes' , 
        'users' => 'Users',
        'you' => 'You:',
        'votes'=> 'Votes',
        'vote'=> 'Vote',
        'no_user_poll' => 'No polls have been created yet',//new
        'poll_closed' =>'Time Up!',
        'poll_saved' => 'Saved poll!',
        'poll_time_left' =>'Time left :',
        'poll_start_on' =>'Start on :',
        'poll_duration' =>'Poll duration :',
        'add_more_poll_option' =>'+ Add more',
        'schedule_poll' =>'Schedule Poll',
        'select_date' =>'Select  Date',
        'save_as_draft' =>'Save as Draft',
        'publish' =>'Publish',
        'edit_poll' =>'Edit Poll',
        'copy_poll' =>'Copy Poll',
        'remove_image' =>'Remove Image',
        'no_poll_answer' =>'The poll has ended. You have not answered this question.',
        
        //Attendance messgaes
        'attendance' => 'Attendance',
        'from' => 'From',
        'to' => 'To',
        'select_subject' => 'Select Attendance',
        'section' => 'Section',
        'fall' => 'Fall',
        'winter' => 'Winter',
        'winter-1' => 'Winter-1',
        'winter-2' => 'Winter-2',
        'spring' => 'Spring',
        'summer' => 'Summer',
        'summer-1' => 'Summer-1',
        'summer-2' => 'Summer-2',
        'start' => 'Start',
        'end' => 'End',
        'semester' => 'Semester',
        'go' => 'Go',
        'students' => 'Students',
        'total' => 'Total',
        'remove_this_date_attendance' => 'Delete Lecture Attendance',
        'remove_attendance' => 'Are you sure you want to delete the record for this lecture?',
        'professor' => 'Professor:',
        'total_attendance' => 'Total Attendance:',
        'cancel' => 'Cancel',
        'attendance_code' => 'Attendance Code',
        'term' => 'Term',
        'attendance_present' => 'Present',
        'attendance_absent' => 'Absent',
        'submit_attendance' =>'Submit Attendance',
        'submit_attendance_success' =>'Submit Attendance Successfully',
        'add_attendance' =>'Take Attendance',
        'select_course' => 'Select course',
        'start_date' => 'Start Date',
        'end_date' => 'End Date',
        'code' => 'Code',
        'sec' => 'Sec',
        'success_change_attendace' => 'Student attendance change successfully.',
        'select_group' => 'Select Group', //new
        'no_group_attendance' => 'There is not any group attendance.', //new
        'no_user_attendance' => 'There is not any attendance.', //new
        'remove_attendance_text' => 'Remove Attendance', //new
        'no_lecture_for_group' => 'There is no more lecture for this group', //new
        
        //Books messages
        'books' => 'Books',
        'add_to_wishlist' => 'Add to wishlist',
        'sell_book' => 'Sell book',
        'seller' => 'Seller',
        'author' => 'Author',
        'isbn' => 'ISBN 10',
        'isbn13' => 'ISBN 13',
        'available' => 'Available',
        'sold_out' => 'Sold out',
        'isbn_number' => ' Search book by ISBN number',
        'book_title' => ' Book title',
        'book_author' => 'Book author',
        'book_price' => 'Book price ',
        'cancel' => 'Cancel ',
        'book_desc' => 'Book Description ',
        'wishlist' => 'Wishlist ',
        'my_books' => 'My Books ',
        'reply' => 'Reply ',
        'add_to_wishlist' => 'Add to Wishlist',
        'interested_buyers' => 'Interested buyers ',
        
        // Documents messages
        'document' => 'Documents',
        'my_docs' => 'My Docs',
        'group_docs' => 'Group Docs',
        'shared_with_me' => 'Shared with me',
        'date' => 'Date',
        'size' => 'Size',
        'share' => 'Share',
        'doc-owner' => 'Owner',
        'create_folder' => 'Create Folder',
        'upload_file' => 'Upload File',
        'share_folder' => 'Share',
        'copy_folder' => 'Copy :folder',
        'share_groups_or_users' => 'Share with other users or groups',
        'allow_user_to_add_folders' => 'Allow other users to add sub-folders',
        'share_doc_with_user' => 'Share with other users or groups',
        'my_documents' => 'My Documents',
        'shared_documents' => 'Shared Documents',
        'group_documents' => ':group Documents',
        'folder_delete_confirmation' => 'This folder may have sub-folders and files. Are you sure you would like to delete this folder?',
        'file_delete_confirmation' => 'Are you sure you would like to delete this file?',
        'share_with' => 'Shared With',
        'share_document_mail_title' => 'Document Share',
        'document_share_mail_body' => ":shared_by has shared document with :shared_with.",
        'document_share_subject' => 'Campusknot Document Share',
        'rename' => 'Rename',
        'download' => 'Download',
        'share' => 'Share',
        'delete' => 'Delete',
        'post-document-title' => 'Post-Documents',
        'document_shared_success' => 'Your document has been shared successfully.',
        'search_document' => 'Search Document',//new
        'copy_tooltip' => 'Copy ',//chnaged @misri
        'copy' => 'Copy',//new
        'shared_group_member' => 'Memebrs', //new
        'read_permission_text' => 'Read',//new
        'write_permission_text' => 'Write',//new
        'not_valid_copy_folder'=> 'You don\'t have Write permission in this Selected Folder.', //new
        'not_allowed_document_share'=> 'You don\'t have to permission to Share :document_name.', //new
        'copy_folder_success'=> 'Document copied successfully.', //new
        'view_doc_permission_error'=> 'You dont\'t have permission to View this document.' , //new
        'download_doc_permission_error'=> 'You dont\'t have permission to download this document.', //new
        'no_document'=> 'There is no document found.', //new
        'authentication_document_error'=> 'You are not creator of this Docment.', //new
        'revoke_document_access'=> 'Are you sure you want to revoke the permission?', //new
        'success_revoke_permission'=> 'Permission revoked successfully', //new
        'no_write_permission' => 'You don\'t have write permission in selected folder to copy other documents.',
        'not_allowed_to_copy' => 'You are only alloed to copy the documents which are created by you.',
        
          
        //quiz
        'quiz' => 'Quiz',
        'add_quiz' => 'Add Quiz',
        'quiz_detail' => 'Quiz Detail',
        'question' =>'Question',
        'multi_choise' =>'Multiple choice',
        'open_ended' =>'Open Ended',
        'correct_answer' =>'Correct answer',
        'add_option' =>'Add option',
        'add_question' =>'Add new question',
        'delete_question_text_confirm' =>'Are you sure you want to delete this question?',
        'delete_quiz_text_confirm' =>'Are you sure you want to delete this quiz?',
        'upcoming' =>'Upcoming',
        'completed' =>'Completed',
        'duration' =>'Duration',
        'quiz_duration' =>'Quiz Duration',
        'quiz_points' =>'Quiz Points',
        'quiz_submitted_by' =>'Quiz submitted by',
        'start_date_quiz' =>'Start Date',
        'end_date_quiz' =>'End Date',
        'quiz_name' =>'Quiz Name',
        'points' =>'Points',
        'quiz_information' =>'Quiz Information',
        'add_question' =>'Add Question',
        'mins' =>'mins',
        'open_quiz' =>'Open Quiz',
        'start_quiz' =>'Start Quiz',
        'end_quiz' =>'End Quiz',
        'time_left' =>'Time Left',
        'time_taken' =>'Time Taken',
        'type_your_answer' =>'Type your answer..',
        'quiz_access_denied' => 'You have no rights to edit question in this quiz.',
        'question_type_openended' => 'This is Open ended question.',
        'not_attempted' => 'Not Attempted',
        'student_name' => 'Students Name',
        'marks_obtained' => 'Marks obtained',
        'allot_marks' =>'Alloted Marks',
        'started' =>'Started',
        'allot_marks_for_open_ended_question' =>'You need to add marks for open ended questions', //faculty side display
        'marks_remaining_for_open_ended_question' =>'Marks Add remaining for open ended questions', //student side diplay
        'alloted_marks_for_quiz' =>'Alloted marks for quiz is',
        'your_quiz_end' =>'Your quiz will end at: ',
        'saved' =>'Saved',
        'users' =>'Users',
        'quiz_access_denied' =>'You have No access for the quiz.',
        'confirm_end_quiz' =>'sure!! You want to End this quiz ?.',
        'marks' =>':mark Marks.',
        'status' => 'Status',
        'or' => 'or',
        'no_upcoming_quiz' => 'Quiz data does not exist as yet.',
        'no_completed_quiz' => 'There is no any completed quiz.',
        'show_in_mobile' => 'Quiz Open For mobile view',
        'quiz_end_after' => 'Quiz End After',
        'schedule_quiz' => 'Schedule Quiz',
    
    
        //error page 
        'page_not_found' => 'Page not found',
        'go_back_to_home_page' => 'Go back to home page',
        'no_access' => 'You have no access rights for this page',
    
        //image alt text
        'campusknot_logo' => 'Campusknot logo',
        'facebook_link' => 'Facebook link',
        'twitter_link' => 'Twitter link',
        'googleplus_link' => 'Google plus link',
    
        //admin messages 
        'username' => 'Username',
        'password' => 'Password',
        'select_campus' => 'Select campus',
        'select' => 'Select',
        'add_extension' => 'Add extension',
        'extension_list' => 'Extension list',
        'verify_user' => 'Verify user',
        'campus_list' => 'Campus list',
        'add_campus' => 'Add campus',  
        'campus_name' => 'Campus name',  
        'campus_url' => 'Campus URL', 
        'select_timezone' => 'Select timezone', 
        'add_university' => 'Add university', 
        'university_name' => 'University name',  
        'university_url' => 'University URL',
        'verified' => 'Verified',
        'pending' => 'Pending',
        'extensions' => 'Extensions',
        'logs' => 'Logs',
        'change_campus' => 'Change campus',
        'search_users' => 'Search user',
        'add_extension' => 'Add extension',
        'search_extension' => 'Search extension',
        'add_new_university' => 'Add new university(+)',
        'campus_details' => 'Campus details',
        'india' => 'India',
        'usa' => 'United States of America',
        'select_country' => 'Select country',
        'india' => 'India',
        'welcome_admin' => 'Campusknot admin',
        'admin_details' => 'Admin details',
        'country' => 'Coutry',
        'sr_no' => 'Sr.No.',
        'extension' => 'Extension',
        'search_campus' => 'Search campus',
        'campuses' => 'Campuses',
        'actions' => 'Actions',
        'info' => 'Info!',
        'extension_info' => 'Following extension allows user to select campus',
        'date_modified' => 'Date modified',
        'edit_user' => 'Edit User',
        'url_placeholder' => 'http(s)://example.com',
        'invalid_extension_message' => 'The campus extension format is invalid.',
        'extension_updated_successfully' => 'Extension Updated Successfully',
        'password_changed_successfully' => 'Password Changed successfully',
        'confirm_new_password' => 'Confirm New Password',
        'add_user' => 'Add user',
        'capital_or' => 'OR',
        'choose_csv_file' => 'Choose CSV file...',
        'add_as_student' => 'Add as student',
        'add_as_faculty' => 'Add as faculty',
        'add_student' => 'Add Student',
        'add_faculty' => 'Add Faculty',
        'add_as_verified' => 'Add as verified user',
        'add_campus_admin' => 'Add Campus Admin',
        'add_single_student' => 'Add single student',
        'add_multiple_students' => 'Add multiple students',
        'add_multiple_faculties' => 'Add multiple faculties',
        'add_single_faculty' => 'Add single faculty',
        'user_type' => 'User Type',
        'back' => 'Back',
        'create_campus_admin' => 'Admin',
        
        'verified_user_success' => 'User has been verified',
        'verification_mail_success' => 'Verification link has been sent',
        'campus_admin_create_success' => 'Campus Admin Created Successfully',
        'faculty_added_success' => 'Faculty Added Successfully',
        'student_added_success' => 'Student Added Successfully',
        'delete_extension_success' => 'Extension Deleted',
        

        

       
        //google analytics js for live nd staging
        'ga_script_production' =>"<script>
                                    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                                    ga('create', 'UA-99225053-1', 'auto');
                                    ga('send', 'pageview');

                                  </script>",
        'ga_script_local'=>"<script>
                                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                                ga('create', 'UA-91094631-1', 'auto');
                                ga('send', 'pageview');

                            </script>"
    ];
