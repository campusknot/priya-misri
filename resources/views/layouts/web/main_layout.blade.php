<!DOCTYPE html>
<html lang="en">
    <head>
        <!--[if IE]>
        <meta HTTP-EQUIV="REFRESH" content="0; url=http://www.google.com">
        <![endif]-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        <meta http-equiv="cache-control" content="private, max-age=0, no-cache">
        <meta http-equiv="pragma" content="no-cache">
        <meta http-equiv="expires" content="0">
        <title>@yield('title')</title>
        @section('includes')
            @include('layouts.web.includes')
        @show
    </head>

    <body>
        <div class="app-wrapper"></div>
<div id="wrapper" class="ck-app" >
            <div id="temp_message" class="temp-message">
                <span id="flash_msg">
                    @if (session('success_message'))
                        {{ session('success_message') }}
                    @endif 
                </span>
            </div>
            <header class="min-width">
                @section('header')
                    @include('layouts.web.main_header')
                @show
            
            </header>
            <div class="container-fluid">
                <div class="row add-top-margin">
                    <div class="attendance">
                        <div class="submit_atten hidden" >
                            @include('layouts.web._submit_attendace')
                        </div>
                        <div class="take_atten hidden">
                            @include('layouts.web._take_attendace')                   
                        </div>
                    </div>
                    <div class="attendance-bg-black"></div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 no-padding">
                        <div class="">
                            @include('layouts.web._navigationbar')
                        </div>
                        <div class="">
                            @yield('left_sidebar')
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 padding-10">
                        
                        @yield('content')
                    </div>
                    <div id="right_sidebar" class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        @yield('right_sidebar')
                    </div>
                    @include('layouts.web.contact_form')
                </div>
            </div>
            <footer>
                <script type="text/javascript">
                    @yield('footer-scripts')                    
                </script>
                @yield('footer_sub')
            </footer>
        </div>
        
        <script type="text/javascript">
            var bgImage = "url('" + $("#campus_image_url").val() + "')";
            $(".app-wrapper").css("background",bgImage);
        </script>
    </body>
</html>