<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                <span >
                    <img class="logo sml-logo" src="{{asset('assets/web/img/main_header_logo.png')}}">
                </span>
            </div>
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                <!-- user profile Action-->
                <div class="display-ib pos-relative pull-right">
                    <span  href="javascript:void(0);" class="profile-pic pull-right ddlink sml-profile-pic">
                        {!! setProfileImage("100",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                        <span class=""><p>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</p></span>
                    </span>
                </div>
                
               
                
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.icon').click(function () {
        $('.header-global-search').toggleClass('view-header-search');
    });
    $(function()
    {
        $('#globalSearch').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('user.global-search-ajax') }}"+"/" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.first_name+' '+value.last_name,
                            value: value.id_user,
                            image: value.profile_img,
                            major: value.major
                        };

                    }));
                });
            },
            minLength: 2,
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                $('#globalSearch').val(  ui.item.label );
            },
            select: function(event, ui) {
                window.location = siteUrl +"/user/profile/"+ ui.item.value;
               
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span class='icon-image img-35'>"+item.image +"</span><a class='header-search-proile-link' href='"+ siteUrl +"/user/profile/"+ item.value +"'  > <span>" + item.label + "</span><span class='major'>"+item.major +"</span></a>" )
                .appendTo( ul );
            };
    });
</script>