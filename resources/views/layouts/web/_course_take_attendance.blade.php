<form id="add-course-from" action="{{ route('group.add-attendance') }}" method="post">
    {!! csrf_field() !!}
    <div id="errors-attendance-add-course" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error_message"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group ">
        
        <input type="text" id="subject_name" data-toggle="tooltip" data-placement="top" class="form-control" name="group_name" placeholder="{{trans('messages.select_course')}}" value="{{ old('course_name') }}" required="required">
        <input id="course_id" name='id_group' type="text" class="hidden" />
        <input id="semester" name="semester" type="text" class="hidden" />
        <input id="id_lecture" type="text" name="id_lecture" value="" class="hidden" />
        <input id="course_attendance" type="text" name="course_attendance" value="1" class="hidden" />
        <input id="end" type="text" name="end" value="0" class="hidden" />
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 attendance-info">
        <div id="course-name"></div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <button type="button"  class="btn btn-primary btn-attendance-start add-course-atten" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.start') }}</button>
        <button type="button"  class="btn btn-primary btn-attendance-end hidden" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.end') }}</button>
        <span onclick="showAttendance(0);" class="close-attendance">{{ trans('messages.cancel') }}</span>
    </div>
</form>       
<script type="text/javascript">
    var TimeSeccounterCourse;
    function timer()
    {
        var time=$("#AttenTimer").text();
        if(time == 1)
        {
            $('.add-course-atten').trigger( "click" );
            $('#random').removeClass( "text-red" );
            $('#AttenTimer').removeClass( "text-red" );
            $("#AttenTimer").text(25);
            clearInterval(TimeSeccounterCourse);
        }
        else if(time == 5)
        { 
            $('#random').addClass( "text-red" );
            $('#AttenTimer').addClass( "text-red" );
            $("#AttenTimer").text(time-1);
        }
        else 
            $("#AttenTimer").text(time-1);
    }
    $(document).ready(function($) 
    {
        var optionsAtten = { 
            beforeSubmit:  showRequestAtten,
            success:       showResponseAtten,
            error:showErrorAtten,
            dataType: 'json' 
            }; 
        $('.add-course-atten').click(function(e){
            e.preventDefault();
            $(this).button('loading');
            $('#add-course-from').ajaxForm(optionsAtten).submit();  		
        });
        $('.btn-attendance-end').click(function(e){
                $(this).button('loading');
                $('#end').val(1);
                $("#AttenTimer").text(25);
                clearInterval(TimeSeccounterCourse);
                $('#add-course-from').ajaxForm(optionsAtten).submit();  		
            });
    });
    function showRequestAtten(formData, jqForm, optionsAtten) 
    { 
        $('#sec-id').text($("input[name='section']").val());
        $('#semester-val').text($(".course_sem option:selected").text());
        $("#errors-attendance-add-course").hide().empty();
        $("#output").css('display','none');        
        return true; 
    } 
    function showResponseAtten(response, statusText, xhr, $form)  
    {
        if(response.success == true && response.actionType == 'start')
        {
            $(".attendance-mid-section").addClass('disabled-field');
            $('.add-course-atten').button('reset');
            $('.add-course-atten').addClass('hidden');
            $('.btn-attendance-end').removeClass('hidden');
            $('#id_lecture').val(response.nIdGroupLecture);
            $('.attendance_type').addClass('hidden');
            $('#attendance_content').addClass('attendance_content');
            $('.random').css("display","block");
            $('#random').text(response.random);
            $('#start_attendance_flag').val(1);
            TimeSeccounterCourse=setInterval(timer, 1000);
        }
        else if(response.success == true && response.actionType == 'end')
        {
            $(".attendance-mid-section").removeClass('disabled-field');
            $('.btn-attendance-end').button('reset');
            $('.add-course-atten').show();
            $('.random').css("display","none");
            $('#random').text('');
            $('#start_attendance_flag').val(0);
            $('.btn-attendance-end').addClass('hidden');
            $('.add-course-atten').removeClass('hidden');
            clearInterval(TimeSeccounterCourse);
            $("#AttenTimer").text(25);
            $('#add-course-from')[0].reset();
            showAttendance(0);
            window.location = siteUrl + '/group/course-attendance';
        }
        else if(response.success == false)
        {
            $('.btn-attendance-end').button('reset');
            $('.add-course-atten').button('reset');
            $("#errors-attendance-add-course").append('<strong>'+response.msg +'</strong>');
            $("#errors-attendance-add-course").show();
            $("input[name='attendance_code']").val('');
        }
    }
    function showErrorAtten(xhr, textStatus, errorThrown)  
    {
        $('.add-course-atten').button('reset');
        $('.btn-attendance-end').button('reset');
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                $("#errors-attendance-add-course").append('<strong>'+ value +'</strong>');
                $(".attendance-mid-section input[name='"+index+"']").addClass('error');
            }
        });
        $("#errors-attendance-add-course").show();
    }

    $(function()
    {
        $('#subject_name').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('group.group-suggestion') }}"+"?group_name=" + request.term+"&search_for=admin&group_cat={{ config('constants.COURSEGROUPCATEGORY') }}", function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: htmlSpecialChars(value.group_name),
                            value: value.id_group,
                            image: value.group_img,
                            section: value.section,
                            semester: value.semester,
                        };

                    }));
                });
            },
            minLength: 0,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                event.preventDefault();
                $("input#subject_name").val(ui.item.label);
                $("#course-name").text(ui.item.label+' section-'+ui.item.section+' '+ui.item.semester);
                $("input#course_id").val(ui.item.value);
                $("input#semester").val(ui.item.semester);
            },
            select: function(event, ui) {
                event.preventDefault();
                event.stopPropagation();
                $("input#subject_name").val(ui.item.label);
                $("#course-name").text(ui.item.label);
                $("input#course_id").val(ui.item.value);
                $("input#semester").val(ui.item.semester);
                
                return false;
            }
        }).focus(function() {
            $(this).autocomplete("search", "");
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span class='icon-image img-35 mr-10'>"+ item.image+"</span><span>"+item.label+'</span>' )
                .appendTo( ul );
            };
    });
    
    
    
</script>