<!DOCTYPE html>
<!-- login layout-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" id="token" content="{{ csrf_token() }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        
        <title>@yield('title')</title>
        @section('includes')
            @include('layouts.web.includes')
        @show
    </head>
    <body>
	<div id="ckLandingPage">
		
	
        <div class="" id="wrapper">
             <div id="temp_message" class="temp-message">
                <span id="flash_msg">
                    @if (session('success_message'))
                        {{ session('success_message') }}
                    @endif 
                </span>
            </div>
            <header class="header-container">
                @section('header')
                    @include('layouts.web.header')
                @show
            </header>

            <div class="clearfix">
                @yield('content')
            </div>

            <footer class="footer-container">
                @section('footer')
                    @include('layouts.web.footer')
                @show
            </footer>
        </div>
    <script src="{{env('VUE_URL')}}/js/app.js"></script>
    </div>
    </body>
</html>