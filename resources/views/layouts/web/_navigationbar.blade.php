<?php $sCurrentPage = Session::get('current_page'); ?>
<ul class="navigation">
    <li class="{{ ($sCurrentPage=='home') ? 'active-feeds':''}} ">
        <a href="{{ route('/') }}">
            @if($sCurrentPage=='home')
                <img class="" src="{{asset('assets/web/img/active-feeds.png')}}">
                <span class="active-feeds">{{trans('messages.home')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-feeds.png')}}">
                <span class="">{{trans('messages.home')}}</span>
            @endif
        </a>
    </li>
    
    <li class="{{ ($sCurrentPage=='groups') ? 'active-groups' :'' }}">
        <a href="{{ route('group.recommended-group-listing') }}" >
            @if($sCurrentPage=='groups')
                <img class="" src="{{asset('assets/web/img/active-groups.png')}}">
                <span class="active-groups">{{trans('messages.groups')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-groups.png')}}">
                <span class="">{{trans('messages.groups')}}</span>
            @endif
        </a>
    </li>
    
    <li class="{{ ($sCurrentPage=='events') ? 'active-planner' :'' }}">
        <a href="{{ route('event.event-listing') }}">
             @if($sCurrentPage=='events')
                <img class="" src="{{asset('assets/web/img/active-planner.png')}}">
                <span class="active-planner">{{trans('messages.events')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-planner.png')}}">
                <span class="">{{trans('messages.events')}}</span>
            @endif
        </a>
    </li>
    <!-- temp code for MSU, Baroda remove it -->
    <li class="{{ ($sCurrentPage=='document') ? 'active-documents' :'' }}">
        <a href="{{ route('documents.document-listing') }}">
            @if($sCurrentPage=='document')
                <img class="" src="{{asset('assets/web/img/active-documents.png')}}">
                <span class="active-documents">{{trans('messages.document')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-documents.png')}}">
                <span class="">{{trans('messages.document')}}</span>
            @endif
        </a> 
    </li>
    
<!--    <li>
        <a href="#">
            <img src="{{asset('assets/web/img/assignment.png')}}">
            @if(!Cookie::get('assignments') && !Session::get('assignments'))
            <span class="new-feature-label">{{ trans('messages.new') }}</span>
            @endif
            @if($sCurrentPage=='assignment')<span class="caret"></span>@endif
            Assignments
        </a>
    </li>-->
    <!-- Remove above menu items -->
    <li class="{{ ($sCurrentPage=='attendance') ? 'active-attendance' :'' }}">
        <a href="{{ route('group.course-attendance') }}">
            @if($sCurrentPage=='attendance')
                <img class="" src="{{asset('assets/web/img/active-attendance.png')}}">
                <span class="active-attendance">{{trans('messages.attendance')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-attendance.png')}}">
                <span class="">{{trans('messages.attendance')}}</span>
            @endif
            
            @if(!Cookie::get('attendance') && !Session::get('attendance'))
            <span class="new-feature-label">{{ trans('messages.new') }}</span>
            @endif
            
        </a>
    </li>
    
    <li class="{{ ($sCurrentPage=='poll') ? 'active-polls' :'' }}">
        <a href="{{ route('user.all-poll') }}">
            @if($sCurrentPage=='poll')
                <img class="" src="{{asset('assets/web/img/active-polls.png')}}">
                <span class="active-polls">{{trans('messages.polls')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-polls.png')}}">
                <span class="">{{trans('messages.polls')}}</span>
            @endif
            
        </a>
    </li>
    
    <li class="{{ ($sCurrentPage=='quiz') ? 'active-quiz' :'' }}">
        <a href="{{ (Auth::user()->user_type == config('constants.USERTYPEFACULTY')) ? route('quiz.quiz-faculty-list') : route('quiz.quiz-student-list') }}">
            @if($sCurrentPage=='quiz')
                <img class="" src="{{asset('assets/web/img/active-quiz.png')}}">
                <span class="active-quiz">{{trans('messages.quiz')}}</span>
            @else
                <img class="" src="{{asset('assets/web/img/inactive-quiz.png')}}">
                <span class="">{{trans('messages.quiz')}}</span>
            @endif
            
            @if(!Cookie::get('quiz') && !Session::get('quiz'))
                <span class="new-feature-label">{{ trans('messages.new') }}</span>
            @endif
        </a>
    </li>
    
</ul>