<!DOCTYPE html>
<html lang="en">
    <head>
        <!--[if IE]>
        <meta HTTP-EQUIV="REFRESH" content="0; url=http://www.google.com">
        <![endif]-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        
        <title>@yield('title')</title>
        @section('includes')
            @include('layouts.web.includes')
        @show
    </head>
    <body>
        <div id="wrapper" class="ck-submit-quiz" >
          <header class="min-width quiz-tab fixed">
                @section('header')
                    @include('layouts.web.quiz_submit_header')
                @show
                <div class="attendance-bg-black"></div>
            </header>
            <div class="container-fluid">
                <div class="row">
                    <div class="">
                        @yield('content')
                    </div>
                    
                </div>
            </div>
            <footer>
                <script type="text/javascript">
                    @yield('footer-scripts')                    
                </script>
                @yield('footer_sub')
            </footer>
        </div>
    </body>
</html>