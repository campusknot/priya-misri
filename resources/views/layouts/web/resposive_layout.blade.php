<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>@yield('title')</title>
        @section('includes')
            @include('layouts.web.includes')
        @show
    </head>
    <body>
        <div class="" id="">
             <div id="temp_message" class="temp-message">
                <span id="flash_msg">
                    @if (session('success_message'))
                        {{ session('success_message') }}
                    @endif 
                </span>
            </div>
            <header class="header-container">
                @section('header')
                    @include('layouts.web.header')
                @show
            </header>

            <div class="clearfix">
                @yield('content')
            </div>

            <footer class="footer-container">
                @section('footer')
                    @include('layouts.web.footer')
                @show
            </footer>
        </div>
    </body>
</html>