<div class="help-desk">
    
    <div class="contact-from-container clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h5 class="open-form">{{ trans('messages.contact_us') }}</h5>
            <form id="contact" name="contact" method="post" role="form" action="{{ route('home.contact') }}" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  field-padding-right hidden">
                    <input id="form_name" type="hidden" name="name" class="form-control" value="{{ Auth::user()->first_name }}">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding" hidden="">
                    <input id="form_lastname" type="hidden" name="surname" value="{{ Auth::user()->last_name }}">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <input id="form_email" type="hidden" name="email" value="{{ Auth::user()->email }}">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group pos-relative">
                    <span class="caret caret-right"></span>
                    <select class="form-control" name="contact_type">
                        <option value="{{ trans('messages.feedback') }}">{{ trans('messages.feedback') }}</option>
                        <option value="{{ trans('messages.technical_support') }}">{{ trans('messages.technical_support') }}</option>

                    </select>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group">
                    <input id="form_phone" type="tel" name="phone" class="form-control {{ ($errors->has('phone'))? 'error' : '' }}" title="{{ ($errors->has('phone'))? $errors->first('phone') : '' }}"  data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.enter_phone_number') }}" value="{{ old('phone') }}">
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group">
                    <textarea id="form_message" name="message" class="form-control {{ ($errors->has('message'))? 'error' : '' }}" title="{{ ($errors->has('message'))? $errors->first('message') : '' }}"  data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.message_placeholder') }} *" rows="4" required="required" data-error="Please,leave us a message." >{{ old('message') }}</textarea>
                    <div class="help-block with-errors"></div>
                </div>
<!--                     <div class="input-group form-group" id="file-input">
                    <label class="input-group-btn file-attachment-btn">
                        <span class="btn btn-primary">
                            {{ trans('messages.browse') }} <input type="file" name="img_file">
                        </span>
                    </label>
                    <input type="text" class=" file-attachment-name" readonly="">
                    <span class="file-attachment-cancel" onclick="" data-id="">{{ trans('messages.cancel') }}</span>
                </div> -->
                <button class="btn btn-primary help-submit-btn contact-btn" id="load" data-loading-text="<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.send_message') }}</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*open side contact form*/
     $('.open-form').on('click', function(evt) {
            evt.stopImmediatePropagation();
            $(".help-desk").toggleClass("open-contact-form");
            
       });
    var optionsContact = { 
        beforeSubmit:  showRequestContactForm,
        success:       showResponseContactForm,
        error:showErrorContactForm,
        dataType: 'json' 
        }; 
    $('.contact-btn').click(function(e){
        e.preventDefault();
        $(this).button('loading');
        $('#contact').ajaxForm(optionsContact).submit();  		
    });
    function showRequestContactForm(formData, jqForm, options) { 
            //$("#validation-errors").hide().empty();
            //$("#output").css('display','none');
            return true; 
    } 
    function showResponseContactForm(response, statusText, xhr, $form)  
    {
        $('.contact-btn').button('reset');
        $('#contact')[0].reset();
        $('#flash_msg').text(response.html);
        showTemporaryMessage();
    }
    function showErrorContactForm(xhr, textStatus, errorThrown)  {
        if(xhr.status == 500 ){
            //window.location = siteUrl + '/home';
            console.log('ewrror');
        }
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                    $( "[name='"+index+"']" ).addClass('error');
                    $( "[name='"+index+"']" ).attr('title',value);
                    //$("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
            }
        });
        $("#validation-errors").show();
        $('.contact-btn').button('reset');
    }
</script>