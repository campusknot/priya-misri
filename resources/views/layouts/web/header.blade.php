
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-1 col-md-1"></div>
        <div class="col-lg-5 col-md-4 col-sm-4 col-xs-6">
            <a href="<?php echo url('/'); ?>" rel="home" >
                <img src="{{asset('assets/web/img/main_header_logo.png')}}" class="logo lg-logo" alt="Logo" />
            </a>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-8 col-xs-6">
             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            <div class="pull-right top-nav navbar-collapse collapse" id="menu">
                <a href="{{ url('/about') }}" class="navbar-brand ">{{ trans('messages.about') }}</a>
                <a href="http://books.campusknot.com/"  target="_blank" class="navbar-brand ">{{ trans('messages.books') }}</a>
                <a href="http://blog.campusknot.com/"  class="navbar-brand ">{{ trans('messages.blog') }}</a>
                <a href="{{ url('/mobile-app') }}"  class="navbar-brand ">{{ trans('messages.mobile_app') }}</a>
                <!--<a href="{{ url('/contact') }}" rel="contact" class="navbar-brand ">{{ trans('messages.contact') }}</a>-->
                <a href="{{ url('/login') }}" class="navbar-brand btn-border">{{ trans('messages.login') }}</a>
            </div>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
    </div>
</div>