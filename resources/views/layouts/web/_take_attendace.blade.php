<div class="faculty">
    <div class="clearfix">
        <div class="attendance-add-course">
            <div class="col-lg-3 col-md-2 col-xs-2 col-sm-2"></div>
            <div class="col-lg-6 col-md-8 col-xs-8 col-sm-8 clearfix attendance-mid-section">
                <h3>{{ trans('messages.add_attendance') }}</h3>
                <div class="attendance-code col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
                    <div class="random">
                        <div class="attendance-block">
                            <div class="attendance-change-code" onselectstart="return false;">
                                <label id="random"></label>
                                <span class="attendance-code-label" >{{ trans('messages.code') }}</span>
                            </div>
                            <span class="attendance-time">
                                <span id="AttenTimer">25</span>
                                <span class="attendance-time-label">{{ trans('messages.sec') }}</span>
                            </span>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="start_attendance_flag" value="0" />
                <ul class="nav nav-tabs attendance_type">
                    <li class="active"><a data-toggle="tab" href="#course123">Attendance for Course</a></li>
                    <li><a data-toggle="tab" href="#group123">Attendance for Group</a></li>
                </ul>
                <div class="tab-content clearfix" id="attendance_content">
                <div id="course123" class="tab-pane fade in active">
                   @include('layouts.web._course_take_attendance')
                </div>    
                 <div id="group123" class="tab-pane fade">
                        <form id="group-attendance" action="{{ route('group.add-attendance') }}" method="post">
                            {!! csrf_field() !!}
                            <div id="errors-group-attendance" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error_message"></div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group attendance-group-name pos-relative">
                                <span class="caret caret-right"></span>
                                <input type="text" id="group_name_take" data-toggle="tooltip" data-placement="top" class="form-control" name="group_name" placeholder="{{trans('messages.search_group')}}" value="{{ old('group_name') }}" required="required">
                                <input id="id_group_take" name='id_group' type="text" class="hidden" />
                                <input id="id_group_lecture" type="text" name="id_lecture" value="" class="hidden" />
                                <input id="end_take" type="text" name="end" value="0" class="hidden" />
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pos-relative form-group text-left ">
                                <div class="faculty-attendance-section ">
                                    <span class="group-attendance-temp">
                                        <select required class="form-control group_sem" name="semester" onchange="getOption(this,'group_sem');">
                                        <option disabled="disabled" selected="selected" value="" class="display-ib">Semester</option>
                                        <option value="fall">{{ trans('messages.fall') }}</option>
                                        <option value="winter">{{ trans('messages.winter') }}</option>
                                        <option value="winter-1">{{ trans('messages.winter-1') }}</option>
                                        <option value="winter-2">{{ trans('messages.winter-2') }}</option>
                                        <option value="spring">{{ trans('messages.spring') }}</option>
                                        <option value="summer">{{ trans('messages.summer') }}</option>
                                        <option value="summer-1">{{ trans('messages.summer-1') }}</option>
                                        <option value="summer-2">{{ trans('messages.summer-2') }}</option>
                                    </select>
                                    <span class="caret"></span>
                                    </span>
                                     
                                    <div class="add-year hidden display-ib" id="group_sem">
                                        <div class="attendance-semester-year display-ib">
                                            <select class="form-control" name="start_year">
                                                <option disabled="disabled" selected="selected" value=""> Start Year</option>
                                                <?php 
                                                $dNow = Carbon::now();
                                                ?>
                                                @if($dNow->month < 3)
                                                <option value="{{ $dNow->year-1}}-{{$dNow->year }}">{{ $dNow->year-1}}-{{$dNow->year }}</option>
                                                @endif
                                                <option value="{{ $dNow->year }}">{{ $dNow->year }}</option>
                                                @if($dNow->month >= 10 )
                                                 <option value="{{ $dNow->year}}-{{$dNow->year+1 }}">{{ $dNow->year}}-{{$dNow->year+1 }}</option>
                                                @endif
                                             </select>
                                            <span class="caret"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 attendance-info">
                                <div id="group-name"></div>
                                <span class="faculty-att-term">{{ trans('messages.term') }} - <span id="group-semester"></span></span>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <button type="button"  class="btn btn-primary group-attendance-start" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.start') }}</button>
                                <button type="button"  class="btn btn-primary group-attendance-end hidden" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.end') }}</button>
                                <span onclick="showAttendance(0);" class="close-attendance">{{ trans('messages.cancel') }}</span>
                            </div>
                        </form>
                    </div>
                  </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
   // var myEvent = window.attachEvent || window.addEventListener;
    //var chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload'; /// make IE7, IE8 compatable

//    myEvent(chkevent, function(e) { // For >=IE7, Chrome, Firefox
//        debugger;
//        var confirmationMessage = ' ';  // a space
//        if($('#start_attendance_flag').val() == 1)
//        {
//            //alert('asdasdasdasd');
//            (e || window.event).returnValue = confirmationMessage;
//            return confirmationMessage;
//        }
//    });
//$(window).bind('beforeunload', function(){
//    debugger;
//     return "You're leaving?";
// });

    var TimeSeccounter;
    function groupTimer()
    {
        console.log('timergroup'+time);
        var time=$("#AttenTimer").text();
        if(time == 1)
        { 
            //console.log('group');
            $('.group-attendance-start').trigger( "click" );
            $('#random').removeClass( "text-red" );
            $('#AttenTimer').removeClass( "text-red" );
            $("#AttenTimer").text(25);
            clearInterval(TimeSeccounter);
        }
        else if(time == 5)
        { 
            $('#random').addClass( "text-red" );
            $('#AttenTimer').addClass( "text-red" );
            $("#AttenTimer").text(time-1);
        }
        else 
            $("#AttenTimer").text(time-1);
    }
    
    $(function()
    {
        $('#group_name_take').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('group.group-suggestion') }}"+"?group_name=" + request.term+"&search_for=admin", function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: htmlSpecialChars(value.group_name),
                            value: value.id_group,
                            image: value.group_img,
                        };

                    }));
                });
            },
            minLength: 0,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                $("input#group_name_take").val(ui.item.label);
                $("#group-name").text(ui.item.label);
                $("input#id_group_take").val(ui.item.value);
                event.preventDefault();
            },
            select: function(event, ui) {
                event.stopPropagation();
                $("input#group_name_take").val(ui.item.label);
                $("#group-name").text(ui.item.label);
                $("input#id_group_take").val(ui.item.value);
                event.preventDefault();
                return false;
            }
        }).focus(function() {
            $(this).autocomplete("search", "");
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span class='icon-image img-35 mr-10'>"+ item.image+"</span><span>" +item.label + "</span>" )
                .appendTo( ul );
            };
    });
    
    $(document).ready(function($) 
    {
        var optionsAtten = { 
            beforeSubmit:  showRequestGroupAtten,
            success:       showResponseGroupAtten,
            error:showErrorGroupAtten,
            dataType: 'json' 
            }; 
        $('.group-attendance-start').click(function(e){
            e.preventDefault();
            $(this).button('loading');
            $('#group-attendance').ajaxForm(optionsAtten).submit();  		
        });
        $('.group-attendance-end').click(function(e){
                $(this).button('loading');
                $('#end_take').val(1);
                $("#AttenTimer").text(25);
                clearInterval(TimeSeccounter);
                $('#group-attendance').ajaxForm(optionsAtten).submit();  		
            });
    });
    function showRequestGroupAtten(formData, jqForm, optionsAtten) 
    { 
        $('#sec-id').text($("input[name='section']").val());
        $('#group-semester').text($(".group_sem option:selected").text());
        $("#errors-group-attendance").hide().empty();
        $("#output").css('display','none');
        
        return true; 
    } 
    function showResponseGroupAtten(response, statusText, xhr, $form)  
    {
        if(response.success == true && response.actionType == 'start')
        {
            $(".attendance-mid-section").addClass('disabled-field');
            $('.group-attendance-start').button('reset');
            $('.group-attendance-start').addClass('hidden');
            $('.group-attendance-end').removeClass('hidden');
            $('#id_group_lecture').val(response.nIdGroupLecture);
            $('.random').css("display","block");
            $('#random').text(response.random);
            $('.attendance_type').addClass('hidden');
            $('#attendance_content').addClass('attendance_content');
            $('#start_attendance_flag').val(1);
            TimeSeccounter=setInterval(groupTimer, 1000);
        }
        else if(response.success == true && response.actionType == 'end')
        {
            $(".attendance-mid-section").removeClass('disabled-field');
            $('.group-attendance-end').button('reset');
            $('.group-attendance-start').show();
            $('.random').css("display","none");
            $('#random').text('');
            $('#start_attendance_flag').val(0);
            $('.group-attendance-end').addClass('hidden');
            $('.group-attendance-start').removeClass('hidden');
            clearInterval(TimeSeccounter);
            $("#AttenTimer").text(25);
            $('#attendance')[0].reset();
            showAttendance(0);
            window.location = siteUrl + '/group/attendance';
        }
        else if(response.success == false)
        {
            //$('#attendance')[0].reset();
            $('.group-attendance-end').button('reset');
            $('.group-attendance-start').button('reset');
            $("#errors-group-attendance").append('<strong>'+response.msg +'</strong>');
            $("#errors-group-attendance").show();
            $("input[name='attendance_code']").val('');
        }
    }
    function showErrorGroupAtten(xhr, textStatus, errorThrown)  
    {
        $('.group-attendance-start').button('reset');
        $('.group-attendance-end').button('reset');
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                $("#errors-group-attendance").append('<strong>'+ value +'</strong>');
                $(".attendance-mid-section input[name='"+index+"']").addClass('error');
            }
        });
        $("#errors-group-attendance").show();
    }
    
    function getOption(option,sId)
    {
        if(option.value == 'winter-1' || option.value == 'winter')
            $('#'+sId).removeClass('hidden');
        else
            $('#'+sId).addClass('hidden');
    }
</script>