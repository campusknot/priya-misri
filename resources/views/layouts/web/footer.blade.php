<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
</script>

<div class="container-fluid footer-bg">
    <div class="">
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 padding-top-footer sml-footer">
            <!--<p class=" sml-footer"> <a href="{{ url('/about') }}">{{ trans('messages.about') }}</a> |-->
            <a href="{{ url('/privacy') }}" target="_blank">{{ trans('messages.privacy_policy') }}</a> |
            <a href="{{ url('/terms') }}" target="_blank">{{ trans('messages.terms_of_use') }}</a>
            
            <span class="pull-right  sml-footer">&copy; {{ trans('messages.All_rights_reserved') }}</span>
            <ul class="sml-footer">
                <li><a href="https://www.facebook.com/campusknot/" target="_blank">
                        <img src="{{asset('assets/web/img/facebook.png')}}" alt=""/>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/campusknot/" target="_blank">
                        <img src="{{asset('assets/web/img/twitter.png')}}" alt=""/>
                    </a>
                </li>
                <li>
                    <a href="https://plus.google.com/+Campusknots/posts" target="_blank">
                        <img src="{{asset('assets/web/img/google-plus.png')}}" alt=""/>
                    </a>
                </li>
            </ul>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
        
    </div>
</div>










        
        
        
        