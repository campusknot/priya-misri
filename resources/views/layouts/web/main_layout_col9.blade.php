<!DOCTYPE html>
<html lang="en">
    <head>
        <!--[if IE]>
        <meta HTTP-EQUIV="REFRESH" content="0; url=http://www.google.com">
        <![endif]-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{!! csrf_token() !!}"/>
        
        <title>@yield('title')</title>
        @section('includes')
            @include('layouts.web.includes')
        @show
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-91094631-1', 'auto');
            ga('send', 'pageview');

        </script>
    </head>
    <body >
         <div class="app-wrapper"></div>
          <input type="hidden" id="campus_image_url" value="{{ $sCampusImage }}" />
        <div id="wrapper" class="ck-app">
            <div id="temp_message" class="temp-message">
                <span id="flash_msg">
                    @if (session('success_message'))
                        {{ session('success_message') }}
                    @endif 
                </span>
            </div>
            <div id="light_box" class="modal fade in">
                <div id="black_overlay" class="black_overlay"></div>
                <div id="lightbox_content" class="lightbox_content"></div>
            </div>
             <div id="slider_light_box" class="modal fade in slider_light_box">
                <div id="slider_black_overlay" class="black_overlay"></div>
                <div id="slider_light_box_container" class="slider-light-box-container">
                     <div class="slider_loader   hidden">
                        <div class="loader-text">
                            <div class="spinner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                            <div>{{ trans('messages.loading') }}</div>
                        </div>
                    </div>
                    <div id="slider_light_box_content" class=" slider_light_box_content"></div>
                </div>
            </div>
            <header class="min-width">
                @section('header')
                    @include('layouts.web.main_header')
                @show
                <div class="attendance-bg-black"></div>
            </header>
            <div class="container-fluid">
                <div class="row add-top-margin">
                    
                    <div class="attendance" >
                     
                        <div class="submit_atten hidden" >
                            @include('layouts.web._submit_attendace')
                        </div>
                        <div class="take_atten hidden">
                            @include('layouts.web._take_attendace')                   
                        </div>
                      
                    </div>
                    <div class="attendance-bg-black"></div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-padding">
                        <div class="">
                            @include('layouts.web._navigationbar')
                        </div>
                        <div class="">
                            @yield('left_sidebar')
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 padding-10">
                        
                        @yield('content')
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        @yield('right_sidebar')
                      </div>
                    @include('layouts.web.contact_form')
                    
                </div>
            </div>
            <footer>
                <script type="text/javascript">
                    @yield('footer-scripts')
                </script>
                @yield('footer_sub')
            </footer>
        </div>
           <script type="text/javascript">
            var bgImage = "url('" + $("#campus_image_url").val() + "')";
            $(".app-wrapper").css("background",bgImage);
            
            
        </script>
    </body>
</html>