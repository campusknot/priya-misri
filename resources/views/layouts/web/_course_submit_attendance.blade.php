<form id="attendance" action="{{ route('group.submit-attendance') }}" method="post">
    {!! csrf_field() !!}
    <div id="errors-attendance" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error_message"></div>
    <div class="clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group ">
            
            <input type="text" id="subject_name_submit" data-toggle="tooltip" data-placement="top" class="form-control" name="course_name" placeholder="{{trans('messages.select_course')}}" value="{{ old('course_name') }}" required="required">
            <input id="course_id_submit" name='group_id' type="text" class="hidden" />
            <input id="end_course_submit" type="text" name="end" value="0" class="hidden" />
            <input type="text" name="entity_type" value="{{ config('constants.ATTENDANCETYPECOURSE') }}" class="hidden" />
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pos-relative form-group text-left">
            <div class="faculty-attendance-section semester">
                <span>{{ trans('messages.attendance_code') }}:</span>  
                <input name="attendance_code" id="code" type="text" maxlength="6" data-toggle="tooltip" data-placement="top" class="form-control " onpaste="return false;" onCopy="return false;">  
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <button type="button"  class="btn btn-primary submit-attendance-course-end btn-attendance-start" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">Submit</button>
            <span onclick="showAttendance(0);" class="close-attendance">{{ trans('messages.cancel') }}</span>
        </div>
    </div>
</form>
<script type="text/javascript">
$(document).ready(function($) 
    {
        var optionsAtten = { 
            beforeSubmit:  showRequestCSubmitAtten,
            success:       showResponseCSubmitAtten,
            error:showErrorCSubmitAtten,
            dataType: 'json' 
            }; 
        $('.submit-attendance-course-end').click(function(e){
                $(this).button('loading');
                $('#end_course_submit').val(1);
                $('#attendance').ajaxForm(optionsAtten).submit();  		
            });
    });
    function showRequestCSubmitAtten(formData, jqForm, optionsAtten) 
    { 
        $('#sec-id').text($("input[name='section']").val());
        $('#semester-val').text($(".attendance-semester select[name='semester']  option:selected").text());
        $("#errors-attendance").hide().empty();
        $("#output").css('display','none');
        return true; 
    } 
    function showResponseCSubmitAtten(response, statusText, xhr, $form)  
    {
        if(response.success == true)
        {
            $('#attendance')[0].reset();
            $('.submit-attendance-course-end').button('reset');
            $('#flash_msg').text(response.msg);
            showAttendance(0);
            showTemporaryMessage();
        }
        else if(response.success == false)
        {
            $('.submit-attendance-course-end').button('reset');
            $('.btn-attendance-start').button('reset');
            $("#errors-attendance").append('<strong>'+response.msg +'</strong>');
            $("#errors-attendance").show();
            $("input[name='attendance_code']").val('');
        }
    }
    function showErrorCSubmitAtten(xhr, textStatus, errorThrown)  
    {
        $('.submit-attendance-course-end').button('reset');
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                $("#errors-attendance").append('<strong>'+ value +'</strong>');
                $(".attendance-mid-section input[name='"+index+"']").addClass('error');
            }
        });
        $("#errors-attendance").show();
    }

    $(function()
    {
        $('#subject_name_submit').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('group.group-suggestion') }}?group_name=" + request.term+"&search_for=member&group_cat={{ config('constants.COURSEGROUPCATEGORY') }}", function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: htmlSpecialChars(value.group_name),
                            value: value.id_group,
                            image: value.group_img,
                        };

                    }));
                });
            },
            minLength: 0,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                $("input#subject_name_submit").val(ui.item.label);
                $("input#course_id_submit").val(ui.item.value);
                event.preventDefault();
            },
            select: function(event, ui) {
                event.stopPropagation();
                $("input#subject_name_submit").val(ui.item.label);
                $("input#course_id_submit").val(ui.item.value);
                return false;
            }
        }).focus(function() {
            $(this).autocomplete("search", "");
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span class='icon-image img-35 mr-10'>"+ item.image+"</span><span>" + item.label +"</span>" )
                .appendTo( ul );
            };
    });
</script>