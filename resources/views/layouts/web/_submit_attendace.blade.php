<div class="student">
    <div class="form-group clearfix">
        
        <div class="col-lg-3 col-md-2 col-sm-2 col-xs-2"></div>
        <div class="col-lg-6 col-md-8 col-xs-8 col-sm-8 attendance-mid-section clearfix">
            <h3>{!! trans('messages.submit_attendance') !!}</h3>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#course_submit">Attendance for Course</a></li>
                <li><a data-toggle="tab" href="#group_submit">Attendance for Group</a></li>
            </ul>
            <div class="tab-content clearfix">
                <div id="course_submit" class="tab-pane fade in active">
                    @include('layouts.web._course_submit_attendance')
                </div>
                <div id="group_submit" class="tab-pane fade">
                    <form id="group-attendance-submit" action="{{ route('group.submit-attendance') }}" method="post">
                        {!! csrf_field() !!}
                        <div id="errors-attendance-group" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 error_message"></div>
                        <div class="clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group pos-relative">
                                <span class="caret caret-right"></span>
                                <input type="text" id="group_name_submit" data-toggle="tooltip" data-placement="top" class="form-control" name="group_name" placeholder="{{trans('messages.search_group')}}" value="{{ old('course_name') }}" required="required">
                                <input id="group_id_submit" name='group_id' type="text" class="hidden" />
                                <input id="end_group_submit" type="text" name="end" value="0" class="hidden" />
                                <input id="entity_type" type="text" name="entity_type" value="{{ config('constants.ATTENDANCETYPEGROUP') }}" class="hidden" />
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pos-relative form-group text-left">
                                <div class="faculty-attendance-section">
                                    <span>{{ trans('messages.attendance_code') }}:</span>  
                                    <input name="attendance_code" id="code" type="text" maxlength="6" data-toggle="tooltip" data-placement="top" class="form-control " onpaste="return false;" onCopy="return false;">  
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="button"  class="btn btn-primary submit-attendance-group-end btn-attendance-start" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">Submit</button>
                                <span onclick="showAttendance(0);" class="close-attendance">{{ trans('messages.cancel') }}</span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>  
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function($) 
    {
        var optionsAtten = { 
            beforeSubmit:  showRequestGSubmitAtten,
            success:       showResponseGSubmitAtten,
            error:showErrorGSubmitAtten,
            dataType: 'json' 
            }; 
        $('.submit-attendance-group-end').click(function(e){
                $(this).button('loading');
                $('#end_course_submit').val(1);
                $('#group-attendance-submit').ajaxForm(optionsAtten).submit();  		
            });
    });
    function showRequestGSubmitAtten(formData, jqForm, optionsAtten) 
    { 
        $("#errors-attendance-group").hide().empty();
        $("#output").css('display','none');
        return true; 
    } 
    function showResponseGSubmitAtten(response, statusText, xhr, $form)  
    {
        if(response.success == true)
        {
            $('#group-attendance-submit')[0].reset();
            $('.submit-attendance-group-end').button('reset');
            $('#flash_msg').text(response.msg);
            showAttendance(0);
            showTemporaryMessage();
        }
        else if(response.success == false)
        {
            $('.submit-attendance-group-end').button('reset');
            $('.btn-attendance-start').button('reset');
            $("#errors-attendance-group").append('<strong>'+response.msg +'</strong>');
            $("#errors-attendance-group").show();
            $("input[name='attendance_code']").val('');
        }
    }
    function showErrorGSubmitAtten(xhr, textStatus, errorThrown)  
    {
        $('.submit-attendance-group-end').button('reset');
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                $("#errors-attendance-group").append('<strong>'+ value +'</strong>');
                $(".attendance-mid-section input[name='"+index+"']").addClass('error');
            }
        });
        $("#errors-attendance-group").show();
    }

    $(function()
    {
        $('#group_name_submit').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('group.group-suggestion') }}"+"?group_name=" + request.term+"&search_for=member", function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: htmlSpecialChars(value.group_name),
                            value: value.id_group,
                            image: value.group_img
                        };

                    }));
                });
            },
            minLength: 0,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                $("input#group_name_submit").val(ui.item.label);
            },
            select: function(event, ui) {
                $("input#group_name_submit").val(ui.item.label);
                $("input#group_id_submit").val(ui.item.value);
                event.stopPropagation();
                return false;
            }
        }).focus(function() {
            $(this).autocomplete("search", "");
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span class='icon-image img-35 mr-10'>"+ item.image+"</span><span>" + item.label + "</span>" )
                .appendTo( ul );
            };
        
    });
</script>