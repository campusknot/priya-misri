@section('includes')
    <!-- fav icon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/web/img/favicon.ico') }}" />
    <link rel="icon" type="image/png" href="{{ asset('assets/web/img/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('assets/web/img/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('assets/web/img/favicon-96x96.png') }}" sizes="96x96" />
    
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap-theme.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/datepicker.css') }}"> 
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/dropzone.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}">
<!--    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/tabdrop.css') }}"> -->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap-chosen.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/select2.min.css') }}">
<!--    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/fixedTable.css') }}">-->
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/fixedTableStyles.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap-select.css') }}">
    
    <script type="text/javascript">var siteUrl = {!! json_encode(url('/')) !!};</script>
    
    <script type="text/javascript" src="{{ asset('assets/web/js/jquery-1.12.2.min.js') }}"></script>
<!--    <script type="text/javascript" src="{{ asset('assets/web/js/common.js') }}"></script>-->
    <script type="text/javascript" src="{{ asset('assets/web/js/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/jquery-ui-1.11.4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/style.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/auto-expand.js') }}"></script>
    
    <script type="text/javascript" src="{{ asset('assets/web/js/bootstrap-tabdrop.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/jquery.form.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/jquery.mousewheel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/chosen.jquery.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/select2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/media-parser.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/fixedheadertable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/jquery.fixedheadertable.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/dropzone.js') }}"></script>
    
    <!--Crop image js and css -->
    <script type="text/javascript" src="{{ asset('assets/web/css/crop/cropper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/css/crop/main.js') }}"></script>
    
    <!--Fancy Alert - Confirmation Box -->
    <script src="{{ asset('assets/web/plugin/dist/sweetalert.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/web/plugin/dist/sweetalert.css') }}">
    <?php echo trans('messages.'.config('constants.ga_script_type')); ?>
@stop