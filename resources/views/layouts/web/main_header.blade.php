<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                <a href="{{ url('/') }}" rel="home" class="">
                    <img class="logo" src="{{asset('assets/web/img/main_header_logo.png')}}">
                </a>
                <form id="group_search" name="group_search" action="{{ route('user.global-search') }}" class="header-global-form" method="POST">
                    {!! csrf_field() !!}
                    <div class="header-global-search pull-right display-ib pos-relative">
                        <input type="text" id="globalSearch" class="form-control search expanded" name="globalSearch" placeholder="{{trans('messages.global_search_placeholder')}}" required="required" value="{{ isset($sGSearchStr) ? htmlspecialchars($sGSearchStr) : ''}}">
                        <button type="submit">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </form>
            </div>
            
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                <!-- user profile Action-->
                <div class="display-ib pos-relative pull-right">
                    <a data-toggle="dropdown" href="javascript:void(0);" class="profile-pic pull-right ddlink">
                        {!! setProfileImage("100",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                        <span class=""><p>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</p></span>
                    </a>
                    <ul id="user-option" class="dropdown-menu profile-edit drop-border arrow-up ">
                        <li><a href="{{ url('/account-settings') }}">
                                   <img src="{{asset('assets/web/img/account-settings.png')}}">
                                   <span> {!! trans('messages.account_settings') !!} </span></a></li>
                        <li><a href="{{ route('user.profile',[Auth::user()->id_user]) }}">
                                   <img src="{{asset('assets/web/img/edit-profile.png')}}">
                                   <span>{!! trans('messages.edit_profile') !!}</span></a></li>
                        <li><a href="{{ url('/logout') }}">
                                   <img src="{{asset('assets/web/img/logout.png')}}">
                                   <span>{!! trans('messages.logout') !!}</span></a>
                        </li>
                    </ul>
                </div>
                
                <div id="notify" class="notification pull-right">
                    <!-- Notification icons-->
                    
                    <a id="all-notification" href="javascript:void(0);" class="display-ib pos-relative" onclick="showNotificationSlider();">
                        <img src="{{asset('assets/web/img/notification.png')}}">
                        @if($nNotificationCount)
                            <div id="notification-count-lable"
                                 class="notification-count-lable">{{ $nNotificationCount }}</div>
                        @endif
                    </a>
                    
                    <!-- Add icons-->

                    <div class="display-ib pos-relative padding-tb-25">
                        <a data-toggle="dropdown" class="dropdown-toggle ddlink" href="javascript:void(0);">
                            <img src="{{asset('assets/web/img/add_button.png')}}">
                        </a>
                        <ul id="add-options" class="dropdown-menu profile-edit drop-border arrow-up ">
                            <li>
                                <a onclick="showAddGroupLightBox();" href="javascript:void(0);">
                                    <img src="{{asset('assets/web/img/add-group.png')}}">
                                    <span> {!! trans('messages.add_group') !!}</span></a>
                            </li>
                            <li>
                                <a onclick="showAddEventLightBox();" href="javascript:void(0);">
                                    <img src="{{asset('assets/web/img/add-event.png')}}">
                                    <span>{!! trans('messages.add_event') !!}</span></a>
                            </li>
                            
<!--                            <li>
                                <a onclick="showAddCourseLightBox();" href="javascript:void(0);">
                                       <img src="{{asset('assets/web/img/add-course.png')}}">
                                    <span>{!! trans('messages.add_course') !!}</span></a>
                            </li>-->
                            <li>
                                <a onclick="showAttendance(1,'submit');" href="javascript:void(0);">
                                       <img src="{{asset('assets/web/img/attendance-submit.png')}}">
                                    <span>{!! trans('messages.submit_attendance') !!}</span></a>
                            </li>
                            <li>
                                <a onclick="showAttendance(1,'take');" href="javascript:void(0);">
                                       <img src="{{asset('assets/web/img/add-attendace.png')}}">
                                    <span>{!! trans('messages.add_attendance') !!}</span></a>
                            </li>
                            
                            
                            @if(Auth::user()->user_type == config('constants.USERTYPEFACULTY'))
                            <li>
                                <a onclick="showAddCourseGroupLightBox();" href="javascript:void(0);">
                                      <img src="{{asset('assets/web/img/add-course.png')}}">
                                       <span>{!! trans('messages.add_corse_group') !!}</span></a>
                            </li>
                            @endif
                            
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
        type: "GET",
        url: siteUrl + '/user/user-check-login',
        cache: false,
        success: function (data) {
        },
        error: function (data) {
            if(data.status == 401)
            {
                window.location = siteUrl + '/login';
            }
        }
        });
    });
</script>
<script type="text/javascript">
    $('.icon').click(function () {
        $('.header-global-search').toggleClass('view-header-search');
    });
    $(function()
    {
        $('#globalSearch').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('user.global-search-ajax') }}"+"/" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.first_name+' '+value.last_name,
                            value: value.id_user,
                            image: value.profile_img,
                            major: value.major
                        };

                    }));
                });
            },
            minLength: 1,
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                $('#globalSearch').val(  ui.item.label );
                event.preventDefault();
            },
            select: function(event, ui) {
                $('#globalSearch').val(  ui.item.label );
                window.location = siteUrl +"/user/profile/"+ ui.item.value;
                event.preventDefault();
               
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span class='icon-image img-35'>"+item.image +"</span><a class='header-search-proile-link' href='"+ siteUrl +"/user/profile/"+ item.value +"'  > <span>" + item.label + "</span><span class='major'>"+item.major +"</span></a>" )
                .appendTo( ul );
            };
    });
</script>