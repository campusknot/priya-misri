<div class="row sub-header"  data-spy="affix" data-offset-top="60" data-offset-bottom="200">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 left-padding">
        <div class="dropdown">
            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <?php
                    $sCurrentPage = (session('current_page') !== null) ? session('current_page') : 'home';
                ?>
                <img src="{{ asset('assets/web/img/menu_'.$sCurrentPage.'.png') }}">
                <span id="current_page_name">{{trans('messages.'.$sCurrentPage)}}</span>
                <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li>
                    <a href="{{ route('/') }}">
                        <img src="{{ asset('assets/web/img/menu_home.png') }}">{{trans('messages.home')}}
                    </a>
                </li>
                <li>
                    <a href="{{ route('group.group-listing') }}">
                        <img src="{{ asset('assets/web/img/menu_groups.png') }}">{{trans('messages.groups')}}
                    </a>
                </li>
                <li>
                    <a href="{{ route('event.event-listing') }}">
                        <img src="{{ asset('assets/web/img/menu_events.png') }}">{{trans('messages.events')}}
                    </a>
                </li>
                <li>
                    <a href="{{ route('user.my-profile') }}">
                        <img src="{{ asset('assets/web/img/menu_profile.png') }}">{{trans('messages.profile')}}
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-7 col-md-5 col-sm-5 col-xs-5">
        @if(Request::route()->getName() == 'event.event-listing' )
            <ul class="nav nav-pills">
                <!-- add tabs -->
                <li role="presentation" class="active"><a href="#tab_group_event_all" data-toggle="tab">All</a></li>

                @foreach($oGroupEventList as $oGroup)
                    <li role="presentation"><a href="#tab_group_event_{{$oGroup->id_group}}" data-toggle="tab">{{$oGroup->group_name}}</a></li>
                @endforeach
            </ul>
        @endif
    </div>
    <div id="sticky-notification" class="col-lg-3 col-md-5 col-sm-5 col-xs-5 notification display hidden" >
        <!-- Notification icon section -->
        <div class="pull-left p-l-3 p-r-3">
            <a href="javascript:void(0);" onclick="showNotificationSlider();" data-target="#myModalNotification" data-toggle="modal">
                <img src="{{asset('assets/web/img/notification.png')}}">
                @if($nNotificationCount)
                <div id="notification-count-lable" class="notification-count-lable">{{ $nNotificationCount }}</div>
                @endif
            </a>
        </div>
        <!-- Add actions -->
        <div class="pull-left p-l-3 p-r-3">
            <a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0);">
                <img src="{{asset('assets/web/img/add_button.png')}}">
            </a>
            <ul id="add-options" style=" margin: 5px -15px 0 0" class="dropdown-menu drop-border arrow-up newppp">
                <li>
                    <a onclick="showAddGroupLightBox();" data-toggle="modal" href="javascript:void(0);">{!! trans('messages.add_group') !!}</a>
                </li>
                <li>
                    <a data-toggle="modal" href="#myModal-add-event">{!! trans('messages.add_event') !!}</a>
                </li>
            </ul>
        </div>
        <!-- User profile actions -->
        <div class="pull-left p-l-3 p-r-3">
            <a data-toggle="dropdown" href="javascript:void(0);">
                <div class="profile-pic">
                    <img src="{{ (Auth::user()->file_name) ? config('constants.MEDIAURL').'/users/'.Auth::user()->file_name : asset('/assets/web/img/user_default.png') }}" alt="">
                    {{ Auth::user()->first_name.' '.Auth::user()->last_name }}
                </div>
            </a>
            <ul id="user-option" style="margin: 5px 0 0 -40px;" class="dropdown-menu drop-border arrow-up newppp newnavright">
                <li><a href="{{ url('/account-settings') }}">{!! trans('messages.account_settings') !!}</a></li>
                <li><a href="{{ route('user.my-profile') }}">{!! trans('messages.edit_profile') !!}</a></li>
                <li style="border-bottom: none"><a href="{{ url('/logout') }}">{!! trans('messages.logout') !!}</a>
                </li>
            </ul>
        </div>
    </div>
</div>