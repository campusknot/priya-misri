<!DOCTYPE html>
<html lang="en">
<head>
        <!--[if IE]>
        <meta HTTP-EQUIV="REFRESH" content="0; url=http://www.google.com">
        <![endif]-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" id="token" content="{{ csrf_token() }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        @section('includes')
        @include('layouts.admin.includes')
        @show
    </head>
    <body>
        <div id="wrapper">
            <div >
                <header class="">
                    @section('header')
                    @include('layouts.admin.main_header')
                    @show
                    <div class = "toast_message"></div>

                </header>
                <div class="container-fluid" id="app">
                    <div class="row add-top-margin">
                      <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                       @include('layouts.admin._navigationbar')
                   </div>
                   <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 padding-10">
                    <div class='ck-alerts'>
                        @include('layouts.admin.alerts')
                    </div>
                    @if(Session::has('campus') && Session::get('campus') != NULL)
                    <div class="campus-header-name padding-10">
                        <h3 class="font-w-400 no-margin display-ib">{{ Session::has('campus') ? App\Libraries\Campus::find(Session::get('campus'))->campus_name : ''}}</h3>
                        @if(Auth::guard('admin')->user()->user_type !=  config('constants.USERTYPECAMPUSADMIN')) 
                        <a href='{{ route('campus.list') }}' class="btn btn-primary btn-xs pull-right">{{trans('messages.change_campus')}}</a>
                        @endif
                    </div>
                    @endif
                    <div class="content-wrapper clearfix">
                        @yield('content')
                    </div>

                </div>
            </div>
        </div>
        <div id="light_box" class="modal fade in">
            <div id="black_overlay" class="black_overlay"></div>
            <div class="modal-dialog" role="document">
                <div id="lightbox_content" class="lightbox_content"></div>
            </div>
        </div>
        <footer>
            <script type="text/javascript">
                @yield('footer-scripts')
            </script>
            @yield('footer_sub')
        </footer>
    </div>
</div>

</body>
</html>