@section('includes')
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/admin/css/style.min.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/admin/css/common.min.css') }}" />
    
    <script type="text/javascript">var siteUrl = {!! json_encode(url('/')) !!};</script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jquery-1.12.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/velocity.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/web/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/common.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/admin/js/jquery.form.js') }}"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    
@stop