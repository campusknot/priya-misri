  <ul class="navigation_bar clearfix">
    <li><a href="{{ url('/') }}">{{ trans('messages.home') }}</a></li>
    @if(Session::has('campus') && Session::get('campus') != NULL)    
    <li><a href="{{ route('user.list') }}">{{ trans('messages.users') }}</a></li>
    <li><a href="{{ route('campus.extension_list') }}">{{ trans('messages.extensions') }}</a></li>
    @if(Auth::guard('admin')->user()->user_type != config('constants.USERTYPECAMPUSADMIN'))   
    <li><a href="{{ url('/log-viewer/logs') }}">{{ trans('messages.logs') }}</a></li>
    <li><a href="{{ route('campus.create_admin') }}">{{ trans('messages.create_campus_admin') }}</a></li>
    @endif
    @endif
</ul>