<div class="container-fluid admin-header">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix ">
       
        <div class="header_logo pull-left">
            <a href="{{ url('/') }}">
               <img class="logo" src="{{asset('assets/admin/img/admin_logo.png')}}" />
            </a>
        </div>
        <div class="pull-right profile-info">
           
            <span class="profile-user dropdown-toggle" data-toggle="dropdown"> <div class="profile-img "></div> {{  Auth::guard('admin')->user()->first_name.' '.Auth::guard('admin')->user()->last_name }}</span>
             <ul class="dropdown-menu">
                 
                <li><a href="{{ route('user.profile') }}">{{trans('messages.edit_profile')}}</a></li>
                <li><a href="{{ route('user.change-password') }}">{{trans('messages.change_password')}}</a></li>
                <li><a href="{{ route('user.logout') }}">{{trans('messages.logout')}}</a></li>
              </ul>
        </div>
    </div>
</div>
