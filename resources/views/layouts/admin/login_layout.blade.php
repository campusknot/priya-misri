<!DOCTYPE html>
<html lang="en">
    <head>
        <!--[if IE]>
        <meta HTTP-EQUIV="REFRESH" content="0; url=http://www.google.com">
        <![endif]-->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" id="token" content="{{ csrf_token() }}"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        @section('includes')
            @include('layouts.admin.includes')
        @show
    </head>
    <body>
        <div id="wrapper" class="main-wrapper">
            <div>
                <header class="">
                    @section('header')
                        @include('layouts.admin.login_header')
                    @show
                </header>
                <div class="container-fluid" id="app">
                    <div class="row add-top-margin">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
                @if(!Auth::guard('admin')->user()) 

                <footer class="footer-container">
                    @section('footer')
                        @include('layouts.admin.login_footer')
                    @show
                    <script type="text/javascript">
                        @yield('footer-scripts')
                    </script>
                    @yield('footer_sub')
                </footer>
                @endif

            </div>
        </div>
        
    </body>
</html>