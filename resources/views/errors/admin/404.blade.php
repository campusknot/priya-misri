<!DOCTYPE html>
<html lang="en">
  <head>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/web/img/favicon.ico') }}" />
    <link rel="icon" type="image/png" href="{{ asset('assets/web/img/favicon-16x16.png') }}" sizes="16x16" />
    <link rel="icon" type="image/png" href="{{ asset('assets/web/img/favicon-32x32.png') }}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{ asset('assets/web/img/favicon-96x96.png') }}" sizes="96x96" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Campusknot - Page not found</title>
    <link href="http://fonts.googleapis.com/css?family=Dosis:400,500,700" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}">
</head>
  <body>
      
      <div class="container-fluid  no-padding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding page-not-found">
            <a href="{{ url('/') }}">
<!--                <img  src="{{asset('assets/web/img/error_404.png')}}">-->
                <div class="error-wrapper">
                    <div class="error-header">
                        <img  src="{{asset('assets/web/img/main_header_logo.png')}}">
                    </div>
                    <div class="large-fonts">
                        404
                    </div>
                    <div class="small-text">
                        {{ trans('messages.page_not_found')}}
                    </div>
                    <span class="error-page-btn btn btn-primary">
                        {{ trans('messages.go_back_to_home_page')}}
                    </span>
                    
                </div>
            </a>
        </div>
      </div>
      
  </body>
</html>