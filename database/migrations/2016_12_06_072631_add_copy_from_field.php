<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCopyFromField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('documents'))
        {
            Schema::table('documents', function ($table) {
                $table->integer('copy_from')->nullable()->after('document_type')->comment('If document will share in group, new copy of current document will generate and this field will contain original document id.');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
