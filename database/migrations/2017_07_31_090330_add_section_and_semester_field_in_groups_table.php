<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSectionAndSemesterFieldInGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('groups'))
        {
            Schema::table('groups', function ($table) {
                $table->string('section')->after('group_type')->nullable();
                $table->string('semester')->after('group_type')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('groups', function ($table) {
            $table->dropColumn('section');
            $table->dropColumn('semester');
        });
    }
}
