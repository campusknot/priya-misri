<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_documents'))
        {
            Schema::create('group_documents', function (Blueprint $table) {
                $table->increments('id_group_document');
                
                $table->integer('id_group')->unsigned()->index();
                $table->integer('id_document')->unsigned()->index();
                $table->enum('group_document_permission', array('W', 'R'))->default('R')
                      ->comment("This field will contain the value of permission when document is shared in group. New member in group will have same permission.");
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_group')
                        ->references('id_group')->on('groups')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_document column
                $table->foreign('id_document')
                        ->references('id_document')->on('documents')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_documents'))
        {
            Schema::drop('group_documents');
        }
    }
}
