<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('events'))
        {
            Schema::create('events', function (Blueprint $table) {
                $table->increments('id_event');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('event_title');
                $table->text('event_description')->nullable();
                $table->dateTime('start_date');
                $table->dateTime('end_date')->nullable();
                $table->string('room_number')->nullable();
                $table->string('address');
                $table->double('latitude', 11, 8);
                $table->double('longitude', 11, 8);
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('events'))
        {
            Schema::drop('events');
        }
    }
}
