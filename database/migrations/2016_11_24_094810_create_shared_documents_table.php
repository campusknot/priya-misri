<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharedDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('shared_documents'))
        {
            Schema::create('shared_documents', function (Blueprint $table) {
                $table->increments('id_shared_document');
                
                $table->integer('id_document')->unsigned()->index();
                $table->integer('id_user')->unsigned()->index()->comment('Shared by this user');
                $table->integer('shared_with')->comment('Shared with this user');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                $table->foreign('id_document')
                        ->references('id_document')->on('documents')
                        ->onDelete('cascade');
                
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('shared_documents'))
        {
            Schema::drop('shared_documents');
        }
    }
}
