<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_requests'))
        {
            Schema::create('group_requests', function (Blueprint $table) {
                $table->increments('id_group_request');
                
                $table->integer('id_group')->unsigned()->index();
                $table->integer('id_user_request_from')->unsigned()->index();
                $table->integer('id_user_request_to')->unsigned()->index();
                $table->enum('request_type', array('I', 'J'))->comment('I = invitation_request, J = join_request');
                $table->enum('member_status', array('P', 'A', 'R'))->comment('P = pending, A = accepted, R = rejected');
                
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_requests'))
        {
            Schema::drop('group_requests');
        }
    }
}
