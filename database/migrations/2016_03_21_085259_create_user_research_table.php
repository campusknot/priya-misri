<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_research'))
        {
            Schema::create('user_research', function (Blueprint $table) {
                $table->increments('id_user_research');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('research_title');
                $table->string('research_url')->nullable();
                $table->string('research_author');
                $table->text('research_description')->nullable();
                $table->date('research_year');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_research'))
        {
            Schema::drop('user_research');
        }
    }
}
