<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIdGroupFieldInDocumentPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('document_permissions'))
        {
            Schema::table('document_permissions', function($table) {
                $table->integer('id_group')->unsigned()->index()->nullable()->after('id_user');
		$table->foreign('id_group')
                        ->references('id_group')->on('groups')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
