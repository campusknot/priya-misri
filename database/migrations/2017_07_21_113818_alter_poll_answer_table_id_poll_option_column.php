<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPollAnswerTableIdPollOptionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('poll_answers'))
        {
            DB::statement('ALTER TABLE `poll_answers` MODIFY `id_poll_option` INTEGER UNSIGNED NULL;');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('poll_answers'))
        {
            DB::statement('ALTER TABLE `poll_answers` MODIFY `id_poll_option` INTEGER UNSIGNED NOT NULL;');
        }
    }
}
