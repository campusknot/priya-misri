<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('attendance_codes'))
        {
            Schema::create('attendance_codes', function (Blueprint $table) {
                $table->increments('id_attendance_code');
                
                $table->integer('id_lecture')->unsigned()->index();
                $table->string('attendance_code')->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_course column
                $table->foreign('id_lecture')
                        ->references('id_lecture')->on('lectures')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('attendance_codes'))
        {
            Schema::drop('attendance_codes');
        }
    }
}
