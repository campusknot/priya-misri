<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        if (!Schema::hasTable('read_notifications'))
        {
            Schema::create('read_notifications', function (Blueprint $table) {
                $table->increments('id_read_notification');

                $table->integer('id_notification')->unsigned()->index();
                $table->timestamps();

                //Add foreign key constrain in id_notification column
                $table->foreign('id_notification')
                        ->references('id_notification')->on('notifications')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('read_notifications'))
        {
            Schema::drop('read_notifications');
        }
    }
}
