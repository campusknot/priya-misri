<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('likes'))
        {
            Schema::create('likes', function (Blueprint $table) {
                $table->increments('id_like');
                
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_entity');
                $table->enum('entity_type', array('P'))->comment('P = post');
                
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('likes'))
        {
            Schema::drop('likes');
        }
    }
}
