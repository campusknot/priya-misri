<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterAttendanceCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('attendance_codes'))
        {
            Schema::disableForeignKeyConstraints();
            Schema::table('attendance_codes', function($table) {
                $table->enum('entity_type',array('C','G'))->default('C')->after('attendance_code')->comment('C = Course Attendace , G = Group Attendance');
                
                $table->dropIndex('attendance_codes_id_lecture_index');
            });
            Schema::enableForeignKeyConstraints();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_codes', function ($table) {
            $table->dropColumn('entity_type');
            $table->index('id_lecture');
        });
    }
}
