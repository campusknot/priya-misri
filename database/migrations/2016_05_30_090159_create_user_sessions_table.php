<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_sessions'))
        {
            Schema::create('user_sessions', function (Blueprint $table) {
                $table->increments('id_user_session');

                $table->integer('id_user')->unsigned()->index();
                $table->string('id_device')->unique();
                $table->string('unique_key');
                $table->dateTime('login_time');
                $table->dateTime('logout_time');
                $table->timestamps();
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_sessions'))
        {
            Schema::drop('user_sessions');
        }
    }
}
