<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePollTypeEnumValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('polls'))
        {
            Schema::table('polls', function($table) {
                $table->dropIndex('polls_poll_type_index');
                $table->dropColumn('poll_type');
            });
            
            Schema::table('polls', function($table) {
                $table->enum('poll_type', array('M', 'O'))->default('M')->index()->after('allow_multiple_answers')->comment('M = multiple choice poll, O = open ended poll');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('polls'))
        {
            Schema::table('polls', function($table) {
                $table->dropIndex('polls_poll_type_index');
                $table->dropColumn('poll_type');
            });
            Schema::table('polls', function($table) {
                $table->enum('poll_type', array('OPN', 'SEC', 'SECD'))->index()->after('allow_multiple_answers')->comment('OPN = display result while poll is active, SEC = secret and not display result, SECD = secret and display the result after poll completed.');
            });
        }
    }
}
