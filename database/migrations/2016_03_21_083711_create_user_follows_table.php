<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserFollowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_follows'))
        {
            Schema::create('user_follows', function (Blueprint $table) {
                $table->increments('id_user_follow');
                
                $table->integer('id_follower')->unsigned()->index();
                $table->integer('id_following')->unsigned()->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_follower column
                $table->foreign('id_follower')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_following column
                $table->foreign('id_following')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_follows'))
        {
            Schema::drop('user_follows');
        }
    }
}
