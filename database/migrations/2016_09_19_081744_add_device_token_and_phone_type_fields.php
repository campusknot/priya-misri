<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeviceTokenAndPhoneTypeFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_sessions'))
        {
            Schema::table('user_sessions', function ($table) {
                $table->string('device_token')->after('id_device');
                $table->enum('device_type', array('A', 'I'))->after('device_token')->comment('A = Android, I = iPhone');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}