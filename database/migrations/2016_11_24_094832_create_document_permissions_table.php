<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('document_permissions'))
        {
            Schema::create('document_permissions', function (Blueprint $table) {
                $table->increments('id_document_permission');
                
                $table->integer('id_document')->unsigned()->index();
                $table->integer('id_user')->unsigned()->index();
                               
                $table->enum('permission_type', array('W', 'R'))
                      ->comment("W = write (user can add file and folders, rename it and share with others), R = read only");
                
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                $table->foreign('id_document')
                        ->references('id_document')->on('documents')
                        ->onDelete('cascade');
                
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('document_permissions'))
        {
            Schema::drop('document_permissions');
        }
    }
}
