<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('campuses'))
        {
            Schema::create('campuses', function (Blueprint $table) {
                $table->increments('id_campus');
                $table->integer('id_university');
                $table->string('campus_name');
                $table->string('country_name');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('campuses'))
        {
            Schema::drop('campuses');
        }
    }
}
