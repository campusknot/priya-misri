<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizGroupInviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('quiz_group_invite'))
        {
            Schema::create('quiz_group_invite', function (Blueprint $table) {
                $table->increments('id_quiz_group_invite');
                
                $table->integer('id_quiz')->unsigned();
                $table->integer('id_group')->unsigned();
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_quiz column
                $table->foreign('id_quiz')
                        ->references('id_quiz')->on('quizzes');
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_group')
                        ->references('id_group')->on('groups');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('quiz_group_invite'))
        {
            Schema::drop('quiz_group_invite'); 
        }
    }
}
