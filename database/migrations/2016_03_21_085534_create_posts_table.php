<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('posts'))
        {
            Schema::create('posts', function (Blueprint $table) {
                $table->increments('id_post');
                
                $table->integer('id_user')->unsigned()->index();
                $table->text('post_text');
                $table->enum('post_type', array('T', 'I', 'D', 'P'))->default('T')->index()->comment('T = text, I = image, D = document, P = poll');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('posts'))
        {
            Schema::drop('posts');
        }
    }
}
