<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserJournalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_journals'))
        {
            Schema::create('user_journals', function (Blueprint $table) {
                $table->increments('id_user_journal');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('journal_title');
                $table->string('journal_url')->nullable();
                $table->string('journal_author');
                $table->text('journal_description')->nullable();
                $table->date('journal_year');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_journals'))
        {
            Schema::drop('user_journals');
        }
    }
}
