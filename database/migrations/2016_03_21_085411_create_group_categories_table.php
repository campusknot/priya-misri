<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_categories'))
        {
            Schema::create('group_categories', function (Blueprint $table) {
                $table->increments('id_group_category');
                
                $table->string('group_category_name')->unique();
                $table->string('file_name');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_categories'))
        {
            Schema::drop('group_categories');
        }
    }
}
