<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_members'))
        {
            Schema::create('group_members', function (Blueprint $table) {
                $table->increments('id_group_member');
                
                $table->integer('id_group')->unsigned()->index();
                $table->integer('id_user')->unsigned()->index();
                $table->enum('member_type', array('C', 'A', 'M'))->comment('C = creator, A = admin, M = member');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_group')
                        ->references('id_group')->on('groups')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_members'))
        {
            Schema::drop('group_members');
        }
    }
}
