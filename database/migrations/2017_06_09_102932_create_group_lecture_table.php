<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupLectureTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up()
    {
        if (!Schema::hasTable('group_lectures'))
        {
            Schema::create('group_lectures', function (Blueprint $table) {
                $table->increments('id_group_lecture');
                
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_group')->unsigned()->index();
                $table->string('semester')->index();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_course column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                $table->foreign('id_group')
                        ->references('id_group')->on('groups')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_lectures'))
        {
            Schema::drop('group_lectures');
        }
    }
}
