<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('courses'))
        {
            Schema::create('courses', function (Blueprint $table) {
                $table->increments('id_course');

                $table->integer('id_university')->unsigned()->index();
                $table->string('course_name');
                $table->string('course_code');
                $table->integer('id_user')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                $table->timestamps();
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_university')
                        ->references('id_university')->on('universities')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('courses'))
        {
            Schema::drop('courses');
        }
    }
}
