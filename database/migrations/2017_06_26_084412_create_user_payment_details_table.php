<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_payment_details'))
        {
            Schema::create('user_payment_details', function (Blueprint $table) {
                $table->increments('id_user_payment_detail');

                $table->integer('id_user')->unsigned();
                $table->string('id_transaction')->nullable();
                $table->enum('device_type', array('A', 'I'))->comment('A = Android, I = iPhone');
                $table->dateTime('contract_start_on');
                $table->dateTime('contract_end_on');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_payment_details'))
        {
            Schema::drop('user_payment_details');
        }
    }
}
