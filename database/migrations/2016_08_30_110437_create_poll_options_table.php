<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('poll_options'))
        {
            Schema::create('poll_options', function (Blueprint $table) {
                $table->increments('id_poll_option');

                $table->integer('id_poll')->unsigned()->index();
                $table->text('option_text')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('poll_options'))
        {
            Schema::drop('poll_options');
        }
    }
}
