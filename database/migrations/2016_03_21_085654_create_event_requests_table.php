<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('event_requests'))
        {
            Schema::create('event_requests', function (Blueprint $table) {
                $table->increments('id_event_request');
                
                $table->integer('id_event')->unsigned()->index();
                $table->integer('id_user_request_from')->unsigned()->index();
                $table->integer('id_user_request_to')->unsigned()->index();
                $table->enum('status', array('G', 'M', 'NG'))->nullable()->comment('G = going, M = maybe, NG = not going');
                
                $table->timestamps();
                
                //Add foreign key constrain in id_event column
                $table->foreign('id_event')
                        ->references('id_event')->on('events')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_user_request_from column
                $table->foreign('id_user_request_from')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_user_request_to column
                $table->foreign('id_user_request_to')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('event_requests'))
        {
            Schema::drop('event_requests');
        }
    }
}
