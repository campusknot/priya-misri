<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLecturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('lectures'))
        {
            Schema::create('lectures', function (Blueprint $table) {
                $table->increments('id_lecture');
                
                $table->integer('id_faculty')->unsigned()->index();
                $table->integer('id_course')->unsigned()->index();
                $table->string('section')->nullable();
                $table->string('semester')->index();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_course column
                $table->foreign('id_course')
                        ->references('id_course')->on('courses')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('lectures'))
        {
            Schema::drop('lectures');
        }
    }
}
