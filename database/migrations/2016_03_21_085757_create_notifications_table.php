<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('notifications'))
        {
            Schema::create('notifications', function (Blueprint $table) {
                $table->increments('id_notification');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('id_entites')->comment('will be a combination of id for different entities like (1_2_3)');
                $table->string('entity_type_pattern')->comment('will be combination of entity type in same sequence of ids like (U_G_P where U = user, G = group, P = post)');
                $table->string('notification_type')->comment('will be text like user_post, user_follow');
                
                $table->timestamps();
                
                //Add foreign key constrain in id_notification column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('notifications'))
        {
            Schema::drop('notifications');
        }
    }
}
