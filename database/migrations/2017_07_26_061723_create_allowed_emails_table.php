<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllowedEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('allowed_emails'))
        {
            Schema::create('allowed_emails', function (Blueprint $table) {
                $table->increments('id_allowed_email');
                
                $table->integer('id_campus')->unsigned();
                $table->string('email')->unique()->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_campus column
                $table->foreign('id_campus')
                        ->references('id_campus')->on('campuses');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('allowed_emails'))
        {
            Schema::drop('allowed_emails'); 
        }
    }
}
