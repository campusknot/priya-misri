<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswerOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       if (!Schema::hasTable('answer_options'))
        {
            Schema::create('answer_options', function (Blueprint $table) {
                $table->increments('id_answer_option');
                
                $table->integer('id_quiz_question')->unsigned()->index();
                $table->text('answer_text')->nullable();
                $table->string('file_name')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_question column
                $table->foreign('id_quiz_question')
                        ->references('id_quiz_question')->on('quiz_questions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('questions'))
        {
            Schema::drop('questions'); 
        }
    }
}
