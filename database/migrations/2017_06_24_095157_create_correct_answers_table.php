<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrectAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('correct_answers'))
        {
            Schema::create('correct_answers', function (Blueprint $table) {
                $table->increments('id_correct_answer');
                
                $table->integer('id_quiz_question')->unsigned();
                $table->integer('id_answer_option')->unsigned()->nullable();
                $table->text('answer_description')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_question column
                $table->foreign('id_quiz_question')
                        ->references('id_quiz_question')->on('quiz_questions');
                
                //Add foreign key constrain in id_answer_option column
                $table->foreign('id_answer_option')
                        ->references('id_answer_option')->on('answer_options');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('correct_answers'))
        {
            Schema::drop('correct_answers'); 
        }
    }
}
