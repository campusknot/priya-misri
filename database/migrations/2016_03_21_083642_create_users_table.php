<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('users'))
        {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id_user');

                $table->string('first_name');
                $table->string('last_name')->nullable();
                $table->string('email',100)->unique();
                $table->string('alternate_email',100)->nullable();
                $table->string('password',64);
                $table->string('salt',64);
                $table->integer('id_campus')->unsigned()->index();
                $table->enum('user_type', array('S', 'F', 'CA'))->index()->comment('S = student, F = faculty, CA = campus_admin');
                $table->text('aspiration')->nullable();
                $table->tinyInteger('verified')->default(0);
                $table->text('verification_key');
                $table->string('remember_token', 100) -> nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();

                $table->timestamps();

                //Add foreign key constrain in id_campus column
                $table->foreign('id_campus')
                        ->references('id_campus')->on('campuses')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('users'))
        {
            Schema::drop('users');
        }
    }
}
