<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_courses'))
        {
            Schema::create('user_courses', function (Blueprint $table) {
                $table->increments('id_user_course');

                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_course')->unsigned()->index();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_course')
                        ->references('id_course')->on('courses')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_courses'))
        {
            Schema::drop('user_courses');
        }
    }
}
