<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNonMemberInvitationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('non_member_invitations'))
        {
            Schema::create('non_member_invitations', function (Blueprint $table) {
                $table->increments('id_non_member_invitation');

                $table->integer('id_user_invite_from')->unsigned()->index();
                $table->string('invite_to')->comment('will be a email id of user');
                $table->integer('id_entity_invite_for')->comment('will be a id of entity for which he/she is invited');
                $table->enum('entity_type', array('G', 'E'))->comment('G = group, E = event');
                $table->string('unique_combination_key')->unique()->comment('Combination key to avoid redundant data.');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('non_member_invitations'))
        {
            Schema::drop('non_member_invitations');
        }
    }
}
