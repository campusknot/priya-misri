<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_events'))
        {
            Schema::create('group_events', function (Blueprint $table) {
                $table->increments('id_group_event');
                
                $table->integer('id_group')->unsigned()->index();
                $table->integer('id_event')->unsigned()->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_group')
                        ->references('id_group')->on('groups')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_event column
                $table->foreign('id_event')
                        ->references('id_event')->on('events')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_events'))
        {
            Schema::drop('group_events');
        }
    }
}
