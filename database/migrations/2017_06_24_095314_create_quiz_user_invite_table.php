<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizUserInviteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('quiz_user_invite'))
        {
            Schema::create('quiz_user_invite', function (Blueprint $table) {
                $table->increments('id_quiz_user_invite');
                
                $table->integer('id_quiz')->unsigned();
                $table->integer('id_user')->unsigned();
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->tinyInteger('completed')->default(0)->index()->comment('1 => Completed, 0 => Incomplete');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_quiz column
                $table->foreign('id_quiz')
                        ->references('id_quiz')->on('quizzes');
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('quiz_user_invite'))
        {
            Schema::drop('quiz_user_invite'); 
        }
    }
}
