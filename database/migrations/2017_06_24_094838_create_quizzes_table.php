<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('quizzes'))
        {
            Schema::create('quizzes', function (Blueprint $table) {
                $table->increments('id_quiz');
                $table->integer('id_user')->unsigned();
                $table->string('quiz_title')->index();
                $table->text('description')->nullable();
                $table->integer('duration');
                $table->integer('total_marks');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('quizzes'))
        {
            Schema::drop('quizzes'); 
        }
    }
}
