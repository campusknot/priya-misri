<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;

class ChangePostTypeField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('posts'))
        {
            Schema::table('posts', function ($table) {
                DB::statement("ALTER TABLE `posts` CHANGE `post_type` `post_type` ENUM( 'T', 'I', 'D', 'P' ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'T' COMMENT 'T = text, I = image, D = document, P = poll'");
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
