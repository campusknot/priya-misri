<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCampusUrlFieldInCampuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('campuses'))
        {
            Schema::table('campuses', function($table) {
                $table->string('campus_url')->after('campus_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campuses', function ($table) {
            $table->dropColumn('campus_url');
        });
    }
}
