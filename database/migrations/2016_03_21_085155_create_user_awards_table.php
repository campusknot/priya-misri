<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAwardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_awards'))
        {
            Schema::create('user_awards', function (Blueprint $table) {
                $table->increments('id_user_award');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('award_title');
                $table->string('award_issuer');
                $table->text('award_description');
                $table->date('awarded_year');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_awards'))
        {
            Schema::drop('user_awards');
        }
    }
}
