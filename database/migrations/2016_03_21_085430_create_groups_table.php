<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('groups'))
        {
            Schema::create('groups', function (Blueprint $table) {
                $table->increments('id_group');
                
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_campus')->unsigned()->index();
                $table->string('group_name')->index();
                $table->integer('id_group_category')->unsigned()->index();
                $table->enum('group_type', array('PUB', 'PVT', 'SEC'))->comment('PUB = public, PVT = private, SEC = secret');
                $table->text('about_group')->nullable();
                $table->string('file_name')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users');
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_campus')
                        ->references('id_campus')->on('campuses');
                
                //Add foreign key constrain in id_group_category column
                $table->foreign('id_group_category')
                        ->references('id_group_category')->on('group_categories');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('groups'))
        {
            Schema::drop('groups');
        }
    }
}
