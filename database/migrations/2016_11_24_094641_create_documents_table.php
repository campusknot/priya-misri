<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('documents'))
        {
            Schema::create('documents', function (Blueprint $table) {
                $table->increments('id_document');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('document_name');
                $table->string('file_name')->nullable();
                $table->integer('lft');
                $table->integer('rgt');
                $table->integer('id_parent')->nullable()->index();
                $table->enum('document_type', array('F', 'FO'))->comment('F = File, FO = Folder.');
                $table->integer('depth');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_notification column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('documents'))
        {
            Schema::drop('documents');
        }
    }
}
