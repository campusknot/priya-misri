<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('poll_answers'))
        {
            Schema::create('poll_answers', function (Blueprint $table) {
                $table->increments('id_poll_answer');

                $table->integer('id_poll')->unsigned()->index();
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_poll_option')->unsigned()->index();
                $table->text('poll_answer_description')->nullable();
                
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('poll_answers'))
        {
            Schema::drop('poll_answers');
        }
    }
}
