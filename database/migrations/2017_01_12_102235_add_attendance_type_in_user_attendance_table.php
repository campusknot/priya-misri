<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAttendanceTypeInUserAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_attendance'))
        {
            Schema::table('user_attendance', function($table) {
                $table->enum('attendance_type', array('P','A','SL'))->default('P')->after('id_lecture')->comment('P = Present ,A = Absent, SL = Sick Leave');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
