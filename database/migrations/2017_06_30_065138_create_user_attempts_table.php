<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAttemptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_attempts'))
        {
            Schema::create('user_attempts', function (Blueprint $table) {
                $table->increments('id_user_attempt');
                
                $table->integer('id_quiz')->unsigned();
                $table->integer('id_user')->unsigned();
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_quiz column
                $table->foreign('id_quiz')
                        ->references('id_quiz')->on('quizzes');
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_attempts'))
        {
            Schema::drop('user_attempts');
        }
    }
}
