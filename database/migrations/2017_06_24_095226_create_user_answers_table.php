<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_answers'))
        {
            Schema::create('user_answers', function (Blueprint $table) {
                $table->increments('id_user_answer');
                $table->integer('id_user')->unsigned();
                $table->integer('id_quiz_question')->unsigned();
                $table->integer('id_answer_option')->nullable()->unsigned();
                $table->text('user_answer_description')->nullable();
                $table->integer('mark_obtain')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users');
                
                //Add foreign key constrain in id_answer_option column
                $table->foreign('id_answer_option')
                        ->references('id_answer_option')->on('answer_options');
                
                //Add foreign key constrain in id_quiz_question column
                $table->foreign('id_quiz_question')
                        ->references('id_quiz_question')->on('quiz_questions');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_answers'))
        {
            Schema::drop('user_answers'); 
        }
    }
}
