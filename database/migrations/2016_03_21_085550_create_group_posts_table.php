<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_posts'))
        {
            Schema::create('group_posts', function (Blueprint $table) {
                $table->increments('id_group_post');
                
                $table->integer('id_group')->unsigned()->index();
                $table->integer('id_post')->unsigned()->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_group column
                $table->foreign('id_group')
                        ->references('id_group')->on('groups')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_post column
                $table->foreign('id_post')
                        ->references('id_post')->on('posts')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_posts'))
        {
            Schema::drop('group_posts');
        }
    }
}
