<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserWorkExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_work_experience'))
        {
            Schema::create('user_work_experience', function (Blueprint $table) {
                $table->increments('id_user_work_experience');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('company_name');
                $table->string('job_title');
                $table->text('job_description')->nullable();
                $table->date('start_year');
                $table->date('end_year')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_work_experience'))
        {
            Schema::drop('user_work_experience');
        }
    }
}
