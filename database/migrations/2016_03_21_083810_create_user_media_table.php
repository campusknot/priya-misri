<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_media'))
        {
            Schema::create('user_media', function (Blueprint $table) {
                $table->increments('id_user_media');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('file_name');
                $table->enum('file_type', array('I', 'V'))->comment('I = image, V = video');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_media'))
        {
            Schema::drop('user_media');
        }
    }
}
