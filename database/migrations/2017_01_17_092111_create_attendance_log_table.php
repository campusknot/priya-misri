<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('attendance_log'))
        {
            Schema::create('attendance_log', function (Blueprint $table) {
                $table->increments('id_attendance_log');
                
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_user_attendance')->unsigned()->index();
                $table->enum('change_from', array('P','A','SL'));
                $table->enum('change_to', array('P','A','SL'));
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                $table->foreign('id_user_attendance')
                        ->references('id_user_attendance')->on('user_attendance')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('attendance_log'))
        {
            Schema::drop('attendance_log');
        }
    }
}
