<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterEventTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('events'))
        {
            Schema::table('events', function($table) {
                $table->string('id_external_event')->nullable()->after('id_user');
                $table->string('id_calendar')->nullable()->after('id_external_event');
                $table->string('calendar_name')->nullable()->after('id_calendar');
                $table->enum('event_from', array('ck', 'g'))->default('ck')->after('longitude');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}