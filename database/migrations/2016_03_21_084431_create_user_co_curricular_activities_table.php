<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserCoCurricularActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_co_curricular_activities'))
        {
            Schema::create('user_co_curricular_activities', function (Blueprint $table) {
                $table->increments('id_user_co_curricular_activity');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('committee_name');
                $table->string('post');
                $table->date('start_year');
                $table->date('end_year')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_co_curricular_activities'))
        {
            Schema::drop('user_co_curricular_activities');
        }
    }
}
