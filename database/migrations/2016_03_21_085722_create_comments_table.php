<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('comments'))
        {
            Schema::create('comments', function (Blueprint $table) {
                $table->increments('id_comment');
                
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_entity');
                $table->enum('entity_type', array('P', 'E'))->comment('P = post, E = event');
                $table->text('comment_text')->nullable();
                $table->enum('comment_type', array('T', 'I', 'V', 'D', 'C'))->comment('T = text, I = image, V = video, D = document, C = code');
                $table->string('display_file_name')->nullable();
                $table->string('file_name')->nullable();
                $table->enum('file_type', array('I', 'V', 'D'))->nullable()->comment('I = image, V = video, D = document');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('comments'))
        {
            Schema::drop('comments');
        }
    }
}
