<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGoogleApiAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasTable('google_api_access_tokens'))
        {
            Schema::create('google_api_access_tokens', function (Blueprint $table) {
                $table->increments('id_google_api_access_token');
                
                $table->integer('id_user')->unsigned()->index();
                $table->text('access_token');
                $table->text('refresh_token');
                $table->string('token_type',20);
                $table->integer('created');
                $table->integer('expires_in');
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasTable('google_api_access_tokens'))
        {
            Schema::drop('google_api_access_tokens');
        }
    }
}