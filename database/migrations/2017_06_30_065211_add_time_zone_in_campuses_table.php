<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTimeZoneInCampusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('campuses'))
        {
            Schema::table('campuses', function($table) {
                $table->string('timezone')->nullable()->after('country_name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('campuses'))
        {
            Schema::table('campuses', function ($table) {
                $table->dropColumn('timezone');
            });
        }
    }
}
