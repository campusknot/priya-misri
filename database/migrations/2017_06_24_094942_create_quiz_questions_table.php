<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('quiz_questions'))
        {
            Schema::create('quiz_questions', function (Blueprint $table) {
                $table->increments('id_quiz_question');
                $table->integer('id_quiz')->unsigned();
                $table->text('question_text');
                $table->enum('question_type',array('O','M'))->comment('O => open ended , M => Multiple choise ');
                $table->string('file_name');
                $table->integer('marks');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_quiz column
                $table->foreign('id_quiz')
                        ->references('id_quiz')->on('quizzes');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('quiz_questions'))
        {
            Schema::drop('quiz_questions'); 
        }
    }
}
