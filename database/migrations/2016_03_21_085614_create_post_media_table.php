<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('post_media'))
        {
            Schema::create('post_media', function (Blueprint $table) {
                $table->increments('id_post_media');
                
                $table->integer('id_post')->unsigned()->index();
                $table->string('display_file_name');
                $table->string('file_name');
                $table->enum('media_type', array('I', 'V', 'D'))->comment('I = image, V = video, D = document');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_post column
                $table->foreign('id_post')
                        ->references('id_post')->on('posts')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('post_media'))
        {
            Schema::drop('post_media');
        }
    }
}
