<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('api_logs'))
        {
            Schema::create('api_logs', function (Blueprint $table) {
                $table->increments('id_api_log');

                $table->text('server_params');
                $table->text('request_params');
                $table->text('response_params');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('api_logs'))
        {
            Schema::drop('api_logs');
        }
    }
}
