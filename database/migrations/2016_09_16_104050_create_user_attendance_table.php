<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAttendanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_attendance'))
        {
            Schema::create('user_attendance', function (Blueprint $table) {
                $table->increments('id_user_attendance');

                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_lecture')->unsigned()->index();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_course column
                $table->foreign('id_lecture')
                        ->references('id_lecture')->on('lectures')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_attendance'))
        {
            Schema::drop('user_attendance');
        }
    }
}
