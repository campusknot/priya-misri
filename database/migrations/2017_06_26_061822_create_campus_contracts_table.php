<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('campus_contracts'))
        {
            Schema::create('campus_contracts', function (Blueprint $table) {
                $table->increments('id_campus_contract');

                $table->integer('id_campus')->unsigned();
                $table->integer('id_university')->unsigned();
                $table->dateTime('contract_start_on');
                $table->dateTime('contract_end_on');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_campus column
                $table->foreign('id_campus')
                        ->references('id_campus')->on('campuses')
                        ->onDelete('cascade');
                
                //Add foreign key constrain in id_university column
                $table->foreign('id_university')
                        ->references('id_university')->on('universities')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('campus_contracts'))
        {
            Schema::drop('campus_contracts');
        }
    }
}
