<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPublicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_publications'))
        {
            Schema::create('user_publications', function (Blueprint $table) {
                $table->increments('id_user_publication');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('publication_title');
                $table->string('publication_url')->nullable();
                $table->string('publication_author');
                $table->text('publication_description')->nullable();
                $table->date('publication_year');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_publications'))
        {
            Schema::drop('user_publications');
        }
    }
}
