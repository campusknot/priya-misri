<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('user_education'))
        {
            Schema::create('user_education', function (Blueprint $table) {
                $table->increments('id_user_education');
                
                $table->integer('id_user')->unsigned()->index();
                $table->string('university');
                $table->string('degree');
                $table->string('major')->nullable();
                $table->string('classification')->nullable();
                $table->date('start_year');
                $table->date('end_year')->nullable();
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('user_education'))
        {
            Schema::drop('user_education');
        }
    }
}
