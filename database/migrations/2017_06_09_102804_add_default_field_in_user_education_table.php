<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultFieldInUserEducationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('user_education'))
        {
            Schema::table('user_education', function($table) {
                $table->tinyInteger('default_education')->default(0)->after('end_year');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('user_education', function ($table) {
            $table->dropColumn('default_education');
        });
        Schema::enableForeignKeyConstraints();
    }
}
