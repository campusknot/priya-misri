<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('polls'))
        {
            Schema::create('polls', function (Blueprint $table) {
                $table->increments('id_poll');

                $table->integer('id_entity')->index();
                $table->enum('entity_type', array('P'))->index()->comment('P = post');
                $table->text('poll_text')->nullable();
                $table->string('file_name')->nullable();
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->tinyInteger('allow_multiple_answers')->default(0)->index();
                $table->enum('poll_type', array('OPN', 'SEC', 'SECD'))->index()->comment('OPN = display result while poll is active, SEC = secret and not display result, SECD = secret and display the result after poll completed.');
                $table->tinyInteger('activated')->default(1)->index();
                $table->tinyInteger('deleted')->default(0)->index();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('polls'))
        {
            Schema::drop('polls');
        }
    }
}
