<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGroupAttendanceLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('group_attendance_logs'))
        {
            Schema::create('group_attendance_logs', function (Blueprint $table) {
                $table->increments('id_group_attendance_log');
                
                $table->integer('id_user')->unsigned()->index();
                $table->integer('id_user_group_attendance')->unsigned()->index();
                $table->enum('change_from', array('P','A','SL'));
                $table->enum('change_to', array('P','A','SL'));
                
                $table->timestamps();
                
                //Add foreign key constrain in id_user column
                $table->foreign('id_user')
                        ->references('id_user')->on('users')
                        ->onDelete('cascade');
                
                $table->foreign('id_user_group_attendance')
                        ->references('id_user_group_attendance')->on('user_group_attendance')
                        ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('group_attendance_logs'))
        {
            Schema::drop('group_attendance_logs');
        }
    }
}
