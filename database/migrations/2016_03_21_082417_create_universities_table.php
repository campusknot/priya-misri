<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUniversitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('universities'))
        {
            Schema::create('universities', function (Blueprint $table) {
                $table->increments('id_university');
                $table->string('university_name');
                $table->string('university_url');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('universities'))
        {
            Schema::drop('universities');
        }
    }
}
