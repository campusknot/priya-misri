<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['sync_api']], function() {
    
    Route::match(['get','post'],'/user/sync-user-data', 'UserController@callSyncUserData');
    Route::match(['get','post'],'/user/sync-course-data', 'UserController@callSyncCourseData');
});

Route::group(['middleware' => ['api']], function() {
    Route::match(['get'],'/', 'HomeController@callIndex');
    Route::match(['get'],'/home', ['as' => 'home.index', 'uses' =>'HomeController@callHome']);
    
    Route::match(['get', 'post'],'/media-parser', 'HomeController@callMediaParser');
    
    //User routes
    Route::match(['get','post'],'/campus-listing', 'UserController@callCampusListing');
    Route::match(['get','post'],'/signup', 'UserController@callUserRegistration');
    Route::match(['get','post'],'/resend-verification', 'UserController@callResendVerificationMail');
    Route::match(['get','post'],'/login', 'UserController@callLogin');
    Route::match(['get','post'],'/auto-login', 'UserController@callAutoLogin');
    Route::match(['get','post'],'/logout', 'UserController@callLogout');
    Route::match(['get','post'],'/forgot-password', 'UserController@callForgotPassword');
    Route::match(['get','post'],'/user/user-auth-data', 'UserController@callUserAuthData');
    Route::match(['get','post'],'/user/feeds', 'UserController@callUserFeeds');
    Route::match(['get','post'],'/user/polls', 'UserController@callUserAllPolls');
    Route::match(['get','post'],'/user/user-polls', 'UserController@callUserPolls');
    Route::match(['get','post'],'/user/edit-profile', 'UserController@callEditUserProfile');
    Route::match(['get','post'],'/user/change-password', 'UserController@callChangePassword');
    
    Route::match(['get','post'],'/user/user-followers/{nIdUser?}', 'UserController@callUserFollowers');
    Route::match(['get','post'],'/user/user-followings/{nIdUser?}', 'UserController@callUserFollowings');
    Route::match(['get','post'],'/user/follow-user/{nIdUser}', 'UserController@callFollowUser');
    Route::match(['get','post'],'/user/unfollow-user/{nIdUser}', 'UserController@callUnfollowUser');
    
    //Profile routes
    Route::match(['get','post'],'/user/user-profile/{nIdUser?}', 'UserController@callUserProfile');
    Route::match(['get','post'],'/user/change-user-profile-pic', 'UserController@callChangeUserProfilePic');
    Route::match(['get','post'],'/user/user-education/{nIdUser?}', 'UserController@callUserEducations');
    Route::match(['get','post'],'/user/user-work-experience/{nIdUser?}', 'UserController@callUserWorkExperiences');
    Route::match(['get','post'],'/user/user-organization/{nIdUser?}', 'UserController@callUserOrganizations');
    Route::match(['get','post'],'/user/user-award/{nIdUser?}', 'UserController@callUserAwards');
    Route::match(['get','post'],'/user/user-publication/{nIdUser?}', 'UserController@callUserPublications');
    Route::match(['get','post'],'/user/user-research-work/{nIdUser?}', 'UserController@callUserResearchWorks');
    Route::match(['get','post'],'/user/user-journal/{nIdUser?}', 'UserController@callUserJournals');
    Route::match(['get','post'],'/user/user-add-course', 'UserController@callAddUserCourse');
    
    //Attendance routes
    Route::match(['get','post'],'/user/user-submit-attendance', 'UserController@callSubmitAttendance');
    Route::match(['get','post'],'/user/user-attendance-listing', 'UserController@callStudentAttendanceList');
    Route::match(['get','post'],'/user/user-attendance-detail/{nIdLecture}', 'UserController@callAllAttendanceOfLecture');
    Route::match(['get','post'],'/user/faculty-attendance-listing', 'UserController@callFacultyAttendanceList');
    Route::match(['get','post'],'/user/faculty-attendance-detail/{nIdLecture}', 'UserController@callFacultyAttendanceDetail');
    
    Route::match(['get','post'],'/user/add-report', 'UserController@callAddContentReport');
    
    //User payment
    Route::match(['get','post'],'/user/add-payment-detail', 'UserController@callAddPaymentDetail');
    
    //Group routes
    Route::match(['get','post'],'/group/group-categorylisting', 'GroupController@callGroupCategoryListing');
    Route::match(['get','post'],'/group/add-group', 'GroupController@callAddGroup');
    Route::match(['get','post'],'/group/add-course-group', 'GroupController@callAddCourseGroup');
    Route::match(['get','post'],'/group/all-group-listing', 'GroupController@callAllGroupListing');
    Route::match(['get','post'],'/group/group-search', 'GroupController@callGroupSearch');
    Route::match(['get','post'],'/group/group-listing/{sMemberType?}', 'GroupController@callGroupListing');
    Route::match(['get','post'],'/group/group-feeds/{nIdGroup}', ['as' => 'group.group-feeds', 'uses' => 'GroupController@callGroupFeeds']);
    Route::match(['get','post'],'/group/group-members/{nIdGroup}/{sSearchTerm?}', ['as' => 'group.group-member', 'uses' => 'GroupController@callGroupMembers']);
    Route::match(['get','post'],'/group/group-photos/{nIdGroup}', ['as' => 'group.group-photos', 'uses' => 'GroupController@callGroupPhotos']);
    Route::match(['get','post'],'/group/group-post-documents/{nIdGroup}', ['as' => 'group.group-documents', 'uses' => 'GroupController@callGroupPostDocuments']);
    Route::match(['get','post'],'/group/group-events/{nIdGroup}/{sStartDate?}', ['as' => 'group.group-events', 'uses' => 'GroupController@callGroupEvents']);
    Route::match(['get','post'],'/group/group-polls/{nIdGroup}', ['as' => 'group.group-polls', 'uses' => 'GroupController@callGroupPolls']);
    Route::match(['get','post'],'/group/group-settings/{nIdGroup}', ['as' => 'group.group-settings', 'uses' => 'GroupController@callGroupSettings']);
    Route::match(['get','post'],'/group/group-settings-edit', 'GroupController@callChangeGroupSettings');
    Route::match(['get','post'],'/group/edit-group-image', 'GroupController@callEditGroupImage');
    Route::match(['get','post'],'/group/join-group/{nIdGroup}', 'GroupController@callJoinGroup');
    Route::match(['get','post'],'/group/group-request-response/{nIdGroupRequest}/{sRequestStatus}', 'GroupController@callGroupRequestResponse');
    Route::match(['get','post'],'/group/invite-group', 'GroupController@callInviteGroup');
    Route::match(['get','post'],'/group/group-submit-attendance', 'GroupController@callSubmitGroupAttendance');
    Route::match(['get','post'],'/group/group-attendance-listing', 'GroupController@callGroupAttendanceListing');
    Route::match(['get','post'],'/group/group-attendance-detail/{nIdGroupLecture}', 'GroupController@callGroupAttendanceDetail');
    Route::match(['get','post'],'/group/group-attendance-admin-detail/{nIdGroupLecture}', 'GroupController@callGroupAttendanceAdminDetail');
    
    //Post routes
    Route::match(['get','post'],'/post/add-post', 'PostController@callAddPost');
    Route::match(['get','post'],'/post/post-detail/{nIdPost}', 'PostController@callPostDetail');
    Route::match(['get','post'],'/post/add-post-comment', 'PostController@callAddPostComment');
    Route::match(['get','post'],'/post/delete-post/{nIdPost}', 'PostController@callDeletePost');
    Route::match(['get','post'],'/post/delete-post-comment/{nIdPostComment}', 'PostController@callDeletePostComment');
    Route::match(['get','post'],'/post/post-like/{nIdPost}', 'PostController@callPostLike');
    Route::match(['get','post'],'/post/poll-answer', 'PostController@callAddPollAnswer');
    Route::match(['get','post'],'/post/activate-poll/{nIdPoll}', 'PostController@callActivatePoll');
    //Event routes
    Route::match(['get','post'],'/event/add-event', 'EventController@callAddEvent');
    Route::match(['get','post'],'/event/event-listing/{sStartDate?}', 'EventController@callEventListing');
    Route::match(['get','post'],'/event/event-detail/{nIdEvent}', 'EventController@callEventDetail');
    Route::match(['get','post'],'/event/event-response/{nIdEvent}/{sStatus}', 'EventController@callEventResponse');
    Route::match(['get','post'],'/event/event-comments/{nIdEvent}', 'EventController@callEventComments');
    Route::match(['get','post'],'/event/add-event-comment', 'EventController@callAddEventComment');
    Route::match(['get','post'],'/event/event-responded-users/{nIdEvent}/{sResponseType}', 'EventController@callEventRespondedUsers');
    
    //Notification routes
    Route::match(['get','post'],'/notification/notification-listing', 'NotificationController@callNotificationListing');
    Route::match(['get','post'],'/notification/group-notification-listing', 'NotificationController@callGroupNotification');
    Route::match(['get','post'],'/notification/event-notification-listing', 'NotificationController@callEventNotification');
    Route::match(['get','post'],'/notification/other-notification-listing', 'NotificationController@callOtherNotification');
    
    //Document routes
    Route::match(['get','post'],'/document/document-listing', 'DocumentController@callDocumentListing');
    Route::match(['get','post'],'/document/child-document-listing/{nIdDocument}', 'DocumentController@callChildDocumentListing');
    Route::match(['get','post'],'/document/shared-document-listing', 'DocumentController@callSharedDocument');
    Route::match(['get','post'],'/document/group-document-listing/{nIdGroup}', 'DocumentController@callGroupDocument');
    Route::match(['get','post'],'/document/delete-document/{nIdDocument}', 'DocumentController@callDeleteDocument');
    Route::match(['get','post'],'/document/share-document', 'DocumentController@callDocumentShare');
    Route::match(['get','post'],'/document/post-document', 'DocumentController@callPostDocumentListing');
    Route::match(['get','post'],'/document/copy-document', 'DocumentController@callCopyDocument');
    
    //Quiz routes
    Route::match(['get','post'],'/quiz/member-quiz-listing', 'QuizController@callMemberQuizList');
    Route::match(['get','post'],'/quiz/member-quiz-detail{nIdQuizUserInvite}', 'QuizController@callMemberQuizDetail');
    Route::match(['get','post'],'/quiz/quiz-questions/{nIdQuizUserInvite}', 'QuizController@callQuizQuestions');
    Route::match(['get','post'],'/quiz/add-answer', 'QuizController@callAddStudentAnswer');
    Route::match(['get','post'],'/quiz/start-quiz/{nIdQuizUserInvite}', 'QuizController@callStartQuiz');
    Route::match(['get','post'],'/quiz/end-quiz/{nIdQuizUserInvite}', 'QuizController@callEndQuiz');
});