<?php

namespace App\Http\Api\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Libraries\ApiLog;

use Illuminate\Http\Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $bDebug = TRUE;
    protected $nUpdateRequired = 1; //0=not_require, 1=update_required, 2=update_optional
    protected $nLatestVersion = 2.10;
    protected $nLatestIOSVersion = 2.10;
    protected $nLatestAndroidVersion = 2.9;
    protected $oApiLog;
    protected $sResponse;
    protected $aAllowedAgents = ["iOS-app", "Android-app"];
    
    public function __construct(Request $oRequest)
    {
        if($this->bDebug)
        {
            $this->oApiLog = ApiLog::create([
                                    'server_params' => json_encode($_SERVER),
                                    'request_params' => json_encode($_REQUEST),
                                    'response_params' => 'response'
                                ]);
        }
        if(!in_array(base64_decode($oRequest->header('User-Agent')), $this->aAllowedAgents))
        {
            exit('You are not allowed to access this api.');
    }
    }
    
    protected function prepareResponse($aResponse)
    {
        $aResponse['update_required'] = $this->nUpdateRequired;
        $aResponse['latest_version'] = $this->nLatestVersion;
        $aResponse['latest_ios_version'] = $this->nLatestIOSVersion;
        $aResponse['latest_android_version'] = $this->nLatestAndroidVersion;
        $this->sResponse = json_encode($aResponse);
        return $aResponse;
    }

    public function __destruct()
    {
        if($this->bDebug)
        {
            $this->oApiLog->response_params = $this->sResponse;
            $this->oApiLog->update();
        }
    }
}
