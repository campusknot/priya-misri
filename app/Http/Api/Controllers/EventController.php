<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Controllers\Controller;

use App\Libraries\Event;
use App\Libraries\EventRequest;
use App\Libraries\Comment;
use App\Libraries\Notification;
use App\Libraries\GroupMember;
use App\Libraries\GroupEvent;
use App\Libraries\GoogleApiAccessToken;

use App\Jobs\SendEventInvitation;
use App\Jobs\SyncGoogleCalendarData;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
        $this->middleware('auth', ['only' => [
                                                "callAddEvent",
                                                "callEventListing", "callEventDetail",
                                                "callEventResponse", "callEventComments",
                                                "callAddEventComment", "callEventRespondedUsers"
                                            ]
                                    ]);
    }
    
    public function callAddEvent(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $dToday = Carbon::today();
            $sToday = $dToday->toDateTimeString();
            
            $aValidationRequiredFor = [
                                        'event_title' => 'required',
                                        'start_date' => 'required|date|after:"'.$sToday.'"',
                                        'end_date' => 'sometimes|date|after:"'.$oRequest->start_date.'"',
                                        'address' => 'required',
                                        'latitude' => 'required',
                                        'longitude' => 'required'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_event';
                $aMessages['response_message'] = 'add_event_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $dStartDate = Carbon::createFromFormat("Y-m-d H:i:s", $oRequest->start_date, Auth::user()->timezone);
            $dStartDate->setTimezone('UTC');
            $sStartDate = $dStartDate->toDateTimeString();
            
            $sEndDate = NULL;
            if(!empty($oRequest->end_date))
            {
                $dEndDate = Carbon::createFromFormat("Y-m-d H:i:s", $oRequest->end_date, Auth::user()->timezone);
                $dEndDate->setTimezone('UTC');
                $sEndDate = $dEndDate->toDateTimeString();
            }
            
            $oEvent = Event::create([
                                        'id_user' => Auth::user()->id_user,
                                        'event_title' => trim($oRequest->event_title),
                                        'event_description' => trim($oRequest->event_description),
                                        'start_date' => $sStartDate,
                                        'end_date' => $sEndDate,
                                        'room_number' => trim($oRequest->room_number),
                                        'address' => trim($oRequest->address),
                                        'latitude' => trim($oRequest->latitude),
                                        'longitude' => trim($oRequest->longitude)
                                    ]);
            
            EventRequest::create([
                                    'id_event' => $oEvent->id_event,
                                    'id_user_request_from' => Auth::user()->id_user,
                                    'id_user_request_to' => Auth::user()->id_user,
                                    'status' => config('constants.EVENTSTATUSGOING')
                                ]);
            
            $aEventInvities = array();
            if($oRequest->invite_id_users)
            {
                $aInvitedUsers = explode(',',$oRequest->invite_id_users);
                foreach ($aInvitedUsers as $nIdUser)
                {
                    $aEventInvities[] = 'U_'.$nIdUser;
                }
            }
            
            if($oRequest->invite_id_groups)
            {
                $aInvitedGroups = explode(',',$oRequest->invite_id_groups);
                foreach ($aInvitedGroups as $nIdGroup)
                {
                    $aEventInvities[] = 'GM_'.$nIdGroup;
                    $this->addGroupEvent($oEvent->id_event, $nIdGroup);
                }
            }
            
            if(count($aEventInvities))
            {
                $this->dispatch(new SendEventInvitation($aEventInvities, $oEvent->id_event, Auth::user()->id_user));
            }
            $aMessages['data'] = $oEvent;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_event';
            $aMessages['response_message'] = 'add_event_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_event';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    private function addGroupEvent($nIdEvent, $nIdGroup)
    {
        $aGroupEvent = array();
        $aGroupEvent['id_event'] = $nIdEvent;
        $aGroupEvent['id_group'] = $nIdGroup;

        GroupEvent::firstOrCreate($aGroupEvent);
    }
    
    public function callEventListing(Request $oRequest, $sStartDate = '')
    {
        if($oRequest->isMethod("GET"))
        {
            if(empty($sStartDate))
            {
                $dStartDate = Carbon::today(Auth::user()->timezone);
                $dStartDate->setTimezone('UTC');
                $sStartDate = $dStartDate->toDateTimeString();
            }
            else
            {
                $aStartDateData = explode("+",$sStartDate);
                $dStartDate = Carbon::createFromFormat("Y-m-d H:i:s", trim($aStartDateData[0]), Auth::user()->timezone);
                $dStartDate->startOfDay();
                $dStartDate->setTimezone('UTC');
                $sStartDate = $dStartDate->toDateTimeString();
            }
            // Google calendar data api
            $oGoogleApiAccessTocken = GoogleApiAccessToken::where('id_user' , Auth::user()->id_user)
                                                            ->first();
            if(count($oGoogleApiAccessTocken))
            {
                $dTimeMin = Carbon::createFromFormat('Y-m-d', $dStartDate->toDateString());
                $sTimeMin = $dTimeMin->toAtomString(); //'2016-11-12T00:00:00-00:00'

                $dTimeMax = Carbon::now()->addMonths(12);
                $sTimeMax = $dTimeMax->toAtomString(); //'2017-01-11T00:00:00-00:00'

                $this->dispatch(new SyncGoogleCalendarData(Auth::user()->id_user,$sTimeMin,$sTimeMax));
            }
            
            $oGroupIds = GroupMember::getUserGroupIds(Auth::user()->id_user);
            $aGroupIds = explode(",", $oGroupIds[0]->id_groups);
            
            $oEventList = Event::getUserEventList(Auth::user()->id_user, $sStartDate);
            
            $nCount = 0;
            foreach ($oEventList as $oEvent)
            {
                $oEventGroups = GroupEvent::getEventGroups($oEvent->id_event, $aGroupIds);
                $oEvent->id_groups = $oEventGroups[0]->id_groups;
                $oEvent->group_names = $oEventGroups[0]->group_names;
                
                $oEventList[$nCount] = $oEvent;
                $nCount ++;
            }
            
            $aMessages['data'] = $oEventList;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'event_listing';
            $aMessages['response_message'] = 'event_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'event_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callEventDetail(Request $oRequest, $nIdEvent)
    {
        if($oRequest->isMethod("GET"))
        {
            $oEventDetail = Event::getEventDetail($nIdEvent);
            
            if(empty($oEventDetail))
            {
                $aMessages['errors'] = array('event_detail' => trans('messages.event_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'event_detail';
                $aMessages['response_message'] = 'event_detail_fail';

                return $this->prepareResponse($aMessages);
            }
            
            if($oEventDetail->user_profile_image != null)
                    $oEventDetail->user_profile_image = getUserImageUrl ($oEventDetail->user_profile_image, 50);
            
            $aMessages['data'] = $oEventDetail;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'event_detail';
            $aMessages['response_message'] = 'event_detail_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'event_detail';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callEventResponse(Request $oRequest, $nIdEvent, $sStatus)
    {
        if($oRequest->isMethod("GET"))
        {
            $oEventRequest = EventRequest::where('id_event', '=', $nIdEvent)
                                            ->where('id_user_request_to', '=', Auth::user()->id_user)
                                            ->first();
            
            if($oEventRequest === NULL)
            {
                $aMessages['errors'] = array('request_not_found' =>[trans('messages.event_request_not_found')]);
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'event_response';
                $aMessages['response_message'] = 'event_response_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oEventRequest->status = "".$sStatus."";
            $oEventRequest->update();
            
            $aMessages['data'] = ['success_message' => trans('messages.event_response_registered')];
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'event_response';
            $aMessages['response_message'] = 'event_response_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'event_response';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }

    public function callEventComments(Request $oRequest, $nIdEvent)
    {
        if($oRequest->isMethod("GET"))
        {
            $oEventComments = Comment::getEntityCommentsWithPagination($nIdEvent, 'E');
            
            $aMessages['data'] = $oEventComments;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'event_feeds_listing';
            $aMessages['response_message'] = 'event_feeds_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'event_feeds_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddEventComment(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'comment_type' => 'required',
                                        'id_event' => 'required'
                                        ];
            
            if(!empty($oRequest->comment_type))
            {
                switch ($oRequest->comment_type)
                {
                    case config('constants.POSTTYPEIMAGE'):
                        $aValidationRequiredFor['file'] = 'required|image|mimes:bmp,jpg,jpeg,png,svg|max:10000';
                        break;
                    case config('constants.POSTTYPEVIDEO'):
                        $aValidationRequiredFor['file'] = 'required|video|mimes:mov,mp4,avi,3gp,mkv|max:100000';
                        break;
                    case config('constants.POSTTYPEDOCUMENT'):
                        $aValidationRequiredFor['file'] = 'required|mimes:doc,docx,pages,rtf,txt,wp,numbers,xls,xlsx,key,ppt,pps,pdf,zip,rar|max:100000';
                        break;
                    default :
                        //Code for text/code
                        $aValidationRequiredFor['comment_text'] = 'required';
                }
            }
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_event_comment';
                $aMessages['response_message'] = 'add_event_comment_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $aRequestParams = $oRequest->all();
            $aInsertValues = [
                                'id_user' => Auth::user()->id_user,
                                'id_entity' => $aRequestParams['id_event'],
                                'entity_type' => 'E',
                                'comment_text' => $aRequestParams['comment_text'],
                                'comment_type' => $aRequestParams['comment_type'],
                                'activated' => 1,
                                'deleted' => 0
                            ];
            
            if($aRequestParams['comment_type'] != config('constants.POSTTYPETEXT')
                    && $aRequestParams['comment_type'] != config('constants.POSTTYPECODE'))
            {
                //Upload file in upload folder
                //And make entry in post_media table
                $oUploadedFile = $oRequest->file('file');
                
                $sOriginalName = $oUploadedFile->getClientOriginalName();
                $sDestinationPath = base_path().'/uploads'; // upload path
                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
                $oUploadedFile->move($sDestinationPath, $sFileName); // uploading file to given path
                
                $aInsertValues['display_file_name'] = $sOriginalName;
                $aInsertValues['file_name'] = $sFileName;
                $aInsertValues['file_type'] = $aRequestParams['comment_type'];
            }
            $oEventComment = Comment::create($aInsertValues);
            
            //Add notification for event creator
            $oEvent = Event::find($oRequest->id_event);
            
            Notification::create([
                                    'id_user' => $oEvent->id_user,
                                    'id_entites' => Auth::user()->id_user.'_'.$oRequest->id_event,
                                    'entity_type_pattern' => 'U_E', //U = user, P = post
                                    'notification_type' => config('constants.NOTIFICATIONCOMMENTONEVENT')
                                ]);
            
            $aMessages['data'] = $oEventComment;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_event_comment';
            $aMessages['response_message'] = 'add_event_comment_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_event_comment';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }

    public function callEventRespondedUsers(Request $oRequest, $nIdEvent, $sResponseType)
    {
        if($oRequest->isMethod("GET"))
        {
            $oEventUserList = EventRequest::getRespondedUserList($nIdEvent, array($sResponseType));
            
            foreach($oEventUserList as $sKey=>$oEventUser)
            {
                if($oEventUser->user_profile_image != NULL)
                    $oEventUserList[$sKey]->user_profile_image = getUserImageUrl ($oEventUser->user_profile_image, 50);
            }
            
            $aMessages['data'] = $oEventUserList;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'event_user_list';
            $aMessages['response_message'] = 'event_user_list_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'event_user_list';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }    
}
