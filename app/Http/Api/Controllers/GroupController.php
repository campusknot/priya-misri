<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Controllers\Controller;

use App\Libraries\Group;
use App\Libraries\GroupRequest;
use App\Libraries\GroupCategory;
use App\Libraries\GroupMember;
use App\Libraries\GroupPost;
use App\Libraries\GroupEvent;
use App\Libraries\GroupLecture;
use App\Libraries\EventRequest;
use App\Libraries\Poll;
use App\Libraries\PollOption;
use App\Libraries\PollAnswer;
use App\Libraries\UserEducation;
use App\Libraries\UserGroupAttendance;
use App\Libraries\AttendanceCode;

use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Jobs\SyncUserGroups;
use App\Jobs\SendGroupInvitation;
use App\Jobs\ShareGroupDocumentsWithNewMember;
use App\Jobs\ShareUpcomingQuizWithNewGroupMember;

class GroupController extends Controller
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
        $this->middleware('auth', ['only' => [
                                                "callGroupCategoryListing",
                                                "callAddGroup", "callAddCourseGroup",
                                                "callAllGroupListing",
                                                "callGroupSearch", "callGroupListing",
                                                "callGroupFeeds", "callGroupMembers",
                                                "callGroupPhotos", "callGroupPostDocuments",
                                                "callGroupEvents", "callGroupPolls",
                                                "callGroupSettings",
                                                "callChangeGroupSettings", "callJoinGroup",
                                                "callGroupRequestResponse", "callInviteGroup",
                                                "callEditGroupImage", "callSubmitGroupAttendance",
                                                "callGroupAttendanceListing", "callGroupAttendanceDetail",
                                                "callGroupAttendanceAdminDetail"
                                            ]
                                    ]);
    }
    
    public function callGroupCategoryListing(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $aGroupCategories = GroupCategory::whereNotIn('id_group_category',array(config('constants.UNIVERSITYGROUPCATEGORY'),config('constants.COURSEGROUPCATEGORY')))
                                                ->where('activated','=',1)
                                                ->where('deleted','=',0)
                                                ->get();
            
            $aMessages['data'] = $aGroupCategories;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_category_listing';
            $aMessages['response_message'] = 'group_category_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_category_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddGroup(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                            'group_category' => 'required',
                                                            'group_name' => 'required|max:128',
                                                            'group_type' => 'required'
                                                ]);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_group';
                $aMessages['response_message'] = 'add_group_fail';
                
                return $this->prepareResponse($aMessages);
            }
            $oCreatedGroup = Group::create([
                                                'id_group_category' => $oRequest->group_category,
                                                'group_name' => $oRequest->group_name,
                                                'group_type' => $oRequest->group_type,
                                                'id_user' => Auth::user()->id_user,
                                                'id_campus' => Auth()->user()->id_campus
                                            ]);
            
            GroupMember::create([
                                'id_group' => $oCreatedGroup->id_group,
                                'id_user' => Auth::user()->id_user,
                                'member_type' => config('constants.GROUPCREATOR')
                            ]);
            
            $aMessages['data'] = $oCreatedGroup;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_group';
            $aMessages['response_message'] = 'add_group_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_group';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddCourseGroup(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                    'group_name' => 'required',
                                                    'section_name' => 'required|max:4'
                                        ]);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_course_group';
                $aMessages['response_message'] = 'add_course_group_fail';
                
                return $this->prepareResponse($aMessages);
            }
            $oCreatedGroup = Group::create([
                                                'id_group_category' => config('constants.COURSEGROUPCATEGORY'),
                                                'group_name' => $oRequest->group_name.' Section - '.$oRequest->section_name,
                                                'group_type' => config('constants.SECRETGROUP'),
                                                'id_user' => Auth()->user()->id_user,
                                                'id_campus' => Auth()->user()->id_campus
                                            ]);
            GroupMember::create([
                                'id_group' => $oCreatedGroup->id_group,
                                'id_user' => Auth()->user()->id_user,
                                'member_type' => config('constants.GROUPCREATOR'),
                            ]);
            
            $aMessages['data'] = $oCreatedGroup;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_course_group';
            $aMessages['response_message'] = 'add_course_group_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_course_group';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupSearch(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            //Get user groups
            $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
            $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
        
            $aSearchParam['search_term'] = isset($oRequest->search_param) ? $oRequest->search_param : '';
            $aSearchParam['id_group_category'] = isset($oRequest->id_group_category) ? $oRequest->id_group_category : '';
            $aSearchParam['id_campus'] = Auth::user()->id_campus;
            $aSearchParam['user_groups'] = $aUserGroups;
            
            $oGroupList = Group::getGroupSearch($aSearchParam);
            foreach($oGroupList as $sKey=>$oGroup)
            {
                if($oGroup->group_image != NULL)
                    $oGroupList[$sKey]->group_image = getGroupImageUrl ($oGroup->group_image, 100);
            }
            
            $aMessages['data'] = $oGroupList;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'recommended_group_list';
            $aMessages['response_message'] = 'recommended_group_list_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'recommended_group_list';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAllGroupListing(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroupList = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            foreach($oGroupList as $sKey=>$oGroup)
            {
                if($oGroup->group_image != NULL)
                    $oGroupList[$sKey]->group_image = getGroupImageUrl ($oGroup->group_image, 50);
            }
            
            $aMessages['data'] = $oGroupList;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'all_group_listing';
            $aMessages['response_message'] = 'all_group_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'all_group_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }

    public function callGroupListing(Request $oRequest, $sMemberType = '')
    {
        $sRequestType = ($sMemberType == config('constants.GROUPMEMBER')) ? 'member_group_listing' : 
                        (($sMemberType == config('constants.GROUPADMIN')) ? 'admin_group_listing' : 'group_listing');
        
        if($oRequest->isMethod("GET"))
        {
            $aOptionalConditions = array();
            $aOptionalConditions['activated'] = 1;
            $aOptionalConditions['member_types'] = ($sMemberType == config('constants.GROUPMEMBER')) ? [config('constants.GROUPMEMBER')] : 
                                                    (($sMemberType == config('constants.GROUPADMIN')) ? [config('constants.GROUPCREATOR'), config('constants.GROUPADMIN')] : 
                                                        [config('constants.GROUPCREATOR'), config('constants.GROUPADMIN'),config('constants.GROUPMEMBER')]);
            
            $oGroupList = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions);
            foreach($oGroupList as $sKey=>$oGroup)
            {
                if($oGroup->group_image != NULL)
                    $oGroupList[$sKey]->group_image = getGroupImageUrl ($oGroup->group_image, 100);
            }
            
            $aMessages['data'] = $oGroupList;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = $sRequestType;
            $aMessages['response_message'] = $sRequestType.'_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = $sRequestType;
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupFeeds(Request $oRequest, $nIdGroup)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_feeds';
                $aMessages['response_message'] = 'group_feeds_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupFeeds = GroupPost::getGroupPost($nIdGroup);
            
            $nCount = 0;
            foreach ($oGroupFeeds as $oGroupFeed)
            {
                if(!empty($oGroupFeed->user_profile_image))
                    $oGroupFeeds[$nCount]->user_profile_image = getUserImageUrl ($oGroupFeed->user_profile_image, 50);
                
                $oGroupFeeds[$nCount]->image_size = array('width' => 0, 'height' => 0);
                $oGroupFeeds[$nCount]->image_width = 0;
                $oGroupFeeds[$nCount]->image_height = 0;
                
                if(isset($oGroupFeed->file_name) && !empty($oGroupFeed->file_name))
                {
                    $oGroupFeeds[$nCount]->file_name = setPostImage($oGroupFeed->file_name, 80);
                    if($oGroupFeed->post_type == config('constants.POSTTYPEIMAGE'))
                    {
                        $aImageSize = getimagesize($oGroupFeeds[$nCount]->file_name);
                        if(count($aImageSize) > 2)
                        {
                            $oGroupFeeds[$nCount]->image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                            $oGroupFeeds[$nCount]->image_width = $aImageSize[0];
                            $oGroupFeeds[$nCount]->image_height = $aImageSize[1];
                        }
                    }
                }
                $nCount ++;
            }
            
            $aMessages['data'] = $oGroupFeeds;
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_feeds';
            $aMessages['response_message'] = 'group_feeds_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_feeds';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupPolls(Request $oRequest, $nIdGroup)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_polls';
                $aMessages['response_message'] = 'group_polls_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $aUncomplitedPolls = array();
            $aComplitedPolls = array();
            $dToday = Carbon::now();
            $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
            $nLimit = config('constants.PERPAGERECORDS');

            $nUncomplitedPollCount = Poll::getGroupUncomplitedPollCount($nIdGroup, $dToday);
            $nComplitedPollCount = Poll::getGroupComplitedPollCount($nIdGroup, $dToday);

            if ($nUncomplitedPollCount > (($nPageNumber - 1) * $nLimit)) {
                //Get max uncomplited polls
                $oUncomplitedPolls = Poll::getGroupUncomplitedPolls($nIdGroup, $dToday, ($nPageNumber - 1)*$nLimit, $nLimit);
                $aUncomplitedPolls = $oUncomplitedPolls->all();
            }

            //If group requests are less then 10 fetch event requests
            if (($nUncomplitedPollCount + $nComplitedPollCount) > (($nPageNumber - 1) * $nLimit)) {
                if (count($aUncomplitedPolls) < $nLimit) {
                    $nSkipRecords = (($nPageNumber - 1) * $nLimit) - $nUncomplitedPollCount;
                    $nSkipRecords = max(0, $nSkipRecords);

                    $nLimit = config('constants.PERPAGERECORDS') - count($aUncomplitedPolls);
                    $oComplitedPolls = Poll::getGroupAllPolls($nIdGroup, $dToday, $nSkipRecords, $nLimit);
                    $aComplitedPolls = $oComplitedPolls->all();
                }
            }

            $oGroupFeeds = array_merge($aUncomplitedPolls, $aComplitedPolls);
            
            foreach ($oGroupFeeds as $nKey=>$oGroupFeed)
            {
                $oGroupFeeds[$nKey]->poll_expired = false;
                    
                if(strtotime($oGroupFeed->end_time) < time())
                {
                    $oGroupFeeds[$nKey]->poll_expired = true;
                }

                $oPollOptions = PollOption::getPollOptions($oGroupFeed->id_poll);
                $nTotalAnswer = PollAnswer::where('id_poll','=',$oGroupFeed->id_poll)->count();
                $aPollAnswers = PollAnswer::getPollAnswer($oGroupFeed->id_poll)->all();

                $aPollOptionIds = array_map(function($oPollAnswer) {
                                                return intval($oPollAnswer->id_poll_option);
                                            }, $aPollAnswers);

                foreach($oPollOptions as $oPollOption)
                {
                    $nPercent=0;

                    $nPollOptionKey = array_search($oPollOption->id_poll_option, $aPollOptionIds);
                    if(is_int($nPollOptionKey))
                    {
                        $oPollAnswer = $aPollAnswers[$nPollOptionKey];
                        $nPercent = ($oPollAnswer->total * 100) / $nTotalAnswer;
                    }
                    $oGroupFeeds[$nKey]['pollanswer_'.$oPollOption->id_poll_option] = round($nPercent, 2);
                    $oPollOption->pollanswer = round($nPercent, 2);
                }
                $oGroupFeeds[$nKey]->poll_options = $oPollOptions;
                
                if(!empty($oGroupFeed->user_profile_image))
                    $oGroupFeeds[$nKey]->user_profile_image = getUserImageUrl ($oGroupFeed->user_profile_image, 50);
                
                $oGroupFeeds[$nKey]->poll_image_size = array('width' => 0, 'height' => 0);
                if(isset($oGroupFeed->poll_file_name) && !empty($oGroupFeed->poll_file_name))
                {
                    $oGroupFeeds[$nKey]->poll_file_name = setPostImage($oGroupFeed->poll_file_name, 80);
                    $aImageSize = getimagesize($oGroupFeeds[$nKey]->poll_file_name);
                    if(count($aImageSize) > 2)
                    {
                        $oGroupFeeds[$nKey]->poll_image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                    }
                }
            }
            
            $nTotalRecords = $nUncomplitedPollCount + $nComplitedPollCount;
            $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

            $aMessages['data']['total'] = $nTotalRecords;
            $aMessages['data']['per_page'] = config('constants.PERPAGERECORDS');
            $aMessages['data']['current_page'] = $nPageNumber;
            $aMessages['data']['last_page'] = $nLastPage;
            $aMessages['data']['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
            $aMessages['data']['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
            $aMessages['data']['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
            $aMessages['data']['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
            
            $aMessages['data']['data'] = $oGroupFeeds;
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_polls';
            $aMessages['response_message'] = 'group_polls_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_polls';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupMembers(Request $oRequest, $nIdGroup, $sSearchTerm='')
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_members';
                $aMessages['response_message'] = 'group_members_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupMember = new GroupMember();
            $oGroupMemberList = $oGroupMember->getGroupMembers($nIdGroup,array(),Auth::user()->id_user,$sSearchTerm);
            
            foreach ($oGroupMemberList as $key=>$oGroupMember)
            {
                $oGroupMemberList[$key]->major = UserEducation::getMajorEducation($oGroupMember->id_user);
                if($oGroupMember->user_profile_image != null)
                    $oGroupMemberList[$key]->user_profile_image = getUserImageUrl ($oGroupMember->user_profile_image, 50);
            }
            
            $aMessages['data'] = $oGroupMemberList;
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_members';
            $aMessages['response_message'] = 'group_members_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_members';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupPhotos(Request $oRequest, $nIdGroup)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_photos';
                $aMessages['response_message'] = 'group_photos_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupFeeds = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEIMAGE'));
            
            $nCount = 0;
            foreach ($oGroupFeeds as $oGroupFeed)
            {
                if($oGroupFeed->user_profile_image != NULL)
                    $oGroupFeeds[$nCount]->user_profile_image = getUserImageUrl ($oGroupFeed->user_profile_image, 50);
                
                if(isset($oGroupFeed->file_name))
                    $oGroupFeeds[$nCount]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oGroupFeed->file_name;
                
                $nCount ++;
            }
            $aMessages['data'] = $oGroupFeeds;
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_photos';
            $aMessages['response_message'] = 'group_photos_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_photos';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupPostDocuments(Request $oRequest, $nIdGroup)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_documents';
                $aMessages['response_message'] = 'group_documents_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupFeeds = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEDOCUMENT'));
            
            $nCount = 0;
            foreach ($oGroupFeeds as $oGroupFeed)
            {
                if($oGroupFeed->user_profile_image != NULL)
                    $oGroupFeeds[$nCount]->user_profile_image = getUserImageUrl ($oGroupFeed->user_profile_image, 50);
                
                if(isset($oGroupFeed->file_name))
                    $oGroupFeeds[$nCount]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oGroupFeed->file_name;
                
                $nCount ++;
            }
            $aMessages['data'] = $oGroupFeeds;
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_documents';
            $aMessages['response_message'] = 'group_documents_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_documents';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupEvents(Request $oRequest, $nIdGroup, $sStartDate = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_events';
                $aMessages['response_message'] = 'group_events_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupIds = GroupMember::getUserGroupIds(Auth::user()->id_user);
            $aGroupIds = explode(",", $oGroupIds[0]->id_groups);
            
            if(empty($sStartDate))
            {
                $dCurrentdate = Carbon::today(Auth::user()->timezone);
                $dCurrentdate->setTimezone('UTC');
                $sStartDate = $dCurrentdate->toDateTimeString();
            }
            
            $oGroupEvents = GroupEvent::getGroupEvents($nIdGroup, $sStartDate);
            foreach ($oGroupEvents as $key=>$oEvent)
            {
                $oEventGroups = GroupEvent::getEventGroups($oEvent->id_event, $aGroupIds);
                $oEvent->id_groups = $oEventGroups[0]->id_groups;
                $oEvent->group_names = $oEventGroups[0]->group_names;
                
                $oGroupEvents[$key] = $oEvent;
            }
            
            $aMessages['data'] = $oGroupEvents;
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_events';
            $aMessages['response_message'] = 'group_events_success';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_events';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupSettings(Request $oRequest, $nIdGroup)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_settings';
                $aMessages['response_message'] = 'group_settings_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupRequest = GroupRequest::getLoginUserRequest($nIdGroup);
            
            if($oGroupDetails->group_image != NULL)
                $oGroupDetails->group_image = getGroupImageUrl ($oGroupDetails->group_image, 100);
            
            if($oGroupDetails->user_profile_image != NULL)
                $oGroupDetails->user_profile_image = getUserImageUrl ($oGroupDetails->user_profile_image, 50);
            
            $aMessages['data'] = $oGroupDetails;
            $aMessages['group_request'] = $oGroupRequest;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_settings';
            $aMessages['response_message'] = 'group_settings_success';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_documents';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callChangeGroupSettings(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                                'id_group' => 'required',
                                                                'group_name' => 'required|unique:groups,group_name,'.$oRequest->id_group.',id_group',
                                                                'group_type' => 'required',
                                                            ]);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_settings_edit';
                $aMessages['response_message'] = 'group_settings_edit_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oGroupDetails = Group::where('id_group', '=', $oRequest->id_group)
                                    ->where('activated', '=', 1)
                                    ->where('deleted', '=', 0)
                                    ->first();
            
            $oGroupDetails->group_name = $oRequest->group_name;
            $oGroupDetails->group_type = $oRequest->group_type;
            $oGroupDetails->about_group = $oRequest->about_group;
            $oGroupDetails->update();
            
            $aMessages['data'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_settings_edit';
            $aMessages['response_message'] = 'group_settings_edit_success';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_settings_edit';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callJoinGroup(Request $oRequest, $nIdGroup)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetail = $oGroup->getGroupDetail($nIdGroup);

            if($oGroupDetail)
            {
                switch ($oGroupDetail->group_type)
                {
                    case config('constants.SECRETGROUP'):
                    {
                        $aMessages['errors'] = ['group_error' => trans('messages.not_invited_to_join')];
                        $aMessages['response_status'] = config('constants.APIERROR');
                        $aMessages['request_type'] = 'join_group';
                        $aMessages['response_message'] = 'join_group_fail';

                        return $this->prepareResponse($aMessages);
                        break;
                    }
                    case config('constants.PUBLICGROUP'):
                    {
                        $oGroupMember = GroupMember::firstOrNew([
                                                                        'id_group' => $nIdGroup,
                                                                        'id_user' => Auth::user()->id_user
                                                                    ]);
                        
                        $oGroupMember->member_type = config('constants.GROUPMEMBER');
                        $oGroupMember->activated = 1;
                        $oGroupMember->deleted = 0;
                        $oGroupMember->save();
                        
                        $this->dispatch(new SyncUserGroups($oGroupMember->id_user));
                        $this->addAllGroupEventsToNewMember($oGroupMember);
                        $this->dispatch(new ShareGroupDocumentsWithNewMember($oGroupMember->id_user, $nIdGroup));
                        $this->dispatch(new ShareUpcomingQuizWithNewGroupMember($oGroupMember->id_user, $nIdGroup));
                        
                        $aMessages['data'] = ['success_message' => trans('messages.new_member_of_group')];
                        $aMessages['errors'] = array();
                        $aMessages['response_status'] = config('constants.APISUCCESS');
                        $aMessages['request_type'] = 'join_group';
                        $aMessages['response_message'] = 'join_group_success';

                        return $this->prepareResponse($aMessages);
                        break;
                    }
                    case config('constants.PRIVATEGROUP'):
                    {
                        $oGroupRequest = GroupRequest::firstOrNew([
                                                                    'id_group' => $nIdGroup,
                                                                    'id_user_request_from' => Auth::user()->id_user,
                                                                    'id_user_request_to' => $oGroupDetail->id_user,
                                                                    'request_type' => 'J',
                                                                ]);
                        $oGroupRequest->member_status = config('constants.GROUPMEMBERPENDING');
                        $oGroupRequest->save();
                        
                        $aMessages['data'] = ['success_message' => trans('messages.request_registored')];
                        $aMessages['errors'] = array();
                        $aMessages['response_status'] = config('constants.APISUCCESS');
                        $aMessages['request_type'] = 'join_group';
                        $aMessages['response_message'] = 'join_group_success';

                        return $this->prepareResponse($aMessages);
                        break;
                    }
                    default :
                        break;
                }
            }
            
            $aMessages['errors'] = ['group_error' => trans('messages.no_record_found')];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'join_group';
            $aMessages['response_message'] = 'join_group_fail';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'join_group';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupRequestResponse(Request $oRequest, $nIdGroupRequest, $sRequestStatus)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroupRequest = GroupRequest::where('id_group_request', $nIdGroupRequest)
                                            ->first();
            
            if(empty($oGroupRequest))
            {
                $aMessages['errors'] = array('group_request_response'=>trans('messages.group_request_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_request_response';
                $aMessages['response_message'] = 'group_request_response_error';

                return $this->prepareResponse($aMessages);
            }
            $oGroupRequest->member_status = $sRequestStatus;
            $oGroupRequest->save();
            
            if($sRequestStatus == config('constants.GROUPMEMBERACCEPTED'))
            {
                if($oGroupRequest->request_type == config('constants.GROUPREQUESTTYPEINVITE'))
                {
                    $oGroupMember = GroupMember::firstOrNew([
                                                            'id_group' => $oGroupRequest->id_group,
                                                            'id_user' => Auth::user()->id_user
                                                        ]);
                }
                else
                {
                    $oGroupMember = GroupMember::firstOrNew([
                                                            'id_group' => $oGroupRequest->id_group,
                                                            'id_user' => $oGroupRequest->id_user_request_from
                                                        ]);
                }
                
                $oGroupMember->member_type = config('constants.GROUPMEMBER');
                $oGroupMember->activated = 1;
                $oGroupMember->deleted = 0;
                $oGroupMember->save();
                
                $this->dispatch(new SyncUserGroups($oGroupMember->id_user));
                $this->addAllGroupEventsToNewMember($oGroupMember);
                $this->dispatch(new ShareGroupDocumentsWithNewMember($oGroupMember->id_user, $oGroupRequest->id_group));
                $this->dispatch(new ShareUpcomingQuizWithNewGroupMember($oGroupMember->id_user, $oGroupRequest->id_group));
                
                $aMessages['data'] = ['success_message' => trans('messages.new_member_of_group')];
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'group_request_response';
                $aMessages['response_message'] = 'group_request_response_success';

                return $this->prepareResponse($aMessages);
            }
            
            $aMessages['data'] = ['success_message' => trans('messages.group_request_rejected')];
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_request_response';
            $aMessages['response_message'] = 'group_request_response_success';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_request_response';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callInviteGroup(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                                'id_group' => 'required',
                                                                'invite_to' => 'required'
                                                            ]);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'invite_group';
                $aMessages['response_message'] = 'invite_group_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $aErrors = array();
            $aInputs = array();
            
            $aCheckedEmails = $this->checkValidEmails(explode(",", $oRequest->invite_to));
            
            $this->dispatch(new SendGroupInvitation(Auth::user(), $aCheckedEmails['valid_emails'], $oRequest->id_group));
            
            if(count($aCheckedEmails['invalid_email']) || count($aCheckedEmails['out_campus_email']))
            {
                if(count($aCheckedEmails['invalid_email']) && count($aCheckedEmails['out_campus_email']))
                {
                    $aErrors['invite_to'] = trans('messages.invalid_and_out_campus_emails', 
                                                ['in_valid_email' => implode(',', $aCheckedEmails['invalid_email']),
                                                    'out_campus_email' => implode(',', $aCheckedEmails['out_campus_email'])]
                                            );
                }
                else if(count($aCheckedEmails['invalid_email']))
                {
                    $aErrors['invite_to'] = trans('messages.invalid_emails', 
                                                ['in_valid_email' => implode(',', $aCheckedEmails['invalid_email'])]
                                            );
                }
                else
                {
                    $aErrors['invite_to'] = trans('messages.out_campus_emails', 
                                                ['out_campus_email' => implode(',', $aCheckedEmails['out_campus_email'])]
                                            );
                }
                
                $aInputs['invite_to'] = array_merge($aCheckedEmails['invalid_email'], $aCheckedEmails['out_campus_email']);
                
                $aMessages['errors'] = $aErrors;
                $aMessages['response_status'] = config('constants.APIFAIL');
                $aMessages['request_type'] = 'invite_group';
                $aMessages['response_message'] = 'invite_group_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $aMessages['data'] = ['success_message' => trans('messages.invite_group_successful')];
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'invite_group';
            $aMessages['response_message'] = 'invite_group_success';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'invite_group';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    private function checkValidEmails($aEmails)
    {
        $aResponse['valid_emails'] = array();
        $aResponse['invalid_email'] = array();
        $aResponse['out_campus_email'] = array();
            
        $aEmailData = explode("@", Auth::user()->email);
        $sEmailCampus = $aEmailData[1];
        
        foreach ($aEmails as $sEmail)
        {
            $sEmail = trim($sEmail);
            if(!empty($sEmail))
            {
                if(filter_var($sEmail, FILTER_VALIDATE_EMAIL))
                {
                    $aInputEmailData = explode("@", $sEmail);
                    if((mb_strpos($aInputEmailData[1], $sEmailCampus) === FALSE) &&
                            (mb_strpos($sEmailCampus, $aInputEmailData[1]) === FALSE))
                    {
                        $aResponse['out_campus_email'][] = $sEmail;
                    }
                    else {
                        $aResponse['valid_emails'][] = $sEmail;
                    }
                }
                else
                {
                    $aResponse['invalid_email'][] = $sEmail;
                }
            }
        }
        return $aResponse;
    }
    
    private function addAllGroupEventsToNewMember($oGroupMember)
    {
        //Get all events of group.
        $aGroupEvents = GroupEvent::getAllGroupEvents($oGroupMember->id_group);
        
        //Add event request for new group member.
        foreach ($aGroupEvents as $oGroupEvent)
        {
            $oEventRequest = EventRequest::firstOrNew([
                                                        'id_event' => $oGroupEvent->id_event,
                                                        'id_user_request_to' => $oGroupMember->id_user
                                                    ]);
            $oEventRequest->id_user_request_from = $oGroupEvent->id_user;
            $oEventRequest->status = NULL;
            $oEventRequest->save();
        }
    }
    
    /**
     * 
     * @param Request $oRequest
     * @return type
     */
    public function callEditGroupImage(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_group' => 'required',
                                        'file' => 'required|image|mimes:jpg,jpeg,png|max:10000'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'change_group_pic';
                $aMessages['response_message'] = 'change_group_pic_fail';
                
                return $this->prepareResponse($aMessages);
            }
            //Get image file
            $oImageFile = $oRequest->file('file');
            
            $sFileName = time().'.'.$oImageFile->getClientOriginalExtension();
            
            $oImage = Image::make($oImageFile);
            $sImageStream = $oImage->stream();
            
            $oS3 = \Storage::disk('s3');
            $sCloudFilePath = '/'.config('constants.GROUPMEDIAFOLDER').'/' . $sFileName;
            $oS3->put($sCloudFilePath, $sImageStream->__toString(),'public');

            $aImageSizes = array(50,100,150);
            foreach($aImageSizes as $nImageThumbSize)
            {
                $sResizedImageName= $nImageThumbSize.'_'.$sFileName;
                
                $oOriginalImage = Image::make($oImageFile);
                $oResizedImage = $oOriginalImage->resize($nImageThumbSize*2, $nImageThumbSize*2);
                $sResizedImageStream = $oResizedImage->stream();
                
                $oS3 = \Storage::disk('s3');
                $sCloudFilePath = '/'.config('constants.GROUPMEDIAFOLDER').'/' . $sResizedImageName;
                $oS3->put($sCloudFilePath, $sResizedImageStream->__toString(), 'public');
            }
            
            $oGroup = Group::find($oRequest->id_group);
            $oGroup->file_name = $sFileName;
            $oGroup->update();
            
            $aMessages['data'] = ['message' => trans('messages.group_image_change_success')];
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'change_group_pic';
            $aMessages['response_message'] = 'change_group_pic_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'change_group_pic';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    //attendance submit for group all member
    public function callSubmitGroupAttendance(Request $oRequest) 
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_group' => 'required',
                                        'attendance_code' => 'required'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'submit_group_attendance';
                $aMessages['response_message'] = 'submit_group_attendance_fail';

                return $this->prepareResponse($aMessages);
            }

            $oAttendance = AttendanceCode::getGroupAttendanceCodeDetail($oRequest->attendance_code);
            if(count($oAttendance) && $oAttendance->id_group == $oRequest->id_group)
            {
                $oUserGroupAttendance = UserGroupAttendance::firstOrNew([
                                                                        'id_user'=>Auth::user()->id_user,
                                                                        'id_group_lecture'=>$oAttendance->id_lecture
                                                                ]);
                $oUserGroupAttendance->save();

                $aMessages['data'] = ['message' => trans('messages.attendance_submitted')];
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'submit_group_attendance';
                $aMessages['response_message'] = 'submit_group_attendance_success';

                return $this->prepareResponse($aMessages);
            }
            else
            {
                $aMessages['errors'] = ['message' => trans('messages.group_attendance_code_not_valid')];
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'submit_group_attendance';
                $aMessages['response_message'] = 'submit_group_attendance_fail';

                return $this->prepareResponse($aMessages);
            }
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'submit_group_attendance';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupAttendanceListing(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oUserGroups = GroupMember::getUserSelectiveGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'), '!=');
            $aUserGroups = explode(",", $oUserGroups[0]->id_groups);

            $aGroupLectures = GroupLecture::getGroupAttendanceList(Auth::user()->id_user, $aUserGroups);
            foreach($aGroupLectures as $nKey => $oGroupLecture)
            {
                $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
                $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
                $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");
                
                $aGroupLectures[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
                $aGroupLectures[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
                $aGroupLectures[$nKey]['all_lectures_count'] = $nAllLecturesCount;
            }
            $aMessages['data'] = $aGroupLectures;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_attendance_listing';
            $aMessages['response_message'] = 'group_attendance_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_attendance_listing';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupAttendanceDetail(Request $oRequest, $nIdGroupLecture)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroupLecture = GroupLecture::getLectureData($nIdGroupLecture);
            $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oGroupLecture->id_group, Auth::user()->id_user);

            if(count($oGroupMemberDetail))
            {
                $aGroupLectures = GroupLecture::getAllSameLecture($oGroupLecture, Auth::user()->id_user);
                
                $aMessages['data'] = $aGroupLectures;
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'group_attendance_detail';
                $aMessages['response_message'] = 'group_attendance_detail_success';
                
                return $this->prepareResponse($aMessages);
            }
            $aMessages['errors'] = array('not_member'=>trans('messages.not_allowed_to_see_attendance'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'group_attendance_detail';
            $aMessages['response_message'] = 'group_attendance_detail_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_attendance_detail';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupAttendanceAdminDetail(Request $oRequest, $nIdGroupLecture)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroupLecture = GroupLecture::getLectureData($nIdGroupLecture);
            $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oGroupLecture->id_group, Auth::user()->id_user);

            if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != config('constants.GROUPMEMBER'))
            {
                $aFilterParams = ['id_group' => $oGroupLecture->id_group, 'semester' => $oGroupLecture->semester];
                
                $aUserGroupAttendance = UserGroupAttendance::getAllUserAttendanceForGroup($aFilterParams);
                
                foreach($aUserGroupAttendance as $nKey => $oUserGroupAttendance)
                {
                    $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, $oUserGroupAttendance->id_user, config('constants.ATTENDANCETYPEPRESENT'));
                    $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, $oUserGroupAttendance->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
                    $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, $oUserGroupAttendance->id_user, "");

                    $aUserGroupAttendance[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
                    $aUserGroupAttendance[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
                    $aUserGroupAttendance[$nKey]['all_lectures_count'] = $nAllLecturesCount;
                    
                    if($oUserGroupAttendance->user_profile_image != null)
                        $aUserGroupAttendance[$nKey]->user_profile_image = getUserImageUrl ($oUserGroupAttendance->user_profile_image, 50);
                }
                $aMessages['data'] = $aUserGroupAttendance;
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'group_attendance_admin_detail';
                $aMessages['response_message'] = 'group_attendance_admin_detail_success';
                
                return $this->prepareResponse($aMessages);
            }
            $aMessages['errors'] = array('not_member'=>trans('messages.not_allowed_to_see_attendance'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'group_attendance_admin_detail';
            $aMessages['response_message'] = 'group_attendance_admin_detail_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_attendance_admin_detail';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
}
