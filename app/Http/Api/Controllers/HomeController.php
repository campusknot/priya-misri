<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
    }
    
    public function callIndex()
    {
        echo 'Sai-Nath'; exit;
    }
    
    public function callHome()
    {
        $aMessages['errors'] = array('not_login'=>trans('messages.need_to_login'));
        $aMessages['response_status'] = config('constants.APIERROR');
        $aMessages['request_type'] = 'login_required';
        $aMessages['response_message'] = 'login_required';

        return $this->prepareResponse($aMessages);
    }
    
    public function callMediaParser(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $sUrl = $oRequest->url;
            $oValidator = Validator::make($oRequest->all(), [
                                                        'url' => 'url',
                                            ]);
            if ($oValidator->fails()) 
            {
                $aParsedMetaTags["html"] = "";
                $aParsedMetaTags["url"] = $sUrl;
                
                $aMessages['data'] = $aParsedMetaTags;
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'media_parser';
                $aMessages['response_message'] = 'media_parser_fail';

                return $this->prepareResponse($aMessages);
            }
            $oCURL = curl_init();

            curl_setopt_array($oCURL, array(
                                            CURLOPT_RETURNTRANSFER  => 1,
                                            CURLOPT_URL             => "https://noembed.com/embed?url=".$sUrl,
                                            CURLOPT_POST            => 0
                                        )
                            );

            // Send the request & save response to $curl_response
            $sCurlResponse = curl_exec($oCURL);

            // Close request to clear up some resources
            curl_close($oCURL);

            // check if Noembed API has returned error or request itself failed
            if( $sCurlResponse === FALSE || isset(json_decode($sCurlResponse)->error) )
            {
                    // parse meta tags of $url when Noembed fails
                    $oCURL = curl_init();
                    curl_setopt_array($oCURL, array(
                                                    CURLOPT_URL => $sUrl,
                                                    CURLOPT_RETURNTRANSFER => 1,
                                                    CURLOPT_FOLLOWLOCATION => 1,
                                                    CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0'
                                                )
                                    );
                    $sPageHtml = curl_exec($oCURL);
                    curl_close($oCURL);
                    
                    $oDom       = new \DOMDocument();
                    @$oDom      -> loadHTML($sPageHtml);

                    // push all parsed meta tags here
                    $aParsedMetaTags = array();

                    // Loop through all meta tags
                    foreach ( $oDom -> getElementsByTagName("meta") as $oProperties )
                    {
                        $sPropertyAttribute = $oProperties->getAttribute("property");
                        $sNameAttribute     = $oProperties->getAttribute("name");
                        $sContentAttribute  = $oProperties->getAttribute("content");

                        // title
                        if(isset($sPropertyAttribute) && $sPropertyAttribute == "og:title") {
                            $aParsedMetaTags["title"] = $sContentAttribute;
                        }
                        elseif (isset($sNameAttribute) && $sNameAttribute == "title") {
                            $aParsedMetaTags["title"] = $sContentAttribute;
                        }

                        // description
                        if(isset($sPropertyAttribute) && $sPropertyAttribute == "og:description") { 
                            $aParsedMetaTags["description"] = $sContentAttribute;
                        }
                        elseif (isset($sNameAttribute) && $sNameAttribute == "description") {
                            $aParsedMetaTags["description"] = $sContentAttribute;
                        }

                        // thumbnail
                        if(isset($sPropertyAttribute) && $sPropertyAttribute == "og:image") { 
                            $aParsedMetaTags["thumbnail_url"] = $sContentAttribute;
                        }
                        elseif (isset($sNameAttribute) && $sNameAttribute == "thumbnail") {
                            $aParsedMetaTags["thumbnail_url"] = $sContentAttribute;
                        }

                        // type
                        if (isset($sPropertyAttribute) && $sPropertyAttribute == "og:type") {
                            $aParsedMetaTags["type"] = $sContentAttribute;
                        }
                        else {
                            $aParsedMetaTags["type"] = "other";
                        }
                    }
                    
                    $sHtml  = "<a href='".$sUrl."' nofollow  target='_blank'><div class='media_thumbnail'>";
                        $sHtml .= "<img src='";
                        $sHtml .= isset($aParsedMetaTags["thumbnail_url"]) ? ($this->isUrlExist($aParsedMetaTags["thumbnail_url"])) ? $aParsedMetaTags["thumbnail_url"] : asset("assets/web/img/default-thumbnail.png") : asset("assets/web/img/default-thumbnail.png");
                        $sHtml .= "'>";
                        $sHtml .= "<div>";
                            if(isset($aParsedMetaTags["type"]) && mb_strtolower($aParsedMetaTags["type"]) == "video") {
                                $sHtml .= "<img src='";
                                $sHtml .= asset('web/img/play_button.png');
                                $sHtml .= "'>";
                            }
                            $sHtml .= "<span>";
                                $sHtml .= isset($aParsedMetaTags["title"]) ? $aParsedMetaTags["title"] : $sUrl;
                            $sHtml .= "</span>";
                        $sHtml .= "</div>";
                    $sHtml .= "</div> </a>";
                    $aParsedMetaTags["html"] = $sHtml;
                    $aParsedMetaTags["url"] = $sUrl;

                    $aMessages['data'] = $aParsedMetaTags;
                    $aMessages['errors'] = array();
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'media_parser';
                    $aMessages['response_message'] = 'media_parser_success';

                    return $this->prepareResponse($aMessages);
            }
            else
            {
                $aMessages['data'] = (array)json_decode($sCurlResponse);
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'media_parser';
                $aMessages['response_message'] = 'media_parser_success';

                return $this->prepareResponse($aMessages);
            }
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'media_parser';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    private function isUrlExist($sUrl)
    {
        $oCURL = curl_init($sUrl);    
        curl_setopt($oCURL, CURLOPT_NOBODY, true);
        curl_exec($oCURL);
        $nCode = curl_getinfo($oCURL, CURLINFO_HTTP_CODE);

        if($nCode == 200){
           $bStatus = true;
        }else{
          $bStatus = false;
        }
        curl_close($oCURL);
        return $bStatus;
    }
}