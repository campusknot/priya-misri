<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Controllers\Controller;

use App\Libraries\Post;
use App\Libraries\PostMedia;
use App\Libraries\Comment;
use App\Libraries\Notification;
use App\Libraries\GroupPost;
use App\Libraries\Like;
use App\Libraries\Poll;
use App\Libraries\PollAnswer;
use App\Libraries\PollOption;

use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Jobs\SendNewGroupPostNotification;
use App\Jobs\SendCommentOnPostNotification;
use App\Jobs\SendPostLikeNotification;

class PostController extends Controller
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
        
        $this->middleware('auth', ['only' =>[
                                                'callAddPost', 'callPostDetail',
                                                'callAddPostComment', 'callPostLike',
                                                'callDeletePost', 'callActivatePoll',
                                                'callAddPollAnswer', 'callDeletePostComment'
                                            ]
        ]);
    }
    
    public function callAddPost(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = ['post_type' => 'required'];
            
            if(!empty($oRequest->post_type))
            {
                switch ($oRequest->post_type)
                {
                    case config('constants.POSTTYPEIMAGE'):
                        $oRequest->offsetSet('extention', $oRequest->hasFile('file') ? mb_strtolower($oRequest->file('file')->getClientOriginalExtension()) : NULL);
                        $aValidationRequiredFor = [
                                                        'file' => 'required|image|max:10000',
                                                        'extention' => 'in:bmp,jpg,jpeg,png,svg'
                                                    ];
                        break;
                    case config('constants.POSTTYPEVIDEO'):
                        $oRequest->offsetSet('extention', $oRequest->hasFile('file') ? mb_strtolower($oRequest->file('file')->getClientOriginalExtension()) : NULL);
                        $aValidationRequiredFor = [
                                                        'file' => 'required|video|max:100000',
                                                        'extention' => 'in:mov,mp4,avi,3gp,mkv'
                                                    ];
                        break;
                    case config('constants.POSTTYPEDOCUMENT'):
                        $oRequest->offsetSet('extention', $oRequest->hasFile('file') ? mb_strtolower($oRequest->file('file')->getClientOriginalExtension()) : NULL);
                        $aValidationRequiredFor = [
                                                        'file' => 'required|max:10000',
                                                        'extention' => 'in:doc,docx,pages,rtf,txt,wp,numbers,xls,xlsx,key,ppt,pps,pdf,zip,rar'
                                                    ];
                        break;
                    default :
                        //Code for text/code
                        $aValidationRequiredFor = [
                                                        'post_text' => 'required'
                                                    ];
                }
            }
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_post';
                $aMessages['response_message'] = 'add_post_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $sFileName = '';
            $oNewPost = Post::create([
                                        'id_user' => Auth::user()->id_user,
                                        'post_text' => ($oRequest->post_type == config('constants.POSTTYPEPOLL')) ? '' : trim($oRequest->post_text),
                                        'post_type' => $oRequest->post_type,
                                        'activated' => 1,
                                        'deleted' => 0
                                    ]);
            if($oRequest->hasFile('file'))
            {
                //Upload file in upload folder
                //And make entry in post_media table
                $oUploadedFile = $oRequest->file('file');
                
                $sOriginalName = $oUploadedFile->getClientOriginalName();
		$sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
		
                $oS3 = \Storage::disk('s3');
                $sFilePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                $oS3->put($sFilePath, fopen($oUploadedFile, 'r+'), 'public');
                
                if(mb_substr($oUploadedFile->getMimeType(), 0, 5) == 'image')
                {
                    $oImage = Image::make($oUploadedFile);
                    
                    $nWidth = $oImage->width();
                    $nHeight = $oImage->height();
                    
                    if($nWidth > 800 || $nHeight > 800)
                    {
                        $oImage = $oImage->resize(800, 800,function ($constraint) {
                                                                $constraint->aspectRatio();
                                                            });
                    }
                    $sImageStream = $oImage->stream();
                    
                    $filePath = '/'.config('constants.POSTMEDIAFOLDER').'/80_' . $sFileName;
                    $oS3->put($filePath, $sImageStream->__toString(), 'public');
                }
                
                if($oRequest->post_type != config('constants.POSTTYPEPOLL'))
                {
                    PostMedia::create([
                                    'id_post' => $oNewPost->id_post,
                                    'display_file_name' => $sOriginalName,
                                    'file_name' => $sFileName,
                                    'media_type' => $oRequest->post_type,
                                    'activated' => 1,
                                    'deleted' => 0
                                ]);
                }
            }
            
            if($oRequest->id_group)
            {
                //Add post as Group post and make entry in group_post table.
                GroupPost::create([
                                    'id_group' => $oRequest->id_group,
                                    'id_post' => $oNewPost->id_post
                                ]);
                
                //Send notification to all group members
                $this->dispatch(new SendNewGroupPostNotification(Auth::user(), $oRequest->id_group, $oNewPost->id_post));
            }
            
            $aMessages['data'] = $oNewPost;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_post';
            $aMessages['response_message'] = 'add_post_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'post_detail';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }

    public function callPostDetail(Request $oRequest, $nIdPost)
    {
        if($oRequest->isMethod("GET"))
        {
            $aPostDetail['post_detail'] = Post::getPostDetail($nIdPost);
            
            if(empty($aPostDetail['post_detail']))
            {
                $aMessages['errors'] = array('post_detail' => trans('messages.post_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'post_detail';
                $aMessages['response_message'] = 'post_detail_fail';

                return $this->prepareResponse($aMessages);
            }
            
            if($aPostDetail['post_detail']->post_type==config('constants.POSTTYPEPOLL'))
            {
                $aPostDetail['post_detail']->poll_expired = false;
                
                if((strtotime($aPostDetail['post_detail']->end_time) <= time()) && ($aPostDetail['post_detail']->activated == 1))
                {
                    $aPostDetail['post_detail']->poll_expired = true;
                }
                $oPollOptions = PollOption::getPollOptions($aPostDetail['post_detail']->id_poll);

                $nTotalAnswer = PollAnswer::where('id_poll','=',$aPostDetail['post_detail']->id_poll)->count();
                $aPollAnswers = PollAnswer::getPollAnswer($aPostDetail['post_detail']->id_poll)->all();

                $aPollOptionIds = array_map(function($oPollAnswer) {
                                                return intval($oPollAnswer->id_poll_option);
                                            }, $aPollAnswers);

                foreach($oPollOptions as $oPollOption)
                {
                    $nPercent=0;

                    $nPollOptionKey = array_search($oPollOption->id_poll_option, $aPollOptionIds);
                    if(is_int($nPollOptionKey))
                    {
                        $oPollAnswer = $aPollAnswers[$nPollOptionKey];
                        $nPercent = ($oPollAnswer->total * 100) / $nTotalAnswer;

                        $aPostDetail['post_detail']['pollanswer_'.$oPollOption->id_poll_option] = round($nPercent, 2);
                    }
                    $oPollOption->pollanswer = round($nPercent, 2);
                }
                $aPostDetail['post_detail']->poll_options = $oPollOptions;
            }
                
            if(!empty($aPostDetail['post_detail']->user_profile_image))
                $aPostDetail['post_detail']->user_profile_image = getUserImageUrl ($aPostDetail['post_detail']->user_profile_image, 50);
            
            $aPostDetail['post_detail']->image_size = array('width' => 0, 'height' => 0);
            if(isset($aPostDetail['post_detail']->file_name) && !empty($aPostDetail['post_detail']->file_name))
            {
                $aPostDetail['post_detail']->file_name = setPostImage($aPostDetail['post_detail']->file_name, 80);
                if($aPostDetail['post_detail']->post_type == config('constants.POSTTYPEIMAGE'))
                {
                    $aImageSize = getimagesize($aPostDetail['post_detail']->file_name);
                    if(count($aImageSize) > 2)
                    {
                        $aPostDetail['post_detail']->image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                    }
                }
            }

            $aPostDetail['post_detail']->poll_image_size = array('width' => 0, 'height' => 0);
            if(isset($aPostDetail['post_detail']->poll_file_name) && !empty($aPostDetail['post_detail']->poll_file_name))
            {
                $aPostDetail['post_detail']->poll_file_name = setPostImage($aPostDetail['post_detail']->poll_file_name, 80);
                $aImageSize = getimagesize($aPostDetail['post_detail']->poll_file_name);
                if(count($aImageSize) > 2)
                {
                    $aPostDetail['post_detail']->poll_image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                }
            }
                
            $oComments = Comment::getEntityCommentsWithPagination($nIdPost, 'P');
            $nCount = 0;
            foreach($oComments as $oComment)
            {
                if(!empty($oComment->user_profile_image))
                    $oComments[$nCount]->user_profile_image = getUserImageUrl ($oComment->user_profile_image, 50);
                
                $oComments[$nCount]->image_size = array('width' => 0, 'height' => 0);
                if(isset($oComment->file_name) && !empty($oComment->file_name))
                {
                    $oComments[$nCount]->file_name = setPostImage($oComment->file_name, 80);
                    if($oComment->comment_type == config('constants.POSTTYPEIMAGE'))
                    {
                        $aImageSize = getimagesize($oComment->file_name);
                        if(count($aImageSize) > 2)
                        {
                            $oComments[$nCount]->image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                        }
                    }
                }
                $nCount++;
            }
            $aPostDetail['comments'] = $oComments;
            
            $aMessages['data'] = $aPostDetail;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'post_detail';
            $aMessages['response_message'] = 'post_detail_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'post_detail';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddPostComment(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'comment_type' => 'required',
                                        'id_post' => 'required'
                                        ];
            
            if(!empty($oRequest->comment_type))
            {
                switch ($oRequest->comment_type)
                {
                    case config('constants.POSTTYPEIMAGE'):
                        $oRequest->offsetSet('extention', $oRequest->hasFile('file') ? mb_strtolower($oRequest->file('file')->getClientOriginalExtension()) : NULL);
                        $aValidationRequiredFor['file'] = 'required|image|max:10000';
                        $aValidationRequiredFor['extention'] = 'in:bmp,jpg,jpeg,png,svg';
                        break;
                    case config('constants.POSTTYPEVIDEO'):
                        $oRequest->offsetSet('extention', $oRequest->hasFile('file') ? mb_strtolower($oRequest->file('file')->getClientOriginalExtension()) : NULL);
                        $aValidationRequiredFor['file'] = 'required|video|max:100000';
                        $aValidationRequiredFor['extention'] = 'in:mov,mp4,avi,3gp,mkv';
                        break;
                    case config('constants.POSTTYPEDOCUMENT'):
                        $oRequest->offsetSet('extention', $oRequest->hasFile('file') ? mb_strtolower($oRequest->file('file')->getClientOriginalExtension()) : NULL);
                        $aValidationRequiredFor['file'] = 'required|max:10000';
                        $aValidationRequiredFor['extention'] = 'in:doc,docx,pages,rtf,txt,wp,numbers,xls,xlsx,key,ppt,pps,pdf,zip,rar';
                        break;
                    default :
                        //Code for text/code
                        $aValidationRequiredFor['comment_text'] = 'required';
                }
            }
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_post_comment';
                $aMessages['response_message'] = 'add_post_comment_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $aRequestParams = $oRequest->all();
            $aInsertValues = [
                                'id_user' => Auth::user()->id_user,
                                'id_entity' => $aRequestParams['id_post'],
                                'entity_type' => 'P',
                                'comment_text' => trim($aRequestParams['comment_text']),
                                'comment_type' => $aRequestParams['comment_type'],
                                'activated' => 1,
                                'deleted' => 0
                            ];
            
            if($aRequestParams['comment_type'] != config('constants.POSTTYPETEXT')
                    && $aRequestParams['comment_type'] != config('constants.POSTTYPECODE'))
            {
                //Upload file in upload folder
                //And make entry in post_media table
                $oUploadedFile = $oRequest->file('file');
                
                $sOriginalName = $oUploadedFile->getClientOriginalName();
                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
                
                $oS3 = \Storage::disk('s3');
                $sFilePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                $oS3->put($sFilePath, fopen($oUploadedFile, 'r+'), 'public');
                
                $aInsertValues['display_file_name'] = $sOriginalName;
                $aInsertValues['file_name'] = $sFileName;
                $aInsertValues['file_type'] = $aRequestParams['comment_type'];
            }
            $oPostComment = Comment::create($aInsertValues);
            
            //Add notification for post creator, post commenter, and who likes the post
            $this->dispatch(new SendCommentOnPostNotification(Auth::user(), $aRequestParams['id_post']));
            
            $aMessages['data'] = $oPostComment;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_post_comment';
            $aMessages['response_message'] = 'add_post_comment_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_post_comment';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callPostLike(Request $oRequest, $nIdPost)
    {
        if($oRequest->isMethod("GET"))
        {
            $aWhereParams = [
                                'id_user' => Auth::user()->id_user,
                                'id_entity' => $nIdPost,
                                'entity_type' => 'P'
                            ];
            $oPostLike = Like::where($aWhereParams)
                                ->first();
            if($oPostLike)
            {
                $oPostLike->delete();
            }
            else
            {
                Like::create($aWhereParams);

                //Add notification for post creator
                $oPost = Post::find($nIdPost);
                if($oPost->id_user != Auth::user()->id_user)
                {
                    Notification::create([
                                            'id_user' => $oPost->id_user,
                                            'id_entites' => Auth::user()->id_user.'_'.$nIdPost,
                                            'entity_type_pattern' => 'U_P', //U = user, P = post
                                            'notification_type' => config('constants.NOTIFICATIONLIKEONPOST')
                                        ]);
                    
                    $this->dispatch(new SendPostLikeNotification($nIdPost, Auth::user()->id_user));
                }
            }

            $nPostLikeCount = Like::where('id_entity', '=', $nIdPost)
                                    ->where('entity_type', '=', 'P')
                                    ->count();
        
            $aMessages['data']['like_count'] = $nPostLikeCount;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'post_like';
            $aMessages['response_message'] = 'post_like_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'post_like';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddPollAnswer(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_poll'=> 'required',
                                        'id_post' => 'required'
                                    ];
            if(isset($oRequest->poll_type) && $oRequest->poll_type == config('constants.POLLTYPEOPEN'))
                $aValidationRequiredFor['poll_answer_text'] = 'required';
            else
                $aValidationRequiredFor['id_poll_option'] = 'required';
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_post_comment';
                $aMessages['response_message'] = 'add_post_comment_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oPost = Post::getPostDetail($oRequest->id_post);
            
            if(strtotime($oPost->end_time) > time())
            {
                $oPollAnswer = PollAnswer::firstOrNew([
                                                        'id_poll' => $oRequest->id_poll,
                                                        'id_user' => Auth::user()->id_user,
                                                    ]);
                $oPollAnswer->id_poll_option = $oRequest->id_poll_option;
                $oPollAnswer->poll_answer_description = $oRequest->poll_answer_text;
                $oPollAnswer->save();
            }
            
            $oUserFeed = Post::getPostDetail($oRequest->id_post);
            $oUserFeed->poll_expired = false;
                    
            if(strtotime($oUserFeed->end_time) < time())
            {
                $oUserFeed->poll_expired = true;
            }
            
            $oPollOptions = PollOption::getPollOptions($oUserFeed->id_poll);

            $nTotalAnswer = PollAnswer::where('id_poll','=',$oUserFeed->id_poll)->count();
            $aPollAnswers = PollAnswer::getPollAnswer($oUserFeed->id_poll)->all();

            $aPollOptionIds = array_map(function($oPollAnswer) {
                                            return intval($oPollAnswer->id_poll_option);
                                        }, $aPollAnswers);

            foreach($oPollOptions as $oPollOption)
            {
                $nPercent=0;

                $nPollOptionKey = array_search($oPollOption->id_poll_option, $aPollOptionIds);
                if(is_int($nPollOptionKey))
                {
                    $oPollAnswer = $aPollAnswers[$nPollOptionKey];
                    $nPercent = ($oPollAnswer->total * 100) / $nTotalAnswer;

                    $oUserFeed->offsetSet('pollanswer_'.$oPollAnswer->id_poll_option, round($nPercent, 2));
                }
                $oPollOption->pollanswer = round($nPercent, 2);
            }
            $oUserFeed->poll_options = $oPollOptions;
                
            $aMessages['data'] = $oUserFeed;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_poll_answer';
            $aMessages['response_message'] = 'add_poll_answer_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_poll_answer';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callActivatePoll(Request $oRequest, $nIdPoll)
    {
        if($oRequest->isMethod("GET"))
        {
            $oPoll = Poll::find($nIdPoll);
            if(!empty($oPoll))
            {
                $oPost = Post::find($oPoll->id_entity);
                if(!empty($oPost) && $oPost->id_user == Auth::user()->id_user)
                {
                    $dStartTime = new Carbon($oPoll->start_time);
                    $dEndTime = new Carbon($oPoll->end_time);
                    $nDIfferenceInSeconds = $dStartTime->diffInSeconds($dEndTime);

                    $dScheduleDate = Carbon::now();
                    $sScheduleDate = $dScheduleDate->toDateTimeString();
                    $dScheduleDate->addSeconds($nDIfferenceInSeconds);
                    $sScheduleEndDate = $dScheduleDate->toDateTimeString();

                    $oPoll->start_time = $sScheduleDate;
                    $oPoll->end_time = $sScheduleEndDate;
                    $oPoll->activated = 1;
                    $oPoll->deleted = 0;
                    $oPoll->save();
                    
                    $oGroupPost = GroupPost::where('id_post',$oPost->id_post)
                                        ->first();
                    if(count($oGroupPost))
                    {
                        //Send notification to all group members
                        $this->dispatch(new SendNewGroupPostNotification(Auth::user(), $oGroupPost->id_group, $oGroupPost->id_post));
                    }
                    
                    $aMessages['data'] = array('success_message' => trans('messages.poll_activated_successful'));
                    $aMessages['errors'] = array();
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'activate_poll';
                    $aMessages['response_message'] = 'activate_poll_success';

                    return $this->prepareResponse($aMessages);
                }
                $aMessages['errors'] = array('error_message' => trans('messages.not_allowed_to_activate_poll'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'activate_poll';
                $aMessages['response_message'] = 'activate_poll_fail';

                return $this->prepareResponse($aMessages);
            }
            $aMessages['errors'] = array('error_message' => trans('messages.poll_not_found'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'activate_poll';
            $aMessages['response_message'] = 'activate_poll_fail';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'activate_poll';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callDeletePost(Request $oRequest, $nIdPost)
    {
        if($oRequest->isMethod("GET"))
        {
            $oPost = Post::find($nIdPost);
            if($oPost)
            {
                if($oPost->id_user == Auth::user()->id_user)
                {
                    $oPost->activated = 0;
                    $oPost->deleted = 1;
                    $oPost->update();
                    
                    $aMessages['data'] = array('success_message' => trans('messages.post_delete_successful'));
                    $aMessages['errors'] = array();
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'delete_post';
                    $aMessages['response_message'] = 'delete_post_success';
                    return $this->prepareResponse($aMessages);
                }
                else
                {
                    $aMessages['errors'] = array(trans('messages.not_allowed_to_delete_post'));
                    $aMessages['response_status'] = config('constants.APIERROR');
                    $aMessages['request_type'] = 'delete_post';
                    $aMessages['response_message'] = 'delete_post_fail';
                    return $this->prepareResponse($aMessages);
                }
            }
            
            $aMessages['errors'] = array(trans('messages.post_not_found'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'delete_post';
            $aMessages['response_message'] = 'delete_post_fail';
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'delete_post';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callDeletePostComment(Request $oRequest, $nIdPostComment)
    {
        if($oRequest->isMethod("GET"))
        {
            $oPostComment = Comment::find($nIdPostComment);
            if($oPostComment)
            {
                if($oPostComment->id_user == Auth::user()->id_user)
                {
                    $oPostComment->activated = 0;
                    $oPostComment->deleted = 1;
                    $oPostComment->update();
                    
                    $aMessages['data'] = array('success_message' => trans('messages.comment_delete_successful'));
                    $aMessages['errors'] = array();
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'delete_post_comment';
                    $aMessages['response_message'] = 'delete_post_comment_success';
                    return $this->prepareResponse($aMessages);
                }
                else
                {
                    $aMessages['errors'] = array(trans('messages.not_allowed_to_delete_comment'));
                    $aMessages['response_status'] = config('constants.APIERROR');
                    $aMessages['request_type'] = 'delete_post_comment';
                    $aMessages['response_message'] = 'delete_post_comment_fail';
                    return $this->prepareResponse($aMessages);
                }
            }
            
            $aMessages['errors'] = array(trans('messages.comment_not_found'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'delete_post_comment';
            $aMessages['response_message'] = 'delete_post_comment_fail';
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'delete_post_comment';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
}