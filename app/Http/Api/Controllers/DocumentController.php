<?php

namespace App\Http\Api\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Libraries\Document;
use App\Libraries\SharedDocument;
use App\Libraries\GroupDocument;
use App\Libraries\DocumentPermisssion;
use App\Libraries\Group;
use App\Libraries\GroupMember;
use App\Libraries\Post;

use App\Jobs\SendDocumentSharedNotification;

class DocumentController extends Controller
{

    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
        $this->middleware('auth', ['only' => [
                                                'callDocumentListing', 'callChildDocumentListing',
                                                'callSharedDocument', 'callGroupDocument',
                                                'callDocumentShare', 'callCopyDocument',
                                                'callDeleteDocument', 'callPostDocumentListing'
                                            ]
                        ]);
    }
    
    /**
     * Will return all the documents that created by logged in user
     * @param Request $oRequest
     * @param type $nIdDocument
     * @return type json array
     */
    public function callDocumentListing(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $sCurrentPermission = config('constants.DOCUMENTPERMISSIONTYPEWRITE');        
            
            $oDocuments = Document::getDocumentList(Auth::user()->id_user);
            
            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oDocuments->count();
            $oDocuments = $oDocuments->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oDocuments = new \Illuminate\Pagination\LengthAwarePaginator($oDocuments, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);
            foreach($oDocuments as $sKey => $oDocument)
            {
                $oDocuments[$sKey]->can_delete = ($oDocument->document_type == config('constants.DOCUMENTTYPEFILE')) ? TRUE : !Document::isChildShareorNot($oDocument);
                if($oDocument->file_name != NULL)
                    $oDocuments[$sKey]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name;
                
                $oGroupDetail = GroupDocument::getGroupDocument($oDocument->id_document);
                
                if(count($oGroupDetail) > 0)
                {
                    $oDocuments[$sKey]->shared_with_names = $oGroupDetail[0]->group_name;
                }
                else
                {
                    $sSharedWith = '';
                    $oSharedUsers = SharedDocument::sharedUserList($oDocument->id_document);
                    foreach ($oSharedUsers as $oSharedUser)
                    {
                        $sSharedWith .= $oSharedUser->first_name.' '.$oSharedUser->last_name.', ';
                    }
                    $oDocuments[$sKey]->shared_with_names = trim($sSharedWith, ', ');
                }
            }
            
            $aDocuments = $oDocuments->toArray();
            $aDocuments['data'] = array_values($aDocuments['data']);
            
            $aMessages['data'] = $aDocuments;
            $aMessages['data']['current_document_permission'] = $sCurrentPermission;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'document_listing';
            $aMessages['response_message'] = 'document_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'document_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    /**
     * Will return all child documents of given document
     * @param Request $oRequest
     * @param type $nIdDocument
     * @return type
     */
    public function callChildDocumentListing(Request $oRequest, $nIdDocument)
    {
        if($oRequest->isMethod("GET"))
        {
            $sCurrentPermission = config('constants.DOCUMENTPERMISSIONTYPEREAD');        
            
            $oDocument = Document::find($nIdDocument);
            $nDocumentWritePermissionCount = Document::countAncestorsWithWritePermission($oDocument, Auth::user()->id_user);
            if($nDocumentWritePermissionCount > 0 )
            {
                $sCurrentPermission = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            }
            
            $oDocuments = Document::getChildDocument($nIdDocument);
            
            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oDocuments->count();
            $oDocuments = $oDocuments->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oDocuments = new \Illuminate\Pagination\LengthAwarePaginator($oDocuments, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);
            foreach($oDocuments as $sKey => $oDocument)
            {
                $oDocuments[$sKey]->can_delete = ($oDocument->document_type == config('constants.DOCUMENTTYPEFILE')) ? TRUE : !Document::isChildShareorNot($oDocument);
                if($oDocument->file_name != NULL)
                    $oDocuments[$sKey]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name;
                
                $oGroupDetail = GroupDocument::getGroupDocument($oDocument->id_document);
                if(count($oGroupDetail) > 0)
                {
                    $oDocuments[$sKey]->shared_with_names = $oGroupDetail[0]->group_name;
                }
                else
                {
                    $sSharedWith = '';
                    $oSharedUsers = SharedDocument::sharedUserList($oDocument->id_document);
                    foreach ($oSharedUsers as $oSharedUser)
                    {
                        $sSharedWith .= $oSharedUser->first_name.' '.$oSharedUser->last_name.', ';
                    }
                    $oDocuments[$sKey]->shared_with_names = trim($sSharedWith, ', ');
                }
            }
            
            $aDocuments = $oDocuments->toArray();
            $aDocuments['data'] = array_values($aDocuments['data']);
            
            $aMessages['data'] = $aDocuments;
            $aMessages['data']['current_document_permission'] = $sCurrentPermission;
            $aMessages['data']['id_parent_document'] = $nIdDocument;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'child_document_listing';
            $aMessages['response_message'] = 'child_document_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'child_document_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    /**
     * 
     * @param Request $oRequest
     * @return type
     */
    public function callSharedDocument(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oSharedDocuments = Document::getDocumentSharedList(Auth::user()->id_user);
            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oSharedDocuments->count();
            $oSharedDocuments = $oSharedDocuments->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oSharedDocuments = new \Illuminate\Pagination\LengthAwarePaginator($oSharedDocuments, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);
            
            foreach($oSharedDocuments as $sKey => $oDocument)
            {
                if($oDocument->file_name != NULL)
                    $oSharedDocuments[$sKey]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name;
                
                $oGroupDetail = GroupDocument::getGroupDocument($oDocument->id_document);
                
                if(count($oGroupDetail) > 0)
                {
                    $oSharedDocuments[$sKey]->shared_with_names = $oGroupDetail[0]->group_name;
                }
                else
                {
                    $sSharedWith = '';
                    $oSharedUsers = SharedDocument::sharedUserList($oDocument->id_document);
                    foreach ($oSharedUsers as $oSharedUser)
                    {
                        $sSharedWith .= $oSharedUser->first_name.' '.$oSharedUser->last_name.', ';
                    }
                    $oSharedDocuments[$sKey]->shared_with_names = trim($sSharedWith, ', ');
                }
            }
            $aSharedDocuments = $oSharedDocuments->toArray();
            $aSharedDocuments['data'] = array_values($aSharedDocuments['data']);
            
            $aMessages['data'] = $aSharedDocuments;
            $aMessages['data']['current_document_permission'] = config('constants.DOCUMENTPERMISSIONTYPEREAD');
            $aMessages['data']['id_parent_document'] = '';
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'shared_document_listing';
            $aMessages['response_message'] = 'shared_document_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'shared_document_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    /**
     * 
     * @param Request $oRequest
     * @param type $nIdGroup
     * @return type
     */
    public function callGroupDocument(Request $oRequest, $nIdGroup) 
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroup = new Group();
            $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
            
            if(empty($oGroupDetails))
            {
                $aMessages['errors'] = array('group_detail' => trans('messages.group_deleted'));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'group_documents';
                $aMessages['response_message'] = 'group_documents_fail';

                return $this->prepareResponse($aMessages);
            }
            
            $oGroupDocuments = GroupDocument::getDocumentGroupShared($nIdGroup);
            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oGroupDocuments->count();
            $oGroupDocuments = $oGroupDocuments->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oGroupDocuments = new \Illuminate\Pagination\LengthAwarePaginator($oGroupDocuments, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);
            
            foreach($oGroupDocuments as $sKey => $oDocument)
            {
                if($oDocument->file_name != NULL)
                    $oGroupDocuments[$sKey]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name;
                
                $oGroupDetail = GroupDocument::getGroupDocument($oDocument->id_document);
                if(count($oGroupDetail) > 0)
                {
                    $oGroupDocuments[$sKey]->shared_with_names = $oGroupDetail[0]->group_name;
                }
            }
            $aGroupDocuments = $oGroupDocuments->toArray();
            $aGroupDocuments['data'] = array_values($aGroupDocuments['data']);
            
            $aMessages['data'] = $aGroupDocuments;
            $aMessages['data']['current_document_permission'] = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            $aMessages['data']['id_parent_document'] = '';
            $aMessages['group_detail'] = $oGroupDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_document_listing';
            $aMessages['response_message'] = 'group_document_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_document_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    /**
     * 
     * @param Request $oRequest
     * @return type
     */
    public function callDocumentShare(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_document' => 'required',
                                        'invite_id_users' => 'required_without:invite_id_groups',
                                        'invite_id_groups' => 'required_without:invite_id_users'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'share_document';
                $aMessages['response_message'] = 'share_document_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $sPermission = isset($oRequest->permission) ? $oRequest->permission : config('constants.DOCUMENTPERMISSIONTYPEREAD');
            
            $aUserList = array();
            if($oRequest->invite_id_users)
            {
                $aUserList = explode(',',$oRequest->invite_id_users);
            }
            
            $aGroupList = array();
            if($oRequest->invite_id_groups)
            {
                $aGroupList = explode(',',$oRequest->invite_id_groups);
            }
            
            foreach($aUserList as $nIdUser)
            {
                $this->addUserDocument($oRequest->id_document, $nIdUser, Auth::user()->id_user, $sPermission);
            }
            
            foreach($aGroupList as $nIdGroup)
            {
                $this->addGroupDocument($oRequest->id_document, $nIdGroup, Auth::user()->id_user, $sPermission);
            }
            
            if(count($aUserList))
            {
                $this->dispatch(new SendDocumentSharedNotification($oRequest->id_document, Auth::user()->id_user, $sPermission, $aUserList));
            }
            
            $aMessages['data']['success_message'] = trans('messages.document_shared_success');
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'share_document';
            $aMessages['response_message'] = 'share_document_success';
            $aMessages['errors'] = array();
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'share_document';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    //share document with group
    private function addGroupDocument($nIdDocument, $nIdGroup, $nIdUserFrom, $sPermission)
    {
        $oGroupDocument = GroupDocument::firstOrCreate([
                                                        'id_document' => $nIdDocument,
                                                        'id_group' => $nIdGroup
                                                    ]);
        $oGroupDocument->group_document_permission = $sPermission;
        $oGroupDocument->save();
        
        $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                            'id_document' => $nIdDocument,
                                            'id_user' => Auth::user()->id_user,
                                            'id_group' => $nIdGroup
                                        ]);
        $oDocumentPermisssion->permission_type = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
        $oDocumentPermisssion->save();
                
        $aGroupMembers = GroupMember::getAllGroupMembers($nIdGroup)->all();
        $aGroupMemberIds = array_map(function($oGroupMember){
                                    return intval($oGroupMember->id_user);
                                },$aGroupMembers);
        $this->dispatch(new SendDocumentSharedNotification($nIdDocument, Auth::user()->id_user, $sPermission, $aGroupMemberIds, $nIdGroup));
        
        return true;
    }
    
    //share document with user one to one
    private function addUserDocument( $nIdDocument ,$nIdUserTo, $nIdUserFrom, $sPermission )
    { 
        //one to one share  //      
        if($nIdUserFrom != $nIdUserTo)
        {
            $oSharedDocument = SharedDocument::firstOrNew([
                                                                'id_document' => $nIdDocument,
                                                                'id_user' => $nIdUserFrom,
                                                                'shared_with' => $nIdUserTo
                                                            ]);
            $oSharedDocument->activated = 1;
            $oSharedDocument->deleted = 0;
            $oSharedDocument->save();
            
            $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                                                            'id_document' => $nIdDocument,
                                                                            'id_user' => $nIdUserTo,
                                                                            'id_group' => NULL
                                                                        ]);
            $oDocumentPermisssion->permission_type = $sPermission;
            $oDocumentPermisssion->activated = 1;
            $oDocumentPermisssion->deleted = 0;
            $oDocumentPermisssion->save();
        }
        return true;
    }
    
    /**
     * 
     * @param Request $oRequest
     * @param type $nIdDocument
     * @return type
     */
    public function callDeleteDocument(Request $oRequest, $nIdDocument)
    {
        if($oRequest->isMethod("GET"))
        {
            $oDocument = Document::find($nIdDocument);
            if($oDocument->id_user == Auth::user()->id_user)
            {        
                Document::where('lft' ,'>=', $oDocument->lft)
                        ->where('rgt' ,'<=', $oDocument->rgt)
                        ->update(['deleted' => 1, 'activated' => 0]);
                
                $aMessages['data']['message'] = trans('messages.document_delete_success');
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'document_delete';
                $aMessages['response_message'] = 'document_delete_success';

                return $this->prepareResponse($aMessages);
            }
            
            $aMessages['errors'] = array("message" => trans('messages.document_delete_no_permission'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'document_delete';
            $aMessages['response_message'] = 'document_delete_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'document_delete';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    /**
     * 
     * @param Request $oRequest
     * @return type
     */
    public function callPostDocumentListing(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oPostDocuments = Post::getUserPostDocument(Auth::user()->id_user, 'D');
            foreach($oPostDocuments as $sKey => $oDocument)
            {
                if($oDocument->file_name != NULL)
                    $oPostDocuments[$sKey]->file_name = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name;
            }
            
            $aMessages['data'] = $oPostDocuments;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'post_documents';
            $aMessages['response_message'] = 'post_documents_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'post_documents';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callCopyDocument(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_document' => 'required|exists:documents'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'copy_document';
                $aMessages['response_message'] = 'copy_document_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oDocument = Document::find($oRequest->id_document);
            if($oDocument->id_user == Auth::user()->id_user)
            {
                if(empty($oRequest->id_group))
                {
                    $nIdDocumentCopyInto = (!empty($oRequest->id_document_copy_into) || $oRequest->id_document_copy_into) ? $oRequest->id_document_copy_into : NULL;
                    $oDuplicateDocument = $this->copyIntoDocument($oDocument, $nIdDocumentCopyInto, TRUE);
                    if($oDuplicateDocument) {
                        $aChildDocuments = $oDocument->getDescendants()->toHierarchy(); 
                        $this->copyChildDocuments($aChildDocuments, $oDuplicateDocument->id_document);
                        
                        $aMessages['data'] = array('success_message' => trans('messages.copy_folder_success'));
                        $aMessages['errors'] = array();
                        $aMessages['response_status'] = config('constants.APISUCCESS');
                        $aMessages['request_type'] = 'copy_document';
                        $aMessages['response_message'] = 'copy_document_success';

                        return $this->prepareResponse($aMessages);
                    }
                    else {
                        $aMessages['errors'] = array('no_write_permission' => trans('messages.no_write_permission'));
                        $aMessages['response_status'] = config('constants.APIERROR');
                        $aMessages['request_type'] = 'copy_document';
                        $aMessages['response_message'] = 'copy_document_fail';

                        return $this->prepareResponse($aMessages);
                    }
                }
                
                $oDuplicateDocument = $this->copyIntoGroup($oDocument, $oRequest->id_group);
                
                if($oDuplicateDocument) {
                    $aChildDocuments = $oDocument->getDescendants()->toHierarchy(); 
                    $this->copyChildDocuments($aChildDocuments, $oDuplicateDocument->id_document);
                    
                    $aMessages['data'] = array('success_message' => trans('messages.copy_folder_success'));
                    $aMessages['errors'] = array();
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'copy_document';
                    $aMessages['response_message'] = 'copy_document_success';

                    return $this->prepareResponse($aMessages);
                }
                else {
                    $aMessages['errors'] = array('no_write_permission' => trans('messages.no_write_permission'));
                    $aMessages['response_status'] = config('constants.APIERROR');
                    $aMessages['request_type'] = 'copy_document';
                    $aMessages['response_message'] = 'copy_document_fail';

                    return $this->prepareResponse($aMessages);
                }
            }
            
            $aMessages['errors'] = array('not_allowed_to_copy' => trans('messages.not_allowed_to_copy'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'copy_document';
            $aMessages['response_message'] = 'copy_document_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'copy_document';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    private function copyIntoGroup($oDocument, $nIdGroup)
    {
        $oDuplicateDocument = $this->copyIntoDocument($oDocument, NULL, FALSE);
        if($oDuplicateDocument) {
            $this->addGroupDocument($oDuplicateDocument->id_document, $nIdGroup, Auth::user()->id_user, config('constants.DOCUMENTPERMISSIONTYPEREAD'));
        }
        
        return $oDuplicateDocument;
    }
    
    private function copyIntoDocument($oDocument, $nIdDocumentCopyInto, $bCreatePermission)
    {
        if(!empty($nIdDocumentCopyInto))
        {
            $oDocumentCopyInto = Document::find($nIdDocumentCopyInto);
            $nCount = Document::checkDocumentPermission($oDocumentCopyInto, Auth::user()->id_user, config('constants.DOCUMENTPERMISSIONTYPEWRITE'));
            if(!$nCount)
                return FALSE;
        }
        
        //Copy document into my documents root
        $oDuplicateDocument = $oDocument->replicate();
        if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
        {
            $oDuplicateDocument->document_name = $oDocument->document_name.'-copy';
        }
        else
        {
            $aDocumentName = explode('.',$oDocument->document_name);
            $sDocumentExtention = array_pop($aDocumentName);
            $aNewDocumentName = array(implode('_', $aDocumentName), $sDocumentExtention);
            $oDuplicateDocument->document_name = $aNewDocumentName[0].'-copy.'.$sDocumentExtention;

        }
        $oDuplicateDocument->copy_from = $oDocument->id_document;
        $oDuplicateDocument->id_parent = $nIdDocumentCopyInto;
        $oDuplicateDocument->save();
        
        if($bCreatePermission)
        {
            DocumentPermisssion::create([
                                        'id_document' => $oDuplicateDocument->id_document,
                                        'id_user' => Auth::user()->id_user,
                                        'permission_type' => config('constants.DOCUMENTPERMISSIONTYPEWRITE')
                                    ]);
        }
        return $oDuplicateDocument;
    }
    
    private function copyChildDocuments($aChildDocuments,$nIdParent) 
    {
        foreach($aChildDocuments as $oDocument)
        {
            $oChildDocument = Document::find($oDocument->id_document);
            $oDuplicateDocument = $oChildDocument->replicate();
            $oDuplicateDocument->id_parent = $nIdParent;
            $oDuplicateDocument->copy_from = $oDocument->id_document;
            $oDuplicateDocument->save();
            if(count($oDocument->children))
            {
                $this->copyChildDocuments($oDocument->children, $oDuplicateDocument->id_document);
            }
        }
    }
}