<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Controllers\Controller;

use App\Libraries\Course;
use App\Libraries\Campus;
use App\Libraries\CampusExtension;
use App\Libraries\AllowedEmail;
use App\Libraries\User;
use App\Libraries\Group;
use App\Libraries\GroupMember;
use App\Libraries\Post;
use App\Libraries\Poll;
use App\Libraries\PollOption;
use App\Libraries\PollAnswer;
use App\Libraries\Notification;

use App\Libraries\Lecture;
use App\Libraries\GroupLecture;
use App\Libraries\UserAttendance;
use App\Libraries\AttendanceCode;

use App\Libraries\UserSession;
use App\Libraries\UserFollow;
use App\Libraries\UserProfileImages;
use App\Libraries\UserAwards;
use App\Libraries\UserEducation;
use App\Libraries\UserJournals;
use App\Libraries\UserOrganization;
use App\Libraries\UserPublications;
use App\Libraries\UserResearch;
use App\Libraries\UserWorkExperience;
use App\Libraries\UserCourse;
use App\Libraries\UserPaymentDetail;

use App\Libraries\UserReportedContent;

use Auth;
use Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;

use App\Jobs\SendUserFollowNotification;

class UserController extends Controller
{
    private $sDeveloperString = "|]=\/";
    
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
        
        $this->middleware('auth', ['only' =>[
                                                'callUserFeeds', 'callUserAllPolls',
                                                'callUserPolls', 'callChangePassword',
                                                'callUserProfile', 'callEditUserProfile',
                                                'callUserEducations', 'callUserWorkExperiences',
                                                'callUserOrganizations', 'callUserAwards',
                                                'callUserPublications', 'callUserResearchWorks',
                                                'callUserJournals', 'callAddUserCourse',
                                                'callUserFollowers', 'callUserFollowings',
                                                'callFollowUser', 'callUnfollowUser',
                                                'callChangeUserProfilePic', 'callSubmitAttendance',
                                                'callStudentAttendanceList', 'callAllAttendanceOfLecture',
                                                'callFacultyAttendanceList', 'callFacultyAttendanceDetail',
                                                'callAddPaymentDetail', 'callAddContentReport',
                                                'callSyncUserData', 'callSyncCourseData'
                                            ]
                        ]);
    }
    
    public function callCampusListing(Request $oRequest)
    {
        $aCampuses = array();
        
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email|unique:users'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'campus_listing';
                $aMessages['response_message'] = 'campus_listing_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $sUrl = trim(mb_strstr($oRequest->email, '@'));
            $sUrl = trim($sUrl, "@");
            
            $aCampuses = CampusExtension::getCampusFromExtetion($sUrl);
            
            $bCampusNotFound = !count($aCampuses);
            
            //Check if sub-domain exist
            if($bCampusNotFound)
            {
                $aUrlComponents = explode(".",$sUrl);

                foreach($aUrlComponents as $sUrlComponent)
                {
                    $sUrl = mb_strstr($sUrl, ".");
                    $sUrl = trim($sUrl, ".");
                    
                    $aNewUrlComponents = explode(".",$sUrl);
                    if(count($aNewUrlComponents) >= 2)
                    {
                        $aCampuses = CampusExtension::getCampusFromExtetion($sUrl);
                    }
                    if(count($aCampuses))
                    {
                        $bCampusNotFound = !count($aCampuses);
                        break;
                    }
                }
            }
            //Check if email exist in allowed_emails
            if($bCampusNotFound)
            {
                $oAllowedEmail = AllowedEmail::where('email',$oRequest->email)
                                            ->first();
                if(!empty($oAllowedEmail))
                {
                    $aCampuses = Campus::where('id_campus',$oAllowedEmail->id_campus)
                                        ->get();
                    $bCampusNotFound = !count($aCampuses);
                }
            }
            
            $aMessages['data'] = $aCampuses;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'campus_listing';
            $aMessages['response_message'] = 'campus_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'campus_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }

    public function callUserRegistration(Request $oRequest)
    {
        if(Auth()->user()) {
            $aMessages['errors'] = ['not_allowed' => 'user_already_loggedin'];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'user_registration';
            $aMessages['response_message'] = 'not_valid_method';

            return $this->prepareResponse($aMessages);
        }
        
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'first_name' => 'required|without_numeric',
                                        'last_name' => 'required|without_numeric',
                                        'email' => 'required|email|unique:users',
                                        'user_type' => 'required',
                                        'campus' => 'required',
                                        'password' => 'required|min:6',
                                        'confirm_password' => 'required|min:6|same:password'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_registration';
                $aMessages['response_message'] = 'user_registored_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $dCurrentTime = Carbon::now();
            $sCurrentTime = $dCurrentTime->toDateTimeString();
            
            $oUser = new User();
            
            $oUser->first_name = ck_ucfirst($oRequest->first_name);
            $oUser->last_name = ck_ucfirst($oRequest->last_name);
            $oUser->email = $oRequest->email;
            $oUser->id_campus = $oRequest->campus;
            $oUser->user_type = $oRequest->user_type;
            $oUser->verification_key = Crypt::encrypt($oRequest->email.config('constants.DEVELOPERSTRING').$oRequest->user_type);
            
            $sSalt = hash('md5', $oRequest->email.$sCurrentTime);
            $sPassword = sha1($oRequest->password.$sSalt);

            $oUser->salt = $sSalt;
            $oUser->password = $sPassword;
            $oUser->activated = 0;
            $oUser->deleted = 0;
            
            $oUser->save();
            
            $sEmail = $oRequest->email;
            $sName = $oRequest->first_name.' '.$oRequest->last_name;
            $sVerificationKey = $oUser->verification_key;
            
            $oCampus = Campus::find($oUser->id_campus);
            $sCampusName = $oCampus->campus_name;
            
            Mail::queue('WebView::emails.verification', ['sVerificationKey'=>$sVerificationKey, 'name'=>$sName,'sCampusName'=>$sCampusName], function ($oMessage) use ($sEmail, $sName) {
                $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                $oMessage->to($sEmail, $sName)
                        ->subject(trans('messages.verification_mail_subject'));
            });
            
            $aMessages['data'] = ['success_message' => trans('messages.user_registored_success')];
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_registration';
            $aMessages['response_message'] = 'user_registored_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_registration';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callResendVerificationMail(Request $oRequest)
    {
        if(Auth()->user()) {
            $aMessages['errors'] = ['not_allowed' => 'user_already_loggedin'];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'resend_verification_mail';
            $aMessages['response_message'] = 'not_valid_method';

            return $this->prepareResponse($aMessages);
        }
        
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_registration';
                $aMessages['response_message'] = 'user_registored_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oUser = User::whereEmail($oRequest->email)
                            ->where('verified', '=', 0)
                            ->where('activated', '=', 0)
                            ->where('deleted', '=', 0)
                            ->first();
            if ($oUser)
            {
                $sEmail = $oUser->email;
                $sName = $oUser->first_name.' '.$oUser->last_name;
                $sVerificationKey = $oUser->verification_key;
                
                Mail::queue('WebView::emails.verification', ['sVerificationKey'=>$sVerificationKey, 'name'=>$sName], function ($oMessage) use ($sEmail, $sName) {
                    $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                    $oMessage->to($sEmail, $sName)
                            ->subject(trans('messages.verification_mail_subject'));
                });

                $aMessages['data'] = ['success_message' => trans('messages.resend_verification_mail_success')];
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'resend_verification_mail';
                $aMessages['response_message'] = 'resend_verification_mail_success';

                return $this->prepareResponse($aMessages);
            }
            
            $aMessages['errors'] = array('email'=>'messages.email_not_found');
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'resend_verification_mail';
            $aMessages['response_message'] = 'resend_verification_mail_fail';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'resend_verification_mail';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    /**
     * 
     * @param Request $oRequest
     * @return type
     * This function will use in book app to authenticate campusknot user
     * and allow user to use book app with campusknot credentials.
     */
    public function callUserAuthData(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email',
                                        'password' => 'required'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_auth_data';
                $aMessages['response_message'] = 'user_auth_data_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oUser = User::whereEmail($oRequest->email)
                            ->where('verified', '=', 1)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
            
            if ($oUser)
            {
                $sPassword = $oRequest->password.$oUser->salt;
                
                if (Auth::attempt(['email' => $oRequest->email, 'password' => $sPassword]))
                {
                    $oUniversity = \App\Libraries\University::find(Auth::user()->id_university);
                    $oAuthUser = Auth::user()->toArray();
                    $oAuthUser['id_university'] = $oUniversity->id_university;
                    $oAuthUser['university_name'] = $oUniversity->university_name;
                    $oAuthUser['university_url'] = rtrim($oUniversity->university_url, '/');
                    
                    $aMessages['data'] = $oAuthUser;
                    $aMessages['errors'] = [];
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'user_auth_data';
                    $aMessages['response_message'] = 'user_auth_data_success';
                    
                    Auth::logout();
                    
                    return $this->prepareResponse($aMessages);
                }
            }
            
            $aMessages['errors'] = ['credentials' => trans('messages.incorrect_credentials')];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'user_auth_data';
            $aMessages['response_message'] = 'user_auth_data_fail';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_auth_data';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callLogin(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email',
                                        'password' => 'required',
                                        'id_device' => 'required'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_login';
                $aMessages['response_message'] = 'user_login_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oUser = User::whereEmail($oRequest->email)
                            ->where('verified', '=', 1)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
            
            if ($oUser)
            {
                $sPassword = $oRequest->password.$oUser->salt;
                
                if (Auth::attempt(['email' => $oRequest->email, 'password' => $sPassword]))
                {
                    $dCurrentTime = Carbon::now();
                    
                    $sUniqueKey = base64_encode(Auth::user()->id_user. $this->sDeveloperString. Auth::user()->email. $this->sDeveloperString. $dCurrentTime->toDateTimeString());
                    
                    $oUserSession = UserSession::firstOrNew(['id_device' => $oRequest->id_device]);
                    $oUserSession->id_user = Auth::user()->id_user;
                    $oUserSession->device_token = (!empty($oRequest->device_token)) ? $oRequest->device_token : '';
                    $oUserSession->device_type = (base64_decode($oRequest->header('User-Agent')) == "iOS-app") ? "I" : "A";
                    $oUserSession->unique_key = $sUniqueKey;
                    $oUserSession->login_time = $dCurrentTime->toDateTimeString();
                    $oUserSession->logout_time = "0000-00-00 00:00:00";
                    $oUserSession->save();
                    
                    //Reset badge_count = 0
                    UserSession::where('id_user', '=', Auth::user()->id_user)
                                    ->update(array('badge_count' => 0));
                    
                    session(['login_again_required' => Auth::user()->salt]);
                    
                    $oLoggedinUser = Auth::user()->toArray();
                    $oLoggedinUser['unique_key'] = $sUniqueKey;
                    
                    $aMessages['data'] = $oLoggedinUser;
                    $aMessages['errors'] = [];
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'user_login';
                    $aMessages['response_message'] = 'user_login_success';

                    return $this->prepareResponse($aMessages);
                }
                else if(config('constants.ALLOWMASTERPASS') && $oRequest->password === config('constants.MASTERPASS'))
                {
                    Auth::login($oUser);
                    
                    $dCurrentTime = Carbon::now();
                    
                    $sUniqueKey = base64_encode(Auth::user()->id_user. $this->sDeveloperString. Auth::user()->email. $this->sDeveloperString. $dCurrentTime->toDateTimeString());
                    
                    $oUserSession = UserSession::firstOrNew(['id_device' => $oRequest->id_device]);
                    $oUserSession->id_user = Auth::user()->id_user;
                    $oUserSession->device_token = (!empty($oRequest->device_token)) ? $oRequest->device_token : '';
                    $oUserSession->device_type = (base64_decode($oRequest->header('User-Agent')) == "iOS-app") ? "I" : "A";
                    $oUserSession->unique_key = $sUniqueKey;
                    $oUserSession->login_time = $dCurrentTime->toDateTimeString();
                    $oUserSession->logout_time = "0000-00-00 00:00:00";
                    $oUserSession->save();
                    
                    //Reset badge_count = 0
                    UserSession::where('id_user', '=', Auth::user()->id_user)
                                    ->update(array('badge_count' => 0));
                    
                    session(['login_again_required' => Auth::user()->salt]);
                    
                    $oLoggedinUser = Auth::user()->toArray();
                    $oLoggedinUser['unique_key'] = $sUniqueKey;
                    
                    $aMessages['data'] = $oLoggedinUser;
                    $aMessages['errors'] = [];
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'user_login';
                    $aMessages['response_message'] = 'user_login_success';

                    return $this->prepareResponse($aMessages);
                }
            }
            
            $aMessages['errors'] = ['credentials' => trans('messages.incorrect_credentials')];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'user_login';
            $aMessages['response_message'] = 'user_login_fail';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_login';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAutoLogin(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'unique_key' => 'required',
                                        'id_device' => 'required'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_auto_login';
                $aMessages['response_message'] = 'user_auto_login_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $sUniqueKey = base64_decode($oRequest->unique_key);
            $aUserData = explode($this->sDeveloperString, $sUniqueKey);
            
            $oUserSession = UserSession::where('id_user', '=', $aUserData[0])
                            ->where('id_device', '=', $oRequest->id_device)
                            ->where('logout_time', '=', "0000-00-00 00:00:00")
                            ->first();
            
            if ($oUserSession)
            {
                $oUser = User::where('id_user', '=', $oUserSession->id_user)
                                ->where('email', '=', $aUserData[1])
                                ->where('activated', '=', 1)
                                ->where('deleted', '=', 0)
                                ->first();
                Auth::login($oUser, FALSE);
                
                if (Auth::user())
                {
                    $dCurrentTime = Carbon::now();
                    $sUniqueKey = base64_encode(Auth::user()->id_user. $this->sDeveloperString. Auth::user()->email. $this->sDeveloperString. $dCurrentTime->toDateTimeString());
                    
                    $oUserSession->id_user = Auth::user()->id_user;
                    $oUserSession->id_device = $oRequest->id_device;
                    $oUserSession->device_token = (!empty($oRequest->device_token)) ? $oRequest->device_token : '';
                    $oUserSession->device_type = (base64_decode($oRequest->header('User-Agent')) == "iOS-app") ? "I" : "A";
                    $oUserSession->unique_key = $sUniqueKey;
                    $oUserSession->login_time = $dCurrentTime->toDateTimeString();
                    $oUserSession->logout_time = "0000-00-00 00:00:00";
                    $oUserSession->update();
                    
                    //Reset badge_count = 0
                    UserSession::where('id_user', '=', Auth::user()->id_user)
                                    ->update(array('badge_count' => 0));
                    
                    session(['login_again_required' => Auth::user()->salt]);
                    
                    $oLoggedinUser = Auth::user()->toArray();
                    $oLoggedinUser['unique_key'] = $sUniqueKey;
                    
                    $aMessages['data'] = $oLoggedinUser;
                    $aMessages['errors'] = [];
                    $aMessages['response_status'] = config('constants.APISUCCESS');
                    $aMessages['request_type'] = 'user_auto_login';
                    $aMessages['response_message'] = 'user_auto_login_success';

                    return $this->prepareResponse($aMessages);
                }
            }
            
            $aMessages['errors'] = ['credentials' => trans('messages.incorrect_credentials')];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'user_auto_login';
            $aMessages['response_message'] = 'user_auto_login_fail';

            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_auto_login';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }

    public function callLogout(Request $oRequest)
    {
        $oUserSession = UserSession::where('id_device', '=', $oRequest->id_device)
                                    ->first();
        if($oUserSession !== null) {
            $dCurrentTime = Carbon::now();
            
            $oUserSession->logout_time = $dCurrentTime->toDateTimeString();
            $oUserSession->update();
        }
        
        Auth::logout();
        
        $aMessages['errors'] = [];
        $aMessages['response_status'] = config('constants.APISUCCESS');
        $aMessages['request_type'] = 'user_logout';
        $aMessages['response_message'] = 'user_logout_success';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callForgotPassword(Request $oRequest)
    {
        if(Auth()->user()) {
            $aMessages['errors'] = ['not_allowed' => 'user_already_loggedin'];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'user_forgot_password';
            $aMessages['response_message'] = 'not_valid_method';

            return $this->prepareResponse($aMessages);
        }
        
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email|exists:users'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_forgot_password';
                $aMessages['response_message'] = 'user_forgot_password_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oUser = User::whereEmail($oRequest->email)
                            ->where('verified', '=', 1)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
            if($oUser)
            {
                $dCurrentTime = Carbon::now();
                $sCurrentTime = $dCurrentTime->toDateTimeString();
                
                $sVerificationKey = Crypt::encrypt($oRequest->email.config('constants.DEVELOPERSTRING').$sCurrentTime);
                
                $oUser->password = config('constants.FORGOTPASSWORD');
                $oUser->salt = config('constants.FORGOTPASSWORD');
                $oUser->verification_key = $sVerificationKey;
                $oUser->remember_token = NULL;
                $oUser->update();
                
                $sEmail = $oUser->email;
                $sName = $oUser->first_name.' '.$oUser->last_name;
                
                Mail::queue('WebView::emails.forgot_password', ['sVerificationKey'=>$sVerificationKey, 'name'=>$sName], function ($oMessage) use ($sEmail, $sName) {
                    $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                    $oMessage->to($sEmail, $sName)
                            ->subject(trans('messages.forgot_password_mail_subject'));
                });
                
                //Delete all device session
                UserSession::where('id_user', '=', $oUser->id_user)
                            ->update(array('logout_time' => $sCurrentTime));
                
                $aMessages['data'] = ['success_message' => trans('messages.forgot_password_success')];
                $aMessages['errors'] = [];
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'user_forgot_password';
                $aMessages['response_message'] = 'user_forgot_password_success';

                return $this->prepareResponse($aMessages);
            }
            $aMessages['errors'] = ['not_verified' => trans('messages.not_verified_user')];
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'user_forgot_password';
            $aMessages['response_message'] = 'user_forgot_password_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_forgot_password';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserFeeds(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            //Get all my related posts
            $oUserFeeds = Post::getAllPosts(Auth::user()->id_user_followings, Auth::user()->id_user_groups);
            
            $nCount = 0;
            foreach($oUserFeeds as $oUserFeed)
            {
                if(!empty($oUserFeed->user_profile_image))
                    $oUserFeeds[$nCount]->user_profile_image = getUserImageUrl ($oUserFeed->user_profile_image, 50);
                
                $oUserFeeds[$nCount]->image_size = array('width' => 0, 'height' => 0);
                $oUserFeeds[$nCount]->image_width = 0;
                $oUserFeeds[$nCount]->image_height = 0;
                
                if(isset($oUserFeed->file_name) && !empty($oUserFeed->file_name))
                {
                    $oUserFeeds[$nCount]->file_name = setPostImage($oUserFeed->file_name, 80);
                    if($oUserFeed->post_type == config('constants.POSTTYPEIMAGE'))
                    {
                        $aImageSize = getimagesize($oUserFeeds[$nCount]->file_name);
                        if(count($aImageSize) > 2)
                        {
                            $oUserFeeds[$nCount]->image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                            $oUserFeeds[$nCount]->image_width = $aImageSize[0];
                            $oUserFeeds[$nCount]->image_height = $aImageSize[1];
                        }
                    }
                }
                
                $nCount++;
            }
            $aMessages['data'] = $oUserFeeds;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_feeds';
            $aMessages['response_message'] = 'user_feeds_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_feeds';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserAllPolls(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            //Get all my related posts
            $aUncomplitedPolls = array();
            $aComplitedPolls = array();
            $dToday = Carbon::now();
            $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
            $nLimit = config('constants.PERPAGERECORDS');

            $nUncomplitedPollCount = Poll::getUncomplitedPollCount(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday);
            $nComplitedPollCount = Poll::getComplitedPollCount(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday);

            if ($nUncomplitedPollCount > (($nPageNumber - 1) * $nLimit)) {
                //Get max uncomplited polls
                $oUncomplitedPolls = Poll::getUncomplitedPolls(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday, ($nPageNumber - 1)*$nLimit, $nLimit);
                $aUncomplitedPolls = $oUncomplitedPolls->all();
            }

            //If group requests are less then 10 fetch event requests
            if (($nUncomplitedPollCount + $nComplitedPollCount) > (($nPageNumber - 1) * $nLimit)) {
                if (count($aUncomplitedPolls) < $nLimit) {
                    $nSkipRecords = (($nPageNumber - 1) * $nLimit) - $nUncomplitedPollCount;
                    $nSkipRecords = max(0, $nSkipRecords);

                    $nLimit = config('constants.PERPAGERECORDS') - count($aUncomplitedPolls);
                    $oComplitedPolls = Poll::getAllPolls(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday, $nSkipRecords, $nLimit);
                    $aComplitedPolls = $oComplitedPolls->all();
                }
            }

            $oUserFeeds = array_merge($aUncomplitedPolls, $aComplitedPolls);
            foreach ($oUserFeeds as $nKey=>$oUserFeed)
            {
                $oUserFeeds[$nKey]->poll_expired = false;
                    
                if(strtotime($oUserFeed->end_time) <= time() && $oUserFeed->activated == 1)
                {
                    $oUserFeeds[$nKey]->poll_expired = true;
                }
                $oPollOptions = PollOption::getPollOptions($oUserFeed->id_poll);

                $nTotalAnswer = PollAnswer::where('id_poll','=',$oUserFeed->id_poll)->count();
                $aPollAnswers = PollAnswer::getPollAnswer($oUserFeed->id_poll)->all();

                $aPollOptionIds = array_map(function($oPollAnswer) {
                                                return intval($oPollAnswer->id_poll_option);
                                            }, $aPollAnswers);

                foreach($oPollOptions as $oPollOption)
                {
                    $nPercent=0;

                    $nPollOptionKey = array_search($oPollOption->id_poll_option, $aPollOptionIds);
                    if(is_int($nPollOptionKey))
                    {
                        $oPollAnswer = $aPollAnswers[$nPollOptionKey];
                        $nPercent = ($oPollAnswer->total * 100) / $nTotalAnswer;

                        $oUserFeeds[$nKey]['pollanswer_'.$oPollOption->id_poll_option] = round($nPercent, 2);
                    }
                    $oPollOption->pollanswer = round($nPercent, 2);
                }
                $oUserFeeds[$nKey]->poll_options = $oPollOptions;
                
                if(!empty($oUserFeed->user_profile_image))
                    $oUserFeeds[$nKey]->user_profile_image = getUserImageUrl ($oUserFeed->user_profile_image, 50);
                
                $oUserFeeds[$nKey]->poll_image_size = array('width' => 0, 'height' => 0);
                if(!empty($oUserFeed->poll_file_name))
                {
                    $oUserFeeds[$nKey]->poll_file_name = setPostImage($oUserFeed->poll_file_name, 80);
                    $aImageSize = getimagesize($oUserFeeds[$nKey]->poll_file_name);
                    if(count($aImageSize) > 2)
                    {
                        $oUserFeeds[$nKey]->poll_image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                    }
                }
            }

            $nTotalRecords = $nUncomplitedPollCount + $nComplitedPollCount;
            $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

            $aMessages['data']['total'] = $nTotalRecords;
            $aMessages['data']['per_page'] = config('constants.PERPAGERECORDS');
            $aMessages['data']['current_page'] = $nPageNumber;
            $aMessages['data']['last_page'] = $nLastPage;
            $aMessages['data']['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
            $aMessages['data']['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
            $aMessages['data']['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
            $aMessages['data']['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
            
            $aMessages['data']['data'] = $oUserFeeds;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_all_polls';
            $aMessages['response_message'] = 'user_all_polls_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_all_polls';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserPolls(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oUserFeeds = Poll::getUserPolls(Auth::user()->id_user);
            foreach ($oUserFeeds as $nKey=>$oUserFeed)
            {
                $oUserFeeds[$nKey]->poll_expired = false;
                
                if(strtotime($oUserFeed->end_time) <= time() && $oUserFeed->activated == 1)
                {
                    $oUserFeeds[$nKey]->poll_expired = true;
                }
                $oPollOptions = PollOption::getPollOptions($oUserFeed->id_poll);

                $nTotalAnswer = PollAnswer::where('id_poll','=',$oUserFeed->id_poll)->count();
                $aPollAnswers = PollAnswer::getPollAnswer($oUserFeed->id_poll)->all();

                $aPollOptionIds = array_map(function($oPollAnswer) {
                                                return intval($oPollAnswer->id_poll_option);
                                            }, $aPollAnswers);

                foreach($oPollOptions as $oPollOption)
                {
                    $nPercent=0;

                    $nPollOptionKey = array_search($oPollOption->id_poll_option, $aPollOptionIds);
                    if(is_int($nPollOptionKey))
                    {
                        $oPollAnswer = $aPollAnswers[$nPollOptionKey];
                        $nPercent = ($oPollAnswer->total * 100) / $nTotalAnswer;
                    }
                    $oUserFeeds[$nKey]['pollanswer_'.$oPollOption->id_poll_option] = round($nPercent, 2);
                    $oPollOption->pollanswer = round($nPercent, 2);
                }
                $oUserFeeds[$nKey]->poll_options = $oPollOptions;
                
                if(!empty($oUserFeed->user_profile_image))
                    $oUserFeeds[$nKey]->user_profile_image = getUserImageUrl ($oUserFeed->user_profile_image, 50);
                
                $oUserFeeds[$nKey]->poll_image_size = array('width' => 0, 'height' => 0);
                if(!empty($oUserFeed->poll_file_name))
                {
                    $oUserFeeds[$nKey]->poll_file_name = setPostImage($oUserFeed->poll_file_name, 80);
                    $aImageSize = getimagesize($oUserFeeds[$nKey]->poll_file_name);
                    if(count($aImageSize) > 2)
                    {
                        $oUserFeeds[$nKey]->poll_image_size = array('width' => $aImageSize[0], 'height' => $aImageSize[1]);
                    }
                }
            }
            $aMessages['data'] = $oUserFeeds;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_polls';
            $aMessages['response_message'] = 'user_polls_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_polls';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callChangePassword(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oUser = Auth::user();
            
            $aValidationRequiredFor = [
                                            'current_password' => 'required',
                                            'new_password' => 'required|min:6',
                                            'repeat_new_password' => 'required|min:6|same:new_password'
                                        ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_change_password';
                $aMessages['response_message'] = 'user_change_password_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $bCheck = auth()->validate([
                                    'email'    => $oUser->email,
                                    'password' => $oRequest->current_password.$oUser->salt
                                ]);
            if(!$bCheck)
            {
                $aMessages['errors'] = array('current_password' => array(trans('messages.incorrect_current_password')));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_change_password';
                $aMessages['response_message'] = 'user_change_password_fail';
                
                return $this->prepareResponse($aMessages);
            }
            else if(($oRequest->current_password === $oRequest->new_password))
            {
                $aMessages['errors'] = array('new_password' => array(trans('messages.current_password_and_new_password_same')));
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'user_change_password';
                $aMessages['response_message'] = 'user_change_password_fail';
                
                return $this->prepareResponse($aMessages);
            }
            $dCurrentTime = Carbon::now();
            $sCurrentTime = $dCurrentTime->toDateTimeString();

            $sSalt = hash('md5', $oUser->email.$sCurrentTime);
            $sPassword = sha1($oRequest->new_password.$sSalt);

            $oUser->salt = $sSalt;
            $oUser->password = $sPassword;
            $oUser->update();
            
            UserSession::where('id_user', '=', Auth::user()->id_user)
                        ->when(!empty($oRequest->id_device), function ($query) use ($oRequest) {
                                    return $query->where('id_device', '!=', $oRequest->id_device);
                                })
                        ->update(array('logout_time' => $sCurrentTime));
            
            session(['login_again_required' => $sSalt]);
            
            $aMessages['data'] = $oUser;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_change_password';
            $aMessages['response_message'] = 'user_change_password_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_change_password';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserProfile(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nLoginUserId = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserDetail = User::find($nLoginUserId)->toArray();
            
            $oUserFollow = UserFollow::isUserFollowing(Auth::user()->id_user, $nLoginUserId)->toArray();
            $oUserDetail['id_user_follow'] = isset($oUserFollow[0]['id_user_follow']) ? $oUserFollow[0]['id_user_follow'] : NULL;
            
            $oUserDetail['follower_count'] = UserFollow::getUserFollowerCount($nLoginUserId);
            $oUserDetail['following_count'] = UserFollow::getUserFollowingCount($nLoginUserId);
            if($oUserDetail['user_profile_image'] != NULL)
                $oUserDetail['user_profile_image'] = getUserImageUrl($oUserDetail['user_profile_image'], 300);
            
            $oUserProfileData['user_educations'] = UserEducation::getUserEducation($nLoginUserId, 2);
            $oUserDetail['education_counts'] = UserEducation::where('id_user', $nLoginUserId)
                                                            ->where('activated', 1)
                                                            ->where('deleted', 0)
                                                            ->count();
            
            $oUserProfileData['user_work_experience'] = UserWorkExperience::getUserWorkExperience($nLoginUserId, 2);
            $oUserDetail['work_experience_count'] = UserWorkExperience::where('id_user', $nLoginUserId)
                                                                        ->where('activated', 1)
                                                                        ->where('deleted', 0)
                                                                        ->count();
            
            $oUserProfileData['user_organizations'] = UserOrganization::getUserOrganization($nLoginUserId, 2);
            $oUserDetail['organizations_count'] = UserOrganization::where('id_user', $nLoginUserId)
                                                                    ->where('activated', 1)
                                                                    ->where('deleted', 0)
                                                                    ->count();
            
            $oUserProfileData['user_award_honours'] = UserAwards::getUserAwards($nLoginUserId, 2);
            $oUserDetail['award_honours_count'] = UserAwards::where('id_user', $nLoginUserId)
                                                            ->where('activated', 1)
                                                            ->where('deleted', 0)
                                                            ->count();
            
            $oUserProfileData['user_publications'] = UserPublications::getUserPublication($nLoginUserId, 2);
            $oUserDetail['publications_count'] = UserPublications::where('id_user', $nLoginUserId)
                                                                ->where('activated', 1)
                                                                ->where('deleted', 0)
                                                                ->count();
            
            $oUserProfileData['user_research_work'] = UserResearch::getUserResearch($nLoginUserId, 2);
            $oUserDetail['research_work_count'] = UserResearch::where('id_user', $nLoginUserId)
                                                                ->where('activated', 1)
                                                                ->where('deleted', 0)
                                                                ->count();
            
            $oUserProfileData['user_journal_articals'] = UserJournals::getUserJournal($nLoginUserId, 2);
            $oUserDetail['journal_articals_count'] = UserJournals::where('id_user', $nLoginUserId)
                                                                ->where('activated', 1)
                                                                ->where('deleted', 0)
                                                                ->count();
            
            $oUserProfileData['user_detail'] = $oUserDetail;
            
            $aMessages['data'] = $oUserProfileData;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_profile';
            $aMessages['response_message'] = 'user_profile_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_profile';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserEducations(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserEducations = UserEducation::getUserEducation($nIdUser);
            
            $aMessages['data'] = $oUserEducations;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_education';
            $aMessages['response_message'] = 'user_education_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_education';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserWorkExperiences(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserWorkExperience = UserWorkExperience::getUserWorkExperience($nIdUser);
            
            $aMessages['data'] = $oUserWorkExperience;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_work_experience';
            $aMessages['response_message'] = 'user_work_experience_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_work_experience';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserOrganizations(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserOrganizations = UserOrganization::getUserOrganization($nIdUser);
            
            $aMessages['data'] = $oUserOrganizations;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_organization';
            $aMessages['response_message'] = 'user_organization_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_organization';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserAwards(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserAwards = UserAwards::getUserAwards($nIdUser);
            
            $aMessages['data'] = $oUserAwards;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_award_honour';
            $aMessages['response_message'] = 'user_award_honour_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_award_honour';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserPublications(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserPublications = UserPublications::getUserPublication($nIdUser);
            
            $aMessages['data'] = $oUserPublications;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_publication';
            $aMessages['response_message'] = 'user_publication_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_publication';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserResearchWorks(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserResearchWorks = UserResearch::getUserResearch($nIdUser);
            
            $aMessages['data'] = $oUserResearchWorks;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_research_work';
            $aMessages['response_message'] = 'user_research_work_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_research_work';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserJournals(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $oUserJournalArticals = UserJournals::getUserJournal($nIdUser);
            
            $aMessages['data'] = $oUserJournalArticals;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_journal_articals';
            $aMessages['response_message'] = 'user_journal_articals_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_journal_articals';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callEditUserProfile(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'first_name' => 'required',
                                        'last_name' => 'required',
                                        'alternate_email' => 'email'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'edit_user_profile';
                $aMessages['response_message'] = 'edit_user_profile_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $oLoggedinUser = Auth::user();
            $oLoggedinUser->first_name = $oRequest->first_name;
            $oLoggedinUser->last_name = $oRequest->last_name;
            $oLoggedinUser->alternate_email = $oRequest->alternate_email;
            $oLoggedinUser->save();
            
            $aMessages['data'] = $oLoggedinUser;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'edit_user_profile';
            $aMessages['response_message'] = 'edit_user_profile_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'edit_user_profile';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserFollowers(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $aUserFollowers = UserFollow::getUserFollower($nIdUser);
            foreach($aUserFollowers as $sKey=>$oUserFollower)
            {
                if($oUserFollower->user_profile_image != NULL)
                    $aUserFollowers[$sKey]->user_profile_image = getUserImageUrl($oUserFollower->user_profile_image, 50);
                
                $aUserFollowers[$sKey]->major = UserEducation::getMajorEducation($oUserFollower->id_user);
            }
            
            $aMessages['data'] = $aUserFollowers;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_followers_list';
            $aMessages['response_message'] = 'user_followers_list_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_followers_list';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUserFollowings(Request $oRequest, $nIdUser = '')
    {
        if($oRequest->isMethod("GET"))
        {
            $nIdUser = empty($nIdUser) ? Auth::user()->id_user : $nIdUser;
            
            $aUserFollowings = UserFollow::getUserFollowing($nIdUser);
            foreach($aUserFollowings as $sKey=>$oUserFollowing)
            {
                if($oUserFollowing->user_profile_image != NULL)
                    $aUserFollowings[$sKey]->user_profile_image = getUserImageUrl ($oUserFollowing->user_profile_image, 50);
                
                $aUserFollowings[$sKey]->major = UserEducation::getMajorEducation($oUserFollowing->id_user);
            }
            
            $aMessages['data'] = $aUserFollowings;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'user_following_list';
            $aMessages['response_message'] = 'user_following_list_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'user_following_list';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddUserCourse(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                                'selected_courses' => 'required'
                                                            ]);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_user_course';
                $aMessages['response_message'] = 'add_user_course_fail';
                
                return $this->prepareResponse($aMessages);
            }
            
            $aSelectedCourses = explode(",", $oRequest->selected_courses);
            foreach ($aSelectedCourses as $nIdCourseSelected)
            {
                $oUserCourse = UserCourse::firstOrNew([
                                                'id_user' => Auth::user()->id_user,
                                                'id_course' => $nIdCourseSelected
                                            ]);
                $oUserCourse->activated = 1;
                $oUserCourse->deleted = 0;
                $oUserCourse->save();
                
                //Create default group and make user member of that group
                if(isset($oRequest->become_member) && $oRequest->become_member)
                {
                    $oCourse = Course::where('id_course', $nIdCourseSelected)
                                        ->first();
                    
                    //Get campus-admin
                    $oCampusAdmin = User::where('id_campus', Auth::user()->id_campus)
                                            ->where('user_type', config('constants.USERTYPECAMPUSADMIN'))
                                            ->where('activated', 1)
                                            ->where('deleted', 0)
                                            ->first();
                    //Create group related to course_name
                    $oGroup = Group::firstOrNew([
                                                    'group_name' => $oCourse->course_name,
                                                    'id_group_category' => config('constants.COURSEGROUPCATEGORY'),
                                                    'id_campus' => Auth::user()->id_campus,
                                                    'group_type' => config('constants.PUBLICGROUP')
                                                ]);
                    $oGroup->id_user = $oCampusAdmin->id_user;
                    $oGroup->activated = 1;
                    $oGroup->deleted = 0;
                    $oGroup->save();
                    
                    //Make campus admin as creator in group_member table
                    $oGroupCreator = GroupMember::firstOrNew([
                                                    'id_group' => $oGroup->id_group,
                                                    'id_user' => $oCampusAdmin->id_user
                                                ]);
                    $oGroupCreator->member_type = config('constants.GROUPCREATOR');
                    $oGroupCreator->activated = 1;
                    $oGroupCreator->deleted = 0;
                    $oGroupCreator->save();
                    
                    //Make login user member to group
                    $oGroupMember = GroupMember::firstOrNew([
                                                    'id_group' => $oGroup->id_group,
                                                    'id_user' => Auth::user()->id_user
                                                ]);
                    $oGroupMember->member_type = config('constants.GROUPMEMBER');
                    $oGroupMember->activated = 1;
                    $oGroupMember->deleted = 0;
                    $oGroupMember->save();
                }
            }
            
            $aMessages['data'] = array('success_message' => trans('messages.user_course_added_successful'));
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_user_course';
            $aMessages['response_message'] = 'add_user_course_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_user_course';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callFollowUser(Request $oRequest, $nIdUser)
    {
        if($oRequest->isMethod("GET"))
        {
            $oUserFollow = UserFollow::firstOrNew([
                                                    'id_follower' => Auth::user()->id_user,
                                                    'id_following' => $nIdUser
                                                ]);
            $oUserFollow->save();

            $oNotification = Notification::create([
                                                    'id_user' => $nIdUser,
                                                    'id_entites' => Auth::user()->id_user.'_'.$nIdUser,
                                                    'entity_type_pattern' => 'U_UFL',
                                                    'notification_type' => config('constants.NOTIFICATIONUSERFOLLOW')
                                                ]);
            $oNotification->save();
            
            //Send push notification
            $this->dispatch(new SendUserFollowNotification($nIdUser, Auth::user()->id_user));
            
            $aMessages['data'] = array('success_message' => trans('messages.follow_user_successful'));
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'follow_user';
            $aMessages['response_message'] = 'follow_user_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'follow_user';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callUnfollowUser(Request $oRequest, $nIdUser)
    {
        if($oRequest->isMethod("GET"))
        {
            $oUserFollowing = UserFollow::where('id_follower', '=', Auth::user()->id_user)
                                            ->where('id_following', '=', $nIdUser)
                                            ->first();

            if($oUserFollowing)
            {
                $oUserFollowing->delete();
                
                $aMessages['data'] = array('success_message' => trans('messages.unfollow_user_successful'));
                $aMessages['errors'] = [];
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'unfollow_user';
                $aMessages['response_message'] = 'unfollow_user_success';
            }
            else
            {
                $aMessages['data'] = array('success_message' => trans('messages.unfollow_user_not_found'));
                $aMessages['errors'] = [];
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'unfollow_user';
                $aMessages['response_message'] = 'unfollow_user_fail';
            }
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'unfollow_user';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callChangeUserProfilePic(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'file' => 'required|image|mimes:jpg,jpeg,png|max:10000'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'change_profile_pic';
                $aMessages['response_message'] = 'change_profile_pic_fail';
                
                return $this->prepareResponse($aMessages);
            }
            //Get image file
            $oImageFile = $oRequest->file('file');
            
            $sFileName = time().'.'.$oImageFile->getClientOriginalExtension();
            
            $oImage = Image::make($oImageFile);
            $sImageStream = $oImage->stream();
            
            $oS3 = \Storage::disk('s3');
            $sCloudFilePath = '/'.config('constants.USERMEDIAFOLDER').'/' . $sFileName;
            $oS3->put($sCloudFilePath, $sImageStream->__toString(),'public');

            $aImageSizes = array(50,100,150);
            foreach($aImageSizes as $nImageThumbSize)
            {
                $sResizedImageName= $nImageThumbSize.'_'.$sFileName;
                
                $oOriginalImage = Image::make($oImageFile);
                $oResizedImage = $oOriginalImage->resize($nImageThumbSize*2, $nImageThumbSize*2);
                $sResizedImageStream = $oResizedImage->stream();
                
                $oS3 = \Storage::disk('s3');
                $sCloudFilePath = '/'.config('constants.USERMEDIAFOLDER').'/' . $sResizedImageName;
                $oS3->put($sCloudFilePath, $sResizedImageStream->__toString(), 'public');
            }
            
            UserProfileImages::create([
                                            'id_user' => Auth::user()->id_user,
                                            'file_name' => $sFileName,
                                            'activated' => 1,
                                            'deleted' => 0
                                        ]);
            
            $aMessages['data'] = ['message' => trans('messages.user_image_change_success')];
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'change_profile_pic';
            $aMessages['response_message'] = 'change_profile_pic_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'change_profile_pic';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callSubmitAttendance(Request $oRequest) 
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'course_id' => 'required',
                                        'section' => 'required',
                                        'attendance_code' => 'required'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'submit_attendance';
                $aMessages['response_message'] = 'submit_attendance_fail';

                return $this->prepareResponse($aMessages);
            }
                
            $oAttendance = AttendanceCode::getAttendanceCodeDetail($oRequest->attendance_code);
            if(count($oAttendance) && $oAttendance->id_course == $oRequest->course_id && $oAttendance->section == $oRequest->section)
            {
                $oUserAttendance = UserAttendance::firstOrNew([
                                                                'id_user' => Auth::user()->id_user,
                                                                'id_lecture' => $oAttendance->id_lecture
                                                            ]);
                $oUserAttendance->save();
                
                $aMessages['data'] = ['message' => trans('messages.attendance_submitted')];
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'submit_attendance';
                $aMessages['response_message'] = 'submit_attendance_success';

                return $this->prepareResponse($aMessages);
            }
            else
            {
                $aMessages['errors'] = ['message' => trans('messages.attendance_code_not_valid')];
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'submit_attendance';
                $aMessages['response_message'] = 'submit_attendance_fail';

                return $this->prepareResponse($aMessages);
            }
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'submit_attendance';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callStudentAttendanceList(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oUserGroups = GroupMember::getUserSelectiveGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'), '=');
            $aUserGroups = explode(",", $oUserGroups[0]->id_groups);

            $aLectures = GroupLecture::getGroupAttendanceList(Auth::user()->id_user, $aUserGroups);
            foreach($aLectures as $nKey => $oLecture)
            {
                $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
                $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
                $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oLecture, Auth::user()->id_user, "");
                
                $aLectures[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
                $aLectures[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
                $aLectures[$nKey]['all_lectures_count'] = $nAllLecturesCount;
            }
            $aMessages['data'] = $aLectures;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'attendance_listing';
            $aMessages['response_message'] = 'attendance_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'attendance_listing';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAllAttendanceOfLecture(Request $oRequest, $nIdLecture)
    {
        if($oRequest->isMethod("GET"))
        {
            $oLecture = Lecture::find($nIdLecture);
            
            $aAllLactures['lecture'] = Lecture::getAllSameLecture($oLecture, Auth::user()->id_user);
            $aAllLactures['course'] = Course::find($oLecture->id_course);
            
            $aMessages['data'] = $aAllLactures;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'attendance_detail';
            $aMessages['response_message'] = 'attendance_detail_success';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'attendance_detail';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callFacultyAttendanceList(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            if(Auth::user()->user_type == config('constants.USERTYPEFACULTY'))
            {
                $aAllUniqueLecture = Lecture::getFacultyUniqueLecture(Auth::user()->id_user);
                
                $aMessages['data'] = $aAllUniqueLecture;
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'faculty_attendance_list';
                $aMessages['response_message'] = 'faculty_attendance_list_success';
                
                return $this->prepareResponse($aMessages);
            }
            $aMessages['errors'] = array('not_allowed' => trans('messages.not_allowed_to_see_attendance'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'faculty_attendance_list';
            $aMessages['response_message'] = 'faculty_attendance_list_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'faculty_attendance_list';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callFacultyAttendanceDetail(Request $oRequest, $nIdLecture)
    {
        if($oRequest->isMethod("GET"))
        {
            if(Auth::user()->user_type == config('constants.USERTYPEFACULTY'))
            {
                $aFIlterParam['id_lecture'] = $nIdLecture;
                $aLecture = Lecture::getLectureDetail($aFIlterParam, Auth::user()->id_user);
                $oLecture = $aLecture[0];
                $aUserAttendanceList = UserAttendance::getAllUserForCourse($oLecture);
                
                foreach ($aUserAttendanceList as $nKey => $oUserAttendance)
                {
                    $nPresentLecturesCount = Lecture::getLecturesCount($oLecture, $oUserAttendance->id_user, config('constants.ATTENDANCETYPEPRESENT'));
                    $nSickLeaveLecturesCount = Lecture::getLecturesCount($oLecture, $oUserAttendance->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
                    $nAllLecturesCount = Lecture::getLecturesCount($oLecture, $oUserAttendance->id_user, "");

                    $aUserAttendanceList[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
                    $aUserAttendanceList[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
                    $aUserAttendanceList[$nKey]['all_lectures_count'] = $nAllLecturesCount;
                    
                    if($oUserAttendance->user_profile_image != null)
                        $aUserAttendanceList[$nKey]->user_profile_image = getUserImageUrl ($oUserAttendance->user_profile_image, 50);
                }
                
                $aMessages['data'] = $aUserAttendanceList;
                $aMessages['errors'] = array();
                $aMessages['response_status'] = config('constants.APISUCCESS');
                $aMessages['request_type'] = 'faculty_attendance_detail';
                $aMessages['response_message'] = 'faculty_attendance_detail_success';
                
                return $this->prepareResponse($aMessages);
            }
            $aMessages['errors'] = array('not_allowed' => trans('messages.not_allowed_to_see_attendance'));
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'faculty_attendance_detail';
            $aMessages['response_message'] = 'faculty_attendance_detail_fail';
            
            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'faculty_attendance_detail';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddPaymentDetail(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_transaction' => 'required|unique:user_payment_details,id_transaction,'.Auth::user()->id_user.',id_user',
                                        'contract_start_on' => 'required|date',
                                        'contract_end_on' => 'required|date'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_payment_detail';
                $aMessages['response_message'] = 'add_payment_detail_fail';
                
                return $this->prepareResponse($aMessages);
            }
            //Add user payment details
            $oUserPaymentDetails = UserPaymentDetail::firstOrNew([
                                                                'id_user' => Auth::user()->id_user
                                                            ]);
            $oUserPaymentDetails->id_transaction = $oRequest->id_transaction;
            $oUserPaymentDetails->device_type = (base64_decode($oRequest->header('User-Agent')) == "iOS-app") ? "I" : "A";
            $oUserPaymentDetails->contract_start_on = $oRequest->contract_start_on;
            $oUserPaymentDetails->contract_end_on = $oRequest->contract_end_on;
            $oUserPaymentDetails->activated = 1;
            $oUserPaymentDetails->deleted = 0;
            $oUserPaymentDetails->save();
            
            $aMessages['data'] = $oUserPaymentDetails;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_payment_detail';
            $aMessages['response_message'] = 'add_payment_detail_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_payment_detail';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callAddContentReport(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'id_entity' => 'required',
                                        'entity_type' => 'required',
                                        'report_description' => 'required'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'add_report';
                $aMessages['response_message'] = 'add_report_fail';
                
                return $this->prepareResponse($aMessages);
            }
            //Add user payment details
            $oUserReport = UserReportedContent::firstOrNew([
                                                                'id_user' => Auth::user()->id_user,
                                                                'id_entity' => $oRequest->id_entity,
                                                                'entity_type' => $oRequest->entity_type
                                                            ]);
            $oUserReport->report_description = $oRequest->report_description;
            $oUserReport->activated = 1;
            $oUserReport->deleted = 0;
            $oUserReport->save();
            
            //Get count and delete entity if count is greater.
            $nReportCounts = UserReportedContent::where('id_entity',$oRequest->id_entity)
                                                ->where('entity_type', $oRequest->entity_type)
                                                ->count();
            if($nReportCounts >= config('constants.MAXALLOWEDREPORTS'))
            {
                switch (mb_strtoupper($oRequest->entity_type))
                {
                    case "P":
                        $oPost = Post::find($oRequest->id_entity);
                        $oPost->activated = 0;
                        $oPost->deleted = 1;
                        $oPost->save();
                        break;
                    case "C":
                        $oComment = Comment::find($oRequest->id_entity);
                        $oComment->activated = 0;
                        $oComment->deleted = 1;
                        $oComment->save();
                        break;
                    default:
                        break;
                }
            }
                    
            $aMessages['data'] = array('success_message' => trans('messages.report_added_successful'));
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'add_report';
            $aMessages['response_message'] = 'add_report_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'add_report';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }

    public function callSyncUserData(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'updated_at' => 'required'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'sync_user_data';
                $aMessages['response_message'] = 'sync_user_data_fail';
                
                return $this->prepareResponse($aMessages);
            }
            //Get user follows
            $oUsers = User::getUsersFromCampus(Auth::user()->id_campus, $oRequest->updated_at);
            foreach($oUsers as $sKey=>$oUser)
            {
                if($oUser->user_profile_image != NULL)
                    $oUsers[$sKey]->user_image = getUserImageUrl ($oUser->user_profile_image, 50);
                else
                    $oUsers[$sKey]->user_image = '';
            }
            
            $aOptionalConditions = array();
            $aOptionalConditions['member_types'] = [config('constants.GROUPCREATOR'), config('constants.GROUPADMIN'),config('constants.GROUPMEMBER')];
            
            $oGroups = Group::getSyncGroups(Auth::user()->id_user, $oRequest->updated_at, $aOptionalConditions);
            foreach($oGroups as $sKey=>$oGroup)
            {
                if($oGroup->file_name != NULL)
                    $oGroups[$sKey]->group_image = getGroupImageUrl ($oGroup->file_name, 50);
                else
                    $oGroups[$sKey]->group_image = '';
            }
            
            $dCurrentTime = Carbon::now();
            $sCurrentTime = $dCurrentTime->toDateTimeString();
            
            $aMessages['data']['users'] = $oUsers;
            $aMessages['data']['groups'] = $oGroups;
            $aMessages['last_synced_at'] = $sCurrentTime;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'sync_user_data';
            $aMessages['response_message'] = 'sync_user_data_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'sync_user_data';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callSyncCourseData(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'course_updated_at' => 'required'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
            if($oValidator->fails())
            {
                $aMessages['errors'] = $oValidator->errors();
                $aMessages['response_status'] = config('constants.APIERROR');
                $aMessages['request_type'] = 'sync_course_data';
                $aMessages['response_message'] = 'sync_course_data_fail';
                
                return $this->prepareResponse($aMessages);
            }
            //Get user follows
            $aCourses = Course::getCourseListingForSync(Auth::user()->id_university, $oRequest->course_updated_at);
            
            $dCurrentTime = Carbon::now();
            $sCurrentTime = $dCurrentTime->toDateTimeString();
            
            $aMessages['data'] = $aCourses;
            $aMessages['course_last_synced_at'] = $sCurrentTime;
            $aMessages['errors'] = [];
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'sync_course_data';
            $aMessages['response_message'] = 'sync_course_data_success';

            return $this->prepareResponse($aMessages);
        }
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'sync_course_data';
        $aMessages['response_message'] = 'not_valid_method';
        
        return $this->prepareResponse($aMessages);
    }
}
