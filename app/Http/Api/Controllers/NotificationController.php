<?php

namespace App\Http\Api\Controllers;

use App\Http\Api\Controllers\Controller;

use App\Libraries\GroupRequest;
use App\Libraries\EventRequest;

use App\Libraries\Notification;
use App\Libraries\ReadNotification;

use Auth;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function __construct(Request $oRequest)
    {
        parent::__construct($oRequest);
        $this->middleware('auth', ['only' => [
                                                "callNotificationListing",
                                                "callGroupNotification", "callEventNotification",
                                                "callOtherNotification"
                                            ]
                                    ]);
    }
    
    public function callNotificationListing(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $oGroupInviteNotifications = array();
            $oEventInviteNotifications = array();
            $aNotificationsDetails = array();

            $nPageNumber = isset($oRequest->page) ? intval($oRequest->page) : 1;
            $nLimit = config('constants.PERPAGERECORDS');

            $nGroupRequestCount = GroupRequest::getGroupPendingRequestCount(Auth::user()->id_user);
            $nEventRequestCount = EventRequest::getEventPendingRequestCount(Auth::user()->id_user);
            $nInformativeNotificationCount = Notification::where('id_user', '=', Auth::user()->id_user)
                                                            ->count();
            
            if($nGroupRequestCount > (($nPageNumber-1) * $nLimit))
            {
                //Get max 10 group requests
                $oGroupInviteNotifications = GroupRequest::getLoginUserGroupRequests(Auth::user()->id_user, (($nPageNumber - 1) * config('constants.PERPAGERECORDS')), $nLimit);
            }

            //If group requests are less then 10 fetch event requests
            if(($nEventRequestCount + $nGroupRequestCount) > (($nPageNumber-1) * $nLimit))
            {
                if(count($oGroupInviteNotifications) < config('constants.PERPAGERECORDS'))
                {
                    $nSkipRecords = (($nPageNumber-1)*config('constants.PERPAGERECORDS')) - $nGroupRequestCount;
                    $nSkipRecords = max(0, $nSkipRecords);
                    
                    $nLimit = config('constants.PERPAGERECORDS') - count($oGroupInviteNotifications);
                    $oEventInviteNotifications = EventRequest::getEventRequest(Auth::user()->id_user, $nSkipRecords, $nLimit);
                }
            }
            
            //If group + event request are less then 10 fetch informative notifications
            if((count($oGroupInviteNotifications) + count($oEventInviteNotifications)) < config('constants.PERPAGERECORDS'))
            {
                $nSkipRecords = (($nPageNumber-1)*config('constants.PERPAGERECORDS')) - ($nGroupRequestCount+$nEventRequestCount);
                $nSkipRecords = max(0, $nSkipRecords);
                    
                $nLimit = config('constants.PERPAGERECORDS') - (count($oGroupInviteNotifications) + count($oEventInviteNotifications));

                $oInformativeNotifications = Notification::getUserInformativeNotification(Auth::user()->id_user, $nSkipRecords, $nLimit);
                $oUnreadNotifications = Notification::getUnreadNotifications(Auth::user()->id_user, $nSkipRecords, $nLimit);
                
                foreach ($oUnreadNotifications as $oUnreadNotification)
                {
                    ReadNotification::create([
                                                'id_notification' => $oUnreadNotification->id_notification
                                            ]);
                }
                
                $nCount = 0;
                foreach($oInformativeNotifications as $oInformativeNotification)
                {
                    $aEntityDetails = Notification::getEntitiesFromPattern($oInformativeNotification->entity_type_pattern, $oInformativeNotification->id_entites);
                    $aNotificationsDetails[$nCount]['notification_text'] = $this->prepareNotificationText($oInformativeNotification, $aEntityDetails);
                    $aNotificationsDetails[$nCount]['id_user'] = $aEntityDetails['user']->id_user;
                    $aNotificationsDetails[$nCount]['first_name'] = $aEntityDetails['user']->first_name;
                    $aNotificationsDetails[$nCount]['last_name'] = $aEntityDetails['user']->last_name;
                    $aNotificationsDetails[$nCount]['user_profile_image'] = getUserImageUrl($aEntityDetails['user']->file_name, 50);
                    $aNotificationsDetails[$nCount]['notification_type'] = $oInformativeNotification->notification_type;
                    $aNotificationsDetails[$nCount]['entity_type_pattern'] = $oInformativeNotification->entity_type_pattern;
                    $aNotificationsDetails[$nCount]['id_entites'] = $oInformativeNotification->id_entites;
                    $aNotificationsDetails[$nCount]['created_at'] = $oInformativeNotification->created_at;
                    $aNotificationsDetails[$nCount]['updated_at'] = $oInformativeNotification->updated_at;
                    
                    $nCount ++;
                }
            }
            
            $nTotalRecords = $nGroupRequestCount + $nEventRequestCount + $nInformativeNotificationCount;
            $nLastPage = floor($nTotalRecords/config('constants.PERPAGERECORDS')) + (($nTotalRecords%config('constants.PERPAGERECORDS')) ? 1 : 0);
            
            $aMessages['data']['total'] = $nTotalRecords;
            $aMessages['data']['per_page'] = config('constants.PERPAGERECORDS');
            $aMessages['data']['current_page'] = $nPageNumber;
            $aMessages['data']['last_page'] = $nLastPage;
            $aMessages['data']['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url()."?page=".($nPageNumber+1) : null;
            $aMessages['data']['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url()."?page=".($nPageNumber-1) : null;
            $aMessages['data']['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber-1) * config('constants.PERPAGERECORDS')) + 1 : null;
            $aMessages['data']['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
            
            $aMessages['data']['group_requests'] = $oGroupInviteNotifications;
            $aMessages['data']['event_requests'] = $oEventInviteNotifications;
            $aMessages['data']['others'] = $aNotificationsDetails;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'notification_listing';
            $aMessages['response_message'] = 'notification_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'notification_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callGroupNotification(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $nPageNumber = isset($oRequest->page) ? intval($oRequest->page) : 1;
            $nLimit = config('constants.PERPAGERECORDS');

            $nGroupRequestCount = GroupRequest::getGroupPendingRequestCount(Auth::user()->id_user);
            $nEventRequestCount = EventRequest::getEventPendingRequestCount(Auth::user()->id_user);
            $nInformativeNotificationCount = Notification::getUnreadNotificationCount(Auth::user()->id_user);
            
            //Get max 10 group requests
            $aGroupInviteNotifications = GroupRequest::getLoginUserGroupRequests(Auth::user()->id_user, (($nPageNumber - 1) * config('constants.PERPAGERECORDS')), $nLimit);
            foreach ($aGroupInviteNotifications as $key => $oGroupInviteNotification)
            {
                if($oGroupInviteNotification->user_profile_image != NULL)
                    $aGroupInviteNotifications[$key]['user_profile_image'] = getUserImageUrl ($oGroupInviteNotification->user_profile_image, 50);
            }
            
            $nTotalRecords = $nGroupRequestCount;
            $nLastPage = floor($nTotalRecords/config('constants.PERPAGERECORDS')) + (($nTotalRecords%config('constants.PERPAGERECORDS')) ? 1 : 0);
            
            $aMessages['data']['total'] = $nTotalRecords;
            $aMessages['data']['per_page'] = config('constants.PERPAGERECORDS');
            $aMessages['data']['current_page'] = $nPageNumber;
            $aMessages['data']['last_page'] = $nLastPage;
            $aMessages['data']['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url()."?page=".($nPageNumber+1) : null;
            $aMessages['data']['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url()."?page=".($nPageNumber-1) : null;
            $aMessages['data']['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber-1) * config('constants.PERPAGERECORDS')) + 1 : null;
            $aMessages['data']['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
            
            $aMessages['data']['group_requests'] = $aGroupInviteNotifications;
            $aMessages['data']['group_requests_count'] = $nGroupRequestCount;
            $aMessages['data']['event_requests_count'] = $nEventRequestCount;
            $aMessages['data']['other_notification_count'] = $nInformativeNotificationCount;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'group_notification_listing';
            $aMessages['response_message'] = 'group_notification_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'group_notification_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callEventNotification(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $nPageNumber = isset($oRequest->page) ? intval($oRequest->page) : 1;
            $nLimit = config('constants.PERPAGERECORDS');

            $nGroupRequestCount = GroupRequest::getGroupPendingRequestCount(Auth::user()->id_user);
            $nEventRequestCount = EventRequest::getEventPendingRequestCount(Auth::user()->id_user);
            $nInformativeNotificationCount = Notification::getUnreadNotificationCount(Auth::user()->id_user);

            $aEventInviteNotifications = EventRequest::getEventRequest(Auth::user()->id_user, ($nPageNumber-1)*config('constants.PERPAGERECORDS'), $nLimit);
            foreach ($aEventInviteNotifications as $key => $oEventInviteNotification)
            {
                if($oEventInviteNotification->user_profile_image != NULL)
                    $aEventInviteNotifications[$key]['user_profile_image'] = getUserImageUrl ($oEventInviteNotification->user_profile_image, 50);
            }
            
            $nTotalRecords = $nEventRequestCount;
            $nLastPage = floor($nTotalRecords/config('constants.PERPAGERECORDS')) + (($nTotalRecords%config('constants.PERPAGERECORDS')) ? 1 : 0);
            
            $aMessages['data']['total'] = $nTotalRecords;
            $aMessages['data']['per_page'] = config('constants.PERPAGERECORDS');
            $aMessages['data']['current_page'] = $nPageNumber;
            $aMessages['data']['last_page'] = $nLastPage;
            $aMessages['data']['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url()."?page=".($nPageNumber+1) : null;
            $aMessages['data']['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url()."?page=".($nPageNumber-1) : null;
            $aMessages['data']['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber-1) * config('constants.PERPAGERECORDS')) + 1 : null;
            $aMessages['data']['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
            
            $aMessages['data']['event_requests'] = $aEventInviteNotifications;
            $aMessages['data']['group_requests_count'] = $nGroupRequestCount;
            $aMessages['data']['event_requests_count'] = $nEventRequestCount;
            $aMessages['data']['other_notification_count'] = $nInformativeNotificationCount;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'event_notification_listing';
            $aMessages['response_message'] = 'event_notification_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'event_notification_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    public function callOtherNotification(Request $oRequest)
    {
        if($oRequest->isMethod("GET"))
        {
            $aNotificationsDetails = array();
            
            $nPageNumber = isset($oRequest->page) ? intval($oRequest->page) : 1;
            $nLimit = config('constants.PERPAGERECORDS');

            $nGroupRequestCount = GroupRequest::getGroupPendingRequestCount(Auth::user()->id_user);
            $nEventRequestCount = EventRequest::getEventPendingRequestCount(Auth::user()->id_user);
            $nUnreadNotificationCount = Notification::getUnreadNotificationCount(Auth::user()->id_user);

            $oInformativeNotifications = Notification::getUserInformativeNotification(Auth::user()->id_user, ($nPageNumber-1)*config('constants.PERPAGERECORDS'), $nLimit);
            $oUnreadNotifications = Notification::getUnreadNotifications(Auth::user()->id_user, ($nPageNumber-1)*config('constants.PERPAGERECORDS'), $nLimit);

            foreach ($oUnreadNotifications as $oUnreadNotification)
            {
                ReadNotification::create([
                                            'id_notification' => $oUnreadNotification->id_notification
                                        ]);
            }

            $nCount = 0;
            foreach($oInformativeNotifications as $oInformativeNotification)
            {
                $aEntityDetails = Notification::getEntitiesFromPattern($oInformativeNotification->entity_type_pattern, $oInformativeNotification->id_entites);
                $aNotificationsDetails[$nCount]['notification_text'] = $this->prepareNotificationText($oInformativeNotification, $aEntityDetails);
                $aNotificationsDetails[$nCount]['id_user'] = $aEntityDetails['user']->id_user;
                $aNotificationsDetails[$nCount]['first_name'] = $aEntityDetails['user']->first_name;
                $aNotificationsDetails[$nCount]['last_name'] = $aEntityDetails['user']->last_name;
                $aNotificationsDetails[$nCount]['user_profile_image'] = ($aEntityDetails['user']->file_name != NULL) ? getUserImageUrl($aEntityDetails['user']->file_name, 50) : $aEntityDetails['user']->file_name;
                $aNotificationsDetails[$nCount]['notification_type'] = $oInformativeNotification->notification_type;
                $aNotificationsDetails[$nCount]['entity_type_pattern'] = $oInformativeNotification->entity_type_pattern;
                $aNotificationsDetails[$nCount]['id_entites'] = $oInformativeNotification->id_entites;
                $aNotificationsDetails[$nCount]['created_at'] = $oInformativeNotification->created_at->format('Y-m-d H:i:s');
                $aNotificationsDetails[$nCount]['updated_at'] = $oInformativeNotification->updated_at->format('Y-m-d H:i:s');

                $nCount ++;
            }
            
            $nTotalRecords = Notification::where('id_user', '=', Auth::user()->id_user)
                                            ->count();
            $nLastPage = floor($nTotalRecords/config('constants.PERPAGERECORDS')) + (($nTotalRecords%config('constants.PERPAGERECORDS')) ? 1 : 0);
            
            $aMessages['data']['total'] = $nTotalRecords;
            $aMessages['data']['per_page'] = config('constants.PERPAGERECORDS');
            $aMessages['data']['current_page'] = $nPageNumber;
            $aMessages['data']['last_page'] = $nLastPage;
            $aMessages['data']['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url()."?page=".($nPageNumber+1) : null;
            $aMessages['data']['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url()."?page=".($nPageNumber-1) : null;
            $aMessages['data']['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber-1) * config('constants.PERPAGERECORDS')) + 1 : null;
            $aMessages['data']['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
            
            $aMessages['data']['other_notifications'] = $aNotificationsDetails;
            $aMessages['data']['group_requests_count'] = $nGroupRequestCount;
            $aMessages['data']['event_requests_count'] = $nEventRequestCount;
            $aMessages['data']['other_notification_count'] = $nUnreadNotificationCount;
            $aMessages['errors'] = array();
            $aMessages['response_status'] = config('constants.APISUCCESS');
            $aMessages['request_type'] = 'other_notification_listing';
            $aMessages['response_message'] = 'other_notification_listing_success';
            
            return $this->prepareResponse($aMessages);
        }
        
        $aMessages['response_status'] = config('constants.APIFAIL');
        $aMessages['request_type'] = 'other_notification_listing';
        $aMessages['response_message'] = 'not_valid_method';
        $aMessages['errors'] = array();
        
        return $this->prepareResponse($aMessages);
    }
    
    private function prepareNotificationText($oNotification, $aEntityDetails)
    {
        $sNotificationText = "";
        
        switch ($oNotification->notification_type)
        {
            case config('constants.NOTIFICATIONUSERFOLLOW'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                $sNotificationText = "<a href='user://".$oUser->id_user."'>".trans('messages.'.config('constants.NOTIFICATIONUSERFOLLOW'), ['user' => $sUserLink])."</a>";
                break;
            
            case config('constants.NOTIFICATIONCOMMENTONEVENT'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oEvent = $aEntityDetails['event'];
                $sEventLink = "<span>".applySubStr($oEvent->event_title, 50)."</span>";
                
                $sNotificationText = "<a href='event://".$oEvent->id_event."'>".trans('messages.'.config('constants.NOTIFICATIONCOMMENTONEVENT'), ['user' => $sUserLink, 'event' => $sEventLink])."</a>";
                break;
            
            case config('constants.NOTIFICATIONEDITEVENT'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oEvent = $aEntityDetails['event'];
                $sEventLink = "<span>".applySubStr($oEvent->event_title, 50)."</span>";
                
                $sNotificationText = "<a href='event://".$oEvent->event_title.config('constants.APIDEVELOPERSTRING').$oEvent->id_event."'>".trans('messages.'.config('constants.NOTIFICATIONEDITEVENT'), ['user' => $sUserLink, 'event' => $sEventLink]). "</a>";
                break;
            
            case config('constants.NOTIFICATIONCANCELEVENT'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oEvent = $aEntityDetails['event'];
                $sEventLink = "<span>".applySubStr($oEvent->event_title, 50)."</span>";
                
                $sNotificationText = trans('messages.'.config('constants.NOTIFICATIONCANCELEVENT'), ['user' => $sUserLink, 'event' => $sEventLink]);
                break;
            
            case config('constants.NOTIFICATIONNEWADMIN'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oGroup = $aEntityDetails['group'];
                $sGroupLink = "<span>".$oGroup->group_name."</span>";
                
                $sNotificationText = "<a href='group://".$oGroup->group_name.config('constants.APIDEVELOPERSTRING').$oGroup->id_group."'>".trans('messages.new_group_admin',['group'=>$sGroupLink,'user'=>$sUserLink]). "</a>";
                break;
            
            case config('constants.NOTIFICATIONNEWGROUPPOST'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oGroup = $aEntityDetails['group'];
                $sGroupLink = "<span>".$oGroup->group_name."</span>";
                
                $oPost = $aEntityDetails['post'];
                $sPostMessage = trans('messages.'.config('constants.NOTIFICATIONNEWGROUPPOST'), ['user' => $sUserLink, 'group' => $sGroupLink]);
                if($oPost->post_type == config('constants.POSTTYPEPOLL'))
                {
                    $sPostMessage = trans('messages.new_group_poll',['user' => $sUserLink, 'group' => $sGroupLink]);
                }
                
                $sNotificationText = "<a href='post://".$oPost->id_post."'>".$sPostMessage. "</a>";
                break;
            
            case config('constants.NOTIFICATIONCOMMENTONPOST'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oPost = $aEntityDetails['post'];
                $sPostLink = "<span>".applySubStr(!empty(trim($oPost->post_text)) ? $oPost->post_text : 'your', 50)."</span>";
                
                $sNotificationText = "<a href='post://".$oPost->id_post."'>".trans('messages.'.config('constants.NOTIFICATIONCOMMENTONPOST'), ['user' => $sUserLink, 'post' => $sPostLink])."</a>";
                break;
            
            case config('constants.NOTIFICATIONLIKEONPOST'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oPost = $aEntityDetails['post'];
                $sPostLink = "<span>".applySubStr(!empty(trim($oPost->post_text)) ? $oPost->post_text : 'your', 50)."</span>";
                
                $sNotificationText = "<a href='post://".$oPost->id_post."'>".trans('messages.'.config('constants.NOTIFICATIONLIKEONPOST'), ['user' => $sUserLink, 'post' => $sPostLink])."</a>";
                break;
            
            case config('constants.NOTIFICATIONGROUPDOCUMENT'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oGroup = $aEntityDetails['group'];
                $sGroupLink = "<span>".$oGroup->group_name."</span>";
                
                $oDocument= $aEntityDetails['document'];
                $sDocumentLink = "<span>".$oDocument->document_name."</span>";
                
                $sNotificationText = "<a href='document://".$oDocument->document_name.config('constants.APIDEVELOPERSTRING').$oDocument->id_document."'>".trans('messages.'.config('constants.NOTIFICATIONGROUPDOCUMENT').'_folder', ['user' => $sUserLink, 'document' => $sDocumentLink, 'group' => $oGroup->group_name])."</a>";
                
                if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                {
                    $sFileUrl = urlencode(config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name);
                    $sNotificationText = "<a href='document_url://".$oDocument->document_name.config('constants.APIDEVELOPERSTRING').$sFileUrl."'>".trans('messages.'.config('constants.NOTIFICATIONGROUPDOCUMENT'), ['user' => $sUserLink, 'document' => $sDocumentLink, 'group' => $oGroup->group_name])."</a>";
                }
                break;
            
            case config('constants.NOTIFICATIONUSERDOCUMENT'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oDocument= $aEntityDetails['document'];
                $sDocumentLink = "<span>".$oDocument->document_name."</span>";
                
                $sNotificationText = "<a href='document://".$oDocument->document_name.config('constants.APIDEVELOPERSTRING').$oDocument->id_document."'>".trans('messages.'.config('constants.NOTIFICATIONUSERDOCUMENT').'_folder', ['user' => $sUserLink, 'document' => $sDocumentLink])."</a>";
                
                if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                {
                    $sFileUrl = urlencode(config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oDocument->file_name);
                    $sNotificationText = "<a href='document_url://".$oDocument->document_name.config('constants.APIDEVELOPERSTRING').$sFileUrl."'>".trans('messages.'.config('constants.NOTIFICATIONUSERDOCUMENT'), ['user' => $sUserLink, 'document' => $sDocumentLink])."</a>";
                }
                break;
            
            case config('constants.NOTIFICATIONUSERATTENDANCECHANGE'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oCourse = $aEntityDetails['course'];
                $oAttendanceLog = $aEntityDetails['attendance_log'];
                
                $sNotificationText = trans('messages.'.config('constants.NOTIFICATIONUSERATTENDANCECHANGE'),['user'=>$sUserLink,'from'=>$oAttendanceLog->change_from,'to'=>$oAttendanceLog->change_to,'course'=>$oCourse->course_name]);
                break;
            
            case config('constants.NOTIFICATIONGROUPATTENDANCECHANGE'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oGroup = $aEntityDetails['group'];
                $oAttendanceLog = $aEntityDetails['group_attendance_log'];
                
                $sNotificationText = trans('messages.'.config('constants.NOTIFICATIONUSERATTENDANCECHANGE'),['user'=>$sUserLink,'from'=>$oAttendanceLog->change_from,'to'=>$oAttendanceLog->change_to,'course'=>$oGroup->group_name]);
                break;
            case config('constants.NOTIFICATIONGROUPQUIZ'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oGroup = $aEntityDetails['group'];
                $sGroupLink = "<span>".$oGroup->group_name."</span>";
                
                $oQuiz = $aEntityDetails['quiz'];
                $sQuizLink = "<span>".$oQuiz->quiz_title."</span>";
                
                $sNotificationText = trans('messages.'.config('constants.NOTIFICATIONGROUPQUIZ'),['name' => $sUserLink, 'quiz'=> $sQuizLink, 'group' => $sGroupLink]);
                break;
            case config('constants.NOTIFICATIONGROUPQUIZDELETE'):
                $oUser = $aEntityDetails['user'];
                $sUserLink = "<span>".$oUser->first_name." ".$oUser->last_name."</span>";
                
                $oGroup = $aEntityDetails['group'];
                $sGroupLink = "<span>".$oGroup->group_name."</span>";
                
                $oQuiz = $aEntityDetails['quiz'];
                $sQuizLink = "<span>".$oQuiz->quiz_title."</span>";
                
                $sNotificationText = trans('messages.'.config('constants.NOTIFICATIONGROUPQUIZDELETE'),['name' => $sUserLink, 'quiz'=> $sQuizLink, 'group' => $sGroupLink]);
                break;
            default :
                break;
        }
        
        return $sNotificationText;
    }
}