<?php

namespace App\Http\Vue\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Auth;
use App\Libraries\Notification;
use App\Libraries\GroupRequest;
use App\Libraries\EventRequest;
use App\Libraries\Campus;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected $nNotificationCount = 0;
    protected $sCampusImage = '';
    
    public function __construct()
    {
        $this->getNotificationCount();
        $this->getCampusImage();
    }
    
    protected function getNotificationCount()
    {
        if(Auth::user())
        {
            $this->nNotificationCount = GroupRequest::getGroupPendingRequestCount(Auth::user()->id_user);
            $this->nNotificationCount += EventRequest::getEventPendingRequestCount(Auth::user()->id_user);
            $this->nNotificationCount += Notification::getUnreadNotificationCount(Auth::user()->id_user);
            
            // Share this property with all the views in your application.
            view()->share('nNotificationCount', $this->nNotificationCount);
        }
    }
    
    protected function getCampusImage()
    {
        if(Auth::user())
        {
            $oCampus = Campus::find(Auth::user()->id_campus);
            $this->sCampusImage = !empty($oCampus->file_name) ? url('/assets/web/img/'.$oCampus->file_name ): 'none';
            view()->share('sCampusImage', $this->sCampusImage);
        }
    }
}
