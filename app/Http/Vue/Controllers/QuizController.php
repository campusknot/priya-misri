<?php 

namespace App\Http\Web\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;

use App\Libraries\Quiz;
use App\Libraries\QuizQuestion;
use App\Libraries\AnswerOption;
use App\Libraries\CorrectAnswer;
use App\Libraries\QuizGroupInvite;
use App\Libraries\QuizUserInvite;
use App\Libraries\UserAnswer;
use App\Libraries\UserAttempt;
use App\Libraries\User;
use App\Libraries\Group;
use App\Libraries\GroupMember;

use Auth;
use Carbon\Carbon;

use App\Jobs\SendQuizGroupInvite;

class QuizController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => [
                                                'callAddQuiz', 'callAddQuizQuestion',
                                                'callAdminQuizList', 'callAdminQuizListAjax',
                                                'callAdminCompletedQuizList','callAdminCompletedQuizListAjax',
                                                'callMemberQuizList', 'callMemberQuizListAjax' ,
                                                'callMemberCompletedQuizList','callMemberCompletedQuizListAjax', 
                                                'callMemberQuizDetail','callQuizQuestions', 
                                                'callQuizUserList', 'callMemberCompletedQuiz','callMemberCompletedQuizDetail', 'callAddStudentMarks',
                                                'callAddStudentAnswer' ,'callEndQuiz','callStartQuiz','callDeleteAnswerOption','callDeleteQuiz',
                                                'callEditQuizPoint','callEditQuizQuestionPoint','callQuizPublish'
                                            ]
                        ]);
        
        parent::__construct();
        session(['current_page' => 'quiz']);
        $aCustomCookies['quiz'] = TRUE;
        // for new feature lable
        Cookie::queue(Cookie::forever('quiz', json_encode($aCustomCookies)));
    }
    
    public function callAddQuiz(Request $oRequest,$nIdQuiz='')
    {   
        $nIdGroupCategory = config('constants.COURSEGROUPCATEGORY');//course group
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPCREATOR'), config('constants.GROUPADMIN')];
        
        $oGroupList = Group::getAttendanceGroupSuggestion('',Auth::user()->id_user,'admin',config('constants.COURSEGROUPCATEGORY'));
        //$oGroupList = Group::getGroupList(Auth::user()->id_user, $nIdGroupCategory, $aOptionalConditions);

        ///this code is for replace only form of quiz not other question data below the quiz
        if($oRequest->error_page == 1)
            return \View::make('WebView::quiz._add_quiz',compact('oGroupList'));

        if($oRequest->isMethod("POST"))
        {
            $dToday = Carbon::now();
            $sToday = $dToday->toDateTimeString();
            if(!empty($oRequest->start_date) && !empty($oRequest->end_date))
            {
                $sStartDate = $oRequest->start_date;
                $aStartDateData = explode('-',$sStartDate);

                //this condition add for time like 00:15
                if($oRequest->start_ampm == 'am' && $oRequest->start_hour == 12){
                    $oRequest->offsetSet('start_hour', '00' );
                }
                if($oRequest->end_ampm == 'am' && $oRequest->end_hour == 12){
                    $oRequest->offsetSet('end_hour', '00' );
                }

                $dStartDate = Carbon::create($aStartDateData[2], $aStartDateData[0], $aStartDateData[1], 
                                                !empty($oRequest->start_hour) ? $oRequest->start_hour : '00',
                                                !empty($oRequest->start_minute) ? $oRequest->start_minute : '00', '00',Auth::user()->timezone);
                if($oRequest->start_ampm == 'pm' && $oRequest->start_hour != 12){
                    $dStartDate->addHour(12);
                }
                $dStartDate->setTimezone('UTC');

                $sEndDate = $oRequest->end_date;
                $aEndDateData = explode('-',$sEndDate);
                $dEndDate = Carbon::create($aEndDateData[2], $aEndDateData[0], $aEndDateData[1],
                                                !empty($oRequest->end_hour) ? $oRequest->end_hour : '00',
                                                !empty($oRequest->end_minute) ? $oRequest->end_minute : '00','00', Auth::user()->timezone);
                if($oRequest->end_ampm == 'pm' && $oRequest->end_hour != 12){
                    $dEndDate->addHour(12);
                }
                $dEndDate->setTimezone('UTC');

                $oRequest->offsetSet('start_date', $dStartDate->toDateTimeString() );
                $oRequest->offsetSet('end_date', $dEndDate->toDateTimeString() );
            }
            $aValidationRequireFor = [
                                        'quiz_title' => 'required',
                                        'duration' => 'required|integer',
                                        'total_marks' => 'required|integer',
                                        'start_hour' => 'required_if:quiz_type,1',
                                        'start_minute' => 'required_if:quiz_type,1',
                                        'end_hour' => 'required_if:quiz_type,1|required_if:quiz_type,2',
                                        'end_minute' => 'required_if:quiz_type,1|required_if:quiz_type,2',
                                        'group_name' => 'required'
                                    ];
            if($oRequest->quiz_type == 1)
            {
                $aValidationRequireFor['start_date'] = 'required_if:quiz_type,1|date|after:"'.$sToday.'"';
                $aValidationRequireFor['end_date'] = 'required_if:quiz_type,1|required_if:quiz_type,2|after:"'.$oRequest->start_date.'"';
            }
            elseif($oRequest->quiz_type == 2)
                $aValidationRequireFor['end_date'] = 'required_if:quiz_type,1|required_if:quiz_type,2|after:"'.$oRequest->start_date.'"';
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor );
            if ($oValidator->fails()) {
                return redirect('/quiz/quiz-add?error_page=1')->withErrors($oValidator)->withInput();
            }
            $nActivated = 1 ;
            $dCurrentTime = Carbon::now();
            
            //minuts in view side set in multiple of five and selected minute is getting wrong
            if($oRequest->quiz_type != 1)
            {
                $nMin = (($dCurrentTime->minute)%5);
                $dCurrentTime->subMinutes($nMin);
            }
            if($oRequest->quiz_type == 2)
                $oRequest->offsetSet('start_date', $dCurrentTime );
            elseif($oRequest->quiz_type == 3)
            {
                $nActivated = 0;
                $oRequest->offsetSet('start_date', $dCurrentTime );
                $oRequest->offsetSet('end_date', $dCurrentTime );
            }
            
            $oQuiz = Quiz::firstOrNew(['id_quiz' => $oRequest->id_quiz]);
            $oQuiz->id_user = Auth::user()->id_user;
            $oQuiz->quiz_title = $oRequest->quiz_title;
            $oQuiz->description = $oRequest->description;
            $oQuiz->duration = $oRequest->duration;
            $oQuiz->total_marks = $oRequest->total_marks;
            $oQuiz->show_in_mobile = isset($oRequest->show_mobile) ? $oRequest->show_mobile : 0;
            $oQuiz->activated = $nActivated;
            $oQuiz->save();

            $nIdQuiz = (!empty($oRequest->id_quiz)) ? $oRequest->id_quiz : $oQuiz->id_quiz;

            $this->dispatch(new SendQuizGroupInvite(Auth::user()->id_user,$oRequest->start_date,$oRequest->end_date,$nIdQuiz,$oRequest->group_name,$oRequest->quiz_type));
            //$this->SendQuizGroupInvite($oRequest->start_date,$oRequest->end_date,$nIdQuiz,$oRequest->group_name);
            return Response::json(['success' => true, 'nIdQuiz' => $nIdQuiz,'nQuizTotal'=>$oRequest->total_marks]);
        }

        if(!empty($nIdQuiz))
        {
            $oQuiz = Quiz::getQuizDetail($nIdQuiz);
            $oQuestions = QuizQuestion::getQuizQuestions($nIdQuiz);
            $nQuizQuestionTotal = QuizQuestion::getQuizQuestionsMarksTotal($nIdQuiz);
            foreach($oQuestions as $key => $oQuestion)
            {
                $oQuestions[$key]->answer_options = AnswerOption::where('id_quiz_question',$oQuestion->id_quiz_question)
                                                                 ->get(); 
            }
            $nQuestionNo = $oQuestions->firstItem();
            $nEditable = ($oQuiz->start_time > Carbon::Now() || $oQuiz->activated == 0) ? 1 : 0;
            if($oQuestions->currentPage() > 1)
                 return \View::make('WebView::quiz._show_questions',compact('oQuiz','oQuestions','nQuestionNo','nEditable'));

        }
        return \View::make('WebView::quiz.quiz_detail',compact('oGroupList','oQuiz','oQuestions','nQuizQuestionTotal','nIdQuiz'));
            
    }
    
    public function callAddQuizQuestion(Request $oRequest,$nIdQuestion='')
    {
            $nQuestionCount = $oRequest->question_count;
            $nIdQuiz = ($oRequest->id_quiz != '') ? $oRequest->id_quiz : $oRequest->old('id_quiz');
            $oQuiz = Quiz::getQuizDetail($nIdQuiz);
            $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuiz->id_group, Auth()->user()->id_user);
            
            if($oRequest->isMethod("POST") && ($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')) )
            {
                $aValidationRequireFor = [
                                            'id_quiz' => 'required',
                                            'question_type' => 'required',
                                            'question_text' => 'required',
                                            'marks' => 'required|integer|min:1'
                                            //'file_name' => 'image|mimes:jpg,jpeg,png|max:10000',
                                        ];

                if($oRequest->question_type == 'M')
                {
                    $aValidationRequireFor['answer_option'] = 'unique_array|array_min_size:2';
                    $aValidationRequireFor['correct_answer'] = 'required_if:question_type,M';

                }
                //print_r($oRequest->all());exit;
                $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor );
                if ($oValidator->fails()) {
                    return redirect('/quiz/quiz-question-add?question_count='.$nQuestionCount)->withErrors($oValidator)->withInput();
                }
                $oQuestion = QuizQuestion::firstOrNew(['id_quiz_question' => $oRequest->id_quiz_question]);

                $oQuestion->id_quiz = $oRequest->id_quiz;
                $oQuestion->question_text = $oRequest->question_text;
                $oQuestion->question_type = $oRequest->question_type;
                $oQuestion->marks = $oRequest->marks;
                $oQuestion->save();

                $nQuestionId = $oQuestion->id_quiz_question;
                if($oQuestion->question_type == 'M')
                {
                    foreach($oRequest->answer_option as $key=>$option)
                    {
                    $oAnswerOption = AnswerOption::firstOrNew(['id_answer_option' => $oRequest->id_answer_option[$key] ]);
                    $oAnswerOption->id_quiz_question = $nQuestionId;
                    $oAnswerOption->answer_text = $option;
                    $oAnswerOption->save();

                    if('option_'.$key == $oRequest->correct_answer)
                    {
                        $oCorrectAnswer = CorrectAnswer::firstOrNew(['id_quiz_question'=>$nQuestionId]);
                        $oCorrectAnswer->id_answer_option = $oAnswerOption->id_answer_option;
                        $oCorrectAnswer->answer_description = $oRequest->answer_description;
                        $oCorrectAnswer->save();

                        $nCorrectAnswer = $oAnswerOption->id_answer_option;
                    }
                }
                }
                //In view file set for multiple question 
                $oQuestions = QuizQuestion::where('id_quiz_question',$nQuestionId)->get();
                if($oQuestion->question_type == 'M')
                {
                    $oQuestions[0]->correct_answer = $nCorrectAnswer;
                    $oQuestions[0]->answer_options = AnswerOption::where('id_quiz_question',$oQuestions[0]->id_quiz_question)
                                                                    ->get();
                }
                $nQuestionNo = $oRequest->question_count;
                $nQuizQuestionTotal = QuizQuestion::getQuizQuestionsMarksTotal($nIdQuiz);
                $questionHtml = \View::make('WebView::quiz._show_questions',compact('oQuestions','nQuestionNo'))->render();
                return Response::json(['success' => true, 'html' => $questionHtml,'nQuizQuestionTotal'=> $nQuizQuestionTotal]);
            }
            if(!empty($nIdQuestion))
            {
                $oQuizQuestion = QuizQuestion::getQuizQuestionDetail($nIdQuestion);
                $oQuizQuestion->answer_option = AnswerOption::where([   'id_quiz_question' => $nIdQuestion ,
                                                                        'activated' => 1,
                                                                        'deleted' => 0])
                                                            ->get()->toArray();
            }
            return \View::make('WebView::quiz._add_question',compact('nQuestionCount','oQuizQuestion','nIdQuiz'));
    }
    
    public function callDeleteQuizQuestion($nIdQuestion)
    {
        $oQuizQuestion = QuizQuestion::getQuestionDetail($nIdQuestion);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuizQuestion->id_group, Auth()->user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $oQuizQuestion = QuizQuestion::firstOrNew(['id_quiz_question'=>$nIdQuestion]);
            $oQuizQuestion->activated = 0;
            $oQuizQuestion->deleted = 1;
            $oQuizQuestion->save();
            return "success";
        }
    }
    
    public function callAdminQuizList($nIdGroup='') 
    {
            $aOptionalConditions = array();
            $aOptionalConditions['activated'] = 1;
            $aOptionalConditions['member_types'] = [config('constants.GROUPCREATOR')];
            //$oGroupList = Group::getGroupList(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'), $aOptionalConditions);
        
            $sQuizListingType = 'upcoming';
            $sUserType = 'admin';

            if($nIdGroup == '')
            {
                $oCourseGroups = GroupMember::getMemberTypeGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'=','admin');
                $aGroupIds = explode(",", $oCourseGroups[0]->id_groups);
            }
            else
                $aGroupIds = explode(",", $nIdGroup);
            
            $dTime = Carbon::Now();
            $dTime->toDateTimeString();
            $oQuizList = Quiz::getFacultyUpcommingQuizList(Auth::user()->id_user,$aGroupIds,'asc',$dTime);
            if($oQuizList->currentPage() > 1)
                return \View::make('WebView::quiz._more_quiz_faculty_list', compact('oQuizList'));
            
            return \View::make('WebView::quiz.quiz_list',compact('oQuizList','sQuizListingType','nIdGroup','sUserType'));
    }
    
    public function callAdminQuizListAjax($nIdGroup='') 
    {
        $sQuizListingType = 'upcoming';
        $sUserType = 'admin';
        if($nIdGroup == '')
        {
            $oCourseGroups = GroupMember::getMemberTypeGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'=','admin');
            $aGroupIds = explode(",", $oCourseGroups[0]->id_groups);
        }
        else
            $aGroupIds = explode(",", $nIdGroup);

        $dTime = Carbon::Now();
        $dTime->toDateTimeString();
            
        $oQuizList = Quiz::getFacultyUpcommingQuizList(Auth::user()->id_user,$aGroupIds,'asc',$dTime);
        return \View::make('WebView::quiz._quiz_faculty_list_ajax',compact('oQuizList','nIdGroup','sQuizListingType','nIdGroup','sUserType'));
        
    }
    
    public function callAdminCompletedQuizList($nIdGroup='') 
    {
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPCREATOR')];
        //$oGroupList = Group::getGroupList(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'), $aOptionalConditions);
        if($nIdGroup == '')
        {
            $oCourseGroups = GroupMember::getMemberTypeGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'=','admin');
            $aGroupIds = explode(",", $oCourseGroups[0]->id_groups);
        }
        else
            $aGroupIds = explode(",", $nIdGroup);

        $nUpcomingQuizCount = Quiz::getFacultyQuizCount(Auth::user()->id_user,$nIdGroup);

        $sQuizListingType = 'completed';
        $sUserType = 'admin';
        $dTime = Carbon::Now();
        $dTime->toDateTimeString();
        $aWhereData = [
                        ['qgi.end_time', '<=', $dTime]
                    ];
        $oQuizList = Quiz::getFacultyCompletedQuizList(Auth::user()->id_user,$aWhereData,$aGroupIds,'desc');
        foreach($oQuizList as $nKey=>$aQuiz)
        {
            $aQuizQuestions = QuizQuestion::getQuizQuestion($aQuiz->id_quiz);
            $aIdQuestions = explode(",", $aQuizQuestions[0]->id_quiz_question);
            $oQuizList[$nKey]->marks_remaining = UserAnswer::checkQuizMarksRemaining($aIdQuestions);
        }
        if($oQuizList->currentPage() > 1)
            return \View::make('WebView::quiz._more_quiz_faculty_list', compact('oQuizList'));

        return \View::make('WebView::quiz.quiz_list',compact('oQuizList','sQuizListingType','nIdGroup','nUpcomingQuizCount','sUserType'));
    }
    
    public function callAdminCompletedQuizListAjax($nIdGroup='') 
    {
        $sQuizListingType = 'completed';
        $sUserType = 'admin';
        $dTime = Carbon::Now();
        $dTime->toDateTimeString();
        
        if($nIdGroup == '')
        {
            $oCourseGroups = GroupMember::getMemberTypeGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'=','admin');
            $aGroupIds = explode(",", $oCourseGroups[0]->id_groups);
        }
        else
            $aGroupIds = explode(",", $nIdGroup);

        $nUpcomingQuizCount = Quiz::getFacultyQuizCount(Auth::user()->id_user,$aGroupIds);

        $aWhereData = [
                        ['qgi.end_time', '<=', $dTime]
                    ];
        $oQuizList = Quiz::getFacultyCompletedQuizList(Auth::user()->id_user,$aWhereData,$aGroupIds,'desc');
        return \View::make('WebView::quiz._quiz_faculty_completed_list_ajax',compact('oQuizList','nIdGroup','sQuizListingType','nUpcomingQuizCount','sUserType'));
    }
   
    public function callMemberQuizList($nIdGroup='') 
    {
        $sQuizListingType = 'upcoming';
        $sUserType = 'member';
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPADMIN'),config('constants.GROUPMEMBER')];
        $oGroupList = Group::getGroupList(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'), $aOptionalConditions);
        
        $dCurrentTime = Carbon::Now();
        
        $oQuizList = QuizUserInvite::getUpcomingUserQuizList(Auth::user()->id_user, $dCurrentTime,$nIdGroup);
        $nUpcomingQuizCount = $oQuizList->total();
        if($oQuizList->currentPage() > 1)
                return \View::make('WebView::quiz._more_quiz_student_list', compact('oQuizList'));
        
        return \View::make('WebView::quiz.quiz_list', compact('nUpcomingQuizCount','oQuizList','sQuizListingType','oGroupList','nIdGroup','sUserType'));
    }
    
    public function callMemberQuizListAjax($nIdGroup='') 
    {
        $sQuizListingType = 'upcoming';
        $sUserType = 'member';
        $dCurrentTime = Carbon::Now();
        $dCurrentTime->toDateTimeString();
        
        $oQuizList = QuizUserInvite::getUpcomingUserQuizList(Auth::user()->id_user, $dCurrentTime,$nIdGroup);
        $nUpcomingQuizCount = $oQuizList->total();
        
        return \View::make('WebView::quiz._quiz_student_list_ajax', compact('nUpcomingQuizCount', 'oQuizList','nIdGroup','sQuizListingType','sUserType'));
    }
    
    public function callMemberCompletedQuizList($nIdGroup='') 
    {
        $sQuizListingType = 'completed';
        $sUserType = 'member';
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPADMIN'),config('constants.GROUPMEMBER')];
        $oGroupList = Group::getGroupList(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'), $aOptionalConditions);
        $dCurrentTime = Carbon::Now();
        $dCurrentTime->toDateTimeString();
        
        $oQuizList = QuizUserInvite::getCompletedUserQuizList(Auth::user()->id_user, $dCurrentTime,$nIdGroup);
        $nUpcomingQuizCount = QuizUserInvite::getUpcomingUserQuizCount(Auth::user()->id_user, $dCurrentTime,$nIdGroup);
        foreach($oQuizList as $Key => $oQuiz)
        {
            $aQuizQuestions = QuizQuestion::getQuizQuestion($oQuiz->id_quiz);
            $aIdQuestions = explode(",", $aQuizQuestions[0]->id_quiz_question);
            $oUserAnswer = UserAnswer::getUserMarkObtain($oQuiz->id_quiz,Auth::user()->id_user)->first();
            $oQuizList[$Key]->mark_obtain = $oUserAnswer->mark_obtain;
            $oQuizList[$Key]->marks_remaining = UserAnswer::checkQuizMarksRemaining($aIdQuestions,Auth::user()->id_user);
        }
        if($oQuizList->currentPage() > 1)
                return \View::make('WebView::quiz._more_quiz_student_list', compact('oQuizList'));
        return \View::make('WebView::quiz.quiz_list', compact('oQuizList','sQuizListingType','nUpcomingQuizCount','oGroupList', 'nIdGroup','sUserType'));
    }
    
    public function callMemberCompletedQuizListAjax($nIdGroup='') 
    {
        $sQuizListingType = 'completed';
        $sUserType = 'member';
        $dCurrentTime = Carbon::Now();
        $dCurrentTime->toDateTimeString();
        
        $oQuizList = QuizUserInvite::getCompletedUserQuizList(Auth::user()->id_user, $dCurrentTime,$nIdGroup);
        foreach($oQuizList as $Key => $oQuiz)
        {
            $aQuizQuestions = QuizQuestion::getQuizQuestion($oQuiz->id_quiz);
            $aIdQuestions = explode(",", $aQuizQuestions[0]->id_quiz_question);
            $oUserAnswer = UserAnswer::getUserMarkObtain($oQuiz->id_quiz,Auth::user()->id_user)->first();
            $oQuizList[$Key]->mark_obtain = $oUserAnswer->mark_obtain;
            $oQuizList[$Key]->marks_remaining = UserAnswer::checkQuizMarksRemaining($aIdQuestions,Auth::user()->id_user);
        }
        $nUpcomingQuizCount = QuizUserInvite::getUpcomingUserQuizCount(Auth::user()->id_user, $dCurrentTime,$nIdGroup);
        return \View::make('WebView::quiz._quiz_student_completed_list_ajax', compact('oQuizList','nIdGroup','sQuizListingType','nUpcomingQuizCount','sUserType'));
    }
    
    public function callMemberQuizDetail($nIdQuizUserInvite)
    {
        $dCurrentTime = Carbon::Now();
        $dCurrentTime->toDateTimeString();
        
        $oQuizUserInvite = QuizUserInvite::checkQuizBelogsToUser($nIdQuizUserInvite, Auth::user()->id_user, $dCurrentTime);
        if(count($oQuizUserInvite) )
        {
            $oQuiz = Quiz::getQuizDetail($oQuizUserInvite->id_quiz);
            $oUserAttemp = UserAttempt::where(['id_quiz' => $oQuizUserInvite->id_quiz,
                                                'id_user' => Auth::user()->id_user
                                                ])->select('end_time')->first();
            //print_r($oUserAttemp);
            $dEndtime = '';
            if(count($oUserAttemp))
            {
                $oUserAttemp->end_time;
                $dTimeDiff = TimeDiffinSec($oUserAttemp->end_time, Carbon::Now());
                $dQuizEndTime = Carbon::createFromFormat('Y-m-d H:i:s', $oUserAttemp->end_time);
                $dQuizEndTime->setTimezone(Auth::user()->timezone);
                $dEndtime = $dQuizEndTime->format('h:i A T'); 
            }
            else
                $dTimeDiff = $oQuiz->duration*60;
            if( $dTimeDiff > 0)
            {
                $aQuizQuestions = QuizQuestion::getUserQuizQuestions($oQuizUserInvite->id_quiz,Auth::user()->id_user);
                $nQuizQuestionCount = $aQuizQuestions->total();

            return \View::make('WebView::quiz.student_quiz',compact('oQuiz','aQuizQuestions', 'nQuizQuestionCount','nIdQuizUserInvite','dTimeDiff','dEndtime'));
            }
            else
                return response()->view('errors.access_denied');
        }
        else
            return response()->view('errors.access_denied');
    }
    
    public function callQuizQuestions($nIdQuizUserInvite)
    {
        $oQuizUserInvite = QuizUserInvite::find($nIdQuizUserInvite);
        $dCurrentTime = Carbon::Now();
        $dStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $oQuizUserInvite->start_time);
        $dEndTime = Carbon::createFromFormat('Y-m-d H:i:s', $oQuizUserInvite->end_time);
        if($dStartTime < $dCurrentTime && $dEndTime > $dCurrentTime)
        {
            $oQuiz = Quiz::find($oQuizUserInvite->id_quiz);
            $aQuizQuestions = QuizQuestion::getUserQuizQuestions($oQuizUserInvite->id_quiz,Auth::user()->id_user);
            foreach ($aQuizQuestions as $nKey => $oQuizQuestion)
            {
                $aOptions = AnswerOption::where('id_quiz_question', $oQuizQuestion->id_quiz_question)
                                            ->get();
                $aQuizQuestions[$nKey]->answer_options = $aOptions;
            }
            if($aQuizQuestions->currentPage() == 1)
            {
                $oUserAttemp = UserAttempt::where(['id_quiz' => $oQuizUserInvite->id_quiz,
                                                  'id_user' => Auth::user()->id_user
                                                ])->first();
                $dEndQuizTime = Carbon::createFromFormat('Y-m-d H:i:s', $oUserAttemp->end_time);
                $dEndQuizTime->setTimezone(Auth::user()->timezone);
                $dEndQuizTime = $dEndQuizTime->format('h:i A T');
                //$dEndQuizTime = gmdate('H:i:s',$dEndQuizTime);
                $dTimeDiff = TimeDiffinSec($oUserAttemp->end_time, Carbon::Now());
                $questionHtml = \View::make('WebView::quiz._more_quiz_question',compact( 'aQuizQuestions', 'nQuizQuestionCount','dTimeDiff'))->render();
                return Response::json(['success' => true, 'html' => $questionHtml,'timeDiff'=> $dTimeDiff,'dEndQuizTime' => $dEndQuizTime]);
            }
            
                return \View::make('WebView::quiz._more_quiz_question',compact( 'aQuizQuestions', 'nQuizQuestionCount'));
        }  
        else
            return response()->view('errors.access_denied');
    }
    
    public function callQuizUserList($nIdQuiz) 
    {

        $aQuizQuestions = QuizQuestion::getQuizQuestion($nIdQuiz);
        $aIdQuestions = explode(",", $aQuizQuestions[0]->id_quiz_question);
        
        $oUserList = QuizUserInvite::getUserInvitedList($nIdQuiz);
        foreach($oUserList as $key=>$oUser)
        {
            $oUserAttemp = UserAttempt::where(['id_quiz'=> $nIdQuiz,'id_user'=>$oUser->id_user ])->first(); 
            $dCurrentTime = Carbon::Now();
            
            if(count($oUserAttemp) && $oUserAttemp->end_time <= $dCurrentTime && $oUser->completed == 0)
            {
                QuizUserInvite::where('id_quiz_user_invite','=',$oUser->id_quiz_user_invite)->update(['completed'=>1]);
                $oUserList[$key]->completed = 1;
            }
             //set because if not completed 
            $oUserList[$key]->mark_obtain = '';
            $oUserList[$key]->marks_remaining = '';
            if($oUser->completed == 1)
            {
                $oUserList[$key]->mark_obtain = QuizQuestion::getStudentTotalMarks($nIdQuiz,$oUser->id_user);
                $oUserList[$key]->marks_remaining = UserAnswer::checkQuizMarksRemaining($aIdQuestions,$oUser->id_user);
            }
        }
        if($oUserList->currentPage() > 1)
            $view = \View::make('WebView::quiz._more_quiz_user_list', compact('oUserList'));
        else
            $view = \View::make('WebView::quiz.quiz_user_list', compact('oUserList','nIdQuiz'));
        
        $html = $view->render();
        return Response::json(['success' => true, 'html' => $html]);
    }

    //show student quiz detail in slider with marks for faculty account
    public function callMemberCompletedQuiz($nIdQuiz,$nIdUser)
    {
        if(!empty($nIdQuiz))
            {
                $oUser = User::find($nIdUser);
                $oQuiz = Quiz::getQuizDetail($nIdQuiz);
                $oQuiz->mark_obtain = QuizQuestion::getStudentTotalMarks($nIdQuiz, $nIdUser);
                $oUserAttemp = UserAttempt::where([ 'id_quiz' => $nIdQuiz, 'id_user'=> $nIdUser])->first();
                $sTime = TimeDiffinSec($oUserAttemp->end_time, $oUserAttemp->start_time);
                $oQuiz->time_taken = gmdate("H:i:s", $sTime);
                $oQuestions = QuizQuestion::getUserQuizQuestions($nIdQuiz,$nIdUser);
                foreach($oQuestions as $key => $oQuestion)
                {
                    $oQuestions[$key]->answer_options = AnswerOption::where('id_quiz_question',$oQuestion->id_quiz_question)
                                                                     ->get(); 
                }
                
                if($oQuestions->currentPage() > 1)
                     return \View::make('WebView::quiz._show_student_completed_question',compact('oQuiz','oQuestions','nIdUser'));
                
                return \View::make('WebView::quiz._show_student_completed_quiz',compact('oQuiz','oQuestions','nIdQuiz','nIdUser','oUser'));
            }
    }
    //show student quiz detail in slider with marks for student account
    public function callMemberCompletedQuizDetail($nIdQuiz,$nIdUser)
    {
        if(!empty($nIdQuiz))
            {
                $oQuiz = Quiz::getQuizDetail($nIdQuiz);
                $oQuiz->mark_obtain = QuizQuestion::getStudentTotalMarks($nIdQuiz, $nIdUser);
                $oUserAttemp = UserAttempt::where([ 'id_quiz' => $nIdQuiz, 'id_user'=> $nIdUser])->first();
                $sTime = TimeDiffinSec($oUserAttemp->end_time, $oUserAttemp->start_time);
                $oQuiz->time_taken = gmdate("H:i:s", $sTime);
                $oQuestions = QuizQuestion::getUserQuizQuestions($nIdQuiz,$nIdUser);
                foreach($oQuestions as $key => $oQuestion)
                {
                    $oQuestions[$key]->answer_options = AnswerOption::where('id_quiz_question',$oQuestion->id_quiz_question)
                                                                     ->get(); 
                }
                
                if($oQuestions->currentPage() > 1)
                     return \View::make('WebView::quiz._student_completed_question',compact('oQuiz','oQuestions','nIdUser'));
                
                return \View::make('WebView::quiz._student_completed_quiz_detail',compact('oQuiz','oQuestions','nIdQuiz','nIdUser'));
            }
    }
    
    public function callAddStudentMarks(Request $oRequest)
    {
        $oQuestion = QuizQuestion::getQuestionDetail($oRequest->id_question);
        $aValidationRequireFor = [
                                    'marks' => 'required|integer|min:0|between:0,'.$oQuestion->marks
                                ];
        $this->validate($oRequest, $aValidationRequireFor);
        
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuestion->id_group, Auth::user()->id_user);
        
        if( $oQuestion->marks >= $oRequest->marks &&($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')))
        {
            $oUserAnswer =UserAnswer::firstOrNew([
                                            'id_user'=>$oRequest->id_user,
                                            'id_quiz_question' => $oRequest->id_question 
                                        ]);
            $oUserAnswer->mark_obtain = $oRequest->marks;
            $oUserAnswer->save();
        }
    }
    
    public function callAddStudentAnswer(Request $oRequest)
    {
        $oQuizQuestion = QuizQuestion::find($oRequest->id_quiz_question);
        
        $aValidationRequiredFor = [
                                    'id_quiz' => 'required',
                                    'id_quiz_question' => 'required|exists:quiz_questions,id_quiz_question,id_quiz,'.$oRequest->id_quiz
                                ];
        if($oQuizQuestion->question_type == config('constants.QUESTIONTYPEMULTIPLE'))
            $aValidationRequiredFor['id_answer_option'] = 'required|exists:answer_options,id_answer_option,id_quiz_question,'.$oRequest->id_quiz_question;
        else
            $aValidationRequiredFor['answer_text'] = 'required';
        
        $oValidator = Validator::make($oRequest->all(), $aValidationRequiredFor);
        if($oValidator->fails())
        {
            $aMessages['errors'] = $oValidator->errors();
            $aMessages['response_status'] = config('constants.APIERROR');
            $aMessages['request_type'] = 'add_student_answer';
            $aMessages['response_message'] = 'add_student_answer_fail';

            return json_encode($aMessages);
        }
        
        $oCorrectAnswer = CorrectAnswer::where('id_quiz_question','=',$oRequest->id_quiz_question)
                                        ->select('id_answer_option as id_answer_option')
                                        ->first();
        $oUserAnswer = UserAnswer::firstOrNew([
                                                'id_user' => Auth::user()->id_user,
                                                'id_quiz_question' => $oRequest->id_quiz_question
                                            ]);
        $oUserAnswer->id_answer_option = $oRequest->id_answer_option;
        $oUserAnswer->user_answer_description = $oRequest->answer_text;
        $oUserAnswer->mark_obtain = !empty($oRequest->id_answer_option) ?
                                            (($oRequest->id_answer_option == $oCorrectAnswer->id_answer_option) ? $oQuizQuestion->marks : 0)
                                            : NULL;
        $oUserAnswer->save();
        
        $aMessages['errors'] = array();
        $aMessages['response_status'] = config('constants.APISUCCESS');
        $aMessages['request_type'] = 'add_student_answer';
        $aMessages['response_message'] = 'add_student_answer_success';

        return json_encode($aMessages);
    }
    
    public function callStartQuiz($nIdQuizUserInvite) 
    {
        $oQuizUserQuestion = QuizUserInvite::find($nIdQuizUserInvite);
        $oQuiz = Quiz::getQuizDetail($oQuizUserQuestion->id_quiz);
        $dToday = Carbon::Now();
        $sToday = $dToday->toDateTimeString();
        if($oQuizUserQuestion->id_user == Auth::user()->id_user)
        {
            $oUserAttempt = UserAttempt::where(['id_quiz' => $oQuizUserQuestion->id_quiz,
                                                'id_user'=> $oQuizUserQuestion->id_user])
                                        ->first();
            
            if($oQuizUserQuestion->start_time < $dToday && count($oUserAttempt) <=0)
            {
                $dToday->addMinutes($oQuiz->duration);
                
                if($dToday > $oQuiz->end_time)
                    $dToday = $oQuiz->end_time;
                    UserAttempt::create([  'id_quiz' => $oQuizUserQuestion->id_quiz,
                                           'id_user'=> $oQuizUserQuestion->id_user,
                                           'start_time' => $sToday,
                                           'end_time' => $dToday
                                       ]);
                if($oUserAttempt)
                    return $this->callQuizQuestions($nIdQuizUserInvite);
                else
                    return trans('messages.cancel');
            }
            elseif($oUserAttempt->end_time > $dToday)
                return $this->callQuizQuestions($nIdQuizUserInvite);
        }
        else
            return response()->view('errors.access_denied');
        
    }
    
    public function callEndQuiz($nIdQuizUserInvite) 
    {
        $oQuizUserQuestion = QuizUserInvite::find($nIdQuizUserInvite);
        if($oQuizUserQuestion->id_user == Auth::user()->id_user)
        {
            $oUserAttemp = UserAttempt::where(['id_quiz' => $oQuizUserQuestion->id_quiz, 'id_user'=>$oQuizUserQuestion->id_user])->first();
            $oUserAttemp->end_time = Carbon::Now();
            $oUserAttemp->update();
            
            $oQuizUserQuestion->completed = 1;
            $oQuizUserQuestion->update();
            return 'success';
        }
        else
            return response()->json(['error' => trans('message.quiz_access_denied')], 404);
        
    }
    
    public function callDeleteAnswerOption($nIdAnswerOption)
    {
        $oIdUserQuiz = AnswerOption::checkQuizCreator($nIdAnswerOption);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oIdUserQuiz->id_group, Auth()->user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $oAnswerOption = AnswerOption::find($nIdAnswerOption);
            $oAnswerOption->delete();
        }
    }
    
    public function callDeleteQuiz($nIdQuiz)
    {
        $oQuiz = Quiz::getQuizDetail($nIdQuiz);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuiz->id_group, Auth()->user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $oQuiz->activated = 0;
            $oQuiz->deleted = 1;
            $oQuiz->save();
            return 'success';
        }
    }
    
    public function callEditQuizPoint(Request $oRequest)
    {
        $oQuiz = Quiz::getQuizDetail($oRequest->id_quiz);
        $aValidationRequiredFor = [
                                    'id_quiz' => 'required',
                                    'marks' => 'required|integer|min:1'
                                ];
        $this->validate($oRequest, $aValidationRequiredFor);
        
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuiz->id_group, Auth()->user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $oQuiz->total_marks = $oRequest->marks;
            $oQuiz->save();
            return Response::json(['success' => true, 'marks' => $oRequest->marks]);
        }
    }
    public function callEditQuizQuestionPoint(Request $oRequest)
    {
        $aValidationRequiredFor = [
                                    'id_question' => 'required',
                                    'marks' => 'required|integer|min:1'
                                ];
        $this->validate($oRequest, $aValidationRequiredFor);
        $oQuizQuestion = QuizQuestion::getQuestionDetail($oRequest->id_question);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuizQuestion->id_group, Auth()->user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $oQuizQuestions = QuizQuestion::where(['id_quiz_question'=>$oRequest->id_question])
                                        ->update(['marks'=> $oRequest->marks]);
            if($oQuizQuestion->question_type == config('constants.QUESTIONTYPEMULTIPLE'))
            {
                $oCorrectAns = CorrectAnswer::where(['id_quiz_question' => $nIdQuestion])->first();
                UserAnswer::where([ 'id_quiz_question' => $nIdQuestion, 'id_answer_option'=>$oCorrectAns->id_answer_option ])
                           ->update([ 'marks' => $oRequest->marks])->save();
            }
            $nQuizQuestionTotal = QuizQuestion::getQuizQuestionsMarksTotal($oQuizQuestion->id_quiz);
            return Response::json(['success' => true, 'nQuizQuestionTotal' => $nQuizQuestionTotal,'marks' => $oRequest->marks]);
        }
    }
    //direct publish from list view for admin 
    public function callQuizPublish($nIdQuiz) 
    {
        $oQuizGroupInvite = QuizGroupInvite::where('id_quiz', $nIdQuiz)->first();
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oQuizGroupInvite->id_group, Auth::user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $oQuiz = Quiz::find($nIdQuiz);
            $nMinute = $oQuiz->duration;
            $oQuizGroupInvite->start_time = Carbon::Now()->subMinute();
            $oQuizGroupInvite->end_time = Carbon::Now()->addMinutes($nMinute-1);
            $oQuizGroupInvite->update();
            
            $oQuizGroupInvites = QuizGroupInvite::find($oQuizGroupInvite->id_quiz_group_invite);
            $this->dispatch(new SendQuizGroupInvite(Auth::user()->id_user,Carbon::Now(),Carbon::Now()->addMinutes($nMinute),$nIdQuiz,$oQuizGroupInvites->id_group,2));
            
            $oQuiz->activated = 1;
            $oQuiz->update();
            return 'success';
        }
    }
}