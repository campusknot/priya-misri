<?php
namespace App\Http\Web\Controllers;

use App\Http\Web\Controllers\Controller;

use App\Libraries\Post;
use App\Libraries\Group;
use App\Libraries\GroupPost;
use App\Libraries\GroupMember;
use App\Libraries\PostMedia;
use App\Libraries\Poll;
use App\Libraries\PollOption;
use App\Libraries\Comment;
use App\Libraries\Like;
use App\Libraries\Notification;
use App\Libraries\PollAnswer;
use App\Libraries\UserEducation;
use App\Libraries\UserReportedContent;

use App\Jobs\SendNewGroupPostNotification;
use App\Jobs\SendCommentOnPostNotification;
use App\Jobs\SendPostLikeNotification;

use Excel;
use Auth;
use Image;
use Response;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
                                    'only' => [
                                                'callAddPost', 'callAddPoll',
                                                'callActivatePoll', 'callAddComment',
                                                'callLikePost', 'callDeletePost',
                                                'callPostDetail', 'callPhotoDetail',
                                                'callPollDetail', 'callAddPollAnswer','callPollAnswerExcel',
                                                'callDeletePostComment','callPostLikeUser','callAddContentReport'
                                            ]
                                ]
                        );
        parent::__construct();
    }

    /**
     *
     * @param Request $oRequest
     * @return type
     */
    public function callAddPost(Request $oRequest)
    {

        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [];

            switch ($oRequest->post_type)
            {
                case config('constants.POSTTYPEIMAGE'):
                    $aValidationRequiredFor = [
                        'file' => 'required|max:10000'
                    ];
                    if ($oRequest->hasFile('file')) {
                        $oRequest->offsetSet('extention', mb_strtolower($oRequest->file('file')->getClientOriginalExtension()));
                        $aValidationRequiredFor['extention'] = 'required|in:bmp,jpg,jpeg,png,svg';
                    }
                    break;
                case config('constants.POSTTYPEVIDEO'):
                    $aValidationRequiredFor = [
                        'file' => 'required|max:100000'
                    ];
                    if ($oRequest->hasFile('file')) {
                        $oRequest->offsetSet('extention', mb_strtolower($oRequest->file('file')->getClientOriginalExtension()));
                        $aValidationRequiredFor['extention'] = 'required|in:mov,mp4,avi,3gp,mkv';
                    }
                    break;
                case config('constants.POSTTYPEDOCUMENT'):

                    $aValidationRequiredFor = [
                        'file' => 'required|max:10000'
                    ];

                    if ($oRequest->hasFile('file')) {
                        $oRequest->offsetSet('extention', mb_strtolower($oRequest->file('file')->getClientOriginalExtension()));
                        $aValidationRequiredFor['extention'] = 'required|in:doc,docx,pages,rtf,txt,wp,numbers,xls,xlsx,csv,key,ppt,pptx,pps,mdb,acc,accdb,pdf,zip,rar';
                    }

                    break;
                case config('constants.POSTTYPECODE'):
                    $aValidationRequiredFor = [
                                                    'post_textarea' => 'required'
                                                ];
                    break;
                default :
                    //Code for text/code
                    $aValidationRequiredFor = [
                                                    'post_text' => 'required'
                                                ];
            }
            $this->validate($oRequest, $aValidationRequiredFor);

            if( isset($oRequest->post_text) && !empty($oRequest->post_text))
            {
                //$sComment = trim($oRequest->post_text);
                $sComment = $oRequest->post_text;
            }
            else
            {
                $sComment = trim($oRequest->post_textarea);
            }
            $oNewPost = Post::create([
                                        'id_user' => Auth::user()->id_user,
                                        'post_text' => $sComment,
                                        'post_type' => $oRequest->post_type,
                                        'activated' => 1,
                                        'deleted' => 0
                                    ]);
            $sFileName='';
            if($oRequest->hasFile('file') && $oRequest->post_type != config('constants.POSTTYPETEXT') && $oRequest->post_type != config('constants.POSTTYPECODE'))
            {
                /**
                 * Upload file in upload folder
                 * And make entry in post_media table
                 */
                $oUploadedFile = $oRequest->file('file');
                $sOriginalName = $oUploadedFile->getClientOriginalName();
                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image

                $oS3 = \Storage::disk('s3');
                $filePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                $oS3->put($filePath, fopen($oUploadedFile, 'r+'), 'public');
                if ($oRequest->post_type == config('constants.POSTTYPEIMAGE'))
                {
                    $oImg = Image::make($oUploadedFile);

                    $nWidth = $oImg->width();
                    $nHeight = $oImg->height();
                    $sSize = $oImg->filesize();

                    if($nWidth > 800 || $nHeight > 800)
                    {
                        $oImg = $oImg->resize(800, 800,function ($constraint) {
                                                                $constraint->aspectRatio();
                                                            });
                    }
                    $oImg = $oImg->stream();
                    $oS3 = \Storage::disk('s3');
                    $sFilePath = '/'.config('constants.POSTMEDIAFOLDER').'/80_' . $sFileName;
                    $oS3->put($sFilePath, $oImg->__toString(), 'public');
                }
                PostMedia::create([
                                        'id_post' => $oNewPost->id_post,
                                        'display_file_name' => $sOriginalName,
                                        'file_name' => $sFileName,
                                        'media_type' => $oRequest->post_type,
                                        'activated' => 1,
                                        'deleted' => 0
                                    ]);
            }
            if(!empty($oRequest->id_group))
            {
                //Add post as Group post and make entry in group_post table.
                GroupPost::create([
                                    'id_group' => $oRequest->id_group,
                                    'id_post' => $oNewPost->id_post
                                ]);

                //Send notification to all group members
                $this->dispatch(new SendNewGroupPostNotification(Auth::user(), $oRequest->id_group, $oNewPost->id_post));
            }
        }
        if($oRequest->id_group)
        {
             $oGroup = new Group();
             $oGroupDetails = $oGroup->getGroupDetail($oRequest->id_group);
             $oGroupMember = new GroupMember();
             $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($oRequest->id_group, Auth()->user()->id_user);
             $oGroupFeeds[0] = Post::getPostDetail($oNewPost->id_post);
             $view=  \View::make('WebView::group._more_group_feeds', compact('oGroupFeeds','oGroupMemberDetail','oGroupDetails'));
             $html = $view->render();
             return Response::json(['success' => true, 'html' => $html]);
        }
        else
        {
            $oUserFeeds [0] = Post::getPostDetail($oNewPost->id_post);
            $view=  \View::make('WebView::user._more_feeds', compact('oUserFeeds'));

            $html = $view->render();
            return Response::json(['success' => true, 'html' => $html]);
        }
        return back();
    }

    public function callAddPoll(Request $oRequest, $nIdPost='')
    {
        if($oRequest->isMethod("POST"))
        {
            $nTime = isset($oRequest->duration) ? $oRequest->duration : ($oRequest->day*24*60*60)+($oRequest->hour*60*60)+($oRequest->minutes*60)+$oRequest->seconds;
            $oRequest->offsetSet('duration',$nTime);
            $aValidationRequiredFor = [
                                            'poll_question'=> 'required',
                                            'duration' => 'integer|required|min:5',
                                            'file'     => 'image|mimes:bmp,jpg,jpeg,png,svg|max:10000',
                                            'poll_type'=> 'required|in:'.config('constants.POLLTYPEMULTIPLE').','.config('constants.POLLTYPEOPEN')
                                        ];
            if(!empty($oRequest->poll_type) && ($oRequest->poll_type == config('constants.POLLTYPEMULTIPLE')))
                $aValidationRequiredFor['options'] = 'array_min_size:2|unique_array';

            if($oRequest->poll_release_type == 'SC')
            {
                $aValidationRequiredFor['schedule_date'] = 'required';
            }
            $this->validate($oRequest, $aValidationRequiredFor);

            if($oRequest->poll_release_type == 'SN')
            {
                $sFilePath = '';
                if($oRequest->hasFile('file'))
                {
                    $oUploadedFile = $oRequest->file('file');
                    $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                    $sFilePath = str_random(8).'_'.time().'.'.$sExtension; // renameing image
                    \Storage::disk('public')->put($sFilePath, file_get_contents($oUploadedFile->getRealPath()));
                }
                $aFormData = $oRequest->all();
                $aFormData['file_path'] = $sFilePath;
                $oView = \View::make('WebView::post._schedule_poll', compact('aFormData'));
                $sHtml = $oView->render();

                return Response::json(['success' => true, 'html'=> $sHtml]);
            }

            $sFileName=$oRequest->poll_file_old;
            if($oRequest->hasFile('file'))
            {
                /**
                 * Upload file in upload folder
                 * And make entry in post_media table
                 */
                $oUploadedFile = $oRequest->file('file');
                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image

                $oS3 = \Storage::disk('s3');
                $filePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                $oS3->put($filePath, fopen($oUploadedFile, 'r+'), 'public');
            }
            if(!empty($oRequest->file_path))
            {
                $oUploadedFile = storage_path('app/public/'.$oRequest->file_path);
                $sFileName = $oRequest->file_path; // renameing image

                $oS3 = \Storage::disk('s3');
                $filePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                $oS3->put($filePath, fopen($oUploadedFile, 'r+'), 'public');

                unlink($oUploadedFile);
            }

            $bSendGroupNotification = TRUE;

            //Remove group post when poll is being edit
            if(!empty($oRequest->id_post))
            {
                GroupPost::where('id_post',$oRequest->id_post)
                        ->delete();
            }
            $oNewPost = Post::firstOrNew(['id_post' => $oRequest->id_post]);
            $oNewPost->id_user = Auth::user()->id_user;
            $oNewPost->post_text = '';
            $oNewPost->post_type = $oRequest->post_type;
            $oNewPost->activated = 1;
            $oNewPost->deleted = 0;
            $oNewPost->save();

            $dScheduleDate = Carbon::now();
            if(!empty($oRequest->schedule_date))
            {
                $bSendGroupNotification = FALSE;
                $aDateParams = explode("-", $oRequest->schedule_date);
                $nHours = !empty($oRequest->schedule_hour) ? $oRequest->schedule_hour : 00;
                $nMinute = !empty($oRequest->schedule_minute) ? $oRequest->schedule_minute : 00;
                $dScheduleDate = Carbon::create($aDateParams[2], $aDateParams[0], $aDateParams[1],
                                                $nHours, $nMinute, 00, Auth::user()->timezone);
                if($oRequest->schedule_ampm == 'pm' && $oRequest->schedule_hour != 12) {
                    $dScheduleDate->addHour(12);
                }
                $dScheduleDate->setTimezone('UTC');
            }
            $sScheduleDate = $dScheduleDate->toDateTimeString();
            $dScheduleDate->addSeconds($oRequest->duration);
            $sScheduleEndDate = $dScheduleDate->toDateTimeString();

            //Remove all options when poll is being edit
            if(!empty($oRequest->id_post))
            {
                PollOption::where('id_poll',$oRequest->id_poll)
                        ->delete();
            }
            $oNewPoll = Poll::firstOrNew(['id_poll' => $oRequest->id_poll]);
            $oNewPoll->id_entity  = $oNewPost->id_post;
            $oNewPoll->entity_type= config('constants.ENTITYTYPEPOST');
            $oNewPoll->poll_text  = $oRequest->poll_question;
            $oNewPoll->file_name  = $sFileName;
            $oNewPoll->start_time = $sScheduleDate;
            $oNewPoll->end_time = $sScheduleEndDate;
            $oNewPoll->allow_multiple_answers = 0;
            $oNewPoll->poll_type = $oRequest->poll_type;
            $oNewPoll->activated  = ($oRequest->poll_release_type == "S") ? 0 : 1;
            $oNewPoll->deleted    = 0;
            $oNewPoll->save();

            if($oRequest->poll_type == config('constants.POLLTYPEMULTIPLE'))
            {
                $nCount = 1;
                foreach($oRequest->options as $sOption)
                {
                    PollOption::create([
                                        'id_poll'  => $oNewPoll->id_poll,
                                        'option_text'  => trim($sOption),
                                        'rank'  => $nCount,
                                        'activated'=> 1,
                                        'deleted'  => 0
                                        ]);
                    $nCount++;
                }
            }

            if(count($oRequest->group_list))
            {
                $aGroupList = $oRequest->group_list;
                //Add post as Group post and make entry in group_post table.
                GroupPost::create([
                                    'id_group' => $aGroupList[0],
                                    'id_post' => $oNewPost->id_post
                                ]);

                if($oNewPoll->activated && $bSendGroupNotification)
                {
                    //Send notification to all group members
                    $this->dispatch(new SendNewGroupPostNotification(Auth::user(), $aGroupList[0], $oNewPost->id_post));
                }

                array_shift($aGroupList);
                foreach ($aGroupList as $nIdGroup)
                {
                    $oNewGroupPost = $oNewPost->replicate(array('id_post'));
                    $oNewGroupPost->save();

                    $oNewGroupPoll = $oNewPoll->replicate(array('id_poll'));
                    $oNewGroupPoll->id_entity = $oNewGroupPost->id_post;
                    $oNewGroupPoll->save();

                    if($oRequest->poll_type == config('constants.POLLTYPEMULTIPLE'))
                    {
                        $nCount = 1;
                        foreach($oRequest->options as $sOption)
                        {
                            PollOption::create([
                                                'id_poll'  => $oNewGroupPoll->id_poll,
                                                'option_text'  => trim($sOption),
                                                'rank'  => $nCount,
                                                'activated'=> 1,
                                                'deleted'  => 0
                                                ]);
                            $nCount++;
                        }
                    }
                    GroupPost::create([
                                'id_group' => $nIdGroup,
                                'id_post' => $oNewGroupPost->id_post
                            ]);
                    if($oNewGroupPoll->activated && $bSendGroupNotification)
                    {
                        //Send notification to all group members
                        $this->dispatch(new SendNewGroupPostNotification(Auth::user(), $nIdGroup, $oNewGroupPost->id_post));
                    }
                }
            }
            return Response::json(['success' => true, 'redirect'=>  route('user.user-poll')]);
        }
        $oPoll='';
        if(!empty($nIdPost))
        {
            $oPoll = Post::getPostDetail($nIdPost);
            $oPoll->poll_options = array();
            if($oPoll->poll_type == config('constants.POLLTYPEMULTIPLE'))
            {
                $aPollOptions = PollOption::where('id_poll',$oPoll->id_poll)
                                            ->get();
                $oPoll->poll_options = $aPollOptions->all();
            }
            $sPollType = $oRequest->poll_type;
            $oView = \View::make('WebView::post._add_poll', compact('oPoll','sPollType'));
            $sHtml = $oView->render();
            return Response::json(['success' => true, 'html'=>$sHtml]);
        }
        return back();
    }

    public function callActivatePoll($nIdPoll)
    {
        $oPoll = Poll::find($nIdPoll);
        if(!empty($oPoll))
        {
            $oPost = Post::find($oPoll->id_entity);
            if(!empty($oPost) && $oPost->id_user == Auth::user()->id_user)
            {
                $dStartTime = new Carbon($oPoll->start_time);
                $dEndTime = new Carbon($oPoll->end_time);
                $nDIfferenceInSeconds = $dStartTime->diffInSeconds($dEndTime);

                $dScheduleDate = Carbon::now();
                $sScheduleDate = $dScheduleDate->toDateTimeString();
                $dScheduleDate->addSeconds($nDIfferenceInSeconds);
                $sScheduleEndDate = $dScheduleDate->toDateTimeString();

                $oPoll->start_time = $sScheduleDate;
                $oPoll->end_time = $sScheduleEndDate;
                $oPoll->activated = 1;
                $oPoll->deleted = 0;
                $oPoll->save();

                $oGroupPost = GroupPost::where('id_post',$oPost->id_post)
                                        ->first();
                if(count($oGroupPost))
                {
                    //Send notification to all group members
                    $this->dispatch(new SendNewGroupPostNotification(Auth::user(), $oGroupPost->id_group, $oGroupPost->id_post));
                }
                return Response::json(['success' => true, 'redirect'=>  route('user.user-poll')]);
            }
        }
        return Response::json(['success' => false, 'error' => trans('messages.poll_not_found'), 'redirect'=>  route('user.user-poll')]);
    }
    /**
     *
     * @param Request $oRequest
     * @return type
     */
    public function callAddComment(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [];

            switch ($oRequest->comment_type)
            {
                case config('constants.POSTTYPEIMAGE'):
                    $aValidationRequiredFor = [
                        'file_'.$oRequest->id_post => 'required|max:10000'
                    ];
                    if ($oRequest->hasFile('file_'.$oRequest->id_post)) {
                        $oRequest->offsetSet('extention_'.$oRequest->id_post, mb_strtolower($oRequest->file('file_'.$oRequest->id_post)->getClientOriginalExtension()));
                        $aValidationRequiredFor = [
                                                        'extention_'.$oRequest->id_post => 'required|in:bmp,jpg,jpeg,png,svg'
                                                    ];
                    }
                    break;
                case config('constants.POSTTYPEVIDEO'):
                    $aValidationRequiredFor = [
                        'file_'.$oRequest->id_post => 'required|max:100000'
                    ];
                    if ($oRequest->hasFile('file_'.$oRequest->id_post)) {
                        $oRequest->offsetSet('extention_'.$oRequest->id_post, mb_strtolower($oRequest->file('file_'.$oRequest->id_post)->getClientOriginalExtension()));
                        $aValidationRequiredFor = [
                                                        'extention_'.$oRequest->id_post => 'required|in:mov,mp4,avi,3gp,mkv'
                                                    ];
                    }
                    break;
                case config('constants.POSTTYPEDOCUMENT'):
                    $aValidationRequiredFor = [
                        'file_'.$oRequest->id_post => 'required|max:10000'
                    ];
                    if ($oRequest->hasFile('file_'.$oRequest->id_post)) {
                        $oRequest->offsetSet('extention_'.$oRequest->id_post, mb_strtolower($oRequest->file('file_'.$oRequest->id_post)->getClientOriginalExtension()));
                        $aValidationRequiredFor = [
                                                   'extention_'.$oRequest->id_post => 'required|in:doc,docx,pages,rtf,txt,wp,numbers,xlsx,xls,csv,key,ppt,pptx,pps,mdb,acc,accdb,pdf,zip,rar'
                                                ];
                    }
                    break;
                default :
                    //Code for text/code
                    $aValidationRequiredFor = [
                                                    'comment_text_'.$oRequest->id_post => 'required'
                                                ];
            }

            $this->validate($oRequest, $aValidationRequiredFor);

            $aRequestParams = $oRequest->all();
            $aInsertValues = [
                                'id_user' => Auth::user()->id_user,
                                'id_entity' => $aRequestParams['id_post'],
                                'entity_type' => 'P',
                                'comment_text' => $aRequestParams['comment_text_'.$oRequest->id_post],
                                'comment_type' => $aRequestParams['comment_type'],
                                'activated' => 1,
                                'deleted' => 0
                            ];

            if($aRequestParams['comment_type'] != config('constants.POSTTYPETEXT') && $aRequestParams['comment_type'] != config('constants.POSTTYPECODE'))
            {
                //Upload file in upload folder
                //And make entry in post_media table
                $oUploadedFile = $oRequest->file('file_'.$oRequest->id_post);

                $sOriginalName = $oUploadedFile->getClientOriginalName();

                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image

                //Move media on cloud server
                $oS3 = \Storage::disk('s3');
                $filePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                $oS3->put($filePath, fopen($oUploadedFile, 'r+'), 'public');

                $aInsertValues['display_file_name'] = $sOriginalName;
                $aInsertValues['file_name'] = $sFileName;
                $aInsertValues['file_type'] = $aRequestParams['comment_type'];
            }
            $oNewComment=Comment::create($aInsertValues);

            $this->dispatch(new SendCommentOnPostNotification(Auth::user(), $aRequestParams['id_post']));
        }
        if(isset($oRequest->flag) && $oRequest->flag==1){
            $oComment = Comment::getSingleComment($oNewComment->id_comment);
            $oComment->id_post=$oRequest->id_post;
            $view=  \View::make('WebView::post._photo_detail_comment', compact('oComment'));
            $html = $view->render();
        }
        else
        {
            $oFeed = Comment::getEntityComments($oRequest->id_post,'P');
            $oFeed->comments=$oFeed->last();
            //print_r($oFeed->comments);exit;
            $oFeed->id_post=$oRequest->id_post;
            $view=  \View::make('WebView::post._comment_ajax', compact('oFeed'));
            $html = $view->render();
        }

        return Response::json(['success' => true, 'html' => $html,'id_post' => $oRequest->id_post,'type'=>'comment']);
    }

    /**
     *
     * @param type $nIdPost
     * @return type
     */
    public function callLikePost($nIdPost)
    {
        $aWhereParams = [
                            'id_user' => Auth::user()->id_user,
                            'id_entity' => $nIdPost,
                            'entity_type' => 'P'
                        ];
        $oPostLike = Like::where($aWhereParams)
                            ->first();
        if($oPostLike)
        {
            $oPostLike->delete();
        }
        else
        {
            Like::create($aWhereParams);

            //Add notification for post creator
            $oPost = Post::find($nIdPost);
            if($oPost->id_user != Auth::user()->id_user)
            {
                Notification::create([
                                        'id_user' => $oPost->id_user,
                                        'id_entites' => Auth::user()->id_user.'_'.$nIdPost,
                                        'entity_type_pattern' => 'U_P', //U = user, P = post
                                        'notification_type' => config('constants.NOTIFICATIONLIKEONPOST')
                                    ]);
                
                //Send push notification
                $this->dispatch(new SendPostLikeNotification($nIdPost, Auth::user()->id_user));
            }
        }

        $nPostLikeCount = Like::where('id_entity', '=', $nIdPost)
                                ->where('entity_type', '=', 'P')
                                ->count();

        $sClasses = ($oPostLike) ? "btn btn-like" : "btn btn-like btn-like-user";
        $sResponseString =  '<div class="'.$sClasses.'" onclick="updatePostLike(\'like_button_'.$nIdPost.'\', \''.$nIdPost.'\');"></div>';
        if($nPostLikeCount > 0)
            $sResponseString .= '<a href="javascript:void(0);" onclick="getLikeUserList('.$nIdPost.');"><span>'.$nPostLikeCount.'</span></a>';

        return  $sResponseString;
    }

    /**
     *
     * @param Request $oRequest
     * @param type $nIdPost
     * @return string
     */
    public function callDeletePost(Request $oRequest, $nIdPost)
    {
        $oPost = Post::getPostDetail($nIdPost);
        if($oPost)
        {
            if($oPost->id_user == Auth::user()->id_user || ($oPost->id_group != '' && $oPost->member_type == config('constants.GROUPCREATOR')))
            {
                $oPost->activated = 0;
                $oPost->deleted = 1;
                $oPost->update();

                $sSuccessMessage = trans('messages.post_deleted_successfully');
                if($oPost->post_type == config('constants.POSTTYPEPOLL'))
                    $sSuccessMessage = trans('messages.poll_deleted_successfully');

                return Response::json(['success' => true,'msg'=>$sSuccessMessage ]);
            }
            else
            {
                $sErrorMessage = trans('messages.not_allowed_to_delete_post');
                if($oPost->post_type == config('constants.POSTTYPEPOLL'))
                    $sErrorMessage = trans('messages.not_allowed_to_delete_poll');

                return Response::json(['success' => false,'msg'=>$sErrorMessage ]);

            }
        }
        $sErrorMessage = trans('messages.post_not_found');
        if($oPost->post_type == config('constants.POSTTYPEPOLL'))
            $sErrorMessage = trans('messages.poll_not_found');

        return Response::json(['success' => false,'msg'=>$sErrorMessage ]);
    }

    public function callDeletePostComment(Request $oRequest, $nIdPostComment)
    {
        $oPostComment = Comment::find($nIdPostComment);
        $oPost = Post::getPostDetail($oPostComment->id_entity);
        if($oPostComment)
        {
            if($oPostComment->id_user == Auth::user()->id_user || ($oPost->id_group != '' && $oPost->member_type == config('constants.GROUPCREATOR') ))
            {
                $oPostComment->activated = 0;
                $oPostComment->deleted = 1;
                $oPostComment->update();
                return redirect()->back();
            }
            else
            {
                $oRequest->session()->flash('error_message', trans('messages.not_allowed_to_delete_post_comment'));
                return redirect()->back();
            }
        }
        $oRequest->session()->flash('error_message', trans('messages.post_not_found'));
        return '1';
    }

    public function callPhotoDetail($nIdPost,$nIdGroup='')
    {
        if($nIdGroup == '')
            $sAllowedComment = 1;
        else
        {
            $oGroupMember = new GroupMember();
            $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
            if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
                $sAllowedComment = 1;
            else
                $sAllowedComment = 0;
        }

        $oPostDetail['post_detail'] = Post::getPostDetail($nIdPost);

        $oComments = Comment::getEntityComments($oPostDetail['post_detail']->id_post, 'P');
        $oPostDetail['comments'] = $oComments->all();

        return \View::make('WebView::post._photo_detail', compact('oPostDetail','sAllowedComment'));
    }

    public function callPollDetail($nIdPost)
    {
        $oPostDetail['post_detail'] = Post::getPostDetail($nIdPost);
               if($oPostDetail['post_detail']->post_type == config('constants.POSTTYPEPOLL'))
                {
                    $oPollOption = PollOption::getPollOptions($oPostDetail['post_detail']->id_poll);
                    $oPostDetail['post_detail']->polloption=$oPollOption;
                    $nTotalAnswer= PollAnswer::where('id_poll','=',$oPostDetail['post_detail']->id_poll)->count();
                    $oPollAnswer = PollAnswer::getPollAnswer($oPostDetail['post_detail']->id_poll);
                    $oPostDetail['post_detail']['totalAns']=$nTotalAnswer;
                    foreach($oPollAnswer as $answer){
                        $percent=($answer->total * 100) / $nTotalAnswer;
                        $oPostDetail['post_detail']['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                    }
                    $oPollAllAnswer = PollAnswer::getPollAllAnswer($oPostDetail['post_detail']->id_poll);
                    $oPostDetail['post_detail']['pollanswer']=$oPollAllAnswer;
                }

        return \View::make('WebView::post._poll_detail', compact('oPostDetail'));
    }

    public function callPollAnswerExcel(Request $oRequest)
    {
        $oPostDetail['post_detail'] = Post::getPostDetail($oRequest->id_post);
        if($oPostDetail['post_detail']->post_type == config('constants.POSTTYPEPOLL'))
        {
            $oPollOption = PollOption::getPollOptions($oPostDetail['post_detail']->id_poll);
            $oPostDetail['post_detail']->polloption=$oPollOption;
            $nTotalAnswer= PollAnswer::where('id_poll','=',$oPostDetail['post_detail']->id_poll)->count();
            $oPollAnswer = PollAnswer::getPollAnswer($oPostDetail['post_detail']->id_poll);
            $oPostDetail['post_detail']['totalAns']=$nTotalAnswer;
            foreach($oPollAnswer as $answer){
                $percent=($answer->total * 100) / $nTotalAnswer;
                $oPostDetail['post_detail']['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
            }
            if($oPostDetail['post_detail']->id_group != ''){
                $oPollAllAnswer = PollAnswer::getPollGroupMemberAnswer($oRequest->id_poll,$oPostDetail['post_detail']->id_group);
                //echo count($oPollAllAnswer);
            }
            else
                $oPollAllAnswer = PollAnswer::getPollAllAnswer($oPostDetail['post_detail']->id_poll);
            $oPostDetail['post_detail']['pollanswer']=$oPollAllAnswer;
        }
//        echo count($oPostDetail['post_detail']['pollanswer']).'<pre><br/>';
//        print_r($oPostDetail['post_detail']['pollanswer']);
        //$oPollMemberCount = PollAnswer::getPollMemberCount($oPostDetail['post_detail']->id_poll);
        $aPollContent = array();
        $oPollDetail = $oPostDetail['post_detail'];
        $aPollText = array($oPollDetail->poll_text);
        //array_push($aPollContent, );
        if($oPollDetail->poll_type == config('constants.POLLTYPEMULTIPLE') && count($oPollDetail->polloption) > 0)
        {
            $sChar = 'A';

            foreach($oPollDetail->polloption as $option)
            {
                $aPollOptions[$option->id_poll_option]=$sChar;
                $aPollOption = array();
                array_push($aPollOption, $sChar);
                array_push($aPollOption, $option->option_text);
                array_push($aPollOption, $oPollDetail['pollanswer_'.$option->id_poll_option].'%');
                array_push($aPollContent, $aPollOption);
                $sChar++;
            }
        }
        //array_push($aPollContent, $aTotal);
        array_push($aPollContent, array());
        array_push($aPollContent, array());
        $aPollAnswerHeading = array('User Name','NetId','Email','User Answer');
        //array_push($aPollContent, $aPollAnswerHeading);
        $aPollUserContent = array();
//        echo count($oPollDetail['pollanswer']).'<pre><br/>';
//        print_r($oPollDetail['pollanswer']);exit;
        if(count($oPollDetail['pollanswer']) > 0)
        {
            foreach($oPollDetail['pollanswer'] as $oPollAns)
            {
                $aUserPollAns = array();
                $sUserName = $oPollAns->first_name.' '.$oPollAns->last_name;
                $aEmail = explode('@', $oPollAns->email);
                $aUserPollAns = array($sUserName , $aEmail[0], $oPollAns->email);
                if($oPollDetail->poll_type == config('constants.POLLTYPEMULTIPLE') && $oPollAns->id_poll_option != '')
                {
                    array_push($aUserPollAns, $aPollOptions[$oPollAns->id_poll_option]);
                }
                else if($oPollDetail->poll_type == config('constants.POLLTYPEOPEN') && $oPollAns->poll_answer_description != '')
                {
                    array_push($aUserPollAns, $oPollAns->poll_answer_description);
                }
                else
                    array_push($aUserPollAns, trans('messages.not_attempted'));
                array_push($aPollUserContent, $aUserPollAns);
            }
        }
        $sFileName = 'Poll';
        $aBlankArray = array();
        $aTotal = array(trans('messages.votes') ,$oPollDetail->totalAns.'/'.count($oPollDetail['pollanswer']));
        //print_r($aPollContent);exit;
        Excel::create($sFileName, function($excel) use($aPollText,$aPollContent, $aTotal, $aPollAnswerHeading, $aPollUserContent,$aBlankArray) {
                                // Set the title
                                $excel->setTitle('Poll Detail');
                                // Chain the setters
                                $excel->setCreator('Campusknot')
                                      ->setCompany('Campusknot.com');

                                $excel->sheet('Poll', function($sheet) use($aPollText,$aPollContent, $aTotal, $aPollAnswerHeading, $aPollUserContent,$aBlankArray) {
                                    $sheet->appendRow($aPollText);
                                    $sheet->mergeCells('A1:D1');
                                    $sheet->row($sheet->getHighestRow(), function ($row) {
                                                    $row->setFontFamily('Arial');
                                                    $row->setFontSize(12);
                                                    $row->setFontWeight('bold');
                                                });
                                    $sheet->rows($aPollContent);
                                    $sheet->appendRow($aTotal);
                                    $sheet->appendRow($aBlankArray);
                                    $sheet->appendRow($aBlankArray);

                                    $sheet->appendRow($aPollAnswerHeading);
                                    $sheet->row($sheet->getHighestRow(), function ($row) {
                                                    $row->setFontFamily('Arial');
                                                    $row->setFontSize(12);
                                                    $row->setFontWeight('bold');
                                                });

                                    $sheet->rows($aPollUserContent);
                                });
        })->download('xls');
    }
    public function callAddPollAnswer(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            if($oRequest->expired == 0)
            {
                $aValidationRequiredFor = [
                                            'id_poll'=> 'required'
                                        ];
                if($oRequest->poll_type == config('constants.POLLTYPEMULTIPLE'))
                {
                    $aValidationRequiredFor['id_poll_option_'.$oRequest->id_post] = 'required';
                }
                else
                {
                    $aValidationRequiredFor['poll_answer_text_'.$oRequest->id_post] = 'required';
                }
                $this->validate($oRequest, $aValidationRequiredFor);

                $oRequestParam = $oRequest->all();
                $oPollAnswer = PollAnswer::firstOrNew([
                                                        'id_poll'        => $oRequest->id_poll,
                                                        'id_user'        => Auth::user()->id_user,
                                                    ]);
                if($oRequest->poll_type == config('constants.POLLTYPEMULTIPLE'))
                {
                    $oPollAnswer->id_poll_option = $oRequestParam['id_poll_option_'.$oRequest->id_post];
                }
                else
                {
                    $oPollAnswer->poll_answer_description = trim($oRequestParam['poll_answer_text_'.$oRequest->id_post]);
                }
                $oPollAnswer->save();
            }

            $oUserFeeds[0] = Post::getPostDetail($oRequest->id_post);
            if($oUserFeeds[0]->post_type==config('constants.POSTTYPEPOLL'))
            {
                $oPollOption = PollOption::getPollOptions($oUserFeeds[0]->id_poll);
                $oUserFeeds[0]->polloption=$oPollOption;
                if($oUserFeeds[0]->id_poll_answer !='' || strtotime($oUserFeeds[0]->end_time) <= time())
                {
                    $nTotalAnswer= PollAnswer::where('id_poll','=',$oUserFeeds[0]->id_poll)->count();
                    $oPollAnswer = PollAnswer::getPollAnswer($oUserFeeds[0]->id_poll);
                    $oUserFeeds[0]['totalAns']=$nTotalAnswer;
                    foreach($oPollAnswer as $answer)
                    {
                        $percent=($answer->total * 100) / $nTotalAnswer;
                        $oUserFeeds[0]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                    }
                }
            }
            $view=  \View::make('WebView::user._more_poll', compact('oUserFeeds'));
            $html = $view->render();
            return Response::json(['success' => true, 'html' => $html,'id_post' => $oRequest->id_post,'type' => 'poll']);
        }
    }

    public function callPostDetail($nIdPsot)
    {
        $oFeed = Post::getPostDetail($nIdPsot);
        if(count($oFeed))
        {
            if($oFeed->post_type==config('constants.POSTTYPEPOLL'))
            {
                $oPollOption = PollOption::getPollOptions($oFeed->id_poll);
                $oFeed->polloption=$oPollOption;
                if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time()){
                    $nTotalAnswer= PollAnswer::where('id_poll','=',$oFeed->id_poll)->count();
                    $oPollAnswer = PollAnswer::getPollAnswer($oFeed->id_poll);
                    $oFeed['totalAns']=$nTotalAnswer;
                    foreach($oPollAnswer as $answer){
                        $percent=0;
                        $percent=($answer->total * 100) / $nTotalAnswer;

                        $oFeed['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                    }
                }
                return \View::make('WebView::post.post_detail', compact('oFeed',''));
            }
        $oComments = Comment::getEntityComments($nIdPsot, 'P');
        $oFeed->comments = $oComments->all();
        return \View::make('WebView::post.post_detail', compact('oFeed'));
        }
        else
            return response()->view('errors.404');
    }
    public function callPostLikeUser($nIdPost)
    {
        $oUserList = Like::getEntityLikes($nIdPost,config('constants.ENTITYTYPEPOST'), Auth::user()->id_user);
        foreach($oUserList as $key=>$oUser)
        {
            $oUserList[$key]->major=UserEducation::getMajorEducation($oUser->id_user);
        }
        return \View::make('WebView::post.post_like_userlist', compact('oUserList'));
    }

    public function callAddContentReport(Request $oRequest)
    {
        $aValidationRequiredFor = [
                                    'id_entity' => 'required',
                                    'entity_type' => 'required',
                                    'report_text' => 'required'
                                ];
        $this->validate($oRequest, $aValidationRequiredFor);

        //Add user payment details
        $oUserReport = UserReportedContent::firstOrNew([
                                                            'id_user' => Auth::user()->id_user,
                                                            'id_entity' => $oRequest->id_entity,
                                                            'entity_type' => $oRequest->entity_type
                                                        ]);
        $oUserReport->report_description = $oRequest->report_text;
        $oUserReport->activated = 1;
        $oUserReport->deleted = 0;
        $oUserReport->save();

        //Get count and delete entity if count is greater.
        $nReportCounts = UserReportedContent::where('id_entity',$oRequest->id_entity)
                                            ->where('entity_type', $oRequest->entity_type)
                                            ->count();
        if($nReportCounts >= config('constants.MAXALLOWEDREPORTS'))
        {
            switch ($oRequest->entity_type)
            {
                case "P":
                    $oPost = Post::find($oRequest->id_entity);
                    $oPost->activated = 0;
                    $oPost->deleted = 1;
                    $oPost->save();
                    break;
                case "C":
                    $oComment = Comment::find($oRequest->id_entity);
                    $oComment->activated = 0;
                    $oComment->deleted = 1;
                    $oComment->save();
                    break;
                default:
                    break;
            }
        }
    }
}
