<?php
namespace App\Http\Web\Controllers;

use App\Http\Web\Controllers\Controller;

use App\Libraries\AllowedEmail;
use App\Libraries\Campus;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Libraries\Document;
use App\Libraries\Post;
use App\Libraries\Comment;
use App\Libraries\CampusExtension;
use Auth;

use Intervention\Image\Facades\Image;

class UtilityController extends Controller
{
    protected $aMSOfficeExtentions = ['doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'];
    
    public function __construct()
    {
        $this->middleware('auth', [
                                    'only' => [
                                                'callViewFile', 'callDownloadFile'
                                            ]
                                ]
                        );
    }
    
    public function getCampus(Request $oRequest)
    {
        $aCampuses = array();
        
        if(filter_var($oRequest->email, FILTER_VALIDATE_EMAIL))
        {
            $sUrl = trim(mb_strstr($oRequest->email, '@'));
            $sUrl = trim($sUrl, "@");
            
            $aCampuses = CampusExtension::getCampusFromExtetion($sUrl);
            
            $bCampusNotFound = !count($aCampuses);
            
            //Check if sub-domain exist
            if($bCampusNotFound)
            {
                $aUrlComponents = explode(".",$sUrl);

                foreach($aUrlComponents as $sUrlComponent)
                {
                    $sUrl = mb_strstr($sUrl, ".");
                    $sUrl = trim($sUrl, ".");
                    
                    $aNewUrlComponents = explode(".",$sUrl);
                    if(count($aNewUrlComponents) >= 2)
                    {
                        $aCampuses = CampusExtension::getCampusFromExtetion($sUrl);
                    }
                    if(count($aCampuses))
                    {
                        $bCampusNotFound = !count($aCampuses);
                        break;
                    }
                }
            }
            //Check if email exist in allowed_emails
            if($bCampusNotFound)
            {
                $oAllowedEmail = AllowedEmail::where('email',$oRequest->email)
                                            ->first();
                if(!empty($oAllowedEmail))
                {
                    $aCampuses = Campus::where('id_campus',$oAllowedEmail->id_campus)
                                        ->get();
                    $bCampusNotFound = !count($aCampuses);
                }
            }
        }
        return \View::make('WebView::utility._campuses', compact('aCampuses'));
    }
    
    public function callViewFile($sIdDocument, $sDisplayFileName)
    {   
        $oS3 = \Storage::disk('s3');
        $sDisplayUrl = '';
        
        $nIdDocument = mb_substr($sIdDocument, 2) ?: '';
        if($nIdDocument != '' && mb_substr($sIdDocument, 0, 2) == 'd_')
        {
            $oDocument = Document::find($nIdDocument);
            $oCurrentPer = Document::getAncestorsWithPermission($oDocument, Auth::user()->id_user);
            if(count($oCurrentPer) > 0 )
            {
                $sFileName = $oDocument->file_name;
            }
            else
            {
                return response()->view('errors.access_denied');
            }
        }
        else if($nIdDocument != '' && mb_substr($sIdDocument, 0, 2) == 'c_')
        {
            $oComment = Comment::find($nIdDocument);
            //$oCurrentPer = Document::countAncestorsWithWritePermission($oDocument, Auth::user()->id_user);
            if($oComment->comment_type == config('constants.POSTTYPEDOCUMENT') || $oComment->comment_type == config('constants.POSTTYPEIMAGE'))
            {
                $sFileName = $oComment->file_name;
            }
            else
            {
                return response()->view('errors.access_denied');
            }
        }
        else
        {
            $oPost = Post::getPostDetail($nIdDocument);
            if($oPost->post_type == config('constants.POSTTYPEDOCUMENT') || $oPost->post_type == config('constants.POSTTYPEIMAGE'))
                $sFileName = $oPost->file_name;
            else
                echo trans('messages.no_document'); 
        }
        
        if(!empty($sFileName) && $oS3->exists(config('constants.POSTMEDIAFOLDER').'/'.$sFileName))
        {
            $sFileUrl = $oS3->url(config('constants.POSTMEDIAFOLDER').'/'.$sFileName);
            
            $aFileType = explode('.', $sFileName);
            if(in_array(last($aFileType), $this->aMSOfficeExtentions))
            {
                $sDisplayUrl = "https://view.officeapps.live.com/op/view.aspx?src=".urlencode($sFileUrl);
            }
            else if(mb_strtolower(last($aFileType)) == "pdf")
            {
                $oFile = $oS3->get(config('constants.POSTMEDIAFOLDER').'/'.$sFileName);
            
                $oResponse = \Response::make($oFile, 200);
                $oResponse->header('Content-Type', "application/pdf");
                return $oResponse;
            }
            else if(in_array (mb_strtolower(last($aFileType)), array("png", "jpg", "jpeg")))
            {
                $oFile = $oS3->get(config('constants.POSTMEDIAFOLDER').'/'.$sFileName);
            
                $oResponse = \Response::make($oFile, 200);
                $oResponse->header('Content-Type', "image/".mb_strtolower(last($aFileType)));
                return $oResponse;
            }
        }
        return \View::make('WebView::utility.view_file', compact('sDisplayUrl', 'sDisplayFileName','sFileName','sIdDocument'));
    }
    
    public function callDownloadFile($sIdDocument, $sDisplayFileName)
    {
        $nIdDocument = mb_substr($sIdDocument, 2) ?: '';
        if($nIdDocument != '' && mb_substr($sIdDocument, 0, 2) == 'd_')
        {
            $oDocument = Document::find($nIdDocument);
            $oCurrentPer = Document::getAncestorsWithPermission($oDocument, Auth::user()->id_user);
            if(count($oCurrentPer) > 0 )
            {
                $sFileName = $oDocument->file_name;
            }
            else
            {
                return response()->view('errors.access_denied');
            }
        }
        else if($nIdDocument != '' && mb_substr($sIdDocument, 0, 2) == 'c_')
        {
            $oComment = Comment::find($nIdDocument);
            //$oCurrentPer = Document::countAncestorsWithWritePermission($oDocument, Auth::user()->id_user);
            if($oComment->comment_type == config('constants.POSTTYPEDOCUMENT') || $oComment->comment_type == config('constants.POSTTYPEIMAGE'))
            {
                $sFileName = $oComment->file_name;
            }
            else
            {
                return response()->view('errors.access_denied');
            }
        }
        else
        {
            $oPost = Post::getPostDetail($nIdDocument);

            if($oPost->post_type == config('constants.POSTTYPEDOCUMENT') || $oPost->post_type == config('constants.POSTTYPEIMAGE'))
                $sFileName = $oPost->file_name;
            else
                echo 'There is no Document found.'; 
        }

        $oS3 = \Storage::disk('s3');
        
        if(!empty($sFileName) && $oS3->exists(config('constants.POSTMEDIAFOLDER').'/'.$sFileName))
        {
            $oFile = $oS3->get(config('constants.POSTMEDIAFOLDER').'/'.$sFileName);
            
            $oResponse = \Response::make($oFile, 200);
            $oResponse->header('Content-Type', 'application/octet-stream');
            return $oResponse;
        }
        else
        {
            echo 'File not found.';
        }
    }
    
    public function callMediaParser(Request $oRequest)
    {
        $sUrl = $oRequest->url;
        $oValidator = Validator::make($oRequest->all(), [
                                                        'url' => 'url',
                                            ]);
        if ($oValidator->fails()) 
        {
            $aParsedMetaTags["html"] = "";
            $aParsedMetaTags["url"] = 'Invalid Url';
            
            header("Content-type: application/json");
            echo json_encode($aParsedMetaTags, JSON_FORCE_OBJECT);
            return;
        }
        $oCURL = curl_init();

        curl_setopt_array($oCURL, array(
        CURLOPT_RETURNTRANSFER 	=> 1,
            CURLOPT_URL => "https://noembed.com/embed?url=".$sUrl,	
            CURLOPT_POST => 0
        ));

        // Send the request & save response to $curl_response
        $sCurlResponse = curl_exec($oCURL);

        // Close request to clear up some resources
        curl_close($oCURL);

        // check if Noembed API has returned error or request itself failed
        if( $sCurlResponse === FALSE || isset(json_decode($sCurlResponse)->error) )
        {
                // parse meta tags of $url when Noembed fails
                $oCURL = curl_init();
                curl_setopt_array($oCURL, array(
                                                CURLOPT_URL => $sUrl,
                                                CURLOPT_RETURNTRANSFER => 1,
                                                CURLOPT_FOLLOWLOCATION => 1,
                                                CURLOPT_USERAGENT => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:49.0) Gecko/20100101 Firefox/49.0'
                                            )
                                );
                $sPageHtml = curl_exec($oCURL);
                curl_close($oCURL);
                // parse meta tags of $url when Noembed fails
                $oDom       = new \DOMDocument();
                @$oDom      -> loadHTML($sPageHtml);

                // push all parsed meta tags here
                $aParsedMetaTags = array();

                // Loop through all meta tags
                foreach ( $oDom -> getElementsByTagName("meta") as $oProperties )
                {
                    $sPropertyAttribute = $oProperties->getAttribute("property");
                    $sNameAttribute     = $oProperties->getAttribute("name");
                    $sContentAttribute  = $oProperties->getAttribute("content");

                    // title
                    if(isset($sPropertyAttribute) && $sPropertyAttribute == "og:title") {
                        $aParsedMetaTags["title"] = $sContentAttribute;
                    }
                    elseif (isset($sNameAttribute) && $sNameAttribute == "title") {
                        $aParsedMetaTags["title"] = $sContentAttribute;
                    }

                    // description
                    if(isset($sPropertyAttribute) && $sPropertyAttribute == "og:description") { 
                        $aParsedMetaTags["description"] = $sContentAttribute;
                    }
                    elseif (isset($sNameAttribute) && $sNameAttribute == "description") {
                        $aParsedMetaTags["description"] = $sContentAttribute;
                    }

                    // thumbnail
                    if(isset($sPropertyAttribute) && $sPropertyAttribute == "og:image") { 
                        $aParsedMetaTags["thumbnail_url"] = $sContentAttribute;
                    }
                    elseif (isset($sNameAttribute) && $sNameAttribute == "thumbnail") {
                        $aParsedMetaTags["thumbnail_url"] = $sContentAttribute;
                    }

                    // type
                    if (isset($sPropertyAttribute) && $sPropertyAttribute == "og:type") {
                        $aParsedMetaTags["type"] = $sContentAttribute;
                    }
                    else {
                        $aParsedMetaTags["type"] = "other";
                    }
                }
                    
                    $sThumbnailUrl = asset("assets/web/img/default-thumbnail.png");
                    
                    if(isset($aParsedMetaTags["thumbnail_url"]))
                    {
                        if(isUrlExist($aParsedMetaTags["thumbnail_url"]))
                        {
                            $sThumbnailUrl = url('/set-image?url=').urlencode($aParsedMetaTags["thumbnail_url"]);
                        }
                    }
                    $sHtml  = "<a href='".$sUrl."' nofollow  target='_blank'><div class='media_thumbnail'>";
                    $sHtml .= "<img src='";
                    $sHtml .= $sThumbnailUrl;
                    $sHtml .= "'>";
                    $sHtml .= "<div>";
                        if(isset($aParsedMetaTags["type"]) && mb_strtolower($aParsedMetaTags["type"]) == "video") {
                            $sHtml .= "<img src='";
                            $sHtml .= asset('web/img/play_button.png');
                            $sHtml .= "'>";
                        }
                        $sHtml .= "<span>";
                            $sHtml .= isset($aParsedMetaTags["title"]) ? $aParsedMetaTags["title"] : $sUrl;
                        $sHtml .= "</span>";
                    $sHtml .= "</div>";
                $sHtml .= "</div> </a>";
                $aParsedMetaTags["html"] = $sHtml;
                $aParsedMetaTags["url"] = $sUrl;

                header("Content-type: application/json");
                echo json_encode($aParsedMetaTags, JSON_FORCE_OBJECT);
        }
        else {

                header("Content-type: application/json");
                echo $sCurlResponse;
        }
    }
    
    public function callSetImage(Request $oRequest)
    {
        if(!empty($oRequest->url))
        {
            return Image::make(urldecode($oRequest->url))->response();
        }
        
        return Image::make(asset("assets/web/img/default-thumbnail.png"))->response();
    }
}