<?php

namespace App\Http\Vue\Controllers;

use App\Http\Vue\Controllers\Controller;

use App\Libraries\University;
use App\Libraries\Campus;
use App\Libraries\Course;
use App\Libraries\User;
use App\Libraries\Group;
use App\Libraries\GroupMember;
use App\Libraries\GroupEvent;
use App\Libraries\GroupRequest;
use App\Libraries\EventRequest;
use App\Libraries\NonMemberInvitation;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

use App\Jobs\SendNewsletter;
use App\Jobs\ShareGroupDocumentsWithNewMember;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
                                    'only' => [
//                                                'callAddCourse', 'callEditCourse','callDeleteCourse'
                                                ]
                                    ]
                        );
        parent::__construct();
        session(['current_page' => 'groups']);
    }
    
    public function index()
    {
        //print_r(count(Auth()->user()));exit;
        if(Auth()->user()) {
            return redirect('/');
        }
        
        return \View::make('VueView::home.index');
    }
    
    public function callShowAppDownloadOptions()
    {
        return \View::make('WebView::home.show_app_download_options');
    }
    
    public function callAboutPage()
    {
        return \View::make('WebView::home.about');
    }
    
    public function callMobileApp()
    {
        return \View::make('WebView::home.mobile_app');
    }
    
    public function callContactPage(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
           
            $oValidator = [
                    'name' => 'required',
                    'surname' => 'required',
                    'email' => 'required|email',
                    'phone' => 'numeric|digits_between:5,15',//'regex:/[0-9]{10}/',
                    'message' => 'required'
                ]; 
            $this->validate($oRequest, $oValidator);
            
            $aRequestParams = $oRequest->all();
            
            Mail::queue('WebView::emails.sendContact', ['sName'=>$aRequestParams['name'],
                'sSurName'=>$aRequestParams['surname'],
                'sEmail'=>$aRequestParams['email'],
                'nPhone'=>$aRequestParams['phone'],
                'sMessage'=>$aRequestParams['message']], function ($oMessage) use ($aRequestParams) {
                $oMessage->from($aRequestParams['email'], $aRequestParams['name'].' '.$aRequestParams['surname']);

                $oMessage->to(config('mail.from.address'), config('mail.from.name'))
                        ->subject(trans('messages.contact_send')." :".$aRequestParams['contact_type']);
            }); 
            Mail::queue('WebView::emails.replyContact', ['sName'=>$aRequestParams['name'].' '.$aRequestParams['surname']], 
                    function ($oMessage) use ($aRequestParams) {
                        $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                        $oMessage->to($aRequestParams['email'], $aRequestParams['name'].' '.$aRequestParams['surname'])
                                ->subject(trans('messages.subject_reply_contact'));
                    }); 
            return Response::json(['success' => true, 'html' => trans('messages.contact_send_successfully')]);
            
        }
        return \View::make('WebView::home.contact');
    }

    public function callPrivacyPage()
    {
       
        return \View::make('WebView::home.privacy');
    }
    
    public function callTermsncondiPage()
    {
       
        return \View::make('WebView::home.terms');
    }
    
    public function callSendVerificationMail(Request $oRequest)
    {
        $error_msg ='';
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email|exists:users'
                                    ];
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $oUser = User::where(['email' => $oRequest->email,'activated'=>0,'deleted'=> 0])->first();
            if(count($oUser))
            {
                $sVerificationKey = $oUser->verification_key;
                $sUserName = $oUser->first_name.' '.$oUser->last_name;
                $sEmail = $oUser->email;
                
                $oCampus = Campus::find($oUser->id_campus);
                $sCampusName = $oCampus->campus_name;
                
                Mail::queue('WebView::emails.verification', ['sVerificationKey'=>$sVerificationKey,'name'=>$sUserName,'sCampusName'=>$sCampusName], function ($oMessage) use ($sEmail, $sUserName) {
                        $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                        $oMessage->to($sEmail, $sUserName)
                                ->subject(trans('messages.verification_mail_subject'));
                    });
                $oRequest->session()->flash('success_message', trans('messages.verification_link_send_successfully'));
                if(isset($oRequest->page) && $oRequest->page =='login')
                    return redirect()->back();
                    
                return redirect('/home/send-verification-mail')->with('email', $oRequest->email);
            }
            else 
                $error_msg = trans('messages.user_already_verified');
        }
        return \View::make('WebView::home.send_verification_mail',  compact('error_msg'))->withInput(['email' => $oRequest->email]);
    }
    
    public function callAddCourse(Request $oRequest)
    {
        $oUniversities = University::all(['id_university','university_name']);
        $oCourseList = Course::getCourseListing();

        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                            'id_university' => 'required'
                                        ];
            if($oRequest->hasFile('file'))
            {
                $aValidationRequiredFor['file'] = 'required|mimes:csv,txt|max:10000';
                
                $this->validate($oRequest, $aValidationRequiredFor);
                
                $oUploadedFile = $oRequest->file('file');
                
                $sDestinationPath = base_path().'/uploads/'; // upload path
                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
                $oUploadedFile->move($sDestinationPath, $sFileName); // uploading file to given path
                
                $hFileResource = fopen($sDestinationPath.$sFileName,'r');
                if (($hFileResource) !== FALSE)
                {
                    while (($aData = fgetcsv($hFileResource)) !== FALSE)
                    {
                        if(!empty(trim($aData[0])) && !empty(trim($aData[1])) && !empty(trim($aData[2])))
                        {
                            $oCourse = Course::firstOrNew([
                                                        'course_name' => trim($aData[2]),
                                                        'course_code' => trim($aData[0]).' '.trim($aData[1]),
                                                        'id_university' => $oRequest->id_university
                                                    ]);
                            $oCourse->save();
                        }
                    }
                    fclose($hFileResource);
                }
                //Remove local copy of media
                unlink($sDestinationPath.$sFileName);
            }
            else
            {
                $aValidationRequiredFor['course_name'] = 'required|unique:courses,course_name,'.$oRequest->id_course.',id_course,id_university,'.$oRequest->id_university.',course_code,'.$oRequest->course_code;
                $aValidationRequiredFor['course_code'] = 'required|unique:courses,course_code,'.$oRequest->id_course.',id_course,id_university,'.$oRequest->id_university;
                
                $this->validate($oRequest, $aValidationRequiredFor);
                
                $oCourse = Course::firstOrNew(['id_course' => $oRequest->id_course]);
                $oCourse->course_name   = $oRequest->course_name;
                $oCourse->course_code   = $oRequest->course_code;
                $oCourse->id_university = $oRequest->id_university;
                $oCourse->save();
            }
            
            return redirect('/home/add-course');
        }
        
        return \View::make('WebView::home.add_course', compact('oUniversities','oCourseList'));
    }
    
    public function callEditCourse($nIdCourse)
    {
        $oUniversities = University::all(['id_university','university_name']);
        $oCourseList = Course::getCourseListing();
        
        $oCourseEdit = Course::where('id_course','=',$nIdCourse)
                                ->where('activated','=',1)
                                ->where('deleted','=',0)
                                ->first();
        
        return \View::make('WebView::home.add_course', compact('oUniversities','oCourseList','oCourseEdit'));
    }
    
    public function callDeleteCourse($nIdCourse)
    {
        $oCourse = Course::find($nIdCourse);
        $oCourse->activated   = 0;
        $oCourse->deleted   = 1;
        $oCourse->save();
        
        return Redirect::route('home.add-course');
    }
    
    public function callSendNewsLetter(Request $oRequest)
    {
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'user_type' => 'required',
                                        'campus_name' => 'required',
                                        'id_campus' => 'required'
                                    ];
            
            $sSubject = (!empty($oRequest->subject)) ? $oRequest->subject : 'New Feature: Now You Can Copy Document and file to different folders.';
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $this->dispatch(new SendNewsletter($oRequest->id_campus,$sSubject,$oRequest->user_type));
        }
        return \View::make('WebView::home.send_newsletter', compact('oUniversities','oCourseList','oCourseEdit'));
    }
    
    public function callCampusSuggetions(Request $oRequest)
    {
        $sCampusName = isset($oRequest->campus_name) ? $oRequest->campus_name : '';
       
        $oCampusList = \App\Libraries\Campus::getCampusSuggetions($sCampusName);
	return Response::json($oCampusList);
    }
    
    public function callAddUser(Request $oRequest)
    {
        $aCampuses = Campus::get();
        
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                            'id_campus' => 'required',
                                            'user_type' => 'required'
                                        ];
            if($oRequest->hasFile('file'))
            {
                $aValidationRequiredFor['file'] = 'required|mimes:csv,txt|max:10000';
            }
            else
            {
                $aValidationRequiredFor['first_name'] = 'required|regex:/(^[A-Za-z.!@$&"#\' ]+$)+/';
                $aValidationRequiredFor['last_name'] = 'required|regex:/(^[A-Za-z.!@$&"#\' ]+$)+/';
                $aValidationRequiredFor['email'] = 'required|email|unique:users';
            }
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $aFaultyRecords = array();
            if($oRequest->hasFile('file'))
            {
                $oUploadedFile = $oRequest->file('file');
                
                $sDestinationPath = storage_path('app/public/'); // upload path
                $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
                $oUploadedFile->move($sDestinationPath, $sFileName); // uploading file to given path
                
                $hFileResource = fopen($sDestinationPath.$sFileName,'r');
                if (($hFileResource) !== FALSE)
                {
                    $nCount = 1;
                    while (($aData = fgetcsv($hFileResource)) !== FALSE)
                    {
                        if(!empty(trim($aData[0])) && !empty(trim($aData[1])) && !empty(trim($aData[2])))
                        {
                            $sFirstName = trim($aData[0]);
                            $sLastName = trim($aData[1]);
                            $sEmail = trim($aData[2]);
                            
                            $aUserDataValidation = [
                                                        'first_name' => 'required|without_numeric',
                                                        'last_name' => 'required|without_numeric',
                                                        'email' => 'required|email|unique:users'
                                                    ];
                            $aUserData = [
                                            'first_name' => $sFirstName,
                                            'last_name' => $sLastName,
                                            'email' => $sEmail,
                                        ];
                            $oValidator = Validator::make($aUserData, $aUserDataValidation);
                            if(!$oValidator->fails())
                            {
                                $dCurrentTime = Carbon::now();
                                $sCurrentTime = $dCurrentTime->toDateTimeString();
                                
                                $sSalt = hash('md5', $sEmail.$sCurrentTime);
                                $sPassword = sha1('123456'.$sSalt);
                                
                                $oUser = User::firstOrNew([
                                                        'first_name' => $sFirstName,
                                                        'last_name' => $sLastName,
                                                        'email' => $sEmail,
                                                        'password' => $sPassword,
                                                        'salt' => $sSalt,
                                                        'id_campus' => $oRequest->id_campus,
                                                        'user_type' => $oRequest->user_type,
                                                        'verified' => 1,
                                                        'verification_key' => 'verified'
                                                    ]);
                                $oUser->save();
                                
                                //Make user to member of default campus group
                                $oCampusGroupMember = $this->addUserToCampusGroup($oUser);

                                $this->addAllGroupEventsToNewMember($oCampusGroupMember);

                                //Get pending request from non_member_invitations table and update in appropriate tables.
                                $this->updatePendingRequests($oUser);
                            }
                            else {
                                $aFaultyRecords[] = 'Row No: '.$nCount;
                            }
                        }
                        else {
                            $aFaultyRecords[] = 'Row No: '.$nCount;
                        }
                        $nCount ++;
                    }
                    fclose($hFileResource);
                    
                }
                //Remove local copy of media
                unlink($sDestinationPath.$sFileName);
            }
            else
            {
                $dCurrentTime = Carbon::now();
                $sCurrentTime = $dCurrentTime->toDateTimeString();
                
                $sSalt = hash('md5', $oRequest->email.$sCurrentTime);
                $sPassword = sha1('123456'.$sSalt);
                
                $oUser = User::firstOrNew([
                                        'first_name' => $oRequest->first_name,
                                        'last_name' => $oRequest->last_name,
                                        'email' => $oRequest->email,
                                        'password' => $sPassword,
                                        'salt' => $sSalt,
                                        'id_campus' => $oRequest->id_campus,
                                        'user_type' => $oRequest->user_type,
                                        'verified' => 1,
                                        'verification_key' => 'verified'
                                    ]);
                $oUser->save();
                
                //Make user to member of default campus group
                $oCampusGroupMember = $this->addUserToCampusGroup($oUser);
                
                $this->addAllGroupEventsToNewMember($oCampusGroupMember);
                
                //Get pending request from non_member_invitations table and update in appropriate tables.
                $this->updatePendingRequests($oUser);
            }
            if(count($aFaultyRecords))
            {
                $aErrors['faulty_records'] = array(implode(',', $aFaultyRecords));
                return redirect('/add-user')
                            ->withErrors($aErrors);
            }
            $oRequest->session()->flash('success_message', trans('messages.user_added_success'));
            return redirect('/add-user');
        }
        return \View::make('WebView::home.add_user', compact('aCampuses'));
    }
    
    private function addUserToCampusGroup($oUser)
    {
        $oCampusGroup = Group::where('id_campus', '=', $oUser->id_campus)
                                ->where('id_group_category', '=', config('constants.UNIVERSITYGROUPCATEGORY'))
                                ->where('activated', '=', 1)
                                ->where('deleted', '=', 0)
                                ->select(
                                            'id_group'
                                        )
                                ->first();
        
        $oCampusGroupMember = GroupMember::firstOrNew([
                                                        'id_group' => $oCampusGroup->id_group,
                                                        'id_user' => $oUser->id_user
                                                    ]);
        $oCampusGroupMember->member_type = config('constants.GROUPMEMBER');
        $oCampusGroupMember->activated = 1;
        $oCampusGroupMember->deleted = 0;
        $oCampusGroupMember->save();
        
        $this->dispatch(new ShareGroupDocumentsWithNewMember($oUser->id_user, $oCampusGroup->id_group));
        return $oCampusGroupMember;
    }
    
    private function addAllGroupEventsToNewMember($oGroupMember)
    {
        //Get all events of group.
        $aGroupEvents = GroupEvent::getAllGroupEvents($oGroupMember->id_group);
        
        //Add event request for new group member.
        foreach ($aGroupEvents as $oGroupEvent)
        {
            $oEventRequest = EventRequest::firstOrNew([
                                                        'id_event' => $oGroupEvent->id_event,
                                                        'id_user_request_to' => $oGroupMember->id_user
                                                    ]);
            $oEventRequest->id_user_request_from = $oGroupEvent->id_user;
            $oEventRequest->status = NULL;
            $oEventRequest->save();
        }
    }
    
    private function updatePendingRequests($oUser)
    {
        $oPendingRequests = NonMemberInvitation::where('invite_to', '=', $oUser->email)
                                                ->get();
        
        foreach ($oPendingRequests as $oPendingRequest)
        {
            switch ($oPendingRequest->entity_type)
            {
                case 'G':
                {
                    $oGroup = Group::find($oPendingRequest->id_entity_invite_for);
                    if($oGroup->id_campus == $oUser->id_campus)
                    {
                        $oGroupMember = GroupMember::where('id_group', '=', $oPendingRequest->id_entity_invite_for)
                                                    ->where('id_user', '=', $oUser->id_user)
                                                    ->where('activated', '=', 1)
                                                    ->where('deleted', '=', 0)
                                                    ->first();
                        if(!$oGroupMember)
                        {
                            $oGroupRequest = GroupRequest::firstOrNew([
                                                                        'id_group' => $oPendingRequest->id_entity_invite_for,
                                                                        'id_user_request_to' => $oUser->id_user
                                                                    ]);
                            $oGroupRequest->id_user_request_from = $oPendingRequest->id_user_invite_from;
                            $oGroupRequest->request_type = 'I';
                            $oGroupRequest->member_status = 'P';
                            $oGroupRequest->save();
                        }
                    }
                    if(isset($oGroupRequest) || $oGroup->id_campus != $oUser->id_campus)
                    {
                        //Delete pending request from non_member_invitation table
                        $oPendingRequest->delete();
                    }
                    break;
                }
                
                case 'E':
                {
                    $oEventRequest = EventRequest::firstOrNew([
                                                            'id_event' => $oPendingRequest->id_entity_invite_for,
                                                            'id_user_request_to' => $oUser->id_user
                                                        ]);
                    $oEventRequest->id_user_request_from = $oPendingRequest->id_user_invite_from;
                    $oEventRequest->status = NULL;
                    $oEventRequest->save();
                    
                    if($oEventRequest)
                    {
                        //Delete pending request from non_member_invitation table
                        $oPendingRequest->delete();
                    }
                    break;
                }
                
                default :
                    break;
            }
        }
    }
    
    public function callAddDummyGroupMember($nIdGroup)
    {
        $oGroup = Group::find($nIdGroup);
        $oUser = User::find($oGroup->id_user);
        $oUsersList = User::where(['activated' =>1,'deleted'=>0,'verified'=>1,'id_campus'=>$oUser->id_campus])
                         ->where('id_user','!=',$oGroup->id_user)
                         ->get();
        foreach($oUsersList as $aUser)
        {
            $oGroupMember = GroupMember::firstOrNew(['id_group' => $nIdGroup,'id_user'=>$aUser->id_user]);
            $oGroupMember->id_group = $nIdGroup;
            $oGroupMember->id_user = $aUser->id_user;
            $oGroupMember->member_type = config('constants.GROUPMEMBER');
            $oGroupMember->activated = 1;
            $oGroupMember->deleted = 0;
            $oGroupMember->save();
        }
        return "success";
    }
}
