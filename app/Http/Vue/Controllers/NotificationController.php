<?php

namespace App\Http\Web\Controllers;

use App\Http\Web\Controllers\Controller;

use App\Libraries\User;
use App\Libraries\UserSession;
use App\Libraries\Group;
use App\Libraries\GroupEvent;
use App\Libraries\GroupRequest;
use App\Libraries\EventRequest;
use App\Libraries\GroupMember;

use App\Libraries\Notification;
use App\Libraries\ReadNotification;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Jobs\ShareGroupDocumentsWithNewMember;
use App\Jobs\ShareUpcomingQuizWithNewGroupMember;
use App\Jobs\SyncUserGroups;

class NotificationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
                'only' => [
                    'callShowNotifications', 'callNotificationListing',
                    'ChangeNotificationStatus','addAllGroupEventsToNewMember','CallChangeGroupNotificationStatus','ChangeEventStatus'
                ]
            ]
        );
        parent::__construct();
    }

    public function callShowNotifications(Request $oRequest)
    {
        UserSession::where('id_user', '=', Auth::user()->id_user)
                    ->update(array('badge_count' => 0 ));
        
        $oGroupInviteNotifications = array();
        $oEventInviteNotifications = array();
        $oInformativeNotifications = array();
        $oEntityDetails = array();

        $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
        $nLimit = config('constants.PERPAGERECORDS');

        $nGroupRequestCount = GroupRequest::getGroupPendingRequestCount(Auth::user()->id_user);
        $nEventRequestCount = EventRequest::getEventPendingRequestCount(Auth::user()->id_user);
        $nInformativeNotificationCount = Notification::where('id_user', '=', Auth::user()->id_user)
            ->count();
        if ($nGroupRequestCount > (($nPageNumber - 1) * $nLimit)) {
            //Get max 10 group requests
            $oGroupInviteNotifications = GroupRequest::getLoginUserGroupRequests(Auth::user()->id_user, ($nPageNumber - 1)*$nLimit, $nLimit);
            $oGroupInviteNotifications->add_header = ($nPageNumber > 1) ? FALSE : TRUE;
        }

        //If group requests are less then 10 fetch event requests
        if (($nEventRequestCount + $nGroupRequestCount) > (($nPageNumber - 1) * $nLimit)) {
            if (count($oGroupInviteNotifications) < config('constants.PERPAGERECORDS')) {
                $nSkipRecords = (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) - $nGroupRequestCount;
                $nSkipRecords = max(0, $nSkipRecords);

                $nLimit = config('constants.PERPAGERECORDS') - count($oGroupInviteNotifications);
                $oEventInviteNotifications = EventRequest::getEventRequest(Auth::user()->id_user, $nSkipRecords, $nLimit);
                $oEventInviteNotifications->add_header = ($nSkipRecords > 0) ? FALSE : TRUE;
            }
        }

        //If group + event request are less then 10 fetch informative notifications
        if ((count($oGroupInviteNotifications) + count($oEventInviteNotifications)) < config('constants.PERPAGERECORDS')) {
            $nSkipRecords = (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) - ($nGroupRequestCount + $nEventRequestCount);
            $nSkipRecords = max(0, $nSkipRecords);

            $nLimit = config('constants.PERPAGERECORDS') - (count($oGroupInviteNotifications) + count($oEventInviteNotifications));
            $oInformativeNotifications = Notification::getUserInformativeNotification(Auth::user()->id_user, $nSkipRecords, $nLimit);            
            $oInformativeNotifications->add_header = ($nSkipRecords > 0) ? FALSE : TRUE;
            $oUnreadNotifications = Notification::getUnreadNotifications(Auth::user()->id_user, $nSkipRecords, $nLimit);

            foreach ($oUnreadNotifications as $oUnreadNotification) {
                ReadNotification::create([
                    'id_notification' => $oUnreadNotification->id_notification
                ]);
            }

            $oEntityDetails = array();
            foreach ($oInformativeNotifications as $oInformativeNotification) {
                $oEntityDetails[$oInformativeNotification->id_notification] = Notification::getEntitiesFromPattern($oInformativeNotification->entity_type_pattern, $oInformativeNotification->id_entites);
            }
        }
        $nTotalRecords = $nGroupRequestCount + $nEventRequestCount + $nInformativeNotificationCount;
        $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

        $aMessages['total'] = $nTotalRecords;
        $aMessages['per_page'] = config('constants.PERPAGERECORDS');
        $aMessages['current_page'] = $nPageNumber;
        $aMessages['last_page'] = $nLastPage;
        $aMessages['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
        $aMessages['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
        $aMessages['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
        $aMessages['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
        
        if ($nPageNumber > 1)
            return \View::make('WebView::notification._more_notification', compact('oGroupInviteNotifications', 'oEventInviteNotifications', 'oInformativeNotifications', 'oEntityDetails', 'aMessages'));
        return \View::make('WebView::notification._notification_listing', compact('oGroupInviteNotifications', 'oEventInviteNotifications', 'oInformativeNotifications', 'oEntityDetails', 'aMessages'));
    }


    public function CallChangeGroupNotificationStatus($sRequestStatus, $nGroupRequestId)
    {
        $oGroupRequest = GroupRequest::find($nGroupRequestId);
        if($oGroupRequest->id_user_request_to == Auth::user()->id_user)
        {
            $oGroupRequest->member_status = $sRequestStatus;
            $oGroupRequest->save();

            if ($sRequestStatus == config('constants.GROUPMEMBERACCEPTED'))
            {
                if ($oGroupRequest->request_type == config('constants.GROUPREQUESTTYPEINVITE')) 
                {
                    $oGroupMember = GroupMember::firstOrNew([
                        'id_group' => $oGroupRequest->id_group,
                        'id_user' => Auth::user()->id_user
                    ]);
                } 
                else 
                {
                    $oGroupMember = GroupMember::firstOrNew([
                        'id_group' => $oGroupRequest->id_group,
                        'id_user' => $oGroupRequest->id_user_request_from
                    ]);
                    $oAdminUser     = User::find($oGroupRequest->id_user_request_to);
                    $oRequestUser   = User::find($oGroupRequest->id_user_request_from);
                    $oGroupDetail   = Group::find($oGroupRequest->id_group);
                    Mail::queue('WebView::emails.admin_accept_request', ['sName' => $oAdminUser->first_name . ' ' . $oAdminUser->last_name, 'nIdGroup' => $oGroupDetail->id_group, 'sRequestBy' => $oRequestUser->first_name . ' ' . $oRequestUser->last_name, 'sGroupName' => $oGroupDetail->group_name], function ($oMessage) use ($oRequestUser, $oAdminUser) {
                        $oMessage->from(config('mail.from.address'), $oAdminUser->first_name . ' ' . $oAdminUser->last_name);

                        $oMessage->to($oRequestUser->email, $oRequestUser->first_name . ' ' . $oRequestUser->last_name)
                            ->subject(trans('messages.group_accept_invitation_subject'));
                    });
                }

                $oGroupMember->member_type = config('constants.GROUPMEMBER');
                $oGroupMember->activated = 1;
                $oGroupMember->deleted = 0;
                $oGroupMember->save();
                //Add all group events to new member
                $this->dispatch(new SyncUserGroups($oGroupMember->id_user));
                $this->addAllGroupEventsToNewMember($oGroupMember);

                $this->dispatch(new ShareGroupDocumentsWithNewMember($oGroupMember->id_user, $oGroupRequest->id_group));
                $this->dispatch(new ShareUpcomingQuizWithNewGroupMember($oGroupMember->id_user, $oGroupRequest->id_group));

            }
    
        }
    }

    private function addAllGroupEventsToNewMember($oGroupMember)
    {
        //Get all events of group.
        $dStartDate = "1970-01-01";
        $aGroupEvents = GroupEvent::getGroupEvents($oGroupMember->id_group, $dStartDate);
        
        //Add event request for new group member.
        foreach ($aGroupEvents as $oGroupEvent)
        {
            $oEventRequest = EventRequest::firstOrNew([
                                                        'id_event' => $oGroupEvent->id_event,
                                                        'id_user_request_to' => $oGroupMember->id_user
                                                    ]);
            $oEventRequest->id_user_request_from = $oGroupEvent->id_user;
            $oEventRequest->status = NULL;
            $oEventRequest->save();
        }
    }
   
    public function ChangeEventStatus($Status, $NotificationId)
    {
        $oNotification = EventRequest::find($NotificationId);
        if($oNotification->id_user_request_to == Auth::user()->id_user)
            $objUnreadGroupInviteNotification = Notification::ChangeEventStatus($Status, $NotificationId);
    }
}