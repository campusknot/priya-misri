<script>
var MEDIA_URL = '{{config('constants.MEDIAURL')}}';



var current_event;
var current_event_request;

function closeDetailBox()
{
    $('#slider_light_box').hide();
}
function setEventDataReset()
{
    $('#model_event_status').val('');
    $('#model_event_title').html('');
    $('#model_event_time').html('');
    $('#model_event_date').html('');
    // https://www.google.com/maps/preview/@<latitude>,<longitude>,<zoom level>z
    $('#model_event_place').attr('href','');
    $('#model_event_place').text('');
    $('#event_desc').html();
    $('#btn_admin_fun_p').hide();
}
function setEventData(data)
{
    $('#model_event_status').val(data.status);
    current_event_request = data.id_event_request;
    var event = data;
    current_event = event.id_event;
    //alert(current_event);
    $('#id_event').val(current_event);
    $('#model_event_title').html(event.event_title);
    $('#model_event_time').html(event.start_date_Hma);
    $('#model_event_date').html(event.start_date_mjy);
    // https://www.google.com/maps/preview/@<latitude>,<longitude>,<zoom level>z
    $('#model_event_place').attr('href','https://www.google.com/maps/preview/@'+event.latitude+','+event.longitude+',8z');
    $('#model_event_place').text(event.address +' '+ event.room_number);
    /*$('#model_event_group').html(event.title);*/
    $('#model_event_status').val(event.user_response).attr("selected", "selected");
    $('#event_desc').html(event.event_description);

    if(event.is_user_admin)
    {
        $('#btn_admin_fun_p').show();
    }else{
        $('#btn_admin_fun_p').hide();
    }


    if(data.feeds){
        strJsondata = '';
        for(var i in data.feeds)
        {
           strJsondata+= '<div class="col-lg-1 col-md-2 col-sm-2 col-xs-2">'+
                                            '<div class="icon-image img-50">'+
                                                '<img src="assets/web/img/slider_img.jpg" /></div></div>';

           strJsondata+= '<div class="col-lg-11 col-md-10 col-sm-10 col-xs-10 clearfix">'+
                            '<div class="post-person-name">'+ data.feeds[i].first_name+ " "+data.feeds[i].last_name
                            + '<span class="post-time">11d ago</span><span>'+data.feeds[i].post_text+'</span>'+
                                '<span class="edit-links"> <a class="cursor-pointer" href="#">edit</a>'+
                                                    '<a class="cursor-pointer"> delete</a>'
                                                + '</span></div></div>';

        };
       document.getElementById('divEventFeeds').innerHTML =  strJsondata;
    }
    /* */
    //
    $('#myModalEventDetail').modal('show');
}

function setEventStatus()
{
    $.ajax({
        type: 'GET',
        url: '/event/status/'+ current_event_request ,
        data: {"action": "set_event_status" , "status":$('#model_event_status').val()},
        dataType: 'json',
        success: function (data) {
            alert(data.msg);
        },
        error: function (jqXHR, exception) {
            //loader_stop();
            showError(jqXHR, exception);
        },
        beforeSend: function () {
            // loader_start();
        }
    });
}

function callEventAction(action)
{
    if(action == 'EDIT')
    {
        getSingleEvent(current_event ,'EDIT');
    }else if(action == 'INVITE')
    {
        showInviteEventPopUp();
    }else if(action == 'DELETE')
    {
        deleteEventData(current_event);
    }
}

function getEvent(id) {
        //FormId = 'pub_'+id;
        $('#myModalEventDetail_slide').modal('hide');
        $.ajax({
            type: 'GET',
            url: '/event/' + id ,
            data: {"id": id},
            success: function (data) {
                openLightBox(data);
            },
            error: function (jqXHR, exception) {
                loader_stop();
                showError(jqXHR, exception);
            },
            beforeSend: function () {
                // loader_start();
            }
        });
    }

function showInviteEventPopUp()
{
    $.ajax({
        type: "GET",
        url: '/event/invite-to-event',
        success: function (data) {
            openLightBox(data);
        },
        error: function (data) {
            console.log('Error:', data);
        }
    });
}

function setEditEventData(data)
{
    current_event_request = data.id_event_request;
    var event = data.event;
    current_event = event.id_event;

    $('#id_ev_edt_event_title').val(event.event_title);
    $('#id_ev_edt_event_description').val(event.event_description);

    $('#id_ev_edt_start_date').val(event.start_date_dmyhma);
    $('#id_ev_edt_end_date').val(event.end_date_dmyhma);

    $('#id_ev_edt_place').val(event.address);
    $('#id_ev_edt_latitude').val(event.latitude);
    $('#id_ev_edt_longitude').val(event.longitude);
    $('#id_ev_edt_room_number').val(event.room_number);
    $('#myModal-update-event').modal('show');
}
function updateEventInfo() {
    $('#form_update_user_event').attr('action', '/event/' + current_event);
    $('#form_update_user_event').submit();
}

$.ajaxSetup({
    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
});
</script>

<!-- update event modal starts here -->
<div class="modal fade" id="myModal-update-event" tabindex="-1" role="dialog" aria-labelledby="myModal-update-event">
    <div class="modal-dialog create-event" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="create_event">{{ trans('messages.edit_event') }}</h4>
            </div>
            <div class="modal-body"><!-- Start: Model Body -->
                <form method="post" action="{{ route('event.update-event' ,['0']) }}" id="form_update_user_event" class="form-horizontal">
                    {{ method_field('PATCH') }}
                    {!! csrf_field() !!}
                    <div class="">
                        <div class="form-group clearfix">
                            <div class="col-lg-12">
                                <input type="text" name="event_title" id="id_ev_edt_event_title" class="form-control" placeholder="Event title" >
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-lg-12">
                                <textarea  type="textarea"  name="event_description" id="id_ev_edt_event_description" class="form-control" placeholder="Desciption (Optional)" ></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                <div class="datepicker">
                                    <input type="text" id="datetimepicker_start_add" name="start_date" placeholder="Select Date">
                                    <select required class="time-hour" name="start_hour">
                                        <option value="" disabled selected hidden>hh</option>
                                        <option value='2'>01</option>
                                        <option value='3'>02</option>
                                        <option value='4'>03</option>
                                        <option value='5'>04</option>
                                        <option value='1'>05</option>
                                        <option value='2'>06</option>
                                        <option value='3'>07</option>
                                        <option value='4'>08</option>
                                        <option value='5'>09</option>
                                        <option value='1'>10</option>
                                        <option value='2'>11</option>
                                        <option value='3'>12</option>
                                    </select>
                                        {{ trans('messages.colon') }}
                                    <select required class="time-minutes" name="start_minute">
                                        <option value="" disabled selected hidden>mm</option>
                                        <option value='1' >00</option>
                                        <option value='2'>05</option>
                                        <option value='3'>10</option>
                                        <option value='4'>15</option>
                                        <option value='5'>20</option>
                                        <option value='1'>25</option>
                                        <option value='2'>30</option>
                                        <option value='3'>35</option>
                                        <option value='4'>40</option>
                                        <option value='5'>45</option>
                                        <option value='1'>50</option>
                                        <option value='2'>55</option>
                                        <option value='3'>60</option>
                                    </select>
                                    <span class="time-maridian1 pull-right">
                                       <label class="time-am">
                                           <span> 
                                               <!-- <img src="{{ asset('assets/web/img/sun-black-20.png') }}" alt="" />  -->
                                           </span> AM<input type="radio" name="">
                                       </label>
                                       <label class="time-pm none">
                                           <span>
                                              <!--   <img src="{{ asset('assets/web/img/moon-black-20.png') }}" alt="" /> -->
                                           </span>PM<input type="radio" name="">
                                       </label>

                                   </span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                               <div class="pull-left add-end-date">
                                   <input type="checkbox" id="end_time" aria-label="...">Add end date
                                </div>
                            </div>
                        </div> <!-- start date over -->
                         <!-- end date start -->
                        <div class="form-group end-date ">
                           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8  ">
                                <div class="datepicker">
                                    <input type="text" id="datetimepicker_end_add" name="end_date" placeholder="End date">
                                    <select required class="time-hour" name="start_hour">
                                       <option value="" disabled selected hidden>hh</option>
                                       <option value='1'>01</option>
                                       <option value='2'>02</option>
                                       <option value='3'>03</option>
                                       <option value='4'>04</option>
                                       <option value='5'>05</option>
                                       <option value='6'>06</option>
                                       <option value='7'>07</option>
                                       <option value='8'>08</option>
                                       <option value='9'>09</option>
                                       <option value='10'>10</option>
                                       <option value='11'>11</option>
                                       <option value='12'>12</option>
                                   </select>
                                   {{ trans('messages.colon') }}
                                    <select required class="time-minutes" name="start_minute">
                                        <option value="" disabled selected hidden>mm</option>
                                        <option value='0' >00</option>
                                        <option value='5'>05</option>
                                        <option value='10'>10</option>
                                        <option value='15'>15</option>
                                        <option value='20'>20</option>
                                        <option value='25'>25</option>
                                        <option value='30'>30</option>
                                        <option value='35'>35</option>
                                        <option value='40'>40</option>
                                        <option value='45'>45</option>
                                        <option value='50'>50</option>
                                        <option value='55'>55</option>
                                        <option value='60'>60</option>
                                    </select>
                                    <span class="time-maridian2 pull-right">
                                       <label class="time-am">
                                           <span> 
                                               <!-- <img src="{{ asset('assets/web/img/sun-black-20.png') }}" alt="" /> -->
                                           </span> AM<input type="radio" name="">
                                       </label>
                                       <label class="time-pm none">
                                           <span>
                                                <!-- <img src="{{ asset('assets/web/img/moon-black-20.png') }}" alt="" /> -->
                                           </span>PM<input type="radio" name="">
                                       </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-lg-6 pull-left">
                                <div id="datetimepicker_edt" class="input-append date">
                                    <input type="text"  id='id_ev_edt_start_date' name="start_date"/>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                     </span>
                                </div>
                             </div>
                            <div class="col-lg-6 pull-right">
                                <div id="datetimepicker1_edt" class="input-append date">
                                    <input type="text"  id='id_ev_edt_end_date' name="end_date"/>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-lg-12">
                                <style>
                                    .pac-container {
                                        z-index: 3500 !important;
                                    }
                                </style>
                                <div id="locationField">
                                    <input id="id_ev_edt_place" name="address" class="form-control" placeholder="Enter your address" onFocus="geolocate()" type="text"/>
                                </div>
                                <input type="text" name="latitude" id="id_ev_edt_latitude"   style="display: none;" />
                                <input type="text" name="longitude" id="id_ev_edt_longitude"  style="display: none;" />
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="col-lg-12">
                                <input type="text"  name="room_number" id="id_ev_edt_room_number" class="form-control" placeholder="Room No." >
                            </div>
                        </div>
                    </div>
                    <div class="submit-options clearfix">
                        <button type="button" class="btn btn-default pull-right " data-dismiss="modal" aria-label="Close">{{ trans('messages.cancel') }}</button>
                        <button type="button" class="btn btn-primary pull-right" onclick="updateEventInfo();"  id="btn_add_event">{{ trans('messages.update_event') }}</button>
                    </div>
                </form>

            </div> <!-- End: Model Body -->
        </div>
    </div>
</div>
<!-- create event modal ends here -->

<script>


$("#end_time").click(function(){
    $(".end-date").toggleClass("sh");
});
$( "#datetimepicker_start_add" ).datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
    yearRange: 'c-10:c+3',
    showButtonPanel: false            
});
$( "#datetimepicker_end_add" ).datepicker({
    dateFormat: 'dd-mm-yy',
    changeYear: true,
    changeMonth: true,
    yearRange: 'c-10:c+3',
    showButtonPanel: false            
});

$('#ui-datepicker-div').addClass('blockMsg');
$('#ui-datepicker-div').addClass('ll-skin-nigran');

$("select").change(function() {
    alert("hello");
    $(this).css('color','#EB974E')
})
</script>