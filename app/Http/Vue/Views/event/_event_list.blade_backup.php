<div class="event-detail">
<?php
    $sPreviousYear = ($oUserEventList->currentPage() > 1) ? ((Session::has('event_list_current_year')) ? Session::get('event_list_current_year') : '') : '';
    $sPreviousDate = ($oUserEventList->currentPage() > 1) ? ((Session::has('event_list_current_date')) ? Session::get('event_list_current_date') : '') : '';
    $nEventCount = 0;
    
        foreach($oUserEventList as $oEventDetail)
        {
            $dEventStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $oEventDetail->start_date);
            $dEventEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $oEventDetail->end_date);
            $sEventYear = $dEventStartDate->format('Y');
            $sEventDate = $dEventStartDate->format('M j');
            
            if($sEventDate != $sPreviousDate)
            {
                session(['event_list_current_date' => $sEventDate]);
                $sPreviousDate = $sEventDate;
                if($nEventCount > 0)
                    echo '</div><div class="event-detail">';

                if($sEventYear != $sPreviousYear)
                {
                    session(['event_list_current_year' => $sEventYear]);
                    $sPreviousYear = $sEventYear;
                    echo '<span class="event-day pull-left">'.$sEventYear.'</span>';
                }
    ?>
                <span class="event-day">{{$dEventStartDate->format('M j')}}  | {{$dEventStartDate->format('l')}}</span>
    <?php } ?>
                <ul onclick="getSingleEvent({{$oEventDetail->id_event}});" class="{!! str_replace(array(' ', '.','@'), '_', $oEventDetail->calendar_name) !!} show">
                    
                    <li class="center">
                        <span class="event-time">{{$dEventStartDate->format('g:i A')}}</span>
                    </li>
                    <li>
                        @if($oEventDetail->event_from == config('constants.EVENTTYPEGOOGLE'))
                        <span class="google-event-indicator">
                            <img src="{{asset('assets/web/img/google_indicator.png')}}">
                        </span>
                        @endif
                        <div class="event-name">{{$oEventDetail->event_title}}</div><!-- event -->
                        <div class="">
                            <img src="{{asset('assets/web/img/event-date.png')}}" alt="">
                            <span>{{$dEventStartDate->format('l')}} {{$dEventStartDate->format('M j')}}, {{$dEventStartDate->format('g:i A')}} - {{ ($dEventStartDate->toDateString() == $dEventEndDate->toDateString()) ? $dEventEndDate->format('g:i A') : $dEventEndDate->format('l').' '.$dEventEndDate->format('M j').', '.$dEventEndDate->format('g:i A')}}</span>
                        </div>

                        @if($oEventDetail->latitude != '0.00000000' &&  $oEventDetail->longitude != '0.00000000' )
                        <a class="event-place"  target="_blank" href="{{config('constants.GMAPURL')}}?q={{$oEventDetail->address}}">
                            <img src="{{asset('assets/web/img/location.png')}}" alt="">
                            <span>{{!empty($oEventDetail->room_number) ? $oEventDetail->room_number.', ' : ''}}{{$oEventDetail->address}}</span>
                        </a><!-- place -->
                        @elseif(trim($oEventDetail->address) != '')
                        <div class="event-place">
                            <img src="{{asset('assets/web/img/location.png')}}" alt="">
                            <span>{{!empty($oEventDetail->room_number) ? $oEventDetail->room_number.', ' : ''}}{{$oEventDetail->address}}</span>
                        </div>
                        @endif

                        @if(isset($oEventDetail->group_names))
                        <div class="event-group">
                            <img src="{{asset('assets/web/img/event-group.png')}}" alt="">
                            <span>{{$oEventDetail->group_names}}</span>
                        </div>
                        @endif
                    </li>
                </ul>
    <?php 
            $nEventCount++ ;
        }
    if(count($oUserEventList) <= 0)
    {
?>
       <div class="no-data">
            <img src="{{asset('assets/web/img/no-events.png')}}">
            {{ trans('messages.no_events') }}
        </div>
<?php
    }
?>
</div>
<script type="text/javascript">
    nCurrentPage = <?php echo $oUserEventList->currentPage(); ?>;
    nLastPage = <?php echo $oUserEventList->lastPage(); ?>;
</script>
<!-- End: tab content div -->