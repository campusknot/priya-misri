<div class="modal-content create-event">
    <div class="modal-header">
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="create_event" class="modal-title">{{ trans('messages.create_event') }}</h4>
    </div>
    <?php
        $nIdEvent = isset($oEvent->id_event) ? $oEvent->id_event : old('id_event');
        $sEventTitle = isset($oEvent->event_title) ? $oEvent->event_title : old('event_title');
        $sEventDescription = isset($oEvent->event_description) ? $oEvent->event_description : old('event_description');
        
        $sStartDate = isset($oEvent->start_date) ? $oEvent->start_date : (mb_strlen(old('start_date')) ? old('start_date') : '');
        $sDisplayStartDate = '';
        $sStartHours = '';
        $sStartMinutes = '';
        $sStartAMPM = 'AM';
        $sEndAMPM = 'AM';
        
        if(mb_strlen($sStartDate))
        {
            $dStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $sStartDate);
            $dStartDate->setTimezone(Auth::user()->timezone);
            $sDisplayStartDate = $dStartDate->toDateString();
            $sStartTime = $dStartDate->toTimeString();
            $aStartTime = explode(":", $sStartTime);
            $sStartHours = $aStartTime[0];            
            if($sStartHours > 12)
            {
                $sStartHours = $sStartHours - 12;
                $sStartAMPM = 'PM';
            }
            //when time come from database is 00 its 12 Am for that date selected
            if($sStartHours == 12)
                $sStartAMPM = 'PM';
            elseif($sStartHours == 00)
                $sStartHours=12;
            $sStartMinutes = $aStartTime[1];
        }
        
        $sEndDate = isset($oEvent->end_date) ? $oEvent->end_date : (mb_strlen(old('end_date')) ? old('end_date') : '');
        $sDisplayEndDate = '';
        $sEndHours = '';
        $sEndMinutes = '';

        if(mb_strlen($sEndDate))
        {
            $dEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $sEndDate);
            $dEndDate->setTimezone(Auth::user()->timezone);
            $sDisplayEndDate = $dEndDate->toDateString();
            $sEndTime = $dEndDate->toTimeString();
            $aEndTime = explode(":", $sEndTime);
            $sEndHours = $aEndTime[0];
            if($sEndHours > 12)
            {
                $sEndHours = $sEndHours - 12;
                $sEndAMPM = 'PM';
            }
            if($sEndHours == 12)
                $sEndAMPM = 'PM';
            elseif($sEndHours == 00)
                $sEndHours=12;
            $sEndMinutes = $aEndTime[1];
        }
        $sAddress = isset($oEvent->address) ? $oEvent->address : old('address');
        $sRoomNumber = isset($oEvent->room_number) ? $oEvent->room_number : old('room_number');
    ?>
    <div class="modal-body"><!-- Start: Model Body -->
        <form id="form_add_user_event" class="form-horizontal" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input id="id_event" name="id_event" type="hidden" value="{{$nIdEvent}}" />
            <input id="latitude" name="latitude" type="hidden" />
            <input id="longitude" name="longitude" type="hidden" />
            <div class="form-group {{ ($errors->has('event_title')) ? 'has-error' : ''}}">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input id="id_ev_event_title" class="form-control" name="event_title" type="text" title="{{($errors->has('event_title')) ? $errors->first('event_title') : '' }}" placeholder="{{ trans('messages.event_title') }}" value="{{$sEventTitle}}" data-toggle="tooltip" data-placement="top" >
                </div>
            </div>
            <div class="form-group {{ ($errors->has('event_description')) ? 'has-error' : ''}}">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <textarea id="id_ev_event_description" class="form-control" name="event_description" type="textarea" title="{{ ($errors->has('event_description')) ? $errors->first('event_description') : '' }}" placeholder="{{ trans('messages.event_description') }}" data-toggle="tooltip" data-placement="top" >{{$sEventDescription}}</textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="datepicker {{ ($errors->has('start_date') || $errors->has('start_hour') || $errors->has('start_minute')) ? 'error' : ''}}">
                        <input type="text" id="add_start_date" name="start_date" title="{{ ($errors->has('start_date')) ? $errors->first('start_date') : ''}}" placeholder="{{trans('messages.start_date')}}" value="{{$sDisplayStartDate}}" data-toggle="tooltip" data-placement="top" >
                        <select required class="time-hour" name="start_hour" title="{{ ($errors->has('start_hour')) ? $errors->first('start_hour') : ''}}" data-toggle="tooltip" data-placement="top" >
                            <option value=''>hh</option>
                            <option value='01' <?php echo $sStartHours==1?"selected":"" ?>>01</option>
                            <option value='02' <?php echo $sStartHours==2?"selected":"" ?>>02</option>
                            <option value='03' <?php echo $sStartHours==3?"selected":"" ?>>03</option>
                            <option value='04' <?php echo $sStartHours==4?"selected":"" ?>>04</option>
                            <option value='05' <?php echo $sStartHours==5?"selected":"" ?>>05</option>
                            <option value='06' <?php echo $sStartHours==6?"selected":"" ?>>06</option>
                            <option value='07' <?php echo $sStartHours==7?"selected":"" ?>>07</option>
                            <option value='08' <?php echo $sStartHours==8?"selected":"" ?>>08</option>
                            <option value='09' <?php echo $sStartHours==9?"selected":"" ?>>09</option>
                            <option value='10' <?php echo $sStartHours==10?"selected":"" ?>>10</option>
                            <option value='11' <?php echo $sStartHours==11?"selected":"" ?>>11</option>
                            <option value='12' <?php echo $sStartHours==12?"selected":"" ?>>12</option>
                        </select>
                        :
                        <select required class="time-minutes " name="start_minute" title="{{($errors->has('start_minute')) ? $errors->first('start_minute') : '' }}" data-toggle="tooltip" data-placement="top" >
                            <option value=''>mm</option>
                            <option value='00' <?php echo $sStartMinutes==00?"selected":"" ?>>00</option>
                            <option value='05' <?php echo $sStartMinutes==05?"selected":"" ?>>05</option>
                            <option value='10' <?php echo $sStartMinutes==10?"selected":"" ?>>10</option>
                            <option value='15' <?php echo $sStartMinutes==15?"selected":"" ?>>15</option>
                            <option value='20' <?php echo $sStartMinutes==20?"selected":"" ?>>20</option>
                            <option value='25' <?php echo $sStartMinutes==25?"selected":"" ?>>25</option>
                            <option value='30' <?php echo $sStartMinutes==30?"selected":"" ?>>30</option>
                            <option value='35' <?php echo $sStartMinutes==35?"selected":"" ?>>35</option>
                            <option value='40' <?php echo $sStartMinutes==40?"selected":"" ?>>40</option>
                            <option value='45' <?php echo $sStartMinutes==45?"selected":"" ?>>45</option>
                            <option value='50' <?php echo $sStartMinutes==50?"selected":"" ?>>50</option>
                            <option value='55' <?php echo $sStartMinutes==55?"selected":"" ?>>55</option>
                        </select>
                        <input id="time_start_am" type="radio" class="hidden" name="start_ampm" value="am" {{ ($sStartAMPM == 'AM') ? 'checked' : '' }} >
                        <input id="time_start_pm" type="radio" class="hidden" name="start_ampm" value="pm" {{ ($sStartAMPM == 'PM') ? 'checked' : '' }} >
                        <label for="time_start_am" class="time-start-am">{{ trans('messages.am') }}</label>
                        <label for="time_start_pm" class="time-start-pm">{{ trans('messages.pm') }}</label>
                    </div>
                    
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 end_date padding-10 add-end-time {{ ($sDisplayEndDate !='') ? 'hidden' : '' }}">
                    {{ trans('messages.add_end_time') }}
                </div>
            </div>
            <!-- start date over -->
            <!-- end date start -->
            <div class="form-group end-date {{ ($sDisplayEndDate !='') ? '' : 'hidden' }}">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <div class="datepicker {{ ($errors->has('end_date') || $errors->has('end_hour') || $errors->has('end_minute')) ? 'error' : ''}}">
                        <input id="add_end_date" type="text" title="{{($errors->has('end_date')) ? $errors->first('end_date') : '' }}" name="end_date" placeholder="{{trans('messages.end_date')}}" value="{{$sDisplayEndDate}}" data-toggle="tooltip" data-placement="top">
                        <select required class="time-hour" name="end_hour" title="{{($errors->has('end_hour')) ? $errors->first('end_hour') : '' }}" data-toggle="tooltip" data-placement="top" >
                            <option value='' >hh</option>
                            <option value='01' <?php echo $sEndHours==1?"selected":"" ?>>01</option>
                            <option value='02' <?php echo $sEndHours==2?"selected":"" ?>>02</option>
                            <option value='03' <?php echo $sEndHours==3?"selected":"" ?>>03</option>
                            <option value='04' <?php echo $sEndHours==4?"selected":"" ?>>04</option>
                            <option value='05' <?php echo $sEndHours==5?"selected":"" ?>>05</option>
                            <option value='06' <?php echo $sEndHours==6?"selected":"" ?>>06</option>
                            <option value='07' <?php echo $sEndHours==7?"selected":"" ?>>07</option>
                            <option value='08' <?php echo $sEndHours==8?"selected":"" ?>>08</option>
                            <option value='09' <?php echo $sEndHours==9?"selected":"" ?>>09</option>
                            <option value='10' <?php echo $sEndHours==10?"selected":"" ?>>10</option>
                            <option value='11' <?php echo $sEndHours==11?"selected":"" ?>>11</option>
                            <option value='12' <?php echo $sEndHours==12?"selected":"" ?>>12</option>
                        </select>
                        :
                        <select required class="time-minutes" name="end_minute" title="{{($errors->has('end_minute')) ? $errors->first('end_minute') : '' }}" data-toggle="tooltip" data-placement="top" >
                            <option value=''>mm</option>
                            <option value='00' <?php echo $sEndMinutes==00?"selected":"" ?>>00</option>
                            <option value='05' <?php echo $sEndMinutes==05?"selected":"" ?>>05</option>
                            <option value='10' <?php echo $sEndMinutes==10?"selected":"" ?>>10</option>
                            <option value='15' <?php echo $sEndMinutes==15?"selected":"" ?>>15</option>
                            <option value='20' <?php echo $sEndMinutes==20?"selected":"" ?>>20</option>
                            <option value='25' <?php echo $sEndMinutes==25?"selected":"" ?>>25</option>
                            <option value='30' <?php echo $sEndMinutes==30?"selected":"" ?>>30</option>
                            <option value='35' <?php echo $sEndMinutes==35?"selected":"" ?>>35</option>
                            <option value='40' <?php echo $sEndMinutes==40?"selected":"" ?>>40</option>
                            <option value='45' <?php echo $sEndMinutes==45?"selected":"" ?>>45</option>
                            <option value='50' <?php echo $sEndMinutes==50?"selected":"" ?>>50</option>
                            <option value='55' <?php echo $sEndMinutes==55?"selected":"" ?>>55</option>
                        </select>
                        <input type="radio" class="hidden" name="end_ampm" value="am" id="time_end_am" {{ ($sEndAMPM == 'AM') ? 'checked' : '' }} > 
                        <input type="radio" class="hidden" name="end_ampm" value="pm" id="time_end_pm" {{ ($sEndAMPM == 'PM') ? 'checked' : '' }} > 

                        <label for="time_end_am" class="time-end-am">{{ trans('messages.am') }}</label>
                        <label for="time_end_pm" class="time-end-pm">{{ trans('messages.pm') }}</label>
                        
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 close_end_date padding-10">
                     {{ trans('messages.close') }}
                </div>
            </div>
            <div class="form-group clearfix {{($errors->has('address')) ? 'has-error' : ''}}">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="locationField">
                        <input id="event_place" title="{{ ($errors->has('address')) ? $errors->first('address'): '' }}" name="address" class="form-control" placeholder="{{ trans('messages.add_address') }}"  onFocus="geolocate()" type="text" value="{{$sAddress}}" data-toggle="tooltip" data-placement="top" />
                    </div>
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="text"  title="{{($errors->has('room_number')) ? $errors->first('room_number') : '' }}" name="room_number" id="id_ev_room_number" class="form-control" placeholder="{{ trans('messages.add_detail_address') }}" value="{{$sRoomNumber}}" data-toggle="tooltip" data-placement="top" >
                </div>
                @if ($errors->has('room_number'))
                    <span class="error_message"><?php echo $errors->first('room_number'); ?></span>
                @endif
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 place-holder">
                    <select id="eventContact" class="js-data-example-ajax" data-placeholder="{{ trans('messages.invite_groups_or_users') }}" multiple="true" name="invite_member_list[]" >
                    </select>
                </div>
                <div id="selected_emails" class="auto-suggetion-outer"></div>
                <div id="auto-suggetion-div">
                    @if(old('invite_to'))
                        @foreach(old('invite_to') as $sInvity)
                            <div id='vR_{{$sInvity}}' class='vR pull-left m-r-1'>
                                <span email='{{$sInvity}}' class='vN'>
                                    <div class='vT invalid-email'>{{$sInvity}}</div>
                                    <div id='{{$sInvity}}' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>
                                </span>
                                <input type='hidden' value='{{$sInvity}}' name='invite_to[]'>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class=" clearfix">
                
                <input id="btn_add_event" type="button" class="btn btn-primary lg-center-button" value="{{ ($nIdEvent != '') ? trans('messages.update') : trans('messages.save')}}" data-loading-text="{{ trans('messages.creating_event') }}" onclick="submitAjaxForm('form_add_user_event', '{{ route('event.add-event') }}', this )">
            </div>
        </form>
    </div>
</div> <!-- End: Model Body -->


<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    
    
    $(document).ready(function(){
        
        $('[data-toggle="tooltip"]').tooltip();   
        $("#eventContact").select2({
            ajax: {
                type: 'POST',
                url: "/event/invite/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search_str: params.term, // search term
                        page: params.page,
                        action: "user_search"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    var processedResult = [];

                    var groups = $.map(data.data.group_list, function(item){
                        return { id: "GM_"+item.id_group, text: item.group_name, image :item.group_img  };
                    });

                    var contacts = $.map(data.data.user_connected_list, function(item){
                        return { id: "CU_"+item.id_user, text: item.first_name+' '+item.last_name, image :item.user_img  };
                    });
                    
                    var others = $.map(data.data.user_other_list, function(item){
                        return { id: "U_"+item.id_user, text: item.first_name+' '+item.last_name, image :item.user_img };
                    });
                    
                    if(groups && groups.length>0){
                        var group = { text: 'Group', children: groups,image :'' };
                        processedResult.push(group);
                    }

                    if(contacts && contacts.length> 0){
                        var contact = { text: 'Contacts', children: contacts ,image :''};
                        processedResult.push(contact);
                    }
                    
                    if(others && others.length> 0){
                        var other = { text: 'Others', children: others,image :'' };
                        processedResult.push(other);
                    }
                    return {
                        results: processedResult,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                error: function(data) {
                    if(data.status == 401)
                    {
                        window.location = siteUrl + '/home';
                    }
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        $('#eventContact').bind('keypress');
    });
    
    function formatRepo (repo) {
        $('#invitePlaceHolder').html('');
        if (repo.loading) return repo.text;

        var markup ="<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>";
                if(repo.image !='')
                           markup+= "<span class='icon-image img-25'>"+repo.image+"</span>";
                    
        markup+= "<span class='select2-result-repository__title'>" + htmlSpecialChars(repo.text) + "</span>" +
            "</div>"+
        "</div>";

        return markup;
    }

    function formatRepoSelection (repo)
    {
        return htmlSpecialChars(repo.text) || '';
    }
    
    $('.end_date').click(function(){
        $(this).addClass('hidden');
        $('.end-date').removeClass('hidden');   
        $('#add_end_date').val($('#add_start_date').val());
    });
    $('.close_end_date').click(function(){
        $('.end_date').removeClass('hidden');
        $('.end-date').addClass('hidden');
        $('#add_end_date').val('');
    });
    
    $("#add_start_date").datepicker({
        dateFormat: 'mm-dd-yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-10:c+3',
        //minDate: 0,
        showButtonPanel: false,
        onSelect: function (date) {

            var endDatePicker = $('#add_end_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
           // endDatePicker.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets endDatePicker maxDate to the last day of 30 days window
            endDatePicker.datepicker('option', 'maxDate', startDate);
            endDatePicker.datepicker('option', 'minDate', minDate);
            //$(this).datepicker('option', 'minDate', new Date());
        }
    });

    $( "#add_end_date" ).datepicker({
        dateFormat: 'mm-dd-yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-10:c+3',
        showButtonPanel: false,
        //minDate: 0,
    });
    
    var startDate = (<?php echo mb_strlen($sStartDate) ?> > 0) ? new Date('<?php echo $sDisplayStartDate; ?>') :'';
    var endDate = (<?php echo mb_strlen($sEndDate); ?> > 0) ? new Date('<?php echo $sDisplayEndDate; ?>') : '' ;
    
    $("#add_start_date").datepicker().datepicker("setDate", startDate);
    $("#add_end_date").datepicker().datepicker("setDate", endDate);
    
    //Following is code for add place with goelocation api from google.
    var placeSearch, autocomplete, autocomplete_edt;
    var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
                /** @type {!HTMLInputElement} */(document.getElementById('event_place')),
                {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
    }
    
    function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();
        $('#latitude').val(place.geometry.location.lat());
        $('#longitude').val(place.geometry.location.lng());
    }

    // Bias the autocomplete object to the user's geographical location,
    // as supplied by the browser's 'navigator.geolocation' object.
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
</script>

<?php
    //selected tab when in a group event created
    if( isset($oGroup->id_group) && is_numeric($oGroup->id_group) )  {
?>
<script type="text/javascript" >
    var groupId = "<?php echo $oGroup->id_group;?>";
    var groupname = "<?php echo $oGroup->group_name;?>";
    $(function() {
        $('#eventContact').append($("<option />").val('GM_'+groupId).text(groupname));
        $('#eventContact option[value="GM_'+groupId+'"]').prop('selected', true);
        $('ul.select2-selection__rendered').prepend('<li class="select2-selection__choice tag-event-group" title="' + groupname + '"><span role="presentation" class="select2-selection__choice__remove">x</span>' + groupname + '</li>')
    });

</script>
<?php
    }
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHGuJHdXq3i2kqtxyEySNtYFI6AaL2tro&libraries=places&callback=initAutocomplete" async defer></script>