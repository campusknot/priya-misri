@include('WebView::event._more_member_list')
@if(count($oEventMembers))
<script type="text/javascript">
nCurrentPageMember = <?php echo $oEventMembers->currentPage(); ?>;
nLastPageMember = <?php echo $oEventMembers->lastPage(); ?>;
$('.tab-content').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if(nCurrentPageMember < nLastPageMember && !nLoadNewDataStatus) {
                var event_status = $('#event_status').val();
                if(event_status == 'G')
                    var sDivId = 'event_going_users_tab';
                else
                    var sDivId = 'event_maybe_users_tab';
                
                nCurrentPageMember += 1;
                getEventMembers('{{$oEventMembers[0]->id_event}}', event_status, sDivId,nCurrentPageMember);
            }
        }
    });
</script>
@endif