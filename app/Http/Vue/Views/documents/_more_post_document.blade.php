@if(count($oGroupDocument))
    @foreach($oGroupDocument as $oDocument)

    <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id=''>
        <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name">
            <a target="_blank" href="{{route('utility.view-file',['p_'.$oDocument->id_posts,$oDocument->display_file_name]) }}" >                        
                <?php
                $aFileNameData = explode('.', $oDocument->file_name);

                //Div for file icon
                if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                {
                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                }
                elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                {
                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                }
                elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                {
                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                }
                elseif (in_array(end($aFileNameData), array('zip','rar')))
                {
                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                }
                elseif (end($aFileNameData) == 'pdf')
                {
                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                }
                echo $sHtmlString;
                ?>
                {{ $oDocument->display_file_name }}                       
            </a> 
        </li>
       <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
            <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{  $oDocument->last_name}}">
                {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
            </span>
            
        </li>
        <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            <span class="pull-right doc-date">
                <span class="doc-date">
                    {{ $oDocument->updated_at->format('m/d/Y') }}
                </span>
                <span class="folder-action pull-right">
                    <a target="_blank" href="{{route('utility.download-file',['p_'.$oDocument->id_post,$oDocument->display_file_name]) }}" > 
                        <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.delete')}}">
                    </a>
                </span>
            </span>
        </li>
                    <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>-->
    </ul>
    @endforeach
@endif
<script type="text/javascript">
    showTooltip();
</script>