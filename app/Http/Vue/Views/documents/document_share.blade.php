<div class="modal-content document-share-popup">
     <form id="invite_users_form" class="form-horizontal clearfix" method="post" onsubmit="submitAjaxForm('invite_users_form', '{{ route('documents.document-share') }}', $('#invited_member_button'));">
        {!! csrf_field() !!}
        <div class="modal-header">
            <button type="button" onclick="closeLightBox();" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 id="create_event" class="text-left">{{ trans('messages.share_folder') }} : {{ $oDocument->document_name }}</h4>
            @if($oDocument->id_user == Auth::user()->id_user)
             <div class="form-group">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 place-holder doc-permission-add-user">
                        <span class=" pull-left doc-perission-user-input">To:
                            <select id="Contact" class="js-data-example-ajax" data-placeholder="{{ trans('messages.share_doc_with_user') }}" multiple="multiple" name="share_member_list[]" required="required"></select>
                        </span>
                        <span class="pos-relative display-ib v-align pull-right">
                            <span class="caret"></span>
                            <select class="permission-listing" name="permission">
                                <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEREAD') }}">Read</option>
                                <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEWRITE') }}">Write</option>
                            </select>
                        </span>

                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        @if($errors->has('share_member_list'))
                            <span class="error_message">{{ $errors->first('share_member_list') }}</span>
                        @endif
                    </div>


                </div>
            @else
                <span>{{ trans('messages.not_allowed_document_share',['document_name' => $oDocument->document_name]) }}</span>
            @endif
            <input id="id_document" name="id_document" type="hidden" value="{{ $oDocument->id_document }}" />

        </div>
    </form>
    <!-- Start: Model Body -->
    <div class="modal-body">
       <div class="user-permission-area no-padding clearfix">
           <span class="user-permission-list-title">Who has access</span>
            <div class="doc-permission-area clearfix">
                @if(isset($oSharedGroupList) && count($oSharedGroupList))
                    @foreach($oSharedGroupList as $oSharedGroup)
                        <span class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix doc-permission">
                            <span class="docs-permission-grp-name">
                                <span class="img-25 display-ib v-align" data-toggle="tooltip" title="" data-original-title="{{ $oSharedGroup->group_name }}">
                                    {!! setGroupImage($oSharedGroup->group_image_name, $oSharedGroup->group_category_image) !!}
                                </span>
                                <span class="docs-share-grp-name">{{ $oSharedGroup->group_name }}</span>
                            </span>
                            <span class="pos-relative display-ib v-align pull-right">
                                @if($oDocument->id_user == Auth::user()->id_user)
                                 <span class="pull-right doc-permission-delete" onclick="deleteAccessRight(this,'{{ trans('messages.revoke_document_access')}}')">
                                    {{trans('messages.remove')}}
                                    <form id="G_{{ $oSharedGroup->id_group }}">
                                       <input type="hidden" name="id_document" value="{{ $oDocument->id_document }}">
                                       <input type="hidden" name="id_entity" value="{{ $oSharedGroup->id_group }}">
                                       <input type="hidden" name="entity_type" value="G">
                                   </form>
                                 </span>
                                
                                <span class="group-member-count" data-id="{{ $oSharedGroup->id_group_document }}">
                                    {{ $oSharedGroup->member_count }} {{ trans('messages.shared_group_member') }}
                                     <span class="caret"></span>
                                </span>
                                <span class="pos-relative v-align display-ib">
                                    <span class="caret"></span>
                                    <select class="permission-listing permission_change" data-id="{{ $oSharedGroup->id_group_document }}_G" >
                                        <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEREAD') }}" <?php if($oSharedGroup->group_document_permission== config('constants.DOCUMENTPERMISSIONTYPEREAD')) echo "selected='selected'"; ?> >{{ trans('messages.read_permission_text') }}</option>
                                        <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEWRITE') }}" <?php if($oSharedGroup->group_document_permission== config('constants.DOCUMENTPERMISSIONTYPEWRITE')) echo "selected='selected'"; ?>>{{ trans('messages.write_permission_text') }}</option>
                                    </select>
                                </span>
                                
                              
                                @endif
                            </span>

                            <div class="doc-permision-group-member clearfix" >
                            </div>

                        </span>
                    @endforeach
                @endif
                @if(isset($oSharedDocumentUserList) && count($oSharedDocumentUserList))
                    @foreach($oSharedDocumentUserList as $oUserList)
                    <span class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 doc-permision-single-member clearfix">
                        <span class="docs-permission-grp-name">
                            <span class="img-25 display-ib v-align"  title="">
                                {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
                            </span>{{ $oUserList->name }}
                        </span>
                        @if($oDocument->id_user == Auth::user()->id_user)
                        <span class="pull-right doc-permission-delete" onclick="deleteAccessRight(this,'{{ trans('messages.revoke_document_access')}}')">{{trans('messages.remove')}}
                            <form id="U_{{ $oUserList->id_user }}">
                                <input type="hidden" name="id_document" value="{{ $oDocument->id_document }}">
                                <input type="hidden" name="id_entity" value="{{ $oUserList->id_user }}">
                                <input type="hidden" name="entity_type" value="U">
                            </form>
                        </span>
                        <span class="pos-relative display-ib v-align pull-right">
                            <span class="caret"></span>
                            <select class="permission-listing permission_change" data-id="{{ $oUserList->id_document_permission }}">
                                <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEREAD') }}" <?php if($oUserList->permission_type== config('constants.DOCUMENTPERMISSIONTYPEREAD')) echo "selected='selected'"; ?> >
                                    {{ trans('messages.read_permission_text') }}
                                </option>
                                <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEWRITE') }}" <?php if($oUserList->permission_type== config('constants.DOCUMENTPERMISSIONTYPEWRITE')) echo "selected='selected'"; ?>>
                                    {{ trans('messages.write_permission_text') }}
                                </option>
                            </select>
                        </span>
                        @endif
                    </span>
                    @endforeach        
                @endif
            </div>
        </div>
    @if($oDocument->id_user == Auth::user()->id_user)
        <div class=" padding-10">
            <button id="invited_member_button" type="button" class="btn btn-primary popup-center-btn" onclick="submitAjaxForm('invite_users_form', '{{ route('documents.document-share') }}', this);">{{ trans('messages.share') }}</button>
        </div>
    @endif
    </div>  
</div>
<script type="text/javascript">
    
    /*show group member in click*/
    $('.group-member-count').click(function () {
        $(this).children("span").toggleClass("move-caret");
        $(this).parent("span").siblings(".doc-permision-group-member").toggleClass("showGroupMember");
        var element = $(this).parent("span").siblings('.doc-permision-group-member');
        var isData=element.html();
        if(isData.trim() == '')
        {
            var loader='<div class="spinner"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>';
            $(this).parent("span").siblings('.doc-permision-group-member').html(loader);
            $.ajax({
                url: siteUrl + '/documents/shared-group-member/'+$(this).attr('data-id'),
                processData: false,
                contentType: false,
                success: function(data) {
                    element.html(data.html);
                },
                error: function (data) {
                    if(data.status == 401)
                    {
                        window.location = siteUrl + '/home';
                    }
                    console.log('Error:', data);
                }
            });
        }
            
    });

    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    
    
    
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
        
        $("#Contact").select2({
            ajax: {
                type: 'POST',
                url: "/documents/share/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search_str: params.term, // search term
                        doc_id:$('#id_document').val(),
                        page: params.page,
                        action: "user_search"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    var processedResult = [];

                    var groups = $.map(data.data.group_list, function(item){
                        return { id: "GM_"+item.id_group, text: item.group_name, image :item.group_img };
                    });

                    var contacts = $.map(data.data.user_connected_list, function(item){
                        return { id: "U_"+item.id_user, text: item.name, image :item.user_img };
                    });                                       
                    
                    if(groups && groups.length>0){
                        var group = { text: 'Group', children: groups };
                        processedResult.push(group);
                    }

                    if(contacts && contacts.length> 0){
                        var contact = { text: 'Contacts', children: contacts };
                        processedResult.push(contact);
                    }

                    return {
                        results: processedResult,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        $('#Contact').bind('keypress');
    });
    
    function formatRepo (repo) {
        $('#invitePlaceHolder').html('');
        if (repo.loading) return repo.text;

        var markup ="<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>";
                if(repo.image != null)
                    markup += "<span class='icon-image img-25'>"+repo.image+"</span>\n";
                            
            markup += "<span class='select2-result-repository__title'>" + htmlSpecialChars(repo.text) + "</span>" +
                    "</div>"+
                "</div>";

        return markup;
    }

    function formatRepoSelection (repo)
    {
        return htmlSpecialChars(repo.text) || '';
    }
    
    $('.doc-permision-group-member').on('change','.permission_change',function(e)
    {
        permission_change(this);
    });
    $('.permission_change').on('change',function(e)
    {
        permission_change(this);
    });
    function permission_change(ele)
    {
        var permission = $(ele).val();
        var id_doc_per=$(ele).attr('data-id');
        
        $.ajax({
            type: "post",
            url: siteUrl + '/documents/change-permission',
            data: {id_doc_per:id_doc_per,permission:permission},
            success: function () {
            },
            error: function (data) {
                if(data.status == 401)
                {
                    window.location = siteUrl + '/home';
                }
                console.log('Error:', data);
            }
        });
    }
</script>