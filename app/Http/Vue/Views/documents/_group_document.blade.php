<div class="clearfix {{ (Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD')) ? 'drop_doc' : '' }}">    
    <ul class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix  docs-top-heading-bar ">
        <li><a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.group-document-ajax',$oGroup->id_group ) }}','{{ route('documents.group-document',$oGroup->id_group) }}');" >{{ trans('messages.group_documents',[ 'group'=> $oGroup->group_name ]) }}</a></li>
        @if(count($aBreadCrumb))
        <?php 
            //print_r($aBreadCrumb);
            $isPermission=0;
            ?>
            @foreach($aBreadCrumb as $breadcrumb)
                @if($isPermission == 1 || $breadcrumb->permission_type != '')
                    <?php 
                    $isPermission=1; 
                    ?>
                <li> <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.group-document-ajax',[$oGroup->id_group,$breadcrumb->id_document]) }}','{{ route('documents.group-document',[$oGroup->id_group,$breadcrumb->id_document]) }}');" >{{ $breadcrumb->document_name }}</a></li>
                @endif
            @endforeach

        @endif
    </ul>
    <form name="fileupload" id="fileupload" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="parent_id" id="parent_id" value="{{ $nIdParent }}" />
        <input type="hidden" name="shared_with" id="shared_with" value="{{ $sSharedWith }}" />
        <input type="hidden" name="current_permission" value="{{ (Session::has('current_permission')) ? Session::get('current_permission') : 'R'}}" />
        <input type="hidden" name="doc_type" id="doc_type" value="" />
        <input type="hidden" name="folder_name" id="fol_name" value="" />
        <input type="hidden" name="id_document" id="id_document" value="" />
        <input type="hidden" name="id_group" id="id_group" value="{{ $oGroup->id_group }}" />
        <input type="file" name="file" id="file_upload" class="hide" /> 
    </form>
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="group_search" name="group_search" onsubmit="searchDocs('{{ route('documents.group-document-ajax',['nIdGroup' => $oGroup->id_group,'nDocumentId' => $nIdParent]) }}','{{ route('documents.group-document',['nIdGroup' => $oGroup->id_group,'nDocumentId' => $nIdParent]) }}','search_member','document_list');return false">
            <div class="input-group docs-search">                
                <input id="search_member" type="text" class="form-control" name="search_member"  placeholder="{{ trans('messages.search_group_document') }}" value="{{ ($sSearchStr) ? $sSearchStr : '' }}" >
                <button type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>              
            </div>
        </form>
    </div>
    
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 document-list_my-docs">
        <ul class="my-docs_document-title clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                {{ trans('messages.name') }}
                <span class="column-sorting-options document_name" onclick="getDocsSort('document_name','{{ route('documents.group-document-ajax', ['nGroupId' => $oGroup->id_group,'nDocumentId' => $nIdParent]) }}','document_list');">
                    <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/asc.png')}}">
                    </span>
                    <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
                <span>
                    {{ trans('messages.doc-owner') }}
                    <span class="column-sorting-options first_name" onclick="getDocsSort('first_name','{{ route('documents.group-document-ajax', ['nGroupId' => $oGroup->id_group,'nDocumentId' => $nIdParent]) }}','document_list');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
                <span class="pull-right">
                    {{ trans('messages.date') }}
                    <span class="column-sorting-options updated_at" onclick="getDocsSort('updated_at','{{ route('documents.group-document-ajax', ['nGroupId' => $oGroup->id_group,'nDocumentId' => $nIdParent]) }}','document_list');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">{{ trans('messages.share_with') }}</li>
        </ul>
        <div class="document-list-parent clearfix" id="<?php ($nIdParent != '') ? $nIdParent : 'more_document'; ?>">
             <div class="file-upload-loader">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div class="file-upload-error">
                <div class="error-message"></div>
            </div>
            @if(!isset($nIdParent))
            <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id=''>
                <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name clearfix">
                    <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.group-post-document-ajax',[$oGroup->id_group]) }}','{{ route('documents.group-post-document',[$oGroup->id_group]) }}');">                        
                        <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ trans('messages.post-document-title') }}                     
                    </a>
                </li>
                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3"></li>
                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date"></li>
            </ul>
            @endif
            @if(count($oGroupDocument))
                @foreach($oGroupDocument as $oDocument)

                <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id='{{ $oDocument->id_document }}'>
                    <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name clearfix">
                        @if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
                            <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.group-document-ajax',[$oGroup->id_group,$oDocument->id_document]) }}','{{ route('documents.group-document',[$oGroup->id_group,$oDocument->id_document]) }}');">                        
                                <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ $oDocument->document_name }}                       
                            </a>
                        @else
                            <a target="_blank" href="{{route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" >                        
                                <?php
                                $aFileNameData = explode('.', $oDocument->file_name);

                                //Div for file icon
                                if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('zip','rar')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                }
                                elseif (end($aFileNameData) == 'pdf')
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                }
                                else
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/image.png').'">';
                                }
                                echo $sHtmlString;
                                ?>
                                {{ $oDocument->document_name }}                       
                            </a>                    
                        @endif
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
                        <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{  $oDocument->last_name}}">
                            {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
                        </span>
                        <span class="pull-right" >
                            <span class="doc-date">{{ $oDocument->updated_at->format('m/d/Y') }}</span>
                            <span class="folder-action pull-right">
                                @if($oDocument->document_type != config('constants.DOCUMENTTYPEFOLDER'))
                                    <a target="_blank" href="{{route('utility.download-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" > 
                                        <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.download')}}">
                                    </a>
                                @endif
                                @if($oDocument->id_user == Auth::user()->id_user)
                                    <img class="" src="{{asset('assets/web/img/doc-share-icon.png')}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                                    <img class="doc-rename" src="{{asset('assets/web/img/doc-rename-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.rename')}}"> 
                                    <img class="doc-copy" src="{{asset('assets/web/img/doc-copy-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.copy_tooltip')}}" onclick="copyDocument({{ $oDocument->id_document }});"> 
                                    @if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                                        <img class="doc-delete" src="{{asset('assets/web/img/doc-delete-icon.png')}}" onclick="deleteDocument({{ $oDocument->id_document }},'{{ trans('messages.file_delete_confirmation') }}');" data-toggle="tooltip" title="{{ trans('messages.delete')}}">
                                    @endif
                                    <!-- @if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                                    <img class="doc-delete" src="{{asset('assets/web/img/doc-delete-icon.png')}}" onclick="deleteDocument({{ $oDocument->id_document }},'{{ trans('messages.document_delete_confirmation') }}');" data-toggle="tooltip" title="{{ trans('messages.delete')}}">
                                    @endif -->
                                @endif
                            </span>
                        </span> 
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 shared-with-col">
                        @if($oDocument->count_user > 3 || $oDocument->count_group > 3)
                        <span class="img-25 pull-right more-shared-with-user" onclick="showDocsShareLightBox({{ $oDocument->id_document }});"> <!-- onclick="showDocsShareUserList(this,{{ $oDocument->id_document }});" -->
                            +
                        </span>
                        <ul class="more-shared-with-user-listing">
                        </ul>
                        @endif
                        <span class="pull-right doc-shared-with">
                        <?php $nSharedCount = 0; ?>
                        @if(count($oDocument->group_document))
                            @foreach($oDocument->group_document as $oGroupList)
                            @if($nSharedCount < 3)
                            <span class="img-25" data-toggle="tooltip" title="{{ $oGroupList->group_name }}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                                {!! setGroupImage($oGroupList->group_image_name, $oGroupList->group_category_image) !!}
                            </span>
                            @endif
                            <?php $nSharedCount++ ; ?>
                            @endforeach
                        @endif
                        @if(count($oDocument->userList))
                            @foreach($oDocument->userList as $oUserList)
                                <span class="img-25 multiple-user" data-toggle="tooltip" title="{{ $oUserList->first_name }} {{  $oUserList->last_name}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                                    {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
                                </span>
                            @endforeach
                        @endif
                         </span>
                        
                    </li>
                </ul>
                @endforeach
            @elseif(count($oGroupDocument)==0 && $nIdParent!= '')
                <div class="large-padding no-data">
                    <img class="" src="{{asset('assets/web/img/no-documents.png')}}">
                    {{ trans('messages.no_documents') }}
                </div>
            @endif
        </div>  
        <script type="text/javascript">
        $(document).ready(function(){
            //toolip show 
            showTooltip();
            var nCurrentPage = 0;
            nCurrentPage = <?php echo $oGroupDocument->currentPage(); ?>;
            var nLastPage = <?php echo $oGroupDocument->lastPage(); ?>;
            $(window).scroll(function (event) {
                if(window.location.href == '<?php echo route('documents.group-document',['nGroupId' => $oGroup->id_group,'nDocumentId' => $nIdParent]); ?>')
                {
                    if($(window).scrollTop() + $(window).height() == $(document).height()) {
                        if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                            nCurrentPage +=1;
                            loadNewData('<?php ($nIdParent != '') ? $nIdParent : 'more_document'; ?>', '<?php echo route('documents.group-document', ['nGroupId' => $oGroup->id_group,'nDocumentId' => $nIdParent]); ?>', nCurrentPage,'&order_by=<?php echo $sOrderBy; ?>&order_field=<?php echo $sOrderField; ?>&search_str='+$('#search_member').val());
                        }
                    }
                }
            });
            $('.drop_doc').dropzone({ 
                headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('documents.create-folder') }}" ,
                params : { parent_id : $('#parent_id').val(), doc_type : 'F', id_group : $('#id_group').val(),shared_with : $('#shared_with').val()},
                success: function(){
                    location.reload();
                }
            });
            /*$('#search_member').keyup(function(){
                var text = $('#search_member').val();
                if(text.length > 2)
                searchDocs('<?php echo route('documents.group-document-ajax',['nIdGroup' => $oGroup->id_group,'nDocumentId' => $nIdParent]); ?>','<?php echo route('documents.group-document',['nIdGroup' => $oGroup->id_group,'nDocumentId' => $nIdParent]); ?>','search_member','document_list')
            });*/
         });  
        </script>
    </div>
</div>