<span class="">
    <span class="add-new-folder">
        <img class="" src="{{asset('assets/web/img/new-folder.png')}}"> {{ trans('messages.create_folder') }}
    </span>
    <span class="add-new-file">
         <img class="" src="{{asset('assets/web/img/new-file.png')}}"> {{ trans('messages.upload_file') }}
    </span>
</span>
<script>
    /*open file broeser*/
    $('.add-new-file').click(function(){
        $('#file_upload').click(); 
        $('#doc_type').val('F');
    });
     /*add new folder take inputs*/
    var newFolder = '<ul class= "mk-new-folder my-docs_document-list dnd clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 ui-widget-content">'+
                        '<li class="col-lg-10 col-md-10 col-sm-10 col-xs-10"> <img class="" src="{{asset("assets/web/img/folder-icon.png")}}"> <input type="text" class="new-folder-name form-control" placeholder= "default" autofocus></li>'+
                        '<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">-- </li>'+
                        '<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>'+
                    '</ul>';
           
    $(".add-new-folder").click(function()
    {            
        $(".document-list-parent").prepend(newFolder);
        $('.new-folder-name').trigger('focus');
        $( ".new-folder-name" ).select();

        $( ".new-folder-name" ).click(function(){
            $(this).trigger('focus');
        });

        $( ".dnd" ).draggable({
            containment: '.document-list-parent',
            start: function( event, ui ) {
                  $(this).addClass('my-docs_document-list__dragging'); 
                },
            stop: function( event, ui ) {
              $(this).removeClass('my-docs_document-list__dragging'); 
            },
            cancel: "button", // these elements won't initiate dragging
            revert: "invalid", // when not dropped, the item will revert back to its initial position
            helper: "clone",
            cursor: "move"

         });

        $( ".dnd" ).droppable({
           activeClass: "activeDraggale",
           hoverClass:  "hoverDroppable",
            drop: function(event, ui) {
               ui.draggable.remove();
               alert("folder moved");
           }   
        });

    });
    function CreateFolder(ele,event,flag)
    {
        
        if(flag)
        {
            var input_folder_name = htmlSpecialChars($(ele).val());

            if(input_folder_name != '')
            {
        
                $(ele).closest('ul').removeClass('mk-new-folder');
                $('#doc_type').val('FO');
                $('#fol_name').val(input_folder_name);
                event.preventDefault();
                event.stopImmediatePropagation() ;
                var folder_img = '<img class="" src="{{asset("assets/web/img/folder-icon.png")}}"> ';
                var link_folder_name = '<a href="#">'+folder_img + input_folder_name +'</a> ';
                $(ele).parent().append(link_folder_name);
                $(ele).parent().addClass('document-name');
                $(ele).siblings('img').remove();
                $(ele).remove();
                var formData = new FormData($('#fileupload')[0]);
                createFolder("{{ route('documents.create-folder') }}", formData, input_folder_name);
            }
            else
            {
                if($('#id_document').val()=='')
                {
                    $(ele).closest('ul').remove();
                }
                else
                {
                    location.reload();
                }
            }
        }
        /*improvements*/
        else
        {
            $(ele).closest('ul').removeClass('mk-new-folder');
            var input_folder_name = $(ele).val();
            event.preventDefault();
            event.stopImmediatePropagation() ;
            var folder_img = '<img class="" src="{{asset("assets/web/img/folder-icon.png")}}"> ';
            var link_folder_name = '<a href="#">'+folder_img + input_folder_name +'</a> </span>';
            $(ele).parent().append(link_folder_name);
            $(ele).parent().addClass('document-name');
            $(ele).siblings('img').remove();
            $(ele).remove();
        }
    }
$(document).ready(function(){    
      flagDoc = 0;
    $(document).on('focusout','.new-folder-name',function(e){   
        if(e.type == "focusout"){
            //alert("in>> focusout");
            if(!flagDoc){
                CreateFolder(this,e,1);
                //alert("in>> focusout  >> created");
            }
            else{
                flagDoc = 0;
                //alert("in>> focusout  >> not creted");
            }
        }
    });
    $(document).on('keyup','.new-folder-name',function(e){
        //e.preventDefault();
        if (e.keyCode == 13) {
            flagDoc = 1;
           // alert("enter key pressed ");  
            CreateFolder(this,e,1);
              
            
        } 
     });


    $('#file_upload').change(function(){
        alert = function() {};
        var formData = new FormData($('#fileupload')[0]);
        createFolder("{{ route('documents.create-folder') }}", formData,'');
    });
});
</script>