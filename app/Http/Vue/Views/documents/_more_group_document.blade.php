@if(count($oGroupDocument))
    @foreach($oGroupDocument as $oDocument)

    <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 ui-widget-content" id='{{ $oDocument->id_document }}'>
        <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name clearfix">
            @if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
                <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.group-document-ajax',[$oGroup->id_group,$oDocument->id_document]) }}','{{ route('documents.group-document',[$oGroup->id_group,$oDocument->id_document]) }}');">                        
                    <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ $oDocument->document_name }}                       
                </a>
            @else
                <a target="_blank" href="{{route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" >                        
                    <?php
                            $aFileNameData = explode('.', $oDocument->file_name);

                            //Div for file icon
                            if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                            }
                            elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                            }
                            elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                            }
                            elseif (in_array(end($aFileNameData), array('zip','rar')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                            }
                            elseif (end($aFileNameData) == 'pdf')
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                            }
                            else
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/image.png').'">';
                            }
                            echo $sHtmlString;
                            ?>
                    {{ $oDocument->document_name }}                       
                </a>                    
            @endif
        </li>
        <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
            <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{  $oDocument->last_name}}">
                {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
            </span>
             <span class=" pull-right">
                 <span class="doc-date">{{ $oDocument->updated_at->format('m/d/Y') }}</span>
                 
                 <span class="folder-action">
                    @if($oDocument->document_type != config('constants.DOCUMENTTYPEFOLDER'))
                        <a target="_blank" href="{{route('utility.download-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" > 
                            <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.download')}}">
                        </a>
                    @endif

                    @if($oDocument->id_user == Auth::user()->id_user)
                        <img class="" src="{{asset('assets/web/img/doc-share-icon.png')}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                        <img class="doc-rename" src="{{asset('assets/web/img/doc-rename-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.rename')}}">
                        <img class="doc-copy" src="{{asset('assets/web/img/doc-copy-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.copy_tooltip')}}" onclick="copyDocument({{ $oDocument->id_document }});"> 
                        @if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                            <img class="doc-delete" src="{{asset('assets/web/img/doc-delete-icon.png')}}" onclick="deleteDocument({{ $oDocument->id_document }},'{{ trans('messages.file_delete_confirmation') }}');" data-toggle="tooltip" title="{{ trans('messages.delete')}}">
                        @endif
                    @endif
                </span>
             </span>
        </li>
        <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 shared-with-col">
            @if($oDocument->count_user > 3 || $oDocument->count_group > 3)
            <span class="img-25 pull-right more-shared-with-user" onclick="showDocsShareLightBox({{ $oDocument->id_document }});"> <!--onclick="showDocsShareUserList(this,{{ $oDocument->id_document }});" -->
                +
            </span>
            <ul class="more-shared-with-user-listing">
            </ul>
            @endif
            <span class="pull-right doc-shared-with">
            <?php $nSharedCount = 0; ?>
            @if(count($oDocument->group_document))
                @foreach($oDocument->group_document as $oGroupList)
                @if($nSharedCount < 3)
                <span class="img-25" data-toggle="tooltip" title="{{ $oGroupList->group_name }}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                    {!! setGroupImage($oGroupList->group_image_name, $oGroupList->group_category_image) !!}
                </span>
                @endif
                <?php $nSharedCount++ ; ?>
                @endforeach
            @endif
            @if(count($oDocument->userList))
                @foreach($oDocument->userList as $oUserList)
                    <span class="img-25 multiple-user" data-toggle="tooltip" title="{{ $oUserList->first_name }} {{  $oUserList->last_name}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                        {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
                    </span>
                @endforeach
            @endif
            </span>

        </li>
    </ul>
    @endforeach
@endif
<script type="text/javascript">
    showTooltip();
</script>