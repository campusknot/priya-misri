<?php
if(count($oGroupDocument))
{
    foreach($oGroupDocument as $oSingleTree)
    {
    ?>
        <li class="has-child">
            <span class="folder-state-indication group_child_doc" data-id="{{ $oSingleTree->id_document }}" data-tag='{{ $sDataFrom }}'>
                <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
            </span>
            <span class="folder-name {{($oSingleTree->permission_type == config('constants.DOCUMENTPERMISSIONTYPEWRITE')) ? 'active' : 'inactive'}}" data-id="{{ $oSingleTree->id_document }}">
                <span class="cpy-doc-folder-icon">
                    <img class="" src="{{($oSingleTree->permission_type == config('constants.DOCUMENTPERMISSIONTYPEWRITE')) ? asset('assets/web/img/folder-icon.png') : asset('assets/web/img/inactive-folder-icon.png')}}">
                </span>
                {{ $oSingleTree->document_name }}
            </span>
            <ul class="clearfix" id="child_{{ $sDataFrom }}_{{ $oSingleTree->id_document }}"> </ul>
        </li>
    <?php       
    }
}
else
{
    echo '<li class="hidden"></li>';
}
?>