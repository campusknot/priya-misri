@if(count($oSharedGroupList))
    @foreach($oSharedGroupList as $oGroupList)
    <li>
        <span class="img-25" data-toggle="tooltip" title="{{ $oGroupList->group_name }}">
            {!! setGroupImage($oGroupList->group_image_name, $oGroupList->group_category_image) !!}
        </span>
        <span class="user-name">{{ $oGroupList->group_name }}</span>
    </li>
    @endforeach
@endif
@foreach($oSharedUserList as $oUserList)
<li>
    <span class="img-25" data-toggle="tooltip" title="">
        {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
    </span>
    <span class="user-name">{{ $oUserList->first_name.' '.$oUserList->last_name }}</span>
</li>
@endforeach
