@extends('layouts.web.login_layout')

@section('title', 'Campusknot')

@section('content')

    <div class="container-fluid clearfix">
        <div class="row clearfix">
             @if (session('home_page_message'))
                <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
            @endif
            @if ($errors->has('faulty_records'))
                <div class="alert alert-error">
                    <strong class="error_message">{{ $errors->first('faulty_records') }}</strong>
                </div>
            @endif
            <div class="container about-content">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 terms">
                    <form id="add_course" name="add_course" action="{{route('add-user')}}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="name">{{ trans('messages.first_name') }}</label>
                            <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}">
                        </div>
                        @if($errors->has('first_name'))
                            <span class="error_message">{{ $errors->first('first_name') }}</span>
                        @endif
                        <div class="form-group">
                            <label for="name">{{ trans('messages.last_name') }}</label>
                            <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                        </div>
                        @if($errors->has('last_name'))
                            <span class="error_message">{{ $errors->first('last_name') }}</span>
                        @endif
                        <div class="form-group">
                            <label for="code">{{ trans('messages.email') }}</label>
                            <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                        @if($errors->has('email'))
                            <span class="error_message">{{ $errors->first('email') }}</span>
                        @endif
                        <div class="form-group">
                            <label for="university">{{ trans('messages.campus') }}</label>
                            <select name="id_campus" class="form-control">
                                <option value="">{{ trans('messages.select_campus') }}</option>
                                <?php 
                                    foreach ($aCampuses as $oCampus)
                                    {
                                        $sSelectedClass = "";
                                        if($oCampus->id_campus == old('id_campus'))
                                            $sSelectedClass="selected='selected'";
                                        echo "<option value='".$oCampus->id_campus."'".$sSelectedClass.">".$oCampus->campus_name."</option>";
                                    }
                                ?>
                            </select>
                        @if($errors->has('id_campus'))
                            <span class="error_message">{{ $errors->first('id_campus') }}</span>
                        @endif
                        </div>
                        
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                            <div class="radio radio-primary form-group">
                                <input type="radio" name="user_type" id="student" value="S" {{ (old('user_type') == 'S') ? "checked = 'checked'" : '' }}>
                                <label for="student">
                                    {{ trans('messages.signup_as_student') }}
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                            <div class="radio radio-primary form-group">
                                <input type="radio" name="user_type" id="faculty" value="F" {{ (old('user_type') == 'F') ? "checked = 'checked'" : '' }}>
                                <label for="faculty">
                                    {{ trans('messages.signup_as_faculty') }}
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="clearfix">
                               @if ($errors->has('user_type'))
                                <span class="error_message"><?php echo $errors->first('user_type'); ?></span>
                               @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="import_file">{{ trans('messages.import_via_csv') }}</label>
                            <input type="file" name="file" class="form-control" />
                        </div>
                        @if($errors->has('file'))
                            <span class="error_message">{{ $errors->first('file') }}</span>
                        @endif
                        <div class="form-group">
                            <input type="submit" name="submit" value="Submit"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop






