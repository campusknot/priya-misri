@extends('layouts.web.login_layout')

@section('title', 'Campusknot')

@section('content')

    <div class="container-fluid banner-img">
         <div class= "col-lg-12 col-sm-12 col-md-12 col-xs-12 main-form">
            @if (session('home_page_message'))
                <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
            @endif
        <div class= " verification-mail clearfix text-center">
            <h4>{{ trans('messages.resend_verification_mail')}}</h4>
            @if($error_msg != '')
                <span class="error_message">{{ $error_msg }}</span>
            @endif
           <form id="send_verification_mail" name="send_verification_mail" action="{{url('/home/send-verification-mail')}}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="form-group">
                    <input type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('messages.email') }}">
                </div>
                @if($errors->has('email'))
                    <span class="error_message">{{ $errors->first('email') }}</span>
                @endif

                <div class="form-group">
                    <input class="btn-primary padding-10 col-lg-2" type="submit" name="submit" value="Submit"/>
                </div>
            </form>
        </div>
    </div>
    </div>
@stop






