@extends('layouts.web.resposive_layout')

@section('title', 'Campusknot')

@section('content')
<div class="header-bg">
    
</div>

<div class="terms-padding clearfix">
    <div class="">
         @if (session('home_page_message'))
            <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
        @endif
        @if ($errors->has('home_page_error'))
            <div class="clearfix">
                <span class="error_message text-center col-sm-12">{{ $errors->first('home_page_error') }}</span>
            </div>
        @endif
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <h2><b>Terms of Service  - USER AGREEMENT</b></h2>
                <p>
                    These Terms of Use & Service (“Terms”) will govern and apply to the use of any and all of <a href="<?php echo url('/'); ?>" >www.Campusknot.com</a>, hereinafter referred to as “Website”, including all related services, products, applications and website access (collectively, “Services”), belonging to Campusknot Inc.
                </p>
                
                <p>
                    By using or accessing the Website and by clicking either “Join Campusknot” or “Sign In”, you are confirming that you have read and agree to fully comply with these Terms and also fully accept the terms, conditions, guidelines and requirements as stated in our Privacy Policy and Disclaimer. Access to the Website includes any information, graphics, documents, applications, advertisements and materials uploaded or downloaded by you, our partners, associates or anyone else so authorized. In the case that you disagree with these terms and conditions or any part thereof, you must not access or use this Website.
                </p>
                
                <p> 
                    You must be 18 years of age or older to use or access this Website. By using this website, you confirm your age to be 18 years or older.
                </p>
                
                    
                
                <h4>Scope </h4>
                <p>
                    The Website provides Services on this web platform for students and faculty members. This is an exclusive platform for university students and faculty members solely in the United States of America. The criterion for authentication is the use of an .edu email account.
                </p>
                
                <p>
                    Our Services may include advertisements by partners or through internet advertising partner companies, which may be targeted based on behavioral or usage information in order to display relevant content or information based on the analysis of this information by us or our partners.
                </p>
                
                <p>
                    We permit your use of the Services within the Terms and in compliance with any domestic, national or international laws that may be applicable to your use from your location, and with the acceptance of additional guidelines and requirements detailed in our Privacy Policy, Legal Disclaimer and other documents available on our website. This document may refer to your responsible obligation to comply with the collective requirements of all legal guidance posted on our website. 
                </p>
                
                <p>
                    Your acceptance for use of <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>Services is deemed as soon as you have signed up and are using the website. This acceptance includes the responsibility to accept actions done through your use of the Service and actions completed on behalf of any and all entities you may be representing using your registered account, and for any consequences thereof. If any of these entities are registered and have a separate account on the Website, it will not prejudice your conduct and use of the service, either directly or on their behalf.</p>
                <p>
                    You are responsible for all content, including but not limited to messages, posts, links, updates, files, videos, photos or any file formats, you post on <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> and must ensure its appropriateness and conform to guidelines contained in this and other documents relating to the Services. This includes the responsible use of the Services provided by <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>; it is deemed that you accept the Website Terms and all other Guidelines and Policies by signing up for and using the Services.This is a legally binding agreement under the laws of the United States of America and any other applicable jurisdiction. 
                </p>
                <h4>Subscription </h4>
                
                <ul>
                        <li>Web app is free for the selected Universities. </li>
                        <li>Campusknot charges convenience fee of $3.99(USD) for 6 month and $6.99(USD) for 1 year to use the mobile app(iOS and Android) with 1 month free trial.</li>
                        <li>Payment System used for Subscription plan: Apple pay for iOS app and Google wallet for android app </li>
                        <li>The Campusknot applications work best with a minimum system requirement of for android  Jelly Bean  4.1+ and  iOS 6.0+</li>
                        
                </ul>
                <h4>GENERAL TERMS</h4>
                <p>
                    A few specific terms of use are referred to, in particular, within these General Terms, as they are areas of immediate concern for both the user and platform provider. While we have proactively taken responsible actions to ensure a secure Service environment, it is also essential for the user to comply with the provisions of these Terms in order to augment the overall security posture of the Service as it also depends on the use or misuse of the privileges.
                </p>
                
                <h4>PASSWORDS</h4>
                <p>
                    The creation, recall and use of passwords are part of every internet user’s daily life. You are urged to create and use a “strong” password for your account; this is a necessary requirement for your use of the Website and Services. We may mention that, in the interest of ‘user ease-of-use’, the Website does not have a ‘forced’ password policy; however, we require that you create your password using a combination of letters (uppercase, lowercase combo), numerals and symbols, using no more than six characters. Please keep your password safe and secure as all activities carried out in your account and under your username is your responsibility and <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> is not liable for any damage, loss or claims arising out of such actions arising from your failure to comply with the Website Terms or other misuse of your account. 
                </p>
                
                <h4>PRIVACY</h4>
                <p>
                    During the course of your association with us, you will be providing and sharing information with us, personal and otherwise. Any information shared with us is subject to our Privacy Policy and the terms, guidelines, and requirements detailed therein will govern the collection, usage and disposal of this information. You understand that through your use of the services you express your acceptance of these Terms, including that any information you provide may be stored on systems that may be located in the United States of America or any other country where our hosting provider is located and that you accept and recognize that the Website has reasonable controls in place to ensure its security. 
                </p>
                <p>
                    By signing-up and accessing the Services provided, you confirm that you have read and accept all the terms of our <a href="{{ url('/privacy') }}" target="_blank">  Privacy Policy</a>.
                </p>
                
                <h4>SPAM</h4>
                <p>
                    The Website supports <b><u>‘ZERO TOLERANCE’</u></b> for spam and any user account found in violation will be suspended immediately. We do not spam any members nor do we share user information with marketing companies or spammers. 
                </p>
                
                <p>
                    <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> may send out mails to users as part of our operational requirements to make service and administrative announcements. We may also send out promotional communication to users for the purpose of advertising our services and/or that of our partners – these mailers are low volume and would be sent out within the parameters of responsible marketing and presentation norms.
                </p>
                
                <h4>Responsibility for Online Content </h4>
                <p>
                    All content, both publically posted and privately transmitted, is the sole responsibility of the person who originated the content in all respects, including but not limited to appropriateness, legitimacy and ownership. By using the Services, you agree to accept, unconditionally, complete liability for any action or consequences thereof arising from your use of the Services due to the nature of the content posted by you. In addition, you agree, unconditionally, to indemnify <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>  from any liability or cost arising due to any action or inaction by you, as indicated earlier. 
                </p>
                
                <p>
                    We, the management at Campusknot Inc. and <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> ,retain the right to remove any and all content reported to be offensive or in violation of the rights of any person, organization or entity. Such action will be taken by us or our agents without any prior notice to any party involved; furthermore, any account(s) found to be in violation will be suspended and the owners of such accounts will be banned from using our Services. Reinstatement of the content or account in question will occur only after the submission of adequate evidence and action from the responsible party confirming that there is no violation and that the charges are unsubstantiated. 
                </p>
                
                <p>
                    You may not post any content that is pornographic, offensive to any business, person or religion, plagiarized, copied, stolen or inappropriate to the nature and objectives of the Website Services. 
                    <br><br><b>The users of this Website:</b> <br>
                    <ul>
                        <li>must not republish any information from this Website to any other website,  </li>
                        <li>must not sell, rent or sub-license any material from this website, </li>
                        <li>must not publically show any information/content from this website, </li>
                        <li>must not reproduce, duplicate, copy or otherwise exploit information/content on this Website for any purpose, commercial or otherwise, </li>
                        <li>must not redistribute any information from this website, except when the user has expressly sought and was specifically granted permission. </li>
                    </ul>
                </p>
                
                <p>
                    As part of the platform promotion and advertising activities, <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> may copy, reproduce or display content(duly sanitized excerpts only) from active accounts and through the use of these Services, you agree to allow the unconditional and appropriate use of this content by the Website. You are free to delete your account at any time, which will result in the deletion of your content stored on our servers.
                </p>
                
                <p>
                    While we do not constantly monitor or control the content that you post online, we do conduct periodic audits and rely on user feedback to be informed about the same. Allowing the posting of your content on our website for the purposes of using our Services does not imply that we support, endorse, promote or guarantee any content or communication expressed via the Services. Our Services are intended to provide an educationally uplifting platform and we are not to be held responsible for the practices and promotions of the users using our Services. You are urged to perform due diligence and conduct necessary checks before engaging with any other organization through use of our Services.
                </p>
                <p>
                    "All posts on a group should follow the policy guidelines as explained above. The Group Admin has the right to delete any post if required without intimation to the creator of the post.The creator of the post may challenge this decision by contacting the Campusknot technical team."
                </p>
                <h4>Restrictions on Use of Services</h4>
                <p>
                    You may use the Services only if you are 18 years of age or older, as mentioned above. Minors are not eligible to use this Website or related Services and must not submit personal information to the Website for any reason. We are not responsible for and/or obligated by the information provided by minors who misrepresent themselves as being of age.
                </p>
                <p>
                    You are permitted to use the Services only if you accept the Terms of Use and Services, the Privacy Policy and additional legal guidance and terms that are available on the website <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> and those that additionally may be communicated from time to time. 
                </p>
                <p>
                    You may not access, download, use or modify the content, software, products or services provided on <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> in violation of the applicable laws of the land and any applicable regulations and practices of good use. Modification or any change is permitted only to the extent allowed by the Website application. 
                </p>
                <p>
                    Under no circumstances will you or any of your appointed representatives, contractors, associates or any third-party entities appointed by you or associated with you, attempt to conduct any network scans or perform a vulnerability assessment or penetration test of the application and/or its underlying infrastructure. 
                </p>
                <p>
                     We reserve the right to deny access to the Services, terminate any account(s) without assigning reasons, reset passwords, and disclose information (including content), personal or otherwise, in response to legal requests from any national or international law enforcement authorities or government, etc.
                </p>
                <h4>Restrictions on Use of Services</h4>
                <p>
                    The Website Services provide users with a platform to present their own profile and associated details; those who present such retain the ownership of any and all of their own representations. We strongly support intellectual property rights and believe that this extends to ideas and plans, in addition to content, statements, presentations and representations. 
                </p>
                <p>
                    As stated previously, any notice of infringement will be treated seriously by us and will result in a takedown of the user content without prior notice until the issue is resolved. However, we also believe in dismissing frivolous claims; therefore, if you believe your IPR has been infringed upon, you may report the allegation to us along with supporting documentation proving your ownership. Any such claims must also include your full name, university details, associated information, and contact information (phone, email, address).
                </p>
                <p>
                    Acceptance of such claims/ disputes and any resultant action will be at the sole discretion of <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> and Campusknot Inc. management, at their convenience, which is not to exceed 30 working days. 
                </p>
                <h4>Links, Logos and External Resources</h4>
                <p>
                    The <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>  logo may only be used online by affiliate websites and content providers solely for the purpose of providing a hyperlink to the <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> website. Any other use must be explicitly authorized by <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> management and inappropriate or unauthorized use will result in legal action. Under no circumstances can the colors or proportion of the logo be changed and any use of such variation will be considered to be unauthorized usage. 
                </p>
                <p>
                    The Services or the content posted online may contain links to third-party websites or resources. We are not responsible for the content, availability or accuracy of any third- party website links provided by us. Users are responsible for the links provided by them as part of their content.  Any risk arising from the action of following a link or any consequence thereof will be the responsibility of the user and in no way will <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>be liable for any damage resulting from these actions. 
                </p>
                <h4>Changes to the Terms of Use </h4>
                <p>
                    The Website management may review and update these Terms and other related guidance periodically as appropriate. Your continued use of the Services after such changes are made constitutes your consent to the revised Terms. If you do not wish to consent to the revised Terms, then you must immediately discontinue your use of the Services and notify us of your decision so that we can de-activate your user name and password. 
                </p>
                <h4>Applicable Laws and Jurisdiction </h4>
                <p>
                    All matters relating to your access to and use of <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> shall be governed by the laws of Mississippi State.
                </p>
                <p>
                    Any legal action or proceeding relating to your access to or use of <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>  shall be instituted in the courts of Mississippi State. You and <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a> agree to submit to the jurisdiction of Mississippi State without regard to or the application of the law and its provisions in your state or country of residence, agreeing that the venue is proper and that you waive any statement of inconvenience of the location.
                </p>
                <h4>As-is Services</h4>    
                <p>
                    EXCEPT WHERE EXPRESSLY PROVIDED OTHERWISE BY CAMPUSKNOT, THE WWW.CAMPUSKNOT.COM WEBSITE, SERVICES AND ALL RELATED CONTENT, MATERIALS, INFORMATION, SOFTWARE, AND PRODUCTS ARE PROVIDED ON AN "AS IS" AND "AS AVAILABLE" BASIS. 
                </p>
                <p>
                    <a href="<?php echo url('/'); ?>" rel="home" > WWW.CAMPUSKNOT.COM </a> EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT.
                </p>
                <p>
                    CAMPUSKNOT MAKES NO WARRANTY THAT THE SERVICES WILL MEET YOUR REQUIREMENTS OR THAT THEY WILL BE AVAILABLE ON AN UNINTERRUPTED, TIMELY, SECURE, OR ERROR-FREE BASIS OR THAT THE QUALITY OF ANY USER CONNECTIONS, PRODUCTS, SERVICES, INFORMATION, OR OTHER MATERIAL PURCHASED, EXCHANGED OR OBTAINED BY YOU THROUGH CAMPUSKNOT WILL MEET YOUR EXPECTATIONS.
                </p>
                <p>
                    ANY USER CONTACTS, CONTENT, MATERIALS OR INFORMATION ACCESSED OR OTHERWISE OBTAINED THROUGH THE USE OF CAMPUSKNOT SERVICES IS DONE SOLELY AT YOUR OWN DISCRETION AND RISK. WE MAKE NO WARRANTY OF AND DISCLAIM ANY AND ALL RESPONSIBILITY FOR THE COMPLETENESS, ACCURACY, AVAILABILITY, TIMELINESS, SECURITY OR RELIABILITY OF THE SERVICES OR ANY CONTENT, PROVIDED BY US, BY YOU OR BY ANY PARTY.
                </p>
                <p>
                    CAMPUSKNOT SHALL HAVE NO RESPONSIBILITY FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM(S), LOSS OF DATA, OR OTHER HARM THAT RESULTS FROM THE USE OF THE SERVICES OR CONTENT. YOU ALSO AGREE THAT CAMPUSKNOT HAS NO RESPONSIBLITY OR LIABILITY FOR THE DELETION OF, OR THE FAILURE TO STORE AND TRANSMIT ANY CONTENT AND COMMUNCATIONS RETAINED / MAINTAINED BY CAMPUSKNOT.
                </p>
                <p>
                    <a href="<?php echo url('/'); ?>" rel="home" > WWW.CAMPUSKNOT.COM </a> RESERVES THE RIGHT TO MAKE CHANGES OR UPDATES TO THE WEBSITE, SERVICES AND TO THE TERMS AND CONDITIONS GOVERNING THE USE OF THE SAME AT ANY TIME WITHOUT PRIOR NOTICE. YOU AGREE TO THE MODIFIED TERMS BY CONTINUING TO USE THE SERVICES AFTER THE MODIFICATIONS HAVE BEEN MADE. 
                </p>
                <h4>LIMITATION OF LIABILITY</h4>    
                <p>
                    TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, IN NO EVENT SHALL CAMPUSKNOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL, OR PUNITIVE DAMAGES, OR ANY LOSS OF PROFITS, REVENUE, DATA OR USE, INCURRED BY YOU OR ANY THIRD PARTY, WHETHER IN AN ACTION IN CONTRACT OR TORT, ARISING FROM YOUR ACCESS TO OR USE OF CAMPUSKNOT SERVICES. 
                </p>
                <p>
                    THIS INCLUDES INTANGIBLE LOSSES ARISING FROM USE, NON-AVAILABILITY OF SERVICES, LOSS OF CONTENT BY COPYING OR OTHER MEANS, UNAUTHORIZED ACCESS, USE OR ALTERATION TO YOUR ACCOUNT, OR ANY METHOD KNOWN OR UNKNOWN THAT IS NOT IDENTIFIED IN THIS DOCUMENT.
                </p>
                <p>
                    <b>SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY. ACCORDINGLY, SOME OF THE LIMITATIONS MAY NOT APPLY TO YOU.</b>
                </p>
                <h4>WAIVER AND SEVERABILITY</h4>    
                <p>
                    Under no circumstances will these Terms be deemed to be waived for any other rights or provisions. In the event that any provision of these Terms is considered invalid or unenforceable due to a conflict with applicable laws, then that provision will be limited or eliminated to the minimum extent necessary, and the other provisions will continue to be in effect.  
                </p>
                 <p>
                     <br>
                     This terms of Service was last updated as of Sep 29, 2017.
                </p>
            </div>

              
            <div class="col-lg-1 col-md-1 col-sm-1"></div>
    </div>
               
</div>
    
</div>
    
   
   
    

    
    
    

@stop






