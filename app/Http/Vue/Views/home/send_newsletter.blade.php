@extends('layouts.web.login_layout')

@section('title', 'Campusknot-Signup')

@section('content')

<div class="container-fluid banner-img">
    <div class="banner-overlay">
    </div>
    <div class= "col-lg-8 col-sm-8 col-md-9 col-xs-9 main-form">
        <div class= "col-lg-4 col-sm-4 col-md-4 col-xs-4 logo-section">
            <div class="ck-logo">
                <img src="{{asset('assets/web/img/main_header_logo.png')}}">
            </diV>
        </div>
        <div class= "col-lg-8 col-sm-8 col-md-8 col-xs-8 login-form-section">
            <form id="newsletter" name="newsletter" method="POST" action="{{ url('/newsletter') }}">
            {!! csrf_field() !!}
            
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="form-group">
                    <input type="text" id="campus_name" data-toggle="tooltip" data-placement="top" class="form-control" name="campus_name" placeholder="{{trans('messages.select_course')}}" value="{{ old('course_name') }}" required="required">
                     <input id="id_campus" name='id_campus' type="text" class="hidden" />
                </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="form-group">
                    <input type="text" id="subject" data-toggle="tooltip" data-placement="top" class="form-control" name="subject" placeholder="Newsletter Subject" value="{{ old('subject') }}" required="required">
                    @if ($errors->has('subject'))
                        <span class="error_message"><?php echo $errors->first('subject'); ?></span>
                    @endif
                </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                <div class="radio radio-primary form-group">
                    <input type="radio" name="user_type" id="student" value="S" {{ (old('user_type') == 'S') ? "checked = 'checked'" : '' }}>
                    <label for="student">
                        {{ trans('messages.signup_as_student') }}
                    </label>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                <div class="radio radio-primary form-group">
                    <input type="radio" name="user_type" id="faculty" value="F" {{ (old('user_type') == 'F') ? "checked = 'checked'" : '' }}>
                    <label for="faculty">
                        {{ trans('messages.signup_as_faculty') }}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="clearfix">
                   @if ($errors->has('user_type'))
                    <span class="error_message"><?php echo $errors->first('user_type'); ?></span>
                   @endif
                </div>
            </div>
            
            <div class="form-group text-center submit-options">
                <input id="signup-button" type="submit" value="Send Newsletter" class="login-btn">
            </div>
        </div>
        
    </div>
</div>

<script type="text/javascript">
$(function()
    {
        $('#campus_name').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('campus-suggestion') }}"+"?campus_name=" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.campus_name,
                            desc: value.id_campus
                        };

                    }));
                });
            },
            minLength: 2,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {

            },
            select: function(event, ui) {
                event.stopPropagation();
                $("input#campus_name").val(ui.item.label);
                $("#course-name").text(ui.item.label);
                $("input#id_campus").val(ui.item.desc);
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span>" + item.label + "</span>" )
                .appendTo( ul );
            };
    });
</script>
@stop