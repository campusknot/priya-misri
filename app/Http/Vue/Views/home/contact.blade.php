@extends('layouts.web.login_layout')

@section('title', 'Campusknot')

@section('content')
<div class="container-fluid banner-img">
    <div class= "col-lg-8 col-sm-8 col-md-9 col-xs-9 main-form">
        @if (session('home_page_message'))
            <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
        @endif
        
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 logo-section">
            <div class="ck-logo">
                <img src="{{asset('assets/web/img/contact-img.png')}}" class="contact-img">
                <h2>{{ trans('messages.get_in_touch') }}</h2>
            </diV>
        </div>
        <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 login-form-section">
          
        </div>
    </div>
</div>
<script type="text/javascript">
    var options = { 
        beforeSubmit:  showRequest,
        success:       showResponse,
        error:showError,
        dataType: 'json' 
        }; 
    $('.contact-btn').click(function(e){
        $(this).button('loading');
        $('#contact').ajaxForm(options).submit();  		
    });
    function showRequest(formData, jqForm, options) { 
            //$("#validation-errors").hide().empty();
            //$("#output").css('display','none');
            return true; 
    } 
    function showResponse(response, statusText, xhr, $form)  
    {
        $('.contact-btn').button('reset');
        $('#contact')[0].reset();
        $('#flash_msg').text(response.html);
        showTemporaryMessage();
    }
    function showError(xhr, textStatus, errorThrown)  {
        if(xhr.status == 500 ){
            //window.location = siteUrl + '/home';
            console.log('ewrror');
        }
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                    $( "[name='"+index+"']" ).addClass('error');
                    $( "[name='"+index+"']" ).attr('title',value);
                    //$("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
            }
        });
        $("#validation-errors").show();
        $('.contact-btn').button('reset');
    }
</script>
        
@stop