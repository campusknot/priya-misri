@extends('layouts.web.login_layout')

@section('title', 'Campusknot')

@section('content')

    <div class="container-fluid ">
        <div class="row">
             @if (session('home_page_message'))
                <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
            @endif
            
            <div class="container about-content">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 terms">
                    <form id="add_course" name="add_course" action="{{url('/home/add-course')}}" method="POST" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <input name="id_course" id="id_course" type="hidden" value="{{ isset($oCourseEdit->id_course) ? $oCourseEdit->id_course : '' }}" >
                        
                        <div class="form-group">
                            <label for="name">{{ trans('messages.course_name') }}</label>
                            <input type="text" class="form-control" name="course_name" value="{{ isset($oCourseEdit->course_name) ? $oCourseEdit->course_name : old('course_name') }}">
                        </div>
                        @if($errors->has('course_name'))
                            <span class="error_message">{{ $errors->first('course_name') }}</span>
                        @endif
                        <div class="form-group">
                            <label for="code">{{ trans('messages.course_code') }}</label>
                            <input type="text" class="form-control" name="course_code" value="{{ isset($oCourseEdit->course_code) ? $oCourseEdit->course_code : old('course_code') }}">
                        </div>
                        @if($errors->has('course_code'))
                            <span class="error_message">{{ $errors->first('course_code') }}</span>
                        @endif
                        <div class="form-group">
                            <label for="university">{{ trans('messages.university') }}</label>
                            <select name="id_university" class="form-control">
                                <option value="">{{ trans('messages.select_university') }}</option>
                                <?php 
                                foreach ($oUniversities as $oUniversity)
                                {
                                    $sSelectedClass = "";
                                    if((isset($oCourseEdit->id_university) && $oCourseEdit->id_university==$oUniversity->id_university)
                                            || ($oUniversity->id_university == old('id_university'))
                                        )
                                        $sSelectedClass="selected='selected'";
                                    
                                    echo "<option value='".$oUniversity->id_university."' ".$sSelectedClass.">".$oUniversity->university_name."</option>";

                                } ?>
                            </select>
                        </div>
                        @if($errors->has('id_university'))
                            <span class="error_message">{{ $errors->first('id_university') }}</span>
                        @endif
                        <div class="form-group">
                            <label for="import_file">{{ trans('messages.import_via_csv') }}</label>
                            <input type="file" name="file" class="form-control" />
                        </div>
                        @if($errors->has('file'))
                            <span class="error_message">{{ $errors->first('file') }}</span>
                        @endif
                        <div class="form-group">
                            <input type="submit" name="submit" value="Submit"/>
                        </div>
                    </form>
                    @if(count($oCourseList)>0)
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Sr No.</th>
                                <th>Course Name</th>
                                <th>Course Code</th>
                                <th>University Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                $nCount = (($oCourseList->currentPage() - 1) * $oCourseList->perPage()) + 1;
                                foreach($oCourseList as $oCourse)
                                {
                            ?>
                            <tr>
                                <td>{{$nCount.'.'}}</td>
                                <td>{{$oCourse->course_name}}</td>
                                <td>{{$oCourse->course_code}}</td>
                                <td>{{$oCourse->university_name}}</td>
                                <td>
                                    <a href="{{ route('home.edit-course', [$oCourse->id_course]) }}">Edit</a>
                                    <a href="{{ route('home.delete-course', ['nIdCourse', $oCourse->id_course]) }}">Delete</a>
                                </td>
                            </tr>
                            <?php
                                    $nCount ++;
                                }
                            ?>
                        </tbody>
                        <tfoot>{{ $oCourseList->links() }}</tfoot>
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop






