@extends('layouts.web.resposive_layout')

@section('title', 'Campusknot')

@section('content')


        <div class="container-fluid app-download">
            <div class="row">
                 @if (session('home_page_message'))
                    <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
                @endif
                @if ($errors->has('home_page_error'))
                    <div class="clearfix">
                        <span class="error_message text-center col-sm-12">{{ $errors->first('home_page_error') }}</span>
                    </div>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding banner-img mobile-app-banner">
                      <div class="banner-overlay"></div>
                </div>
               <div class="slide-content">
                <h1>
                    <div>{!! trans('messages.get_mobile_app')  !!}</div>
                    
                </h1>
                <a target="_blank" href="https://play.google.com/store/apps/details?id=com.campusknot.android&hl=en">
                    <img src="{{asset('assets/web/img/get-android-app.png')}}" class="app-icon">
                </a>
                <a target="_blank" href="http://itunes.com/apps/campusknot">
                    <img src="{{asset('assets/web/img/get-ios-app.png')}}" class="app-icon">
                </a>
                   <div class="download-now-text">{{ trans('messages.download_now') }}</div>  
          
            </div>
            
                
                
               
            </div>
        </div>
    
   
   
    

    
    
    

@stop






