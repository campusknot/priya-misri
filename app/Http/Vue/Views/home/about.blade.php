@extends('layouts.web.resposive_layout')

@section('title', 'Campusknot')

@section('content')


        <div class="container-fluid ">
            <div class="row">
                 @if (session('home_page_message'))
                    <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
                @endif
                @if ($errors->has('home_page_error'))
                    <div class="clearfix">
                        <span class="error_message text-center col-sm-12">{{ $errors->first('home_page_error') }}</span>
                    </div>
                @endif
                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding banner-img about-banner">
                      <div class="banner-overlay">
                       </div>
                    <div class="slide-content">
                        <h1>
                            <div class="header-bg-text">{!! trans('messages.about_text') !!}</div>
                        </h1>
                    </div>
                </div>
                <div class="about-content clearfix">
                    <div class="">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1"></div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-10 about-content-area clearfix">
                            
                            <p>Campusknot.com is a socially conscious business and an educational hub for students and professors. Above all, Campusknot.com encourages online learning and collaboration between faculty and classmates by using a variety of organizational tools. We are a technology company that provides a unique education and teaching experience and has created a free & innovative online academic management system for students & professors to better manage their class schedules. 
                            </p>
                            <p>We have become a resource that allows students and teachers to collaborate seamlessly through free discussion boards, upload assignments, post photos, and ask questions. 
                            <p>Interactive Planners for Students & Faculty: Notify your group members with events and reminders and schedule away assignments and other pivotal milestones. 
                            </p>
                            <p> Marketplace for College Textbooks: Get the most bang for your buck and search our online marketplace for college books. Search by subject, title, ISBN or by author’s name. 
                            </p>
                            <p>
                            Student & Faculty Collaboration: Follow Your Classmates and Professors, share what’s going on, update your activity feed and stay current within your intellectual communities! 
                            </p>
                            <p>Campusknot.com donates a percentage of its revenue to a non-profit organization known as the “Being Student Foundation.” This revenue is then used to fund scholarships, textbooks, laptops and other essential resources and materials. 
                            </p>       
                            <p class="text-center"> Get connected and join us at Campusknot.com today! 
                                <a href="{{ route('user.signup') }}" class="btn btn-primary">Join Now</a>
                            </p>
                            
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-1"></div>
                    </div>
                    
                </div>
               
            </div>
        </div>
    
   
   
    

    
    
    

@stop






