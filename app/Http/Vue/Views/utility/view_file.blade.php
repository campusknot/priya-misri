<html>
    <head>
        <title>{{ 'Campusknot - '.$sDisplayFileName }}</title>
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}">
        <script type="text/javascript" src="{{ asset('assets/web/js/jquery-1.12.2.min.js') }}"></script>
    </head>
    <body>
        <div class="container-fluid col-lg-12 col-md-12 col-sm-12 co-xs-12 file-preview no-padding">
            @if(empty($sDisplayUrl))
            
            <ul>
                
                <li>
                    <?php
                        $sCheckFileExt = explode('.', $sDisplayFileName);

                        //Div for file icon
                        if(in_array(end($sCheckFileExt), array('doc','docx','pages','rtf','txt','wp')))
                        {
                            $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                        }
                        elseif (in_array(end($sCheckFileExt), array('numbers','xls','xlsx')))
                        {
                            $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                        }
                        elseif (in_array(end($sCheckFileExt), array('key','ppt','pps','pptx')))
                        {
                            $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                        }
                        elseif (in_array(end($sCheckFileExt), array('zip','rar')))
                        {
                            $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                        }
                        elseif (end($sCheckFileExt) == 'pdf')
                        {
                            $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                        }
                        else
                        {
                            $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/image.png').'">';
                        }
                        echo $sHtmlString;
                    ?>
                </li>
                <li>
                    
                    <span>{{ $sDisplayFileName }}</span>
                    <a href="{{ route('utility.download-file',[$sIdDocument,$sDisplayFileName]) }}">{{ trans('messages.download') }}</a>
                    
                </li>
            </ul>
            <span class="file-preview-text">{{ trans('messages.file_preview_not_available') }}</span>
            @else
            <iframe src="{{$sDisplayUrl}}" frameborder='0' width="100%"></iframe>
            @endif
        </div>
        <footer>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('iframe').css('height', $(window).height());
                });
                
                $(window).resize(function() {
                    $('iframe').css('height', $(window).height());
                });
            </script>
        </footer>
    </body>
</html>