<span class="caret"></span>
<select id="campus" name="campus" class="form-control">
    <option value="" disabled selected>{{ trans('messages.select_campus') }}</option>
    @foreach($aCampuses as $aCampus)
    <option value="{{ $aCampus->id_campus }}">{{ $aCampus->campus_name }}</option>
    @endforeach
</select>
    
<script type="text/javascript">
    $("select").change(function() {
        $(this).css('color','#000');
        $(this).css('font-size','18px');
        
        $(this).css('font-weight','normal');
    });
</script>