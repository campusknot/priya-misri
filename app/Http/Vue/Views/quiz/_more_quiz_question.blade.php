<?php
    $nIndex = $aQuizQuestions->firstItem();
    $aSubmitQuetion = array();
?>
@foreach($aQuizQuestions as $nKey => $oQuizQuestion)
    <?php
        if($oQuizQuestion->user_answer != '' || $oQuizQuestion->user_answer_description != '')
            array_push($aSubmitQuetion,$nIndex);
    ?>
    <div class="quiz-single-question pos-relative disp-single-question clearfix" onclick="addActiveClass(this);">
        <span class="pull-left saved-answer-label">{{ trans('messages.saved') }}</span>
        <div class="clearfix text-center">
            <span class="disp-quiz-counter">{{ trans('messages.question') }} {{$nIndex}}</span>
            <span class="disp-quiz-points  pull-right">{{ trans('messages.points') }} : {{$oQuizQuestion->marks}} </span>
        </div>
        <div class="disp-quiz-question">
           {!! $oQuizQuestion->question_text !!}
           @if(!empty($oQuizQuestion->file_name))
           <span class="qs-image">
               <img class="" src="{!! setQuestionImage($oQuizQuestion->file_name,800); !!}" alt="" />
           </span>
           @endif
        </div>
        <div class="select-answer Col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <form id="q_{{$nIndex}}" name="submit_user_answer">
                {!! csrf_field() !!}
                <input type="hidden" name="id_quiz" value="{{$oQuizQuestion->id_quiz}}" />
                <input type="hidden" name="id_quiz_question" value="{{$oQuizQuestion->id_quiz_question}}" />
                @if($oQuizQuestion->question_type == config('constants.QUESTIONTYPEMULTIPLE'))
                    @foreach($oQuizQuestion->answer_options as $nOptionKey => $oAnswerOption)
                        <?php $sClass='';?>
                        @if($oAnswerOption->id_answer_option == $oQuizQuestion->user_answer)
                            <?php $sClass = 'checked'; ?>
                        @endif
                    <div class="">
                        <input id="btn{{$nIndex}}_{{$nOptionKey}}" type="radio" name="id_answer_option" value="{{$oAnswerOption->id_answer_option}}" {{ $sClass }} />
                        <label for="btn{{$nIndex}}_{{$nOptionKey}}" class="disp-quiz-answer clearfix" >{!! $oAnswerOption->answer_text !!}</label>
                    </div>
                    @endforeach
                @else
                <textarea type="text" class="form-control v-align animated" name="answer_text" placeholder="Answer">{{ $oQuizQuestion->user_answer_description}}</textarea>
                @endif
                <button class="btn btn-primary center-block" onclick="submitUserAnswer({{$nIndex}}, event);">
                    {{ trans('messages.save') }}
                </button>
            </form>
        </div>
    </div>       
<?php
    $nIndex++;
?>
@endforeach
<script type="text/javascript">
    $(document).ready(function(){
        setAutosizeTextAreaHeight(34);
        callAutoSizeTextArea();
        var aSubmitQuetion= <?php echo json_encode($aSubmitQuetion); ?>;
        for(var i=0;i<aSubmitQuetion.length;i++)
        {
            $('#index_'+aSubmitQuetion[i]).addClass('attempted');
        }
    });
</script>