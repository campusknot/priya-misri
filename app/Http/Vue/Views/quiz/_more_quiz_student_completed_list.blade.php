@foreach($oQuizList as $Quiz)
    <?php
    $dQuizStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $Quiz->start_time);
    $dQuizStartDate->setTimezone(Auth::user()->timezone);
    $dQuizEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $Quiz->end_time);
    $dQuizEndDate->setTimezone(Auth::user()->timezone);
    ?>
    <div class="panel clearfix cursor-pointer" >
        <span class="hidden">{{ $Quiz->marks_question }}</span>
        @if($Quiz->id_user_attempt != NULL )
            <?php $class = "openQuizSlider('". route('quiz.student-completed-quiz-detail',['nIdQuiz'=> $Quiz->id_quiz,'nIdUser' => $Quiz->id_user]) ."')"; ?>
        @else
            <?php $class=''; ?>
        @endif
        <div class="quiz pos-relative course"  onclick="{{ $class }}">
            <div class="clearfix quiz-card-title">
                <span class="">
                    {{ $Quiz->quiz_title }}
                </span>      
                <span class="pull-right quiz-card-right-section">
                    
                    <span class="quiz-card-points">  
                        @if($Quiz->id_user_attempt == NULL )
                        {{ trans('messages.not_attempted')}} : {{ trans('messages.marks',['mark'=> '0'])}}
                    @else
                         {{ ($Quiz->mark_obtain !='') ? $Quiz->mark_obtain : 0 }}
                    @endif / {{ $Quiz->total_marks }} pt</span>
                </span>
            </div>
            <div class="quiz-card-information">
                <div>
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/faculty-icon.png')}}"></span>
                    {!! $Quiz->first_name !!} {!! $Quiz->last_name !!}
                </div>
                <div>
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-group.png')}}"></span>
                    {{ $Quiz->group_name }}
                    @if($Quiz->section != '')
                            {{ trans('messages.section') }}- {{$Quiz->section}} ({{$Quiz->semester}})
                    @endif
                </div>
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-date.png')}}"></span>
                <span><span class="text-uppercase font-w-500">{{ trans('messages.start') }}</span> : {{ $dQuizStartDate->format('l')}} {{$dQuizStartDate->format('M j')}}, {{$dQuizStartDate->format('g:i A')}}</span> |  <span class="text-uppercase font-w-500">{{ trans('messages.end') }}</span> : {{ $dQuizEndDate->format('l')}} {{$dQuizEndDate->format('M j')}}, {{$dQuizEndDate->format('g:i A')}}
            </div>
                <div>
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/duration.png')}}">  </span> {{ trans('messages.quiz_duration')}} : {{ $Quiz->duration }} {{ trans('messages.mins') }}  
                </div>
<!--            <div>
                <span>
                    @if($Quiz->id_user_attempt == NULL )
                        {{ trans('messages.not_attempted')}} : {{ trans('messages.marks',['mark'=> '0'])}}
                    @else
                        {{ trans('messages.marks',['mark'=> $Quiz->mark_obtain])}}
                    @endif
                </span>
            </div>-->

            </div>
            @if($Quiz->marks_remaining != 0)
                <div class="show-quiz-notification">
                    <span class="marks-alert marks-add-alert">
                        <img class="alert-img v-align-top" src="{{asset('assets/web/img/yellow-alert.png')}}" alt="" />
                        <span class="alert-text">{{ trans('messages.marks_remaining_for_open_ended_question') }}</span>
                    </span>
                </div>
            @endif
        </div>
    </div>
@endforeach