<ul class=" quiz-user-list-heading clearfix">
    <li class="display-ib col-lg-5 col-md-5 col-sm-5 col-xs-5 ">
        {{ trans('messages.student_name') }}
    </li>
    <li class="display-ib col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
        {{ trans('messages.marks_obtained') }}
    </li>
    <li class="display-ib col-lg-4 col-md-4 col-sm-4 col-xs-4  text-center">
        {{ trans('messages.status') }}
    </li>
</ul>
<div id="more_user_list_{{ $nIdQuiz }}" data-id="{{ $nIdQuiz }}">
    @include('WebView::quiz._more_quiz_user_list')
</div>

<script type="text/javascript">
var nCurrentPage = <?php echo $oUserList->currentPage(); ?>;
var nLastPage = <?php echo $oUserList->lastPage(); ?>;
$('.user_list').scroll(function(){
    if($(this).scrollTop() + $(this).innerHeight()+50 >= $(this)[0].scrollHeight ) {
        if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
            var nIdQuiz = $(this).attr('data-id');
            nCurrentPage +=1;
            getUserInvitedList(nIdQuiz,'more_user_list',nCurrentPage);
        }
    }
});
</script>