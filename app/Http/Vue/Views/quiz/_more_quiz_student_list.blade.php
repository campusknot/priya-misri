@foreach($oQuizList as $oUpcomingQuiz)
    <?php
    $dTime = Carbon::Now(Auth::user()->timezone);
    $dQuizStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $oUpcomingQuiz->start_time);
    $dQuizStartDate->setTimezone(Auth::user()->timezone);
    $dQuizEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $oUpcomingQuiz->end_time);
    $dQuizEndDate->setTimezone(Auth::user()->timezone);
    ?>
    <div class="panel clearfix cursor-pointer" >
        <a target="_blank" class="course accordion-toggle pos-relative" href="{{ route('quiz.quiz-student-detail',['nIdUserInvite'=>$oUpcomingQuiz->id_quiz_user_invite]) }}">
            <div class="clearfix quiz-card-title">
                <span class="">
                    {!! $oUpcomingQuiz->quiz_title !!}
                    @if($dTime > $dQuizStartDate)
                        <span class="quiz-started-alert">{{ trans('messages.started') }}</span>
                    @endif
                </span>      
                <span class="pull-right quiz-card-right-section">
                      <span class="quiz-card-points">{{ $oUpcomingQuiz->total_marks }} pt</span>
                </span>
            </div>
            <div class="quiz-card-information">
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/faculty-icon.png')}}"></span>
                {!! $oUpcomingQuiz->first_name !!} {!! $oUpcomingQuiz->last_name !!}
            </div>
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-group.png')}}"></span>
                {!! $oUpcomingQuiz->group_name !!}
                @if($oUpcomingQuiz->section != '')
                        {{ trans('messages.section') }}- {{$oUpcomingQuiz->section}} ({{$oUpcomingQuiz->semester}})
                @endif
            </div>
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-date.png')}}"></span>
                <span class="text-uppercase font-w-500">{{ trans('messages.start') }}</span> : {{$dQuizStartDate->format('M j')}}, {{$dQuizStartDate->format('g:i A')}} |  <span class="text-uppercase font-w-500">{{ trans('messages.end') }}</span> : {{$dQuizEndDate->format('M j')}}, {{$dQuizEndDate->format('g:i A')}}
                
            </div>
            <div>
                <span class="quiz-card-icon">
                    <img class="" src="{{asset('assets/web/img/duration.png')}}">
                </span> {{ trans('messages.quiz_duration') }} : {{ $oUpcomingQuiz->duration }} {{ trans('messages.mins') }} 
                <span class="pull-right open-quiz-lable">{{ trans('messages.open_quiz') }}</span>
            </div>

            </div>
        </a>
    </div>
@endforeach