<ul class="nav nav-tabs ck-tabs  sub-tabs text-center">
    <li class="{{ ($sQuizListingType != 'completed') ? 'active' : ''}} pos-relative" onclick="changeQuizList('quiz_card', '{{ route('quiz.quiz-student-list-ajax',['nIdGroup' => $nIdGroup]) }}', '{{ route('quiz.quiz-student-list',['nIdGroup' => $nIdGroup]) }}','');">
        @if($nUpcomingQuizCount > 0)<span class="upcoming-quiz-notification">{{$nUpcomingQuizCount}}</span>@endif
        <a data-toggle="tab" href="" >{{ trans('messages.upcoming') }}</a>
    </li>
    <li class="{{ ($sQuizListingType == 'completed') ? 'active' : ''}}" onclick="changeQuizList('quiz_card', '{{ route('quiz.quiz-student-completed-list-ajax',['nIdGroup' => $nIdGroup]) }}', '{{ route('quiz.quiz-student-completed-list',['nIdGroup' => $nIdGroup]) }}','') ">
        <a data-toggle="tab" href="" >{{ trans('messages.completed') }}</a>
    </li>
</ul>
<div class="quiz-card" id="quiz_cards">
    @if(count($oQuizList))
        @include('WebView::quiz._more_quiz_student_list')
    @else
        {{ trans('messages.no_upcoming_quiz') }}
    @endif
</div>
<script type="text/javascript">
    var nCurrentPage = <?php echo $oQuizList->currentPage(); ?>;
    var nLastPage = <?php echo $oQuizList->lastPage(); ?>;

    $(document).scroll(function (event) {
        if(window.location.href == '<?php echo route('quiz.quiz-student-list', ['nIdGroup' => $nIdGroup ]); ?>')
        {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;                    
                    loadNewData('quiz_cards', '<?php echo route('quiz.quiz-student-list', ['nIdGroup' => $nIdGroup ]); ?>',nCurrentPage);
                }
            }
        }
    });
</script>