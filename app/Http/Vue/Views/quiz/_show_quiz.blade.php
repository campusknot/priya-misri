<?php
    $nIdQuiz = isset($oQuiz->id_quiz) ? $oQuiz->id_quiz : old('id_quiz');
    $sQuizTitle = isset($oQuiz->quiz_title) ? $oQuiz->quiz_title : old('quiz_title');
    $sQuizDescription = isset($oQuiz->description) ? $oQuiz->description : old('description');
    $nQuizTotalMark = isset($oQuiz->total_marks) ? $oQuiz->total_marks : old('total_marks');
    $nIdGroup = isset($oQuiz->id_group) ? $oQuiz->id_group : old('group_name');
    $nQuizDuration = isset($oQuiz->duration) ? $oQuiz->duration : old('duration');
    $sStartDate = isset($oQuiz->start_time) ? $oQuiz->start_time : (mb_strlen(old('start_date')) ? old('start_date') : '');
    $sEndDate = isset($oQuiz->end_time) ? $oQuiz->end_time : (mb_strlen(old('end_date')) ? old('end_date') : '');
    $sShowMobile = isset($oQuiz->show_in_mobile) ? $oQuiz->show_in_mobile : (mb_strlen(old('show_mobile')) ? old('show_mobile') : '');
    $sDisplayStartDate = '';
    $sStartHours = (mb_strlen(old('start_hour')) ? old('start_hour') : '');
    $sStartMinutes = (mb_strlen(old('start_minute')) ? old('start_minute') : '');
    $sStartAMPM = 'AM';
    $sEndAMPM = 'AM';
    $nEditable = ((!empty($nIdQuiz) && isset($oQuiz->start_time) && $oQuiz->start_time > Carbon::Now()) || empty($nIdQuiz)) ? 1 : 0;

    if(mb_strlen($sStartDate))
    {
        $dStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $sStartDate);
        $dStartDate->setTimezone(Auth::user()->timezone);
        $sDisplayStartDate = $dStartDate->format('m-d-Y');
        $sStartTime = $dStartDate->toTimeString();
        $aStartTime = explode(":", $sStartTime);
        $sStartHours = $aStartTime[0];
        if($sStartHours > 12)
        {
            $sStartHours = $sStartHours - 12;
            $sStartAMPM = 'PM';
        }
        if($sStartHours == 12)
            $sStartAMPM = 'PM';
        elseif($sStartHours == 00)
            $sStartHours=12;
        $sStartMinutes = $aStartTime[1];
    }


    $sDisplayEndDate = '';
    $sEndHours = (mb_strlen(old('end_hour')) ? old('end_hour') : '');
    $sEndMinutes = (mb_strlen(old('end_minute')) ? old('end_minute') : '');

    if(mb_strlen($sEndDate))
    {
        $dEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $sEndDate);
        $dEndDate->setTimezone(Auth::user()->timezone);
        $sDisplayEndDate = $dEndDate->format('m-d-Y');
        $sEndTime = $dEndDate->toTimeString();
        $aEndTime = explode(":", $sEndTime);
        $sEndHours = $aEndTime[0];
        if($sEndHours > 12)
        {
            $sEndHours = $sEndHours - 12;
            $sEndAMPM = 'PM';
        }
        if($sEndHours == 12)
            $sEndAMPM = 'PM';
        elseif($sEndHours == 00)
            $sEndHours=12;
            
        $sEndMinutes = $aEndTime[1];
    }

?>
<span class="pull-left saved-answer-label">{{ trans('messages.saved') }}</span>
<div class="add-quiz-info">  
    <form id="form_add_quiz" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="form-group">
            <input type="text" class="form-control" placeholder="{{ trans('messages.quiz_name') }}"  value="{{$sQuizTitle}}" disabled="disabled"/>
        </div>
        <div class="form-group">
            <textarea class="form-control" placeholder="{{ trans('messages.quiz_information') }}" disabled="disabled" >{{ $sQuizDescription }}</textarea>
        </div>
        <div class="form-group clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding quiz-date">
                <div class="datepicker  disabled">
                <input type="text" id="add_start_date" placeholder="{{trans('messages.start_date_quiz')}}" value="{{$sDisplayStartDate}}" disabled="disabled">
                    <select class="time-hour" disabled="disabled">
                       <option value=''>hh</option>
                        @for($nStartHour = 1 ; $nStartHour <=12 ; $nStartHour++)
                        <option value='{{ $nStartHour }}' {{ $nStartHour == $sStartHours  ? "selected":"" }}>{{ $nStartHour }}</option>
                        @endfor
                    </select>
                    :
                    <select class="time-minutes" disabled="disabled">
                        <option>{{ $sStartMinutes }}</option>
                    </select>
                
                    <input id="time_start_am" type="radio" class="hidden" disabled="disabled" value="am" {{ ($sStartAMPM == 'AM') ? 'checked' : '' }} >
                    <input id="time_start_pm" type="radio" class="hidden" disabled="disabled" value="pm" {{ ($sStartAMPM == 'PM') ? 'checked' : '' }} >
                    <label for="time_start_am" class="time-start-am">{{ trans('messages.am') }}</label>
                    <label for="time_start_pm" class="time-start-pm">{{ trans('messages.pm') }}</label>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding quiz-date pull-right" >
                <div class="datepicker pos-relative disabled">
                    <input id="add_end_date" type="text" disabled="disabled" placeholder="{{trans('messages.end_date_quiz')}}" value="{{ $sDisplayEndDate }}">
                    <select required class="time-hour" disabled="disabled" >
                        <option value=''>hh</option>
                        @for($nEndHour = 1 ; $nEndHour <=12 ; $nEndHour++)
                        <option value='{{ $nEndHour }}' {{ $nEndHour == $sEndHours  ? "selected":"" }}>{{ $nEndHour }}</option>
                        @endfor
                    </select>
                    :
                    <select required class="time-minutes " disabled="disabled">
                        <option value=''>{{ $sEndMinutes }}</option>
                    </select>
                    <input type="radio" class="hidden" value="am" id="time_end_am" {{ ($sEndAMPM == 'AM') ? 'checked' : '' }} disabled="disabled"> 
                    <input type="radio" class="hidden" value="pm" id="time_end_pm" {{ ($sEndAMPM == 'PM') ? 'checked' : '' }} disabled="disabled"> 

                    <label for="time_end_am" class="time-end-am">{{ trans('messages.am') }}</label>
                    <label for="time_end_pm" class="time-end-pm">{{ trans('messages.pm') }}</label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <span class="quiz-date label-input-parent pos-relative disabled">
                <label>{{ trans('messages.quiz_duration') }}</label>
                <input type="text" class=" form-control " placeholder="{{ trans('messages.mins') }}" disabled="disabled" value="{{$nQuizDuration}}" />
                <span class="quiz-mins-label">{{ trans('messages.mins') }}</span>
            </span>
            <span class="quiz-date pull-right label-input-parent ">
                <label>{{ trans('messages.quiz_points') }}</label>
                <input type="text" class=" form-control" name="marks" id="marks" value="{{$nQuizTotalMark}}" />
            </span>


        </div> 
        <div class="form-group pos-relative">
            <span class=" caret caret-right"></span>
            <select class="form-control" disabled="disabled">
                <option value="">{{ trans('messages.select_group') }}</option>
                @foreach($oGroupList as $oGroup)
                    <?php ($oGroup->id_group == $nIdGroup) ? $selected = "selected = 'selected'" : $selected = ''; ?>
                    <option value="{{ $oGroup->id_group }}" {{ $selected }}>{{ $oGroup->group_name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <input type="checkbox" class="mobile-quiz-chk" disabled="disabled" name="show_mobile" value="1" {{ ($sShowMobile && $sShowMobile == 1) ? 'checked' : ''}} />{{ trans('messages.show_in_mobile') }}
        </div>
            <input type="hidden" name="id_quiz" id="id_quiz_add" value="{{$nIdQuiz}}"/>    

        <div class="">
            <input type="button" class="btn btn-primary lg-center-button btn-disabled" onclick="submitFormQuiz('form_add_quiz', '{{ route('quiz.quiz-edit-point') }}', this ,'editPoint')" value="Save" />
        </div>

    </form>
    <script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();   
       
     $("#add_start_date").datepicker({
        dateFormat: 'mm-dd-yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-10:c+3',
        minDate: 0,
        showButtonPanel: false,
        onSelect: function (date) {

            var endDatePicker = $('#add_end_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
           // endDatePicker.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets endDatePicker maxDate to the last day of 30 days window
            endDatePicker.datepicker('option', 'maxDate', startDate);
            endDatePicker.datepicker('option', 'minDate', minDate);
            $(this).datepicker('option', 'minDate', new Date());
        }
    });

    $( "#add_end_date" ).datepicker({
        dateFormat: 'mm-dd-yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-10:c+3',
        showButtonPanel: false,
        minDate: 0,
    });
    $('#marks').on('keydown',function(){
        $('#form_add_quiz').find('.btn-disabled').removeClass('btn-disabled')
    });
    </script>
</div>