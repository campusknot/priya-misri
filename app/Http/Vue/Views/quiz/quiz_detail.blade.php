<?php $nQuizTotalMark = isset($oQuiz->total_marks) ? $oQuiz->total_marks : old('total_marks');
$nEditable = ((!empty($nIdQuiz) && ($oQuiz->start_time > Carbon::Now() || $oQuiz->activated == 0) ) || empty($nIdQuiz)) ? 1 : 0;
?>
<div class="modal-content add-quiz pos-relative">
        <span class="show-total-points {{ isset($nQuizQuestionTotal) ? '':'hidden' }}" >
            {{ trans('messages.allot_marks') }} : <span class="question_total_mark"> {{ isset($nQuizQuestionTotal) ? $nQuizQuestionTotal :'' }} </span> / <span class="quiz_total_mark"> {{ $nQuizTotalMark }} </span>
        </span>
    <div class="modal-header">
         <span class="show-total-points-header {{ isset($nQuizQuestionTotal) ? '':'hidden' }} header-attached-points" >
            {{ trans('messages.allot_marks') }} : <span class="question_total_mark"> {{ isset($nQuizQuestionTotal) ? $nQuizQuestionTotal :'' }} </span> / <span class="quiz_total_mark"> {{ $nQuizTotalMark }} </span>
        </span>
            <div class="show-quiz-popup-header">
                <button type="button" onClick="window.location.reload();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ trans('messages.add_quiz') }}
            </div>
            <div class="show-quiz-slider-header">
                <button type="button" onclick="hideSlider();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ trans('messages.quiz_detail') }}
            </div>
        </div>
    <div class="modal-body">
        <div class="clearfix quiz-container">
            @if($nEditable == 1)
               @include('WebView::quiz._add_quiz')
            @else
                @include('WebView::quiz._show_quiz')
            @endif
            <div class="quiz-questions clearfix" id="quiz_questions">
                @if(isset($oQuestions) && count($oQuestions))
                    @include('WebView::quiz._show_questions')
                @endif
            </div>
            @if($nEditable == 1 )
            
            <div class="clearfix text-center">
                <span class="new-question {{ (!empty($nIdQuiz) ? '':'hidden') }}" >
                    {{ trans('messages.add_question') }}  
                </span>
                <span class="or-text {{ (!empty($nIdQuiz) ? '':'hidden') }}">
                     
                </span>
                  
                <span class="close-popup {{ (!empty($nIdQuiz) ? '':'hidden') }}" onClick="window.location.reload();">
                    {{ trans('messages.or') }} {{ trans('messages.close') }} 
                </span>
<!--                  <span class="close-slider" onclick="hideSlider();">
                      {{ trans('messages.close') }} 
                  </span>-->
            </div>
            @endif
        </div>
        
    </div>
</div>
<script type="text/javascript">
    var questionCount = 0;
    <?php
    if(isset($oQuestions) && count($oQuestions)) { ?>
        //more que is auto load bcz add que problem with new que num.
        var nCurrentPage = <?php echo $oQuestions->currentPage(); ?>;
        var nLastPage = <?php echo $oQuestions->lastPage(); ?>;
        $(document).ready(function(){
            (function loop() {          
                setTimeout(function () {   
                    nCurrentPage++;
                    console.log(nCurrentPage);
                   loadNewData('quiz_questions', '<?php echo route('quiz.quiz-add',['nIdQuiz'=> $nIdQuiz] ); ?>', nCurrentPage);
                   if (nCurrentPage <= nLastPage) loop(); // iteration counter
                }, 500);
            })();
        });
    <?php } ?>
    $(document).ready(function(){
        
        // Add smooth scrolling
        $(".new-question").on('click', function(event) {
            var scrollDownTo =$('.quiz-container').height();
            $('.modal-body').animate({
                scrollTop: scrollDownTo
                }, 800, function(){
            });
        });
        
    });

$('.end_date').click(function(){
        $(this).addClass('hidden');
        $('.end-date').removeClass('hidden');   
        $('#add_end_date').val($('#add_start_date').val());
        
    });

   
    
    $('.new-question').on('click',function(){
        questionCount = $('.quiz-single-question').length;
        if(questionCount > 0 && $( "#form_add_question_"+questionCount ).length)
        {
            submitFormQuiz('form_add_question_'+questionCount, '{{ route('quiz.quiz-question-add') }}', this ,'question');
        }
        else
        {
            questionCount++;
            addQuizQuestion(questionCount,'');
        }
        
    });
     
    function addOption(ele,nidQue)
    {
        var radio = $(ele).parent().siblings(".answer_type_M").children().length+1;
        var html= "<div class='single-mcq-answer form-group added-option pos-relative clearfix'>"+
                    "<textarea type='text' class='form-control q-question display-ib v-align  animated' placeholder='option' name='answer_option["+radio+"]'></textarea>"+
                    "<input type='hidden' name='id_answer_option["+radio+"]' ><input type='radio' id='que"+nidQue+"_radio"+radio+"' name='correct_answer' value='option_"+radio+"'>"+
                    "<label for='que"+nidQue+"_radio"+radio+"'>{{ trans('messages.correct_answer') }}</label>"+
                    "<span class='remove-option pull-right' onclick='deleteThisOption(this,\"\");'>&times;</span></div>";
        $(ele).parent().siblings(".answer_type_M").append(html);
        $('.added-option .animated').autosize({append: "\n"});
        $('.added-option .animated').css('height',34);
        $(ele).parent().siblings(".answer_type_M").children().last().removeClass("added-option");
    }
</script>
