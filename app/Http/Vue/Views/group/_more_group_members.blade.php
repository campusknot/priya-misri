@if(count($oGroupMemberList))
<?php
    foreach($oGroupMemberList as $oGroupMember)
    {
        $sDivEffects = "";
        $sHtml = "";
        if(isset($oGroupMemberDetail) && $oGroupMemberDetail->member_type != config('constants.GROUPMEMBER') && $oGroupMember->id_user != Auth::user()->id_user && $oGroupMember->member_type == config('constants.GROUPMEMBER') )
        {
            $sDivEffects = "onmouseover=showRemoveLink('".$oGroupMember->id_group_member."'); onmouseout=hideRemoveLink('".$oGroupMember->id_group_member."');";
            $sHtml = "<a id='remove_".$oGroupMember->id_group_member."' class='hidden pull-right color-light-gray remove-link' onclick=\"removeMemberFromGroup('".trans('messages.remove_user_confirmation', ['member' => $oGroupMember->first_name])."',".$oGroupMember->id_group.", ".$oGroupMember->id_user.");\" href='javascript:void(0);' data-toggle='tooltip' title='".trans('messages.remove_member')."'>";
            $sHtml .= "<span class='round-img glyphicon glyphicon-remove-circle'></span>";
            $sHtml .= "</a>";
        }
?>
        <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12  no-padding" {{$sDivEffects}}>
            <!-- <div class="clearfix member-detail ck-background-white round-coreners-5">  -->
              
            <div class="text-left single-member clearfix">
            <a href=" {{ route('user.profile', ['nIdUser' => $oGroupMember->id_user]) }}" title="{{ trans('messages.click_here', ['click_hint' => 'to view '.$oGroupMember->first_name.' '. $oGroupMember->last_name.'\'s profile']) }}">
                <div class="icon-image img-66 pos-relative">
                    {!! setProfileImage("150",$oGroupMember->user_profile_image,$oGroupMember->id_user,$oGroupMember->first_name,$oGroupMember->last_name) !!}
                    @if($oGroupMember->user_type == config('constants.USERTYPEFACULTY'))
                        <span class="faculty-label">{{config('constants.USERTYPEFACULTY') }}</span>
                    @endif
                    
                </div>
                <span class="contact-info display-ib v-align ">
                    <span class="mightOverflow {{$oGroupMember->user_type}}-name text-left font-w-500" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                        {{ $oGroupMember->first_name.' '. $oGroupMember->last_name }}
                        @if($oGroupMember->member_type == config('constants.GROUPADMIN'))
                            <span class="label admin-label">{{trans('messages.admin')}}</span>
                        @endif
                    </span>
                    <!--<span>
                        <?php 
                             switch ($oGroupMember->user_type) {
                                 case config('constants.USERTYPESTUDENT'):
                                     echo trans('messages.signup_as_student');
                                     break;
                                 case config('constants.USERTYPEFACULTY'):
                                     echo trans('messages.signup_as_faculty');
                                     break;
                                 default:
                                     break;
                             }
                             ?>
                     </span>-->
                    <span class="user-degree  set-max-limit text-left">
                        @if(count($oGroupMember->major)>0)
                            {{$oGroupMember->major['degree']}} {{ ($oGroupMember->major['major']) ? ','.$oGroupMember->major['major'] : ''}}
                        @endif
                    </span>

                </span>
            </a>
            @if($oGroupMember->id_user != Auth::user()->id_user)
                <span class="follow-wrapper">
                @if($oGroupMember->id_user_follow!='')
                    <a href="javascript:void(0);" class=" btn btn-outline pull-right" onclick="callFollowUser(this,'{{ route("user.un-follow-user", ['user' => $oGroupMember->id_user])}}',1,{{ $oGroupMember->id_user }})">{{ trans('messages.unfollow') }}</a>
                      {!! $sHtml !!}
                @else
                    <a href="javascript:void(0);" class=" btn btn-outline pull-right" onclick="callFollowUser(this,'{{ route("user.follow-user", ['user' => $oGroupMember->id_user])}}',0,{{ $oGroupMember->id_user }})">{{ trans('messages.follow') }}</a>
                      {!! $sHtml !!}
                @endif
                </span>
             @else
                <span class="no-action"></span>    
            @endif
            </div>
        </div>
<?php
    }
?>
@else
    <div class="col-lg-12 no-data-found m-t-2 background-white round-coreners-8">
        <h2> {{ trans('messages.no_record_found') }} </h2>
    </div>
@endif