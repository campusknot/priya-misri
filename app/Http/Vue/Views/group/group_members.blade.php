@extends('layouts.web.main_layout')
@section('title', 'Campusknot-Group Members')

@section('left_sidebar')
    @if(count($oGroupDetails))
        @include('WebView::group._group_sidebar')
    @endif
@endsection

@section('content')
<div id="group_detail_main">
@if(empty($oGroupDetails))
    <div class="ck-background-white clearfix">
        <div class="deleted-data">
            <img class="" src="{{asset('assets/web/img/deleted_group.png')}}">
            {{ trans('messages.group_deleted') }}
        </div>
        
    </div>
@else
    @include('WebView::group._group_members_ajax')
@endif
</div>

    <div class="switch-tab-loader side-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

@endsection