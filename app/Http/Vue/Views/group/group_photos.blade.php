@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Group Photos')

@section('left_sidebar')
    @if(count($oGroupDetails))
        @include('WebView::group._group_sidebar')
    @endif
@endsection

@section('content')
<div id="group_detail_main">
     @if(empty($oGroupDetails))
        <div class="ck-background-white clearfix">
            <div class="deleted-data">
                <img class="" src="{{asset('assets/web/img/deleted_group.png')}}">
                {{ trans('messages.group_deleted') }}
            </div>

        </div>
    @else
        @if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
            <div class="ck-background-white">
                <div class="no-data ">
                    <img src="{{asset('assets/web/img/private-group.png')}}">
                    <div>{{ $oGroupDetails->about_group }}</div>
                    <div class="sml-text">
                    @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                        {{ trans('messages.private_group_member') }}
                    @else
                        {{ trans('messages.secret_group_member') }}
                    @endif
                    </div>
                </div>
            </div>
        @else
            <div class="group-photos">
                 <div class="inner-page-heading padding-10 clearfix">
                    <h3>{{ trans('messages.photos') }}</h3>
                     @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
                        <div class="btn btn-primary  pull-right" data-toggle="collapse" data-target="#add_photo" aria-expanded="false">{{ trans('messages.upload_photo') }}</div>
                    @endif
                    @if(count($oGroupDetails))
                        @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
                            <div class="clearfix collapse" id="add_photo" >
                                @include('WebView::post._add_photo')
                            </div>
                        @endif
                </div>

                    <div id="group_photo" class="photo-list clearfix">
                        @if(count($oGroupFeeds))
                            @foreach($oGroupFeeds as $oGroupFeed)
                                <div id="{{ $oGroupFeed->id_post }}" class="col-lg-4 col-md-4 col-sm-6 col-xs-6" onclick="showPhotoDetails('{{ $oGroupFeed->id_post }}','{{ $oGroupDetails->id_group }}');">
                                    <div class=" card-bg sml-height-card no-padding">
                                        <div class="photo">
                                            <img class="photo-feed" src="{{ config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oGroupFeed->file_name }}"/>
                                        </div>
                                        <div class="card-bottom-info clearfix" >
                                             <span class="file-name col-lg-8 col-md-8 col-sm-8 col-xs-8 mightOverflow no-padding" data-toggle="tooltip" data-placement="bottom" title="">{{ $oGroupFeed->display_file_name }}</span>
                                             <span class="pull-right ">{{ secondsToTime($oGroupFeed->created_at) }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach


                        @else
                        <div class="no-data">
                            <img src="{{asset('assets/web/img/no-photos.png')}}">
                            {{ trans('messages.no_group_photos') }}
                        </div>
                        @endif

                        <div class="more-data-loader hidden">
                            <div class="spinner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                    </div>
                @else
                <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 no-data-found m-t-2 background-white round-coreners-8">
                    <h2> {{ trans('messages.no_record_found') }} </h2>
                </div>
                @endif
            </div>
        @endif
    @endif
    
</div>

    <div class="switch-tab-loader side-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

@section('footer-scripts')
    var nCurrentPage = <?php echo $oGroupFeeds->currentPage(); ?>;
    var nLastPage = <?php echo $oGroupFeeds->lastPage(); ?>;
    
    $(window).scroll(function (event) {
        if(window.location.href == '<?php echo route('group.group-photos', ['nIdGroup' => $oGroupDetails->id_group]); ?>')
        {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('group_photo', '<?php echo route('group.group-photos', ['nIdGroup' => $oGroupDetails->id_group]); ?>',nCurrentPage);
                }
            }
        }
    });
@endsection
@stop