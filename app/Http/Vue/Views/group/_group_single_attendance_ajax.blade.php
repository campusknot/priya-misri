
<!-- Filename:: group._group_single_attendance_ajax -->
@if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
    <div class="ck-background-white">
        <div class="no-data ">
            <img src="{{asset('assets/web/img/private-group.png')}}">
            <div>{{ $oGroupDetails->about_group }}</div>
            <div class="sml-text">
            @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                {{ trans('messages.private_group_member') }}
            @else
                {{ trans('messages.secret_group_member') }}
            @endif
            </div>
        </div>
    </div>
@else
    <div class="inner-page-heading padding-10">
        <h3 class=""> {{ trans('messages.attendance') }} </h3>
    </div>
    <div class="clearfix event-list pos-relative">
        @if(count($oAllUniqueLecture))
            <div id="attendance_list" class="attendance-detail">
                @include('WebView::group._group_more_attendance')
            </div>
        @else
        <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="no-data"> {{ trans('messages.no_record_found') }} </div>
        </div>
        @endif
    </div>
@endif

<script type="text/javascript">
    var nCurrentPage = <?php echo $oAllUniqueLecture->currentPage(); ?>;
    var nLastPage = <?php echo $oAllUniqueLecture->lastPage(); ?>;
    
    $(document).scroll(function (event) {
        if(window.location.href == "{{ route('group.group-attendance', ['nIdGroup' => $oGroupDetails->id_group]) }}")
        {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('attendance_list', '<?php echo route('group.group-attendance', ['nIdGroup' => $oGroupDetails->id_group]); ?>',nCurrentPage,'');
                }
            }
        }
    });
</script>
