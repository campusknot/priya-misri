<!-- Filename:: group._group_more_attendane -->
    @foreach($oAllUniqueLecture as $oLecture)
        <div class="stu-course" id="">
            @if($oLecture->member_type == config('constants.GROUPMEMBER'))
                <div class="panel" onclick="showAttendaceDataSlider({{ $oLecture->id_group_lecture }},'{{ route('group.student-attendance-table') }}','s');">
            @else
                <div class="panel" onclick="showAttendaceDataSlider({{ $oLecture->id_group_lecture }},'{{ route('group.admin-attendance-table') }}','f');">
            @endif
                <a data-toggle="collapse" class="course accordion-toggle pos-relative" >
                    <span class="attendance-subject-name">
                        {{ $oLecture->group_name }}
                        @if($oLecture->section != '')
                            {{ trans('messages.section') }}- {{$oLecture->section}} ({{$oLecture->semester}})
                        @endif
                    </span>            

                    <div class="pull-right stu-total-attendance">
                    </div>
                    <div class="attendance-faculty">
                        <div class="">
                            {{ trans('messages.semester') }} : {{ $oLecture->semester }}
                        </div>
                        <div class="">
                             {{ trans('messages.group_creator') }} : {{ $oLecture->first_name }} {{ $oLecture->last_name }}
                            @if($oLecture->member_type ==  config('constants.GROUPMEMBER'))
                            <span class="pull-right">
                                {{ trans('messages.total_attendance') }}
                               <span class="present stu-present-indication">{{ $oLecture->present_lectures_count }}</span> |
                               <span class="absent stu-absent-indication">{{ $oLecture->absent_lectures_count }}</span> |
                               <span class="sick stu-sick-indication">{{ $oLecture->sick_leave_lectures_count }}</span>
                               <span class="total_lec">/ {{ $oLecture->all_lectures_count }}</span>
                            </span>
                             @endif
                        </div>
                        
                        
                    </div>
                    
                </a>
            </div>
        </div>
    @endforeach
