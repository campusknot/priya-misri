<div class="group-list clearfix group-list-bg" id="tab_group_deactivated" >
    @if(count($oGroupList['deactive_groups']) > 0)
        @foreach($oGroupList['deactive_groups'] as $oGroupDetails)
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <a href="{{ route('group.group-feeds', ['nIdGroup' => $oGroupDetails->id_group]) }}" >	
                    <div class="text-left single-group">
                        <div class="group-icon  icon-image img-80">
                            {!! setGroupImage($oGroupDetails->group_image,$oGroupDetails->group_category_image) !!}
                        </div>
                        <span class="group-info display-ib v-align">
                            <span class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                                {{ $oGroupDetails->group_name }}
                                @if($oGroupDetails->section != '')
                                    {{ trans('messages.section').'-'. $oGroupDetails->section.'('.$oGroupDetails->semester.')'}}
                                @endif
                            </span>
                            <div>{{ trans('messages.category') }}{{ $oGroupDetails->group_category_name }} | {{ trans('messages.'.$oGroupDetails->group_type) }}</div>
                            <div>{{ trans('messages.creator') }}{{ $oGroupDetails->first_name }} {{ $oGroupDetails->last_name }}</div>
                            <span class="group-member-detail">{{ trans('messages.member_count',['member_count' => $oGroupDetails->member_count]) }}</span>

                        </span>
<!--                        <div class="group-info"><span class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>{{ $oGroupDetails->group_name }}</span>{{ $oGroupDetails->group_category_name }}</div>
                        <div class="card-bottom-info">{{ trans('messages.member_count',['member_count' => $oGroupDetails->member_count]) }}<span class="pull-right">{{ trans('messages.'.$oGroupDetails->group_type) }}</span></div>-->
                    </div>
                </a>
            </div>
        @endforeach
    @else
        <div>{{ trans('messages.no_record_found') }}</div>
    @endif
</div>
<script type="text/javascript">
    var nCurrentPage = <?php echo $oGroupList['deactive_groups']->currentPage(); ?>;
    var nLastPage = <?php echo $oGroupList['deactive_groups']->lastPage(); ?>;

    $(window).scroll(function (event) {
        if ($('#tab_group_deactivated').length){
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if(nCurrentPage < nLastPage) {
                    nCurrentPage +=1;
                    loadNewData('tab_group_deactivated', '<?php echo route('group.deactive-group-listing-ajax'); ?>', nCurrentPage);
                }
            }
        }
    });
</script>