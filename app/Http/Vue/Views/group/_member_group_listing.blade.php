<div class="group-list-bg clearfix" id="tab_group_member" >
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="group_search"  class="clearfix" name="group_search" method="POST" enctype="multipart/form-data" onsubmit="searchGroup('/group/member-group-listing-ajax/','/group/member-group-listing/');return false;">
            <div class="input-group group-search pull-right">
                <input id="search_group" type="text" class="form-control" name="search_param"  placeholder="{{ trans('messages.search_group') }}" value="{{ $sSeacrhStr }}" required="required">
                <button type="submit" onclick="searchGroup('/group/member-group-listing-ajax/','/group/member-group-listing/');return false;">
                    <i class="glyphicon glyphicon-search"></i>
                </button>  
            </div>
        </form>
    </div>
    @if(count($oGroupList['member_groups']) > 0)
      @include('WebView::group._more_member_group_listing')
    @else
        <div class="no-data">
            <img src="{{asset('assets/web/img/no-group.png')}}">
            {!! trans('messages.no_admin_group') !!}
        </div>
    @endif
</div>
<script type="text/javascript">
    var nCurrentPage = <?php echo $oGroupList['member_groups']->currentPage(); ?>;
    var nLastPage = <?php echo $oGroupList['member_groups']->lastPage(); ?>;

    $(window).scroll(function (event) {
        if ($('#tab_group_member').length){
            if($(window).scrollTop() + $(window).height()+50 >= $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('tab_group_member', '<?php echo route('group.member-group-listing-ajax'); ?>/'+$('#search_group').val(), nCurrentPage);
                }
            }
        }
    });
</script>