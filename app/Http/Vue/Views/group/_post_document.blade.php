<div class="{{ (Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD')) ? 'drop_doc' : '' }}">
    <ul class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix  docs-top-heading-bar group-docs ">
        <li> <a onclick="getDocumentData('{{ route('group.group-child-documents-ajax',$oGroupDetails->id_group ) }}','{{ route('group.group-documents',$oGroupDetails->id_group) }}');" >{{ trans('messages.group_documents',[ 'group'=> $oGroupDetails->group_name ]) }}</a></li>
        <li class="post-doc-title">{{ trans('messages.post-document-title') }} </li>
    </ul>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
    <form id="group_search" name="group_search" onsubmit="searchDocument('{{ route('group.group-post-documents-ajax',['nIdGroup' => $oGroupDetails->id_group]) }}','{{ route('group.group-post-documents',['nIdGroup' => $oGroupDetails->id_group]) }}','search_post_document','group_detail_main');return false">
        <div class="input-group group-search">                
            <input id="search_post_document" type="text" class="form-control" name="search_member"  placeholder="{{ trans('messages.search_group_document') }}" value="{{ ($sSearchStr) ? $sSearchStr : '' }}" >
            <button type="submit">
                <i class="glyphicon glyphicon-search"></i>
            </button>              
        </div>
    </form>
    </div>    
        
        
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 document-list_my-docs">
        <ul class="my-docs_document-title clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <li class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                {{ trans('messages.name') }}
                <span class="column-sorting-options display_file_name" onclick="getDocumentSort('display_file_name','{{ route('group.group-post-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}','group_detail_main');">
                    <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='display_file_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/asc.png')}}">
                    </span>
                    <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='display_file_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                <span>
                    {{ trans('messages.doc-owner') }}
                    <span class="column-sorting-options first_name" onclick="getDocumentSort('first_name','{{ route('group.group-post-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}','group_detail_main');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
                <span class="pull-right">
                    {{ trans('messages.date') }}
                    <span class="column-sorting-options updated_at" onclick="getDocumentSort('updated_at','{{ route('group.group-post-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}','group_detail_main');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
            </li>
            <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{ trans('messages.size') }}</li>-->
        </ul>
        <div class="document-list-parent clearfix" id="more_document">
            @if(count($oPostDocument))
                @foreach($oPostDocument as $oDocument)

                <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id=''>
                    <li class="col-lg-9 col-md-9 col-sm-9 col-xs-9 document-name">
                        <a target="_blank" href="{{route('utility.view-file',['p_'.$oDocument->id_post,$oDocument->display_file_name]) }}" >                        
                                <?php
                                $aFileNameData = explode('.', $oDocument->file_name);

                                //Div for file icon
                                if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('zip','rar')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                }
                                elseif (end($aFileNameData) == 'pdf')
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                }
                                echo $sHtmlString;
                                ?>
                                {{ $oDocument->display_file_name }}                       
                            </a> 
                    </li>
                    
                 <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
                    <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{ $oDocument->last_name }}">
                        {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
                    </span>
                    <span class="pull-right ">
                        <span class="doc-date">{{ $oDocument->updated_at->format('Y-m-d') }}</span>

                        <span class="folder-action pull-right">
                            <a target="_blank" href="{{route('utility.download-file',['p_'.$oDocument->id_post,$oDocument->display_file_name]) }}" > 
                                <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.delete')}}">
                            </a>
                        </span>
                    </span>
 
                 </li>   
                    <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>-->
                </ul>
                @endforeach
            @else
                <div class="large-padding no-data">
                    <img class="" src="{{asset('assets/web/img/no-documents.png')}}">
                    {{ trans('messages.no_documents') }}

                </div>
            
            @endif
        </div>

    </div>
</div>
  
<script>
$(document).ready(function(){
    var nCurrentPage = 0;
    nCurrentPage = <?php echo $oPostDocument->currentPage(); ?>;
    var nLastPage = <?php echo $oPostDocument->lastPage(); ?>;
    $(window).scroll(function (event) {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            if(nCurrentPage < nLastPage) {
                nCurrentPage +=1;
                loadNewData('more_document', '<?php echo route('group.group-post-documents', ['nGroupId' => $oGroupDetails->id_group]); ?>', nCurrentPage, '&order_by=<?php echo $sOrderBy; ?>&order_field=<?php echo $sOrderField; ?>&search_str=<?php echo $sSearchStr; ?>');
            }
        }
    });
 });  
$('#search_post_document').keyup(function(){
    var text = $('#search_post_document').val();
    if(text.length > 2)
    searchDocument('<?php echo route('group.group-post-documents-ajax',['nIdGroup' => $oGroupDetails->id_group]); ?>','<?php echo route('group.group-post-documents',['nIdGroup' => $oGroupDetails->id_group]); ?>','search_post_document','group_detail_main');
});
</script>