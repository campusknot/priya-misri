<!-- Filename:: _attendance_group_admin_ajax -->
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding faculty-attendance-table">
    <div class="attendance-log-show ">
    </div>
    <div id="attendance_table" class="height400">
        <table class="fancyTable" id="myTable03" cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th class="attendance-heading pos-relative"><span>{{ trans('messages.students') }}</span></th>
                    <th class="attendance-heading set-padding">{{ trans('messages.total') }}</th>
                      <?php $aLecture=array(); $record=0; $nColom = 2;?>
                        @foreach($oLecture as $Lecture)
                            <?php 
                            $aLecture[$record]=$Lecture->id_group_lecture;
                            $record++;
                            $dLectureDate = Carbon::createFromFormat('Y-m-d H:i:s', $Lecture->created_at);
                            $dLectureDate = $dLectureDate->setTimezone(Auth::user()->timezone);
                            $nColom++;
                            ?>
                        <th class="attendance-heading">
                            <div class="present-student-count">
                                <span>{{ $Lecture->present_student }}</span>
                                <span> / {{ $oUserAttendance->total() }}</span>
                            </div>
                            <span onclick="removeAttenColumn({{ $nColom }},{{ $Lecture->id_group_lecture }},'{{trans('messages.remove_attendance')}}');"><img class="remove-attendance cursor-pointer" src="{{asset('assets/web/img/remove.png')}}" data-toggl="tooltip" title="{{trans('messages.remove_attendance_text') }}"></span>
                            {{ $dLectureDate->format('d, M ') }}
                            <div>{{ $dLectureDate->format('D | h:i A') }}</div>
                        </th>
                        @endforeach

                </tr>
            </thead>
            <tbody>
                @foreach($oUserAttendance as $oUserData)
                <?php
                //echo "<pre>";print_r($oUserData);
                ?>
                <tr>
                    <td class=""><span class="img-35">
                            <a href="{{route('user.profile',[$oUserData->id_user])}}">
                                {!! setProfileImage("50",$oUserData->user_profile_image,$oUserData->id_user,$oUserData->first_name,$oUserData->last_name) !!}
                            </a>
                        </span>
                        <span class="person-name">{{ $oUserData->first_name.' '.$oUserData->last_name}}
                            <span>({{ $oUserData->email }})</span>
                        </span>
                    </td>
                        <?php 
                        $aIdPresentLecture = explode(',',$oUserData->id_lecture_present);
                        $aIdSickLecture = explode(',',$oUserData->id_lecture_sick);
                        ?>
                    <td class="min-cell-width pos-relative">
                        <span class="present">{{ $oUserData->present_lectures_count }}</span> |
                        <span class="absent">{{ $oUserData->absent_lectures_count }}</span> |
                        <span class="sick-leave">{{ $oUserData->sick_leave_lectures_count }}</span>
                        <span class="total_lec">/ {{ $oUserData->all_lectures_count }}</span>

                        @if($oUserData->id_group_attendance_log!='')
                            <span class="check-log" onclick="getUserAttendanceLog('{{ route('group.attendance-log') }}','{{ $oUserData->id_group_attendance_log }}' , this);">
                                {{ trans('messages.log') }}
                            </span>
                        
                        @endif
                    </td>
                        @foreach($aLecture as $rec)
                            <td class="min-cell-width attendance-cell pos-relative">
                                <span class="edit-text">{{ trans('messages.edit') }}</span>
                                <span class="edit-attendance" id="{{$oUserData->id_user}}_{{$rec}}">
                                @if(in_array($rec,$aIdPresentLecture)) 
                                <?php //print_r($aIdPresentLecture);?>
                                <span class="attendance-record present pos-relative"><!--onclick="changeStudentAttendance({{$oUserData->id_user}},{{$rec}},'A');"-->
                                    <span>{{ trans('messages.present_short')}}</span>
                                    <span class="edit-attendance-text arrow-right">
                                        <span class="pull-right close-edit-attendance">×</span>
                                        <span>
                                            <input class="" type="radio" name="attendance_{{ $oUserData->id_user }}_{{$rec}}" value="{{ config('constants.ATTENDANCETYPEABSENT') }}" > 
                                            {{ trans('messages.mark_as_absent') }}
                                        </span>
                                        <span>
                                            <input class="" type="radio" name="attendance_{{$oUserData->id_user}}_{{$rec}}" value="{{ config('constants.ATTENDANCETYPESICKLEAVE') }}"> 
                                            {{ trans('messages.mark_as_sick_leave') }}
                                        </span>
                                        <span>
                                            <input type="button" onclick="changeStudentAttendance({{$oUserData->id_user}},{{$rec}},'group');" value="{{ trans('messages.save') }}" class="link-button pull-right"> 
                                        </span>

                                    </span>
                                </span>
                                @elseif(in_array($rec,$aIdSickLecture)) 
                                <span class="attendance-record sick-leave pos-relative"><!--onclick="changeStudentAttendance({{$oUserData->id_user}},{{$rec}},'A');"-->
                                    <span>{{ trans('messages.sick_leave_short')}}</span>
                                    <span class="edit-attendance-text arrow-right">
                                        <span class="pull-right close-edit-attendance">×</span>
                                        <span>
                                            <input class="" type="radio"  name="attendance_{{$oUserData->id_user}}_{{$rec}}" value="{{ config('constants.ATTENDANCETYPEABSENT') }}"> 
                                            {{ trans('messages.mark_as_absent') }}
                                        </span>
                                        <span>
                                            <input class="" type="radio" name="attendance_{{$oUserData->id_user}}_{{$rec}}" value="{{ config('constants.ATTENDANCETYPEPRESENT') }}" > 
                                            {{ trans('messages.mark_as_paresent') }}
                                        </span>
                                        <span>
                                            <input type="button" onclick="changeStudentAttendance({{$oUserData->id_user}},{{$rec}},'group');" value="{{ trans('messages.save') }}" class="link-button pull-right"> 
                                        </span>
                                    </span>
                                </span>
                                @else 
                                <span class="attendance-record absent pos-relative"> <!--onclick="changeStudentAttendance({{$oUserData->id_user}},{{$rec}},'P');"-->
                                    <span>{{ trans('messages.absent_short')}}</span>
                                    <span class="edit-attendance-text arrow-right">
                                        <span class="pull-right close-edit-attendance">×</span>
                                        <span>
                                            <input class="" type="radio" name="attendance_{{$oUserData->id_user}}_{{$rec}}" value="{{ config('constants.ATTENDANCETYPEPRESENT') }}">
                                            {{ trans('messages.mark_as_paresent') }}
                                        </span>
                                        <span>
                                            <input class="" type="radio" name="attendance_{{$oUserData->id_user}}_{{$rec}}" value="{{ config('constants.ATTENDANCETYPESICKLEAVE') }}">
                                            {{ trans('messages.mark_as_sick_leave') }}
                                        </span>
                                        <span>
                                            <input type="button" onclick="changeStudentAttendance({{$oUserData->id_user}},{{$rec}},'group');" value="{{ trans('messages.save') }}" class="link-button pull-right"> 
                                        </span>
                                    </span>
                                </span>    
                                @endif
                                </span>
                            </td>
                        @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="clear"></div>
<script type="text/javascript">
showTooltip();
$(document).ready( function() {
     /*attendance > add student > hide when click somewhere else*/
    $('.faculty-attendance-table , .attendance-lec-info,.faculty-attendance ').click(function() {
        $('.attendance_studnet-suggestion-box').removeClass("show");
    });
    $('.attendance_studnet-suggestion-box').click(function(event){
       event.stopPropagation();
    });
	
})
</script>