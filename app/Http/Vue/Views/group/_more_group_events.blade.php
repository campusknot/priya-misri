@if(count($oEventTabs))
<?php
$sPreviousYear = (Session::has('event_list_current_year')) ? Session::get('event_list_current_year') : '';
$sPreviousDate = (Session::has('event_list_current_date')) ? Session::get('event_list_current_date') : '';
$nEventCount = 0;
?>
@foreach( $oEventTabs as $Event )
<?php
$dEventStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $Event->start_date);
$dEventStartDate->setTimezone(Auth::user()->timezone);
$dEventEndDate='';
if($Event->end_date != null){
    $dEventEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $Event->end_date);
    $dEventEndDate->setTimezone(Auth::user()->timezone);
}
$sEventYear = $dEventStartDate->format('Y');
$sEventDate = $dEventStartDate->format('M j');
if($sEventDate != $sPreviousDate)
{
    session(['event_list_current_date' => $sEventDate]);
    $sPreviousDate = $sEventDate;
    if($nEventCount > 0)
        echo '</div><div class="event-detail">';

    if($sEventYear != $sPreviousYear)
    {
        session(['event_list_current_year' => $sEventYear]);
        $sPreviousYear = $sEventYear;
        echo '<div class="clearfix"><span class="event-day">'.$sEventYear.'</span></div>';
    }
?>
<span class="event-day">{{$dEventStartDate->format('M j')}}  | {{$dEventStartDate->format('l')}}</span>
<?php } ?>
    @if(count($Event))
        <ul class="" onclick="getSingleEvent({{$Event->id_event}},'GET');">
            <li class="center">
                <span class="event-time">{{$dEventStartDate->format('g:i A')}}</span>
            </li>
            <li>
                <div class="event-name">{{$Event->event_title}}</div><!-- event -->
                <div class="">{{$dEventStartDate->format('l')}} {{$dEventStartDate->format('M j')}}, {{$dEventStartDate->format('g:i A')}}
                    @if($dEventEndDate != '')
                - {{ ($dEventStartDate->toDateString() == $dEventEndDate->toDateString()) ? $dEventEndDate->format('g:i A') : 
                            $dEventEndDate->format('l').' '.$dEventEndDate->format('M j').', '.$dEventEndDate->format('g:i A') }}
                @endif
                </div>
                @if($Event->latitude != '0.00000000' &&  $Event->longitude != '0.00000000' )
                    <a class="event-place" target="_blank" href="{{config('constants.GMAPURL')}}?q={{$Event->address}}">{{$Event->address}}:{{$Event->room_number}}</a> <!-- place -->
                @else
                    <div>{{$Event->address}}:{{$Event->room_number}}</div>
                @endif
            </li>
        </ul>
        @else
        <div class="center">
                <img src="{{ asset('assets/web/img/no-event.png') }}" />
            <h4> {{ trans('messages.no_events_found') }} </h4>
        </div>
    @endif
@endforeach
@else
<div class="col-lg-12 no-data-found m-t-2 background-white round-coreners-8">
    <h2> {{ trans('messages.no_record_found') }} </h2>
</div>
@endif