
@if(count($oLecture) > 0)
<div class="attendance-lec-info clearfix">
        {{ $oLecture[0]->group_name }} 
        @if($oLecture[0]->section != '')
            {{ trans('messages.section') }}- {{$oLecture[0]->section}} ({{$oLecture[0]->semester}})
        @endif
        <br>
    <span>{{ trans('messages.semester') }} : {{ $oLecture[0]->semester }}</span> 
    <form method="post" action="{{ route('group.attendance-excel') }}" id="excel" class="download-excel-form">
        {!! csrf_field() !!}
        <input type="hidden" id="id_lecture_excel" name="id_lecture_excel" value="{{ $oLecture[0]->id_group_lecture }}"/>
        <input type="hidden" id="id_group" name="id_group" value="{{ $oLecture[0]->id_group }}"/>
        <input type="submit" value="{{ trans('messages.download_excel') }}" id="excel_download" class="">
    </form>
<!--    <div onclick="" class="btn btn-primary pull-right"  aria-expanded="false" id="excel_download">{{ trans('messages.download_excel') }}</div>-->
</div>
<div class="attendance-options clearfix">
    <form method="post" action="{{ route('group.admin-attendance-table') }}" id="getattendance" class="faculty-attendance">
        {!! csrf_field() !!}
        <div class="input-group group-search attendance-search">
            <input id="search_student" type="text" class="form-control" name="search_student" placeholder="{{ trans('messages.search_student') }}" value="{{ isset($oRequest->search_student) ? $oRequest->search_student : ''}}">
            <button type="submit" id="search_btn">
                <i class="glyphicon glyphicon-search"></i>
            </button>  
        </div>
        <div class=" attendance_date-filter">
            <span>Filter |</span>
            <input type="text" id="attendqance_from_dt" name="from" class="form-control" placeholder="{{trans('messages.start_date')}}" value="{{ (isset($oRequest->from)) ? $oRequest->from : Carbon::createFromFormat('Y-m-d H:i:s', $oLecture[0]->created_at)->setTimezone(Auth::user()->timezone)->format('m-d-Y') }}">
            
            <input type="text" id="attendqance_to_dt" name="to" class="form-control" placeholder="{{trans('messages.end_date')}}" value="{{ (isset($oRequest->to)) ? $oRequest->to : Carbon::createFromFormat('Y-m-d H:i:s', $oLecture[0]->created_at)->setTimezone(Auth::user()->timezone)->format('m-d-Y') }}">
            <input type="hidden" name="id_lecture" id="id_lecture" value="{{ $oLecture[0]->id_group_lecture }}" />
            <button onclick="closeLightBox();" class="btn btn-primary" type="button" id="btn-atten" data-loading-text="">{{ trans('messages.go') }}</button>
        </div>
       
    </form>
<!--    <div class=" attendance_add-student-wraper ">
        <span class="attendance_add-student">
            {{ trans('messages.add_student') }}
            
        </span>
        <div class="attendance_studnet-suggestion-box arrow-up">
            <div id='msg-add-user'></div>
            <input class="form-control" id='user_list' type="text" placeholder="{{ trans('messages.student_name') }}">
            <input class="form-control" id='id_user' type="hidden">
            <input type="submit" class="btn-add-user" value="Add" data-loading-text="">
            
        </div>
    </div>-->
</div>
<div class="attendance-notation">
       <ul class="pull-right">
       <li class="stu-present-indication">
           {{ trans('messages.attendance_present') }}
       </li>
       <li class="stu-absent-indication">
           {{ trans('messages.attendance_absent') }}
       </li>
       <li class="stu-sick-indication">
           {{ trans('messages.sick_leave') }}
       </li>
   </ul>
</div>

<div id="append_attendance">
    @if(count($oLecture))
        @include('WebView::group._attendance_group_admin_ajax')
    @else
    <div class="no-data">{{ trans('messages.no_record_found') }}</div>
    @endif
    
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable03').fixedHeaderTable({ fixedColumns: 1 });
         $( "#attendqance_from_dt" ).datepicker({
            dateFormat: 'mm-dd-yy',
            changeYear: true,
            changeMonth: true,
            maxDate: 0,
        });
         $( "#attendqance_to_dt" ).datepicker({
            dateFormat: 'mm-dd-yy',
            changeYear: true,
            changeMonth: true,
            maxDate: 0,
        });
    });
    $(".attendance_add-student").on('click', function(){
          $('.attendance_studnet-suggestion-box').toggleClass("show");
    });
   
    $(".edit-text").on('click', function(){
          $('.edit-attendance-text').css('display','none');
        $('.attendance-text').css('display','none');
        $('.edit-text').css('display','none');
        $(this).css('display','block');
        $(this).siblings('.edit-attendance').children('.attendance-record').children('.edit-attendance-text').css('display', 'block');
        
    });
    $(".close-edit-attendance").on('click', function(){
          $('.edit-attendance-text').css('display','none');
          $('.edit-text').css('display','none');
    });
var optionsgetAtten = { 
    beforeSubmit:  showRequestAttenAjax,
    success:       showResponseAttenAjax,
    error:         showErrorAttenAjax,
    dataType: 'json' 
    }; 
$('#btn-atten,#search_btn').click(function(){
    var data = $(this);
    $('#getattendance').ajaxForm(optionsgetAtten).submit();
});
function showRequestAttenAjax(formData, jqForm, options)
{ 
    $("#validation-errors").hide().empty();
    $("#output").css('display','none');
    return true; 
}

function showResponseAttenAjax(response, statusText, xhr, $form)
{ 
    if(response.success == true)
    {
        $('#slider_light_box_content').html(response.html);
       
    }
    else
    { 
        var arr = response;
            $.each(arr, function(index, value)
            {
                    if (value.length != 0)
                    {
                            $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                    }
            });                     
    }
}

function showErrorAttenAjax(xhr, textStatus, errorThrown)
{
    var arr = xhr.responseText;
    try {
    $.parseJSON(arr);
       } catch (e) {
        }

    var result = $.parseJSON(arr);
    $.each(result, function(key, value)
    {
        key=key.replace('file','comment_text');
        //$('.btn-md').button('reset');
        if (key.length != 0)
        {
                $("#"+key).html('<strong>'+ value +'</strong>');
        }
    });
}
    var nCurrentPage = <?php echo $oUserAttendance->currentPage(); ?>;
    var nLastPage = <?php echo $oUserAttendance->lastPage(); ?>;
    $('.fht-tbody').scroll(function (event) {
        if($(this).scrollTop() + $(this).innerHeight()+50 >= $(this)[0].scrollHeight ) {
            if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                var search_str = $('#search_student').val();
                var dFrom = $('#attendqance_from_dt').val();
                var dTo = $('#attendqance_to_dt').val();
                nCurrentPage +=1;
                loadAttendanceData('<?php echo route('group.admin-attendance-table').'/'.$oLecture[0]->id_group_lecture; ?>', nCurrentPage,'&search_str='+search_str+'&from='+dFrom+'&to='+dTo);
            }
        }
    });

$('#excel_download').click(function(){
    $('#id_lecture_excel').val($('#id_lecture').val());
    $( "#excel" ).submit();
});

$(function()
    {
        $('#user_list').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('group.member-suggestion',[$oLecture[0]->id_group]) }}"+"?member_str=" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.first_name+' '+value.last_name,
                            value: value.id_user,
                            image: value.profile_img,
                            email:value.email,
                        };

                    }));
                });
            },
            minLength: 2,
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                $('#user_list').val(  ui.item.email );
                $('#id_user').val(  ui.item.value );
                event.preventDefault();
            },
            select: function(event, ui) {
                $('#user_list').val(  ui.item.email );
                $('#id_user').val(  ui.item.value );
                event.preventDefault();
                //window.location = siteUrl +"/user/profile/"+ ui.item.value;       
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion attendance-add-student'></li>" )
                .append( "<span class='icon-image img-35'>"+item.image +"</span><span class='display-ib v-align'>" + item.label +"<div class='user-email'>("+ item.email+ ")</div></span>" )
                .appendTo( ul );
            };
    });
    $('.btn-add-user').on('click',function(){
        $.ajax({
            type: "GET",
            url: "{{ route('group.add-group-attendance') }}",
            data : {'id_user' : $('#id_user').val() , 'id_group_lecture' : $('#id_lecture').val()},
            success: function (data) {
               if(data.success == true)
                {
                    $('#msg-add-user').text(data.msg);
                    $('#msg-add-user').addClass('ck-success');
                    $('#btn-atten').trigger( "click" );
                }
                else
                {
                    $('#msg-add-user').text(data.msg);
                    $('#msg-add-user').addClass('ck-error');
                    
                }
            },
            error: function (data) {
                if(data.status == 401)
                {
                    window.location = siteUrl + '/home';
                }
                console.log('Error:', data);
            },
        });
    
    });
</script>

@else
<div class="no-grp-lecture no-data">{{trans('messages.no_lecture_for_group')}}</div>
@endif