@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Group Documents')

@section('left_sidebar')
    @if(count($oGroupDetails))
        @include('WebView::group._group_sidebar')
    @endif
@endsection

@section('content')
<div id="group_detail_main">
    <!-- Filename:: group.group_documents -->
    @if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
        <div class="ck-background-white">
            <div class="no-data ">
                <img src="{{asset('assets/web/img/private-group.png')}}">
                <div>{{ $oGroupDetails->about_group }}</div>
                <div class="sml-text">
                @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                    {{ trans('messages.private_group_member') }}
                @else
                    {{ trans('messages.secret_group_member') }}
                @endif
                </div>
            </div>
        </div>
    @else
        <div class="group-docs ck-background-white clearfix">
            <div class="inner-page-heading padding-10 clearfix">
                <h3>{{ trans('messages.group_documents_list') }}</h3>
                <!-- check group member and folder or subfolder have a write permission --> 
                <div class="pull-right add-new-document-bar" >
                @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL && Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD'))                
                    @include('WebView::documents._create_folder_tab')                
                @endif
                </div>
            </div>
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="file-upload-loader">
                    <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                    </div>
                </div>
                <div class="file-upload-error">
                    <div class="error-message"></div>
                </div>
            </div>
            <div class="loader_ajax col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden">
                <div class='spinner'> 
                    <div class='bounce1'></div>
                    <div class='bounce2'></div>
                    <div class='bounce3'></div>
                </div>
          </div>
            <div id="document_list" >
                @if($sDocumentListing == 'GroupDocumentList')
                   @include('WebView::group._group_document')
                @elseif($sDocumentListing == 'GroupPostDocumentList')
                    @include('WebView::group._post_document')
                @else
                    <div class="col-lg-12 no-data-found m-t-2 background-white round-coreners-8">
                        <h2> {{ trans('messages.no_record_found') }} </h2>
                    </div>
                @endif
            </div>
            <div class="more-data-loader hidden">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
    @endif
    <script type="text/javascript">
    
        $(document).on('click','.doc-rename',function(e){
            var folderName = $.trim($(this).parent().parent().parent().siblings('li').children('a').text());
            var imgSrc = $.trim($(this).parent().parent().siblings('li').find("img").attr('src'));
            $('#fol_name').val(folderName);
            var renameFolder ='<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <img class="" src="'+imgSrc+'"> <input type="text" class="new-folder-name  form-control"  maxlength="250" value ="'+ folderName+ '" autofocus></li>';
            var doc_id=$(this).closest('ul').attr('id');
            $('#id_document').val(doc_id);
            $(this).closest('ul').prepend(renameFolder);
            $(this).closest('ul').addClass('mk-new-folder');
            $('.new-folder-name').trigger('focus');
            $( ".new-folder-name" ).select();
            $(this).parent().parent().siblings('.document-name').remove();
            $(this).parent().remove();
            e.preventDefault();
        });
    </script>
</div>

<div class="switch-tab-loader side-tab-loader hidden">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
@stop