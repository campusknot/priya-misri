@foreach($oGroupFeeds as $oGroupFeed)
    <div id="{{ $oGroupFeed->id_post }}" class="col-lg-4 col-md-4 col-sm-4 col-xs-4" onclick="showPhotoDetails('{{ $oGroupFeed->id_post }}','{{ $oGroupDetails->id_group }}');">
        <div class="card-bg no-padding">
            <div class="photo">
                <img class="photo-feed" src="{{ config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oGroupFeed->file_name }}" />
            </div>
            <div class="card-bottom-info clearfix">
                <span class="file-name col-lg-8 mightOverflow no-padding" data-toggle="tooltip" data-placement="bottom" title="">{{ $oGroupFeed->display_file_name }}</span>
                
                <span class="pull-right">{{ secondsToTime($oGroupFeed->created_at) }}</span>
            </div>
        </div>
    </div>
@endforeach