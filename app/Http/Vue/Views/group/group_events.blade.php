@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Group Events')
@section('left_sidebar')
    @if(count($oGroupDetails))
        @include('WebView::group._group_sidebar')
    @endif
@endsection

@section('content')
    <div id="group_detail_main">
        @include('WebView::group._group_events_ajax')
    </div>

    <div class="switch-tab-loader side-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
@stop
@section('right_sidebar')
    @if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
    <div></div>
    @else
        @include('WebView::group._group_event_sidebar')
    @endif
@endsection