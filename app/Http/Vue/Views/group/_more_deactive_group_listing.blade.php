@foreach($oGroupList['deactive_groups'] as $oGroupDetails)
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
        <a href="{{ route('group.group-feeds', ['nIdGroup' => $oGroupDetails->id_group]) }}" >	
            <div class="text-left single-group">
                <div class="group-icon   group-icon icon-image img-80">
                    {!! setGroupImage($oGroupDetails->group_image,$oGroupDetails->group_category_image) !!}
                </div>
                <span class="group-info display-ib v-align">
                    <span class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                        {{ $oGroupDetails->group_name }}
                        @if($oGroupDetails->section != '')
                            {{ trans('messages.section').'-'. $oGroupDetails->section.' '.$oGroupDetails->semester}}
                        @endif
                    </span>
                    <div>{{ trans('messages.category') }}{{ $oGroupDetails->group_category_name }} | {{ trans('messages.'.$oGroupDetails->group_type) }}</div>
                    <div>{{ trans('messages.creator') }}{{ $oGroupDetails->first_name }} {{ $oGroupDetails->last_name }}</div>
                    <span class="group-member-detail">{{ trans('messages.member_count',['member_count' => $oGroupDetails->member_count]) }}</span>
                </span>
                
            </div>
        </a>
    </div>
@endforeach
