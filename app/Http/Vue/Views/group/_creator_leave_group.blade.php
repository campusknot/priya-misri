<!-- Modal content-->
<div class="modal-content modal-dialog leave-group">
    <div class="modal-header">
        <h4 class="modal-title">{{ trans('messages.creator_leave_group_title') }}</h4>
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
    <div class="modal-body clearfix">
        <form id="creator_rights_form" name="creator_rights_form" method="POST" enctype="multipart/form-data">
         <p>{{ trans('messages.leave_group_info') }}</p>
         <p>{{ trans('messages.select_from_options') }}</p>
         <div class="clearfix leave-options">
            <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 radio radio-primary">
                <input type="radio" name="leave_option" id="choose_member" value="CM" onclick="leaveOptionClicked(this);" {{ ($errors->has('existing_member'))? 'checked="checked"' : '' }} />
                <label for="choose_member" class="leave-option-radio-buttons">
                    {{ trans('messages.choose_member') }}
                </label>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 radio radio-primary">
                <input type="radio" name="leave_option" id="random_member" value="RM" onclick="leaveOptionClicked(this);" />
                <label for="random_member" class="leave-option-radio-buttons">
                  {{ trans('messages.random_member') }}
                </label>
            </div>
        </div>
         <div class="clearfix col-lg-12">
        
        <div id="existing_members" class="clearfix existing-members {{ ($errors->has('existing_member')) ? '' : 'hidden' }}">
            <h4 class="m-t-0">{{ trans('messages.choose_from_existing_user') }}</h4>
            @if ($errors->has('existing_member'))
                <div class="alert alert-danger">
                    <span class="error_message">{{ $errors->first('existing_member') }}</span>
                </div>
            @endif
            
                {!! csrf_field() !!}
                <input type="hidden" name="id_group" value="{{ $oGroupMemberDetail->id_group }}">
                <input type="hidden" name="id_creator" value="{{ $oGroupMemberDetail->id_user }}">
                <div class="row clearfix">
                    <?php
                        $nCount = 0;
                        foreach($oGroupMembers as $oGroupMember)
                        {
                            if($oGroupMember->id_user != $oGroupMemberDetail->id_user)
                            {
                                if($nCount > 0 && $nCount % 2 == 0)
                                {
                                    echo '</div><div class="row clearfix">';
                                }
                    ?>
                    
                                <div class="col-lg-6 col-md-6 col-sm-6 col-x-6 radio radio-primary choose-member">
                                     <input type="radio" name="existing_member" value="{{ $oGroupMember->id_group_member }}"  id="existing_{{ $oGroupMember->id_group_member }}" />
                                    <label for="existing_{{ $oGroupMember->id_group_member }}" class="leave-option-radio-buttons">
                                        <span class="img-25">
                                            {!! setProfileImage("50",$oGroupMember->user_profile_image,$oGroupMember->id_user,$oGroupMember->first_name,$oGroupMember->last_name) !!}
                                        </span>    
                                        
                                        <span>{{ $oGroupMember->first_name.' '. $oGroupMember->last_name }}</span>
                                    </label>
                                </div>
                    <?php
                                $nCount ++;
                            }
                        }
                    ?>
                </div>
                
            
        </div>
            <div class="row clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 m-t-2">
                    <input class="btn btn-success" type="button" name="save" value="{{ trans('messages.proceed') }}" onclick="submitAjaxForm('creator_rights_form', '{{ route("group.pass-creator-rights") }}', this);">
            </div>
         </div>
        </form>
    </div>
    
    
    
</div>