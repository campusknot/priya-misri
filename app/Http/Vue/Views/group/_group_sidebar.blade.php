<div class="sidebar">
    <!-- Filename:: group._group_sidebar -->
    <div class="top-detail">
        <div class="icon-image img-150 no-margin">
           {!! setGroupImage($oGroupDetails->group_image,$oGroupDetails->group_category_image) !!}
            @if(count($oGroupMemberDetail) && ($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR') || $oGroupMemberDetail->member_type == config('constants.GROUPADMIN'))) 
                <div id="crop-avatar">
                    <div class="avatar-view" title="Change Profile Image">
                        <div class="change-pic">
                            {{ trans('messages.change_icon')}}
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <div class="entity-name "> 
            <span class="bold set-max-limit"  title="{{ $oGroupDetails->group_name }}">
                {{ $oGroupDetails->group_name }}
                @if($oGroupDetails->section != '')
                    {{ trans('messages.section') }}- {{$oGroupDetails->section}} ({{$oGroupDetails->semester}})
                @endif
            </span>
            <span class="set-max-limit sml-light-text"  title="{{ $oGroupDetails->group_category_name }}">{{ $oGroupDetails->group_category_name }}</span>
            <span class="text-capitalize set-max-limit sml-light-text"   title="{{ $oGroupDetails->first_name.' '.$oGroupDetails->last_name }}">{{ trans('messages.creator') }}   {{ $oGroupDetails->first_name.' '.$oGroupDetails->last_name }}</span>
            <span class="sml-light-text">{{ trans('messages.type') }}
            <?php
                switch($oGroupDetails->group_type)
                {
                    case config('constants.PRIVATEGROUP'): echo trans('messages.'.config('constants.PRIVATEGROUP'));
                                                            break;
                    case config('constants.PUBLICGROUP'): echo trans('messages.'.config('constants.PUBLICGROUP'));                                                                            break;
                    case config('constants.SECRETGROUP'): echo trans('messages.'.config('constants.SECRETGROUP'));
                                                            break;
                }
            ?>
            </span> 
        </div>
      
    </div>
    @if($oGroupDetails->member == '')   <!-- if not a member -->
        @if (isset($oGroupRequestJoin) && count($oGroupRequestJoin)>0)  <!-- if user have request for thus group -->
        <div class="show-status">
            @if($oGroupRequestJoin->request_type==config('constants.GROUPREQUESTTYPEINVITE'))
                <span class="request-action">
                    <a href="javascript:void(0);" value="Accept" onclick="UpdateNotificationStatus('A', '{{ $oGroupRequestJoin->id_group_request }}')" class="accept">{{ trans('messages.accept') }}</a>
                    <a href="javascript:void(0);" value="Decline" onclick="UpdateNotificationStatus('R', '{{ $oGroupRequestJoin->id_group_request }}')" class=" decline">{{ trans('messages.decline') }}</a>
                </span>   
            @elseif($oGroupRequestJoin->request_type == config('constants.GROUPREQUESTTYPEJOIN'))  
                <span>{{ trans('messages.request_pending') }}</span>
            @endif
        </div>
        @elseif($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))
        <div class="show-status"> 
            <a href="{{ route('group.join-group', ['nIdGroup' => $oGroupDetails->id_group]) }}" class="btn btn-outline"> {{ trans('messages.ask_join') }} </a>
        </div>
        @elseif( $oGroupDetails->group_type != config('constants.SECRETGROUP'))
        <div class="show-status"> 
            <a  href="{{ route('group.join-group', ['nIdGroup' => $oGroupDetails->id_group]) }}" title="{{ trans('messages.click_here', ['click_hint' => 'to '.trans('messages.join_group')]) }}" class="btn btn-outline">{{ trans('messages.join') }}</a>  
        </div>
        @endif

    @endif
    <ul class="options sidebar-menu-options">
        <li id="active_feeds" class="active_feeds" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-feeds-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-feeds', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>
               {{ trans('messages.group_feeds') }} 
            </a>
        </li>
        <li id="active_member" class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-member-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-member', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.members', ['members_count'=>$aGroupSpecificCounts['members_count']]) }}</a> 
        </li>
        <li id="active_attendance" class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-attendance-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-attendance', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.attendance') }}</a>
        </li>
        <li id="active_events" class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-events-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-events', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.events', ['events_count'=>$aGroupSpecificCounts['events_count']]) }}</a>
        </li>
        
        @if($oGroupDetails->member != '')
            <li id="active_documents"class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-documents', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
                <a> {{ trans('messages.documents') }}</a>
            </li>
        @endif
        
        <li id="active_poll" class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-poll-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-poll', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.polls') }}</a>
        </li>
        
        <li id="active_photos" class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-photos-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-photos', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>@if($aGroupSpecificCounts['photos_count'] == 0) {{ trans('messages.photos') }} @else {{ trans('messages.photos_count', ['photos_count'=>$aGroupSpecificCounts['photos_count']]) }} @endif</a>
        </li>
        
        
        
        @if(count($oGroupMemberDetail)
                && ( ($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR') || $oGroupDetails->id_group_category == config('constants.UNIVERSITYGROUPCATEGORY') ) && $oGroupDetails->member != '')
            )
        <li id="active_invite" class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.invite-member-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.invite-member', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.invite_users') }}</a>
        </li>
        @endif
        
        @if(count($oGroupMemberDetail) && (($oGroupMemberDetail->member_type == config('constants.GROUPADMIN')) || ($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))))
        <li id="active_settings"class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-settings-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}','{{ route('group.group-settings', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.group_settings') }}</a>
        </li>
        @else
        <li id="active_settings"class="" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-settings-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}','{{ route('group.group-settings', ['nIdGroup' => $oGroupDetails->id_group]) }}');">
            <a>{{ trans('messages.about_group') }}</a>
        </li>
        @endif  
    </ul>
</div>
@if(count($oGroupMemberDetail) && (($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')) || ($oGroupMemberDetail->member_type == config('constants.GROUPADMIN'))))
<link rel="stylesheet" href="{{ asset('assets/web/css/crop/cropper.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/web/css/crop/main.css') }}">
<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="">
        <div class="modal-content profile-photo-modal">
            <form class="avatar-form" action="{{ url('group/edit-group-image') }}" enctype="multipart/form-data" method="post">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title" id="avatar-modal-label">{{ trans('messages.change_group_icon') }}</h4>
                </div>
                <div class="modal-body">
                    <div class="avatar-body">

                        <!-- Upload image and data -->
                        <div class="avatar-upload hidden">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <input type="hidden" name="group_id" value="{{ $oGroupDetails->id_group }}" />
                            <input type="hidden" class="avatar-src" name="avatar_src">
                            <input type="hidden" class="avatar-data" name="avatar_data">
                            <label for="avatarInput"></label>
                            <input type="file" class="avatar-input" id="avatarInput" name="Img_file">
                        </div>
                        <div id="validation-error" class="alert alert-danger" style="display: none;"></div>
                        <!-- Crop and preview -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="avatar-wrapper hidden">
                                </div>
                                <div class="select-area">
                                    <div class="select-file-area">
                                        <img src="{{ asset('assets/web/img/profile-image-placeholder.png') }}">
                                        <button class="browse-btn">Select a photo from computer</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row avatar-btns">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block avatar-save" id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal -->
@endif
<script type="text/javascript">
$('.select-file-area').click(function(){
    $('#avatarInput').click(); 
});
$(".options li").click(function(){
    $(this).siblings().attr('class', '');
    $(this).attr('class', '');
    var id= $(this).attr('id');
    $(this).addClass(id);
});

$(window).bind('popstate', function() {
    console.log('group-popstate');
    var sPathName = location.pathname;
    var aUrlComponents = sPathName.split('/');
    
    //var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 3] + '/' + aUrlComponents[aUrlComponents.length - 2] + '-ajax/' + aUrlComponents[aUrlComponents.length - 1];
    var sLoadUrl = siteUrl + '/' + aUrlComponents[1] + '/' + aUrlComponents[2] + '-ajax';
    
    for(var nUrlIndex = 3; nUrlIndex < aUrlComponents.length; nUrlIndex++)
    {
        sLoadUrl += '/' + aUrlComponents[nUrlIndex];
    }
    getGroupDetailPage('group_detail_main', sLoadUrl, location);
    
    //Make side bar tab selected
    makeTabSelected();
});

$(document).ready(function(){
    makeTabSelected();
});

function makeTabSelected() {
    var sPathName = location.pathname;
    var aUrlComponents = sPathName.split('/');
    
    var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 3] + '/' + aUrlComponents[aUrlComponents.length - 2] + '-ajax/' + aUrlComponents[aUrlComponents.length - 1];
    
    //Make side bar tab selected
    var sAction = aUrlComponents[aUrlComponents.length - 2];
    var aActionComponents = sAction.split('-');
    
    var oActiveTab = $('#active_'+aActionComponents[aActionComponents.length - 1]);
    oActiveTab.siblings().attr('class', '');
    oActiveTab.attr('class', '');
    oActiveTab.addClass(oActiveTab.attr('id'));
}

$('.avatar-save').click(function(e){
    var $this = $(this);
    if($('#avatarInput').val()==''){
        $('#validation-error').text('Image is required');
        $('#validation-error').css('display','block');
        return false;
    }
    else{
         $('#validation-error').css('display','none');
    }
    var $this = $('.avatar-save');
    $this.button('loading');
});
$('#crop-avatar').click(function(){
    $('.select-area').removeClass('hidden');
    $('.avatar-wrapper').addClass('hidden');
   $('.avatar-form')[0].reset(); 
   $('.avatar-alert').html('');
    $('#validation-error').css('display','none');
});
$('.show-status').click(function(){
    setTimeout(function(){
    location.reload();
    },1000);
})
</script>