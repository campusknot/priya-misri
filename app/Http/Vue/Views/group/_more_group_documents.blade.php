@if(count($oGroupDocument))
    @foreach($oGroupDocument as $oDocument)

    <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id='{{ $oDocument->id_document }}'>
        <li class="col-lg-9 col-md-7 col-sm-7  col-xs-7 document-name clearfix">
            @if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
                <a href="javascript:void(0);" onclick="getDocumentData('{{ route('group.group-child-documents-ajax',[$oGroupDetails->id_group,$oDocument->id_document]) }}','{{ route('group.group-documents',[$oGroupDetails->id_group,$oDocument->id_document]) }}');">                        
                    <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ $oDocument->document_name }}                       
                </a>
            @else
                <a target="_blank" href="{{route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" >                        
                    <?php
                    $aFileNameData = explode('.', $oDocument->file_name);

                    //Div for file icon
                    if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                    {
                        $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                    }
                    elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                    {
                        $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                    }
                    elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                    {
                        $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                    }
                    elseif (in_array(end($aFileNameData), array('zip','rar')))
                    {
                        $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                    }
                    elseif (end($aFileNameData) == 'pdf')
                    {
                        $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                    }
                    else
                    {
                        $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/image.png').'">';
                    }
                    echo $sHtmlString;
                    ?>
                    {{ $oDocument->document_name }}                       
                </a>                    
            @endif
        </li>
        <li class="col-lg-3 col-md-5 col-sm-5 col-xs-5 document-date">
            <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{  $oDocument->last_name}}">
                {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
            </span>
            <span class="pull-right doc-date">{{ $oDocument->updated_at->format('m/d/Y') }}</span>
              <span class="folder-action pull-right">
                @if($oDocument->document_type != config('constants.DOCUMENTTYPEFOLDER'))
                    <a target="_blank" href="{{route('utility.download-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" > 
                        <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.download')}}">
                    </a>
                @endif

                @if($oDocument->id_user == Auth::user()->id_user)
                    <img class="" src="{{asset('assets/web/img/doc-share-icon.png')}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});" data-toggle="tooltip" title="{{ trans('messages.share')}}">
                    <img class="doc-rename" src="{{asset('assets/web/img/doc-rename-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.rename')}}">
                    <img class="doc-copy" src="{{asset('assets/web/img/doc-copy-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.copy_tooltip')}}" onclick="copyDocument({{ $oDocument->id_document }});"> 
                    @if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                        <img class="doc-delete" src="{{asset('assets/web/img/doc-delete-icon.png')}}" onclick="deleteDocument({{ $oDocument->id_document }},'{{ trans('messages.file_delete_confirmation') }}');" data-toggle="tooltip" title="{{ trans('messages.delete')}}">
                    @endif
                @endif
            </span>

        </li>
        <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>-->
    </ul>
    @endforeach
@endif