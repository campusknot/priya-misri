<div class="modal-content add-group-popup">
    <form id="edit_group_image" name="edit_group_image" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" id="id_group" name="id_group" value="{{ $oRequest->id_group }}" />
        <div class="modal-header">
            <h4 class="modal-title event-title">{{ trans('messages.edit_group_image') }}</h4>
        </div>
        <div class="modal-body clearfix">
            
            <div class="form-group clearfix">
                <div class="col-xs-12 col-sm-10 col-md-10">
                    <input type="file" id="group_image" class="form-control" name="group_image" placeholder="{{trans('messages.group_image')}}" value="" required="required">
                    @if ($errors->has('group_image'))
                        <span class="error_message"><?php echo $errors->first('group_image'); ?></span>
                    @endif
                </div>
            </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button onclick="closeLightBox();" data-dismiss="modal" class="btn btn-default pull-right" type="button">{{ trans('messages.cancel') }}</button>
                <button type="button" onclick="submitAjaxForm('edit_group_image', '{{ route('group.edit-group-image') }}', this )" class="btn btn-primary pull-right">{{ trans('messages.upload') }}</button>
             </div>
        </div>
      
    </form>
</div>