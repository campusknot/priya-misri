<div class="{{ (Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD')) ? 'drop_doc' : '' }}"> 
    @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
    <ul class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix  docs-top-heading-bar group-docs  no-padding ">
         <li><a href="javascript:void(0);" onclick="getGroupDetailPage('group_detail_main', '{{ route('group.group-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group]) }}', '{{ route('group.group-documents', ['nIdGroup' => $oGroupDetails->id_group]) }}');">{{ trans('messages.group_documents',[ 'group'=> $oGroupDetails->group_name ]) }}</a></li>
        @if(count($aBreadCrumb))
            <?php 
                $isPermission=0;
            ?>
            @foreach($aBreadCrumb as $breadcrumb)
                @if($isPermission == 1 || $breadcrumb->permission_type != '')
                    <?php 
                        $isPermission=1; 
                    ?>
                <li><a href="javascript:void(0);" onclick="getDocumentData('{{ route('group.group-child-documents-ajax',['nIdGroup' => $oGroupDetails->id_group,'nIdDocument'=>$breadcrumb->id_document]) }}','{{ route('group.group-documents',['nIdGroup' => $oGroupDetails->id_group,'nIdDocument'=>$breadcrumb->id_document]) }}');" >{{ $breadcrumb->document_name }}</a></li>
                @endif
            @endforeach
        @endif
    </ul>
    @endif
     <form name="fileupload" id="fileupload" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="parent_id" id="parent_id" value="{{ $nIdParent }}" />
        <input type="hidden" name="shared_with" id="shared_with" value="{{ config('constants.DOCUMENTSHAREDWITHGROUP') }}" />
        <input type="hidden" name="current_permission" value="{{ (Session::has('current_permission')) ? Session::get('current_permission') : 'R'}}" />
        <input type="hidden" name="doc_type" id="doc_type" value="" />
        <input type="hidden" name="folder_name" id="fol_name" value="" />
        <input type="hidden" name="id_document" id="id_document" value="" />
        <input type="hidden" name="id_group" id="id_group" value="{{ $oGroupDetails->id_group }}" />
        <input type="file" name="file" id="file_upload" class="hide" /> 
    </form>
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="group_search" name="group_search" onsubmit="searchDocument('{{ route('group.group-documents-ajax',['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]) }}','{{ route('group.group-documents',['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]) }}','search_document','group_detail_main');return false">
            <div class="input-group docs-search">                
                <input id="search_document" type="text" class="form-control" name="search_member"  placeholder="{{ trans('messages.search_group_document') }}" value="{{ ($sSearchStr) ? $sSearchStr : '' }}" required="required">
                <button type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>              
            </div>
        </form>
    </div>
    
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 document-list_my-docs post-document">
        <ul class="my-docs_document-title clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <li class="col-lg-9 col-md-8 col-sm-8 col-xs-8">
                {{ trans('messages.name') }}
                <span class="column-sorting-options document_name" onclick="getDocumentSort('document_name','{{ route('group.group-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]) }}','group_detail_main');">
                    <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/asc.png')}}">
                    </span>
                    <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-4 col-sm-4 col-xs-4">
                <span>
                    {{ trans('messages.doc-owner') }}
                    <span class="column-sorting-options first_name" onclick="getDocumentSort('first_name','{{ route('group.group-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]) }}','group_detail_main');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
                <span class="pull-right">
                    {{ trans('messages.date') }}
                    <span class="column-sorting-options updated_at" onclick="getDocumentSort('updated_at','{{ route('group.group-documents-ajax', ['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]) }}','group_detail_main');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
            </li>
            <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{ trans('messages.size') }}</li>-->
        </ul>
        <div class="document-list-parent clearfix" id="group_documents">
            @if(!isset($nIdParent))
            <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id=''>
                <li class="col-lg-9 col-md-8 col-sm-8 col-xs-8 document-name clearfix">
                    <a href="javascript:void(0);" onclick="getDocumentData('{{ route('group.group-post-child-document-ajax',[$oGroupDetails->id_group]) }}','{{ route('group.group-post-documents',[$oGroupDetails->id_group]) }}');">                        
                        <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ trans('messages.post-document-title') }}                     
                    </a>
                </li>
                
                <li class="col-lg-3 col-md-4 col-sm-4 col-xs-4 document-date"></li>
                <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>-->
            </ul>
            @endif
            @if(count($oGroupDocument))
            
                @include('WebView::group._more_group_documents')
                
            @elseif(count($oGroupDocument)==0 && $nIdParent!= '')
                <div class="large-padding no-data">
                    <img class="" src="{{asset('assets/web/img/no-documents.png')}}">
                    {{ trans('messages.no_documents') }}
                </div>
            @endif
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        //callTabDrop('docs-top-heading-bar');
        showTooltip();
        $('.drop_doc').dropzone({ 
            headers:
            {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            url: "{{ route('documents.create-folder') }}" ,
            params : { parent_id : $('#parent_id').val(), doc_type : 'F', id_group : $('#id_group').val(),shared_with : $('#shared_with').val()},
            success: function(){
                    location.reload();
            }
        });
    });
             
    var nCurrentPage = <?php echo $oGroupDocument->currentPage(); ?>;
    var nLastPage = <?php echo $oGroupDocument->lastPage(); ?>;
    
    $(window).scroll(function (event) {
        if(window.location.href == '<?php echo route('group.group-documents', ['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]); ?>')
        {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('group_documents', '<?php echo route('group.group-documents', ['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]); ?>',nCurrentPage, '&order_by=<?php echo $sOrderBy; ?>&order_field=<?php echo $sOrderField; ?>&search_str=<?php echo $sSearchStr; ?>');
                }
            }
        }
    });
    
    $('#search_document').keyup(function(){
        var text = $('#search_document').val();
        if(text.length > 2)
        searchDocument('<?php echo route('group.group-documents-ajax',['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]); ?>','<?php echo route('group.group-documents',['nIdGroup' => $oGroupDetails->id_group,'nDocumentId' => $nIdParent]); ?>','search_document','group_detail_main');
    });
</script>