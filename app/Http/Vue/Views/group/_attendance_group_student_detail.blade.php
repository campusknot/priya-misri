<div id="atten" class="student-attendance-slider">
    <div class="attendance-lec-info clearfix">
        {{ $oLecture->group_name }} 
        @if($oLecture->section != '')
            {{ trans('messages.section') }}- {{$oLecture->section}} ({{$oLecture->semester}})
        @endif
        <br>
        <span>  {{ trans('messages.semester') }} : {{ $oLecture->semester }}</span><br/>
        <span class="">
            {{ trans('messages.total_attendance') }}
            <span class="present stu-present-indication" id="present"></span> |
            <span class="absent stu-absent-indication" id="absent"></span> |
            <span class="sick stu-sick-indication" id="sick"></span> / 
            <span class="total_lec" id="total_lec"></span>
        </span>
         <span class="attendance-notation pull-right display-ib">
                <ul class="pull-right">
                <li class="stu-present-indication">
                    {{ trans('messages.attendance_present') }}
                </li>
                <li class="stu-absent-indication">
                    {{ trans('messages.attendance_absent') }}
                </li>
                <li class="stu-sick-indication">
                    {{ trans('messages.sick_leave') }}
                </li>
            </ul>
        </span>
    </div>
    <div class="clearfix pos-relative">
        @if($oLecture->isLog)
            <span class="check-log" onclick="getUserAttendanceLog('{{ route('group.member-attendance-log') }}','{{ $oAllLacture['lecture_ids'] }}',this);">
                {{ trans('messages.log') }}
            </span>
         <div class="attendance-log-show  student-log">
                            
         </div>
        @endif
    </div>
    <div class="" id="datepicker"></div>
        <?php
        $aGroupLectures=$aAllGroupLectures=$dPresentDates=$dAbsentDtaes=$dSickDates=array();
        ?>

        @foreach($oAllLacture['lecture'] as $nKey=>$AllLecture)
        <?php
        array_push($aGroupLectures,Carbon::createFromFormat('Y-m-d H:i:s', $AllLecture->created_at)->setTimezone(Auth::user()->timezone));
        array_push($aAllGroupLectures,Carbon::createFromFormat('Y-m-d H:i:s', $AllLecture->created_at)->setTimezone(Auth::user()->timezone)->format('m/d/Y'));
        if($AllLecture->attendance_type == 'P') 
            array_push($dPresentDates,Carbon::createFromFormat('Y-m-d H:i:s', $AllLecture->created_at)->setTimezone(Auth::user()->timezone)->format('m/d/Y'));
        else if($AllLecture->attendance_type == 'SL') 
            array_push($dSickDates,Carbon::createFromFormat('Y-m-d H:i:s', $AllLecture->created_at)->setTimezone(Auth::user()->timezone)->format('m/d/Y'));
        else
            array_push($dAbsentDtaes,Carbon::createFromFormat('Y-m-d H:i:s', $AllLecture->created_at)->setTimezone(Auth::user()->timezone)->format('m/d/Y'));
        ?>
        @endforeach
        <?php
        $aUniqueGroupLecture = array_unique($aAllGroupLectures);
        $aMultiDatesKey = array();
        foreach($aUniqueGroupLecture as $aUnique)
        {
            $aMultiDatesKey[$aUnique] = array_keys($aAllGroupLectures, $aUnique);
        }
        $sCalendarDefaultDate = date("m/d/Y",max(array_map('strtotime',$aGroupLectures)));
        $aAllGroupLecture = array_count_values($aAllGroupLectures);
        $aCheckMultiLecture = array_filter(
            $aAllGroupLecture,
            function ($value) {
                return ($value >1);
            }
        );

        //total count of present absent and sick
        $nPresentCount = count($dPresentDates);
        $nAbsentCount = count($dAbsentDtaes);
        $nSickCount = count($dSickDates);
        
        //if there is same count of lecture->array_flip is not use bcz key take same value
        $aMultiLecture = array();
        foreach($aCheckMultiLecture as $key => $value){
            $aMultiLecture[]= $key;
        }
        $dPresentDates = array_diff($dPresentDates, $aMultiLecture);
        $dAbsentDtaes = array_diff($dAbsentDtaes, $aMultiLecture);
        $dSickDates = array_diff($dSickDates, $aMultiLecture);
        ?>
        @foreach($aUniqueGroupLecture as $aLec)
            @if(count($aMultiDatesKey[$aLec]) > 1)
                <div class="{!! str_replace('/', '', $aLec) !!} hidden show-multi-lec">
                    <div class="lec-wrapper">
                        <h4>
                            <?php 
                            $dCreateDate = Carbon::createFromFormat('Y-m-d H:i:s', $aGroupLectures[$aMultiDatesKey[$aLec][0]]);
                            //$dCreateDate = $dCreateDate->setTimezone(Auth::user()->timezone);
                            ?>
                            
                            {{ $dCreateDate->format('M d, Y') }}
                            <button type="button"  class=" hideData close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </h4>
                        <ul class="lec-data">
                            <ul class="headers clearfix">
                                <li class="display-ib col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">{{trans('messages.time')}}</li>
                                <li class="display-ib col-lg-6 col-md-6 col-sm-6 col-xs-6">{{trans('messages.attendance')}}</li>
                            </ul>    

                        @foreach($aMultiDatesKey[$aLec] as $nKeyValue)
                            <?php 
                            $dCreateTime = Carbon::createFromFormat('Y-m-d H:i:s', $oAllLacture['lecture'][$nKeyValue]->created_at);
                            $dCreateTime = $dCreateTime->setTimezone(Auth::user()->timezone)->format('h:i A');
                            if($oAllLacture['lecture'][$nKeyValue]->attendance_type == '')
                                $sClass = "stu-A";
                            else
                                $sClass = "stu-".$oAllLacture['lecture'][$nKeyValue]->attendance_type;
                            ?>
                            <ul class="lec-count-per-day clearfix m-10">
                                <li class="display-ib col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">{{ $dCreateTime }}</li>
                                <li class="display-ib col-lg-6 col-md-6 col-sm-6 col-xs-6 {{ $sClass }}">
                                    @if($oAllLacture['lecture'][$nKeyValue]->attendance_type == config('constants.ATTENDANCETYPEPRESENT'))
                                        {{ trans('messages.present_short') }}
                                    @elseif($oAllLacture['lecture'][$nKeyValue]->attendance_type == config('constants.ATTENDANCETYPESICKLEAVE'))
                                        {{ trans('messages.sick_leave_short') }}
                                    @else
                                        {{ trans('messages.absent_short') }}
                                    @endif
                                </li>
                            </ul>
                        @endforeach
                        </ul>
                    </div>
                    
                    
                </div>
            @endif
            
        @endforeach
    <script>
    $(document).ready(function(){
    

        $('.hideData').click(function(){
            $(this).parents('.show-multi-lec').addClass('hidden');
        });
        var jPresentArray= <?php echo json_encode($dPresentDates); ?>;
        var jAbsentArray= <?php echo json_encode($dAbsentDtaes); ?>;
        var jSickArray= <?php echo json_encode($dSickDates); ?>;
        //array of dates multiple lecture
        var jMultiLectureArray = <?php echo json_encode($aMultiLecture); ?>;
        var jLectureCount= <?php echo count($aAllGroupLectures); ?>;
        var PresenDates = {};
        var AbsentDates = {};
        var SickDates = {};
        var MultiLectureDates = {};
        $( "#present" ).text(<?php echo $nPresentCount; ?>);
        $( "#sick" ).text(<?php echo $nSickCount; ?>);
        $( "#absent" ).text(<?php echo $nAbsentCount; ?>);
        $( "#total_lec" ).text(jLectureCount);
        
        jQuery.each(jMultiLectureArray, function(key, obj) {
            MultiLectureDates[ new Date(obj)] = obj;
        });
        jQuery.each(jPresentArray, function(key, obj) {
            PresenDates[ new Date(obj)] = obj;
        });
        jQuery.each(jAbsentArray, function(key, obj) {
            AbsentDates[ new Date(obj)] = obj;
        });
        jQuery.each(jSickArray, function(key, obj) {
            SickDates[ new Date(obj)] = obj;
        });
        

        $( "#datepicker" ).datepicker({
            changeYear: true,
            changeMonth: true,
            defaultDate: '<?php echo $sCalendarDefaultDate; ?>',
            beforeShowDay: function( date ) {
                var present = PresenDates[date];
                var absent = AbsentDates[date];
                var sick = SickDates[date];
                var multi = MultiLectureDates[date];
                if( present ) {
                     return [true, "stu-present", 'Tooltip text'];
                } else if( absent ){
                    return [true, "stu-absent", 'Tooltip text'];
                } else if( sick ){
                    return [true, "stu-sick", 'Tooltip text'];
                }
                else if( multi ){
                    return [true, "has-multi-lec", 'Tooltip text'];
                }
                else
                {
                     return [true, '', ''];
                }
            },
            beforeShow : checkshow,
            onSelect: function (date) {
                var cDate = date.replace(/\//g,'');
                $('.'+cDate).removeClass('hidden');
                // Your CSS changes, just in case you still need them
                $('a.ui-state-default').removeClass('ui-state-active');
                $(this).removeClass('ui-state-active');
                checkshow();
            },
            onChangeMonthYear: checkshow
         });
    checkshow();
    });

    function checkshow() {
        setTimeout(function() {
            $('.has-multi-lec').each(function () {
                var m= parseInt($(this).attr('data-month'))+1;
                var mnth= ('0' + m).slice(-2);
                var y= $(this).attr('data-year');
                var d= $(this).children('a').text();
                var day= ('0' + d).slice(-2);
                var classDay = mnth+day+y;
                var count = $('.'+classDay).find('.lec-count-per-day').length;
                $(this).append('<span class='+'lec-count'+'>'+count+'</span>');//.children('a')
            });
        },200);  
    }
    </script>
</div>