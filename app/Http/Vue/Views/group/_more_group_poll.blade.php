@foreach($oGroupFeeds as $oFeed)
<div id="{{ $oFeed->id_post }}" class="post">             
    <div class=" clearfix">
        <!-- Div for creator image -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-top-section">
            <div class="icon-image img-50">
                <a href="{{route('user.profile',[$oFeed->id_user])}}">
                    {!! setProfileImage("50",$oFeed->user_profile_image,$oFeed->id_user,$oFeed->first_name,$oFeed->last_name) !!}
                
                </a>
            </div>
            <div class="post-person-name">
                <a href="{{route('user.profile',[$oFeed->id_user])}}">{{ $oFeed->first_name.' '.$oFeed->last_name}}</a>
                <span class="post-time">{{ secondsToTime($oFeed->created_at) }} 
                    @if($oFeed->group_name != NULL){{' |'}} 
                        <a href="{{ route('group.group-feeds', ['nIdGroup' => $oFeed->id_group]) }}">
                            <span class="group-name">{{$oFeed->group_name }}</span>
                        </a>  
                    @endif
                </span>
                @if(count($oGroupMemberDetail) && ($oFeed->id_user == Auth::user()->id_user || ( $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR') || $oGroupMemberDetail->member_type == config('constants.GROUPADMIN'))))
                <span class="edit-links">
                    <img src="{{asset('assets/web/img/dropdown-icon.png')}}" class="post-dropdown" alt="dropdown icon" data-toggle="dropdown" />
                    <ul class="dropdown-menu report-post">
                            <li onclick="deletePost('{{trans('messages.poll_delete_confirmation')}}','{{$oFeed->id_post}}');">{{ trans('messages.delete') }}</li>
                    </ul>
                </span>
                @endif
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-10">
            <div class="post-content">
                <div class="post-poll">
                    @if(strtotime($oFeed->end_time) >= time())
                     <div class="poll-time">{{ trans('messages.poll_time_left') }}
                         <div class="left_time" >{{ secondsToTime($oFeed->end_time,1) }}</div>
                         <span class="hidden left-time"></span> 
                         <input type="text" class="minute hidden" value="{{ TimeDiffinSec($oFeed->end_time) }}" disabled />
                     </div>
                    @else
                         <div class="poll-time poll-close">{{ trans('messages.poll_closed') }}</div>
                     @endif
                     <div class="post-poll-question">
                         <p>{!! setTextHtml($oFeed->poll_text) !!}</p>
                         @if($oFeed->poll_file_name)
                         <div class="post-poll-question-img">
                             <img src="{{ setPostImage($oFeed->poll_file_name,80) }}" onclick="showProfileImageLightBox('{{ setPostImage($oFeed->poll_file_name,80) }}');">
                         </div>
                         @endif
                     </div>
                     <form id="poll_{{$oFeed->id_poll}}" action="{{ route('post.add-poll-answer') }}" method="POST">
                         {!! csrf_field() !!}
                         <input type="hidden" name="id_poll" value="{{$oFeed->id_poll}}" >
                         <input type="hidden" name="id_post" value="{{$oFeed->id_post}}" >
                         <input type="hidden" name="poll_type" value="{{$oFeed->poll_type}}" >
                         <input type="hidden" name="expired" value="0" >
                         <span class="error_message" id="id_poll_option_{{ $oFeed->id_post }}"></span>
                         @if($oFeed->poll_type == config('constants.POLLTYPEMULTIPLE') && count($oFeed->polloption) > 0)
                         <div class="post-poll-options clearfix">
                             <?php $sChar = 'A';
                             $sDisable='';
                             if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time() || $oGroupMemberDetail->member_type == NULL)
                             $sDisable='disabled';
                             ?>
                             @foreach($oFeed->polloption as $option)
                                 <?php
                                     $sSelected='';
                                     if($oFeed->id_poll_option && $option->id_poll_option==$oFeed->id_poll_option)
                                         $sSelected="checked";
                                 ?>

                                 <div class="radio radio-primary clearfix">
                                     <input type="radio" name="id_poll_option_{{ $oFeed->id_post }}" id="option{{$option->id_poll_option}}" value="{{$option->id_poll_option}}" {{ $sSelected }} {{$sDisable}} />
                                     <label for="option{{$option->id_poll_option}}" class="">
                                         <span class="poll-option-tag">{{ $sChar }}</span>
                                         <span class="poll-option-text">{{$option->option_text}} </span>
                                         @if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time())
                                         <div class="poll-result">
                                             <span><p>
                                                 @if($oFeed['pollanswer_'.$option->id_poll_option])
                                                     {{$oFeed['pollanswer_'.$option->id_poll_option]}} %
                                                 @else
                                                     0 %
                                                 @endif
                                             </p></span>
                                          </div>
                                         @endif
                                     </label>

                                 </div>
                             <?php $sChar++; ?>
                             @endforeach
                         </div>
                        @elseif($oFeed->poll_type == config('constants.POLLTYPEOPEN'))
                        <div class="post-poll-options mb-10 clearfix">
                            @if(empty($oFeed->id_poll_answer) && strtotime($oFeed->end_time) > time())
                            <textarea autocomplete="off" class="input form-control animated" i  name="poll_answer_text" type="text" placeholder="{{ trans('messages.type_your_answer') }}"  maxlength="255" ></textarea>
                            @else
                                @if(!empty($oFeed->poll_answer_description))
                                    <div class="show-poll-short-answer">{{ $oFeed->poll_answer_description }}</div>
                                @else
                                    <div class="show-poll-short-answer no-poll-answer">{{ trans('messages.no_poll_answer') }}</div>
                                @endif
                            
                            @endif
                        </div>
                        @endif
                         @if(strtotime($oFeed->start_time) < time() && $oFeed->id_poll_answer =='' && strtotime($oFeed->end_time) >= time() && count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
                         <button type="button" class="btn btn-primary btn-md btn-poll" data-id='{{$oFeed->id_poll}}' id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >Save</button>
                         @endif
                     </form>
                     <div class="pull-right view-poll-answer">

                         @if($oFeed->id_user == Auth::user()->id_user && $oFeed->totalAns!=0)
                             <a href="javascript:void(0);" onclick="showPollDetail({{ $oFeed->id_post }});" class="">
                             @if($oFeed->totalAns == 0)
                             <span>{{ trans('messages.vote') }}</span>
                             @endif
                         @endif

                         @if($oFeed->totalAns == 1)
                             <span>{{ $oFeed->totalAns }} {{ trans('messages.vote') }} </span>
                         @elseif($oFeed->totalAns > 1)
                             <span>{{ $oFeed->totalAns }} {{ trans('messages.votes') }} </span>
                         @endif

                         @if($oFeed->id_user == Auth::user()->id_user && $oFeed->totalAns!=0)
                         </a>
                         @endif

                     </div>
                 </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<script type="text/javascript">
$(document).ready(function() {
    setAutosizeTextAreaHeight(34);
    callAutoSizeTextArea();
});
$('.report').on('click', function(evt) {
            evt.stopImmediatePropagation();
            $(this).parent().addClass("containsForm");
            
       });
       $('.remove-form').on('click', function(evt) {
            evt.stopImmediatePropagation();
            $('.report-error').remove();
            $(this).parents(".report-post").removeClass("containsForm");
            $(this).parents(".edit-links").removeClass("open");
            
       });
</script>