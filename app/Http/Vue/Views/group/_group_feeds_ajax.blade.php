@if(empty($oGroupDetails))
    <!-- Filename::group._group_feeds_ajax  -->
    <div class="ck-background-white clearfix">
        <div class="deleted-data">
            <img class="" src="{{asset('assets/web/img/deleted_group.png')}}">
            {{ trans('messages.group_deleted') }}
        </div>
        
    </div>
    @else
    <div class="clearfix tabbable pos-relative">
        <!-- group tabs -->
        <ul class="nav nav-tabs group-tabs group-tabs-dropdown ">
            <li class="active"> <a href="{{ url('/') }}" class="mightOverflow" data-toggle="tooltip"   data-placement="top" title="" nowrap>All</a></li>
            @foreach($oGroupListTab as $oGroup)
                <li id="groupId_{{$oGroup->id_group}}" class="group-{{$oGroup->group_type}}" data-toggle="tooltip"   data-placement="top" title="{{$oGroup->group_name}}
                    {{ ($oGroup->section != '') ? trans('messages.section').'-'.$oGroup->section.'('.$oGroup->semester.')' : '' }}"   nowrap>
                    <a href="{{ route('group.group-feeds', ['nIdGroup' => $oGroup->id_group]) }}" class="mightOverflow show-tooltip">
                        {{$oGroup->group_name}}
                        @if($oGroup->section != '')
                            {{ trans('messages.section') }}- {{$oGroup->section}} ({{$oGroup->semester}})
                        @endif
                    </a>
                </li>
            @endforeach
        </ul>
        
        @if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
            <div class="ck-background-white clearfix">
                <div class="no-data">
                    <img src="{{asset('assets/web/img/private-group.png')}}">
                    <div>{{ $oGroupDetails->about_group }}</div>
                    <div class="sml-text">
                        @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                            {{ trans('messages.private_group_member') }}
                        @else
                            {{ trans('messages.secret_group_member') }}
                        @endif
                    </div>
                </div>
            </div>
        @else
        
            <div class="share-bg padding-10 clearfix">
                @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
                    @include('WebView::post._add_post')
                @endif
            </div>
            @if(count($oGroupDetails))
                <div class="tab-content">       
                    <div id="group_feed" class="clearfix feeds">
                        @include('WebView::group._more_group_feeds')
                    </div>
                    <div class="more-data-loader hidden">
                        <div class="spinner">
                            <div class="bounce1"></div>
                            <div class="bounce2"></div>
                            <div class="bounce3"></div>
                        </div>
                    </div>
                </div>
            @else
                <div class="col-lg-12 no-data-found m-t-2 background-white round-coreners-8">
                    <h2> {{ trans('messages.no_record_found') }} </h2>
                </div>
            @endif
        @endif
    </div>
    @endif
    <script type="text/javascript">
        $(function(){
            setAutosizeTextAreaHeight(30);
            callAutoSizeTextArea();
        });
        var groupId =  window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
        $('.group-tabs li').removeClass('active');
        $('#groupId_'+groupId).addClass('active');
        var index=$('#groupId_'+groupId).index();
        if(index >= 3){
            var other = $('#groupId_'+groupId);
            $(".group-tabs li:eq(2)").after(other.clone());
            other.after($(".group-tabs li:eq(2)")).remove();
        }
        $('.group-tabs-dropdown').tabdrop();
        $(".group-tabs-dropdown").css("overflow","visible");
        
        $.fn.exchangePositionWith = function(selector) {
            var other = $(selector);
            this.after(other.clone());
            other.after(this).remove();
        };
        $(document).on('fileselect','.file-attachment-btn', function(event, numFiles, label) {
            showCustomBrowseButton(event, numFiles, label, this);
        });
        $(document).load(function(){
            $('.group-tabs-dropdown').tabdrop();
            var groupId =  window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
            $('.group-tabs li').removeClass('active');
            $('#groupId_'+groupId).addClass('active');
            var index=$('#groupId_'+groupId).index();
            if(index >= 3){
                $(".group-tabs li:eq(2)").exchangePositionWith('#groupId_'+groupId);
            }
            
            findUrlThumbnails();
        });
    $(document).ready(function(){
        $(document).on('click',".comment-attchment label:first-child",function(){
              $(this).parent().siblings("#file-input").addClass("show-input");
              $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
              $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any document ");
              
        });
        $(document).on('click','.comment-attchment label:last-child',function(){
            var id='file_'+$(this).attr('data-id');
            $(this).parent().siblings("#file-input").addClass("show-input");
            $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
            $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any photo ");
              
        });
        $(document).on('click','.file-attachment-cancel',function(){
            var id = $(this).attr('data-id');
            $('#file_'+id).val('').clone(true);
            $(this).parent().removeClass("show-input");
            input = $(".file-attachment-name").val('');
        });
        
        $(document).on('fileselect','.file-attachment-btn', function(event, numFiles, label) {
                showCustomBrowseButton(event, numFiles, label, this);
            });
        findUrlThumbnails();
    });
    $(document).ready(function($) 
    {
        var options = { 
            beforeSubmit:  showRequestComment,
            success:       showResponseComment,
            error:showErrorComment,
            dataType: 'json' 
        }; 

        $(document).on('click','.btn-comment',function(e){
            var sBrowserUrl = window.location.href;
            if(sBrowserUrl.indexOf("group/group-feeds") > 0){
                e.preventDefault();
                var data=$(this);
                var id_post=data.attr('data-id');
                $(this).button('loading');
                $('#add_comment_'+id_post).ajaxForm(options).submit(); 
                }
        });

        $(document).on('click','.btn-poll',function(e){
            var sBrowserUrl = window.location.href;
            if(sBrowserUrl.indexOf("group/group-feeds") > 0){
                var data = $(this);
                var id_poll = data.attr('data-id');
                $(this).button('loading');
                $('#poll_'+id_poll).ajaxForm(options).submit();
                }
        });
    });	

    function showRequestComment(formData, jqForm, options) 
    { 
            $("#validation-errors").hide().empty();
            $("#output").css('display','none');
            return true; 
    } 
    function showResponseComment(response, statusText, xhr, $form) 
    { 
        if(response.success == true)
        {
            if(response.type=='poll'){
                $('#'+response.id_post).html(response.html);
            }
            else
            {
                $('#add_comment_'+ response.id_post)[0].reset();
                var count = $('#count_'+ response.id_post).text();
                $('#count_'+ response.id_post).text(parseInt(count)+1);
                $('#comment-box-'+ response.id_post).removeClass('hidden');
                $('#ext_comments_'+response.id_post).append(response.html);
            }
            $('.btn-md').button('reset');
            removeErrorComment(response.id_post);
        } else { 
            var arr = response;
            $.each(arr, function(index, value)
            {
                    if (value.length != 0)
                    {
                            $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                    }
            });
            $("#validation-errors").show();
           $('.post-btn').button('reset');
        }
        setAutosizeTextAreaHeight(30);
    }
    function showErrorComment(xhr, textStatus, errorThrown)  
    {
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
               //location.reload();
            }

        var result = $.parseJSON(arr);
        $('.btn-md').button('reset');
        $.each(result, function(key, value)
        {
            key=key.replace('file','comment_text');
            key=key.replace('extention','comment_text');
            if (key.length != 0)
            {
                    $("#"+key).html('<strong>'+ value +'</strong>');
            }
        });
    }

    function removeErrorComment(id)
    {
        console.log('removeErrorComment');
        $("#comment_text_"+id).html('');
    }

<?php
    if(count($oGroupDetails))
    {
?>
        var nCurrentPage = <?php echo $oGroupFeeds->currentPage(); ?>;
        var nLastPage = <?php echo $oGroupFeeds->lastPage(); ?>;

        $(window).scroll(function (event) {
            if(window.location.href == '<?php echo route('group.group-feeds', ['nIdGroup' => $oGroupDetails->id_group]); ?>')
            {
                if($(window).scrollTop() + $(window).height() == $(document).height()) {
                    if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                        nCurrentPage +=1;
                        loadNewData('group_feed', '<?php echo route('group.group-feeds', ['nIdGroup' => $oGroupDetails->id_group]); ?>', nCurrentPage);
                    }
                }
            }
        });
        setInterval(function() {
            $(".minute").each(function(){
                var ele=$(this);
                var myTime = ele.val()-1;

                if(myTime <= 300 ){
                    var MinSec=getTime(myTime);
                    ele.siblings('span').text(MinSec);
                    ele.siblings('span').removeClass('hidden');
                    ele.siblings('div').addClass('hidden');
                }
                ele.val(myTime);

                if(myTime <= 0){
                    ele.parent().addClass('hidden');
                    var parent=ele.parent().parent().children('form');
                    parent.children('input[name=expired]').val(1);
                    parent.children('button').trigger( "click" );

                }
            });
        },  1000);
<?php
    }
?>
</script>