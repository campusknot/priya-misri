<div class="modal-content edit-information"  id="edit_work_experience">
    <div class="modal-header">
        <div class="modal-header-img bg-work" >
            <img src="{{ asset('assets/web/img/work.png') }}" alt="" class="" />
        </div>
        {{ trans('messages.work_experience') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdWorkexperience  = isset($oUserWorkExperience->id_user_work_experience) ? $oUserWorkExperience->id_user_work_experience : old('id_workexperience');
        $sCompanyName       = isset($oUserWorkExperience->company_name) ? $oUserWorkExperience->company_name : old('company_name');
        $sJobTitle          = isset($oUserWorkExperience->job_title) ? $oUserWorkExperience->job_title : old('job_title');
        $sJobDescription    = isset($oUserWorkExperience->job_description) ? $oUserWorkExperience->job_description : old('job_description');
        $nStartYear         = isset($oUserWorkExperience->start_year) ? $oUserWorkExperience->start_year : old('start_year');
        $nStartMonth        = isset($oUserWorkExperience->start_month) ? $oUserWorkExperience->start_month : old('start_month');
        $nEndYear           = isset($oUserWorkExperience->end_year) ? $oUserWorkExperience->end_year : old('end_year');
        $nEndMonth          = isset($oUserWorkExperience->end_month) ? $oUserWorkExperience->end_month : old('end_month');
        $isPresent = '';
        if( $nIdWorkexperience != '' && empty($nEndYear) && !$errors->has('end_year') && !$errors->has('end_month')) {
            $isPresent = "checked='checked'";
        }
    ?>
    <div class="modal-body clearfix">
        <form id="add_workexperience_form" class="center" enctype="multipart/form-data">
            <input id="id_workexperience" name="id_workexperience" type="hidden" value="{{$nIdWorkexperience}}" >
            {!! csrf_field() !!}
            <div class="col-lg-12">
                <input id="company_name" class="form-control {{ ($errors->has('company_name')) ? 'error':'' }}"
                       type="text" placeholder="{{ trans('messages.company_name') }}"  
                       title="{{ ($errors->has('company_name')) ? $errors->first('company_name'):'' }}" data-toggle="tooltip"  data-placement="top" 
                       name="company_name" value="{{$sCompanyName}}">
            </div>
            <div class="col-lg-12">
                <input id="job_title" class="form-control {{ ($errors->has('job_title')) ? 'error': ''}}"
                       type="text" placeholder="{{ trans('messages.job_title') }}" 
                       title="{{ ($errors->has('job_title')) ? $errors->first('job_title') : ''}}" data-toggle="tooltip"  data-placement="top" 
                       name="job_title" value="{{$sJobTitle}}">
            </div>
            <div class="col-lg-12">
                <textarea id="job_description" class="form-control {{ ($errors->has('job_description')) ? 'error' :'' }}" placeholder="{{ trans('messages.job_description') }}" title="{{ ($errors->has('job_description')) ? $errors->first('job_description') :'' }}" data-toggle="tooltip"  data-placement="top" name="job_description">{{$sJobDescription}}</textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="date-select  pull-left {{ ($errors->has('start_month') || $errors->has('start_year')) ? 'error' : '' }}" >
                    <div class="date-month" title="{{ ($errors->has('start_month')) ? $errors->first('start_month') : '' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="start_month" class="form-control" name="start_month" required>
                            <option value="">{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nStartMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nStartMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nStartMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nStartMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nStartMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nStartMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nStartMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nStartMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nStartMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nStartMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nStartMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nStartMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input class="form-control"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('start_year')) ? $errors->first('start_year') : '' }}" data-toggle="tooltip"  data-placement="top" 
                           name="start_year" value="{{ $nStartYear }}" onkeypress="return isNumberKey(event);" maxlength="4">
                </div>
                <span class="dash-span"> - </span>
                <span class="company-present-text">{{ trans('messages.present') }}</span>
                <div class="company-end-date date-select  pull-right {{($errors->has('end_month') || $errors->has('end_year')) ? 'error' : '' }}" 
                         >
                    <div class="date-month" title="{{($errors->has('end_month')) ? $errors->first('end_month') : '' }}" data-toggle="tooltip" data-placement="top">
                        <span class="caret"></span>
                        <select id="end_month" class="form-control" name="end_month" required>
                            <option value="">End Month</option>
                            <option value='1' <?php echo $nEndMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nEndMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nEndMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nEndMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nEndMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nEndMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nEndMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nEndMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nEndMonth==9?"selected":"" ?>>September</option>
                            <option value='10'<?php echo $nEndMonth==10?"selected":"" ?>>October</option>
                            <option value='11'<?php echo $nEndMonth==11?"selected":"" ?>>November</option>
                            <option value='12'<?php echo $nEndMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="end_year" class="form-control"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('end_year')) ? $errors->first('end_year') :'' }}" data-toggle="tooltip"  data-placement="top" 
                           name="end_year" value="{{ $nEndYear }}" onkeypress="return isNumberKey(event);" maxlength="4">
                </div>
                <div class="date-error">{{ ($errors->has('end_date')) ? $errors->first('end_date') : '' }}</div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left">
                <div class="pull-left present">
                    <input id="present" class="form-control" name="present" value="1" type="checkbox" {{$isPresent}}>&nbsp;&nbsp;Present
                </div>
            </div>
            <div class="col-lg-12 submit-options">
                <button class="btn btn-default pull-right" type="button" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="we_save_btn_aii" class="btn btn-primary pull-right" name="we_save_btn_aii" type="button" value="{{ empty($nIdWorkexperience) ? trans('messages.save') : trans('messages.update') }}" onclick="submitAjaxForm('add_workexperience_form', '{{ route('user.add-workexperience') }}', this )" >
            </div>
        </form>
    </div>
</div>
<script>
    $("#present").click(function(){
        $(".company-end-date").toggleClass("hide");
        $(".company-present-text").toggleClass("show");
    });
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        
        var isPresent = "<?php echo $isPresent; ?>";
            if(isPresent != ''){
                $(".company-end-date").toggleClass("hide");
                $(".company-present-text").toggleClass("show");
            }
    });
</script>