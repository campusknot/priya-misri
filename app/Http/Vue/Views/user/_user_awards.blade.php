<div class="modal-content edit-information" id="edit_awards" >
    <div class="modal-header">
        <div class="modal-header-img bg-award" >
            <img src="{{ asset('assets/web/img/awards.png') }}" alt="" class="" />
        </div>
        {{ trans('messages.awards_honors') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdAward = isset($oUserAward->id_user_award) ? $oUserAward->id_user_award : old('id_award_hdn');
        $sAwardTitle = isset($oUserAward->award_title) ? $oUserAward->award_title : old('award_title');
        $sAwardIssuer = isset($oUserAward->award_issuer) ? $oUserAward->award_issuer : old('award_issuer');
        $sAwardDescription = isset($oUserAward->award_description) ? $oUserAward->award_description : old('award_description');
        $nAwardedYear = isset($oUserAward->awarded_year) ? $oUserAward->awarded_year : old('awarded_year');
        $nAwardedMonth = isset($oUserAward->awarded_month) ? $oUserAward->awarded_month : old('awarded_month');
    ?>

    <div class="modal-body clearfix">
        <form id="add_award_form" class="center">
            {!! csrf_field() !!}
            <input id="id_award" name="id_award" type="hidden" value="{{$nIdAward}}" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="award_title" class="form-control {{($errors->has('award_title')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.award_title') }}"
                       title="{{($errors->has('award_title')) ? $errors->first('award_title') : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="award_title" value="{{$sAwardTitle}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="award_issuer" class="form-control {{($errors->has('award_issuer')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.award_issuer') }}"
                       title="{{($errors->has('award_issuer')) ? $errors->first('award_issuer') : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="award_issuer" value="{{$sAwardIssuer}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <textarea id="award_description" class="form-control {{($errors->has('award_description')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.award_description') }}"
                       title="{{($errors->has('award_description')) ? $errors->first('award_description') : '' }}" data-toggle="tooltip" data-placement="top" 
                       name="award_description" value="{{$sAwardDescription}}"></textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left">
                <span class="select-lable">{{ trans('messages.time_of_winning') }}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="date-select  pull-left {{($errors->has('awarded_month') || $errors->has('awarded_year')) ? 'error': '' }}" >
                    <div class="date-month " title="{{($errors->has('awarded_month')) ? $errors->first('awarded_month'): '' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="awarded_month" class="form-control" name="awarded_month" required>
                            <option value="" >{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nAwardedMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nAwardedMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nAwardedMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nAwardedMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nAwardedMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nAwardedMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nAwardedMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nAwardedMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nAwardedMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nAwardedMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nAwardedMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nAwardedMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="awarded_year" class="form-control {{ ($errors->has('awarded_year')) ?$errors->first('awarded_year')  : '' }}"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{ ($errors->has('awarded_year')) ?$errors->first('awarded_year')  : '' }}" data-toggle="tooltip"  data-placement="top" 
                           name="awarded_year" value="{{$nAwardedYear}}" onkeypress="return isNumberKey(event);"  maxlength="4">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="ah_save_btn_aii" class="btn btn-primary pull-right" name="ah_save_btn_aii" type="button" value="{{ empty($nIdAward) ? trans('messages.save') : trans('messages.update') }}" onclick="submitAjaxForm('add_award_form', '{{ route('user.add-award') }}', this )">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });    
</script>