<div class="modal-content edit-information"  id="edit_education">
    <div class="modal-header">
        <div class="modal-header-img bg-education" >
            <img src="{{ asset('assets/web/img/education.png') }}" alt="" class="" />
        </div>

        {{ trans('messages.education') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdEducation     = isset($oUserEducation->id_user_education) ? $oUserEducation->id_user_education : old('id_education');
        $sUniversity     = isset($oUserEducation->university) ? $oUserEducation->university: old('university');
        $sDegree         = isset($oUserEducation->degree) ? $oUserEducation->degree : old('degree');
        $sMajor          = isset($oUserEducation->major) ? $oUserEducation->major : old('major');
        $sClassification = isset($oUserEducation->classification) ? $oUserEducation->classification : old('classification');
        $nStartYear     = isset($oUserEducation->start_year) ? $oUserEducation->start_year : old('start_year');
        $nStartMonth    = isset($oUserEducation->start_month) ? $oUserEducation->start_month : old('start_month');
        $nEndYear       = isset($oUserEducation->end_year) ? $oUserEducation->end_year : old('end_year');
        $nEndMonth      = isset($oUserEducation->end_month) ? $oUserEducation->end_month : old('end_month');
        $sIsPresent = '';
        //error condition add for when update record null end yr nd null present they default select present and not show error of end yr
        if(!empty($nIdEducation) && empty($nEndYear) && !$errors->has('end_year') && !$errors->has('end_month')) {
            $sIsPresent = "checked='checked'";
        }
    ?>
    <div class="modal-body clearfix">
        <form id="add_education_form" name="add_education_form" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input id="id_education" name="id_education" type="hidden" value="{{$nIdEducation}}" >
            <div class="col-lg-12">
                <input id="university" class="form-control {{ ($errors->has('university')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.university') }}"
                       title="{{ ($errors->has('university')) ? $errors->first('university') : '' }}" data-toggle="tooltip" data-placemet="top" 
                       name="university" value="{{$sUniversity}}">
              
            </div>
            <div class="col-lg-12">
                <input id="degree" class="form-control {{ ($errors->has('degree')) ? 'error': '' }}"
                       type="text" placeholder="{{ trans('messages.degree') }}" 
                       title="{{ ($errors->has('degree')) ? $errors->first('degree') : '' }}" data-toggle="tooltip" data-placement="top"  
                       name="degree" value="{{$sDegree}}">
            </div>
            <div class="col-lg-12">
                <input id="major" class="form-control {{ ($errors->has('major')) ? 'error':'' }}"
                       type="text" placeholder="{{ trans('messages.major') }}" 
                       title="{{ ($errors->has('major')) ? $errors->first('major') :'' }}" data-toggle="tooltip" data-placement="top"
                       name="major" value="{{ $sMajor }}" >
            </div>
            <div class="col-lg-12">
                <input id="classification" class="form-control"
                       type="text" placeholder="{{ trans('messages.classification') }}" 
                       name="classification"  value="{{ $sClassification }}">
            </div>
            <div class="col-lg-12 left">
                <span class="select-lable">{{ trans('messages.date_of_attendance') }}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"> 
                <div class="date-select  pull-left {{ ($errors->has('start_month') || $errors->has('start_year')) ? 'error' : '' }}" >
                    <div class="date-month" title="{{($errors->has('start_month')) ? $errors->first('start_month') : '' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="start_month" class="form-control" name="start_month" required>
                            <option value="">{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nStartMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nStartMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nStartMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nStartMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nStartMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nStartMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nStartMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nStartMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nStartMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nStartMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nStartMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nStartMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="id_edu_e_start_year" class="form-control {{($errors->has('start_year')) ? 'error': '' }}"
                           type="text"  placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('start_year')) ? $errors->first('start_year'): '' }}" data-toggle="tooltip"  data-placement="top" 
                           name="start_year" value="{{ $nStartYear }}" onkeypress="return isNumberKey(event);" >
                </div>
                <span class="dash-span"> - </span>
                <span class="education-present-text">{{ trans('messages.present') }}</span>
                <div class="education-end-date date-select pull-right {{($errors->has('end_month') || $errors->has('end_year')) ? 'error': '' }}" >
                    <div class="date-month " title="{{($errors->has('end_month')) ? $errors->first('end_month'): '' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select required class="form-control" id="id_edu_e_end_month" name="end_month">
                            <option value="">{{ trans('messages.end_month') }}</option>
                            <option value='1' <?php echo $nEndMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nEndMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nEndMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nEndMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nEndMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nEndMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nEndMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nEndMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nEndMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nEndMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nEndMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nEndMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="end_year" class="form-control {{ ($errors->has('end_year')) ? 'error':'' }}"
                           type="text" placeholder="{{ trans('messages.year') }}"  
                           title="{{ ($errors->has('end_year')) ? $errors->first('end_year'):'' }}" data-toggle="tooltip"  data-placement="top"
                           name="end_year" value="{{ $nEndYear }}" onkeypress="return isNumberKey(event);">
                </div>
                <div class="date-error">
                    {{ ($errors->has('end_date')) ? $errors->first('end_date') : '' }}
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="pull-left present">
                    <input id="is_present" name="present" class="form-control" value="1" style="margin-top: -1px;" type="checkbox" {{$sIsPresent}}>&nbsp;&nbsp;Present
                </div>
            </div>
            <div class="col-lg-12 submit-options">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="add_education_button" class="btn btn-primary pull-right" type="button" name="add_education_button" value="{{ empty($nIdEducation) ? trans('messages.save') : trans('messages.update') }}" onclick="submitAjaxForm('add_education_form', '{{ route('user.add-education') }}', this )">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("#is_present").click(function(){
        $(".education-end-date").toggleClass("hide");
        $(".education-present-text").toggleClass("show");
    });
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        
        var isPresent = "<?php echo $sIsPresent; ?>";
        if(isPresent != ''){
            $(".education-end-date").toggleClass("hide");
            $(".education-present-text").toggleClass("show");
        }
    });
</script>