<div id="polls" class="clearfix feeds">
    @if(count($oUserFeeds))
        @include('WebView::user._more_poll')
    @else
    <div class="ck-background-white">
        <div class="no-data ">
            <img src="{{asset('assets/web/img/no-poll.png')}}">
            <div class="sml-text">
                 {{ trans('messages.no_user_poll') }}
            </div>
        </div>
    </div>
    @endif
</div>
<script type="text/javascript">
    var nCurrentPage = <?php echo isset($aPaginator['current_page']) ? $aPaginator['current_page'] : $oUserFeeds->currentPage(); ?>;
    var nLastPage = <?php echo isset($aPaginator['last_page']) ? $aPaginator['last_page'] : $oUserFeeds->lastPage(); ?>;

    $(window).scroll(function (event) {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            if(nCurrentPage < nLastPage) {
                nCurrentPage +=1;
                var sBrowserUrl = window.location.href;
                if(sBrowserUrl.indexOf("user/user-poll") > 0)
                    loadNewData('polls', '<?php echo route('user.user-poll'); ?>', nCurrentPage);
                else
                   loadNewData('polls', '<?php echo route('user.all-poll'); ?>', nCurrentPage); 
            }
        }
    });
    

    setAutosizeTextAreaHeight(34);
    callAutoSizeTextArea();

</script>
