@extends('layouts.web.login_layout')

@section('title', 'Campusknot-Forgot-Password')

@section('content')
<div class="container-fluid banner-img">
    <div class= "col-lg-8 col-sm-8 col-md-9 col-xs-9 main-form">
        
   
    <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 logo-section">
        <div class="ck-logo">
            <img src="{{asset('assets/web/img/main_header_logo.png')}}">
            <h2>Nice to see you agian!</h2>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 login-form-section">
        <form id="forgot_password_form" method="POST" action="{{ route('user.forgot-password') }}" class="login-form">
        {!! csrf_field() !!}
        <div class="clearfix">
            @if ($errors->has('not_verified'))
                <span class="error_message">{{ $errors->first('not_verified') }}</span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" title="{{ ($errors->has('email')) ? $errors->first('email') : '' }}" id="email" class="form-control {{ ($errors->has('email')) ? 'error' : '' }}" name="email" value="{{ old('email') }}"  data-toggle="tooltip" data-placemet="top" placeholder="{{trans('messages.email') }}" />
        </div>
        <div class="">
            <input type="submit" value="{{ trans('messages.request_new_password') }}" class="login-btn">
        </div>
        <span><a href="{{ route('user.login') }}">{{ trans('messages.login') }}</a>
            <a href="{{ route('user.signup') }}" class="pull-right">{{ trans('messages.sign_up_free') }}</a>
        </span>
    </div>
</div>
 </div>
@stop