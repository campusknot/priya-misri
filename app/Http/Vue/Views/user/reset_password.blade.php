<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Campusknot</title>
    <link href="http://fonts.googleapis.com/css?family=Dosis:400,500,700" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}">
</head>
  <body>
<div class="container-fluid banner-img reset-password">
    <div class= "col-lg-8 col-sm-8 col-md-12 col-xs-12 main-form">
        <div class= "col-lg-6 col-sm-6 col-md-12 col-xs-12 logo-section">
            <div class="ck-logo">
                <img src="{{asset('assets/web/img/main_header_logo.png')}}">
                <h2>{{ trans('messages.reset_password_page_title')}}</h2>
            </div>
        </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 login-form-section">
                <form id="reset_password_form" class="login-form" method="POST" action="{{ route('user.reset-password') }}">
                    {!! csrf_field() !!}
                    @if(isset($sMsg) && $sMsg != '')
                        <div class="ck-error">{{ $sMsg }}</div>
                    @endif
                    <input type="hidden" id="address" name="address" value=""  />
                    <input type="hidden" id="encryption_key" name="encryption_key" value="{{ $sVerificationKey }}" />
                    <div class="form-group">
                        <input type="password" title="{{($errors->has('new_password')) ? $errors->first('new_password'):''}}" id="new_password" class="form-control" name="new_password" value="" placeholder="{{trans('messages.new_password') }}" />
                    </div>
                    <div class="form-group">
                        <input type="password" title="{{($errors->has('confirm_new_password'))? $errors->first('confirm_new_password')  :''}}"id="confirm_new_password" class="form-control" name="confirm_new_password" value="" placeholder="{{trans('messages.repeat_new_password') }}" />
                    </div>
                    <input type="submit" value="{{ trans('messages.reset_password_page_title') }}" class="login-btn">
                    
                </form>
            </div>
        </div>
    </div>
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 app-download-footer">
                <p> 
                    <a href="{{ url('/about') }}">{{ trans('messages.about') }}</a> |
                    <a href="{{ url('/privacy') }}" target="_blank">{{ trans('messages.privacy_policy') }}</a> |
                    <a href="{{ url('/terms') }}" target="_blank">{{ trans('messages.terms_of_use') }}</a>
                </p>
            </div>
 </body>
 </html>
<script type="text/javascript">
$(document).ready(function(){
    $.get("http://ipinfo.io", function(response) {
        $("#address").val(response.city + ", " + response.region +", " + response.country);
    }, "jsonp");
});
</script>