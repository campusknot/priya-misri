
@extends('layouts.web.main_layout')
@include('common.errors')

@section('title', 'Campusknot-User-Profile')

@section('content')

<div class="main">

	<div class="col-md-3"></div>
	<div class="col-md-6">
		@foreach($user_profile_pic_list as $user_profile_pic)

		 <img style="width:170px;height:170px"  src="/uploads/user_profile/{{ $user_profile_pic->file_name }}"  class="img-responsive" alt="">

		@endforeach
	</div>

</div>

@endsection