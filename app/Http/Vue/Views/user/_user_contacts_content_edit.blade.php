@if(\Request::route()->getName() == 'user.user-contacts-follower' )
<!-- Start: content row start -->
<div class="innerpage clearfix">
    <div class="inner-page-heading padding-10">
        <h3>{{trans('messages.followers') }}</h3>
    </div>
        @if(count($user_follower_list))
            @foreach($user_follower_list as $follower_list)
            <!-- Start: user detail div start -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix ">
                <div class="text-left single-member pos-relative">
                    <a href="{{route('user.profile',[$follower_list->id_user])}}">
                        <div class="contact-pic icon-image img-66">
                            {!! setProfileImage("150",$follower_list->user_profile_image,$follower_list->id_user,$follower_list->first_name,$follower_list->last_name) !!}
                        </div>
                        <span class="contact-info  display-ib v-align ">
                            <span class="{{$follower_list->user_type}}-name font-w-500">{{$follower_list->first_name}}&nbsp;&nbsp;{{$follower_list->last_name}}</span>

                            </span>
                            <span class="mightOverflow user-degree  set-max-limit text-left">
                                @if(count($follower_list->major)>0)
                                    {{$follower_list->major['degree']}} {{ ($follower_list->major['major']) ? ','.$follower_list->major['major'] : ''}}
                                @endif
                            </span>
                        </span>
                    </a>
                    <span class="follow-wrapper">
                        @if($follower_list->id_follow!='')
                            <a href="{{ route("user.un-follow-user", ['user' => $follower_list->id_user])}}" class="btn btn-outline pull-right">{{ trans('messages.unfollow') }}</a>
                        @else
                            <a href="{{ route("user.follow-user", ['user' => $follower_list->id_user])}}" class=" btn btn-outline pull-right">{{ trans('messages.follow') }}</a>
                        @endif
                    </span>

                </div>
            </div>
            <!-- End: user detail div start -->
            @endforeach
        @else
            <div class="no-data">
                <img src="{{asset('assets/web/img/no-follower.png')}}">
                {{ trans('messages.no_followers') }}
            </div>
        @endif
</div>
<!-- End: content row start -->
@endif


@if(\Request::route()->getName() == 'user.user-contacts-following')
<!-- Start: content row start -->
<div class="innerpage clearfix">
    <h3>{{trans('messages.following') }}</h3>
    @if(count($user_following_list))
        @foreach($user_following_list as $following_list)
        <!-- Start: user detail div start -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix ">
            <div class="text-left single-member pos-relative" >
                <a href="{{route('user.profile',[$following_list->id_user])}}">
                    <div class="contact-pic icon-image img-66">
                        {!! setProfileImage("150",$following_list->user_profile_image,$following_list->id_user,$following_list->first_name,$following_list->last_name) !!}
                    </div>
                    <span class="contact-info display-ib v-align">
                        <span  class="{{$following_list->user_type}}-name font-w-500">{{$following_list->first_name}}&nbsp;&nbsp;{{$following_list->last_name}}</span>
                        <span class="mightOverflow user-degree set-max-limit text-left">
                            @if(count($following_list->major)>0)
                                {{$following_list->major['degree']}} {{ ($following_list->major['major']) ? ','.$following_list->major['major'] : ''}}

                            @endif
                        </span>
                    </span>
                </a>
                <span class="follow-wrapper">
                    <a href="{{ route("user.un-follow-user", ['user' => $following_list->id_user])}}" class="btn btn-outline pull-right">{{ trans('messages.unfollow') }}</a>
                </span>
                    

            </div>

        </div>
        <!-- End: user detail div start -->
        @endforeach
    @else
        <div class="no-data">
            <img src="{{asset('assets/web/img/no-follower.png')}}">
            {{ trans('messages.no_following') }}
        </div>
    @endif
</div>

    <div class="switch-tab-loader side-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
<!-- End: content row start -->
@endif
