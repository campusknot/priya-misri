@extends('layouts.web.main_layout')
@include('common.errors')

@section('title', 'Campusknot-User Profile')


@section('left_sidebar')
    @include('WebView::user._user_profile_sidebar')
@endsection

@section('content')
<div class="user-photos innerpage">
    <h3>User Profile Images</h3>
    <div class="content-area clearfix">
        @if(count($user_profile_pic_list))
        @foreach($user_profile_pic_list as $user_profile_pic)
        <div class="col-lg-4 padding-5 user-profile-pic">
            <img     src="{{config('constants.MEDIAURL')}}/user/{{ $user_profile_pic->file_name }}"
                     class="img-responsive" alt="">
        </div>
        @endforeach
        @else
            <div class="no-data">
                <img src="{{asset('assets/web/img/no-photos.png')}}">
                {{ trans('messages.no_group_photos') }}
            </div>
        @endif
    </div>
    
</div>

 {{--   <br>============================================<br>
    User Profile Images
    <br>============================================<br>

    <div class="row">
        @foreach($user_profile_pic_list as $user_profile_pic)
            <div class="col-md-3">
                <img style="width:170px;height:170px"
                     src="{{config('constants.MEDIAURL')}}/user/{{ $user_profile_pic->file_name }}"
                     class="img-responsive" alt="">
            </div>
        @endforeach
    </div>

    <br>============================================<br>
    User Post Images
    <br>============================================<br>

    <div class="row">
        @foreach($user_post_pic_list as $user_post_pic)
            <div class="col-md-3">
                <img style="width:170px;height:170px"
                     src="{{config('constants.MEDIAURL')}}/{{ $user_post_pic->file_name }}"
                     class="img-responsive" alt="">
            </div>
        @endforeach
    </div>

--}}
@endsection

@section('right_sidebar')

@endsection
