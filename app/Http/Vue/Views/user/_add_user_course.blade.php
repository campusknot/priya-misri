<!-- Modal content-->
<div class="modal-content add-group-popup">
    <form id="add_user_course_form" name="add_user_course_form" method="POST" action="" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="modal-header">
            <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title event-title">{{ trans('messages.add_course') }}</h4>
        </div>
        <div class="modal-body clearfix">
            <div class="type-info-icon"> 
                <img src="{{asset('assets/web/img/info.png')}}" class="" >
            </div>
            <div class="type-info-text">
                  {{ trans('messages.add_course_info') }}
            </div>
            <div class="form-group  padding-t-10 clearfix">
                <input type="text" id="course_name" class="form-control" name="course_name" placeholder="{{trans('messages.select_course')}}" value="{{ old('course_name') }}" required="required">
                @if ($errors->has('selected_courses'))
                    <span class="error_message"><?php echo $errors->first('selected_courses'); ?></span>
                @endif
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="auto_suggetion_div" class=""></div>
                </div>
            </div>
            <div id="selected_course" class="form-group clearfix selected_course"></div>
            <div class="form-group clearfix hidden">
                <input id="become_member" class="pull-left" type="checkbox" name="become_member" value="1" checked="checked">
                <span class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                    {{ trans('messages.become_member_confirmation') }}
                </span>
            </div>
            <div class="form-group clearfix">
                <!--<button onclick="closeLightBox();" data-dismiss="modal" class="btn btn-default pull-right" type="button">{{ trans('messages.cancel') }}</button>-->
                <button type="button" onclick="submitAjaxForm('add_user_course_form', '{{ route('user.add-user-course') }}', this )" class="btn btn-primary lg-center-button">{{ trans('messages.add_course') }}</button>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(function()
    {
        $('#course_name').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('user.course-suggestion') }}"+"?course_name=" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.course_name,
                            value: value.course_code,
                            desc: value.id_course
                        };

                    }));
                });
            },
            minLength: 2,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {

            },
            select: function(event, ui) {
                event.stopPropagation();
                var sHtml = "<div id='vR_"+ui.item.desc+"' class='vR pull-left m-r-1'>";
                    sHtml += "<span email='"+ui.item.value+"' class='vN'>";
                    sHtml += "<div class='vT'>"+ui.item.label+"</div>";
                    sHtml += "<div id='"+ui.item.desc+"' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>";
                    sHtml += "</span>";
                    sHtml += "<input type='hidden' value='"+ ui.item.desc + "' name='selected_courses[]'>";
                    sHtml += "</div>";
                $('#selected_course').append( sHtml );
                $("#course_name").val('');
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span>" + item.label + " (" + item.value + ")</span>" )
                .appendTo( ul );
            };
    });
        
    $(document).on('keydown', 'input#course_name', function(e) { 
        var keyCode = e.keyCode || e.which; 

        if (keyCode == 9 || keyCode == 188) { 
            e.preventDefault(); 
            // call custom function here
            if( $(this).val().length !== 0 ) {
                
                var sHtml = "<div id='vR_"+$(this).val()+"' class='vR pull-left m-r-1'>";
                    sHtml += "<span email='"+$(this).val()+"' class='vN'>";
                    sHtml += "<div class='vT'>"+$(this).val()+"</div>";
                    sHtml += "<div id='"+$(this).val()+"' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>";
                    sHtml += "</span>";
                    sHtml += "<input type='hidden' value='"+ $(this).desc() + "' name='selected_courses[]'>";
                    sHtml += "</div>";
                $('#selected_course').append( sHtml );
                $(this).val('');
            }
        } 
    });
</script>