@extends('layouts.web.main_layout')
@include('common.errors')

@section('title', 'Campusknot-User Profile')

@section('left_sidebar')
    @if(Auth::user()->id_user == $user->id_user)
        @include('WebView::user._user_profile_sidebar')
    @else
        @include('WebView::user._user_profile_sidebar_show')
    @endif
@endsection

@section('content')
<div class="profile-Content">
    <div class="user-photos innerpage">
        <div class="inner-page-heading padding-10">
            <h3>{{ trans('messages.user_profile_images') }}</h3>
        </div>
        <div class="content-area clearfix">
             @if(count($aUserProfilePicList))
                @if($aUserProfilePicList[0]->id_user == Auth::user()->id_user)
                    @foreach($aUserProfilePicList as $oUserProfilePic)
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-10">
                        <div class=" img-bg-show no-padding">
                            <button class="close" type="button" onclick="deleteUserImage({{$oUserProfilePic->id_user_profile_image}}, '{{ trans('messages.delete_image_confirmation') }}');" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <img src="{{ config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$oUserProfilePic->file_name }}"
                             class="img-responsive " alt="" onclick="showProfileImageLightBox('{{ config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$oUserProfilePic->file_name }}');">
                        </div>
                    </div>
                    @endforeach
                @else
                    @foreach($aUserProfilePicList as $oUserProfilePic)
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 padding-10">
                        <div class=" img-bg-show no-padding">
                            <img src="{{ config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$oUserProfilePic->file_name }}"
                             class="img-responsive " alt="" onclick="showProfileImageLightBox('{{ config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$oUserProfilePic->file_name }}');">
                        </div>
                    </div>
                    @endforeach
                @endif
            @else
            <div class="no-data">
                <img src="{{asset('assets/web/img/no-photos.png')}}">
                {{ trans('messages.no_group_photos') }}
            </div>
            @endif
        </div>
    </div>  
</div>

    <div class="switch-tab-loader side-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

@endsection