<link rel="stylesheet" href="{{ asset('assets/web/css/crop/cropper.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/web/css/crop/main.css') }}">
<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
    <div class="">
        <div class="modal-content profile-photo-modal">
          <form class="avatar-form" action="{{ url('user/profile-upload-image') }}" enctype="multipart/form-data" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" id="avatar-modal-label">{{ trans('messages.change_profile_icon') }}</h4>
            </div>
            <div class="modal-body">
                <div class="avatar-body">

                    <!-- Upload image and data -->
                    <div class="avatar-uploa hidden">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <input type="hidden" class="avatar-src" name="src">
                        <input type="hidden" class="avatar-data" name="data">

                        <label for="avatarInput"></label>
                        <input type="file" class="avatar-input" id="avatarInput" name="Img_file" accept="image/*">
                    </div>
                    <div id="validation-error" class="alert alert-danger" style="display: none;"></div>
                    <!-- Crop and preview -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="avtar-tagline">{{trans('messages.upload_image_tagline')}}</div>
                            <div class="avatar-wrapper hidden">
                            </div>
                            <div class="select-area">
                                <div class="select-file-area">
                                    <img src="{{ asset('assets/web/img/profile-image-placeholder.png') }}">
                                    <button class="browse-btn">
                                        {{trans('messages.upload_image')}}
                                        </button>
                                </div>
                            </div>
                        </div>
                        <!--Avataar preview show
                        <div class="col-md-3">
                            <div class="avatar-preview preview-lg"><img src='{{ asset('/assets/web/img/file-icons/image.png') }}'></div>
                        </div>-->
                    </div>

                    <div class="row avatar-btns">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <button type="submit" class="btn btn-primary avatar-save" id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.done') }}</button>
                        </div>
                    </div>
                </div>
            </div>
            
          </form>
        </div>
    </div>
</div><!-- /.modal -->
<link rel="stylesheet" href="{{ asset('assets/web/css/crop/jquery.min.js') }}">
<script type="text/javascript">
$('.select-file-area').click(function(){
    $('#avatarInput').click(); 
});
$('#crop-avatar').click(function(){
   // debugger;
    $('.select-area').removeClass('hidden');
    $('.avatar-wrapper').addClass('hidden');
    $('.avatar-form')[0].reset(); 
    $('.avatar-alert').html('');
    $('#validation-error').css('display','none');
});
$('.avatar-save').click(function(e){
    if($('#avatarInput').val()==''){
        $('#validation-error').text('Image is required');
        $('#validation-error').css('display','block');
        return false;
    }
    else{
         $('#validation-error').css('display','none');
    }
    var $this = $('.avatar-save');
    $this.button('loading');    
});
</script>