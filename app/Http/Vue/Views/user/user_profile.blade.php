@extends('layouts.web.main_layout')

@section('title', 'Campusknot-User Profile')

@section('left_sidebar')
    @include('WebView::user._user_profile_sidebar')
@endsection

@section('content')
    @include('WebView::user._user_profile_content_edit')
@endsection