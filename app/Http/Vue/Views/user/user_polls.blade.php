@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Poll List')


@section('content')
<div class="tabbable pos-relative clearfix">
    @if (session('error_message'))
        <div class="clearfix alert alert-danger">{{ session('error_message') }}</div>
    @endif
    <!-- group tabs -->
    <div class="clearfix pos-relative" id="tabs">
        <ul class="group-tabs nav nav-tabs poll-tabs">
            <li id="all-poll" data-toggle="tooltip" data-placement="top" title="All" nowrap onclick="changePollListing('polls', '{{ route('user.all-poll-ajax') }}', '{{ route('user.all-poll') }}');" class="active" ><a>{{ trans('messages.all') }}</a></li>
            <li id="user-poll" data-toggle="tooltip" data-placement="top" title="" nowrap onclick="changePollListing('polls', '{{ route('user.user-poll-ajax') }}', '{{ route('user.user-poll') }}');"><a>{{ trans('messages.my_poll') }}</a></li>
            
            <span class="btn btn-primary add-poll-btn  pull-right" data-toggle="collapse" data-target="#add_poll" aria-expanded="false">{{ trans('messages.add_poll') }}</span>
            
        </ul>
    </div>
    
        <div class="tab-content"> 
            <div class="switch-tab-loader hidden">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div class="clearfix collapse" id="add_poll" >
                @include('WebView::post._add_poll')
            </div>
                @include('WebView::user._poll_ajax')
            <div class="more-data-loader hidden">   
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
        </div>
    
</div>
@section('footer-scripts')
    
    setInterval(function() {
       $(".minute").each(function(){
            var ele=$(this);
            var myTime = ele.val()-1;

            if(myTime <= 300 ){
                var MinSec=getTime(myTime);
                ele.siblings('span').text(MinSec);
                ele.siblings('span').removeClass('hidden');
                ele.siblings('div').addClass('hidden');
            }
            ele.val(myTime);

            if(myTime <= 0){
                ele.parent().addClass('hidden');
                var parent=ele.parent().parent().children('form');
                parent.children('input[name=expired]').val(1);
                parent.children('button').trigger( "click" );

            }
        });
    },  1000);
    function showRequestComment(formData, jqForm, options)
    { 
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
    }
    
    function showResponseComment(response, statusText, xhr, $form)
    { 
        if(response.success == true)
        {
            if(response.type=='poll'){
                $('#'+response.id_post).html(response.html);
            }
            else
            {
                $('#add_comment_'+ response.id_post)[0].reset();
                $('#comment-box-'+response.id_post).html(response.html);
                removeErrorComment(response.id_post);
            }
            $('.btn-md').button('reset');
            <!--$('.animated').css('height','36px');-->
        }
        else
        { 
            var arr = response;
                $.each(arr, function(index, value)
                {
                        if (value.length != 0)
                        {
                                $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                        }
                });                     
        }
    }
    
    function showErrorComment(xhr, textStatus, errorThrown)
    {
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
            }
        
        var result = $.parseJSON(arr);
        $.each(result, function(key, value)
        {
            key=key.replace('file','comment_text');
            $('.btn-md').button('reset');
            if (key.length != 0)
            {
                    $("#"+key).html('<strong>'+ value +'</strong>');
            }
        });
    }

    function removeErrorComment(id)
    {
        $("#comment_text_"+id).html('');
    }
    
    $(document).ready(function(){
        $(".file-attachment-cancel").click(function(){
            var id = $(this).attr('data-id');
            $('#file_'+id).val('').clone(true);
            $(this).parent().removeClass("show-input");
            input = $(".file-attachment-name").val('');
        });
        
        var options = { 
        beforeSubmit:  showRequestComment,
        success:       showResponseComment,
        error:showErrorComment,
        dataType: 'json' 
        }; 
        
        makeTabSelected();
        
        $(document).on('click','.btn-poll',function(e){
            var data = $(this);
            var id_poll = data.attr('data-id');
            $(this).button('loading');
            $('#poll_'+id_poll).ajaxForm(options).submit();
        });
        
        findUrlThumbnails();

    });
    
    $('.group-tabs li').click(function(){
        var id=$(this).attr('id');
        $('.group-tabs li').removeClass('active');
        $('#'+id).addClass('active');
    });
    function makeTabSelected() {
        var sPathName = location.pathname;
        var aUrlComponents = sPathName.split('/');

        var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 3] + '/' + aUrlComponents[aUrlComponents.length - 2] + '-ajax/' + aUrlComponents[aUrlComponents.length - 1];

        //Make side bar tab selected
        var sAction = aUrlComponents[aUrlComponents.length - 1];
        var aActionComponents = sAction.split('/');

        var oActiveTab = $('#'+aActionComponents[0]);
        oActiveTab.siblings().removeClass('active');
        oActiveTab.attr('class', '');
        oActiveTab.addClass('active');
    }
    
    $(window).bind('popstate', function() {
        var sPathName = location.pathname;
        var aUrlComponents = sPathName.split('/');

        var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 2] + '/' + aUrlComponents[aUrlComponents.length - 1] + '-ajax';
        changePollListing('polls',sLoadUrl, location);

        //Make side bar tab selected
        makeTabSelected();
    });
@endsection
@stop
