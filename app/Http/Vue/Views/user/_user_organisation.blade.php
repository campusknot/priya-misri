<div class="modal-content edit-information"  id="edit_organization">
    <div class="modal-header">
        <div class="modal-header-img bg-organization" >
            <img src="{{ asset('assets/web/img/organization.png') }}" alt="Organization" class="" />
        </div>
        {{ trans('messages.organization') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdOrganization = isset($oUserOrganization->id_user_co_curricular_activity) ? $oUserOrganization->id_user_co_curricular_activity : old('id_org_hdn');
        $sCommitteeName  = isset($oUserOrganization->committee_name) ? $oUserOrganization->committee_name : old('committee_name');
        $sPost           = isset($oUserOrganization->post) ? $oUserOrganization->post : old('post');
        $nStartYear      = isset($oUserOrganization->start_year) ? $oUserOrganization->start_year : old('start_year');
        $nStartMonth     = isset($oUserOrganization->start_month) ? $oUserOrganization->start_month : old('start_month');
        $nEndYear        = isset($oUserOrganization->end_year) ? $oUserOrganization->end_year : old('end_year');
        $nEndMonth       = isset($oUserOrganization->end_month) ? $oUserOrganization->end_month : old('end_month');
        $isPresent = '';
        if(!empty($nIdOrganization) && empty($nEndYear) && !$errors->has('end_year') && !$errors->has('end_month')) {
            $isPresent = "checked='checked'";
        }
    ?>

    <div class="modal-body clearfix">
        <form id="add_organization_form" class="center">
            <input id="id_organization" name="id_organization" type="hidden" value="{{$nIdOrganization}}" >
            {!! csrf_field() !!}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="committee_name" class="form-control {{($errors->has('committee_name')) ?'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.organization_name') }}"
                       title="{{($errors->has('committee_name')) ? $errors->first('committee_name') : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="committee_name" value="{{$sCommitteeName}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="post" class="form-control {{($errors->has('post'))?'error': '' }}"
                       type="text" placeholder="{{ trans('messages.position_held') }}"
                       title="{{($errors->has('post')) ?$errors->first('post'):''}}" data-toggle="tooltip"  data-placement="top" 
                       name="post" value="{{$sPost}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <div class="date-select  pull-left {{ ($errors->has('start_month') || $errors->has('start_year')) ? 'error' :'' }}" >
                    <div class="date-month" title="{{ ($errors->has('start_month')) ?$errors->first('start_month') :'' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="start_month" class="form-control" name="start_month" required>
                            <option value="">{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nStartMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nStartMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nStartMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nStartMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nStartMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nStartMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nStartMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nStartMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nStartMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nStartMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nStartMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nStartMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="start_year" class="form-control {{($errors->has('start_year')) ? 'error': ''}}"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('start_year')) ? $errors->first('start_year'): ''}}" data-toggle="tooltip"  data-placement="top" 
                           name="start_year" value="{{$nStartYear}}" onkeypress="return isNumberKey(event);" maxlength="4">
                </div>
                <span class="dash-span"> - </span>
                <span class="organization-present-text">{{ trans('messages.present') }}</span>
                <div class="organization-end-date date-select  pull-right {{($errors->has('end_month') || $errors->has('end_year')) ? 'error':'' }}" >
                    <div class="date-month " title="{{($errors->has('end_month')) ? $errors->first('end_month'):'' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="end_month" class="form-control" name="end_month" required>
                            <option value="" disabled selected hidden>{{ trans('messages.end_month') }}</option>
                            <option value='1' <?php echo $nEndMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nEndMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nEndMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nEndMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nEndMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nEndMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nEndMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nEndMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nEndMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nEndMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nEndMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nEndMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="end_year" class="form-control {{ ($errors->has('end_year')) ? 'error' : '' }}"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{ ($errors->has('end_year')) ? $errors->first('end_year') : '' }}" data-toggle="tooltip"  data-placement="top" 
                           name="end_year" value="{{$nEndYear}}" onkeypress="return isNumberKey(event);" maxlength="4">
                </div>
                <div class="date-error">{{ ($errors->has('end_date')) ? $errors->first('end_date') : '' }}</div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left">
                <div class="pull-left present">
                    <input id="present" name="present" class="form-control" placeholder="Year" value="1" type="checkbox" {{$isPresent}}>&nbsp;&nbsp;present
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button class="btn btn-default pull-right" type="button" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="org_save_btn_aii" name="org_save_btn_aii" class="btn btn-primary pull-right" type="button" value="{{ empty($nIdOrganization) ? trans('messages.save') : trans('messages.update') }}" onclick="submitAjaxForm('add_organization_form', '{{ route('user.add-organization') }}', this )">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
        
    $("#present").click(function(){
        $(".organization-end-date").toggleClass("hide");
        $(".organization-present-text").toggleClass("show");
    });
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
        var isPresent = "<?php echo $isPresent; ?>";
        if(isPresent != ''){
            $(".organization-end-date").toggleClass("hide");
            $(".organization-present-text").toggleClass("show");
        }

    });    
</script>