<!--side bar-->
<div class="sidebar">
    <div class="top-detail">
            <div class="icon-letter">
                    <span>P</span>
                    <span>H</span>
            </div>
            <div class="icon-image img-150">
                {!! setProfileImage("150",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                     <div id="crop-avatar">
                        <div class="avatar-view" title="Change Profile Image">
                            <div class="change-pic">{{ trans('messages.change_avatar') }}</div>
                      </div>
                   </div>                               
            </div>
            <div class="entity-name"> 
                    {{ $user->first_name }} {{ $user->last_name }}
                    <span class="sml-light-text">{{ $user->campus->campus_name }}</span>
            </div>
    </div>
    <ul class="options sidebar-menu-options">
        <li id="active_profile" class="active_profile" onclick="getProfileData('{{ route('user.profile-ajax',['nIdUser'=>$user->id_user]) }}','{{ route('user.profile',['nIdUser'=>$user->id_user]) }}');">
            <a>{{ trans('messages.profile') }}</a>
        </li>
        <li id="active_image" class="" onclick="getProfileData('{{ route("user.profile-image-list-user-ajax" ,[$user->id_user]) }}','{{ route("user.profile-image-list-user" ,[$user->id_user]) }}');">
            <a>@if($aUserSpecificCounts['image_count']>0) {{ $aUserSpecificCounts['image_count'] }} @endif Photos </a>
        </li>
        <li id="active_follower" class="" onclick="getProfileData('{{ route("user.user-contacts-follower-show-ajax",[$user->id_user]) }}','{{ route("user.user-contacts-follower-show",[$user->id_user]) }}');">
            <a>@if($aUserSpecificCounts['following_count']>0) {{ $aUserSpecificCounts['following_count'] }} @endif Followers </a>
        </li>
        <li id="active_following" class="" onclick="getProfileData('{{ route("user.user-contacts-following-show-ajax",[$user->id_user]) }}','{{ route("user.user-contacts-following-show",[$user->id_user]) }}');">
            <a>@if($aUserSpecificCounts['follower_count']>0) {{ $aUserSpecificCounts['follower_count'] }} @endif Following </a>
        </li>

    </ul>
</div>
<!--side bar-->
@include('WebView::user._user_profileupload')
<script>
$(".options li").click(function(){
    $(this).siblings().attr('class', '');
    $(this).attr('class', '');
    var id= $(this).attr('id');
    $(this).addClass(id);
});

$(document).ready(function(){
    makeTabSelected();
});

function makeTabSelected() {
    var sPathName = location.pathname;
    var aUrlComponents = sPathName.split('/');
    
    //Make side bar tab selected
    var sAction = aUrlComponents[aUrlComponents.length - 2];
    var aActionComponents = sAction.split('-');
    
    var oActiveTab = $('#active_'+aActionComponents[aActionComponents.length - 1]);
    oActiveTab.siblings().attr('class', '');
    oActiveTab.attr('class', '');
    oActiveTab.addClass(oActiveTab.attr('id'));
}

$(window).bind('popstate', function() {
    var sPathName = location.pathname;
    var aUrlComponents = sPathName.split('/');
    if(aUrlComponents.length == 4)
        var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 3] + '/' + aUrlComponents[aUrlComponents.length - 2] + '-ajax/' + aUrlComponents[aUrlComponents.length - 1];
    else if(aUrlComponents.length == 5)
        var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 4] + '/' + aUrlComponents[aUrlComponents.length - 3] + '/' + aUrlComponents[aUrlComponents.length - 2] + '-ajax/' + aUrlComponents[aUrlComponents.length - 1];
    
    getProfileData(sLoadUrl, location);
    
    //Make side bar tab selected
    makeTabSelected();
});
</script>