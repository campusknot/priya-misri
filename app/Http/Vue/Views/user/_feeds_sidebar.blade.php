@if(count($oSidebarContent)>0)
<div id="followFacultyCarousel" class="carousel slide follow-faculty-sidebar" data-ride="carousel">
    <div class="follow-faculty-title">{{ trans('messages.follow_faculty') }} </div>
    <div class="carousel-inner">
        @foreach($oSidebarContent as $oSidebar)
            <div class="item follow-faculty">
              <div class="col-lg-12 card-bg">
                  <a href="{{route('user.profile',[$oSidebar->id_user])}}">
                      <div class="contact-pic icon-image img-125 pos-relative">
                          {!! setProfileImage("150",$oSidebar->user_profile_image,$oSidebar->id_user,$oSidebar->first_name,$oSidebar->last_name) !!}
                          <span class="faculty-label">{{config('constants.USERTYPEFACULTY') }}</span>  
                      </div>
                      <div class="contact-info">
                          <span class="S-name">{{ $oSidebar->first_name.' '.$oSidebar->last_name}}</span>
                          <span class=" mightOverflow user-degree set-max-limit">
                              @if(count($oSidebar->major)>0)
                                {{$oSidebar->major['degree']}} {{ (!empty($oSidebar->major['major'])) ? $oSidebar->major['major']: '' }}
                            @endif
                          </span>
                      </div>
                  </a>
                  <span>
                      <a href="javascript:void(0);" class=" follow" onclick="callFollowFaculty(this,'{{ route("user.follow-user", ['user' => $oSidebar->id_user])}}',0,{{ $oSidebar->id_user }})">{{ trans('messages.follow') }}</a>
                  </span>

              </div>
            </div>
        @endforeach
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#followFacultyCarousel" data-slide="prev">
      <span class="slider-navigation"><img class="" src="{{asset('assets/web/img/arrow-slider-back.png')}}"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#followFacultyCarousel" data-slide="next">
      <span class="slider-navigation"><img class="" src="{{asset('assets/web/img/arrow-slider-forward.png')}}"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
@endif
<script>
$('.carousel').carousel({
    interval : false
});
$('.carousel-inner .item').first().addClass('active');
function callFollowFaculty(ele,sRoute,nFollowFlag,nIdUser){
    callFollowUser(ele,sRoute,nFollowFlag,nIdUser);
    $(ele).parent().parent().parent().remove();
    $('.carousel').carousel({
        interval : false
    });
    $('.carousel-inner .item').first().addClass('active');
    var followCardCount = $('.carousel-inner .item').siblings().length;
    if(followCardCount < 2){
         $(".slider-navigation").css("display","none");
    }
    if($('.carousel-inner .item').length == 0){
        $('#followFacultyCarousel').remove();
    }
    
}
  var followCardCount = $('.carousel-inner .item').siblings().length;
     if(followCardCount < 2){
         $(".slider-navigation").css("display","none");
    }
    
</script>