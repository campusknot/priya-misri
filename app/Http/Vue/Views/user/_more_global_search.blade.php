@if(count($oUser))
<?php
    foreach($oUser as $oUserList)
    {

?>
    <div class="clearfix col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
        <div class="col-lg-12 list-card-bg">
        <a href=" {{ route('user.profile', ['nIdUser' => $oUserList->id_user]) }}" title="{{ trans('messages.click_here', ['click_hint' => 'to view '.$oUserList->first_name.' '. $oUserList->last_name.'\'s profile']) }}">
            <div class="icon-image img-66 display-ib">
                {!! setProfileImage("150",$oUserList->user_profile_image,$oUserList->id_user,$oUserList->first_name,$oUserList->last_name) !!}
            </div>
            <div class=" display-ib v-align single-user-search-card">
                <span class="mightOverflow {{$oUserList->user_type}}-name person-name" data-toggle="tooltip" data-placement="bottom" title="" nowrap>{{ $oUserList->first_name.' '. $oUserList->last_name }}</span>
                <span class="user-email">{{ $oUserList->email }}</span>
                <span class="user-degree set-max-limit">
                    @if(count($oUserList->major)>0)
                        {{$oUserList->major['degree']}} @if(!empty($oUserList->major['major'])), {{$oUserList->major['major']}} @endif
                    @endif
                </span>
            </div>
             
        </a>
            <span class="pull-right">
                @if($oUserList->id_user_follow!='')
                    <a href="javascript:void(0);" class=" follow" onclick="callFollowUser(this,'{{ route("user.un-follow-user", ['user' => $oUserList->id_user])}}',1,{{ $oUserList->id_user }})">{{ trans('messages.unfollow') }}</a>
                @else
                    <a href="javascript:void(0);" class=" follow" onclick="callFollowUser(this,'{{ route("user.follow-user", ['user' => $oUserList->id_user])}}',0,{{ $oUserList->id_user }})">{{ trans('messages.follow') }}</a>
                @endif
            </span>
            
        </div>
    </div>
<?php
    }
?>
@else

    <div class="col-lg-12 no-data-found  background-white no-data">
         {{ trans('messages.no_record_found') }} 
    </div>
@endif