@extends('layouts.web.main_layout_col9')

@section('title', 'Campusknot-Book List')

@section('content')
<div class="clearfix ">
    @if ($errors->has('group_error'))
        <div class="clearfix alert alert-danger">{{ $errors->first('group_error') }}</div>
    @endif
    <form id="group_search"  class="clearfix" name="group_search" action="{{ route('group.group-search') }}" method="GET" enctype="multipart/form-data" onsubmit="searchGroup();">
        
    </form>
    <div class="clearfix pos-relative" id="tabs">
        <ul class="group-tabs nav nav-tabs padding-right clearfix">
            <li id="all" class="active" onclick=""> <a href="#tab_group_all" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>{{ trans('messages.recomended') }}</a></li>
            <li class="" onclick=""> <a href="#tab_group_admin" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>{{ trans('messages.my_books') }}</a></li>
            <li class="" onclick=""> <a href="#tab_group_member" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>{{ trans('messages.wishlist') }}</a></li>
        </ul>
        <div class="pull-right create"> <a href="javascript:void(0);" class="mightOverflow" onclick="showAddSellBookLightBox();" title="" nowrap>{{ trans('messages.sell_book') }}</a></div>
        <div class="pull-right wishlist-btn"> <a href="javascript:void(0);" class="mightOverflow" onclick="showAddToWishlistLightBox();" title="" nowrap>{{ trans('messages.add_to_wishlist') }}</a></div>
        <div id="group_list" class="book-list clearfix">
            @include('WebView::book._recommended_book_listing')
        </div>
    </div>
</div>
@stop
