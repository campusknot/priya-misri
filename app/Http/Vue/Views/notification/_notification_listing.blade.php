<div id="notification_load" class="clearfix notification-bar">
    <h3>{{ trans('messages.notification') }}
        <button type="button" onclick="hideSlider();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </h3>
    @include('WebView::notification._more_notification')
</div>
<div class="more-data-loader hidden more-notification-loder ">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<script type="text/javascript">
    var nCurrentNotiPage = <?php echo $aMessages['current_page']; ?>;
    var nLastNotiPage = <?php echo $aMessages['last_page']; ?>;
     $('.notification-bar').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if(nCurrentNotiPage < nLastNotiPage) {
                nCurrentNotiPage += 1;
                loadNewData('notification_load', '<?php echo route('notification.notification-listing'); ?>', nCurrentNotiPage);
            }
        }
    });
    function redirectEventPage(nIdEvent)
    {
        window.location.href='<?php echo route('event.event-listing'); ?>?nIdEvent='+nIdEvent;
    }
</script>