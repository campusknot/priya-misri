@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Post Data')


@section('content')
<div class="tabbable pos-relative clearfix">
    <div class="tab-content"> 
        @if(count($oFeed))
        <div id="feeds" class="clearfix feeds">
            <div id="{{ $oFeed->id_post }}" class="post">             
                <div class=" clearfix">
                    <!-- Div for creator image -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-top-section">
                        <div class="icon-image img-50">
                            <a href="{{route('user.profile',[$oFeed->id_user])}}">
                                {!! setProfileImage("50",$oFeed->user_profile_image,$oFeed->id_user,$oFeed->first_name,$oFeed->last_name) !!}
                            </a>
                        </div>

                        <div class="post-person-name">
                            <a href="{{route('user.profile',[$oFeed->id_user])}}">{{ $oFeed->first_name.' '.$oFeed->last_name}}</a>
                            <span class="post-time">{{ secondsToTime($oFeed->created_at)  }}
                                @if($oFeed->group_name != NULL){{' |'}} 
                                    <a href="{{ route('group.group-feeds', ['nIdGroup' => $oFeed->id_group]) }}">
                                        <span class="group-name">{{$oFeed->group_name }}</span>
                                    </a>  
                                @endif
                            </span>
                            @if($oFeed->post_type != config('constants.POSTTYPEPOLL') || $oFeed->id_user == Auth::user()->id_user)
                            <span class="edit-links">
                                <img src="{{asset('assets/web/img/dropdown-icon.png')}}" class="post-dropdown" alt="dropdown icon" data-toggle="dropdown" />
                                <ul class="dropdown-menu report-post">
                                    @if($oFeed->id_user == Auth::user()->id_user)
                                        <li onclick="deletePost('{{trans('messages.post_delete_confirmation')}}','{{$oFeed->id_post}}');">{{ trans('messages.delete') }}</li>
                                    @elseif($oFeed->post_type != config('constants.POSTTYPEPOLL'))
                                        <li class="report">{{ trans('messages.report') }}</li>
                                        <li class="report-post-form">
                                            <div class="text-left report-post-header">{{ trans('messages.report_heading') }}</div>
                                            <form id="post_report_{{$oFeed->id_post}}" method="post" onsubmit="submitReport( 'post_report_{{$oFeed->id_post}}','{{ route('post.post-add-report')}}',this);return false;">
                                                <textarea name="report_text" placeholder="{{ trans('messages.report_placeholder') }}"></textarea>
                                                <input type="hidden" name="id_entity" value="{{$oFeed->id_post}}" />
                                                <input type="hidden" name="entity_type" value="{{ config('constants.ENTITYTYPEPOST') }}" />
                                                <input type="button" class="btn btn-primary" value="{{ trans('messages.submit') }}" onclick="submitReport( 'post_report_{{$oFeed->id_post}}','{{ route('post.post-add-report')}}',this)" />
                                                <input type="reset" value="{{ trans('messages.cancel') }}" class="remove-form btn btn-default"/>
                                            </form>
                                        </li>
                                    @endif
                                </ul>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-10">
                        <div class="post-content">
                            @if(trim($oFeed->post_text)!= '')
                            <div class="post-text">
                                <p>{!! setTextHtml($oFeed->post_text) !!}</p>
                            </div>
                            @endif
                            @if($oFeed->file_name || $oFeed->post_type==config('constants.POSTTYPEPOLL'))
                            <?php
                                    $sHtmlString = '';
                                    switch ($oFeed->post_type)
                                    {
                                        case config('constants.POSTTYPEIMAGE'):
                                                                                    //var_dump($contents); 
                                            $sHtmlString = '<div class="post-img">';
                                                $sFileName = setPostImage($oFeed->file_name,80);
                                                $sHtmlString .= '<img src="'.$sFileName.'" onclick="showPhotoDetails('. $oFeed->id_post .');">';
                                                //$sHtmlString .= '<img src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/80_'.$oFeed->file_name.'" onclick="showPhotoDetails('. $oFeed->id_post .');">';
                                                                                       // $sHtmlString .= $contents;
                                            $sHtmlString .= '</div>';

                                            echo $sHtmlString;
                                            break;
                                        case config('constants.POSTTYPEVIDEO'):

                                            break;
                                        case config('constants.POSTTYPEDOCUMENT'):
                                            $sHtmlString = '<div class="post-doc">';

                                            $aFileNameData = explode('.', $oFeed->file_name);

                                            //Div for file icon
                                            if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                            }
                                            elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                            }
                                            elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                            }
                                            elseif (in_array(end($aFileNameData), array('zip','rar')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                            }
                                            elseif (end($aFileNameData) == 'pdf')
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                            }

                                            //Div for file details and view/download option
                                            $sFileDisplayName = ($oFeed->display_file_name) ? $oFeed->display_file_name : $oFeed->file_name;
                                            $sHtmlString .= '<span class="">'.$sFileDisplayName;
                                            $sHtmlString .= '<br>';
                                                $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['p_'.$oFeed->id_post,$oFeed->display_file_name]).'">'.trans('messages.view').'</a>';
                                                $sHtmlString .= '<a class="" target="blank" href="'.route('utility.download-file',['p_'.$oFeed->id_post,$oFeed->display_file_name]).'">'.trans('messages.download').'</a>';
                                            $sHtmlString .= '</span>';
                                            $sHtmlString .= '</div>';
                                            echo $sHtmlString;
                                            break;
                                        case config('constants.POSTTYPEPOLL'):
                                                ?>
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-10">
                                                    <div class="post-content">
                                                        <div class="post-poll">
                                                            @if( strtotime($oFeed->end_time) >= strtotime("now") && $oFeed->activated == 1)
                                                                @if(strtotime($oFeed->start_time) > strtotime("now"))
                                                                    <div class="poll-time">{{ trans('messages.poll_start_on') }}
                                                                        <?php
                                                                            $dPollStartDate = new Carbon($oFeed->start_time);
                                                                            $dPollStartDate->setTimezone(Auth::user()->timezone);
                                                                        ?>
                                                                        <div class="left_time" >{{ $dPollStartDate->format('M j') }} {{$dPollStartDate->format('g:i A')}} | {{ secondsToTime($oFeed->end_time,1,$oFeed->start_time) }}</div>
                                                                        <span class="hidden left-time"></span>
                                                                    </div>
                                                                @else
                                                                <div class="poll-time">{{ trans('messages.poll_time_left') }}
                                                                    <div class="left_time" >{{ secondsToTime($oFeed->end_time,1) }}</div>
                                                                    <span class="hidden left-time"></span> 
                                                                    <input type="text" class="minute hidden" value="{{ TimeDiffinSec($oFeed->end_time) }}" disabled />
                                                                </div>
                                                                @endif
                                                            @elseif($oFeed->activated == 0)
                                                                <div class="poll-time poll-close">{{ trans('messages.poll_saved') }}</div>
                                                            @else
                                                                <div class="poll-time poll-close">{{ trans('messages.poll_closed') }}</div>
                                                            @endif
                                                            <div class="post-poll-question">
                                                                <p>{!! nl2br(auto_link_text(trim($oFeed->poll_text))) !!}</p>
                                                                @if($oFeed->poll_file_name)
                                                                <div class="post-poll-question-img">
                                                                    <img src="{{ setPostImage($oFeed->poll_file_name,80) }}" onclick="showProfileImageLightBox('{{ setPostImage($oFeed->poll_file_name,80) }}');">
                                                                </div>
                                                                @endif
                                                            </div>
                                                            <form id="poll_{{$oFeed->id_poll}}" action="{{ route('post.add-poll-answer') }}" method="POST">
                                                                {!! csrf_field() !!}
                                                                <input type="hidden" name="id_poll" value="{{$oFeed->id_poll}}" >
                                                                <input type="hidden" name="id_post" value="{{$oFeed->id_post}}" >
                                                                <input type="hidden" name="poll_type" value="{{$oFeed->poll_type}}" >
                                                                <input type="hidden" name="expired" value="0" >
                                                                @if($oFeed->poll_type == config('constants.POLLTYPEMULTIPLE') && count($oFeed->polloption) > 0)
                                                                <span class="error_message" id="id_poll_option_{{ $oFeed->id_post }}"></span>
                                                                <div class="post-poll-options clearfix">
                                                                    <?php $sChar = 'A';
                                                                        $sDisable='';
                                                                        if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= strtotime("now"))
                                                                        $sDisable='disabled';
                                                                    ?>
                                                                    @foreach($oFeed->polloption as $option)
                                                                        <?php
                                                                            $sSelected='';
                                                                            if($oFeed->id_poll_option && $option->id_poll_option==$oFeed->id_poll_option)
                                                                                $sSelected="checked";
                                                                        ?>

                                                                        <div class="radio radio-primary clearfix">
                                                                            <input type="radio" name="id_poll_option_{{ $oFeed->id_post }}" id="option{{$option->id_poll_option}}" value="{{$option->id_poll_option}}" {{ $sSelected }} {{$sDisable}} />
                                                                            <label for="option{{$option->id_poll_option}}" class="">
                                                                                <span class="poll-option-tag">{{ $sChar }}</span>
                                                                                <span class="poll-option-text">{{$option->option_text}} </span>
                                                                                @if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= strtotime("now"))
                                                                                <div class="poll-result">
                                                                                    <span><p>
                                                                                        @if($oFeed['pollanswer_'.$option->id_poll_option])
                                                                                            {{$oFeed['pollanswer_'.$option->id_poll_option]}} %
                                                                                        @else
                                                                                            0 %
                                                                                        @endif
                                                                                    </p></span>
                                                                                 </div>
                                                                                @endif
                                                                            </label>
                                                                        </div>
                                                                    <?php $sChar++; ?>
                                                                    @endforeach
                                                                </div>
                                                                @elseif($oFeed->poll_type == config('constants.POLLTYPEOPEN') && strtotime("now") >= strtotime($oFeed->start_time) && $oFeed->activated == 1)
                                                                <span class="error_message" id="poll_answer_text_{{ $oFeed->id_post }}"></span>
                                                                <div class="post-poll-options mb-10 clearfix">
                                                                    @if(empty($oFeed->id_poll_answer) && strtotime($oFeed->end_time) > strtotime("now"))
                                                                    <textarea autocomplete="off" class="input form-control animated" i  name="poll_answer_text_{{ $oFeed->id_post }}" type="text" placeholder="{{ trans('messages.type_your_answer') }}"  maxlength="255" ></textarea>
                                                                    @else
                                                                        @if(!empty($oFeed->poll_answer_description))
                                                                        <div class="show-poll-short-answer">{{ $oFeed->poll_answer_description }}</div>
                                                                        @else
                                                                        <div class="show-poll-short-answer no-poll-answer">{{ trans('messages.no_poll_answer') }}</div>
                                                                        @endif
                                                                    @endif
                                                                </div>
                                                                @endif
                                                                @if(strtotime("now") >= strtotime($oFeed->start_time) && $oFeed->id_poll_answer =='' && strtotime($oFeed->end_time) >= strtotime("now") && $oFeed->activated == 1)
                                                                <button type="button" class="btn btn-primary btn-md btn-poll" data-id='{{$oFeed->id_poll}}' data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.save') }}</button>
                                                                @elseif($oFeed->activated == 0)
                                                                <button type="button" class="btn btn-primary btn-md" onclick="activatePoll(this,'{{$oFeed->id_poll}}');" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.publish') }}</button>
                                                                @endif
                                                            </form>
                                                            <div class="pull-right view-poll-answer">

                                                                @if($oFeed->id_user == Auth::user()->id_user && $oFeed->totalAns!=0)
                                                                    <a href="javascript:void(0);" onclick="showPollDetail({{ $oFeed->id_post }});" class="">
                                                                @endif

                                                                @if($oFeed->totalAns == 1)
                                                                    <span>{{ $oFeed->totalAns }} {{ trans('messages.vote') }} </span>
                                                                @elseif($oFeed->totalAns > 1)
                                                                    <span>{{ $oFeed->totalAns }} {{ trans('messages.votes') }} </span>
                                                                @endif

                                                                @if($oFeed->id_user == Auth::user()->id_user && $oFeed->totalAns!=0)
                                                                </a>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <?php
                                            break;
                                        default :
                                            //Code for text/code
                                            echo 'No file found';
                                    }
                                ?>
                            @endif
                        </div>
                        @if($oFeed->post_type != config('constants.POSTTYPEPOLL'))
                        <!-- Div for post like button -->
                        <div class="post-likes like_button_{{ $oFeed->id_post }}">
                            <div class="btn btn-like {{ ($oFeed->id_like) ? "btn-like-user" : "" }}" onclick="updatePostLike('like_button_{{$oFeed->id_post}}','{{$oFeed->id_post}}');"></div>
                            @if($oFeed->likes > 0)
                                <a href="javascript:void(0);" onclick="getLikeUserList({{ $oFeed->id_post }});"><span>{{$oFeed->likes}}</span></a>
                            @endif
                        </div>
                        @endif
                    </div>
                </div>
                
                @if($oFeed->post_type != config('constants.POSTTYPEPOLL'))
                <!-- Comment listing -->
                <div class="comment-box {{ (count($oFeed->comments) > 0) ? '' : 'hidden' }}" id="comment-box-{{$oFeed->id_post}}">
                    <div class="comments-number closed" >
                        {!! trans('messages.comments', ['comment_count' => $oFeed->comment_count,'post' => $oFeed->id_post]) !!}
                        <span class="glyphicon glyphicon-chevron-down"></span>
                        <span class="glyphicon glyphicon-chevron-up"></span>
                    </div>
                    <div id="ext_comments_{{$oFeed->id_post}}">
                        @if(count($oFeed->comments) > 0)
                            <!-- extra comment strats here-->
                            @foreach($oFeed->comments as $oComment)
                                <div class="row post-comment padding-10">
                                    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 no-padding">
                                            <div class="icon-image img-35 ">
                                                {!! setProfileImage("50",$oComment->user_profile_image,$oComment->id_user,$oComment->first_name,$oComment->last_name) !!}
                                            </div>
                                    </div>
                                    <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 no-padding clearfix" >
                                            <div class="comment-person-name no-padding">{{ $oComment->first_name.' '.$oComment->last_name }} 
                                                    <span class="comment-time">{{ secondsToTime($oComment->created_at) }}</span>
                                                    <span class="comment-links pull-right">
                                                        <img src="{{asset('assets/web/img/dropdown-icon.png')}}" class="post-dropdown" alt="dropdown icon" data-toggle="dropdown" />
                                                        <ul class="dropdown-menu report-post">
                                                            @if($oComment->id_user == Auth::user()->id_user)
                                                                <li onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ["record_name" => "comment"]) }}','{{route('post.delete-post-comment',[$oComment->id_comment])}}');">{{ trans('messages.delete') }}</li>
                                                            @else
                                                                <li class="report">{{ trans('messages.report') }}</li>
                                                                <li class="report-post-form">
                                                                    <form id="comment_report_{{$oComment->id_comment}}" method="post" onsubmit="submitReport( 'comment_report_{{$oComment->id_comment}}','{{ route('post.post-add-report')}}',this);return false;">
                                                                        <textarea name="report_text" placeholder="{{ trans('messages.report_placeholder') }}"></textarea>
                                                                        <input type="hidden" name="id_entity" value="{{$oComment->id_comment}}" />
                                                                        <input type="hidden" name="entity_type" value="C" />
                                                                        <input type="button" class="btn btn-primary" value="{{ trans('messages.submit') }}" onclick="submitReport( 'comment_report_{{$oComment->id_comment}}','{{ route('post.post-add-report')}}',this)" />
                                                                        <input type="reset" value="{{ trans('messages.cancel') }}" class="remove-form btn btn-default"/>
                                                                    </form>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </span>
                                            </div>
                                            <div class="comment-content">
                                                    <div class="comment-text">
                                                            <p>{!! setTextHtml($oComment->comment_text) !!}</p>
                                                    </div>
                                                    @if($oComment->file_name)
                                                    <?php
                                                    $sHtmlString = '';
                                                    switch ($oComment->comment_type)
                                                    {
                                                        case config('constants.POSTTYPEIMAGE'):
                                                            $sHtmlString = '<div class="comment-img">';
                                                                $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['c_'.$oComment->id_comment,$oComment->display_file_name]).'">';
                                                                    $sHtmlString .= '<img src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'">';
                                                                $sHtmlString .= '</a>';
                                                            $sHtmlString .= '</div>';

                                                            echo $sHtmlString;
                                                            break;
                                                        case config('constants.POSTTYPEVIDEO'):

                                                            break;
                                                        case config('constants.POSTTYPEDOCUMENT'):
                                                            $sHtmlString = '<div class="comment-doc">';

                                                            $aFileNameData = explode('.', $oComment->file_name);

                                                            //Div for file icon
                                                            if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                                            {
                                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                                            }
                                                            elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                                            {
                                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                                            }
                                                            elseif (in_array(end($aFileNameData), array('key','ppt','pps')))
                                                            {
                                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                                            }
                                                            elseif (in_array(end($aFileNameData), array('zip','rar')))
                                                            {
                                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                                            }
                                                            elseif (end($aFileNameData) == 'pdf')
                                                            {
                                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                                            }

                                                            //Div for file details and view/download option
                                                            $sHtmlString .= '<span class="">'.(($oComment->display_file_name) ? $oComment->display_file_name : $oComment->file_name);
                                                            $sHtmlString .= '<br>';
                                                                //$sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['sFileName' => $oComment->file_name]).'">'.trans('messages.view').'</a>';
                                                                $sHtmlString .= '<a class="" target="blank" href="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'" download="'.$oComment->display_file_name.'">'.trans('messages.download').'</a>';
                                                            $sHtmlString .= '</span>';
                                                            $sHtmlString .= '</div>';

                                                            echo $sHtmlString;
                                                            break;
                                                        default :
                                                            //Code for text/code
                                                            echo 'No file found';
                                                    }
                                                    ?>
                                                    @endif
                                            </div>
                                    </div>
                                </div>        
                            @endforeach
                            <!-- extra comment ends here-->
                        @endif
                    </div>
                </div>
                <!-- Div for add comment -->
                <div class="user-comment-box">
                    <div class="padding-10 clearfix">
                        <form id="add_comment_{{ $oFeed->id_post }}" class="clearfix add_comment" name="add_comment" action="{{ route('post.add-comment') }}" method="POST" enctype="multipart/form-data" class="add_comment">
                            {!! csrf_field() !!}
                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                                <div class="img-35 display-ib v-align">
                                    {!! setProfileImage("50",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                                </div>
                                <input type="hidden" name="id_post" value="{{ $oFeed->id_post }}">
                                <input id="comment_type_{{ $oFeed->id_post }}" name="comment_type" type="hidden" value="{{ (old('comment_type_'.$oFeed->id_post)) ? old('comment_type_'.$oFeed->id_post) : config('constants.POSTTYPETEXT') }}">
                                <div class="input-group v-align">
                                    <textarea type="text" class="form-control animated" name="comment_text_{{ $oFeed->id_post }}" placeholder="{{ trans('messages.add_comment') }}" aria-describedby="basic-addon1">{{old('comment_text_'.$oFeed->id_post)}}</textarea>
                                    <!--<input type="text" class="form-control" name="comment_text_{{ $oFeed->id_post }}" value="{{old('comment_text_'.$oFeed->id_post)}}" placeholder="{{ trans('messages.add_comment') }}" aria-describedby="basic-addon1"> -->
                                    <span class="error_message" id="comment_text_{{ $oFeed->id_post }}"></span>

                                    <span class="comment-attchment">
                                        <label><img class="no-border" src="{{ asset('assets/web/img/attachment-icon.png') }}" onclick="setPostCommentType('{{ config('constants.POSTTYPEDOCUMENT') }}','{{ $oFeed->id_post }}');" data-id='{{$oFeed->id_post}}' /></label>
                                        <label><img src="{{ asset('assets/web/img/camera-icon.png') }}" onclick="setPostCommentType('{{ config('constants.POSTTYPEIMAGE')}}','{{ $oFeed->id_post }}');" data-id='{{$oFeed->id_post}}' /></label>
                                    </span>
                                    <div class="input-group" id="file-input">
                                        <label class="input-group-btn file-attachment-btn">
                                            <span class="btn btn-primary">
                                                Browse… <input type="file" name="file_{{ $oFeed->id_post }}"   multiple="">
                                            </span>
                                        </label>
                                        <input type="text" class=" file-attachment-name" readonly="">
                                        <span class="file-attachment-cancel" onclick="setPostCommentType('{{ config('constants.POSTTYPETEXT')}}','{{ $oFeed->id_post }}');" data-id="{{ $oFeed->id_post }}">{{ trans('messages.cancel') }}</span>
                                    </div>  
                                    <span class="error_message" id="comment_text_{{ $oFeed->id_post }}"></span>
                                    @if($errors->has('file_'.$oFeed->id_post))
                                        <span class="error_message">{{ $errors->first('file_'.$oFeed->id_post) }}</span>
                                    @endif 

                                </div>
                                 <button type="button" class="btn btn-primary btn-md btn-comment v-align" data-id='{{$oFeed->id_post}}' id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.send') }}</button>
                            </div>
                           
                        </form>
                    </div>
                </div>
                @endif
            </div>
        </div>
        @else
        <div id="feeds" class="clearfix feeds post">
            <div class="deleted-data">
                <img class="" src="{{asset('assets/web/img/deleted_post.png')}}">
            {{ trans('messages.post_deleted') }}
            </div>
            
        </div>
        @endif
    </div>
</div>
@section('footer-scripts')
    
    function showRequestComment(formData, jqForm, options)
    { 
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
    }
    
    function showResponseComment(response, statusText, xhr, $form)
    { 
        if(response.success == true)
        {
            if(response.type=='poll'){
                $('#'+response.id_post).html(response.html);
            }
            else
            {
                $('#add_comment_'+ response.id_post)[0].reset();
                var count = $('#count_'+ response.id_post).text();
                $('#count_'+ response.id_post).text(parseInt(count)+1);
                $('#comment-box-'+ response.id_post).removeClass('hidden');
                $('#ext_comments_'+response.id_post).append(response.html);
                removeErrorComment(response.id_post);
            }
            $('.btn-md').button('reset');
            setAutosizeTextAreaHeight(30);
        }
        else
        { 
            var arr = response;
                $.each(arr, function(index, value)
                {
                        if (value.length != 0)
                        {
                            $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                        }
                });                     
        }
    }
    
    function showErrorComment(xhr, textStatus, errorThrown)
    {
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
            }
        
        var result = $.parseJSON(arr);
        $.each(result, function(key, value)
        {
            key=key.replace('file','comment_text');
            $('.btn-md').button('reset');
            if (key.length != 0)
            {
                    $("#"+key).html('<strong>'+ value +'</strong>');
            }
        });
    }
    
    function removeErrorComment(id)
    {
        $("#comment_text_"+id).html('');
    }
    
    $(document).ready(function(){
        setAutosizeTextAreaHeight(30);
        callAutoSizeTextArea();
        $(".comment-attchment label:first-child").click(function(){
              $(this).parent().siblings("#file-input").addClass("show-input");
              $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
              $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any document ");
              
        });
        $(".comment-attchment label:last-child").click(function(){
            var id='file_'+$(this).attr('data-id');
            $(this).parent().siblings("#file-input").addClass("show-input");
            $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
            $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any photos ");
              
        });
        $(".file-attachment-cancel").click(function(){
            var id = $(this).attr('data-id');
            $('#file_'+id).val('').clone(true);
            $(this).parent().removeClass("show-input");
            input = $(".file-attachment-name").val('');
        });
        
        var options = { 
        beforeSubmit:  showRequestComment,
        success:       showResponseComment,
        error:showErrorComment,
        dataType: 'json' 
        }; 
        
        setInterval(function() {
            $(".minute").each(function(){
                var ele=$(this);
                var myTime = ele.val()-1;
                
                if(myTime <= 300 ){
                    var MinSec=getTime(myTime);
                    ele.siblings('span').text(MinSec);
                    ele.siblings('span').removeClass('hidden');
                    ele.siblings('div').addClass('hidden');
                }
                ele.val(myTime);

                if(myTime <= 0){
                    ele.parent().addClass('hidden');
                    var parent=ele.parent().parent().children('form');
                    parent.children('input[name=expired]').val(1);
                    parent.children('button').trigger( "click" );
                    
                }
            });
        },  1000);

        $(document).on('click','.btn-comment',function(e){
            var data = $(this);
            var id_post = data.attr('data-id');
            $(this).button('loading');
            $('#add_comment_'+id_post).ajaxForm(options).submit();
        });
        
        $(document).on('click','.btn-poll',function(e){
            var data = $(this);
            var id_poll = data.attr('data-id');
            $(this).button('loading');
            $('#poll_'+id_poll).ajaxForm(options).submit();
        });
        findUrlThumbnails();
    });
    $('.report').on('click', function(evt) {
        evt.stopImmediatePropagation();
        $(this).parent().addClass("containsForm");

   });
   $('.remove-form').on('click', function(evt) {
        evt.stopImmediatePropagation();
        $('.report-error').remove();
        $(this).parents(".report-post").removeClass("containsForm");
        $(this).parents(".edit-links").removeClass("open");

   });  
    
@endsection
@stop
