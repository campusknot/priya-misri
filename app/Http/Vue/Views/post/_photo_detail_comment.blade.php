<div class=" photo-detail-comment">
    <a href="{{route('user.profile',[$oComment->id_user])}}" class="icon-image img-35">
        {!! setProfileImage("50",$oComment->user_profile_image,$oComment->id_user,$oComment->first_name,$oComment->last_name) !!}
    </a>
    <div class="post-person-name">
        @if($oComment->id_user == Auth::user()->id_user)
            <span class="comment-links pull-right">
                <a href="javascript:void(0)" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ["record_name" => "comment"]) }}','{{route('post.delete-post-comment',[$oComment->id_comment])}}');">{{ trans('messages.delete') }}</a>
            </span>
        @endif
        <a href="{{route('user.profile',[$oComment->id_user])}}">{{ $oComment->first_name.' '.$oComment->last_name }}</a>
        <span class="comment-text">{!! setTextHtml($oComment->comment_text) !!}</span>
         @if($oComment->file_name)
    <div class="clearfix comment-content">
        <?php
            switch ($oComment->comment_type)
            {
                case config('constants.POSTTYPEIMAGE'):
                    echo '<div class="comment-img"> <a class="" target="blank" href="'.route('utility.view-file',['c_'.$oComment->id_comment,$oComment->display_file_name]).'"><img  src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'"> </a></div>';
                    break;
                case config('constants.POSTTYPEVIDEO'):

                    break;
                case config('constants.POSTTYPEDOCUMENT'):
                    $aFileNameData = explode('.', $oComment->file_name);

                    //Div for file icon
                    $sHtmlString = '<div class="comment-doc">';
                    if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                    {
                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                    }
                    elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                    {
                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                    }
                    elseif (in_array(end($aFileNameData), array('key','ppt','pps')))
                    {
                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                    }
                    elseif (in_array(end($aFileNameData), array('zip','rar')))
                    {
                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                    }
                    elseif (end($aFileNameData) == 'pdf')
                    {
                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                    }


                    //Div for file details and view/download option


                            $sHtmlString .= '<span>'.(($oComment->display_file_name) ? $oComment->display_file_name : $oComment->file_name);


                           // $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['sFileName' => $oComment->file_name]).'">'.trans('messages.view').'</a> </span>';

                                $sHtmlString .= '<a class="btn" target="blank" href="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'" download="'.$oComment->file_name.'">'.trans('messages.download').'</a>';

                    $sHtmlString .= '</div>';

                    echo $sHtmlString;
                    break;
                default :
                    //Code for text/code
                    echo 'No file found';
            }
        ?>
    </div>
    @endif
    </div>
</div>