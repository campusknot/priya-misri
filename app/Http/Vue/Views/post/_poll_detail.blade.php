<?php
    $oPostDetail=$oPostDetail['post_detail'];
?>
<div class="view-pollResult-bar">
    <h3>{{ trans('messages.poll_votes') }}
        <button type="button" onclick="hideSlider();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </h3>    
    <div class="pollResult-question-area">
        <div class="post-poll-question">
            <form method="post" action="{{ route('post.poll-answer-excel') }}" id="excel" class="download-excel-form">
                {!! csrf_field() !!}
                <input type="hidden" id="id_lecture_excel" name="id_poll" value="{{ $oPostDetail->id_poll }}"/>
                <input type="hidden" id="id_group" name="id_post" value="{{ $oPostDetail->id_post }}"/>
                <input type="hidden" name="poll_type" value="{{$oPostDetail->poll_type}}" >
                <input type="submit" value="{{ trans('messages.download_excel') }}" id="excel_download" class="">
            </form>
            <form id="poll_{{$oPostDetail->id_poll}}" action="{{ route('post.add-poll-answer') }}" method="POST">
                {!! csrf_field() !!}
                <input type="hidden" name="id_poll" value="{{$oPostDetail->id_poll}}" >
                <input type="hidden" name="id_post" value="{{$oPostDetail->id_post}}" >
                <input type="hidden" name="poll_type" value="{{$oPostDetail->poll_type}}" >
                <input type="hidden" name="expired" value="0" >
                <span class="error_message" id="id_poll_option_{{ $oPostDetail->id_post }}"></span>
                <p>{!! setTextHtml($oPostDetail->poll_text) !!}</p>
                @if($oPostDetail->file_name)
                <div class="post-poll-question-img">
                    <img src="{{ setPostImage($oFeed->poll_file_name,80) }}" onclick="showProfileImageLightBox('{{ setPostImage($oFeed->poll_file_name,80) }}');">
                </div>
                @endif
                <div class="post-poll-options clearfix">
                @if($oPostDetail->poll_type == config('constants.POLLTYPEMULTIPLE') && count($oPostDetail->polloption) > 0)
                    <?php $sChar = 'A';
                        $sDisable='';
                        if($oPostDetail->id_poll_answer !='' || strtotime($oPostDetail->end_time) <= time())
                        $sDisable='disabled';
                    ?>
                    @foreach($oPostDetail->polloption as $option)
                        <?php
                            $sPollOption[$option->id_poll_option]=$sChar;
                            $sSelected='';
                            if($oPostDetail->id_poll_option && $oPostDetail->id_poll_option==$option->id_poll_option)
                                $sSelected="checked";
                        ?>
                        <div class="radio radio-primary clearfix">
                            <input type="radio" name="id_poll_option_{{ $oPostDetail->id_post }}" id="option{{$option->id_poll_option}}" value="{{$option->id_poll_option}}" {{ $sSelected }} {{$sDisable}}/>
                            <label for="option{{$option->id_poll_option}}" class="">
                                <span class="poll-option-tag">{{ $sChar }}</span>
                                <span class="poll-option-text">{!! setTextHtml($option->option_text) !!} </span>
                                @if($oPostDetail->id_poll_answer !='' || strtotime($oPostDetail->end_time) <= time())
                                    <div class="poll-result">
                                        <span>
                                            <p>
                                            @if($oPostDetail['pollanswer_'.$option->id_poll_option])
                                                {{$oPostDetail['pollanswer_'.$option->id_poll_option]}} %
                                            @else
                                                0 %
                                            @endif
                                            </p>
                                        </span>
                                     </div>
                                @endif
                            </label>
                        </div>
                    <?php $sChar++; ?>
                    @endforeach
                @elseif($oPostDetail->poll_type == config('constants.POLLTYPEOPEN'))
                            @if(empty($oPostDetail->id_poll_answer) && strtotime($oPostDetail->end_time) > time())
                            <textarea autocomplete="off" class="input form-control animated" i  name="poll_answer_text" type="text" placeholder="{{ trans('messages.type_your_answer') }}"  maxlength="255" ></textarea>
                            @else
                                @if(!empty($oPostDetail->poll_answer_description))
                                
                                <div class="show-poll-short-answer">{{ $oPostDetail->poll_answer_description }}</div>
                                @endif
                            @endif      
                @endif
                    @if(strtotime($oPostDetail->start_time) < time() && $oPostDetail->id_poll_answer =='' && strtotime($oPostDetail->end_time) >= time())
                        <button type="button" class="btn btn-primary btn-md btn-poll" data-id='{{$oPostDetail->id_poll}}' id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >Save</button>
                    @endif
                </div>
            </form>
        </div>
    </div>
    <div class="poll-popup-votes">
        <img src="{{asset('assets/web/img/drag.png')}}" class="drag-img">
        <ul class="clearfix poll-vote heading">
            <li class="col-lg-9 col-sm-9 col-md-9 ">
                {{ trans('messages.users') }}
            </li>
            <li class="col-lg-3 col-sm-3 col-md-3 text-center">
                {{ trans('messages.votes') }} ( {{ $oPostDetail->totalAns }} )
            </li>
        </ul>
        @if(count($oPostDetail['pollanswer']) > 0)
        <div class="poll-vote-list">
           @foreach($oPostDetail['pollanswer'] as $oPollAns)
            <div class="poll-vote-person clearfix">
                <ul class="clearfix">
                     @if($oPostDetail->poll_type == config('constants.POLLTYPEMULTIPLE'))
                        <li class="col-lg-9 col-sm-9 col-md-9 pull-left">
                            <a href="{{route('user.profile',[$oPollAns->id_user])}}" class="icon-image img-35">
                            {!! setProfileImage("50",$oPollAns->user_profile_image,$oPollAns->id_user,$oPollAns->first_name,$oPollAns->last_name) !!}
                            </a>
                            <a href="#" class="poll-person-name">{{ $oPollAns->first_name.' '.$oPollAns->last_name }}</a>
                        </li>
                        <li class="col-lg-3 col-sm-3 col-md-3 text-center ">
                            {{ $sPollOption[$oPollAns->id_poll_option] }}
                        </li>
                     @else
                     <li class="col-lg-12 col-sm-12 col-md-12 pull-left">
                         <a href="{{route('user.profile',[$oPollAns->id_user])}}" class="icon-image img-35">
                        {!! setProfileImage("50",$oPollAns->user_profile_image,$oPollAns->id_user,$oPollAns->first_name,$oPollAns->last_name) !!}
                        </a>
                        <a href="#" class="poll-person-name">{{ $oPollAns->first_name.' '.$oPollAns->last_name }}</a>
                        <span class="poll-slider-short-answer">{{$oPollAns->poll_answer_description}}</span> 
                     </li>
                     @endif
<!--                    <li class="col-lg-10 col-sm-10 col-md-10 pull-left">
                        <a href="{{route('user.profile',[$oPollAns->id_user])}}" class="icon-image img-35">
                        {!! setProfileImage("50",$oPollAns->user_profile_image,$oPollAns->id_user,$oPollAns->first_name,$oPollAns->last_name) !!}
                        </a>
                        <a href="#" class="poll-person-name">{{ $oPollAns->first_name.' '.$oPollAns->last_name }}</a>
                         @if($oPostDetail->poll_type == config('constants.POLLTYPEOPEN'))
                        <span class="poll-slider-short-answer">{{$oPollAns->poll_answer_description}}</span> 
                        @endif
                    </li>
                    @if($oPostDetail->poll_type == config('constants.POLLTYPEMULTIPLE'))
                    <li class="col-lg-2 col-sm-2 col-md-2 text-center ">
                        
                            {{ $sPollOption[$oPollAns->id_poll_option] }}
                       
                        
                    </li>
                    @endif-->
                </ul>
            </div>
           @endforeach
        </div>
       @endif
    </div>
</div>
<script type="text/javascript">
    /*js for draggable poll question div*/
    $hheight = $( window ).height() - 130;
    $('.pollResult-question-area').resizable({
        handles: 'n,s',
        minHeight: 100,
        maxHeight: $hheight,
        alsoResize: ".poll-popup-votes"
    });
</script>
