<div class="edit-poll-content">

<div class="modal-header poll-popup-header">
    <button type="button" onclick="closeLightBox();" class="close poll-close-popup" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class=" event-title poll-header">{{ (isset($sPollType) && $sPollType=='edit') ? trans('messages.edit_poll') : trans('messages.copy_poll') }}</h4>
</div>

    
        
<div class="share-bg padding-10 clearfix">
    
    <div class="share clearfix">
        <div id="validation-errors"></div>
        <form id="add_post" name="add_post" action=" {{ route('post.add-poll') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  clearfix write-post" >
                <div class="icon-image img-50">
                    {!! setProfileImage("100",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                </div>
                <input id="id_post" name="id_post" type="hidden" value="{{ (isset($oPoll) && isset($sPollType) && $sPollType=='edit')?$oPoll->id_post:'' }}">
                <input id="id_poll" name="id_poll" type="hidden" value="{{ (isset($oPoll) && isset($sPollType) && $sPollType=='edit')?$oPoll->id_poll:'' }}">
                <input id="poll_file_old" name="poll_file_old" type="hidden" value="{{ isset($oPoll)?$oPoll->poll_file_name:'' }}">
                <input id="post_type" name="post_type" type="hidden" value="{{ config('constants.POSTTYPEPOLL') }}">
                <input id="poll_release_type" name="poll_release_type" type="hidden" value="P">
                <?php 
                    if(isset($oGroupDetails->group_name))
                        $sPlaceholder= trans('messages.post_placeholder',['name'=>$oGroupDetails->group_name]);
                    else
                        $sPlaceholder= trans('messages.post_placeholder',['name'=>'Followers']);
                    ?>
                
                <div class="input-group">
                    <textarea class="form-control poll-question" placeholder="{{ $sPlaceholder }}" id="post_text" name="poll_question" aria-describedby="basic-addon1">{{ isset($oPoll)?$oPoll->poll_text:old('poll_question') }}</textarea>
                    <span class="pos-relative select-poll-type display-ib">
                        <span class="caret caret-right"></span>
                        <select id="poll_type" name="poll_type" class="">
                            <option value="{{config('constants.POLLTYPEMULTIPLE')}}" {{((isset($oPoll) && $oPoll->poll_type == config('constants.POLLTYPEMULTIPLE')) || old('poll_type') == config('constants.POLLTYPEMULTIPLE')) ? 'selected' : ''}}>{{ trans('messages.multi_choise') }}</option>
                            <option value="{{config('constants.POLLTYPEOPEN')}}" {{((isset($oPoll) && $oPoll->poll_type == config('constants.POLLTYPEOPEN')) || old('poll_type') == config('constants.POLLTYPEOPEN')) ? 'selected' : ''}}>{{ trans('messages.open_ended') }}</option>
                        </select>
                    </span>
                    @if ($errors->has('poll_question'))
                       <span class="error_message"><?php echo $errors->first('poll_question'); ?></span>
                    @endif
                </div>
                <div id="add_code" class="clearfix m-t-3 {{ ($errors->has('post_textarea')) ? '' : 'hidden' }}">
                    <div class="input-group">
                        <textarea name="post_textarea" id="post_textarea" > </textarea>
                        @if ($errors->has('post_textarea'))
                           <span class="error_message"><?php echo $errors->first('post_textarea'); ?></span>
                        @endif
                    </div>
                </div>
                @if(isset($oPoll) && !empty($oPoll->poll_file_name))
                <div id="poll_image" class="post-poll-question-img pos-relative">
                    <img src="{{ setPostImage($oPoll->poll_file_name,80) }}" alt="{{trans('messages.poll_image')}}">
                    <span class="pull-right poll-remove-image cursor-pointer">{{trans('messages.remove_image') }}</span>
                </div>
                @endif
                <div id="add_file" class="clearfix m-t-3">
                    <div class="">
                        <div class="input-group file-attachment" id="file-input">
                            <label class="input-group-btn file-attachment-btn">
                                <span class="btn btn-primary">
                                    {{ trans('messages.browse') }} <input type="file" name="file" multiple="" accept=".bmp,.jpg,.jpeg,.png,.svg" id="file_form">
                                </span>
                            </label>
                            <input type="text" class=" file-attachment-name" readonly="" placeholder=" Share your pictorial query here ">
                            <span class="file-attachment-cancel" data-toggle="collapse" data-target="#add_document" >{{ trans('messages.cancel') }}</span>
                        </div>
                        @if ($errors->has('file'))
                            <span class="error_message"><?php echo $errors->first('file'); ?></span>
                        @endif
                    </div>
                </div>
                <div id="add_poll" class="add-poll clearfix">
                     <div class="controls" id="profs">
                        <?php
                            $sStyle = "display: block;";
                            $nDays = 0;
                            $nHours = 0;
                            $nMins = 0;
                            $nSec = 0;
                            if(isset($oPoll))
                            {
                                $dStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $oPoll->start_time);
                                $dEndTime = Carbon::createFromFormat('Y-m-d H:i:s', $oPoll->end_time);
                                $sTimeDiff = $dEndTime->diff($dStartTime)->format('%a:%h:%i:%s');
                                list($nDays, $nHours, $nMins,$nSec) = explode(":", $sTimeDiff);
                                
                                if($oPoll->poll_type == config('constants.POLLTYPEOPEN'))
                                    $sStyle = "display: none;";
                            }
                        ?>
                        <div class="input-append multiple-choise-poll clearfix text-right" style="{{$sStyle}}">
                            <div id="field" class="poll-options">
                                @if(isset($oPoll) && !empty($oPoll->poll_options))
                                    @foreach($oPoll->poll_options as $nKey => $oPollOption)
                                        <div class="new-poll-option">
                                            <textarea autocomplete="off" class="input form-control animated" id="field{{$nKey+1}}" name="options[{{$nKey+1}}]" type="text" placeholder="{{ trans('messages.type_your_answer') }}" data-items="8" maxlength="255">{{$oPollOption->option_text}}</textarea>
                                        </div>
                                    @endforeach
                                @else
                                <div class="new-poll-option">
                                    <textarea autocomplete="off" class="input form-control animated" id="field1" name="options[1]" type="text" placeholder="{{ trans('messages.type_your_answer') }}" data-items="8" maxlength="255" ></textarea>
                                </div>
                                <div class="new-poll-option">
                                    <textarea autocomplete="off" class="input form-control animated" id="field2" name="options[2]" type="text" placeholder="{{ trans('messages.type_your_answer') }}" data-items="8" maxlength="255"></textarea>
                                </div>
                                @endif
                            </div>
                            <span id="b1" class="add-more" type="button">{{ trans('messages.add_more_poll_option') }}</span>
                        </div>
                        
                        <div class="poll-expire-time">
                            {{ trans('messages.poll_duration') }}
                            <span class="display-ib">
                                <span class="poll-expire-day">
                                    <span class="caret"></span>
                                    <select  class="" name="day">
                                        <option value="">{{ trans('messages.day') }}</option> 
                                        @for ($i = 1; $i < 32; $i++)
                                           <option value='{{ $i }}' {{($nDays == $i)?'selected':''}}>{{ $i }}</option> 
                                        @endfor
                                    </select>
                                </span>
                                <span class="poll-expire-hour">
                                    <span class="caret"></span>
                                     <select   class="" name="hour">
                                         <option value="">{{ trans('messages.hour') }}</option> 
                                        @for ($i = 1; $i < 24; $i++)
                                           <option value="{{ $i }}" {{($nHours == $i)?'selected':''}}>{{ $i }}</option> 
                                        @endfor
                                    </select>
                                </span>
                                <span class="poll-expire-min">
                                    <span class="caret"></span>
                                    <select  class="" name="minutes">
                                        <option value="">{{ trans('messages.minute') }}</option>
                                        <option value="1" {{($nMins == 1)?'selected':''}}>1</option>
                                        <option value="2" {{($nMins == 2)?'selected':''}}>2</option>
                                        <option value="3" {{($nMins == 3)?'selected':''}}>3</option>
                                        <option value="4" {{($nMins == 4)?'selected':''}}>4</option>
                                        @for ($i = 5; $i < 56; $i=$i+5)
                                           <option value="{{ $i }}" {{($nMins == $i)?'selected':''}}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </span>
                                <span class="poll-expire-min">
                                    <span class="caret"></span>
                                    <select  class="" name="seconds">
                                        <option value="">{{ trans('messages.second') }}</option>
                                        @for ($i = 5; $i < 56; $i=$i+5)
                                           <option value="{{ $i }}" {{($nSec == $i)?'selected':''}}>{{ $i }}</option>
                                        @endfor
                                    </select>
                                </span>
                            </span>
                          
                        </div>
                        <div class="form-group clearfix">
                            <div class="place-holder pos-relative">
                                <select id="group_suggetions" class="js-data-example-ajax" data-placeholder="{{ trans('messages.select_group') }}" multiple="true" name="group_list[]" ></select>
                            </div>
                            <div id="selected_emails" class="auto-suggetion-outer"></div>
                            <div id="auto-suggetion-div">
                                @if(old('invite_to'))
                                    @foreach(old('invite_to') as $sInvity)
                                        <div id='vR_{{$sInvity}}' class='vR pull-left m-r-1'>
                                            <span email='{{$sInvity}}' class='vN'>
                                                <div class='vT invalid-email'>{{$sInvity}}</div>
                                                <div id='{{$sInvity}}' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>
                                            </span>
                                            <input type='hidden' value='{{$sInvity}}' name='invite_to[]'>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="attachment poll-post-options">
                    <div class="dropdown display-ib pull-right">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                        <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li id="schedule_poll_btn" class="cursor-pointer">
                                <span>{{ trans('messages.schedule_poll') }}</span>
                            </li>
                            <li id="save_poll_btn" class="cursor-pointer">
                                <span>{{ trans('messages.save_as_draft') }}</span>
                            </li>
                        </ul>
                    </div>
                    <button id="post_poll_btn" type="button" class="btn btn-primary btn-lg post-btn "  name="submit" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.post') }}</button>
                </div>
            </div>    
        </form>
    </div>
</div>
</div>

<script type="text/javascript">
    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    $(document).ready(function() {
        $(".dropdown-toggle").dropdown();
        setAutosizeTextAreaHeight(30);
        callAutoSizeTextArea();
        $(".poll-post-options .dropdown-toggle").click(function(){
            var scroll = $(window).scrollTop();
            
            if(scroll < 100) { //abuse 0 == false :)
                 $('html, body').animate({
                    scrollTop: 100
                }, 500);
            }
            $('.share-bg').animate({
                    scrollTop: 300
                }, 500);
        });
//        $(".dropdown-menu>li").click(function(){
//            $('html, body').animate({
//                    scrollTop: 80
//                }, 500);
//            
//        });
        
        /*poll type option selection*/
        $('#poll_type').change(function(){
            var v =$(this).val();
            if( v === 'O'){
                console.log('open');
                $(".multiple-choise-poll").css("display","none");
            }
            else{
                console.log('multi');
                $(".multiple-choise-poll").css("display","block");
            }
        });
        var countLength = $("#field").children().length;
        var next = countLength;
        $(".add-more").click(function(e){
            var count = $("#field").children().length;
            if(count < 10){
                e.preventDefault();
                var addto = "#field";
                //var addRemove = "#field" + (next);
                next = next + 1;
                var newIn = '<div class="new-poll-option"><textarea autocomplete="off" rows="1" placeholder="Type your option" class="input form-control animated" id="field' + next + '" name="options[' + next + ']" type="text" maxlength="255"></textarea><span id="remove' + (next) + '" class="remove-me"  >×</span></div>';
                $(addto).append(newIn);
                $("#field" + next).attr('data-source',$(addto).attr('data-source'));
                $("#field" + next).focus();
                $("#count").val(next);  
                if(count==9) {
                   $('.add-more').css('color','#ccc');
                }
            }
            
            $('.remove-me').on('click',function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).parent().remove();
                $(fieldID).remove();
                $('.add-more').css('color','#51a2cc');
            });
            e.stopImmediatePropagation();
        });
        
        var options = { 
            beforeSubmit:  showRequest,
            success:       showResponse,
            error:showError,
            dataType: 'json' 
        }; 
        $('#post_poll_btn').click(function(){
            $(this).button('loading');
            $('#poll_release_type').val('P');
            $('#add_post').ajaxForm(options).submit();
        });
        $('#save_poll_btn').click(function(){
            $('#post_poll_btn').button('loading');
            $('#poll_release_type').val('S');
            $('#add_post').ajaxForm(options).submit();
        });
        $('#schedule_poll_btn').click(function(){
            $('#post_poll_btn').button('loading');
            $('#poll_release_type').val('SN');
            $('#add_post').ajaxForm(options).submit();
        });
        
        $("#group_suggetions").select2({
            ajax: {
                type: 'POST',
                url: siteUrl + "/user/user-group-search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search_str: params.term
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    var processedResult = [];

                    var groups = $.map(data, function(item){
                        return { id: item.id_group, text: htmlSpecialChars(item.group_name), image: item.group_img };
                    });

                    if(groups && groups.length>0){
                        var group = { text: 'Group', children: groups, image: '' };
                        processedResult.push(group);
                    }
                    
                    return {
                        results: processedResult,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                error: function(data) {
                    if(data.status == 401)
                    {
                        window.location = siteUrl + '/home';
                    }
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        $('#group_suggetions').bind('keypress');
    });
    
    function formatRepo (repo) {
        $('#invitePlaceHolder').html('');
        if (repo.loading) return repo.text;

        var markup ="<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>";
                            if(repo.image !='') {
                                markup += "<span class='icon-image img-25'>"+repo.image+"</span>";
                            }
                            markup +="<span class='select2-result-repository__title'>" + repo.text + "</span>" +
                        "</div>"+
                    "</div>";

        return markup;
    }

    function formatRepoSelection (repo)
    {
        return repo.text || '';
    }
    
    function showRequest() {
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
    }
    
    function showResponse(response)  {
        
        if(response.success == true && response.html)
        {
            $('#post_poll_btn').button('reset');
            openLightBox(response.html);
        }
        else if(response.success == true)
        {
            if(response.redirect)
                window.location.href = response.redirect;
            else
                window.location.reload(true);
        }
        else { 
            var arr = response;
            $.each(arr, function(index, value)
            {
                if (value.length != 0)
                {
                    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                }
            });
            $("#validation-errors").show();
            $('#save_poll_btn').removeClass('disabled');
            $('#post_poll_btn').removeClass('disabled');
            $('.post-btn-save').button('reset');
            $('.post-btn').button('reset');
        }
    }
    
    function showError(xhr)  {
        console.log("add poll error call");
        if(xhr.status == 500 ){
            window.location = siteUrl + '/home';
        }
        var arr = xhr.responseText;
        try {
            var result = $.parseJSON(arr);
        
            $("#validation-errors").append('<div class="alert alert-error"><div>');
            $.each(result, function(index, value)
            {
                if (value.length != 0)
                {
                    $("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
                }
            });
            $("#validation-errors").show();
            $('#save_poll_btn').removeClass('disabled');
            $('#post_poll_btn').removeClass('disabled');
            $('.post-btn-save').button('reset');
            $('.post-btn').button('reset');
        } catch (e) {
            console.log(xhr.responseText);
        }
    }
    
    $('.file-attachment-cancel').click(function(){
       $(this).siblings(".file-attachment-name").val(''); 
        $('#file_form').val('');
    });
    
    //browse button set file name
    $(':file').on('fileselect', function(event, numFiles, label) {
        showCustomBrowseButton(event, numFiles, label, this);
    });
    
    $('.poll-remove-image').click(function() {
        $('#poll_image').remove();
        $('#poll_file_old').val('');
    });
    <?php
        if(isset($oPoll) && !empty($oPoll->id_group))
        {
    ?>
            var groupId = "<?php echo $oPoll->id_group;?>";
            var groupname = "<?php echo $oPoll->group_name;?>";
            $(function() {
                $('#group_suggetions').append($("<option />").val(groupId).text(groupname));
                $('#group_suggetions option[value="'+groupId+'"]').prop('selected', true);
                $('ul.select2-selection__rendered').prepend('<li class="select2-selection__choice tag-event-group" title="' + groupname + '"><span role="presentation" class="select2-selection__choice__remove">x</span>' + groupname + '</li>')
            });
    <?php
        }
    ?>
</script>