<div class="share-bg padding-10 clearfix">
    <div class="share clearfix">
         <div id="validation-errors"></div>
        <form id="add_post" name="add_post" action=" {{ route('post.add-post') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  clearfix write-post" >
                <div class="icon-image img-50">
                    {!! setProfileImage("100",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                </div>
                <input id="post_type" name="post_type" type="hidden" value="{{ config('constants.POSTTYPEIMAGE') }}">

                @if(isset($oGroupDetails))
                    <input id="id_group" name="id_group" type="hidden" value="{{ $oGroupDetails->id_group }}">
                @endif
                <div class="input-group">
                    <textarea class="form-control" placeholder="{{ trans('messages.post_placeholder',['name'=>$oGroupDetails->group_name] ) }}" id="post_text" name="post_text" aria-describedby="basic-addon1">{{ old('post_text') }}</textarea>
                    @if ($errors->has('post_text'))
                       <span class="error_message"><?php echo $errors->first('post_text'); ?></span>
                    @endif
                </div>
                <div id="add_code" class="clearfix m-t-3 {{ ($errors->has('post_textarea')) ? '' : 'hidden' }}">
                    <div class="input-group">
                        <textarea name="post_textarea" id="post_textarea" > </textarea>
                        @if ($errors->has('post_textarea'))
                           <span class="error_message"><?php echo $errors->first('post_textarea'); ?></span>
                        @endif
                    </div>
                </div>
                <div id="add_file" class="clearfix m-t-3">
                    <div class="">
                        <div class="input-group" id="file-input">
                            <label class="input-group-btn file-attachment-btn">
                                <span class="btn btn-primary">
                                    Browse <input type="file" name="file" multiple="" accept=".bmp,.jpg,.jpeg,.png,.svg" id="file_form">
                                </span>
                            </label>
                            <input type="text" class=" file-attachment-name" readonly="" placeholder="Share any photos">
                            <span class="file-attachment-cancel" data-toggle="collapse" data-target="#add_document" >Cancel</span>
                        </div>  
                      <!--   <input type="file" name="file" class="form-control" data-input="false" > -->
                        @if ($errors->has('file'))
                            <span class="error_message"><?php echo $errors->first('file'); ?></span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="attachment">
                    <button type="button" class="btn btn-primary btn-lg post-btn" name="save" id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.post') }}</button>
                </div>
            </div>    
        </form>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function($) {
        
    var options = { 
        beforeSubmit:  showRequest,
        success:       showResponse,
        error:showError,
        dataType: 'json' 
        }; 
        $('.post-btn').click(function(e){
            $(this).button('loading');
            $('#add_post').ajaxForm(options).submit();  		
        });
    });		
    function showRequest(formData, jqForm, options) { 
            $("#validation-errors").hide().empty();
            $("#output").css('display','none');
            return true; 
    } 
    function showResponse(response, statusText, xhr, $form)  { 
            if(response.success == true)
            {
                $('#add_post')[0].reset();
                $(".feeds").prepend(response.html);
                $('.post-btn').button('reset');
                location.reload();
                
            } else { 
                var arr = response;
                //alert(arr);
                    $.each(arr, function(index, value)
                    {
                            if (value.length != 0)
                            {
                                    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                            }
                    });
                    $("#validation-errors").show();
                   $('.post-btn').button('reset');
                   //location.reload();
                     
            }
    }
    function showError(xhr, textStatus, errorThrown)  {
        if(xhr.status == 500 ){
            window.location = siteUrl + '/home';
        }
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
               location.reload();
            }
        
        var result = $.parseJSON(arr);
        
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
            }
        });
        $("#validation-errors").show();
        $('.post-btn').button('reset');
        
    }
$('.file-attachment-cancel').click(function(){
   $(this).siblings(".file-attachment-name").val(''); 
    //$('#file_form').replaceWith( $("#file_form").clone() );
    $('#file_form').val('');
});
//browse button set file name
    $(':file').on('fileselect', function(event, numFiles, label) {
        showCustomBrowseButton(event, numFiles, label, this);
    });
</script>