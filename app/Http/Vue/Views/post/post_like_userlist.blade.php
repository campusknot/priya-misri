<div class="modal-content post-like-users-popup">
    <div class="modal-header">
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title ">
            {{ count($oUserList) }}
            @if(count($oUserList) ==1)
                {{ trans('messages.user_graded it') }}
            
            @else
                {{ trans('messages.users_graded it') }}
            
            @endif
        </h4>
    </div>
    <div class="modal-body clearfix">
        
        <ul class="post-like-users">
            @if(count($oUserList))
                @foreach($oUserList as $oUser)
                <li class="">
                    <span class="icon-image img-35" data-toggle="tooltip" title="">
                        {!! setProfileImage("50",$oUser->user_profile_image, $oUser->id_user, $oUser->first_name, $oUser->last_name) !!}
                    </span>
                    <span class="user-name display-ib v-align-top">{{ $oUser->first_name.' '.$oUser->last_name }}
                        <div class="user-major">
                            @if(count($oUser->major)>0)
                                {{$oUser->major['degree']}} {{ ($oUser->major['major']) ? ','.$oUser->major['major'] : ''}}
                            @endif
                        </div>
                    </span>
                    @if($oUser->id_user != Auth::user()->id_user)
                        <span class="pull-right">
                        @if($oUser->id_user_follow!='')
                        <a href="javascript:void(0);" class=" follow" onclick="callFollowUser(this,'{{ route("user.un-follow-user", ['user' => $oUser->id_user])}}',1,{{ $oUser->id_user }})">{{ trans('messages.unfollow') }}</a>
                        @else
                            <a href="javascript:void(0);" class=" follow" onclick="callFollowUser(this,'{{ route("user.follow-user", ['user' => $oUser->id_user])}}',0,{{ $oUser->id_user }})">{{ trans('messages.follow') }}</a>
                        @endif
                        </span>
                     @else
                        <span class="no-action"></span>    
                    @endif    
                </li>
                @endforeach
            @endif
        </ul>
    
    </div>
    
</div>




