@if(count($oFeed->comments) > 0)
    <div class="row post-comment padding-10">
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-1 no-padding">
                    <div class="icon-image img-35">
                        {!! setProfileImage("50",$oFeed->comments->user_profile_image,$oFeed->comments->id_user,$oFeed->comments->first_name,$oFeed->comments->last_name) !!}
                    </div>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-11 sml-no-pad no-padding clearfix" >
                    <div class="comment-person-name no-padding">{{ $oFeed->comments->first_name.' '.$oFeed->comments->last_name }} 
                            <span class="comment-time">{{ secondsToTime($oFeed->comments->created_at) }}</span>
                            <span class="comment-links pull-right">
                                <img src="{{asset('assets/web/img/dropdown-icon.png')}}" class="post-dropdown" alt="dropdown icon" data-toggle="dropdown" />
                                <ul class="dropdown-menu report-post">
                                    @if($oFeed->comments->id_user == Auth::user()->id_user)
                                        <li onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ["record_name" => "comment"]) }}','{{route('post.delete-post-comment',[$oFeed->comments->id_comment])}}');">Delete</li>
                                    @else
                                        <li class="report">{{ trans('messages.report') }}</li>
                                        <li class="report-post-form">
                                            <div class="text-left report-post-header">{{ trans('messages.report_heading') }}</div>
                                            <form id="comment_report_{{$oFeed->comments->id_comment}}" method="post" onsubmit="submitReport( 'comment_report_{{$oFeed->comments->id_comment}}','{{ route('post.post-add-report')}}',this);return false;">
                                                <textarea name="report_text" placeholder="{{ trans('messages.report_placeholder') }}"></textarea>
                                                <input type="hidden" name="id_entity" value="{{$oFeed->comments->id_comment}}" />
                                                <input type="hidden" name="entity_type" value="C" />
                                                <input type="button" class="btn btn-primary" value="{{ trans('messages.submit') }}" onclick="submitReport( 'comment_report_{{$oFeed->comments->id_comment}}','{{ route('post.post-add-report')}}',this)" />
                                                <input type="reset" value="{{ trans('messages.cancel') }}" class="remove-form btn btn-default"/>
                                            </form>
                                        </li>
                                    @endif
                                </ul>
                            </span>
                    </div>
                    <div class="comment-content">
                            <div class="comment-text">
                                    <p class="more commentAjaxMore">{!! auto_link_text(htmlspecialchars($oFeed->comments->comment_text)) !!}</p>
                            </div>
                            @if($oFeed->comments->file_name)
                            <?php
                                    $sHtmlString = '';
                                    switch ($oFeed->comments->comment_type)
                                    {
                                        case config('constants.POSTTYPEIMAGE'):
                                            $sHtmlString = '<div class="comment-img">';
                                                $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['c_'.$oFeed->comments->id_comment,$oFeed->comments->display_file_name]).'">';
                                                    $sHtmlString .= '<img src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oFeed->comments->file_name.'">';
                                                $sHtmlString .= '</a>';
                                            $sHtmlString .= '</div>';

                                            echo $sHtmlString;
                                            break;
                                        case config('constants.POSTTYPEVIDEO'):

                                            break;
                                        case config('constants.POSTTYPEDOCUMENT'):
                                            $sHtmlString = '<div class="comment-doc">';

                                            $aFileNameData = explode('.', $oFeed->comments->file_name);

                                            //Div for file icon
                                            if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                            }
                                            elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                            }
                                            elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                            }
                                            elseif (in_array(end($aFileNameData), array('zip','rar')))
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                            }
                                            elseif (end($aFileNameData) == 'pdf')
                                            {
                                                $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                            }

                                            //Div for file details and view/download option
                                            $sHtmlString .= '<span class="">'.(($oFeed->comments->display_file_name) ? $oFeed->comments->display_file_name : $oFeed->comments->file_name);
                                            $sHtmlString .= '<br>';
                                                $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['c_'.$oFeed->comments->id_comment,$oFeed->comments->display_file_name]).'">'.trans('messages.view').'</a>';
                                                $sHtmlString .= '<a class="" target="blank" href="'.route('utility.download-file',['c_'.$oFeed->comments->id_comment,$oFeed->comments->display_file_name]).'">'.trans('messages.download').'</a>';
                                            $sHtmlString .= '</span>';
                                            $sHtmlString .= '</div>';

                                            echo $sHtmlString;
                                            break;
                                        default :
                                            //Code for text/code
                                            echo 'No file found';
                                    }
                                ?>
                            @endif
                    </div>
            </div>
    </div>
@endif
<script type="text/javascript">
    /*js forshow less and more image caption*/
        
        showMoreAmountdata(200,'commentAjaxMore');
        
        $(".commentAjaxMore .morelink").on("click", function(e){
           setMoreAmountDataLabel(this);
           e.preventDefault();
        });
</script>