<!-- Modal content-->
<div class="modal-content add-group-popup">
    <div class="modal-header">
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
         <h4 class="modal-title event-title">
             {{ trans('messages.schedule_poll') }}
         </h4>
    </div>
    <div class="modal-body clearfix">
        <div id="schedule-validation-errors"></div>
        <form id="schedule_post" name="schedule_post" action=" {{ route('post.add-poll') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <input type="hidden" name="id_post" value="{{!empty($aFormData['id_post'])?$aFormData['id_post']:''}}">
            <input type="hidden" name="id_poll" value="{{!empty($aFormData['id_poll'])?$aFormData['id_poll']:''}}">
            <input type="hidden" name="poll_file_old" value="{{!empty($aFormData['poll_file_old'])?$aFormData['poll_file_old']:''}}">
            <input type="hidden" name="post_type" value="{{ config('constants.POSTTYPEPOLL') }}">
            <input type="hidden" name="poll_release_type" value="SC">
            <input type="hidden" name="poll_type" value="{{$aFormData['poll_type']}}">
            <input type="hidden" name="poll_question" value="{{$aFormData['poll_question']}}">
            <input type="hidden" name="duration" value="{{isset($aFormData['duration']) ? $aFormData['duration'] : ''}}">
            <input type="hidden" name="file_path" value="{{isset($aFormData['file_path']) ? $aFormData['file_path'] : ''}}">
            <?php
                foreach ($aFormData['options'] as $sKey=>$sOption)
                {
            ?>
                    <input type="hidden" name="options[{{$sKey}}]" value="{{$sOption}}">
            <?php
                }
            ?>
            <?php
            if(isset($aFormData['group_list']))
            {
                foreach ($aFormData['group_list'] as $sKey=>$sGroup)
                {
            ?>
                    <input type="hidden" name="group_list[{{$sKey}}]" value="{{$sGroup}}">
            <?php
                }
            }
            ?>
            <span class="Col-lg-2 col-md-2 col-sm-2 col-xs-2"></span>
            <span class="datepicker display-ib pos-relative col-lg-8 col-md-8 col-sm-8 col-xs-8 mb-10">
                <input type="text"  id="poll_start_date" name="schedule_date" value="{{old('schedule_date')}}" class="" placeholder="{{ trans('messages.select_date') }}">
                <select class="time-hour" name="schedule_hour" title="" data-toggle="tooltip" data-placement="top">
                    <option value="">hh</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
                <select class="time-minutes" name="schedule_minute" title="" data-toggle="tooltip" data-placement="top">
                    <option value="" selected="">mm</option>
                    <option value="05">05</option>
                    <option value="10">10</option>
                    <option value="15">15</option>
                    <option value="20">20</option>
                    <option value="25">25</option>
                    <option value="30">30</option>
                    <option value="35">35</option>
                    <option value="40">40</option>
                    <option value="45">45</option>
                    <option value="50">50</option>
                    <option value="55">55</option>
                </select>
                <input id="time_start_am" type="radio" class="hidden" name="schedule_ampm" value="am" checked="">
                <input id="time_start_pm" type="radio" class="hidden" name="schedule_ampm" value="pm">
                <label for="time_start_am" class="time-start-am">AM</label>
                <label for="time_start_pm" class="time-start-pm">PM</label>
            </span>
            <span class="Col-lg-2 col-md-2 col-sm-2 col-xs-2"></span>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <button id="post_schedule_poll_btn" class="btn btn-primary lg-center-button">{{ trans('messages.save') }}</button>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $("#poll_start_date").datepicker({
         dateFormat: 'mm-dd-yy',
         changeYear: true,
        changeMonth: true,
          yearRange: 'c-10:c+3',
            minDate: 0
    });
    
    function showScheduleRequest() {
        $("#schedule-validation-errors").hide().empty();
        return true; 
    }
    
    function showScheduleResponse(response)  {
        
        if(response.success == true)
        {
            if(response.redirect)
                window.location.href = response.redirect;
            else
                window.location.reload(true);
        }
        else { 
            console.log("Success called");
            var arr = response;
            $.each(arr, function(index, value)
            {
                if (value.length != 0)
                {
                    $("#schedule-validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                }
            });
            $("#schedule-validation-errors").show();
            $('#post_schedule_poll_btn').button('reset');
        }
    }
    
    function showScheduleError(xhr) {
        if(xhr.status == 500 ){
            window.location = siteUrl + '/home';
        }
        var arr = xhr.responseText;
        try {
            var result = $.parseJSON(arr);
        
            $("#schedule-validation-errors").html('<div class="alert alert-error"></div>');
            $.each(result, function(index, value)
            {
                if (value.length != 0)
                {
                    $("#schedule-validation-errors .alert").append('<strong>'+ value +'</strong><br />');
                }
            });
            $("#schedule-validation-errors").show();
            $('#post_schedule_poll_btn').button('reset');
        } catch (e) {
            console.log(xhr.responseText);
        }
    }
    
    var options = { 
        beforeSubmit: showScheduleRequest,
        success: showScheduleResponse,
        error: showScheduleError,
        dataType: 'json' 
    }; 

    $('#post_schedule_poll_btn').click(function(e){
        e.preventDefault();
        $('#post_schedule_poll_btn').button('loading');
        $('#schedule_post').ajaxForm(options).submit();
    });
</script>