<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<title>Newsletter</title>
<style>
img {max-width:100%; vertical-align: middle;}
@media only screen and (max-width: 640px){
.table-email td { display: inline-block !important;
    padding: 7px 10px !important;
    text-align: left;
    width: 100%;}
.table-email td p {word-wrap:break-word; font-size:11px !important; }
.table-email td.menu a {padding:12px 10px !important;}
.heading {overflow:hidden;}
.table-email table{ width:100%;}
p{ padding-left:0 !important; }
.table-email{ max-width:100% !important;}
.menu > a{ font-size:12px !important;}
.heading > img { width: 100%;}
.menu > a:hover{ background:#3cc4b6; color:#fff !important;}
.instruction p{ margin:0 !important;}
}
</style>
</head>

<body style="margin:0; padding:0;">
<table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="ffffff" style="width:100%; max-width:650px;" class="table-email">
    <tr bgcolor="f8fafc">
    	<td style="font-size:22px; font-family:Arial, Helvetica, sans-serif; color:#fff; padding:7px 17px; border-bottom:1px solid #ebeef2;">
            <img src="https://s3.amazonaws.com/campusknot/file/icon/logo.png" style="width:100px;" alt="{{trans('messages.campusknot_logo')}}" />
	</td>
    </tr>
    <tr>
    	<td colspan="2" align="center" class="heading"><span style="font-family:Georgia, 'Times New Roman', Times, serif; color:#383838; font-size:22px; margin-bottom:10px; font-style:italic; margin-top:28px; display:block; text-align:center;">{{ trans('messages.password_reset') }}</span>
            <img src="https://s3.amazonaws.com/campusknot/file/icon/bdr-btm.png" alt=""  />
        </td>
    </tr>
     <tr>
    	<td colspan="2" style="padding-left:20px;">
            <p style="color:#355871; font-size:20px; font-family:Lato, sans-serif; margin:10px 0;"> <span style="color:#678baf;">
                    Date: </span>
                <span>  {{ $aInfo['date'] }}</span>
            </p>
        </td>
    </tr>
     <tr>
    	<td colspan="2" style="padding-left:20px;">
            <p style="color:#355871; font-size:20px; font-family:Lato, sans-serif; margin:10px 0;"> <span style="color:#678baf;"> Your password reset from <br/>
                    Browser: </span>
                <span>  {{ $aInfo['name']}} / {{ $aInfo['version'] }}</span>
            </p>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding-left:20px;">
            <p style="color:#355871; font-size:20px; font-family:Lato, sans-serif; margin:10px 0;"> <span style="color:#678baf;"> Operating System: </span>
                <span>  {{ $aInfo['platform'] }}</span>
            </p>
        </td>
    </tr>
    
    <tr>
        <td colspan="2" style="padding-left:20px;">
            <p style="color:#355871; font-size:20px; font-family:Lato, sans-serif; margin:10px 0;"> <span style="color:#678baf;"> Approximate Location: </span>
                <span> {{ $aInfo['location'] }}</span>
            </p>
        </td>
    </tr>
</table>
</body>
</html>