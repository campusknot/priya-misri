<?php

namespace App\Http\Web\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class AddEventFeedRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if($this->request->get('post_type') == 'I'){
            return [
                'post_text' => 'required',
                'file' => 'required|image|mimes:bmp,jpg,jpeg,png,svg|max:10000'
            ];
        } else {
            return [
                'post_text' => 'required'
            ];
        }
    }

}
