<?php

namespace App\Http\Web\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;

class AddEventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $carbon = new Carbon();
                
        return [
                'event_title' => 'required',
                'event_description' => 'required',
                'start_date' => 'required',               
                'end_date' => 'required',
                'address' => 'required',
                'latitude' => 'required',
                'longitude' => 'required',
                'room_number' => 'required'                
            ];        
    }
}
