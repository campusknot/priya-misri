<?php

namespace App\Http\Web\Providers;

use Illuminate\Support\ServiceProvider AS ServiceProvider;

class SHAHashServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->app['hash'] = $this->app->share(function () {
            return new \App\Libraries\SHAHasher();
        });

    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {
        return array('hash');
    }

}