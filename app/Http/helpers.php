<?php

function secondsToTime($sToDate,$bReverseDifference=0,$sFromDate='') {
    $dToDate = new DateTime($sToDate);
    $dFromDate = new DateTime(date('Y-m-d H:i:s', time()));
    if(!empty($sFromDate))
        $dFromDate = new DateTime($sFromDate);
    if($bReverseDifference == 1)
        $oTimeDifference = $dToDate->diff($dFromDate);
    else
        $oTimeDifference = $dFromDate->diff($dToDate);
    if($oTimeDifference->y)
        return trans('messages.post_time',['time' => $oTimeDifference->y.' yr']);
    else if($oTimeDifference->m)
        return trans('messages.post_time',['time' => $oTimeDifference->m.' mo']);
    else if($oTimeDifference->d)
        return trans('messages.post_time',['time' => $oTimeDifference->d.' d']);
    else if($oTimeDifference->h)
    {
        if($oTimeDifference->h == 1)
            return trans('messages.post_time',['time' => $oTimeDifference->h.' hr']);
        else if($oTimeDifference->h > 1)
            return trans('messages.post_time',['time' => $oTimeDifference->h.' hrs']);
    }
    else if($oTimeDifference->i){
        if($oTimeDifference->i == 1)
            return trans('messages.post_time',['time' => $oTimeDifference->i.' min']);
        else if($oTimeDifference->i > 1)
            return trans('messages.post_time',['time' => $oTimeDifference->i.' mins']);
    }
    else if($oTimeDifference->s)
        return trans('messages.post_time',['time' => $oTimeDifference->s.' sec']);
    else
        return trans('messages.just_now');
}

function TimeDiffinSec($sEndDate,$sStartDate =''){

    $dStartDate = !empty($sStartDate) ? new DateTime( $sStartDate ) : new DateTime();
    $dEndDate = new DateTime( $sEndDate );

    $diffInSeconds = $dEndDate->getTimestamp() - $dStartDate->getTimestamp();
    return $diffInSeconds;
}

function ck_ucfirst($str) {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc.mb_substr($str, 1);
}

function applySubStr($sString, $nLength, $sSuffix = "...")
{
    return (mb_strlen($sString) > $nLength) ? mb_substr($sString, 0, $nLength - mb_strlen($sSuffix)) . $sSuffix : $sString;
}

/**
 * Replace links in text with html links
 *
 * @param  string $text
 * @return string
 */
function auto_link_text($text)
{
    //$sPattern = '!(((f|ht)tp(s)?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{1,4})[-a-zA-Zа-яА-Я()0-9\!\@:%_+.~#?&;//=]+)!i';
    $sPattern = '#\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))#';
    
    $mCallback = create_function('$aMatches', '
        $sUrl       = array_shift($aMatches);
        $sText = $sUrl;
        if (!preg_match("~^(?:f|ht)tps?://~i", $sUrl)) {
            $sUrl = "//" . $sUrl;
        }

        return sprintf("<a href=\"%s\" target=\"_blank\" rel=\"nofollow\">%s</a>", $sUrl, $sText);
        ');
    return preg_replace_callback($sPattern, $mCallback, $text);
}

function setTextHtml($sText)
{
    return nl2br(auto_link_text(htmlspecialchars(trim($sText))));
}

function setProfileImage($nImgsize,$sImg,$nIdUser,$sFirstName,$sLastName)
{
    $html = '<span class=" deault-img profile-background-color-'.(strlen($sFirstName.$sLastName)+$nIdUser)%config('constants.COLORCOUNT').'" >'
    .'<p>'.mb_substr($sFirstName, 0, 1).' '.mb_substr($sLastName, 0, 1).'</p>'
    .'</span>';
    
    if(strlen($sImg))
    {
        if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$nImgsize.'_'.$sImg))
            $html='<img src="'.config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$nImgsize.'_'.$sImg.'">';
        else if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$sImg))
            $html='<img src="'.config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$sImg.'">';
    }
    return $html;
}

function getUserImageUrl($sImgName,$nImgSize='')
{
    $sImageUrl = NULL;
    
    if(!empty($sImgName))
    {
        if(!empty($nImgSize) && isUrlExist(config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$nImgSize.'_'.$sImgName)) {
            $sImageUrl = config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$nImgSize.'_'.$sImgName;
        } elseif (isUrlExist(config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$sImgName)) {
            $sImageUrl = config('constants.MEDIAURL').'/'.config('constants.USERMEDIAFOLDER').'/'.$sImgName;
        }
    }
    return $sImageUrl;
}

function setGroupImage($sImg,$sDefaultImg)
{
    $html = '<img src="'.asset('/assets/web/img/group-categories').'/'.$sDefaultImg.'" class="icon-img">';
    
    if(strlen($sImg))
    {
        if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.GROUPMEDIAFOLDER').'/'.$sImg))
            $html='<img src="'.config('constants.MEDIAURL').'/'.config('constants.GROUPMEDIAFOLDER').'/'.$sImg.'">';
    }
    return $html;
}

function getGroupImageUrl($sImgName,$nImgSize='')
{
    $sImageUrl = NULL;
    
    if(!empty($sImgName))
    {
        if(!empty($nImgSize) && isUrlExist(config('constants.MEDIAURL').'/'.config('constants.GROUPMEDIAFOLDER').'/'.$nImgSize.'_'.$sImgName)) {
            $sImageUrl = config('constants.MEDIAURL').'/'.config('constants.GROUPMEDIAFOLDER').'/'.$nImgSize.'_'.$sImgName;
        } elseif (isUrlExist(config('constants.MEDIAURL').'/'.config('constants.GROUPMEDIAFOLDER').'/'.$sImgName)) {
            $sImageUrl = config('constants.MEDIAURL').'/'.config('constants.GROUPMEDIAFOLDER').'/'.$sImgName;
        }
    }
    return $sImageUrl;
}

function setPostImage($sImg,$nSize)
{
    $sFileName = asset('/assets/web/img/no-photos.png');
    
    if(!empty($sImg))
    {
        if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$nSize.'_'.$sImg))
            $sFileName = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$nSize.'_'.$sImg;
        else if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$sImg))
            $sFileName = config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$sImg;
    }
    return $sFileName;
}

function setQuestionImage($sImg,$nSize)
{
    $sFileName = asset('/assets/web/img/no-photos.png');
    
    if(!empty($sImg))
    {
        if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.QUESTIONMEDIAFOLDER').'/'.$nSize.'_'.$sImg))
            $sFileName = config('constants.MEDIAURL').'/'.config('constants.QUESTIONMEDIAFOLDER').'/'.$nSize.'_'.$sImg;
        else if(isUrlExist(config('constants.MEDIAURL').'/'.config('constants.QUESTIONMEDIAFOLDER').'/'.$sImg))
            $sFileName = config('constants.MEDIAURL').'/'.config('constants.QUESTIONMEDIAFOLDER').'/'.$sImg;
    }
    return $sFileName;
}

function isUrlExist($sUrl)
{
    $sRedirectLocation = "";
    $oCURL = curl_init($sUrl);    
    curl_setopt($oCURL, CURLOPT_NOBODY, true);
    
    curl_setopt($oCURL, CURLOPT_HEADERFUNCTION,
        function($curl, $header) use (&$sRedirectLocation)
        {
          $len = strlen($header);
          $aHeader = explode(':', $header, 2);
          if (count($aHeader) < 2) // ignore invalid headers
          return $len;

          $name = mb_strtolower(trim($aHeader[0]));
          if ($name == "location")
            $sRedirectLocation = trim($aHeader[1]);

        return $len;
    }
    );
    curl_exec($oCURL);
    $nCode = curl_getinfo($oCURL, CURLINFO_HTTP_CODE);

    if($nCode == 200){
     $bStatus = true;
 }
 else if($nCode == 301 && strlen($sRedirectLocation)) {
    $bStatus = isUrlExist($sRedirectLocation);
}
else{
  $bStatus = false;
}
curl_close($oCURL);
return $bStatus;
}

function getBrowser()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version= "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    }
    elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    }
    elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
    {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }
    elseif(preg_match('/Trident/i',$u_agent))
    { // this condition is for IE11
        $bname = 'Internet Explorer';
        $ub = "rv";
    }
    elseif(preg_match('/Firefox/i',$u_agent))
    {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    }
    elseif(preg_match('/OPR/i',$u_agent))
    {
        $bname = 'Opera';
        $ub = "OPR";
    }
    elseif(preg_match('/Chrome/i',$u_agent))
    {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    }
    elseif(preg_match('/Safari/i',$u_agent))
    {
        $bname = 'Apple Safari';
        $ub = "Safari";
    }
    elseif(preg_match('/Netscape/i',$u_agent))
    {
        $bname = 'Netscape';
        $ub = "Netscape";
    }

    // finally get the correct version number
    // Added "|:"
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
    ')[/|: ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }

    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (mb_strripos($u_agent,"Version") < mb_strripos($u_agent,$ub)){
            $version= $matches['version'][0];
        }
        else {
            $version= $matches['version'][1];
        }
    }
    else {
        $version= $matches['version'][0];
    }

    // check if we have a number
    if ($version==null || $version=="") {$version="?";}

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
        );
}


function getTimeZoneList()
{
    return  [
    'Etc/GMT+12'                      => '(GMT-12:00) International Date Line West',

    'Etc/GMT+11'                      => '(GMT-11:00) Midway Island, Samoa',

    'America/Adak'                    => '(GMT-10:00) Aleutian Islands',

    'Pacific/Honolulu'                => '(GMT-10:00) Hawaii',

    'Pacific/Marquesas'               => '(GMT-10:00) Marquesas Islands',

    'America/Anchorage'               => '(GMT-09:00) Alaska',

    'Etc/GMT+9'                       => '(GMT-09:00) Coordinated Universal Time-09',

    'America/Tijuana'                 => '(GMT-08:00) Baja California, Tijuana',

    'America/Los_Angeles'             => '(GMT-08:00) Pacific Time (US and Canada)',

    'Etc/GMT+7'                       => '(GMT-07:00) Arizona',

    'Etc/GMT+8'                       => '(GMT-09:00) Coordinated Universal Time-08',

    'America/Chihuahua'               => '(GMT-07:00) Chihuahua, La Paz, Mazatlan',

    'America/Denver'                  => '(GMT-07:00) Mountain Time (US and Canada)',

    'America/Managua'                 => '(GMT-06:00) Central America',

    'America/Chicago'                 => '(GMT-06:00) Central Time (US and Canada)',

    'America/Mexico_City'             => '(GMT-06:00) Guadalajara, Mexico City, Monterrey',

    'America/Regina'                  => '(GMT-06:00) Saskatchewan',

    'Pacific/Easter'                  => '(GMT-06:00) Easter Island', /*--*/

    'America/Bogota'                  => '(GMT-05:00) Bogota, Lima, Quito, Rio Branco',

    'America/Monterrey'               => '(GMT-05:00) Chetumal',

    'America/New_York'                => '(GMT-05:00) Eastern Time (US and Canada)',

    'America/Indiana/Indianapolis'    => '(GMT-05:00) Indiana (East)',

    'America/Havana'                  => '(GMT-05:00) Havana', /*--*/

    'America/Caracas'                 => '(GMT-04:30) Caracas',

    'America/Asuncion'                => '(GMT-04:00) Asuncion',

    'America/Halifax'                 => '(GMT-04:00) Atlantic Time (Canada)',

    'America/Santiago'                => '(GMT-04:00) Santiago',

    'America/Cuiaba'                  => '(GMT-04:00) Cuiaba',

    'America/Antigua'                 => '(GMT-04:00) Turks and Caicos', /*--*/

    'America/Manaus'                  => '(GMT-03:00) Georgetown, La Paz, Manaus, San Juan',

    'America/St_Johns'                => '(GMT-03:30) Newfoundland and Labrador',

    'America/Sao_Paulo'               => '(GMT-03:00) Brasilia',

    'America/Cayenne'                 => '(GMT-03:00) Cayenne, Fortaleza',

    'America/Argentina/Buenos_Aires'  => '(GMT-03:00) City of Buenos Aires',

    'America/Godthab'                 => '(GMT-03:00) Greenland',

    'America/Montevideo'              => '(GMT-03:00) Montevideo',

    'Etc/GMT+3'                       => '(GMT-03:00) Salvador',

    'America/Araguaina'               => '(GMT-03:00) Araguaina',

    'Etc/GMT+2'                       => '(GMT-02:00) Coordinated Universal Time-02',

    'Atlantic/Azores'                 => '(GMT-01:00) Azores',

    'Atlantic/Cape_Verde'             => '(GMT-01:00) Cabo Verde Is.',

    'Africa/Casablanca'               => '(GMT) Casablanca',

    'Etc/GMT'                         => '(GMT) Coordinated Universal Time',    

    'Europe/Dublin'                   => '(GMT) Dublin, Edinburgh, Lisbon, London',

    'Africa/Monrovia'                 => '(GMT) Monrovia, Reykjavik',

    'Europe/Amsterdam'                => '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna',

    'Europe/Belgrade'                 => '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague',

    'Europe/Brussels'                 => '(GMT+01:00) Brussels, Copenhagen, Madrid, Paris',

    'Europe/Sarajevo'                 => '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb',

    'Africa/Lagos'                    => '(GMT+01:00) West Central Africa',

    'Africa/Windhoek'                 => '(GMT+01:00) Windhoek',

    'Asia/Amman'                      => '(GMT+02:00) Amman',

    'Europe/Athens'                   => '(GMT+02:00) Athens, Bucharest',

    'Asia/Beirut'                     => '(GMT+02:00) Beirut',

    'Africa/Cairo'                    => '(GMT+02:00) Cairo',

    'Asia/Damascus'                   => '(GMT+02:00) Damascus',

    'Etc/GMT-2'                       => '(GMT+02:00) E.Europe',

    'Africa/Harare'                   => '(GMT+02:00) Harare, Pretoria',

    'Europe/Helsinki'                 => '(GMT+02:00) Helsinki, Kiev, Riga, Sofia, Tallinn, Vilnius',

    'Europe/Istanbul'                 => '(GMT+02:00) Istanbul',

    'Asia/Jerusalem'                  => '(GMT+02:00) Jerusalem',

    'Europe/Kaliningrad'              => '(GMT+02:00) Kaliningrad (RTZ-1)',

    'Africa/Tripoli'                  => '(GMT+02:00) Tripoli',

    'Europe/Chisinau'                 => '(GMT+02:00)Chisinau',

    'Asia/Hebron'                     => '(GMT+02:00)Gaza, Hebron',

    'Asia/Baghdad'                    => '(GMT+03:00) Baghdad',

    'Asia/Kuwait'                     => '(GMT+03:00) Kuwait, Riyadh',

    'Europe/Minsk'                    => '(GMT+03:00) Minsk',

    'Europe/Moscow'                   => '(GMT+03:00) Moscow, St. Petersburg, Volgograd (RTZ-2)',

    'Africa/Nairobi'                  => '(GMT+03:00) Nairobi',

    'Asia/Tehran'                     => '(GMT+03:30) Tehran',

    'Asia/Muscat'                     => '(GMT+04:00) Abu Dhabi, Muscat',

    'Asia/Baku'                       => '(GMT+04:00) Baku', 

    'Europe/Samara'                   => '(GMT+04:00) Izhevsk, Samara (RTZ-3)', 

    'Etc/GMT-4'                       => '(GMT+04:00) Port Louis', 

    'Asia/Tbilisi'                    => '(GMT+04:00) Tbilisi', 

    'Asia/Yerevan'                    => '(GMT+04:00) Yerevan', 

    'Asia/Kabul'                      => '(GMT+04:30) Kabul',

    'Europe/Astrakhan'                => '(GMT+04:00) Astrakhan, Ulyanovsk',

    'Asia/Ashgabat'                   => '(GMT+05:00) Ashgabat, Tashkent',

    'Asia/Yekaterinburg'              => '(GMT+05:00) Ekaterinburg (RTZ-4)',

    'Asia/Karachi'                    => '(GMT+05:00) Islamabad, Karachi',

    'Asia/Kolkata'                    => '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi',

    'Asia/Colombo'                    => '(GMT+05:30) Sri Jayawardenepura',

    'Asia/Kathmandu'                  => '(GMT+05:45) Kathmandu',

    'Etc/GMT-6'                       => '(GMT+06:00) Astana',

    'Asia/Dhaka'                      => '(GMT+06:00) Dhaka',

    'Asia/Novosibirsk'                => '(GMT+06:00) Novosibirsk (RTZ-5)',

    'Asia/Rangoon'                    => '(GMT+06:30) Yangon (Rangoon)',

    'Asia/Bangkok'                    => '(GMT+07:00) Bangkok, Hanoi, Jakarta',

    'Asia/Krasnoyarsk'                => '(GMT+07:00) Krasnoyarsk (RTZ-6)',

    'Asia/Barnaul'                    => '(GMT+07:00) Barnaul, Gorno-Altaysk',

    'Asia/Hovd'                       => '(GMT+07:00) Hovd',

    'Etc/GMT-7'                       => '(GMT+07:00) Tomsk',

    'Asia/Urumqi'                     => '(GMT+08:00) Beijing, Chongqing, Hong Kong SAR, Urumqi',

    'Asia/Irkutsk'                    => '(GMT+08:00) Irkutsk (RTZ-7)',

    'Asia/Kuala_Lumpur'               => '(GMT+08:00) Kuala Lumpur, Singapore',

    'Australia/Perth'                 => '(GMT+08:00) Perth',

    'Asia/Taipei'                     => '(GMT+08:00) Taipei',

    'Asia/Ulaanbaatar'                => '(GMT+08:00) Ulaanbaatar',

    'Asia/Pyongyang'                  => '(GMT+08:30) Pyongyang',

    'Australia/Eucla'                 => '(GMT+08:45) Eucla',

    'Asia/Tokyo'                      => '(GMT+09:00) Osaka, Sapporo, Tokyo',

    'Asia/Seoul'                      => '(GMT+09:00) Seoul',

    'Asia/Yakutsk'                    => '(GMT+09:00) Yakutsk',

    'Australia/Adelaide'              => '(GMT+09:30) Adelaide',

    'Australia/Darwin'                => '(GMT+09:30) Darwin',

    'Asia/Chita'                      => '(GMT+09:00) Chita',

    'Australia/Brisbane'              => '(GMT+10:00) Brisbane',

    'Australia/Sydney'                => '(GMT+10:00) Canberra, Melbourne, Sydney',

    'Pacific/Guam'                    => '(GMT+10:00) Guam, Port Moresby',

    'Australia/Hobart'                => '(GMT+10:00) Hobart',

    'Asia/Magadan'                    => '(GMT+10:00) Magadan',

    'Asia/Vladivostok'                => '(GMT+10:00) Vladivostok',

    'Australia/Lord_Howe'             => '(GMT+10:30) Lord Howe Island',

    'Etc/GMT-11'                      => '(GMT+11:00) Chokurdakh (RTZ-10), Solomon Islands, New Caledonia',

    'Pacific/Bougainville'            => '(GMT+11:00) Bougainville Island',

    'Pacific/Norfolk'                 => '(GMT+11:00) Norfolk Island',

    'Asia/Sakhalin'                   => '(GMT+11:00) Sakhalin',

    'Asia/Anadyr'                     => '(GMT+12:00) Anadyr, Petropavlovsk-Kamchatsky (RTZ-11)',

    'Pacific/Auckland'                => '(GMT+12:00) Auckland, Wellington',

    'Etc/GMT-12'                      => '(GMT+12:00) Coordinated Universal Time+12',

    'Pacific/Fiji'                    => '(GMT+12:00) Fiji',

    'Pacific/Chatham'                 => '(GMT+12:45) Chatham Islands',

    'Pacific/Tongatapu'               => '(GMT+13:00) Nuku\'alofa',

    'Pacific/Apia'                    => '(GMT+13:00) Samoa',

    'Etc/GMT-13'                      => '(GMT+13:00) Coordinated Universal Time+13',

    'Pacific/Kiritimati'              => '(GMT+13:00) Kiritimati Islands',
    ];
}

function extract_dates($string)
{
    return preg_replace('/.*(\d{4}(-\d{2}){2}).*/', '$1', $string);
}
