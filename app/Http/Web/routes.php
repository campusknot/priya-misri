<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
//priya-vue
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return back();
});
Route::get('/clear-view-cache', function() {
    Artisan::call('view:clear');
    return back();
});


Route::group(['middleware' => ['web']], function () {
    Route::get('/home/download-app', ['as' => 'home.show-app-download-options', 'uses' => 'HomeController@callShowAppDownloadOptions']);
    Route::match(['get'],'/about', ['as' => 'home.about', 'uses' => 'HomeController@callAboutPage']);
    Route::match(['get'],'/mobile-app', ['as' => 'home.mobile-app', 'uses' => 'HomeController@callMobileApp']);
    Route::match(['get'],'/privacy', ['as' => 'home.privacy', 'uses' => 'HomeController@callPrivacyPage']);
    Route::match(['get'],'/terms', ['as' => 'home.terms', 'uses' => 'HomeController@callTermsncondiPage']);
    
    Route::match(['get'],'/verification/{sVerificationKey}', ['as' => 'user.verification', 'uses' => 'UserController@callUserVerification']);
    Route::match(['get', 'post'],'/reset-password', ['as' => 'user.reset-password', 'uses' => 'UserController@callResetPassword']);
    
    Route::match(['get'],'/set-image', ['as' => 'utility.set-image', 'uses' => 'UtilityController@callSetImage']);
});

Route::group(['middleware' => ['web', 'mobile_browser_check']], function () {
   
    Route::match(['get'],'/home', ['as' => 'home.index', 'uses' =>'HomeController@index']);
    
    Route::match(['get', 'post'],'/contact', ['as' => 'home.contact', 'uses' => 'HomeController@callContactPage']);
    
    //This three routes will go in admin side.
//    Route::match(['get', 'post'],'/newsletter', 'HomeController@callSendNewsLetter');
//    Route::match(['get', 'post'], '/campus-suggestion', ['as' => 'campus-suggestion', 'uses' => 'HomeController@callCampusSuggetions']);
    Route::match(['get', 'post'],'/home/send-verification-mail', ['as' => 'home.send-verification-mail', 'uses' => 'HomeController@callSendVerificationMail']);
//    Route::match(['get', 'post'],'/home/add-course', ['as' => 'home.add-course', 'uses' => 'HomeController@callAddCourse']);
//    Route::match(['get', 'post'],'/home/edit-course/{nIdCourse}', ['as' => 'home.edit-course', 'uses' => 'HomeController@callEditCourse']);
//    Route::match(['get'],'/home/delete-course/{nIdCourse}', ['as' => 'home.delete-course', 'uses' => 'HomeController@callDeleteCourse']);
    Route::match(['get', 'post'], '/add-user', ['as' => 'add-user', 'uses' => 'HomeController@callAddUser']);
    //Route::match(['get', 'post'], '/add-dummy-group-member/{nIdGroup}', ['as' => 'add-dummy-group-member', 'uses' => 'HomeController@callAddDummyGroupMember']);
    
    //User routes
    Route::match(['get', 'post'],'/signup', ['as' => 'user.signup', 'uses' => 'UserController@callUserRegistration']);
    Route::match(['get', 'post'],'/login', ['as' => 'user.login', 'uses' => 'UserController@callLogin']);
    Route::match(['get', 'post'],'/forgot-password', ['as' => 'user.forgot-password', 'uses' => 'UserController@callForgotPassword']);
    Route::match(['get'], '/logout', ['as' => 'user.logout', 'uses' => 'UserController@callLogout']);
    Route::match(['get', 'post'], '/account-settings', ['as' => 'user.account-settings', 'uses' => 'UserController@callAccountSettings']);
    
    Route::match(['get'],'/getCampus', ['as' => 'utility.get-campus', 'uses' => 'UtilityController@getCampus']);
    Route::match(['get'],'/', ['as' => '/', 'uses' => 'UserController@callUserFeeds']);
    
    Route::match(['post'], '/user/user-search/{sSearchParam?}', ['as' => 'user.user-search', 'uses' => 'UserController@callUserSearch']);
    
    //User profile  routes
    Route::match(['get'], '/user/profile/{nIdUser}',['as'=>'user.profile','uses'=>'UserController@callShowProfile']);
    Route::match(['get'], '/user/profile', ['as' => 'user.my-profile', 'uses' => 'UserController@callUserProfile']);
    Route::match(['get', 'post'], '/user/add-user-course', ['as' => 'user.add-user-course', 'uses' => 'UserController@callAddUserCourse']);
    Route::match(['get', 'post'], '/user/course-suggestion', ['as' => 'user.course-suggestion', 'uses' => 'UserController@callCourseSuggetions']);
    
    //User profile images routes
    Route::match(['get'], '/user/profile/{nIdUser}',['as'=>'user.profile','uses'=>'UserController@callShowProfile']);
    //Route::match(['get'], '/user/edit-profile-image', ['as' => 'user.edit-profile-image', 'uses' => 'UserController@uploadProfile']);
    Route::post('user/profile-upload-image', ['as' => 'user.crop-image', 'uses' => 'UserController@callChangeProfileImage']);
    Route::get('/user/image/{user}',['as' =>'user.profile-image-list-user','uses'=>'UserController@callShowUserImages']);
    Route::get('/user/delete-user-image/{nIdImage}',['as' =>'user.delete-user-image','uses'=>'UserController@callDeleteUserImage']);
    
    //User profile contacts routes
    Route::get('/user/follow/{nIdUser}',['as'=>'user.follow-user','uses'=>'UserController@callFollowUser']);
    Route::get('/user/follow/{nIdUser}/unfollow',['as'=>'user.un-follow-user','uses'=>'UserController@callUnfollowUser']);
    
    Route::get('/user/contacts/following/{user}',['as'=>'user.user-contacts-following-show','uses'=>'UserController@callShowUserContacts']);
    Route::get('/user/contacts/follower/{user}',['as'=>'user.user-contacts-follower-show','uses'=>'UserController@callShowUserContacts']);
    //user ajax url
    Route::match(['get'], '/user/profile-ajax/{nIdUser}',['as'=>'user.profile-ajax','uses'=>'UserController@callShowProfileAjax']);
    Route::get('/user/image-ajax/{user}',['as' =>'user.profile-image-list-user-ajax','uses'=>'UserController@callShowUserImagesAjax']);
    Route::get('/user/contacts/following-ajax/{nIdUser}',['as'=>'user.user-contacts-following-show-ajax','uses'=>'UserController@callShowUserContactsAjax']);
    Route::get('/user/contacts/follower-ajax/{nIdUser}',['as'=>'user.user-contacts-follower-show-ajax','uses'=>'UserController@callShowUserContactsAjax']);
    

    //User profile education routes
    Route::match(['get', 'post'],'/user/add-education/{nIdEducation?}',['as'=>'user.add-education','uses'=>'UserController@callAddEducation']);
    Route::get('/user/delete-education/{nIdEducation}',['as'=>'user.delete-education','uses'=>'UserController@callDeleteEducation']);
    Route::get('/user/default-education/{nIdEducation}',['as'=>'user.default-education','uses'=>'UserController@callSetDefaultEducation']);
    Route::get('/user/education-update',['as'=>'user.education-update','uses'=>'UserController@updateUserEducation']);

    //User profile work experience routes
    Route::match(['get', 'post'],'/user/add-workexperience/{nIdWorkexperience?}',['as'=>'user.add-workexperience','uses'=>'UserController@callAddWorkExperience']);
    Route::get('/user/delete-workexperience/{nIdWorkexperience}',['as'=>'user.delete-workexperience','uses'=>'UserController@callDeleteWorkExperience']);

    //User profile organization routes
    Route::match(['get', 'post'],'/user/add-organization/{nIdOrganization?}',['as'=>'user.add-organization','uses'=>'UserController@callAddOrganization']);
    Route::get('/user/delete-organization/{nIdOrganization}',['as'=>'user.delete-organization','uses'=>'UserController@callDeleteOrganization']);

    //User profile award routes
    Route::match(['get', 'post'],'/user/add-award/{nIdAward?}',['as'=>'user.add-award','uses'=>'UserController@callAddAward']);
    Route::get('/user/delete-award/{nIdAward}',['as'=>'user.delete-award','uses'=>'UserController@callDeleteAward']);

    // User Profile Aspiration
    Route::match(['get', 'post'],'/user/add-aspiration',['as'=>'user.add-aspiration','uses'=>'UserController@callAddAspiration']);
    
    //User profile journal routes
    Route::match(['get', 'post'], '/user/add-journal/{nIdJournal?}',['as'=>'user.add-journal','uses'=>'UserController@callAddJournal']);
    Route::get('/user/delete-journal/{nIdUserJournal}',['as'=>'user.delete-journal','uses'=>'UserController@callDeleteJournal']);

    //User profile research routes
    Route::match(['get', 'post'],'/user/add-research/{nIdResearch?}',['as'=>'user.add-research','uses'=>'UserController@callAddResearch']);
    Route::get('/user/delete-research/{nIdResearch}', ['as'=>'user.delete-researchwork','uses'=>'UserController@callDeleteResearch']);

    //User profile publication routes
    Route::match(['get', 'post'], '/user/add-publication/{nIdPublication?}', ['as'=>'user.add-publication','uses'=>'UserController@callAddPublication']);
    Route::get('/user/delete-publication/{nIdPublication}',['as'=>'user.delete-publication','uses'=>'UserController@callDeletePublication']);
    
    //User Poll 
    Route::match(['get'], '/user/user-poll', ['as' => 'user.user-poll', 'uses' => 'UserController@callUserPoll']);
    Route::match(['get'], '/user/user-poll-ajax', ['as' => 'user.user-poll-ajax', 'uses' => 'UserController@callUserPollAjax']);
    Route::match(['get'], '/user/all-poll', ['as' => 'user.all-poll', 'uses' => 'UserController@callUserAllPoll']);
    Route::match(['get'], '/user/all-poll-ajax', ['as' => 'user.all-poll-ajax', 'uses' => 'UserController@callUserAllPollAjax']);
    Route::match(['get','post'], '/user/global-search-ajax/{sSearchStr?}', ['as' => 'user.global-search-ajax', 'uses' => 'UserController@getGlobalSearchDataAjax']);
    Route::match(['get','post'], '/user/global-search', ['as' => 'user.global-search', 'uses' => 'UserController@getGlobalSearchData']);
    Route::match(['post'], '/user/user-group-search', ['as' => 'user.user-group-search', 'uses' => 'UserController@callUserGroupSearch']);
    Route::match(['get'], '/user/user-check-login', ['as' => 'user.user-check-login', 'uses' => 'UserController@callCheckLogin']);

    //Notification routes
    Route::match(['get'], '/notification/notification-listing', ['as' => 'notification.notification-listing', 'uses' => 'NotificationController@callShowNotifications']);
    Route::match(['get'], '/notification/update-notification-status/{status}/{nid}', ['as' => 'notification.update-notification-status', 'uses' => 'NotificationController@CallChangeGroupNotificationStatus']);
    Route::match(['get'], '/notification/update-event-status/{status}/{eid}', ['as' => 'notification.update-event-status', 'uses' => 'NotificationController@ChangeEventStatus']);

    //Event routes
    Route::match(['get', 'post'],'/event/add-event',['as'=>'event.add-event','uses'=>'EventController@callAddEvent']);
    Route::get('/event/event-listing', ['as' => 'event.event-listing', 'uses' => 'EventController@callEventListing']);
    Route::get('/event/filter-event-listing', ['as' => 'event.filter-event-listing', 'uses' => 'EventController@callFilterEventListing']);
    Route::get('/event/detail/{oEvent}',['as'=>'event.get-event','uses'=>'EventController@callEventDetail']);
    Route::get('/event/status/{oEventRequest}',['as'=>'event.set-event-status','uses'=>'EventController@callSetEventStatus']);
    Route::get('/event/event-members-by-status/{nIdEvent}/{sStatus}',['as'=>'event.event-user-by-status','uses'=>'EventController@callEventMembersByStatus']);
    Route::match(['get', 'post'], '/event/invite-member', ['as' => 'event.invite-member', 'uses'=>'EventController@callEventInviteMember']);
    Route::match(['post'], '/event/invite-member-search', ['as' => 'event.invite-member-search', 'uses'=>'EventController@callEventInviteMemberSearch']);
    Route::match(['post'], '/event/invite-member-request', ['as' => 'event.invite-member-request', 'uses'=>'EventController@callEventInviteMemberRequest']);
    Route::get('/event/delete-event/{nIdEvent}',['as'=>'event.delete-event','uses'=>'EventController@callDeleteEvent']);
    Route::post('/event/event-search/{sSearchParam?}',['as'=>'event.event-search','uses'=>'EventController@callEventSearch']);//pending
    Route::get('/event/group-event-listing',['as'=>'event.group-event-listing','uses' => 'EventController@callGroupEventList']);
    Route::post('/event/invite/search',['as'=>'event.event-invite-search','uses'=>'EventController@callGetSearchForInvite']);
    Route::match(['get'], '/event/invite-to-event', ['as' => 'event.invite-to-event', 'uses'=>'EventController@callEventInviteMember']);
    Route::post('/event/feeds/post',['as'=>'event.event-feeds','uses'=>'EventController@callPostEventFeed']);
    Route::match(['get'],'/event/google-credential',['as'=>'event.event-google','uses'=>'EventController@callAddGoogleCredential']);
    Route::match(['get'],'/event/event-google-disconnect',['as'=>'event.event-google-disconnect','uses'=>'EventController@callGoogleDisconnect']);
    Route::match(['get'],'/event/event-google-cron/{nPageId}',['as'=>'event.event-google-cron','uses'=>'EventController@callGoogleCronRun']);
    
    //Group routes 
    Route::match(['get', 'post'], '/group/add-group', ['as' => 'group.add-group', 'uses' => 'GroupController@callAddGroup']);
    Route::match(['get', 'post'], '/group/add-course-group', ['as' => 'group.add-course-group', 'uses' => 'GroupController@callAddCourseGroup']);
    Route::match(['get'], '/group/auto-suggetions-for-invite-user/{nIdGroup}', ['as' => 'group.auto-suggetions-for-invite-user', 'uses' => 'GroupController@callAutoSuggetionsForInviteUser']);
    Route::match(['get', 'post'], '/group/edit-group-image', ['as' => 'group.edit-group-image', 'uses' => 'GroupController@callEditGroupImage']);
    
    //Route::match(['get'], '/group/group-search', ['as' => 'group.group-search', 'uses' => 'GroupController@callGroupSearch']);
    Route::match(['get'], '/group/recommended-group-listing', ['as' => 'group.recommended-group-listing', 'uses' => 'GroupController@callGroupSearch']);
    Route::match(['get'], '/group/admin-group-listing', ['as' => 'group.admin-group-listing', 'uses' => 'GroupController@callAdminGroupListing']);
    Route::match(['get'], '/group/member-group-listing', ['as' => 'group.member-group-listing', 'uses' => 'GroupController@callMemberGroupListing']);
    Route::match(['get'], '/group/deactive-group-listing', ['as' => 'group.deactive-group-listing', 'uses' => 'GroupController@callDeactiveGroupListing']);
    Route::match(['get'], '/group/group-feeds/{nIdGroup}', ['as' => 'group.group-feeds', 'uses' => 'GroupController@callGroupFeeds']);
    Route::match(['get'], '/group/group-member/{nIdGroup}/{sSerachStr?}', ['as' => 'group.group-member', 'uses' => 'GroupController@callGroupMembers']);
    Route::match(['get','post'], '/group/group-member-excel/{nIdGroup}', ['as' => 'group.group-member-excel', 'uses' => 'GroupController@callGroupMembersExcel']);
    Route::match(['get'], '/group/group-photos/{nIdGroup}', ['as' => 'group.group-photos', 'uses' => 'GroupController@callGroupPhotos']);
    Route::match(['get','post'], '/group/group-documents/{nIdGroup}/{nIdDocument?}', ['as' => 'group.group-documents', 'uses' => 'GroupController@callGroupDocuments']);
    Route::match(['get', 'post'],'/group/group-post-documents/{nGroupId}',['as'=>'group.group-post-documents','uses'=>'GroupController@callGroupPostDocument']);
    Route::match(['get'], '/group/group-events/{nIdGroup}/{sSerachStr?}', ['as' => 'group.group-events', 'uses' => 'GroupController@callGroupEvents']);
    Route::match(['get'], '/group/group-poll/{nIdGroup}', ['as' => 'group.group-poll', 'uses' => 'GroupController@callGroupPoll']);
    Route::match(['get', 'post'], '/group/member-invite/{nIdGroup}', ['as' => 'group.invite-member', 'uses' => 'GroupController@callInviteUser']);
    Route::match(['post'], '/group/member-invite-via-csv', ['as' => 'group.invite-member-via-csv', 'uses' => 'GroupController@callInviteUserViaCsv']);
    Route::match(['get'], '/group/invite-again/{sGroupRequestData}', ['as' => 'group.invite-again', 'uses' => 'GroupController@callInviteUserAgain']);
    Route::match(['get', 'post'], '/group/group-settings/{nGroupRequest}', ['as' => 'group.group-settings', 'uses' => 'GroupController@callGroupSettings']);
    Route::match(['get'], '/group/group-request-status/{nIdGroup}', ['as' => 'group.group-request-status', 'uses' => 'GroupController@callGroupRequestStatus']);
    
    Route::match(['get'], '/group/join-group/{nIdGroup}', ['as' => 'group.join-group', 'uses' => 'GroupController@callJoinGroup']);
    Route::match(['get'], '/group/leave-group/{nIdGroup}', ['as' => 'group.leave-group', 'uses' => 'GroupController@callLeaveGroup']);
    Route::match(['post'], '/group/pass-creator-rights', ['as' => 'group.pass-creator-rights', 'uses' => 'GroupController@callPassCreatorRights']);
    Route::match(['get'], '/group/remove-member/{nIdGroup}/{nIdUser}', ['as' => 'group.remove-member', 'uses' => 'GroupController@callRemoveMember']);
    Route::match(['get'], '/group/member-suggestion/{nIdGroup}', ['as' => 'group.member-suggestion', 'uses' => 'GroupController@callGroupMemberSuggestion']);
    Route::match(['get'], '/group/change-group-member-type/{nIdGroup}', ['as' => 'group.change-group-member-type', 'uses' => 'GroupController@callGroupMemberAsAdmin']);
    
    //Group ajax routes
    Route::match(['get'], '/group/recommended-group-listing-ajax/{sSerachStr?}', ['as' => 'group.recommended-group-listing-ajax', 'uses' => 'GroupController@callGroupSearchAjax']);
    Route::match(['get'], '/group/admin-group-listing-ajax/{sSerachStr?}', ['as' => 'group.admin-group-listing-ajax', 'uses' => 'GroupController@callAdminGroupListingAjax']);
    Route::match(['get'], '/group/member-group-listing-ajax/{sSerachStr?}', ['as' => 'group.member-group-listing-ajax', 'uses' => 'GroupController@callMemberGroupListingAjax']);
    Route::match(['get'], '/group/deactive-group-listing-ajax', ['as' => 'group.deactive-group-listing-ajax', 'uses' => 'GroupController@callDeactiveGroupListingAjax']);
    Route::match(['get'], '/group/group-feeds-ajax/{nIdGroup}', ['as' => 'group.group-feeds-ajax', 'uses' => 'GroupController@callGroupFeedsAjax']);
    Route::match(['get'], '/group/group-member-ajax/{nIdGroup}/{sSerachStr?}', ['as' => 'group.group-member-ajax', 'uses' => 'GroupController@callGroupMembersAjax']);
    Route::match(['get'], '/group/group-member-search/{nIdGroup}/{sSerachStr?}', ['as' => 'group.group-member-search', 'uses' => 'GroupController@callGroupMembersSearch']);
    Route::match(['get'], '/group/group-photos-ajax/{nIdGroup}', ['as' => 'group.group-photos-ajax', 'uses' => 'GroupController@callGroupPhotosAjax']);
    Route::match(['get','post'], '/group/group-child-documents-ajax/{nIdGroup}/{nIdDocument?}', ['as' => 'group.group-child-documents-ajax', 'uses' => 'GroupController@callGroupDocumentsAjax']);
    Route::match(['get','post'], '/group/group-documents-ajax/{nIdGroup}/{nIdDocument?}', ['as' => 'group.group-documents-ajax', 'uses' => 'GroupController@callGroupDocumentsSidebarAjax']);
    Route::match(['get','post'],'/group/group-post-child-document-ajax/{nGroupId}',['as'=>'group.group-post-child-document-ajax','uses'=>'GroupController@callGroupPostChildDocumentAjax']);
    Route::match(['get','post'],'/group/group-post-documents-ajax/{nGroupId}',['as'=>'group.group-post-documents-ajax','uses'=>'GroupController@callGroupPostDocumentAjax']);
    Route::match(['get'], '/group/group-events-ajax/{nIdGroup}/{sSerachStr?}', ['as' => 'group.group-events-ajax', 'uses' => 'GroupController@callGroupEventsAjax']);
    Route::match(['get'], '/group/group-poll-ajax/{nIdGroup}', ['as' => 'group.group-poll-ajax', 'uses' => 'GroupController@callGroupPollAjax']);
    Route::match(['get'], '/group/member-invite-ajax/{nIdGroup}', ['as' => 'group.invite-member-ajax', 'uses' => 'GroupController@callInviteUserAjax']);
    Route::match(['get', 'post'], '/group/group-settings-ajax/{nGroupRequest}', ['as' => 'group.group-settings-ajax', 'uses' => 'GroupController@callGroupSettingsAjax']);
    
    //group attendace
    Route::match(['get', 'post'], '/group/group-suggestion', ['as' => 'group.group-suggestion', 'uses' => 'GroupController@callAdminGroupSuggetion']);
    Route::match(['post'], '/group/add-attendance', ['as' => 'group.add-attendance', 'uses' => 'GroupController@callAddAttendance']);
    Route::match(['post'], '/group/submit-attendance', ['as' => 'group.submit-attendance', 'uses' => 'GroupController@callSubmitAttendance']);
    Route::match(['get','post'], '/group/attendance', ['as' => 'group.attendance', 'uses' => 'GroupController@callGetAttendance']);//@misri
    Route::match(['get','post'], '/group/attendance-ajax', ['as' => 'group.attendance-ajax', 'uses' => 'GroupController@callGetAttendanceAjax']);//@misri
    Route::match(['get','post'], '/group/course-attendance', ['as' => 'group.course-attendance', 'uses' => 'GroupController@callCourseAttendance']);
    Route::match(['get','post'], '/group/course-attendance-ajax', ['as' => 'group.course-attendance-ajax', 'uses' => 'GroupController@callCourseAttendanceAjax']);
    Route::match(['get','post'], '/group/admin-attendance-table/{nIdLecture?}', ['as' => 'group.admin-attendance-table', 'uses' => 'GroupController@callGetAttendanceTable']);
    Route::match(['get','post'], '/group/add-group-attendance', ['as' => 'group.add-group-attendance', 'uses' => 'GroupController@callAddMemberInList']);
    Route::match(['get','post'], '/group/student-attendance-table/{nIdLecture?}', ['as' => 'group.student-attendance-table', 'uses' => 'GroupController@getStudentSignleAttendace']);
    Route::match(['get','post'], '/group/attendance-excel', ['as' => 'group.attendance-excel', 'uses' => 'GroupController@callGetAttendanceExcel']);
    Route::match(['get','post'], '/group/attendance-log', ['as' => 'group.attendance-log', 'uses' => 'GroupController@callAttendaceLog']);
    Route::match(['get','post'], '/group/member-attendance-log', ['as' => 'group.member-attendance-log', 'uses' => 'GroupController@getMemberAttendaceLog']);
    Route::match(['get'], '/group/delete-lecture/{nIdGroupLecture}', ['as' => 'group.delete-lecture', 'uses' => 'GroupController@callDeleteGroupLecture']);
    Route::match(['get','post'], '/group/change-attendance', ['as' => 'group.change-attendance', 'uses' => 'GroupController@callChangeStudentAttendance']);
    //group attendance tab
    Route::match(['get','post'], '/group/group-attendance-ajax/{nIdGroup}', ['as' => 'group.group-attendance-ajax', 'uses' => 'GroupController@callGetGroupAttendanceAjax']);
    Route::match(['get','post'], '/group/group-attendance/{nIdGroup}', ['as' => 'group.group-attendance', 'uses' => 'GroupController@callGetGroupAttendance']);
    
   
    //Post routes
    Route::match(['post'], '/post/add-post', ['as' => 'post.add-post', 'uses' => 'PostController@callAddPost']);
    Route::match(['get','post'], '/post/add-poll/{nIdPost?}', ['as' => 'post.add-poll', 'uses' => 'PostController@callAddPoll']);
    Route::match(['get'], '/post/activate-poll/{nIdPoll}', ['as' => 'post.activate-poll', 'uses' => 'PostController@callActivatePoll']);
    Route::match(['post'], '/post/add-comment', ['as' => 'post.add-comment', 'uses' => 'PostController@callAddComment']);
    Route::match(['post'], '/post/add-poll-answer', ['as' => 'post.add-poll-answer', 'uses' => 'PostController@callAddPollAnswer']);
    Route::match(['get'], '/post/like-post/{nIdPost}', ['as' => 'post.like-post', 'uses' => 'PostController@callLikePost']);
    Route::match(['get'], '/post/delete-post/{nIdPost}', ['as' => 'post.delete-post', 'uses' => 'PostController@callDeletePost']);
    Route::match(['get'], '/post/delete-post-comment/{nIdPostComment}', ['as' => 'post.delete-post-comment', 'uses' => 'PostController@callDeletePostComment']);
    Route::match(['get'], '/post/photo-detail/{nIdPost}/{nIdGroup?}', ['as' => 'post.photo-detail', 'uses' => 'PostController@callPhotoDetail']);
    Route::match(['get'], '/post/poll-detail/{nIdPoll}', ['as' => 'post.poll-detail', 'uses' => 'PostController@callPollDetail']);
    Route::match(['post'], '/post/poll-answer-excel', ['as' => 'post.poll-answer-excel', 'uses' => 'PostController@callPollAnswerExcel']);
    Route::match(['get'], '/post/post-detail/{nIdPost}', ['as' => 'post.post-detail', 'uses' => 'PostController@callPostDetail']);
    Route::match(['get'], '/post/post-Like-user/{nIdPost}', ['as' => 'post.post-Like-user', 'uses' => 'PostController@callPostLikeUser']);
    Route::match(['post'], '/post/post-add-report', ['as' => 'post.post-add-report', 'uses' => 'PostController@callAddContentReport']);
    
    
    Route::match(['get'], '/utility/view-file/{sFileName}/{sDisplayFileName}', ['as' => 'utility.view-file', 'uses' => 'UtilityController@callViewFile']);
    Route::match(['get'], '/utility/download-file/{sFileName}/{sDisplayFileName}', ['as' => 'utility.download-file', 'uses' => 'UtilityController@callDownloadFile']);
    Route::match(['get'], '/utility/media-parser', ['as' => 'utility.media-parser', 'uses' => 'UtilityController@callMediaParser']);
    
    
    //Book Route
    Route::match(['get'], '/book/book-listing', ['as' => 'book.book-listing', 'uses' => 'BookController@callBookListing']);
    Route::match(['get'], '/book/book-sell', ['as' => 'book.book-sell', 'uses' => 'BookController@callBookSell']);
    Route::match(['get'], '/book/book-wishlist', ['as' => 'book.book-wishlist', 'uses' => 'BookController@callBookUserWishList']);
    
    //documents Route
    Route::match(['get', 'post'], '/documents/document-listing/{nDocumentId?}', ['as' => 'documents.document-listing', 'uses' => 'DocumentController@callDocumentListing']);
    Route::match(['get', 'post'],'/documents/shared-document/{nDocumentId?}',['as'=>'documents.shared-document','uses'=>'DocumentController@callSharedDocument']);
    Route::match(['get', 'post'],'/documents/group-document/{nGroupId}/{nDocumentId?}',['as'=>'documents.group-document','uses'=>'DocumentController@callGroupDocument']);
    Route::match(['get', 'post'],'/documents/post-document',['as'=>'documents.post-document','uses'=>'DocumentController@callPostDocument']);
    Route::match(['get', 'post'],'/documents/group-post-document/{nGroupId}',['as'=>'documents.group-post-document','uses'=>'DocumentController@callGroupPostDocument']);
    Route::match(['get', 'post'],'/documents/document-listing-ajax/{nDocumentId?}',['as'=>'documents.document-listing-ajax','uses'=>'DocumentController@callDocumentListingAjax']);
    Route::match(['get', 'post'],'/documents/shared-document-ajax/{nDocumentId?}',['as'=>'documents.shared-document-ajax','uses'=>'DocumentController@callSharedDocumentAjax']);
    Route::match(['get', 'post'],'/documents/group-document-ajax/{nGroupId}/{nDocumentId?}',['as'=>'documents.group-document-ajax','uses'=>'DocumentController@callGroupDocumentAjax']);
    Route::match(['get', 'post'],'/documents/post-document-ajax',['as'=>'documents.post-document-ajax','uses'=>'DocumentController@callPostDocumentAjax']);
    Route::match(['get', 'post'],'/documents/group-post-document-ajax/{nGroupId}',['as'=>'documents.group-post-document-ajax','uses'=>'DocumentController@callGroupPostDocumentAjax']);
    
    Route::match(['get', 'post'], '/documents/document-share', ['as' => 'documents.document-share', 'uses'=>'DocumentController@callDocumentShare']);
    Route::match(['get', 'post'], '/documents/document-delete/{nDocumentId}', ['as' => 'documents.document-delete', 'uses'=>'DocumentController@callDeleteDocument']);
    Route::match(['get', 'post'], '/documents/document-revoke-access', ['as' => 'documents.document-revoke-access', 'uses'=>'DocumentController@callRevokeDocumentAccess']);
    Route::match(['get', 'post'], '/documents/create-folder', ['as' => 'documents.create-folder', 'uses'=>'DocumentController@callCreateDocument']);
    Route::post('/documents/share/search',['as'=>'documents.document-share-search','uses'=>'DocumentController@callGetSearchForShare']);    
    Route::match(['get', 'post'],'/documents/change-permission',['as'=>'documents.change-permission','uses'=>'DocumentController@callDocumentPermissionChange']);
    Route::match(['get', 'post'],'/documents/document-shared-userlist/{nDocumentId}',['as'=>'documents.document-shared-userlist','uses'=>'DocumentController@callDocumentShareUserList']);
    Route::match(['get','post'],'/documents/document-copy/{nDocumentId?}',['as'=>'documents.document-copy','uses'=>'DocumentController@copyFolder']);
    Route::match(['get','post'],'/documents/shared-group-member/{nGroupDocumentId?}',['as'=>'documents.shared-group-member','uses'=>'DocumentController@getGroupDocumentMember']);
    Route::match(['get','post'],'/documents/group-child-document/{nDocumentId?}',['as'=>'documents.shared-group-member','uses'=>'DocumentController@getGroupChildDocument']);
       
    //quiz
    Route::match(['get','post'],'/quiz/quiz-add/{nIdQuiz?}',['as'=>'quiz.quiz-add','uses'=>'QuizController@callAddQuiz']);
    Route::match(['get','post'],'/quiz/quiz-question-add/{nIdQuestion?}',['as'=>'quiz.quiz-question-add','uses'=>'QuizController@callAddQuizQuestion']);
    Route::match(['get','post'],'/quiz/quiz-question-delete/{nIdQuestion}',['as'=>'quiz.quiz-question-delete','uses'=>'QuizController@callDeleteQuizQuestion']);
    
    //list view route
    Route::match(['get','post'],'/quiz/quiz-faculty-list/{nIdGroup?}',['as'=>'quiz.quiz-faculty-list','uses'=>'QuizController@callAdminQuizList']);
    Route::match(['get','post'],'/quiz/quiz-faculty-list-ajax/{nIdGroup?}',['as'=>'quiz.quiz-faculty-list-ajax','uses'=>'QuizController@callAdminQuizListAjax']);
    Route::match(['get','post'],'/quiz/quiz-faculty-completed-list/{nIdGroup?}',['as'=>'quiz.quiz-faculty-completed-list','uses'=>'QuizController@callAdminCompletedQuizList']);
    Route::match(['get','post'],'/quiz/quiz-faculty-completed-list-ajax/{nIdGroup?}',['as'=>'quiz.quiz-faculty-completed-list-ajax','uses'=>'QuizController@callAdminCompletedQuizListAjax']);
    Route::match(['get'],'/quiz/quiz-student-list/{nIdGroup?}',['as'=>'quiz.quiz-student-list','uses'=>'QuizController@callMemberQuizList']);
    Route::match(['get','post'],'/quiz/quiz-student-list-ajax/{nIdGroup?}',['as'=>'quiz.quiz-student-list-ajax','uses'=>'QuizController@callMemberQuizListAjax']);
    Route::match(['get','post'],'/quiz/quiz-student-completed-list/{nIdGroup?}',['as'=>'quiz.quiz-student-completed-list','uses'=>'QuizController@callMemberCompletedQuizList']);
    Route::match(['get','post'],'/quiz/quiz-student-completed-list-ajax/{nIdGroup?}',['as'=>'quiz.quiz-student-completed-list-ajax','uses'=>'QuizController@callMemberCompletedQuizListAjax']);
    
    Route::match(['get','post'],'/quiz/quiz-student-detail/{nIdQuizUserInvite}',['as'=>'quiz.quiz-student-detail','uses'=>'QuizController@callMemberQuizDetail']);
    Route::match(['get','post'],'/quiz/quiz-question/{nIdQuizUserInvite}',['as'=>'quiz.quiz-question','uses'=>'QuizController@callQuizQuestions']);
    Route::match(['get','post'],'/quiz/quiz-user-list/{nIdQuiz}',['as'=>'quiz.quiz-user-list','uses'=>'QuizController@callQuizUserList']);
    
    Route::match(['get','post'],'/quiz/student-completed-quiz/{nIdQuiz}/{nIdUser}',['as'=>'quiz.student-completed-quiz','uses'=>'QuizController@callMemberCompletedQuiz']);
    Route::match(['get','post'],'/quiz/student-completed-quiz-detail/{nIdQuiz}/{nIdUser}',['as'=>'quiz.student-completed-quiz-detail','uses'=>'QuizController@callMemberCompletedQuizDetail']);
    Route::match(['get','post'],'/quiz/add-student-marks',['as'=>'quiz.add-student-marks','uses'=>'QuizController@callAddStudentMarks']);
    Route::match(['post'],'/quiz/add-student-answer',['as'=>'quiz.add-student-answer','uses'=>'QuizController@callAddStudentAnswer']);
    Route::match(['get','post'],'/quiz/quiz-start/{nIdQuizUserInvite}',['as'=>'quiz.quiz-start','uses'=>'QuizController@callStartQuiz']);
    Route::match(['get','post'],'/quiz/quiz-end/{nIdQuizUserInvite}',['as'=>'quiz.quiz-end','uses'=>'QuizController@callEndQuiz']);
    Route::match(['get','post'],'/quiz/answer-option-delete/{nIdAnswerOption}',['as'=>'quiz.answer-option-delete','uses'=>'QuizController@callDeleteAnswerOption']);
    Route::match(['get','post'],'/quiz/quiz-delete/{nIdQuiz}',['as'=>'quiz.quiz-delete','uses'=>'QuizController@callDeleteQuiz']);
    Route::match(['get','post'],'/quiz/quiz-edit-point',['as'=>'quiz.quiz-edit-point','uses'=>'QuizController@callEditQuizPoint']);
    Route::match(['get','post'],'/quiz/quiz-question-edit-point',['as'=>'quiz.quiz-question-edit-point','uses'=>'QuizController@callEditQuizQuestionPoint']);
    Route::match(['get','post'],'/quiz/quiz-publish/{nIdQuiz}',['as'=>'quiz.quiz-publish','uses'=>'QuizController@callQuizPublish']);
    
    
    //database update route
    
    //Route::match(['get'],'/group/group-database',['as'=>'group.document-copy','uses'=>'GroupController@callDatabaseUpdate']);
});