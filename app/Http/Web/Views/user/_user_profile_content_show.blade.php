<div class="profile-Content">
    <!-- education detail starts here  -->
    <div class="profile-details">
        <div class="heading education">
            <span class="profile-header-img bg-education">
                <img src="{{ asset('assets/web/img/education.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.education') }} </h3>
        </div>
        <div class="content">
        @foreach($user_profile['education_list'] as $education)
            <div class="detail">
                <span class="sub-heading">
                    <p>{!! setTextHtml($education->university) !!}</P>
                </span>
                <ul>	
                    <li>{!! setTextHtml($education->degree) !!}'s Degree @if($education->major != '') in {!! setTextHtml($education->major) !!} @endif</li>
                    @if($education->classification!='')
                    <li>Classification: {!! setTextHtml($education->classification) !!}</li>
                    @endif
                    <li><?php $carbon = new Carbon($education->start_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year }}
                        &nbsp;-&nbsp;
                        @if(isset($education->end_year) && $education->end_year != NULL)
                            <?php $carbon = new Carbon($education->end_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                        @else
                            Present
                        @endif</li>
                </ul>
            </div>
        @endforeach       
        </div>
    </div>
    <!-- education detail ends here  -->
    <!-- work experience detail starts here  -->
    <div class="profile-details">
        <div class="heading work-experience">
            <span class="profile-header-img bg-work">
                <img src="{{ asset('assets/web/img/work.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.work_experience') }} </h3>
        </div>
        <div class="content">
        @foreach($user_profile['work_experience_list'] as $oWe)    
            <div class="detail">
                <span class="sub-heading">
                    <p>{!! setTextHtml($oWe->company_name) !!}</p>
                </span>
                <ul>	
                    <li>{!! setTextHtml($oWe->job_title) !!}</li>
                    <li>{!! setTextHtml($oWe->job_description) !!}</li>
                    <li><?php $carbon = new Carbon($oWe->start_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year }}
                        &nbsp;-&nbsp;
                        @if(isset($oWe->end_year) && $oWe->end_year != NULL)
                            <?php $carbon = new Carbon($oWe->end_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                        @else
                            Present
                        @endif</li>
                </ul>
            </div>
        @endforeach
        </div>
    </div>
    <!-- work experience detail ends here  -->
    <!-- organization information starts here-->
    <div class="profile-details">
        <div class="heading organization">
              <span class="profile-header-img bg-organization">
                <img src="{{ asset('assets/web/img/organization.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.organization') }} </h3>
        </div>
        <div class="content">
        @foreach($user_profile['organization_list'] as $organization)
            <div class="detail">
                <span class="sub-heading"><p>{!! setTextHtml($organization->committee_name) !!}</p>

                </span>
                <ul>
                    <li>{!! setTextHtml($organization->post) !!}</li>
                    <li><?php $carbon = new Carbon($organization->start_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year }}
                        &nbsp;-&nbsp;
                        @if(isset($organization->end_year) && $organization->end_year != NULL)
                            <?php $carbon = new Carbon($organization->end_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                        @else
                            Present
                        @endif</li>
                </ul>
            </div>
        @endforeach
        </div>
    </div>
    <!-- organization information ends here-->
    <!-- Awards and Honors information starts here-->
    <div class="profile-details">
        <div class="heading awards-and-honors">
            <span class="profile-header-img bg-award">
                    <img src="{{ asset('assets/web/img/awards.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.awards_honors') }}</h3>

        </div>
        <div class="content">
        @foreach($user_profile['award_list'] as $award)
            <div class="detail">
                <span class="sub-heading">
                    <p>{!! setTextHtml($award->award_title) !!}</p>
                </span>
                <ul>
                    <li>{!! setTextHtml($award->award_issuer) !!}</li>
                    <li>{!! setTextHtml($award->award_description) !!}</li>
                    <li><?php $carbon = new Carbon($award->awarded_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year}}</li>
                </ul>
            </div>
        @endforeach
        </div>
    </div>
    <!-- Awards and Honors information ends here-->
    <!-- Aspirations information starts here-->
    <div class="profile-details">
        <div class="heading aspirations">
             <span class="profile-header-img bg-aspiration">
                    <img src="{{ asset('assets/web/img/aspirations.png') }}" alt="" class="" />
                </span>
            <h3>{{ trans('messages.aspirations') }}</h3>

        </div>
        @if(trim($user->aspiration)!='')
        <div class="content">
            <div class="detail">
                <p class="no-inrormation">{!! setTextHtml($user->aspiration) !!}</p>
            </div>
        </div>
        @endif
    </div>
    <!-- Aspirations information starts here-->
    <!-- Journal Article information starts here-->
    <div class="profile-details">
        <div class="heading journal-article">
            <span class="profile-header-img bg-jornal">
                <img src="{{ asset('assets/web/img/journal.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.journal_article') }}</h3>

        </div>
        <div class="content">
        @foreach($user_profile['journal_list'] as $ojournal)
            <div class="detail">
                <span class="sub-heading">
                    <p>{!! setTextHtml($ojournal->journal_title ) !!} </p>
                    <span class="publish-date"> <?php $carbon = new Carbon($ojournal->journal_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                    </span>
                </span>
                <ul>
                    <li><p>{{ trans('messages.by') }} {!! setTextHtml($ojournal->journal_author) !!}</p></li>
                    <li><p><a href="{{ $ojournal->journal_url }}" target="_blank">{{ $ojournal->journal_url }}</a></p></li>
                    <li><p>{!! setTextHtml($ojournal->journal_description) !!}</p>
                    </li>
                </ul>
            </div>
        @endforeach
        </div>
    </div>
    <!-- Journal Article information ends here-->
    <!-- Research Work information starts here-->
    <div class="profile-details">
        <div class="heading research-work">
             <span class="profile-header-img bg-research">
                <img src="{{ asset('assets/web/img/research.png') }}" alt="" class="" />
            </span>
            <h3> {{ trans('messages.research_work') }} </h3>

        </div>
        <div class="content">
            @foreach($user_profile['research_list'] as $oResearch)
              <div class="detail">
                    <span class="sub-heading">
                        <p>{!! setTextHtml($oResearch->research_title) !!}</p>
                        <span class="publish-date"><?php $carbon = new Carbon($oResearch->research_year) ?>
                    {{ $carbon->format('F') }} {{ $carbon->year }}</span>

                    </span>
                    <ul>
                            <li><p>BY: {!! setTextHtml($oResearch->research_author) !!}</p></li>
                            <li><p><a href="{{ $oResearch->research_url }}" target="_blank"> {{ $oResearch->research_url }}</a></p></li>
                            <li>{!! setTextHtml($oResearch->research_description) !!}</li>
                    </ul>
              </div>
             @endforeach
        </div>
    </div>
    <!-- Research Work information ends here-->
    <!-- Publications information starts here-->
    <div class="profile-details">
        <div class="heading publication">
               <span class="profile-header-img bg-publication">
                <img src="{{ asset('assets/web/img/publications.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.publication') }} </h3>

        </div>
        <div class="content">
        @foreach($user_profile['publication_list'] as $oPublication)
            <div class="detail">
                <span class="sub-heading">
                    <p>{!! setTextHtml($oPublication->publication_title) !!}</p>
                    <span class="publish-date"><?php $carbon = new Carbon($oPublication->publication_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }} </span>

                </span>
                <ul>
                    <li><p>{{ trans('messages.by') }} {!! setTextHtml($oPublication->publication_author) !!}</p></li>
                    <li><p><a href="{{ $oPublication->publication_url }}" target="_blank">
                                {{ $oPublication->publication_url }}</a></p></li>
                    <li>{!! setTextHtml($oPublication->publication_description) !!}</li>
                </ul>
            </div>
        @endforeach
        </div>
    </div>
</div>