<div class="modal-content edit-information" id="edit_detail">
    <div class="modal-header">
        <div class="modal-header-img bg-aspiration" >
            <img src="{{ asset('assets/web/img/aspirations.png') }}" alt="" class="" />
        </div>

        {{ trans('messages.aspirations') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <div class="modal-body clearfix">
        <form id="add_aspiration_form" class="center" method="POST">
            {!! csrf_field() !!}
            <div class="col-lg-12">
                <div id="textarea_feedback" class="textarea_feedback"></div>
                <textarea class="form-control aspiration" placeholder="{{ trans('messages.aspirations_placeholder') }}"  id="aspirations_desc_update" name="aspiration" maxlength="250">{{$sAspiration}}</textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button class="btn btn-default pull-right" type="button" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input class="btn btn-primary pull-right" type="button" value="{{ empty($sAspiration) ? trans('messages.save') : trans('messages.update') }}" onclick="submitAjaxForm('add_aspiration_form', '{{ route('user.add-aspiration') }}', this )">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var text_max = 250;
        if($('#aspirations_desc_update').val().length>0){
            getRemainingChar();
        }
        else{
            $('#textarea_feedback').html(text_max);
        }
        $('#aspirations_desc_update').keyup(function() {
            getRemainingChar();
        });
    });
</script>