@extends('layouts.web.main_layout')
@include('common.errors')

@section('title', 'Campusknot-User Profile')

@section('left_sidebar')
@include('WebView::user._user_profile_sidebar_show')
@endsection

@section('content')
@include('WebView::user._user_contacts_content_show')
    
        <div class="switch-tab-loader side-tab-loader hidden">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    
@endsection

@section('right_sidebar')
@endsection