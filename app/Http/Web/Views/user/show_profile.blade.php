@extends('layouts.web.main_layout')
@include('common.errors')

@section('title', 'Campusknot-User Profile')


@section('left_sidebar')
    @if(Auth::user()->id_user == $user->id_user)
        @include('WebView::user._user_profile_sidebar')
    @else
        @include('WebView::user._user_profile_sidebar_show')
    @endif
@endsection

@section('content')
    @if(Auth::user()->id_user == $user->id_user)
        @include('WebView::user._user_profile_content_edit')
    @else
        @include('WebView::user._user_profile_content_show')
    @endif

        <div class="switch-tab-loader side-tab-loader hidden">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
@endsection

@section('right_sidebar')

@endsection
