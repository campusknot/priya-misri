<div class="modal-content edit-information"  id="edit_detail">
    <div class="modal-header">
        <div class="modal-header-img bg-research" >
            <img src="{{ asset('assets/web/img/research.png') }}" alt="" class="" />
        </div>
        {{ trans('messages.research_work') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdResearch          = isset($oUserResearch->id_user_research) ? $oUserResearch->id_user_research : old('id_user_research_hdn');
        $sResearchTitle       = isset($oUserResearch->research_title) ? $oUserResearch->research_title : old('research_title');
        $sResearchUrl         = isset($oUserResearch->research_url) ? $oUserResearch->research_url : old('research_url');
        $sResearchAuthor      = isset($oUserResearch->research_author) ? $oUserResearch->research_author : old('research_author');
        $sResearchDescription = isset($oUserResearch->research_description) ? $oUserResearch->research_description : old('research_description');
        $nResearchYear        = isset($oUserResearch->research_year) ? $oUserResearch->research_year : old('research_year');
        $nResearchMonth       = isset($oUserResearch->research_month) ? $oUserResearch->research_month : old('research_month');
    ?>
    <div class="modal-body clearfix">
        <form id="add_research_form" class="center" >
            {!! csrf_field() !!}
            <input id="id_user_research" name="id_user_research" type="hidden" value="{{$nIdResearch}}" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="research_title" class="form-control {{($errors->has('research_title')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.research_title') }}"
                       title="{{($errors->has('research_title')) ? $errors->first('research_title') : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="research_title" value="{{$sResearchTitle}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="research_url" class="form-control {{ ($errors->has('research_url')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.research_work_url') }}"
                       title="{{ ($errors->has('research_url')) ? $errors->first('research_url') : '' }}" data-toggle="tooltip"  data-placement="top"  
                       name="research_url" value="{{$sResearchUrl}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="research_author" class="form-control {{($errors->has('research_author')) ? 'error' :'' }}"
                       type="text" placeholder="{{ trans('messages.research_authors') }}"
                       title="{{($errors->has('research_author')) ? $errors->first('research_author') :'' }}" data-toggle="tooltip"  data-placement="top"  
                       name="research_author" value="{{$sResearchAuthor}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <textarea id="research_description" class="form-control {{ ($errors->has('research_description')) ? 'error' : '' }}"
                          placeholder="{{ trans('messages.research_description') }}"
                          title="{{ ($errors->has('research_description')) ? $errors->first('research_description') : '' }}" data-toggle="tooltip"  data-placement="top"
                          name="research_description">{{$sResearchDescription}}</textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left">
                <span class="select-lable">{{ trans('messages.research_date') }}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="date-select  pull-left {{ ($errors->has('research_year') || $errors->has('research_month')) ? 'error' :'' }}" >
                    <div class="date-month " title="{{ ($errors->has('research_year')) ? $errors->first('research_year') :'' }}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="research_month" name="research_month" class="form-control">
                            <option value="" disabled selected hidden>{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nResearchMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nResearchMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nResearchMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nResearchMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nResearchMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nResearchMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nResearchMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nResearchMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nResearchMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nResearchMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nResearchMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nResearchMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="research_year" class="form-control"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('research_year')) ?$errors->first('research_year') : '' }}" data-toggle="tooltip"  data-placement="top"
                           name="research_year" value="{{$nResearchYear}}" onkeypress="return isNumberKey(event);" maxlength="4">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="research_submit" class="btn btn-primary pull-right" type="button" value="{{ empty($nIdResearch) ? trans('messages.save') : trans('messages.update') }}" name="ja_save_btn_aii" onclick="submitAjaxForm('add_research_form', '{{route('user.add-research')}}', this )">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });    
</script>
