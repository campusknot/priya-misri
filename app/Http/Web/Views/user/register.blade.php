@extends('layouts.web.login_layout')

@section('title', 'Campusknot-Signup')

@section('content')

<div class="container-fluid banner-img">
    <div class="banner-overlay">
    </div>
    <div class= "col-lg-8 col-sm-8 col-md-9 col-xs-9 main-form">
        <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 logo-section">
            <div class="ck-logo">
                <img src="{{asset('assets/web/img/main_header_logo.png')}}" alt=""/>
                <h2>{{ trans('messages.signup_title_text') }}</h2>
            </diV>
        </div>
        <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 login-form-section">
            <form id="signup" name="signup" method="POST" action="{{ url('/signup') }}">
            {!! csrf_field() !!}
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                 <div class="form-group  ppadding-r">
                <input type="text" title="{{ ($errors->has('first_name'))? $errors->first('first_name') : '' }}" 
                       autocomplete="off" id="first_name" name="first_name" class="form-control {{ ($errors->has('first_name'))? 'error': '' }}" data-toggle="tooltip" data-placement="top"  placeholder="{{ trans('messages.first_name') }}" value="{{ old('first_name') }}" onchange="applyCapitalization(this);">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                  <div class="form-group ">
                <input type="text" title="{{ ($errors->has('last_name')) ? $errors->first('last_name') : '' }}"   
                       autocomplete="off" id="last_name" name="last_name" class="form-control {{ ($errors->has('last_name')) ? 'error' : '' }}" data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.last_name') }}" value="{{ old('last_name') }}" onchange="applyCapitalization(this);">
                </div>  
            </div>
          
           
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                 <div class="form-group">
                <input type="text"  title="{{ ($errors->has('email')) ? $errors->first('email') : ''}}" 
                       autocomplete="off" onblur="getCampuses(this.value, 'campus_list');return false;" id="email" name="email" class="form-control {{ ($errors->has('email')) ? 'error' : ''}}" data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.xyz@university.edu') }}" value="{{ old('email') }}">
                 </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div id="campus_list" class=" form-group pos-relative">
                <span class="caret"></span>
                <select id="campus" name="campus" class="form-control {{ ($errors->has('campus')) ? 'error' : '' }}" title="{{ ($errors->has('campus')) ? $errors->first('campus') : '' }}" data-toggle="tooltip" data-placement="top">
                    <option value="" disabled selected>{{ trans('messages.select_campus') }}</option>
                    @foreach($aCampuses as $aCampus)
                    <option value="{{ $aCampus->id_campus }}" {{ (old('campus') == $aCampus->id_campus) ? "selected = 'selected'" : "" }}>{{ $aCampus->campus_name }}</option>
                    @endforeach
                </select>
            </div>
            </div>
            
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                <div class="radio radio-primary form-group">
                    <input type="radio" name="user_type" id="student" value="S" {{ (old('user_type') == 'S') ? "checked = 'checked'" : '' }}>
                    <label for="student">
                        {{ trans('messages.signup_as_student') }}
                    </label>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                <div class="radio radio-primary form-group">
                    <input type="radio" name="user_type" id="faculty" value="F" {{ (old('user_type') == 'F') ? "checked = 'checked'" : '' }}>
                    <label for="faculty">
                        {{ trans('messages.signup_as_faculty') }}
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="clearfix">
                   @if ($errors->has('user_type'))
                    <span class="error_message"><?php echo $errors->first('user_type'); ?></span>
                   @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                 <div class="form-group ppadding-r">
                    <input type="password" title="{{ ($errors->has('password')) ? $errors->first('password') : '' }}" 
                       autocomplete="off" id="password" name="password" kl_virtual_keyboard_secure_input="on" data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.password') }}" class="form-control {{ ($errors->has('password')) ? 'error' : '' }}">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">
                 <div class="form-group ">
                     <input type="password" title="{{ ($errors->has('confirm_password')) ? $errors->first('confirm_password') : '' }}" 
                       autocomplete="off" id="confirm_password" name="confirm_password" kl_virtual_keyboard_secure_input="on" 
                       placeholder="{{ trans('messages.confirm_password') }}"  data-toggle="tooltip" data-placement="top" class="form-control {{ ($errors->has('confirm_password')) ? 'error' : '' }}">
                </div>
            </div>
           
            <div class="form-group clearfix">
                    <input type="checkbox" id="agree" name="agree" value="1" {{ (old('agree')) ? 'checked' : '' }} onclick="changeButtonAbility(this, 'signup-button');">
                    <label class="col-lg-11 col-md-11 col-sm-11 col-xs-11 terms">

                        {!! trans('messages.terms_and_condition', ['terms' => url('/terms') ,'privacy'=> url('/privacy') ]) !!}
                    </label>
            </div>
            <div class="form-group text-center submit-options">
                <input id="signup-button" type="submit" value="{{ trans('messages.sign_up') }}" class="login-btn" {{ (old('agree')) ? '' : 'disabled' }}>
            </div>
        </div>
        
    </div>
</div>















@stop
