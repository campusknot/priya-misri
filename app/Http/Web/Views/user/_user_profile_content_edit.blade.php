<div class="profile-Content">
    <div class="profile-details">
        <!-- Education details -->
        <div class="heading">
            <span class="profile-header-img bg-education">
                <img src="{{ asset('assets/web/img/education.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.education') }}</h3>
            <a href="javascript:void(0);" class="glyphicon-plus pull-right" onclick="showEducationLightbox();"></a>
        </div>
        <div class="content">
            @foreach($user_profile['education_list'] as $education)
            <div class="detail">
                <span class="sub-heading">
                    <p>{!! setTextHtml($education->university) !!}</p>
                    <div class="pull-right edit-profile" id="options_edit">
                        <a onclick="getEducationInfo('{{$education->id_user_education}}');" href="javascript:void(0);" class=""> {{ trans('messages.edit') }}</a>
                        <a  href="javascript:void(0)" class="" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'education']) }}','{{ route('user.delete-education',[$education->id_user_education]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">{{ trans('messages.delete') }}</a>
                        @if(!$education->default_education)
                        <a href="{{ route('user.default-education',[$education->id_user_education]) }}" class="set-default-major"> {{ trans('messages.default_education') }} </a>
                        @endif
                    </div>
                </span>
                <ul>	
                    <li>{!! setTextHtml($education->degree) !!}@if($education->major != '') in {!! setTextHtml($education->major) !!} @endif</li>
                    @if(!empty($education->classification))
                    <li>Classification: {!! setTextHtml($education->classification) !!}</li>
                    @endif
                    <li><?php $carbon = new Carbon($education->start_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year }}
                        &nbsp;-&nbsp;
                        @if(isset($education->end_year) && $education->end_year != NULL)
                            <?php $carbon = new Carbon($education->end_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                        @else
                            Present
                        @endif
                    </li>
                </ul>
            </div>
            @endforeach    
        </div>
    </div>
    <!-- work experience start here -->
    <div class="profile-details">
        <div class="heading">
            <span class="profile-header-img bg-work">
                <img src="{{ asset('assets/web/img/work.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.work_experience') }} </h3>
            <a href="javascript:void(0);" class="glyphicon-plus pull-right" data-toggle="modal" data-target="#edit_work_experience"  onclick="showWorkExpLightbox();"></a>
        </div>
        <div class="content">
            @foreach($user_profile['work_experience_list'] as $oWe)
            <div class="detail">
                <span class="sub-heading"><p>{!! setTextHtml($oWe->company_name) !!}</p>
                    <div class="pull-right edit-profile" id="options_edit">
                        <a class="" onclick="getWorkExpLightbox('{{$oWe->id_user_work_experience}}');" href="javascript:void(0);" >  {{ trans('messages.edit') }}</a>
                        <a  href="javascript:void(0)" class="" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'work experience']) }}','{{ route('user.delete-workexperience',[$oWe->id_user_work_experience]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">{{ trans('messages.delete') }}</a>
                    </div>
                </span>
                <ul>	
                    <li>{!! setTextHtml($oWe->job_title) !!}</li>
                    <li>{!! setTextHtml($oWe->job_description) !!}</li>
                    <li><?php $carbon = new Carbon($oWe->start_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year }}
                        &nbsp;-&nbsp;
                        @if(isset($oWe->end_year) && $oWe->end_year != NULL)
                            <?php $carbon = new Carbon($oWe->end_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                        @else
                            Present
                        @endif
                    </li>
                </ul>
            </div>
            @endforeach    
        </div>
    </div>
    <!-- work experience ends here -->
    <!-- organization information starts here-->
    <div class="profile-details">
        <div class="heading">
             <span class="profile-header-img bg-organization">
                <img src="{{ asset('assets/web/img/organization.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.organization') }} </h3><a href="javascript:void(0);" class="glyphicon-plus pull-right" onclick="showOrgLightbox();" ></a>
        </div>
        <div class="content">
            @foreach($user_profile['organization_list'] as $organization)
            <div class="detail">
                <span class="sub-heading"><p>{!! setTextHtml($organization->committee_name) !!}</p>
                    <div class="pull-right edit-profile" id="options_edit">
                        <a  onclick="getOrgInfoLightbox('{{$organization->id_user_co_curricular_activity}}');" href="javascript:void(0);"  >
                             {{ trans('messages.edit') }}
                        </a>
                        <a  href="javascript:void(0)" class="" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'organization']) }}','{{ route('user.delete-organization',[$organization->id_user_co_curricular_activity]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">{{ trans('messages.delete') }}</a>
                    </div>
                </span>
                <ul>
                    <li>{!! setTextHtml($organization->post) !!}</li>
                    <li> <?php $carbon = new Carbon($organization->start_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year }}
                        &nbsp;-&nbsp;
                        @if(isset($organization->end_year) && $organization->end_year != NULL)
                            <?php $carbon = new Carbon($organization->end_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                        @else
                            Present
                        @endif
                    </li>
                </ul>
            </div>
            @endforeach    
        </div>
    </div>
    <!-- organization information ends here-->
    <!-- Awards and Honors information starts here-->
    <div class="profile-details">
        <div class="heading">
            <span class="profile-header-img bg-award">
                <img src="{{ asset('assets/web/img/awards.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.awards_honors') }} </h3><a href="javascript:void(0);" class="glyphicon-plus pull-right" data-toggle="modal" data-target="#edit_awards" onclick="showAwardLightBox();"></a>
        </div>
        <div class="content">
            @foreach($user_profile['award_list'] as $award)
            <div class="detail">
                <span class="sub-heading"><p>{!! setTextHtml($award->award_title) !!}</p>
                    <div class="pull-right edit-profile" id="options_edit">
                        <a onclick="getAwardHonorInfoLightBox({{$award->id_user_award}});" href="javascript:void(0);" >
                           {{ trans('messages.edit') }}</a>
                            <a  href="javascript:void(0)"class="pull-right" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'award']) }}','{{ route('user.delete-award',[$award->id_user_award]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">{{ trans('messages.delete') }}</a>
                    </div>
                </span>
                <ul>
                    <li>{!! setTextHtml($award->award_issuer) !!}</li>
                    <li>{!! setTextHtml($award->award_description) !!}</li>
                    <li><?php $carbon = new Carbon($award->awarded_year) ?>
                        {{ $carbon->format('F') }} {{ $carbon->year}} 
                    </li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
    <!-- Awars and Honors information ends here-->
    <!-- Aspirations information starts here-->
    <div class="profile-details">
        <div class="heading ">
            <span class="profile-header-img bg-aspiration">
                <img src="{{ asset('assets/web/img/aspirations.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.aspirations') }}</h3><a class="glyphicon-plus pull-right" href="javascript:void(0);"  data-toggle="modal"  data-target="#edit_aspiration" onclick="showAspirationLightBox();"></a>
        </div>
        @if(trim($user->aspiration)!='')
        <div class="content">
            <div class="detail">
                <p class="no-inrormation">{!! setTextHtml($user->aspiration) !!}</p>
            </div>
        </div>
        @endif
    </div>
    <!-- Aspirations information starts here-->
    <!-- Journal Article information starts here-->
    <div class="profile-details">
        <div class="heading">
            <span class="profile-header-img bg-jornal">
                <img src="{{ asset('assets/web/img/journal.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.journal_article') }}</h3><a href="javascript:void(0);" class="glyphicon-plus pull-right" data-toggle="modal" data-target="#edit_journal_articles" onclick="showJournalLightBox();"></a>
        </div>
        <div class="content">
            @foreach($user_profile['journal_list'] as $ojournal)
            <div class="detail">
                <span class="sub-heading"><p>
                        {!! setTextHtml($ojournal->journal_title) !!}
                            <span class="publish-date"><?php $carbon = new Carbon($ojournal->journal_year) ?>
                                {{ $carbon->format('F') }} {{ $carbon->year }}
                            </span>
                        
                        </p> 
                    <div class="pull-right edit-profile" id="options_edit">
                        <a onclick="getJournalArticleInfo('{{ $ojournal->id_user_journal }}');" href="javascript:void(0);" >
                         {{ trans('messages.edit') }}
                        </a>
                        <a   href="javascript:void(0)" class="pull-right" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'journal']) }}','{{ route('user.delete-journal',[$ojournal->id_user_journal]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">
                         {{ trans('messages.delete') }}
                        </a>
                    </div>
                </span>
                <ul>
                    <li><p>{{ trans('messages.by') }} {!! setTextHtml($ojournal->journal_author) !!}</p></li>
                    <li><p><a href="{{ $ojournal->journal_url }}" target="_blank">{{ $ojournal->journal_url }}</a></p></li>
                    <li><p>{!! setTextHtml($ojournal->journal_description) !!}</p></li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
    <!-- Journal Article information ends here-->
    <!-- Research Work information starts here-->
    <div class="profile-details">
        <div class="heading research-work">
            <span class="profile-header-img bg-research">
                <img src="{{ asset('assets/web/img/research.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.research_work') }}</h3>
            <a href="javascript:void(0);" class="glyphicon-plus pull-right " onclick="showResearchLightBox();"></a>
        </div>
        <div class="content">
            @foreach($user_profile['research_list'] as $oResearch)
            <div class="detail">
                <span class="sub-heading"><p>{!! setTextHtml($oResearch->research_title) !!}
                    <span class="publish-date"><?php $carbon = new Carbon($oResearch->research_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }}
                    </span></p>
                    <div class="pull-right edit-profile" id="options_edit">
                        <a onclick="getResearchWorkInfo({{$oResearch->id_user_research}});"
                           href="javascript:void(0);" > 
                             {{ trans('messages.edit') }}</a>
                             <a  href="javascript:void(0)" class="pull-right" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'research work']) }}','{{ route('user.delete-researchwork',[$oResearch->id_user_research]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">
                              {{ trans('messages.delete') }}
                             </a>
                    </div>
                </span>
                <ul>
                    <li><p>{{ trans('messages.by') }} {!! setTextHtml($oResearch->research_author) !!}</p></li>
                    <li><a href="{{ $oResearch->research_url }}" target="_blank"> {{ $oResearch->research_url }}</a></li>
                    <li><p>{!! setTextHtml($oResearch->research_description) !!}</p></li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
    <!-- Research Work information ends here-->
    <!-- Publications information starts here-->
    <div class="profile-details">
        <div class="heading publication">
            <span class="profile-header-img bg-publication">
                <img src="{{ asset('assets/web/img/publications.png') }}" alt="" class="" />
            </span>
            <h3>{{ trans('messages.publication') }} </h3><a href="javascript:void(0);" class="glyphicon-plus pull-right" data-toggle="modal" data-target="#edit_publications" onclick="showPublicationLightBox();"></a>
        </div>
        <div class="content">
            @foreach($user_profile['publication_list'] as $oPublication)
            <div class="detail">
                <span class="sub-heading"><p>{!! setTextHtml($oPublication->publication_title) !!} 
                    <span class="publish-date"><?php $carbon = new Carbon($oPublication->publication_year) ?>
                            {{ $carbon->format('F') }} {{ $carbon->year }} 
                    </span></p>
                    <div class="pull-right edit-profile" id="options_edit">
                    <a onclick="getPublicationsInfo({{$oPublication->id_user_publication}});"
                       href="javascript:void(0);" >
                         {{ trans('messages.edit') }}</a>
                        <a  href="javascript:void(0)" class="pull-right" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ['record_name' => 'publication']) }}','{{ route('user.delete-publication',[$oPublication->id_user_publication]) }}', '{{ trans('messages.delete_confirm_button_text') }}', '{{ trans('messages.delete_cancel_button_text') }}');">
                        {{ trans('messages.delete') }}
                        </a>

                    </div>
                </span>
                <ul>
                    <li><p>{{ trans('messages.by') }}  {!! setTextHtml($oPublication->publication_author) !!}</p></li>
                    <li><a href="{{ $oPublication->publication_url }}" target="_blank">
                            {{ $oPublication->publication_url }}</a></li>
                    <li><p>{!! setTextHtml($oPublication->publication_description) !!}</p></li>
                </ul>
            </div>
             @endforeach  
        </div>
    </div>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
        });
    </script>
    <!-- Research Work information ends here-->
</div>