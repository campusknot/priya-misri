@extends('layouts.web.main_layout')
@section('title', 'Campusknot-Search User')

@section('content')
<div id="global-search">
    <div class="global-search-result-page">
        <div class="search-result-page clearfix" id="search-result-page">
            @include('WebView::user._more_global_search')
        </div>
    </div>
</div>

    <div class="switch-tab-loader side-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>



@section('footer-scripts')


    var nCurrentPage = <?php echo $oUser->currentPage(); ?>;
    var nLastPage = <?php echo $oUser->lastPage(); ?>;
    $(window).scroll(function (event) {
        if($(window).scrollTop() + $(window).height() == $(document).height() ) {
            if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                nCurrentPage +=1;
                loadNewData('search-result-page', '<?php echo route('user.global-search'); ?>', nCurrentPage,'&globalSearch=<?php echo htmlspecialchars($sGSearchStr); ?>');
            }
        }
    });
@endsection
@stop