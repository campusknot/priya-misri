<!--side bar-->
<div class="sidebar">
    <div class="top-detail">
            <div class="icon-letter">
                    <span>P</span>
                    <span>H</span>
            </div>
            <div class="icon-image img-150">
                {!! setProfileImage("150",$user->user_profile_image,$user->id_user,$user->first_name,$user->last_name) !!}
            </div>
            
            <div class="entity-name bold"> 
                    {{ $user->first_name }} {{ $user->last_name }}
                    <span class="sml-light-text">{{ $user->campus->campus_name }}</span>
            </div>
        <span>
            @if(count($oIsUserFollowing) != 0)
                <a href="javascript:void(0);" class=" follow" onclick="callFollowUser(this,'{{ route("user.un-follow-user", ['user' => $user->id_user])}}',1,{{ $user->id_user }})">{{ trans('messages.unfollow') }}</a>
            @else
                <a href="javascript:void(0);" class=" follow" onclick="callFollowUser(this,'{{ route("user.follow-user", ['user' => $user->id_user])}}',0,{{ $user->id_user }})">{{ trans('messages.follow') }}</a>
            @endif
        </span>
    </div>
    <ul class="options sidebar-menu-options">
        <li id="active_profile" class="active_profile" onclick="getProfileData('{{ route('user.profile-ajax',['nIdUser'=>$user->id_user]) }}','{{ route('user.profile',['nIdUser'=>$user->id_user]) }}');">
            <a>{{ trans('messages.profile') }}</a>
        </li>
        <li id="active_image" class="" onclick="getProfileData('{{ route("user.profile-image-list-user-ajax" ,[$user->id_user]) }}','{{ route("user.profile-image-list-user" ,[$user->id_user]) }}');">
            <a>@if($aUserSpecificCounts['image_count']>0) {{ $aUserSpecificCounts['image_count'] }} @endif Photos </a>
        </li>
        <li id="active_follower" class="" onclick="getProfileData('{{ route("user.user-contacts-follower-show-ajax",[$user->id_user]) }}','{{ route("user.user-contacts-follower-show",[$user->id_user]) }}');">
            <a>@if($aUserSpecificCounts['following_count']>0) {{ $aUserSpecificCounts['following_count'] }} @endif Followers </a>
        </li>
        <li id="active_following" class="" onclick="getProfileData('{{ route("user.user-contacts-following-show-ajax",[$user->id_user]) }}','{{ route("user.user-contacts-following-show",[$user->id_user]) }}');">
            <a>@if($aUserSpecificCounts['follower_count']>0) {{ $aUserSpecificCounts['follower_count'] }} @endif Following </a>
        </li>
    </ul>
</div>
<!--side bar-->
<script type="text/javascript">
$(".options li").click(function(){
    $(this).siblings().attr('class', '');
    $(this).attr('class', '');
    var id= $(this).attr('id');
    $(this).addClass(id);
});

$(document).ready(function(){
    makeTabSelected();
});

function makeTabSelected() {
    var sPathName = location.pathname;
    var aUrlComponents = sPathName.split('/');
    
    //Make side bar tab selected
    var sAction = aUrlComponents[aUrlComponents.length - 2];
    var aActionComponents = sAction.split('-');
    
    var oActiveTab = $('#active_'+aActionComponents[aActionComponents.length - 1]);
    oActiveTab.siblings().attr('class', '');
    oActiveTab.attr('class', '');
    oActiveTab.addClass(oActiveTab.attr('id'));
}

</script>