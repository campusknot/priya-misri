@extends('layouts.web.login_layout')

@section('title', 'Campusknot-Login')

@section('content')
<div class="container-fluid banner-img">
    <div class="banner-overlay">
    </div>
    <div class= "col-lg-8 col-sm-8 col-md-9 col-xs-9 main-form">
        <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 logo-section">
            <div class="ck-logo">
                <img src="{{asset('assets/web/img/main_header_logo.png')}}" alt=""/>
                <h2>{{ trans('messages.login_page_title') }} </h2>
            </diV>
        </div>
        
        <div class= "col-lg-6 col-sm-6 col-md-6 col-xs-6 login-form-section">
            {{ Session::get('msg') }}
            @if(Session::has('msg') && Session::get('msg')== 'resend')
            <div class="resend-verification-link-wrapper">
                <div class="resend-verification-link">
                    <span>{{trans('messages.resend_mail_info')}}</span>
                     <form id="send_verification_mail" name="send_verification_mail" action="{{url('/home/send-verification-mail')}}" method="POST">
                        {!! csrf_field() !!}
                        <input type="hidden" class="form-control" name="email" value="{{ old('email') }}">
                        <input type="hidden" class="form-control" name="page" value="login">
                        <input class="btn-primary" type="submit" name="submit" value="{{ trans('messages.resend_mail_button') }}"/>
                    </form>
                </div>
               
            </div>
              
            @endif
            <form id="login-form" method="POST" action="{{ route('user.login') }}" class="login-form" >
                {!! csrf_field() !!}
                <div class="clearfix">
                    @if ($errors->has('credentials'))
                        <span class="error_message">{{ $errors->first('credentials') }}</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" id="email" title="{{($errors->has('email')) ?$errors->first('email') : '' }}" class="form-control {{($errors->has('email')) ? 'error' : '' }} " 
                           name="email" value="{{ old('email') }}"  data-toggle="tooltip" data-placement="top" placeholder="{{trans('messages.xyz@university.edu') }}" autofocus="autofocus"/>
                   
                </div>
                <div class="form-group">
                    <input type="password" id="password" title="{{ ($errors->has('password')) ? $errors->first('password') : '' }}"
                           class="form-control {{ ($errors->has('password')) ? 'error' : '' }} " name="password" value=""  data-toggle="tooltip" data-placement="top" placeholder="{{trans('messages.password') }}"/>
                </div>
                
                <div class="form-group">
                    <input type="submit" value="{{ trans('messages.login') }}" class="login-btn">
                   
                </div>
                <span><a href="{{ route('user.forgot-password') }}" class="frgt-pwd">{{trans('messages.forgot_password')}}</a></span>
                <span class="pull-right"><a href="{{ route('user.signup') }}">{{ trans('messages.sign_up_free') }}</a></span>
            </form>
        </div>
    </div>
</div>
@stop


