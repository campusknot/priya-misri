<div class="modal-content edit-information" id="edit_detail">
    <div class="modal-header">
        <div class="modal-header-img bg-jornal" >
            <img src="{{ asset('assets/web/img/journal.png') }}" alt="" class="" />
        </div>
        {{ trans('messages.journal_article') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdJournal          = isset($oUserJournal->id_user_journal) ? $oUserJournal->id_user_journal : old('id_user_journal');
        $sJournalTitle       = isset($oUserJournal->journal_title) ? $oUserJournal->journal_title : old('journal_title');
        $sJournalUrl         = isset($oUserJournal->journal_url) ? $oUserJournal->journal_url : old('journal_url');
        $sJournalAuthor      = isset($oUserJournal->journal_author) ? $oUserJournal->journal_author : old('journal_author');
        $sJournalDescription = isset($oUserJournal->journal_description) ? $oUserJournal->journal_description : old('journal_description');
        $nJournalYear        = isset($oUserJournal->journal_year) ? $oUserJournal->journal_year : old('journal_year');
        $nJournalMonth       = isset($oUserJournal->journal_month) ? $oUserJournal->journal_month : old('journal_month');
    ?>
    <div class="modal-body clearfix">
        <form id="add_journal_form" class="center">
            {!! csrf_field() !!}
            <input id="id_user_journal" name="id_user_journal" type="hidden" value="{{$nIdJournal}}" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="journal_title" class="form-control {{($errors->has('journal_title')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.journal_title') }}"
                       title="{{($errors->has('journal_title')) ? $errors->first('journal_title') : '' }}" data-toggle="tooltip"  data-placement="top"  
                       name="journal_title" value="{{$sJournalTitle}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="journal_url" class="form-control {{($errors->has('journal_url')) ? 'error'  : '' }}"
                       type="text" placeholder="{{ trans('messages.article_url') }}"
                       title="{{($errors->has('journal_url')) ?$errors->first('journal_url')  : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="journal_url" value="{{$sJournalUrl}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="journal_author" class="form-control {{($errors->has('journal_author')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.authors') }}"
                       title="{{($errors->has('journal_author')) ?$errors->first('journal_author') : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="journal_author" value="{{$sJournalAuthor}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <textarea id="journal_description" class="form-control"
                          placeholder="{{ trans('messages.journal_description') }}"
                          title="{{($errors->has('journal_description')) ?$errors->first('journal_description') : '' }}" data-toggle="tooltip"  data-placement="top" 
                          name="journal_description" >{{$sJournalDescription}}</textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left">
                <span class="select-lable">{{ trans('messages.journal_date') }}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="date-select  pull-left {{ ($errors->has('journal_month') || $errors->has('journal_year') ) ? 'error': ''}}" >
                    <div class="date-month " title="{{ ($errors->has('journal_month')) ? $errors->first('journal_month'): ''}}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="journal_month" class="form-control" name="journal_month" required>
                            <option value="" >{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nJournalMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nJournalMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nJournalMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nJournalMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nJournalMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nJournalMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nJournalMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nJournalMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nJournalMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nJournalMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nJournalMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nJournalMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="journal_year" class="form-control {{($errors->has('journal_year')) ? 'error': '' }}"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('journal_year')) ? $errors->first('journal_year'): '' }}" data-toggle="tooltip"  data-placement="top" 
                           name="journal_year" onkeypress="return isNumberKey(event);"  value="{{$nJournalYear}}" maxlength="4" >
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="ja_save_btn_aii" class="btn btn-primary pull-right" type="button" name="ja_save_btn_aii" value="{{ empty($nIdJournal) ? trans('messages.save') : trans('messages.update') }}" onclick="submitAjaxForm('add_journal_form', '{{route('user.add-journal')}}', this )">
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   

    });    
</script>
