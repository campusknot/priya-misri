@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Account Settings')

@section('content')
<div class="innerpage">
    <div class="inner-page-heading padding-10">
        <h3>{{trans('messages.account_settings')}}</h3>
    </div>
 
       
        <form id="account_settings_form" name="account_settings_form" class="account-settings-form" method="post" action="{{ url('/account-settings') }}">
            {!! csrf_field() !!}
         @if (Session::has('account_settings_message'))
            <div class="alert alert-info">{{ Session::get('account_settings_message') }}</div>
        @endif
        <form id="account_settings_form" name="account_settings_form" class="" method="post" action="{{ url('/account-settings') }}">
            {!! csrf_field() !!}
           <div class="content-area">
                <div class="form-group clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <label class="form-label">{{trans('messages.first_name')}}</label>
                    </div>
                      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <label class="form-label">{{trans('messages.last_name')}}</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <input type="text" id="first_name" class="form-control" name="first_name" value="{{ $oUser->first_name }}" placeholder="{{trans('messages.first_name')}}" onchange="applyCapitalization(this);">
                            @if ($errors->has('first_name'))
                                <span class="error_message"><?php echo $errors->first('first_name'); ?></span>
                            @endif
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <input type="text" id="last_name" class="form-control" name="last_name" value="{{ $oUser->last_name }}" placeholder="{{trans('messages.last_name')}}" onchange="applyCapitalization(this);">
                            @if ($errors->has('last_name'))
                                <span class="error_message"><?php echo $errors->first('last_name'); ?></span>
                            @endif
                    </div>
                </div>
                <div class="form-group clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                         <label class="form-label">{{trans('messages.alternate_email')}}</label>
                        <input type="text" id="alternate_email" class="form-control" name="alternate_email" value="{{ $oUser->alternate_email }}" placeholder="{{trans('messages.alternate_email')}}">
                          @if ($errors->has('alternate_email'))
                              <span class="error_message"><?php echo $errors->first('alternate_email'); ?></span>
                          @endif
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <label class="form-label">{{trans('messages.email')}}</label>
                         <input type="text" id="email" class="form-control" name="email" value="{{ $oUser->email }}" placeholder="{{trans('messages.email')}}" disabled="disabled">
                    </div>
                </div>
           </div>
             <div class="content-area">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                    <h4 class="sub-head">{{ trans('messages.change_password') }}</h4>
                </div>
           
                <div class="form-group clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label class="form-label">{{ trans('messages.current_password') }}</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6  " title="{{ ($errors->has('current_password')) ? $errors->first('current_password') : '' }}" data-toggle="tooltip" data-placemet="top" >
                        <input type="password" id="current_password" class="form-control {{ ($errors->has('current_password')) ? 'error' : '' }}" name="current_password" placeholder="{{ trans('messages.current_password') }}">
                       
                    </div>
                </div>
                  <div class="form-group clearfix">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <label class="form-label">{{ trans('messages.new_password') }}</label>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 "  title="{{ ($errors->has('new_password')) ? $errors->first('new_password') : '' }}" data-toggle="tooltip" data-placemet="top">
                            <input type="password" id="new_password" class="form-control {{ ($errors->has('new_password')) ? 'error' : '' }}" name="new_password" placeholder="{{ trans('messages.new_password') }}">
                            
                        </div>
                    </div>
                <div class="form-group clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <label class="form-label">{{ trans('messages.repeat_new_password') }}</label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" title="{{ ($errors->has('repeat_new_password')) ? $errors->first('repeat_new_password') : '' }}" data-toggle="tooltip" data-placemet="top">
                        <input type="password" id="repeat_new_password" class="form-control {{ ($errors->has('repeat_new_password')) ? 'error' : '' }}" name="repeat_new_password" placeholder="{{ trans('messages.repeat_new_password') }}">
                      
                    </div>
                </div>

                 <div class="form-group clearfix">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                        <div class="">
                            <input type="submit" class="btn btn-primary min-btn-width" value="{{ trans('messages.save') }}" name="save">
                            <button onclick="document.location='{{ url('/') }}'" type="button" class="btn btn-default min-btn-width" data-dismiss="modal">{{ trans('messages.cancel') }}</button>
                        </div>
                    </div>
                 </div>
                 
                 
                
            </div>   
        </form>
        
    
    
    
</div>

@stop