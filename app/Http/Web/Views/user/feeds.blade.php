@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Home')


@section('content')
<!-- Filename::user.feeds  -->
<div class="tabbable pos-relative clearfix">
    @if (session('error_message'))
        <div class="clearfix alert alert-danger">{{ session('error_message') }}</div>
    @endif
    <!-- group tabs -->
    <ul class="nav nav-tabs group-tabs group-tabs-dropdown ">
        <li class="active" data-toggle="tooltip" data-placement="top" title="" nowrap> <a href="{{ url('/') }}" class="mightOverflow show-tooltip" >All</a></li>
        @foreach($oGroupListTab as $oGroup)
            <li id="groupId_{{$oGroup->id_group}}" data-toggle="tooltip" data-placement="top" title="{{$oGroup->group_name}} 
                {{ ($oGroup->section != '') ? trans('messages.section').'-'.$oGroup->section.'('.$oGroup->semester.')' : '' }}" nowrap> 
                <a href="{{ route('group.group-feeds', ['nIdGroup' => $oGroup->id_group]) }}" class="mightOverflow " nowrap>
                    {{$oGroup->group_name}}
                    @if($oGroup->section != '')
                        {{ trans('messages.section') }}- {{$oGroup->section}} ({{$oGroup->semester}})
                    @endif
                </a>
            </li>
        @endforeach
    </ul>
    <div class="share-bg  padding-10 clearfix">
        @include('WebView::post._add_post')
    </div>

    <div class="tab-content"> 
        <div id="feeds" class="clearfix feeds">
            @include('WebView::user._more_feeds')
        </div>
        <div class="more-data-loader hidden">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
    </div>
</div>

@section('footer-scripts')


    var nCurrentPage = <?php echo $oUserFeeds->currentPage(); ?>;
    var nLastPage = <?php echo $oUserFeeds->lastPage(); ?>;
    $(window).scroll(function (event) {
        if($(window).scrollTop() + $(window).height() == $(document).height() ) {
            if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                nCurrentPage +=1;
                loadNewData('feeds', '<?php echo route('/'); ?>', nCurrentPage);
            }
        }
    });
    
    function showRequestComment(formData, jqForm, options)
    { 
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
    }
    
    function showResponseComment(response, statusText, xhr, $form)
    { 
        if(response.success == true)
        {
            if(response.type=='poll'){
                $('#'+response.id_post).html(response.html);
            }
            else
            {
                $('#add_comment_'+ response.id_post)[0].reset();
                var count = $('#count_'+ response.id_post).text();
                $('#count_'+ response.id_post).text(parseInt(count)+1);
                $('#comment-box-'+ response.id_post).removeClass('hidden');
                $('#ext_comments_'+response.id_post).append(response.html);
                removeErrorComment(response.id_post);
            }
            $('.btn-md').button('reset');
            setAutosizeTextAreaHeight(30);
        }
        else
        { 
            var arr = response;
                $.each(arr, function(index, value)
                {
                        if (value.length != 0)
                        {
                                $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                        }
                });                     
        }
    }
    
    function showErrorComment(xhr, textStatus, errorThrown)
    {
        if(xhr.status == 500 ){
            window.location = siteUrl + '/home';
        }
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
            }
        
        var result = $.parseJSON(arr);
        $.each(result, function(key, value)
        {
            key=key.replace('file','comment_text');
            key=key.replace('extention','comment_text');
            $('.btn-md').button('reset');
            if (key.length != 0)
            {
                    $("#"+key).html('<strong>'+ value +'</strong>');
            }
        });
    }
    
    function removeErrorComment(id)
    {
        $("#comment_text_"+id).html('');
    }
    
    
    $(document).ready(function(){
        setAutosizeTextAreaHeight(30);
        //callAutoSizeTextArea();
        $(document).on('click',".comment-attchment label:first-child",function(){
              $(this).parent().siblings("#file-input").addClass("show-input");
              $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
              $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any document ");
              
        });
        $(document).on('click','.comment-attchment label:last-child',function(){
            var id='file_'+$(this).attr('data-id');
            $(this).parent().siblings("#file-input").addClass("show-input");
            $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
            $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any photo ");
              
        });
        $(document).on('click','.file-attachment-cancel',function(){
            var id = $(this).attr('data-id');
            $('#file_'+id).val('').clone(true);
            $(this).parent().removeClass("show-input");
            input = $(".file-attachment-name").val('');
        });
        
        $(document).on('fileselect','.file-attachment-btn', function(event, numFiles, label) {
            showCustomBrowseButton(event, numFiles, label, this);
        });
    
        var options = { 
        beforeSubmit:  showRequestComment,
        success:       showResponseComment,
        error:showErrorComment,
        dataType: 'json' 
        }; 
        
        setInterval(function() {
            $(".minute").each(function(){
                var ele=$(this);
                var myTime = ele.val()-1;
                if(myTime <= 300 ){
                    var MinSec=getTime(myTime);
                    ele.siblings('span').text(MinSec);
                    ele.siblings('span').removeClass('hidden');
                    ele.siblings('div').addClass('hidden');
                }
                ele.val(myTime);

                if(myTime <= 0){
                    ele.parent().addClass('hidden');
                    var parent=ele.parent().parent().children('form');
                    parent.children('input[name=expired]').val(1);
                    parent.children('button').trigger( "click" );
                    
                }
            });
        },  1000);

        $(document).on('click','.btn-comment',function(e){
            var data = $(this);
            var id_post = data.attr('data-id');
            $(this).button('loading');
            $('#add_comment_'+id_post).ajaxForm(options).submit();
        });
        
        $(document).on('click','.btn-poll',function(e){
            var data = $(this);
            var id_poll = data.attr('data-id');
            $(this).button('loading');
            $('#poll_'+id_poll).ajaxForm(options).submit();
        });
        findUrlThumbnails();
        
        <?php //if($bShowCoursePopup) { ?>
            //showAddCourseLightBox();
        <?php //} ?>
    });
    
    
@endsection

@section('right_sidebar')
     @include('WebView::user._feeds_sidebar')
@endsection

@stop
