@if(\Request::route()->getName() == 'user.user-contacts-follower-show' )
@if(count($user_follower_list))
    @foreach($user_follower_list as $follower_list)
    <!-- Start: user detail div start -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
        <div class="text-left single-member pos-relative">
            <a href="{{route('user.profile',[$follower_list->id_user])}}">
                <div class="contact-pic icon-image img-66 pos-relative">
                    {!! setProfileImage("150",$follower_list->user_profile_image,$follower_list->id_user,$follower_list->first_name,$follower_list->last_name) !!}
                    @if($follower_list->user_type == config('constants.USERTYPEFACULTY'))
                        <span class="faculty-label">{{config('constants.USERTYPEFACULTY') }}</span>
                    @endif
                </div>
                <div class="contact-info display-ib v-align">
                    <span class="{{$follower_list->user_type}}-name font-w-500">{{$follower_list->first_name}}&nbsp;&nbsp;{{$follower_list->last_name}}</span>

                    </span>
                    <span class="mightOverflow user-degree  set-max-limit text-left">
                        @if(count($follower_list->major)>0)
                            {{$follower_list->major['degree']}} {{ ($follower_list->major['major']) ? ','.$follower_list->major['major'] : ''}}
                        @endif
                    </span>
                </div>
            </a>

            <span class="follow-wrapper">
                @if($follower_list->id_follow!='')
                    <a href="javascript:void(0);" class=" btn btn-outline pull-right" onclick="callFollowUser(this,'{{ route("user.un-follow-user", ['user' => $follower_list->id_user])}}',1,{{ $follower_list->id_user }})">{{ trans('messages.unfollow') }}</a>
                @else
                    <a href="javascript:void(0);" class=" btn btn-outline pull-right" onclick="callFollowUser(this,'{{ route("user.follow-user", ['user' => $follower_list->id_user])}}',0,{{ $follower_list->id_user }})">{{ trans('messages.follow') }}</a>
                @endif
                </span>
           
        </div>
    </div>
    <!-- End: user detail div start -->
    @endforeach
    @endif
@endif
@if(\Request::route()->getName() == 'user.user-contacts-following-show')
    @if(count($user_following_list))
    @foreach($user_following_list as $following_list)
    <!-- Start: user detail div start -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix ">
        <div class="text-left single-member pos-relative">
            <a href="{{route('user.profile',[$following_list->id_user])}}">
                <div class="contact-pic icon-image img-66 pos-relative">
                    {!! setProfileImage("150",$following_list->user_profile_image,$following_list->id_user,$following_list->first_name,$following_list->last_name) !!}
                    @if($following_list->user_type == config('constants.USERTYPEFACULTY'))
                        <span class="faculty-label">{{config('constants.USERTYPEFACULTY') }}</span>
                    @endif
                </div>
                <span class="contact-info display-ib v-align">
                    <span  class="{{$following_list->user_type}}-name font-w-500">{{$following_list->first_name}}&nbsp;&nbsp;{{$following_list->last_name}}</span>
                    <span class="mightOverflow user-degree set-max-limit text-left">
                        @if(count($following_list->major)>0)
                            {{$following_list->major['degree']}}, {{$following_list->major['major']}}

                        @endif
                    </span>
                </span>
            </a>

            @if($following_list->id_user != Auth::user()->id_user)
            <span class="follow-wrapper">
                @if($following_list->id_follow!='')
                    <a href="javascript:void(0);" class=" btn btn-outline pull-right" onclick="callFollowUser(this,'{{ route("user.un-follow-user", ['user' => $following_list->id_user])}}',1,{{ $following_list->id_user }})">{{ trans('messages.unfollow') }}</a>
                @else
                    <a href="javascript:void(0);" class=" btn btn-outline pull-right" onclick="callFollowUser(this,'{{ route("user.follow-user", ['user' => $following_list->id_user])}}',0,{{ $following_list->id_user }})">{{ trans('messages.follow') }}</a>
                @endif
                </span>
            @endif

        </div>

    </div>
    <!-- End: user detail div start -->
    @endforeach
    @endif
@endif