<div class="modal-content edit-information"  id="edit_detail">
    <div class="modal-header">
        <div class="modal-header-img bg-publication" >
            <img src="{{ asset('assets/web/img/publications.png') }}" alt="" class="" />
        </div>
        {{ trans('messages.publication') }}
        <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    </div>
    <?php
        $nIdPublication          = isset($oUserPublication->id_user_publication) ? $oUserPublication->id_user_publication : old('id_user_publication');
        $sPublicationTitle       = isset($oUserPublication->publication_title) ? $oUserPublication->publication_title : old('publication_title');
        $sPublicationUrl         = isset($oUserPublication->publication_url) ? $oUserPublication->publication_url : old('publication_url');
        $sPublicationAuthor      = isset($oUserPublication->publication_author) ? $oUserPublication->publication_author : old('publication_author');
        $sPublicationDescription = isset($oUserPublication->publication_description) ? $oUserPublication->publication_description : old('publication_description');
        $nPublicationYear        = isset($oUserPublication->publication_year) ? $oUserPublication->publication_year : old('publication_year');
        $nPublicationMonth       = isset($oUserPublication->publication_month) ? $oUserPublication->publication_month : old('publication_month');
    ?>
    <div class="modal-body clearfix">
        <form id="add_publication_form" class="center">
            {!! csrf_field() !!}
            <input id="id_user_publication" name="id_user_publication" type="hidden" value="{{$nIdPublication}}" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="publication_title" class="form-control {{($errors->has('publication_title')) ? 'error' : '' }}"
                       type="text" placeholder="{{ trans('messages.publication_title') }}"
                       title="{{($errors->has('publication_title')) ?$errors->first('publication_title') : '' }}" data-toggle="tooltip"  data-placement="top" 
                       name="publication_title" value="{{$sPublicationTitle}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="publication_url" class="form-control {{($errors->has('publication_url')) ? 'error' :'' }}"
                       type="text" placeholder="{{ trans('messages.publication_url') }}"
                       title="{{($errors->has('publication_url')) ?$errors->first('publication_url') :'' }}" data-toggle="tooltip"  data-placement="top" 
                       name="publication_url" value="{{$sPublicationUrl}}" >
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <input id="publication_author" class="form-control {{ ($errors->has('publication_author')) ?'error':''}}"
                       type="text" placeholder="{{ trans('messages.publication_authors') }}"
                       title="{{ ($errors->has('publication_author')) ?$errors->first('publication_author') :''}}" data-toggle="tooltip"  data-placement="top" 
                       name="publication_author" value="{{$sPublicationAuthor}}">
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <textarea id="publication_description" class="form-control {{ ($errors->has('publication_description')) ? 'error' :'' }}"
                          placeholder="{{ trans('messages.publication_description') }}"
                          title="{{ ($errors->has('publication_description')) ?$errors->first('publication_description') :'' }}" data-toggle="tooltip"  data-placement="top" 
                          name="publication_description">{{$sPublicationDescription}}</textarea>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 left">
                <span class="select-lable">{{ trans('messages.publication_date') }}</span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="date-select  pull-left {{ ($errors->has('publication_month') || $errors->has('publication_year')) ? 'error':''}}" >
                    <div class="date-month " title="{{ ($errors->has('publication_month')) ? $errors->first('publication_month'):''}}" data-toggle="tooltip"  data-placement="top">
                        <span class="caret"></span>
                        <select id="publication_month" class="form-control" name="publication_month" required>
                            <option value="" disabled selected hidden>{{ trans('messages.start_month') }}</option>
                            <option value='1' <?php echo $nPublicationMonth==1?"selected":"" ?> >January</option>
                            <option value='2' <?php echo $nPublicationMonth==2?"selected":"" ?>>February</option>
                            <option value='3' <?php echo $nPublicationMonth==3?"selected":"" ?>>March</option>
                            <option value='4' <?php echo $nPublicationMonth==4?"selected":"" ?>>April</option>
                            <option value='5' <?php echo $nPublicationMonth==5?"selected":"" ?>>May</option>
                            <option value='6' <?php echo $nPublicationMonth==6?"selected":"" ?>>June</option>
                            <option value='7' <?php echo $nPublicationMonth==7?"selected":"" ?>>July</option>
                            <option value='8' <?php echo $nPublicationMonth==8?"selected":"" ?>>August</option>
                            <option value='9' <?php echo $nPublicationMonth==9?"selected":"" ?>>September</option>
                            <option value='10' <?php echo $nPublicationMonth==10?"selected":"" ?>>October</option>
                            <option value='11' <?php echo $nPublicationMonth==11?"selected":"" ?>>November</option>
                            <option value='12' <?php echo $nPublicationMonth==12?"selected":"" ?>>December</option>
                        </select>
                    </div>
                    <input id="publication_year" class="form-control"
                           type="text" placeholder="{{ trans('messages.year') }}"
                           title="{{($errors->has('publication_year')) ?$errors->first('publication_year') : '' }}" data-toggle="tooltip"  data-placement="top"
                           name="publication_year" value="{{$nPublicationYear}}" onkeypress="return isNumberKey(event);" maxlength="4">
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 submit-options">
                <button type="button" class="btn btn-default pull-right" data-dismiss="modal" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <input id="pu_save_btn_aii" class="btn btn-primary pull-right" type="button" value="{{ empty($nIdPublication) ? trans('messages.save') : trans('messages.update') }}" name="pu_save_btn_aii"  onclick="submitAjaxForm('add_publication_form', '{{route('user.add-publication')}}', this )">
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });    
</script>
