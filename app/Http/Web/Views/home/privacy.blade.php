@extends('layouts.web.resposive_layout')

@section('title', 'Campusknot')

@section('content')

<div class="header-bg">
    
</div>
<div class=" terms-padding clearfix">
    <div class="">
         @if (session('home_page_message'))
            <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
        @endif
        @if ($errors->has('home_page_error'))
            <div class="clearfix">
                <span class="error_message text-center col-sm-12">{{ $errors->first('home_page_error') }}</span>
            </div>
        @endif
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
            <h2><b>Privacy Policy   </b></h2>
				
              <p>
                  Campusknot Inc. (“<b>Campusknot</b>” or “<b>we</b>”) provides a platform which enables students, faculties or guests (unregistered users) from universities in the USA to interact with each other through our website, accessible at <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>  (the “<b>Site</b>”; and the services provided through the Site, the “<b>Service</b>”).  Using the Site, students can interact with each other or with faculties; they can also create their groups, fraternities, sororities, student organizations, and athletic groups. The registered users can access materials and information available to them, and areas where faculties and students can post messages, share links, photos, books, articles, videos, etc. and updates about their areas of interest.  We want to make communication between faculties and students, and between students, easier so that learning can be more interactive and fun.
              </p>
              <p>
                  Campusknot provides this privacy policy (“<b>Privacy Policy</b>”) to inform you of our policies and procedures regarding the collection, use and disclosure of personal information we receive from users of the Site.  This Privacy Policy applies only to information that you provide to us through this Site.
              </p>
              <p>
                  This Privacy Policy may be updated from time to time.  If we modify this Privacy Policy, we will post the modification on the Site or provide you with notice of the modification.  We will also update the “Last Updated Date” at the top of this Privacy Policy.  You are advised to consult this policy regularly for any changes.  “<b>Member</b>”, as used in this Privacy Policy, refers to a person that completes Campusknot’s account registration process, whether as a faculty or as a student, as described under the “Account Registration” section of the Terms.
              </p>
              <h4>Internet-based transfers</h4>
              <p>Given that the Internet is a global environment, using the Internet to collect and process personal data necessarily involves the transmission of data on an international basis. Therefore, by browsing this website and communicating electronically with us, you acknowledge and agree to our processing of personal data in this way.</p>
              <h4>What information do we collect?</h4>
              <p>We collect information from you when you register on our site, subscribe to our newsletter, respond to a survey or visit as a guest.</p>
              <p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, e-mail address, phone number and university name.</p>
              <p>Google, as a third-party vendor, uses cookies to serve ads on the Site. Google's use of the DART cookie enables it to serve ads to users of the Site based on their visit to this Site and other sites on the Internet. Users may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy.</p>
              <p>
                  <b>Information collected</b>
                  <br>We collect personal information from visitors to this website through the use of online forms. If you email us, we may keep your message, email address and contact information. We collect additional information automatically about your visit to our Site.
              </p>
              <h4>How do we use your information?</h4>
              <p>Any of the information we collect from you may be used in one or more of the following ways: </p>
              <ul>
                  <li>To personalize your experience since your information helps us to better respond to your individual needs.</li>
                  <li>For the betterment of our website as we continually strive to improve our website offerings based on the information and feedback we receive from you.</li>
                  <li>To process transactions: Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever without your consent, other than for the express purpose of delivering the purchased product or service requested.</li>
                  <li>To administer a contest, promotion, survey or other site feature from time to time.</li>
                  <li>To send periodic emails; the email address provided by you may be used to send you information, respond to inquiries, alert you to important notifications, and/or other requests or questions.</li>
                  
              </ul>
              <h4>Security of your information?</h4>
              <p>We implement all reasonable security measures to maintain the safety of your personal information when you enter, submit, or access your personal information. </p>
              <h4>Cookies?</h4>
              <p>Cookies are small data files that a site or its service provider transfers to your computer’s hard drive through your Web browser (if you allow) to enable the sites or service provider’s systems to recognize your browser and capture and remember certain information. Like many websites, we use cookies to understand and save your preferences for future visits and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.
                    If you are a resident of a European Union (EU) and/or are accessing the Site from an EU region, then you are requested to stop using this Site as we are not complying with the EU cookie laws. 
              </p>
              <h4>Disclosure of any information to outside parties?</h4>
              <p>We do not sell, trade, or otherwise transfer your personally identifiable information to outside third parties. This does not include trusted third parties who assist us in operating our website, conducting our business, or providing services to you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our Site policies, or protect others or our rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
              <p>
                  <b>Disclosures</b> <br>
                  We will take reasonable steps to protect your personal information and will disclose such information to trusted third parties or business partners only to the extent that is necessary to fulfil the Services offered though the Site and when required by government bodies and law enforcement agencies as well as to successors in title to our business and suppliers we engage to process data on our behalf.
              </p>
              <h4>Third party links:</h4>
              <p>Occasionally, at the discretion of the Site’s management, we may include or offer third party products or services on our website. These third party sites have separate and independent privacy policies. We, therefore, have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>
              <h4>Children's Online Privacy:</h4>
              <p>We recognize the particular importance of protecting the privacy of children. Upon registering on the Site, the user is representing to us that he or she is an adult (age 18 or older). We do not knowingly collect personal information from children under 18.</p>
              <p>If a child (below 18 years of age) provides the Site with personally identifiable information, we ask that a parent or guardian send an email to: info@campusknot.com to request the removal of said information. We will then delete the personally identifiable information pertaining to the child from our records.</p>
              <h4>Online Privacy Policy Only:</h4>
              <p>This online privacy policy applies only to information collected through our Site and not to information collected offline.</p>
              <p>
                  <b>Access right</b><br>
                  You have a right to access the personal data held about you. To obtain a copy of the personal information we hold about you, please write to us at info@campusknot.com
              </p>
              <h4>Your Consent:</h4>
              <p>
                  By using our Website <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>, you consent irrevocably to our collection and use of your personal information as described in this Privacy Policy. We change our privacy policies and procedures in compliance with the development in laws and technology. You have to check periodically to view the changes made in this Privacy Policy; we may or may not post those changes on our Site.
             </p>
              <h4>Change in our Privacy Policy:</h4>
              <p>The Privacy Policy is subject to periodic review and change. In order to view the current Privacy Policy, simply check this section of the web site: <a href="<?php echo url('/'); ?>" rel="home" >www.Campusknot.com </a>. If we materially change our Privacy Policy, we will post those changes to this privacy statement and other places we deem appropriate so that users are aware of what information we collect, how it is used, and under what circumstances, if any, it will be disclosed. In some cases where we post a notice, we may also email users who have opted to or opted not to receive communications from us, notifying them of the changes in our privacy practices.</p>
              <h4>How To Contact Us:</h4>
              <p>If you would like to contact us for any reason regarding this Privacy Policy or our privacy practices, please write or e-mail us at info@campusknot.com:</p>
              <p>
                     <br>
                     This Privacy Policy  was last updated as of Aug 5, 2016.
                </p>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
        </div>
        

</div>
</div>

    
   
   
    

    
    
    

@stop






