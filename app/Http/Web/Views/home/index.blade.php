@extends('layouts.web.login_layout')
@section('title', 'Campusknot')
@section('content')
<div class="container-fluid ">
    <div class="row">
        @if (session('home_page_message'))
            <div class="clearfix alert alert-info">{{ session('home_page_message') }}</div>
        @endif
        @if ($errors->has('home_page_error'))
            <div class="clearfix">
                <span class="error_message text-center col-sm-12">{{ $errors->first('home_page_error') }}</span>
            </div>
        @endif
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding banner-img">
            <div class="banner-overlay">
            </div>
            <div class="slide-content">
                <h1>
                    <div>{{ trans('messages.slider_text') }}</div>
                    <span>{{ trans('messages.anytime') }}</span>
                    <span>{{ trans('messages.anywhere') }}</span>
                </h1>
                <a href="{{ route('user.signup') }}" class="btn orange-btn">{{ trans('messages.sign_up') }}</a>
                <span class="btn blue-btn " data-toggle="modal" data-target="#vdo_tour">{{ trans('messages.take_a_tour') }}</span>
            </div>
            <div class="browser-compatible">
                {{ trans('messages.compatible_browser') }}
                <span class="pos-relative browser-img">
                    <img class="" src="{{asset('assets/web/img/browser-edge.png')}}" alt="{{ trans('messages.edge') }}"/>
                    <span class="browser-name">{{ trans('messages.edge') }}</span>
                </span>
                <span class="pos-relative browser-img">
                    <img class="" src="{{asset('assets/web/img/browser-mozila.png')}}" alt="{{ trans('messages.mozila') }}"/>
                    <span class="browser-name">{{ trans('messages.mozila') }}</span>
                </span>
                <span class="pos-relative browser-img">
                    <img class="" src="{{asset('assets/web/img/browser-chrome.png')}}" alt="{{ trans('messages.chrome') }}"/>
                        <span class="browser-name">{{ trans('messages.chrome') }}</span>
                </span>
                <span class="pos-relative browser-img">
                    <img class="" src="{{asset('assets/web/img/browser-safari.png')}}" alt="{{ trans('messages.safari') }}"/>
                    <span class="browser-name">{{ trans('messages.safari') }}</span>
                </span>

            </div>
        </div>
        <!-- ck-feature listing -->
        <div class=" ck-features">
            <span class="ck-feature-list-wrapper">
                <ul class="ck-feature-list">
                    <li class="groups active">
                        {{ trans('messages.feeds') }}
                        <div>{{ trans('messages.feeds_text') }}</div>
                        <div class="sml-tagline-show">{{ trans('messages.feeds_text') }}</div>
                    </li>
                    <li class="groups">
                        {{ trans('messages.groups') }}
                        <div>{{ trans('messages.groups_text') }}</div>
                        <div class="sml-tagline-show">{{ trans('messages.groups_text') }}</div>
                    </li>
                    <li>
                        {{ trans('messages.planner') }}
                        <div>{{ trans('messages.planner_text') }}</div>
                        <div class="sml-tagline-show">{{ trans('messages.planner_text') }}</div>
                    </li>
                    <li>
                        {{ trans('messages.docs') }}
                        <div>{{ trans('messages.docs_text') }}</div>
                        <div class="sml-tagline-show">{{ trans('messages.docs_text') }}</div>
                    </li>
                    <li>
                        {{ trans('messages.attendance') }}
                        <div>{{ trans('messages.attendance_text') }}</div>
                        <div class="sml-tagline-show">{{ trans('messages.attendance_text') }}</div>
                    </li>
                </ul>
            </span>
            <span class="ck-feature-image-wrapper">
                <div class="ck-feature-image-container">
                    <div class="image-container-header">
                        <span class="sml-oval"></span>
                        <span class="sml-oval"></span>
                        <span class="sml-oval"></span>
                    </div>
                    <div class="ck-image">
                        <img class="ck-feature" src="{{asset('assets/web/img/ck-feeds-image.png')}}" alt="{{ trans('messages.feeds') }}"/>
                        <img class="ck-feature" src="{{asset('assets/web/img/ck-groups_image.png')}}" alt="{{ trans('messages.groups') }}"/>
                        <img class="ck-feature" src="{{asset('assets/web/img/ck-planner_image.png')}}" alt="{{ trans('messages.planner') }}"/>
                        <img class="ck-feature" src="{{asset('assets/web/img/ck-docs-image.png')}}" alt="{{ trans('messages.docs') }}"/>
                        <img class="ck-feature" src="{{asset('assets/web/img/ck-attendance-image.png')}}" alt="{{ trans('messages.attendance') }}"/>
                    </div>
                </div>
            </span>
        </div>
        <!-- testimonial section -->
        <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12  ck-testimonial">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 testimonial-text">
                <div class="testimonial-text-wrapper pull-right">
                    <img class="quotes-image" src="{{asset('assets/web/img/quotes.png')}}" alt=""/>
                    <div class="quotes-text">{{ trans('messages.testimonial_text') }}</div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 testimonial-video">
                    <iframe width="660" height="350" src="https://www.youtube.com/embed/E5nJJs3UHtI?rel=0" frameborder="0" 	allowfullscreen></iframe>
            </div>
        </div>
        <!-- media cover-->
        <div class="media-coverage Col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h1>{{ trans('messages.media_coverage') }}<span class="underline"></span></h1>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 text-center lg-media" >
                <ul class="top-grid top-grid-styles">
                    <li>
                        <div class="padding-top-10">
                            <a href="http://www.clarionledger.com/story/money/business/2015/07/29/msu-students-score-tech-startup/30832111/" target="_blank"><img class="active" src="{{asset('assets/web/img/clarion-act.png')}}" alt=""/></a>
                        </div>
                    </li>
                    <li class="mid-cell">
                        <a href="https://www.entrepreneur.com/article/249087" target="_blank"><img class="active" src="{{asset('assets/web/img/entrepreneur-act.png')}}" alt=""/></a>
                    </li>
                    <li>
                        <a href="http://www.reflector-online.com/news/article_a6010cc0-559b-11e4-ae46-001a4bcf6878.html" target="_blank"><img class="active" src="{{asset('assets/web/img/reflector-act.png')}}" alt=""/></a>
                    </li>
                </ul>
                <ul class="top-grid">
                    <li>
                         <a href="https://yourstory.com/2015/08/startup-indian-students/"  target="_blank"><img class="active" src="{{asset('assets/web/img/your-story-act.png')}}" alt=""/></a>
                    </li>
                    <li class="mid-cell">
                        <a href="http://www.dnaindia.com/india/report-indian-trio-s-social-media-start-up-for-campuses-bags-100000-funding-in-us-2111427" target="_blank"><img class="active" src="{{asset('assets/web/img/dna.png')}}" alt=""/></a>
                    </li>
                    <li>
                        <a href="http://www.iamwire.com/2015/07/edu-tech-startup-campusknot-raises-100000/120427" target="_blank"><img class="active" src="{{asset('assets/web/img/iamwire-act.png')}}" alt=""/></a>
                    </li>
                </ul>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  sml-media">
                <ul>
                    <li>
                        <div class="padding-10">
                            <a href="http://www.clarionledger.com/story/money/business/2015/07/29/msu-students-score-tech-startup/30832111/" target="_blank"><img class="active" src="{{asset('assets/web/img/clarion-act.png')}}" alt=""/></a>
                        </div>
                    </li>
                     <li class="mid-cell">
                        <a href="https://www.entrepreneur.com/article/249087" target="_blank"><img class="active" src="{{asset('assets/web/img/entrepreneur-act.png')}}" alt=""/></a>
                    </li>
                    <li>
                        <a href="http://www.reflector-online.com/news/article_a6010cc0-559b-11e4-ae46-001a4bcf6878.html" target="_blank"><img class="active" src="{{asset('assets/web/img/reflector-act.png')}}" alt=""/></a>
                    </li>
                     <li>
                        <a href="https://yourstory.com/2015/08/startup-indian-students/" target="_blank"><img class="active" src="{{asset('assets/web/img/your-story-act.png')}}" alt=""/></a>
                    </li>
                    <li class="mid-cell">
                        <a href="http://www.dnaindia.com/india/report-indian-trio-s-social-media-start-up-for-campuses-bags-100000-funding-in-us-2111427" target="_blank"><img class="active" src="{{asset('assets/web/img/dna.png')}}" alt=""/></a>
                    </li>
                    <li>
                        <a href="http://www.iamwire.com/2015/07/edu-tech-startup-campusknot-raises-100000/120427" target="_blank"><img class="active" src="{{asset('assets/web/img/iamwire-act.png')}}" alt=""/></a>
                    </li>                   
                </ul>
            </div>
        </div>
        <div class="contact-form Col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="Col-lg-4 col-md-4 col-sm-8 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-2">
                <h1>{{ trans('messages.contact_us') }}</h1>
                <form id="contact" name="contact" method="post" role="form" action="{{ route('home.contact') }}">
                    {!! csrf_field() !!}
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12  field-padding-right">
                        <input id="form_name" type="text" name="name" class="form-control {{ ($errors->has('name'))? 'error' : '' }}" title="{{ ($errors->has('name'))? $errors->first('name') : '' }}"  data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.first_name') }} *"  required="required" value="{{ old('name') }}">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                        <input id="form_lastname" type="text" name="surname" class="form-control {{ ($errors->has('surname'))? 'error' : '' }}" title="{{ ($errors->has('surname'))? $errors->first('surname') : '' }}"  data-toggle="tooltip" data-placement="top"  placeholder="{{ trans('messages.last_name') }} *" required="required" data-error="Lastname is required." value="{{ old('surname') }}">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <input id="form_email" type="email" name="email" class="form-control {{ ($errors->has('email'))? 'error' : '' }}" title="{{ ($errors->has('email'))? $errors->first('email') : '' }}"  data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.email') }} *" required="required" data-error="Valid email is required." value="{{ old('email') }}">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group contact-cat">
                        <span class="caret"></span>
                        <select class="form-control" name="contact_type">
                            <option value="{{ trans('messages.feedback') }}">{{ trans('messages.feedback') }}</option>
                            <option value="{{ trans('messages.business_licensing') }}">{{ trans('messages.business_licensing') }}</option>
                            <option value="{{ trans('messages.press') }}">{{ trans('messages.press') }}</option>
                            <option value="{{ trans('messages.technical_support') }}">{{ trans('messages.technical_support') }}</option>
                            
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                    
                    <div class="form-group">
                        <input id="form_phone" type="tel" name="phone" class="form-control {{ ($errors->has('phone'))? 'error' : '' }}" title="{{ ($errors->has('phone'))? $errors->first('phone') : '' }}"  data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.phone_number') }}" value="{{ old('phone') }}">
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group">
                        <textarea id="form_message" name="message" class="form-control {{ ($errors->has('message'))? 'error' : '' }}" title="{{ ($errors->has('message'))? $errors->first('message') : '' }}"  data-toggle="tooltip" data-placement="top" placeholder="{{ trans('messages.message_placeholder') }} *" rows="4" required="required" data-error="Please,leave us a message." >{{ old('message') }}</textarea>
                        <div class="help-block with-errors"></div>
                    </div>
                    <button class="login-btn contact-btn" id="load" data-loading-text="<div class='spinner'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.send_message') }}</button>
                    
                    
                </form>
            </div>   
        </div>  
    </div>
</div>
<div id="vdo_tour" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content tour-modal">
            <button type="button" class="close" data-dismiss="modal" onclick="stopVideo();">&times;</button>
            <!-- 1. The <iframe> (and video player) will replace this <div> tag. -->
            <div id="player"></div>

            <script>
              // 2. This code loads the IFrame Player API code asynchronously.
              var tag = document.createElement('script');

              tag.src = "https://www.youtube.com/iframe_api";
              var firstScriptTag = document.getElementsByTagName('script')[0];
              firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

              // 3. This function creates an <iframe> (and YouTube player)
              //    after the API code downloads.
              var player;
              function onYouTubeIframeAPIReady() {
                player = new YT.Player('player', {
                  height: '600',
                  width: '100%',
                  videoId: 'aamiwRvSzCA',
                  playerVars: {rel: 0},
                  events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                  }
                });
              }

              // 4. The API will call this function when the video player is ready.
              function onPlayerReady(event) {
              console.log('onPlayerReady');
               // event.target.playVideo();
              }

              // 5. The API calls this function when the player's state changes.
              //    The function indicates that when playing a video (state=1),
              //    the player should play for six seconds and then stop.
              var done = false;
              function onPlayerStateChange(event) {
                /*if (event.data == YT.PlayerState.PLAYING && !done) {
                 // setTimeout(stopVideo, 6000);
                  done = true;
                  console.log('onPlayerStateChange = PLAYING');
                }else{
                    console.log('onPlayerStateChange = STOP');
                }*/
              }
              function stopVideo() {
                console.log('stopVideo');
                player.stopVideo();
              }


              $('#vdo_tour').on('hidden.bs.modal', function () {
                // do something…
                 console.log('hidden.bs.modal');
                 stopVideo();
              })
            </script>
        </div>
    </div>
</div>

<div id="email_verify" class="modal fade" role="dialog">
    <div class="modal-content email-verify-modal">
        <div class="modal-header">
          {{ trans('messages.confirmation_email') }}
            <button aria-label="Close" data-dismiss="modal" class="close"  data-dismiss="modal" type="button"><span aria-hidden="true">×</span></button>
        </div>
        <div class="modal-body clearfix">
            <img src="{{asset('assets/web/img/verify-email.png')}}">
            <p>{{ trans('messages.confirm_email_text') }}
            <span>
                @if (session('email')) 
                    {{session('email')}}.
                @else
                    useremailid@id.com.
                @endif
            </span>
            {{ trans('messages.confirm_email_click') }}</p>
        </div>
     
    </div>
    
</div>
 
<script type="text/javascript">
    //hide browser compatible div on scroll
    $(window).scroll(function() {
        if ($(this).scrollTop()>0)
        {
            $('.browser-compatible').fadeOut();
        }
        else
         {
          $('.browser-compatible').fadeIn();
         }
    });
    // set modal to center vertically
    var b = $(window ).height();
    var c = (b -600)/2;
    $(".tour-modal").css({ top: c });
    
    @if (session('success_message1'))    
    $( document ).ready(function() {
       $('#email_verify').modal('show'); 
             setTimeout(function(){
                $("#email_verify").modal('hide');
            }, 15*1000);
        
        
    });
    @endif
     
    $( document ).ready(function() {
      $(".ck-feature-list li").click(function(){
	if(!$(this).hasClass("active")){
            $(this).siblings().removeClass("active");
            $(this).addClass("active");
            var mis = ($( this ).index()) + 1; 
            $(".ck-feature").slideUp( "fast" );
            $(" .ck-feature:nth-child(" + mis + ")").slideDown( "fast" );
        }
	});
    });
    
    var options = { 
        beforeSubmit:  showRequest,
        success:       showResponse,
        error:showError,
        dataType: 'json' 
        }; 
        $('.contact-btn').click(function(e){
            $(this).button('loading');
            $('#contact').ajaxForm(options).submit();  		
        });
    function showRequest(formData, jqForm, options) { 
            //$("#validation-errors").hide().empty();
            //$("#output").css('display','none');
            return true; 
    } 
    function showResponse(response, statusText, xhr, $form)  
    {
        $('.contact-btn').button('reset');
        $('#contact')[0].reset();
        $('#flash_msg').text(response.html);
        showTemporaryMessage();
    }
    function showError(xhr, textStatus, errorThrown)  {
        if(xhr.status == 500 ){
            //window.location = siteUrl + '/home';
            console.log('ewrror');
        }
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                    $( "[name='"+index+"']" ).addClass('error');
                    $( "[name='"+index+"']" ).attr('title',value);
                    //$("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
            }
        });
        $("#validation-errors").show();
        $('.contact-btn').button('reset');
    }
</script>
@stop