<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Campusknot</title>
    <link href="http://fonts.googleapis.com/css?family=Dosis:400,500,700" rel="stylesheet" type="text/css">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/bootstrap.min.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('assets/web/css/style.css') }}">
</head>
  <body>
      
      <div class="container-fluid  no-padding app-download">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding banner-img">
            <div class="banner-overlay">
            </div>
              <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 app-download-header">
                        <a href="<?php echo url('/'); ?>" rel="home" >
                            <img src="{{asset('assets/web/img/main_header_logo.png')}}" class="logo">
                        </a>
                    </div>
                </div>
            </div>
            <div class="slide-content">
                <h1>
                    <div>{{ trans('messages.slider_text') }}</div>
                    <span>{{ trans('messages.anytime') }}</span>
                    <span>{{ trans('messages.anywhere') }}</span>
                </h1>
                <a href="#">
                    <img src="{{asset('assets/web/img/get-android-app.png')}}" class="app-icon">
                </a>
                <a href="#">
                    <img src="{{asset('assets/web/img/get-ios-app.png')}}" class="app-icon">
                </a>
          
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 app-download-footer">
                <p> 
                    <a href="{{ url('/about') }}">{{ trans('messages.about') }}</a> |
                    <a href="{{ url('/privacy') }}" target="_blank">{{ trans('messages.privacy_policy') }}</a> |
                    <a href="{{ url('/terms') }}" target="_blank">{{ trans('messages.terms_of_use') }}</a>
                </p>
            </div>
        </div>
      </div>
      
  </body>
</html>