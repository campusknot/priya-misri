<div class="clearfix">
    <div class="panel-body">
        <h3>Feedback</h3>
        <div class="pad-box">
            <textarea style="resize: none;" autocomplete="off" name="feedback_comment" id="feedback_comment" class="form-control" rows="4"></textarea>
            <br>
            <input type="button" onclick="fnSendFeedback();return false;" class="btn btn-primary pull-right margin-top" value="Send" name="send">
        </div>
  </div>
</div>