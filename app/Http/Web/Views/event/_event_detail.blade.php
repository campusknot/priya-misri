<div id="myModalEventDetail_slide" tabindex="-1" role="dialog" aria-labelledby="myModalEventDetail" class="max-height event-detail-page">
    <div class="" role="document">
        <div class="modal-content">
            <h3 class="modal-title" id="myModalLabel2">{{ trans('messages.event_detail') }}
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="hideSlider();">
                   <span aria-hidden="true">&times;</span>
               </button>
            </h3>
            
            <div class="">
            @if(count($oEventDetail))
                <div class="modal-event-detail">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                        <div class=" event-tilte" id="model_event_title">{{$oEventDetail->event_title}}</div>
                        
                    <!--</div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 m-10 padding-0"> -->
                        <?php 
                        $dEventStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $oEventDetail->start_date);
                        if($oEventDetail->event_from != config('constants.EVENTTYPEGOOGLE'))
                            $dEventStartDate->setTimezone(Auth::user()->timezone);
                        $dEventEndDate='';
                        //echo $oEventDetail->end_date."sfd";exit;
                        if($oEventDetail->end_date != null)
                        {
                            $dEventEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $oEventDetail->end_date);
                            if($oEventDetail->event_from != config('constants.EVENTTYPEGOOGLE'))
                                $dEventEndDate->setTimezone(Auth::user()->timezone);
                            $dCheckDate = $dEventEndDate;
                        }
                        else
                            $dCheckDate = $dEventStartDate;
                        $dToday = Carbon::now(Auth::user()->timezone);
                        $sToday = $dToday->toDateTimeString();
                        ?>
                        
                        <span  id="model_event_date"><img src="{{asset('assets/web/img/event-date.png')}}" alt="">
                            {{$dEventStartDate->format('l')}} {{$dEventStartDate->format('M j')}}, {{$dEventStartDate->format('g:i A')}} 
                             @if($dEventEndDate != '')
                                
                                - {{ ($dEventStartDate->toDateString() == $dEventEndDate->toDateString()) ? $dEventEndDate->format('g:i A') : $dEventEndDate->format('l').' '.$dEventEndDate->format('M j').', '.$dEventEndDate->format('g:i A')}}
                             @endif
                        </span>
                    
                        
                        <span>
                            
                            @if($oEventDetail->latitude != '0.00000000' &&  $oEventDetail->longitude != '0.00000000' )
                                <img src="{{asset('assets/web/img/location.png')}}" alt="">
                                <a class="event-place"  target="_blank" href="{{config('constants.GMAPURL')}}?q={{$oEventDetail->address}}">{{!empty($oEventDetail->room_number) ? $oEventDetail->room_number.', ' : ''}}{{$oEventDetail->address}}</a> <!-- place -->
                            @elseif(trim($oEventDetail->address) != '')
                                <img src="{{asset('assets/web/img/location.png')}}" alt="">
                                <p>{{!empty($oEventDetail->room_number) ? $oEventDetail->room_number.', ' : ''}}{{$oEventDetail->address}}</p>
                            @endif
                        </span>
                        <span>
                            <img src="{{asset('assets/web/img/faculty-icon.png')}}" alt=""/>
                            {{ $oEventDetail->first_name.' '.$oEventDetail->last_name }}
                        </span>
                         @if($oEventDetail->group_names )
                            <span  id="model_event_group"><img src="{{asset('assets/web/img/event-group.png')}}" alt=""><p>{{$oEventDetail->group_names}}</p></span>
                         @endif
                        
                    </div>
                    
                    @if($dCheckDate > Carbon::now(Auth::user()->timezone) && $oEventDetail->event_from == config('constants.EVENTTYPECK'))
                            
                    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                        <div class="status">{{ trans('messages.i_am') }}
                            @if($oEventDetail->id_event_request!='')
                            <span class="caret"></span>
                            <select placeholder="Choose your status" id="id_status_{{ $oEventDetail->id_event_request }}" onchange="ChangeEventStatus({{ $oEventDetail->id_event_request }});">
                                <!--<option value="" <?php if($oEventDetail->user_response=='') echo "selected='selected'"; ?>  disabled="disabled">_____</option>-->
                                <option value="G" <?php if($oEventDetail->user_response=='G') echo "selected='selected'"; ?> >{{ trans('messages.going') }}</option>
                                <option value="M" <?php if($oEventDetail->user_response=='M') echo "selected='selected'"; ?>>{{ trans('messages.may_be') }}</option>
                                <option value="NG" <?php if($oEventDetail->user_response=='NG') echo "selected='selected'"; ?> >{{ trans('messages.not_going') }}</option>
                            </select>
                            @endif
                        </div>
                    </div>
                    @endif
                        
                    <div class="col-lg-8 col-md-8 col-sm-7 col-xs-8">
                        
                    </div>
                </div>
                <div class="">
                    <div class="tabbable-panel">
                        <div class="tabbable-line">
                            <ul class="nav nav-tabs ">
                                <li class="active">
                                    <a href="#event_description_tab" data-toggle="tab">{{ trans('messages.description') }} </a>
                                </li>
                                 @if($oEventDetail->event_from == config('constants.EVENTTYPECK'))
                                    <li>
                                        <a href="#event_going_users_tab" data-toggle="tab" onclick="getEventMembers('{{$oEventDetail->id_event}}', '{{ config('constants.EVENTSTATUSGOING')}}', 'event_going_users_tab',1);">{{ trans('messages.going') }} </a>
                                    </li>
                                    <li>
                                        <a href="#event_maybe_users_tab" data-toggle="tab" onclick="getEventMembers('{{$oEventDetail->id_event}}', '{{ config('constants.EVENTSTATUSMAYBE')}}', 'event_maybe_users_tab',1);">{{ trans('messages.may_be') }} </a>
                                    </li>
                                @endif
                                @if($oEventDetail->is_admin && $oEventDetail->event_from != config('constants.EVENTTYPEGOOGLE'))
                                <li class="event-options edit-event dropdown" id="btn_admin_fun_p">
                                    <a type="button" id="btn_admin_fun" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <img src="{{asset('assets/web/img/navigation.png')}}" alt="">
                                    </a>
                                    <ul class=" dropdown-menu group-options">
                                        <li>
                                            <span id="event_invite_member" >{{ trans('messages.invite_members') }}</span></li>
                                       
                                        <li>
                                            <span  id="event_edit"  data-toggle="modal">{{ trans('messages.edit_event_title') }}</span>
                                        </li>
                                        <li>
                                            <span  id="event_cancel" >{{ trans('messages.cancel_event_title') }}</span>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                            </ul>
                            <input type="hidden" id="event_status" >
                            <div class="tab-content clearfix">
                                <div id="event_description_tab" class="tab-pane event-desc active">
                                    <p  id="event_desc">{{$oEventDetail->event_description}}</p>
                                </div>
                                <div id="event_feeds_tab" class="tab-pane"></div>
                                <!--  Start:tab 3 User Going -->
                                <div id="event_going_users_tab" class="tab-pane"></div>
                                <!--  End:tab 3 User Going -->
                                <!--  Start:tab 4 User Maybe -->
                                <div id="event_maybe_users_tab" class="tab-pane"></div>
                                <!--  End:tab 4 User Maybe -->
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="modal-event-detail">
                    {{ trans('messages.event_deleted') }}
                </div>
            @endif
            </div>
        </div>
    </div><!-- modal-content -->
</div><!-- modal-dialog -->
@if($oEventDetail->is_admin)
<script type="text/javascript">
    eventId = <?php echo $oEventDetail->id_event ?>;
    cancelEventMessage =  "<?php echo trans('messages.cancel_event_confirmation') ?>";
    document.getElementById('event_invite_member').onclick = function() {
        showEventInviteLightBox(eventId);
      };
       document.getElementById('event_edit').onclick = function() {
        editEvent(eventId);
      };
       document.getElementById('event_cancel').onclick = function() {
       deleteEvent(cancelEventMessage,eventId);
      };
</script>
@endif