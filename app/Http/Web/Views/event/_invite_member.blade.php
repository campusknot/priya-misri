<div class="modal-content invite-to-event-popup">
    <div class="modal-header">
        <button type="button" onclick="closeLightBox();" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 id="create_event">{{ trans('messages.invite_to_event') }}</h4>
    </div>
    <div class="modal-body"><!-- Start: Model Body -->
        <div class="event-name clearfix">{{trans('messages.invite_event_text')}}{{$oEvent->event_title}}</div>
        <form id="invite_users_form" class="form-horizontal clearfix" method="post" onsubmit="submitAjaxForm('invite_users_form', '{{ route('event.invite-member') }}', $('#invited_member_button'));">
            {!! csrf_field() !!}
            <div class="form-group clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 place-holder">
                    <select id="eventContact" class="js-data-example-ajax" data-placeholder="{{ trans('messages.invite_groups_or_users') }}" multiple="multiple" name="invite_member_list[]" ></select>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 place-holder">
                    @if ($errors->has('invite_member_list'))
                    <span class="error_message">{{ $errors->first('invite_member_list') }}</span>
                @endif
                </div>
                
            </div>
            <input id="id_event" name="id_event" type="hidden" value="{{$oEvent->id_event}}" />
            <div id="selected_course" class="form-group clearfix"></div>
            <div class=" submit-options">
                <button type="button" class="btn btn-default pull-right" onclick="closeLightBox();">{{ trans('messages.cancel') }}</button>
                <button id="invited_member_button" type="button" class="btn btn-primary pull-right" onclick="submitAjaxForm('invite_users_form', '{{ route('event.invite-member') }}', this);">{{ trans('messages.send') }}</button>
            </div>
        </form>  
    </div>
</div>
<script type="text/javascript">

    $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
    });
    
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
        
        $("#eventContact").select2({
            ajax: {
                type: 'POST',
                url: "/event/invite/search",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        search_str: params.term, // search term
                        page: params.page,
                        action: "user_search"
                    };
                },
                processResults: function (data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;

                    var processedResult = [];

                    var groups = $.map(data.data.group_list, function(item){
                        return { id: "GM_"+item.id_group, text: item.group_name };
                    });

                    var contacts = $.map(data.data.user_connected_list, function(item){
                        return { id: "CU_"+item.id_user, text: item.first_name+' '+item.last_name };
                    });
                    
                    var others = $.map(data.data.user_other_list, function(item){
                        return { id: "U_"+item.id_user, text: item.first_name+' '+item.last_name };
                    });
                    
                    if(groups && groups.length>0){
                        var group = { text: 'Group', children: groups };
                        processedResult.push(group);
                    }

                    if(contacts && contacts.length> 0){
                        var contact = { text: 'Contacts', children: contacts };
                        processedResult.push(contact);
                    }
                    
                    if(others && others.length> 0){
                        var other = { text: 'Others', children: others };
                        processedResult.push(other);
                    }
                    
                    return {
                        results: processedResult,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
        $('#eventContact').bind('keypress');
    });
    
    function formatRepo (repo) {
        $('#invitePlaceHolder').html('');
        if (repo.loading) return repo.text;

        var markup ="<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>" +
                            "<div class='select2-result-repository__title'>" + repo.text + "</div>" +
                        "</div>"+
                    "</div>";

        return markup;
    }

    function formatRepoSelection (repo)
    {
        return repo.text || '';
    }
    
</script>