@extends('layouts.web.main_layout')
@include('common.errors')

@section('title', 'Campusknot-Event List')

@section('content')
<!-- Filename::event.event_list -->
<div class="clearfix">
    <div class="">
        <form id="group_search" name="group_search" onsubmit="getEventFilterBySearchStr('search_event');return false">
            <span class="input-group group-search planner-search">
                <input id="search_event" type="text" class="form-control" name="search_event" placeholder="{{ trans('messages.search_event') }}" value="" required="required">
                <button type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>  
            </span>
            <span class="create-event pull-right clearfix">
                <a href="javascript:void(0);" class="mightOverflow" onclick="showAddEventLightBox();" title="" nowrap>{{ trans('messages.create_event') }}</a>
            </span>
        </form>
    </div>        
</div>

<div class="tabbable pos-relative">
    <ul class=" nav nav-tabs group-tabs group-tabs-dropdown" >
        <li class="active" id="all_events" onclick="getEventFilterByGroup('all_events', '');" data-toggle="tooltip" data-placement="top" title="">
            <a id="allEventDiv" class="mightOverflow" href="javascript:void(0);" nowrap>{{ trans('messages.all') }}</a>
        </li>
        <?php
            $nCount = 1;
            foreach($oGroupList as $oGroup)
            {
        ?>
                <li id="groupId_<?php echo $nCount; ?>" class="" data-toggle="tooltip" data-placement="top" title="{{$oGroup->group_name}}" nowrap>
                    <a href="javascript:void(0);" class="mightOverflow" onclick="getEventFilterByGroup('groupId_'+<?php echo $nCount; ?>, '{{ $oGroup->id_group }}');" nowrap>
                        {{$oGroup->group_name}}
                        @if($oGroup->section != '')
                            {{ trans('messages.section') }}- {{$oGroup->section}} ({{$oGroup->semester}})
                        @endif
                    </a>
                </li>
        <?php
                $nCount++ ;
            }
        ?>
                
    </ul>

    <div id="event_list" class="event-list">
        @include('WebView::event._event_list')
    </div>
    <div class="more-data-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <div class="switch-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
    <form id="event_filter_form" name="event_filter_form" method="GET" enctype="multipart/form-data">
        <input id="event_filter_id_group" name="id_group" type="hidden" value="{{ old('id_group') }}" />
        <input id="event_filter_start_date" name="start_date" type="hidden" value="{{ old('start_date') }}" />
        <input id="event_filter_search_str" name="search_str" type="hidden" value="{{ old('search_str') }}" />
    </form>
</div>


<script type="text/javascript">
var setCron=0;
var nCurrentPage = <?php echo $oUserEventList->currentPage(); ?>;
var nLastPage = <?php echo $oUserEventList->lastPage(); ?>;
 $(window).scroll(function (event) {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            if(nCurrentPage < nLastPage) {
                var nIdGroup = $('#event_filter_id_group').val();
                var sStartDate = $('#event_filter_start_date').val();
                var sSearchStr = $('#search_event').val();

                if(nLoadNewDataStatus == 0)
                {
                     nCurrentPage +=1;
                     //disableScroll();
                     loadNewData('event_list', '<?php echo route('event.filter-event-listing'); ?>', nCurrentPage, '&id_group='+nIdGroup+'&start_date='+sStartDate+'&search_str='+sSearchStr);
                }else{
                    console.log('else nLoadNewDataStatus = '+nLoadNewDataStatus);
                }

            }
            else if(setCron==0)
            {
                setCron=1;
                callGoogleCron('/event/event-google-cron/',nLastPage);
            }
        }
    });
$(document).ready(function(){
    var sPageURL = window.location;
    var nIdEvent ='';
    if(/nIdEvent=(\d+)/.exec(sPageURL) !== null)
    {
        var nIdEvent = /nIdEvent=(\d+)/.exec(sPageURL)[1];
    }
    if(nIdEvent !='')
    {
        getSingleEvent(nIdEvent);
        var sBrowserUrl = "<?php echo route('event.event-listing'); ?>";
        window.history.pushState({path:sBrowserUrl},'',sBrowserUrl);
    }
});
</script>


@endsection

@section('right_sidebar')
     @include('WebView::event._event_sidebar')
@endsection