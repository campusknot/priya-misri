<div class="">
   <div id="calender" class="event-calender"></div>
</div>
<div class="google_events">
    <div>
        <!--<img src="{{asset('assets/web/img/google-calender.png')}}"> -->
        <div class="calender-header">
            @if(!Cookie::get('google_planner'))
             <span class="new-feature">{{ trans('messages.new') }}</span>
             <?php 
             $aCustomCookies['atten'] = TRUE;
             Cookie::queue(Cookie::forever('google_planner', json_encode($aCustomCookies)));
             ?>
            @endif
             {{ trans('messages.sync_calendar') }}</div>
        <div class="calender">
            <div class="accordion-toggle google-cal-title" data-toggle="" data-parent="" href="#collapseGoogle">  
                <span>
                    {{ trans('messages.google_caledar') }}
                       <span class="pull-right">
                       @if(isset($gAuthUrl) && $gAuthUrl != '')
                            <a class="connect" href="{{ $gAuthUrl }}">{{ trans('messages.google_caledar_connect') }}</a>
                       @else
                            <a class="disconnect" onclick="GoogleDisconnect('{{ trans('messages.google_caledar_disconnect_msg') }}');">{{ trans('messages.google_caledar_disconnect') }}</a>
                       @endif
                   </span>
                </span>
                
            </div> 
            <div id="collapseGoogle" class="accordion-body collapse in">  
               @foreach($oGoogleCalendarList as $sCalendarName)
                   @if($sCalendarName->calendar_name != '')
                   <div class="google-user-cal">
                       <input type="checkbox" value="{!! str_replace(array(' ', '.','@'), '_', $sCalendarName->calendar_name) !!}" checked>
                       <label>{{ $sCalendarName->calendar_name }}</label>
                   </div>
                   @endif
               @endforeach
           </div>  
        </div>
    </div>
    
    
</div>

<script>
$( "#calender" ).datepicker( {
   dateFormat: 'yy-mm-dd',
   onSelect: function()
   {
        $('#event_filter_start_date').val($(this).val());
        filterEventList();
   }

});

/* change in calendar list event show hide */
$("input[type='checkbox']").change(function() {
    var calendar_name=$(this).val();

    if($(this).is(':checked')){
        $('.'+calendar_name).addClass('show');
        $('.'+calendar_name).removeClass('hidden');
        
    }
    else{
        $('.'+calendar_name).addClass('hidden');
        $('.'+calendar_name).removeClass('show');
        
    }
    $( ".event-detail" ).each(function( ) {
        if($(this).children().hasClass("show")){$(this).removeClass('hidden');}
        else{ $(this).addClass('hidden')};
    });
    

});
/* change in calendar list event show hide */
</script>