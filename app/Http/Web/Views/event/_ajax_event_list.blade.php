@if(count($oUserEventList[0]->group_event))
    @if( count($oUserEventList[0]) )
        <div class="event-detail">
        <?php $dPrevDate = '';?>
        @foreach($oUserEventList[0]->group_event as $oEventDetail)
            <?php
                $dEventStartDate = new Carbon($oEventDetail->start_date);
                $dEventStartDate->setTimezone(Auth::user()->timezone);
                
                $dRepeatedate = $dEventStartDate->format('M j');

                if(  $dRepeatedate != $dPrevDate )  {
                $detail = 1;
            ?>
        </div>
        <div class='event-detail'>
            <ul class="" >
                <span class="event-day">{{$dEventStartDate->format('M j')}}</span>
                <?php }  else {
                $detail = 0;
                ?>
                <ul> <?php } ?>

            {{--<ul class="event-detail" onclick="getSingleEvent({{$oEventDetail->id_event}},'GET');">--}}
                {{--data-toggle="modal" data-target="#myModalEventDetail"--}}
                <li class="center">
                    <?php $dEventStartDate = new Carbon($oEventDetail->start_date) ?>
                    <span class="event-day">{{$dEventStartDate->format('M j')}}</span>
                    <span class="event-date">{{$dEventStartDate->format('l')}}</span>
                    <span class="event-time">{{$dEventStartDate->format('H:m')}}</span>
                </li>
                <li>
                    @if($oEventDetail->event)
                    <div class="event-name">{{$oEventDetail->event->event_title}}</div>
                    <a class="event-place"  target="_blank" href="{{config('constants.GMAPURL')}}?q={{$oEventDetail->event->latitude}},{{$oEventDetail->event->longitude}}">{{$oEventDetail->event->address}}:{{$oEventDetail->event->room_number}}</a> <!-- place -->
                    <div class="event-group"> {{$oEventDetail->grouo_name}}</div>
                    @endif
                </li>
                <?php if($detail == 1) { ?> <?php }?></ul>
        <?php $dPrevDate = $dEventStartDate->format('M j'); ?>
        @endforeach
    @endif
    @else
        <div class="no-data">
            <img src="{{asset('assets/web/img/no-events.png')}}">
            {{ trans('messages.no__group_events') }}
        </div>
@endif
