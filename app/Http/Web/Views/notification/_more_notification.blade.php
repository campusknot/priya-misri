<!-- Group request notifications -->
    @if(count($oGroupInviteNotifications) > 0)
        @if($oGroupInviteNotifications->add_header)
        <div class="group-request-notification">
            <h4>{{ trans('messages.group_requests') }}</h4>
        </div>
        @endif
        @foreach($oGroupInviteNotifications as $oGroupRequest)
        <div class="notification-detail clearfix ">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="icon-image img-35">
                    <a href="{{route('user.profile',[$oGroupRequest->id_user])}}">
                        {!! setProfileImage("50",$oGroupRequest->user_profile_image,$oGroupRequest->id_user,$oGroupRequest->first_name,$oGroupRequest->last_name) !!}
                    </a>
                </div>
                <div class="notification-text">
                       <span class="notification-about">
                    <?php
                        $sUserLink = '<a href="'.route('user.profile',[$oGroupRequest->id_user]).'" >'.
                                                    $oGroupRequest->first_name.' '.$oGroupRequest->last_name.
                                                '</a>';
                        if($oGroupRequest->request_type==config('constants.GROUPREQUESTTYPEINVITE'))
                        {
                            $oGroup = '<a href="'.route('group.group-feeds',[$oGroupRequest->id_group]).'" >'. htmlspecialchars($oGroupRequest->group_name) .'</a>';
                            echo trans('messages.request_invite_user',['user'=>$sUserLink, 'group'=>$oGroup]);
                        }
                        else
                        {
                            $oGroup = '<a href="'.route('group.group-member',[$oGroupRequest->id_group]).'" >'. htmlspecialchars($oGroupRequest->group_name) .'</a>';
                            echo trans('messages.request_join_user',['user'=>$sUserLink, 'group'=>$oGroup]);
                        }
                    ?>
                    <span class="post-time">{{ secondsToTime($oGroupRequest->created_at)}}</span>
                </span>
                    
                </div>
             
                <span class="notification-action hover-underline no-padding" id="group_{{ $oGroupRequest->id_group_request}}">
                    @if($oGroupRequest->request_type == config('constants.GROUPREQUESTTYPEINVITE'))
                        <a value="Accept"  onclick="UpdateNotificationStatus('A', '{{$oGroupRequest->id_group_request}}')" class=" accept ">{{ trans('messages.accept') }}</a>
                        <a value="Decline"  onclick="UpdateNotificationStatus('R', '{{$oGroupRequest->id_group_request}}')" class=" decline ">{{ trans('messages.decline') }} </a>
                    @else
                        <a value="Accept"  onclick="UpdateNotificationStatus('A', '{{$oGroupRequest->id_group_request}}')" class=" ">{{ trans('messages.approve') }}</a>
                        <a value="Decline"  onclick="UpdateNotificationStatus('R', '{{$oGroupRequest->id_group_request}}')" class="decline ">{{ trans('messages.dis_approve') }}</a>
                    @endif      
                </span>
            </div>
<!--            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 notification-action no-padding ">
              @if($oGroupRequest->request_type == config('constants.GROUPREQUESTTYPEINVITE'))
                    <a value="Accept"  onclick="UpdateNotificationStatus('A', '{{$oGroupRequest->id_group_request}}')" class=" accept ">{{ trans('messages.accept') }}</a>
                    <a value="Decline"  onclick="UpdateNotificationStatus('R', '{{$oGroupRequest->id_group_request}}')" class=" decline ">{{ trans('messages.decline') }} </a>
                @else
                    <a value="Accept"  onclick="UpdateNotificationStatus('A', '{{$oGroupRequest->id_group_request}}')" class=" ">{{ trans('messages.approve') }}</a>
                    <a value="Decline"  onclick="UpdateNotificationStatus('R', '{{$oGroupRequest->id_group_request}}')" class="decline ">{{ trans('messages.dis_approve') }}</a>
                @endif      
            </div>-->
        </div>
        @endforeach
    @endif
    
    <!-- Event request notifications -->
    @if(count($oEventInviteNotifications) > 0)
        @if($oEventInviteNotifications->add_header)
        <div class="event-request-notification">
            <h4>{{ trans('messages.event_requests') }}</h4>
        </div>
        @endif
        @foreach($oEventInviteNotifications as $oEventRequest )
            <div class="event-notification notification-detail clearfix ">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding">
                    <div class="icon-image img-35">
                        <a href="{{route('user.profile',[$oEventRequest->id_user])}}">
                            {!! setProfileImage("50",$oEventRequest->user_profile_image,$oEventRequest->id_user,$oEventRequest->first_name,$oEventRequest->last_name) !!}
                        </a>
                    </div>
                    <div class=" notification-text">
                        <span class="notification-person">
                            <a href="{{route('user.profile',[$oEventRequest->id_user])}}" >{{ $oEventRequest->first_name }} {{ $oEventRequest->last_name }}</a>
                        </span>
                        <span class="notification-about">has invited you to join  </span>
                        <span class="notification-area"><a href="javascript:void(0);" onclick="redirectEventPage({{$oEventRequest->id_event}});" >{{$oEventRequest->event_title}}</a></span>
                        <span class="notification-time">{{ secondsToTime($oEventRequest->created_at)}}</span>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 notification-action hover-underline no-padding ">
                    <div class="select-box">
                        <select name="status_{{$oEventRequest->id_event_request}}" id="id_status_{{$oEventRequest->id_event_request}}"  onchange="ChangeEventStatus({{$oEventRequest->id_event_request}})">
                            <option value="">Are You?</option>
                            <option value="G" > {{ trans('messages.going') }}</option>
                            <option value="M" >{{ trans('messages.may_be') }}</option>
                            <option value="NG" > {{ trans('messages.not_going') }}</option>
                        </select>
                        <span class="caret"></span>
                   </div>
               </div>
            </div>     
        @endforeach
    @endif
    
    <!-- Informative notifications -->
    @if(count($oInformativeNotifications) > 0)
        @if($oInformativeNotifications->add_header)
        <div class="informative-notification">
                <h4>{{ trans('messages.others') }}</h4>
        </div>
        @endif
        <div class="others-notification">
        @foreach($oInformativeNotifications as $oInfoNotification)
            <?php 
            $aIdEntities = explode('_', $oInfoNotification->id_entites);
            $aEntityPattern = explode('_', $oInfoNotification->entity_type_pattern);
        ?>
            <div class="notification-detail  clearfix ">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                    <div class="icon-image img-35">
                        <a href="{{route('user.profile',[$oEntityDetails[$oInfoNotification->id_notification]['user']->id_user])}}">
                            {!! setProfileImage("50",$oEntityDetails[$oInfoNotification->id_notification]['user']->file_name,$oEntityDetails[$oInfoNotification->id_notification]['user']->id_user,$oEntityDetails[$oInfoNotification->id_notification]['user']->first_name,$oEntityDetails[$oInfoNotification->id_notification]['user']->last_name) !!}
                        </a>
                    </div>
                    <div class=" notification-text">
                        <span class="notification-person">
                            <?php
                                switch($oInfoNotification->notification_type)
                                {
                                    case config('constants.NOTIFICATIONUSERFOLLOW'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        echo trans('messages.'.config('constants.NOTIFICATIONUSERFOLLOW'), ['user' => $sUserLink]);
                                        break;
                                    case config('constants.NOTIFICATIONEDITEVENT'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        $oEvent = $oEntityDetails[$oInfoNotification->id_notification]['event'];
                                        $sEventLink = '<a href="javascript:void(0);" onclick="redirectEventPage('.$oEvent->id_event.')" >'.
                                                            $oEvent->event_title.
                                                        '</a>';
                                        echo trans('messages.'.config('constants.NOTIFICATIONEDITEVENT'), ['user' => $sUserLink,'event' => $sEventLink]);
                                        break;
                                    case config('constants.NOTIFICATIONCOMMENTONEVENT'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        $oEvent = $oEntityDetails[$oInfoNotification->id_notification]['event'];
                                        echo trans('messages.'.config('constants.NOTIFICATIONCOMMENTONEVENT'),['user'=>$sUserLink, 'event'=>applySubStr($oEvent->event_title, 50)]);
                                        break;
                                    case config('constants.NOTIFICATIONCANCELEVENT'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        $oEvent = $oEntityDetails[$oInfoNotification->id_notification]['event'];
                                        echo trans('messages.'.config('constants.NOTIFICATIONCANCELEVENT'),['user'=>$sUserLink,'event'=>applySubStr($oEvent->event_title, 50)]);
                                        break;
                                    case config('constants.NOTIFICATIONNEWGROUPPOST'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $oPost = $oEntityDetails[$oInfoNotification->id_notification]['post'];
                                        $oGroup = $oEntityDetails[$oInfoNotification->id_notification]['group'];
                                        $sPostLink = '<a href="'.route('post.post-detail',[$oPost->id_post]).'" >'.
                                                        trans('messages.'.config('constants.NOTIFICATIONNEWGROUPPOST'),['user'=>$oUser->first_name.' '.$oUser->last_name, 'group'=>htmlspecialchars($oGroup->group_name),'group_id'=>$oGroup->id_group ]);
                                                    '</a>';
                                        if($oPost->post_type == config('constants.POSTTYPEPOLL'))
                                        {
                                            $sPostLink = '<a href="'.route('post.post-detail',[$oPost->id_post]).'" >'.
                                                        trans('messages.new_group_poll',['user'=>$oUser->first_name.' '.$oUser->last_name, 'group'=>htmlspecialchars($oGroup->group_name),'group_id'=>$oGroup->id_group ]);
                                                    '</a>';
                                        }
                                        
                                        echo $sPostLink;
                                        break;
                                    case config('constants.NOTIFICATIONCOMMENTONPOST'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $oPost = $oEntityDetails[$oInfoNotification->id_notification]['post'];
                                        $sPostLink = '<a href="'.route('post.post-detail',[$oPost->id_post]).'" >'.
                                                        trans('messages.'.config('constants.NOTIFICATIONCOMMENTONPOST'), ['user' => $oUser->first_name.' '.$oUser->last_name, 'post' => applySubStr(!empty(trim($oPost->post_text)) ? $oPost->post_text : 'your', 50)]).
                                                    '</a>';
                                        
                                        
                                        echo $sPostLink;
                                       break;
                                    case config('constants.NOTIFICATIONLIKEONPOST'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $oPost = $oEntityDetails[$oInfoNotification->id_notification]['post'];
                                        $sPostLink = '<a href="'.route('post.post-detail',[$oPost->id_post]).'" >'.
                                                        trans('messages.'.config('constants.NOTIFICATIONLIKEONPOST'), ['user' => $oUser->first_name.' '.$oUser->last_name, 'post' => applySubStr(!empty(trim($oPost->post_text)) ? $oPost->post_text : 'your', 50)]).
                                                    '</a>';
                                        
                                        
                                        echo $sPostLink;
                                        break;
                                    case config('constants.NOTIFICATIONNEWADMIN'):
                                        $oGroup=$oEntityDetails[$oInfoNotification->id_notification]['group'];
                                        $oGroupLink = '<a href="'.route('group.group-member',[$oGroup->id_group]).'" >'. htmlspecialchars($oGroup->group_name) .'</a>';
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        echo trans('messages.new_group_admin',['group'=>$oGroupLink,'user'=>$sUserLink]);
                                        break;
                                    
                                    case config('constants.NOTIFICATIONUSERATTENDANCECHANGE'):
                                        $oCourse = $oEntityDetails[$oInfoNotification->id_notification]['course'];
                                        $oAttendanceLog = $oEntityDetails[$oInfoNotification->id_notification]['attendance_log'];
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        echo trans('messages.'.config('constants.NOTIFICATIONUSERATTENDANCECHANGE'),['user'=>$sUserLink,'from'=>$oAttendanceLog->change_from,'to'=>$oAttendanceLog->change_to,'course'=>$oCourse->course_name]);
                                        break;
                                    case config('constants.NOTIFICATIONGROUPATTENDANCECHANGE'):
                                        $oGroup = $oEntityDetails[$oInfoNotification->id_notification]['group'];
                                        $oAttendanceLog = $oEntityDetails[$oInfoNotification->id_notification]['group_attendance_log'];
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        echo trans('messages.'.config('constants.NOTIFICATIONUSERATTENDANCECHANGE'),['user'=>$sUserLink,'from'=>$oAttendanceLog->change_from,'to'=>$oAttendanceLog->change_to,'course'=>htmlspecialchars($oGroup->group_name)]);
                                        break;
                                    
                                    case config('constants.NOTIFICATIONGROUPDOCUMENT'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $oGroup = $oEntityDetails[$oInfoNotification->id_notification]['group'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        $oDocument = $oEntityDetails[$oInfoNotification->id_notification]['document'];
                                        if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                                        {
                                            $sDocumentLink = '<a href="'.route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]).'" target="_blank">'.
                                                                $oDocument->document_name.
                                                            '</a>';
                                            echo trans('messages.'.config('constants.NOTIFICATIONGROUPDOCUMENT'), ['user' => $sUserLink, 'document' => $sDocumentLink, 'group' => htmlspecialchars($oGroup->group_name)]);
                                        }
                                        else
                                        {
                                            $sDocumentLink = '<a href="'.route('documents.group-document',['nIdGroup'=>$oGroup->id_group,'nIdDocument'=>$oDocument->id_document]).'" >'.
                                                                $oDocument->document_name.
                                                            '</a>';
                                            echo trans('messages.'.config('constants.NOTIFICATIONGROUPDOCUMENT').'_folder', ['user' => $sUserLink, 'document' => $sDocumentLink, 'group' => htmlspecialchars($oGroup->group_name)]);
                                        }
                                        break;
                                    case config('constants.NOTIFICATIONUSERDOCUMENT'):
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        $sUserLink = '<a href="'.route('user.profile',[$oUser->id_user]).'" >'.
                                                        $oUser->first_name.' '.$oUser->last_name.
                                                    '</a>';
                                        $oDocument = $oEntityDetails[$oInfoNotification->id_notification]['document'];
                                        if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                                        {
                                            //$oDocumentparent = $oEntityDetails[$oInfoNotification->id_notification]['document_parent'];
                                            $sDocumentLink = '<a href="'.route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]).'" target="_blank" >'.
                                                                $oDocument->document_name.
                                                            '</a>';
                                            echo trans('messages.'.config('constants.NOTIFICATIONUSERDOCUMENT'), ['user' => $sUserLink, 'document' => $sDocumentLink]);
                                        }
                                        else
                                        {
                                            $sDocumentLink = '<a href="'.route('documents.shared-document',[$oDocument->id_document]).'" >'.
                                                                $oDocument->document_name.
                                                            '</a>';
                                            echo trans('messages.'.config('constants.NOTIFICATIONUSERDOCUMENT').'_folder', ['user' => $sUserLink, 'document' => $sDocumentLink]);
                                        }
                                        break;
                                    case config('constants.NOTIFICATIONGROUPQUIZ'):
                                        $oGroup = $oEntityDetails[$oInfoNotification->id_notification]['group'];
                                        $oQuiz = $oEntityDetails[$oInfoNotification->id_notification]['quiz'];
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        echo '<a href="'.route('quiz.quiz-student-list',[$oGroup->id_group]).'" >'.
                                                        trans('messages.'.config('constants.NOTIFICATIONGROUPQUIZ'),['name' => $oUser->first_name.' '.$oUser->last_name , 'quiz'=> $oQuiz->quiz_title,'group' => htmlspecialchars($oGroup->group_name)]).
                                                    '</a>';
                                    break;
                                    case config('constants.NOTIFICATIONGROUPQUIZDELETE'):
                                        $oGroup = $oEntityDetails[$oInfoNotification->id_notification]['group'];
                                        $oQuiz = $oEntityDetails[$oInfoNotification->id_notification]['quiz'];
                                        $oUser = $oEntityDetails[$oInfoNotification->id_notification]['user'];
                                        echo '<a href="'.route('quiz.quiz-student-list',[$oGroup->id_group]).'" >'.
                                                        trans('messages.'.config('constants.NOTIFICATIONGROUPQUIZDELETE'),['name' => $oUser->first_name.' '.$oUser->last_name , 'quiz'=> $oQuiz->quiz_title,'group' => htmlspecialchars($oGroup->group_name)]).
                                                    '</a>';
                                    break;
                                    default: break;
                                }
                            ?>  
                            <span class="post-time">{{ secondsToTime($oInfoNotification->created_at)}}</span>
                        </span>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
    @endif