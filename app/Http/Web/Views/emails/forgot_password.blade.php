<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <title>Newsletter</title>
    <style>
    img {max-width:100%; vertical-align: middle;}
    @media only screen and (max-width: 640px){
    .table-email td { display: inline-block !important;
        padding: 7px 10px !important;
        text-align: left;
        width: 100%;}
    .table-email td p {word-wrap:break-word; font-size:11px !important; }
    .table-email td.menu a {padding:12px 10px !important;}
    .heading {overflow:hidden;}
    .table-email table{ width:100%;}
    p{ padding-left:0 !important; }
    .table-email{ max-width:100% !important;}
    .menu > a{ font-size:12px !important;}
    .heading > img { width: 100%;}
    .menu > a:hover{ background:#3cc4b6; color:#fff !important;}
    .instruction p{ margin:0 !important;}
    }
    </style>
    </head>

    <body style="margin:0; padding:0;">
        <table cellpadding="0" cellspacing="0" border="0" align="center" bgcolor="ffffff" style="width:100%; max-width:650px;" class="table-email">
                <tr>
                <td colspan="2">
                <table cellpadding="0" cellspacing="0" border="0" width="100%" align="center" class="instruction">

                </table>
                </td>
            </tr>
            <tr bgcolor="f8fafc">
                <td style="font-size:22px; font-family:Arial, Helvetica, sans-serif; color:#fff; padding:7px 17px; border-bottom:1px solid #ebeef2;">
                    <img src="https://s3.amazonaws.com/campusknot/file/icon/logo.png" style="width:100px;" alt="{{trans('messages.campusknot_logo')}}" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="heading">
                    <span style="font-family:Georgia, 'Times New Roman', Times, serif; color:#383838; font-size:22px; margin-bottom:10px; font-style:italic; margin-top:28px; display:block; text-align:center;">
                        {{ trans('messages.reset_password_page_title') }}
                    </span>
                    <img src="https://s3.amazonaws.com/campusknot/file/icon/bdr-btm.png" alt=""  />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left:20px;">
                    <p style="color:#355871; font-size:20px; font-family:Lato, sans-serif; margin:45px 0 25px;">{{ trans('messages.dear') }} <span style="color:#678baf;"> {{ $name }}, </span></p>
                    <p style="color:#355871; font-size:16px; font-family:Lato, sans-serif; margin:0 ; border-bottom:1px solid #ebeef2; padding:0 0 30px 0;"> 
                        {{ trans('messages.reset_password_mail_text') }} 
                    </p>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="heading" style="padding:15px;">
                    <a href="{{ url(env('APP_URL').'/reset-password?encryption_key='.$sVerificationKey) }}" style="font-family:Lato, sans-serif; background:#4fb0c6;text-decoration: none; border-radius: 5px;padding: 10px 19px 12px 19px;font-size: 17px;font-weight: 500;border-collapse: collapse;display: inline-block;color:#fff;">
                        {{ trans('messages.reset_password_link') }}
                    </a>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left:20px;">
                    <p style="color:#355871; font-size:16px; font-family:Lato, sans-serif; line-height:30px;">
                        {{ trans('messages.cheers') }}  <br>{{ trans('messages.campusknot') }}
                    </p>
                </td>
            </tr>
           <tr>
                <td colspan="2" bgcolor="f8fafc" align="center" style="padding:10px; font-family:Lato, sans-serif; color:#5e5a5a;">
                    <p style="text-align:left; float:left; margin:0;"><a href="#"  style="text-decoration:none; color:#5e5a5a;font-size:14px;">{{ trans('messages.privacy_policy') }}</a> | <a href="#" style="text-decoration:none; color:#5e5a5a;font-size:14px;">{{ trans('messages.terms_of_use') }} </a></p>
                    <p style="text-align:right ; margin:0; display: inline-block; float: right;">
                        <a href="https://www.facebook.com/campusknot/">
                            <img src="https://s3.amazonaws.com/campusknot/file/icon/facebook.png" alt="{{trans('messages.facebook_link')}}"/></a>
                        <a href="https://twitter.com/campusknot/">
                            <img src="https://s3.amazonaws.com/campusknot/file/icon/twitter.png" alt="{{trans('messages.twitter_link')}}"/></a>
                        <a href="https://plus.google.com/+Campusknots/posts">
                            <img src="https://s3.amazonaws.com/campusknot/file/icon/google-plus.png" alt="{{trans('messages.googleplus_link')}}"/></a>
                    </p>
                </td>
            </tr>
        </table>
    </body>
</html>