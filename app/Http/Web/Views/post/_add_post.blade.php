<div class="share clearfix">
    <div id="validation-errors"></div>
        <form id="add_post" name="add_post" action=" {{ route('post.add-post') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  clearfix write-post" >
                <div class="icon-image img-50">
                    {!! setProfileImage("100",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                </div>
                <input id="post_type" name="post_type" type="hidden" value="{{ (old('post_type')) ? old('post_type') : config('constants.POSTTYPETEXT') }}">
            
                @if(isset($oGroupDetails))
                    <input id="id_group" name="id_group" type="hidden" value="{{ $oGroupDetails->id_group }}">
                @endif
                <div class="input-group">
                    <?php 
                    
                    if(isset($oGroupDetails->group_name))
                    {
                        $sPostPlaceholder=$oGroupDetails->group_name;
                        if($oGroupDetails->section != '')
                            $sPostPlaceholder.=' '.trans('messages.section').'-'.$oGroupDetails->section.'('.$oGroupDetails->semester.')';  
                    }
                    else
                        $sPostPlaceholder= 'Followers';
                    ?>
                    <textarea class="form-control" placeholder="{{ trans('messages.post_placeholder',['name'=>htmlspecialchars($sPostPlaceholder)]) }}" id="post_text" name="post_text" aria-describedby="basic-addon1">{{ old('post_text') }}</textarea>
                    @if ($errors->has('post_text'))
                        <span class="error_message"><?php echo $errors->first('post_text'); ?></span>
                    @endif
                </div>
                 <div id="add_code" class="clearfix m-t-3 {{ ($errors->has('post_textarea')) ? '' : 'hidden' }}">
                    <div class="input-group">
                        <textarea name="post_textarea" id="post_textarea" > </textarea>
                        @if ($errors->has('post_textarea'))
                            <span class="error_message"><?php echo $errors->first('post_textarea'); ?></span>
                        @endif
                    </div>
                </div>
              
                <div id="add_file" class="clearfix m-t-3 {{ ($errors->has('file')) ? '' : 'hidden' }}">
                    
                    <div class="input-group file-attachment">
                        <label class="input-group-btn file-attachment-btn">
                            <span class="btn btn-primary">
                                {{ trans('messages.browse') }}<input id="attachment_file" type="file" name="file" multiple="">
                            </span>
                        </label>
                        <input type="text" class="file-attachment-name" readonly="" id="file-attachment">
                        <span class="file-attachment-cancel">{{ trans('messages.cancel') }}</span>
                    </div> 
                    
                    @if ($errors->has('file'))
                        <span class="error_message"><?php echo $errors->first('file'); ?></span>
                    @endif
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="attachment">
                    <a onclick="setPostType('{{ config('constants.POSTTYPETEXT') }}');" href="javascript:void(0);">
                        <img src="{{ asset('assets/web/img/edit-icon.png') }}">
                    </a>
                    <a onclick="setPostType('{{ config('constants.POSTTYPEIMAGE') }}');" href="javascript:void(0);">
                        <img src="{{ asset('assets/web/img/camera-icon.png') }}">
                    </a>

                    <a onclick="setPostType('{{ config('constants.POSTTYPEDOCUMENT') }}');" href="javascript:void(0);">
                        <img src="{{ asset('assets/web/img/attachment-icon.png') }}">
                    </a>
                    <button type="button" class="btn btn-primary btn-lg post-btn" name="save" id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.post') }}</button>
                </div>
            </div>    
            
        </form>
    </div>

<script type="text/javascript" src="{{asset('assets/web/js/tinymce/tinymce.min.js')}}"></script>
<script>
    
    /*add or remove poll options*/
    $(document).ready(function(){
        var next = 2;
        $(".add-more").click(function(e){
            var count = $("#field").children().length;
            if(count < 10){
                e.preventDefault();
                var addto = "#field";
                next = next + 1;
                var newIn = '<div class="new-poll-option"><textarea autocomplete="off" rows="1" placeholder="Type your option" class="input form-control animated" id="field' + next + '" name="option[' + next + ']" type="text" maxlength="255"></textarea><span id="remove' + (next) + '" class="remove-me"  >×</span></div>';
                $(addto).append(newIn);
                callAutoSizeTextArea();

                $("#field" + next).attr('data-source',$(addto).attr('data-source'));
                $("#field" + next).focus();
                $("#count").val(next);  
                 if(count==9){
                    $('.add-more').css('color','#ccc');
                }
            }
           $('.remove-me').on('click',function(e){
                    e.preventDefault();
                    var fieldNum = this.id.charAt(this.id.length-1);
                    var fieldID = "#field" + fieldNum;
                    $(this).parent().remove();
                    $(fieldID).remove();
                    $('.add-more').css('color','#51a2cc');

                });

        });

        callAutoSizeTextArea();
        /*draggable thing*/
    });
    //browse button set file name
    $(':file').on('fileselect', function(event, numFiles, label) {
        showCustomBrowseButton(event, numFiles, label, this);
    });

    $(document).ready(function($) {
        tinymce.init({
            selector: '#post_textarea',
            plugins: [
                "code, preview, contextmenu, image, link, searchreplace,codetag,save"
            ],
            toolbar: "bold italic | example | code | preview | link | searchreplace ",
            setup: function (editor) {
            editor.on('change', function () {
                editor.save();
            });
            }
        });

    $(".attachment a:nth-child(1)").click(function(){
        var post_place="Share with <?php echo htmlspecialchars($sPostPlaceholder); ?>";
        $("#post_text").attr("placeholder", post_place);
    });

    $(".attachment a:nth-child(2)").click(function(){
        $("#file-attachment").val('');
        $("#validation-errors").text('');
        $("#file-attachment").attr("placeholder", "<?php echo trans('messages.post_img') ?>");
        $("#post_text").attr("placeholder", "<?php echo trans('messages.post_placeholder',['name'=>htmlspecialchars($sPostPlaceholder)]) ?>");
    });
    $(".attachment a:nth-child(3)").click(function(){
        $("#file-attachment").val('');
        $("#validation-errors").text('');
        $("#file-attachment").attr("placeholder", "<?php echo trans('messages.post_doc') ?>");   
        $("#post_text").attr("placeholder", "<?php echo trans('messages.post_placeholder',['name'=>htmlspecialchars($sPostPlaceholder)]) ?>");   
    });
    $(".attachment a:nth-child(4)").click(function(){
        $("#file-attachment").val('');
        $("#validation-errors").text('');
        $("#file-attachment").attr("placeholder", "<?php echo trans('messages.post_poll') ?>");   
        $("#post_text").attr("placeholder", "<?php echo trans('messages.post_pollplaceholder',['name'=>htmlspecialchars($sPostPlaceholder)]) ?>");   
    });
    
    var options = { 
        beforeSubmit:  showRequest,
        success:       showResponse,
        error:showError,
        dataType: 'json' 
        }; 
        $('.post-btn').click(function(e){
            $(this).button('loading');
            tinyMCE.triggerSave;
            $('#add_post').ajaxForm(options).submit();  		
        });
    });
    
    function showRequest(formData, jqForm, options) { 
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
    }
    
    function showResponse(response, statusText, xhr, $form)  {
        if(response.success == true)
        {
            $(".feeds").prepend(response.html);
            $( ".feeds div" ).first().hide();
            $('#add_post')[0].reset();
            $('.post-btn').button('reset');
            $( ".feeds div" ).first().slideDown( "slow" );
            $( ".attachment a:nth-child(1)" ).trigger( "click" );
        } else { 
            var arr = response;
            $("#validation-errors").append('<div class="alert alert-error"><div>');
            $.each(result, function(index, value)
            {
                if (value.length != 0)
                {
                    $("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
                }
            });
            $("#validation-errors").show();
            $('.post-btn').button('reset');
        }
    }
    
    function showError(xhr, textStatus, errorThrown)  {
        if(xhr.status == 500 ){
            window.location = siteUrl + '/home';
        }
        var arr = xhr.responseText;
        var result = $.parseJSON(arr);
        $("#validation-errors").append('<div class="alert alert-error"><div>');
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                $("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
            }
        });
        $("#validation-errors").show();
        $('.post-btn').button('reset');
    }
</script>