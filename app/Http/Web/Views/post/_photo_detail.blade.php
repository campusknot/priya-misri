<div class="clearfix modal-content photo-detail">
    <div class="clearfix col-lg-8 col-md-8 col-sm-8 col-xs-8 photo-detail-left">
        <img src="{{ setPostImage($oPostDetail['post_detail']->file_name,80) }}">
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding photo-comment">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-top-section">
            <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
             <div class="icon-image img-35">
                 {!! setProfileImage("50",$oPostDetail['post_detail']->user_profile_image,$oPostDetail['post_detail']->id_user,$oPostDetail['post_detail']->first_name,$oPostDetail['post_detail']->last_name) !!}
            </div>
            <div class="post-person-name">
                <a href="#">{{ $oPostDetail['post_detail']->first_name.' '.$oPostDetail['post_detail']->last_name }}</a>
                <span class="post-time">{{ secondsToTime($oPostDetail['post_detail']->created_at) }}
                @if($oPostDetail['post_detail']->group_name != NULL){{' |'}} 
                    | <a href="{{ route('group.group-feeds', ['nIdGroup' => $oPostDetail['post_detail']->id_group]) }}">
                        <span class="group-name">{{$oPostDetail['post_detail']->group_name }}</span>
                    </a>  
                </span>
                @endif
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12">
            @if(trim($oPostDetail['post_detail']->post_text) != NULL)
                <p class="more commentDetailmore">{{ $oPostDetail['post_detail']->post_text }}</p>
            @endif
            @if($sAllowedComment == 1)
            <div class="post-likes like_button_{{ $oPostDetail['post_detail']->id_post }}" >
                <div class="btn btn-like {{ ($oPostDetail['post_detail']->id_like) ? "btn-like-user" : "" }}" onclick="updatePostLike('like_button_{{ $oPostDetail['post_detail']->id_post }}', '{{ $oPostDetail['post_detail']->id_post }}');">
                    <!-- A<span>+</span> -->
                </div>
            </div>
            @endif
                

                
            
        </div>
        <div class=" col-lg-12 col-md-12 col-sm-12 col-xs-12 commnts-list ">
        @if(count($oPostDetail['comments']) > 0)
            @foreach($oPostDetail['comments'] as $oComment)
                <div class=" photo-detail-comment">
                    <a href="{{route('user.profile',[$oComment->id_user])}}" class="icon-image img-35">
                        {!! setProfileImage("50",$oComment->user_profile_image,$oComment->id_user,$oComment->first_name,$oComment->last_name) !!}
                    </a>
                        <div class="post-person-name">
                            @if($oComment->id_user == Auth::user()->id_user)
                                <span class="comment-links pull-right">
                                    <a href="javascript:void(0)" onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ["record_name" => "comment"]) }}','{{route('post.delete-post-comment',[$oComment->id_comment])}}');">{{ trans('messages.delete') }}</a>
                                </span>
                            @endif
                            <a href="{{route('user.profile',[$oComment->id_user])}}">{{ $oComment->first_name.' '.$oComment->last_name }}</a>
                            <span class="comment-text">
                                <p class="">{!! setTextHtml($oComment->comment_text) !!}</p></span>
                            
                             @if($oComment->file_name)
                        <div class="clearfix comment-content">
                            <?php
                                switch ($oComment->comment_type)
                                {
                                    case config('constants.POSTTYPEIMAGE'):
                                        echo '<div class="comment-img"> <img  src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'"> </div>';
                                        break;
                                    case config('constants.POSTTYPEVIDEO'):
                                        break;
                                    case config('constants.POSTTYPEDOCUMENT'):
                                        $aFileNameData = explode('.', $oComment->file_name);

                                        //Div for file icon
                                        $sHtmlString = '<div class="comment-doc">';
                                        if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                        {
                                            $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                        }
                                        elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                        {
                                            $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                        }
                                        elseif (in_array(end($aFileNameData), array('key','ppt','pps')))
                                        {
                                            $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                        }
                                        elseif (in_array(end($aFileNameData), array('zip','rar')))
                                        {
                                            $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                        }
                                        elseif (end($aFileNameData) == 'pdf')
                                        {
                                            $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                        }
                                      

                                        //Div for file details and view/download option
                                      
                                          
                                                $sHtmlString .= '<span>'.(($oComment->display_file_name) ? $oComment->display_file_name : $oComment->file_name);
                                          
                                          
                                               // $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['sFileName' => $oComment->file_name]).'">'.trans('messages.view').'</a> </span>';
                                                
                                                    $sHtmlString .= '<a class="btn" target="blank" href="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'" download="'.$oComment->file_name.'">'.trans('messages.download').'</a>';
                                         
                                        $sHtmlString .= '</div>';

                                        echo $sHtmlString;
                                        break;
                                    default :
                                        //Code for text/code
                                        echo 'No file found';
                                }
                            ?>
                        </div>
                        @endif
                        </div>
                    </div>
            @endforeach
        @endif
        </div>
        
        @if($sAllowedComment == 1)
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 add-comment">
                <form id="add_comment_photo_detail" class="clearfix add_comment" name="add_comment" action="{{ route('post.add-comment') }}" method="POST" enctype="multipart/form-data">
                   {!! csrf_field() !!}
                   <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding" >

                           <input type="hidden" name="id_post" value="{{ $oPostDetail['post_detail']->id_post }}">
                           <input type="hidden" name="flag" value="1">
                           <input id="comment_type_{{ $oPostDetail['post_detail']->id_post }}" name="comment_type" type="hidden" value="{{ (old('comment_type_'.$oPostDetail['post_detail']->id_post)) ? old('comment_type_'.$oPostDetail['post_detail']->id_post) : config('constants.POSTTYPETEXT') }}">
                           <div class="">
                                <textarea class="form-control animated" name="comment_text_{{ $oPostDetail['post_detail']->id_post }}" value="{{old('comment_text_'.$oPostDetail['post_detail']->id_post)}}" placeholder="{{ trans('messages.add_comment') }}" aria-describedby="basic-addon1"></textarea>
                                <!-- <input type="text" class="form-control" name="comment_text_{{ $oPostDetail['post_detail']->id_post }}" value="{{old('comment_text_'.$oPostDetail['post_detail']->id_post)}}" placeholder="{{ trans('messages.add_comment') }}" aria-describedby="basic-addon1">  -->
                                <span class="error_message" id="error_message_photo_detail"></span>
                                <span class="comment-attchment" >
                                    <label   ><img src="{{ asset('assets/web/img/attachment-icon.png') }}" class="no-border" onclick="setPostCommentType('{{ config('constants.POSTTYPEDOCUMENT') }}','{{ $oPostDetail['post_detail']->id_post }}');" /></label>
                                    <label  ><img src="{{ asset('assets/web/img/camera-icon.png') }}" onclick="setPostCommentType('{{ config('constants.POSTTYPEIMAGE')}}','{{ $oPostDetail['post_detail']->id_post }}');" /></label>
                                </span>
                                <div class="input-group" id="file-input">
                                    <label class="input-group-btn file-attachment-btn">
                                        <span class="btn btn-primary">
                                            Browse… <input type="file" name="file_{{ $oPostDetail['post_detail']->id_post }}"   multiple="">
                                        </span>
                                    </label>
                                    <input type="text" class=" file-attachment-name" readonly="">
                                    <span class="file-attachment-cancel" onclick="setPostCommentType('{{ config('constants.POSTTYPETEXT')}}','{{ $oPostDetail['post_detail']->id_post }}');">{{ trans('messages.cancel') }}</span>
                                </div>

                                <!-- <input id="file-input" type="file" name="file_{{ $oPostDetail['post_detail']->id_post }}" class="file-input" aria-expanded="false" />   -->

                               <!-- @if ($errors->has('comment_text_'.$oPostDetail['post_detail']->id_post))
                                   <span class="error_message"><?php echo $errors->first('comment_text_'.$oFeed->id_post); ?></span>
                               @elseif($errors->has('file_'.$oPostDetail['post_detail']->id_post))
                                   <span class="error_message"><?php echo $errors->first('file_'.$oFeed->id_post); ?></span>
                               @endif -->
                           </div>

                   </div>
                   <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 no-padding">
                       <button type="button" class="btn btn-primary btn-md btn-md-pd" data-id='{{$oPostDetail['post_detail']->id_post}}' id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.send') }}</button>
                   </div>
               </form>
            </div>
        @endif
    </div>    
</div>
<script>
    /*js for auto expand add comment box*/
//    $(function(){
//        $('.normal').autosize();
//        $('.animated').autosize({append: "\n"});
//    });
        callAutoSizeTextArea();
    
    
    
    $(document).ready(function(){
        /*js forshow less and more image caption*/
        showMoreAmountdata(100,'commentDetailmore');
        showMoreAmountdata(100,'commentDetailListMore');
        $(".commentDetailmore .morelink").on("click", function(e){
           setMoreAmountDataLabel(this);
           e.preventDefault();
        });
         
            var wheight=$( window ).height();
            var cheight= wheight-160;
            $('.photo-comment').css('max-height',(wheight-32)+'px');
            $('.commnts-list').css('max-height',cheight+'px');
            
        
        $(".comment-attchment label:first-child").click(function(){
            $("#error_message_photo_detail").html('');
            $(this).parent().siblings("#file-input").addClass("show-input");
            $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
            $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any document ");
        });
        $(".comment-attchment label:last-child").click(function(){
            $("#error_message_photo_detail").html('');
            $(this).parent().siblings("#file-input").addClass("show-input");
            $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
            $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any photos ");
        });
        $(".file-attachment-cancel").click(function(){
              $(this).parent().removeClass("show-input");
              
        });
    });
    
    $(function() {

  // We can attach the `fileselect` event to all file inputs on the page
  $(document).on('change', ':file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
  });

  // We can watch for our custom `fileselect` event like this
  $(document).ready( function() {
      $(':file').on('fileselect', function(event, numFiles, label) {

          var input = $(this).parents('.input-group').find(" .file-attachment-name"),
              log = numFiles > 1 ? numFiles + ' files selected' : label;

          if( input.length ) {
              input.val(log);
          } else {
              if( log ) alert(log);
          }

      });
  });
  
});
$(document).ready(function($) {
        
    var options_photo_detail = {
        beforeSubmit:  showRequestCommentPhotoDetail,
        success:       showResponseCommentPhotoDetail,
        error:  showErrorCommentPhotoDetail,
        dataType: 'json' 
        };
        $('.btn-md-pd').click(function(e){
            var data=$(this);
            var id_post=data.attr('data-id');
            $(this).button('loading');
            $('#add_comment_photo_detail').ajaxForm(options_photo_detail).submit();
        });

        $(document).on("keypress", "#add_comment_photo_detail input:text", function (e) {
          if (e.keyCode == 13){
              e.preventDefault();
              $('#add_comment_photo_detail').ajaxForm(options_photo_detail).submit();
          }
        });

    });		
    function showRequestCommentPhotoDetail(formData, jqForm, options_photo_detail) {
            $("#validation-errors").hide().empty();
            $("#output").css('display','none');
            return true; 
    } 
    function showResponseCommentPhotoDetail(response, statusText, xhr, $form)  {
        if(response.success == true)
        {
            $('#add_comment_photo_detail')[0].reset();
            $('.commnts-list').prepend(response.html);
            $('.btn-md-pd').button('reset');
//            $('.animated').css('height','36px');
            setAutosizeTextAreaHeight(30);
            $("#error_message_photo_detail").html('');
        } else { 
            var arr = response;
                $.each(arr, function(index, value)
                {
                        if (value.length != 0)
                        {
                                //$(".error_message").append('<strong>'+ value +'</strong>');
                        }
                });      
        }
    }
    function showErrorCommentPhotoDetail(xhr, textStatus, errorThrown)  {
    console.log('showErrorCommentPhotoDetail');
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
               //location.reload();
            }
        
        var result = $.parseJSON(arr);
        $('.btn-md-pd').button('reset');
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            { 
                    $("#error_message_photo_detail").html('<strong>'+ value +'</strong>');
            }
        }); 
    }
</script>

