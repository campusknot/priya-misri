<div class="modal-content book-popup">
    <form>
        <div class="modal-header">
            <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title event-title">{{ trans('messages.add_to_wishlist') }}</h4>
        </div>
        <div class="modal-body clearfix">
            <div class="form-group clearfix">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no-padding">
                    <input type="text" title="" data-toggle="tooltip"  data-placement="top" id="group_name" 
                           class="form-control {{ ($errors->has('group_name')) ? 'error' : '' }}" 
                           name="group_name" placeholder="{{trans('messages.isbn_number')}}" value="" 
                           required="required">
                </div>
                
                <input  type="button" class="btn btn-primary pull-right book_isbn_search" value="Search" id="">
                <div class="add-new-book book_isbn_search">or <span>Add New book</span></div>
                
                
            </div>
            <div class="book-detail-input col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 no-padding">
                    <div class="form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <input type="text" title="" data-toggle="tooltip"  data-placement="top" id="group_name" 
                               class="form-control " 
                               name="group_name" placeholder="{{trans('messages.book_title')}}" value="" 
                               required="required">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 no-padding">
                            <input type="text" title="" data-toggle="tooltip"  data-placement="top" id="group_name" 
                               class="form-control " 
                               name="group_name" placeholder="{{trans('messages.isbn')}}" value="" 
                               required="required">
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding pull-right">
                            <input type="text" title="" data-toggle="tooltip"  data-placement="top" id="group_name" 
                               class="form-control " 
                               name="group_name" placeholder="{{trans('messages.isbn13')}}" value="" 
                               required="required">
                        </div>
                    </div>
                    
                    <div class="form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <input type="text" title="" data-toggle="tooltip"  data-placement="top" id="group_name" 
                                   class="form-control " 
                                   name="group_name" placeholder="{{trans('messages.book_author')}}" value="" 
                                   required="required">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <textarea class="font-control" placeholder="{{trans('messages.book_desc')}}"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    <div class="book-detail-thumbnail">
                        <img class="" src="{{asset('assets/web/img/book-cover.jpg')}}">
                    </div>
                    <div class="book-detail-price">
                        $
                        <input type="text" class="form-control" placeholder="10">
                    </div>
                </div>
                 <div class=" submit-options clearfix">
                    <input  type="button" class="btn btn-primary btn-book-buy" value="Save">
                </div>
            </div>
          
        </div>
        
    </form>
</div>


<script>
    $(".book_isbn_search").click(function(){
        $(".book-detail-input").toggleClass("show-book-detail");
        $(this).parent(".form-group").css("display", "none");
        
    });
    
    
</script>
