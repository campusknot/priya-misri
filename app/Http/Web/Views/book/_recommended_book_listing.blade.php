
<div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <input type="text" placeholder="Search book by ISBN, Title or Author " class="form-control search-book-bar">
</div>

<div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
     <!-- Book listing design for Recommended section -->
    <ul class="book-desc col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
        <li class="book-thumbnail col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <img class="" src="{{asset('assets/web/img/book-cover.jpg')}}">

        </li>
        <li class="book-info col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <span class="book-title">Business 10th edition</span>
            <span class="book-caption">{{ trans('messages.author') }}:
                <span class="book-caption-value">O C Ferrell, Ferrell, Hirt, Geoffrey Hirt</span>
            </span>
            
            <span class="book-caption">{{ trans('messages.isbn') }}:
                <span class="book-caption-value">1259179397</span>
            </span> 
            
            <span class="book-caption">{{ trans('messages.seller') }}:
                <span class="book-caption-value">Jay pandya</span>
            </span> 
            
        </li>
        <li class="book-buy col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
            <span class="book-price">$5</span>
            <button class="pull-right book-buy-action">Buy</button>
            <span class="book-buy-block">
                
                <textarea class="form-control" placeholder="Drop a message"></textarea>
                <input type="text" class="form-control col-lg-8 col-md-8 col-sm-8 col-xs-8" placeholder="Enter your contact detail">
                <button class="book-buy-btn col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">{{ trans('messages.send') }}</button>
            </span>
        </li>
    </ul>
     
      <!-- Book listing design for Recommended section -->
    <ul class="book-desc col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
        <li class="book-thumbnail col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <img class="" src="{{asset('assets/web/img/book-cover.jpg')}}">

        </li>
        <li class="book-info col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <span class="book-title">Business 10th edition</span>
            <span class="book-caption">{{ trans('messages.author') }}:
                <span class="book-caption-value">O C Ferrell, Ferrell, Hirt, Geoffrey Hirt</span>
            </span>
            
            <span class="book-caption">{{ trans('messages.isbn') }}:
                <span class="book-caption-value">1259179397</span>
            </span> 
            
            <span class="book-caption">{{ trans('messages.seller') }}:
                <span class="book-caption-value">Jay pandya</span>
            </span> 
            
        </li>
        <li class="book-buy col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
            <span class="book-price">$5</span>
            <button class="pull-right book-buy-action">Buy</button>
            <span class="book-buy-block">
                
                <textarea class="form-control" placeholder="Drop a message"></textarea>
                <input type="text" class="form-control col-lg-8 col-md-8 col-sm-8 col-xs-8" placeholder="Enter your contact detail">
                <button class="book-buy-btn col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">{{ trans('messages.send') }}</button>
            </span>
        </li>
    </ul>
      
       <!-- Book listing design for Recommended section -->
    <ul class="book-desc col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
        <li class="book-thumbnail col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <img class="" src="{{asset('assets/web/img/book-cover.jpg')}}">

        </li>
        <li class="book-info col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <span class="book-title">Business 10th edition</span>
            <span class="book-caption">{{ trans('messages.author') }}:
                <span class="book-caption-value">O C Ferrell, Ferrell, Hirt, Geoffrey Hirt</span>
            </span>
            
            <span class="book-caption">{{ trans('messages.isbn') }}:
                <span class="book-caption-value">1259179397</span>
            </span> 
            
            <span class="book-caption">{{ trans('messages.seller') }}:
                <span class="book-caption-value">Jay pandya</span>
            </span> 
            
        </li>
        <li class="book-buy col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
            <span class="book-price">$5</span>
            <button class="pull-right book-buy-action">Buy</button>
            <span class="book-buy-block">
                
                <textarea class="form-control" placeholder="Drop a message"></textarea>
                <input type="text" class="form-control col-lg-8 col-md-8 col-sm-8 col-xs-8" placeholder="Enter your contact detail">
                <button class="book-buy-btn col-lg-4 col-md-4 col-sm-4 col-xs-4 pull-right">{{ trans('messages.send') }}</button>
            </span>
        </li>
    </ul>
    <!-- Book listing design for My books section -->
    <ul class="book-desc col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix">
        <li class="book-thumbnail col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <img class="" src="{{asset('assets/web/img/book-cover.jpg')}}">

        </li>
        <li class="book-info col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <span class="book-title">Business 10th edition</span>
            <span class="book-caption">{{ trans('messages.author') }}:
                <span class="book-caption-value">O C Ferrell, Ferrell, Hirt, Geoffrey Hirt</span>
            </span>
            <span class="book-caption">{{ trans('messages.isbn') }}:
                <span class="book-caption-value">1259179397</span>
            </span>
            <span class="buyers new-buyer clearfix">
                 <div data-toggle="collapse" data-target="#intrested_buyers" class="intrested-buyers-title">
                     5 {{ trans('messages.interested_buyers') }}
                     <span class="new-buyer-number">5</span>
                </div>
                <div id="intrested_buyers" class="collapse buyers-list clearfix">
                    <span class="buyer-info clearfix">
                        <span class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <span class="buyer-pic icon-image img-35">
                                <img src="https://s3.amazonaws.com/campusknot-local/user-media/20161111063815.png">
                            </span>
                            <span class="buyer-message-section clearfix">
                                <span class="buyer-name">Priya Patel  | </span>
                                <span class="buyer-contact">Contact: +10-120-522-5994</span>
                                <span class="buyer-message">
                                    Hi..! i want to buy your book &Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                </span>
                            </span>
                        </span>
                    </span>
                    <span class="buyer-info clearfix">
                        <span class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                            <span class="buyer-pic icon-image img-35">
                                <img src="https://s3.amazonaws.com/campusknot-local/user-media/20161111063815.png">
                            </span>
                            <span class="buyer-message-section clearfix">
                                <span class="buyer-name">Hiten  Patel  | </span>
                                <span class="buyer-contact">Contact: +10-120-522-5994</span>
                                <div  class="buyer-message" data-toggle="collapse" data-target="#buyer-chat">
                                    Hi..! i want to buy your book &Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                    <span class="view-buyer-chat">View more</span>  
                                </div>
                                <div class="">
                                    <div class="buyers-chat-view ">
                                        <span class="buyer-pic icon-image img-35">
                                            <img src="https://s3.amazonaws.com/campusknot-local/user-media/20161114084608.png">
                                        </span>
                                        <span class="message">
                                             Seller:Hi..! i want to buy your book &Lorem ipsum dolor sit amet, incididunt ut labore et dolore magna aliqua. 
                                        </span>
                                    </div>
                                    <div class="buyers-chat-view">
                                        <span class="buyer-pic icon-image img-35">
                                            <img src="https://s3.amazonaws.com/campusknot-local/user-media/20161111063815.png">
                                        </span>
                                        <span class="message">
                                             Buyer:Hi..! i want to buy your book &Lorem ipsum dolor sit amet
                                             <div class="reply-buyer" data-toggle="collapse" data-target="#buyer-reply">{{ trans('messages.reply') }}</div>
                                             <div class="collapse" id="buyer-reply">
                                                <input type="text" class="form-control ">
                                                <span class=" btn btn-primary">
                                                    {{ trans('messages.send') }}
                                                </span>
                                             </div>
                                        </span>
                                    </div>
                                </div>
                            </span>
                        </span>
                    </span>
                </div>
            </span>
        </li>
        <li class="book-status col-lg-2 col-md-2 col-sm-2 col-xs-2 pull-right">
            <span class="book-price">$5</span>
            <span class="book-select-status">
                <span class="caret status-1"></span>
                <select class="form-control status-1">
                    <option>{{ trans('messages.available') }}</option>
                    <option>{{ trans('messages.sold_out') }}</option>
                </select>
            </span>
        </li>
    </ul>
</div>