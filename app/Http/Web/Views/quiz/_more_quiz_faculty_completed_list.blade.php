@foreach($oQuizList as $Quiz)
    <?php
    $dQuizStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $Quiz->start_time);
    $dQuizStartDate->setTimezone(Auth::user()->timezone);
    $dQuizEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $Quiz->end_time);
    $dQuizEndDate->setTimezone(Auth::user()->timezone);
    ?>
    <div class="panel clearfix" >
        <span class="hidden">{{ $Quiz->marks_question }}</span>
        <div class="quiz pos-relative course" onclick="openQuizSlider('{{ route('quiz.quiz-add',['nIdQuiz'=> $Quiz->id_quiz])}}');">
            <div class="clearfix quiz-card-title">
                <span class="">
                    {{ $Quiz->quiz_title }}
                </span>      
                <span class="pull-right quiz-card-right-section">
                    <span class="quiz-card-points">{{ $Quiz->total_marks }} pt</span>
                </span>
            </div>
            <div class="quiz-card-information">
                <div>
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-group.png')}}"></span>
                    {{ $Quiz->group_name }}
                    @if($Quiz->section != '')
                            {{ trans('messages.section') }}- {{$Quiz->section}} ({{$Quiz->semester}})
                    @endif
                </div>
                <div>
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-date.png')}}"></span>
                    <span><span class="text-uppercase font-w-500">{{ trans('messages.start') }}</span> : {{ $dQuizStartDate->format('l')}} {{$dQuizStartDate->format('M j')}}, {{$dQuizStartDate->format('g:i A')}}</span> |  <span class="text-uppercase font-w-500">{{ trans('messages.end') }}</span> : {{ $dQuizEndDate->format('l')}} {{$dQuizEndDate->format('M j')}}, {{$dQuizEndDate->format('g:i A')}}
                </div>
                <div >
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/duration.png')}}"></span>
                     {{ trans('messages.quiz_duration') }}: {{ $Quiz->duration }} {{ trans('messages.mins') }}  
                </div>

            </div>
            @if($Quiz->marks_remaining != 0)
                <div class="show-quiz-notification">
                    <span class="marks-alert marks-add-alert">
                        <img class="alert-img v-align-top" src="{{asset('assets/web/img/yellow-alert.png')}}" alt="" />
                        <span class="alert-text">{{ trans('messages.allot_marks_for_open_ended_question') }}</span>
                    </span>
                </div>
            @endif
        </div>
        <a data-toggle="collapse" href="#users_{{$Quiz->id_quiz}}" class="padding-10 submited-user-link accordion-toggle" onclick="getUserInvitedList({{$Quiz->id_quiz}},'user_list',1)">
            <span  class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/check.png')}}"></span>
            {{ trans('messages.quiz_submitted_by') }} : {{ $Quiz->completed_users }} /{{ $Quiz->total_user }} {{ trans('messages.users') }}
        </a>
        <div class="collapse" id="users_{{$Quiz->id_quiz}}">
            <div class="user_list" id="user_list_{{ $Quiz->id_quiz }}" data-id="{{$Quiz->id_quiz}}"></div>
            <div class='spinner hidden'> 
                <div class='bounce1'></div>
                <div class='bounce2'></div>
                <div class='bounce3'></div>
            </div>
        </div>
    </div>
@endforeach