<div class="quiz-single-question pos-relative clearfix">
    <?php
        $nMarks= isset($oQuizQuestion->marks) ? $oQuizQuestion->marks : old('marks');
        $nIdquiz = isset($nIdQuiz) ? $nIdQuiz : old('id_quiz');
        $sQuestionText = isset($oQuizQuestion->question_text) ? $oQuizQuestion->question_text : old('question_text');
        $sQuestionType = isset($oQuizQuestion->question_type) ? $oQuizQuestion->question_type : old('question_type');
        $nCorrectAnswer = isset($oQuizQuestion->correct_answer) ? $oQuizQuestion->correct_answer : old('correct_answer');
        $aAnswerOptions = isset($oQuizQuestion->answer_option) ? $oQuizQuestion->answer_option : old('answer_option');
        $nQuestionCount = isset($nQuestionCount) ? $nQuestionCount : old('question_count');
        $aIdQuestion = isset($oQuizQuestion->id_quiz_question) ? $oQuizQuestion->id_quiz_question : '';
    ?>
    @if(empty($aIdQuestion))
    <span class="pull-right quiz-edit-btn">
        <a href="javascript:void(0);" class="ck-red-color" onclick="$('#form_add_question_{{ $nQuestionCount }}').parent().remove();">{{ trans('messages.delete') }}</a> 
    </span>
    @endif
    <form id="form_add_question_{{ $nQuestionCount }}" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="clearfix text-center">
            <span class="quiz-question-count disp-quiz-counter">{{ trans('messages.question') }} <span class="counter_cal">{{ $nQuestionCount }}<span></span>
        </div>
        <div class="clearfix quiz-add-pts-type">
            <span class="quiz-question-points" >
                <span class="label-input-parent display-ib {{ ($errors->has('marks')) ? 'error' : ''}}">
                    <label>{{ trans('messages.points') }}</label>
                    <input type="text" class="form-control" placeholder="" name="marks" value="{{ $nMarks }}"/>
                </span>
            </span>
             <span class="pos-relative q-type pos-relative display-ib v-align-top pull-right">
                <select class=" display-ib form-control pull-right question-type" name="question_type">
                    <option value="{{ config('constants.QUESTIONTYPEMULTIPLE') }}">{{ trans('messages.multi_choise') }}</option>
                    <option value="{{ config('constants.QUESTIONTYPEOPEN') }}" {{($sQuestionType == config('constants.QUESTIONTYPEOPEN')) ? 'selected = "selected"':''}}>{{ trans('messages.open_ended') }}</option>
                </select>
                <span class="caret caret-right"></span>
            </span>
        </div>
        <div class="form-group clearfix">
            <span class="{{ ($errors->has('question_text')) ? 'has-error' : ''}}" >
                <textarea type="text" class="form-control q-question display-ib animated quiz-question" name="question_text" placeholder="{{ trans('messages.add_question') }}" title="{{($errors->has('question_text')) ? $errors->first('question_text') : '' }}">{{$sQuestionText }}</textarea>
            </span>
        </div>
        <div class="answer_type answer_type_M  text-right clearfix {{ ($errors->has('answer_option')) ? 'has-error' : ''}} {{($sQuestionType == config('constants.QUESTIONTYPEOPEN')) ? 'hidden':''}}">
            <div class="ck-error text-left">{{($errors->has('answer_option')) ? $errors->first('answer_option') : '' }}<br/>
                {{($errors->has('correct_answer')) ? $errors->first('correct_answer') : '' }}</div>
            @if(count($aAnswerOptions))
                @foreach($aAnswerOptions as $key=>$answer)
                    <div class="single-mcq-answer form-group clearfix pos-relative">
                        <textarea type="text" class="form-control q-question display-ib v-align animated" name="answer_option[{{ $key }}]" placeholder="option">{{ isset($answer['answer_text']) ? $answer['answer_text'] : old('answer_option.'. $key) }}</textarea>
                        <input type='hidden' name="id_answer_option[{{ $key }}]" value="{{ isset($answer['id_answer_option']) ? $answer['id_answer_option'] :'' }}" >
                        <input type="radio" id="que{{ $nQuestionCount }}_radio{{ $key }}" name="correct_answer" value="option_{{ $key }}" {{ (( isset($answer['id_answer_option']) && $nCorrectAnswer == $answer['id_answer_option']) || ($nCorrectAnswer == 'option_'.$key) ) ? 'checked' : '' }} >
                        <label for="que{{ $nQuestionCount }}_radio{{ $key }}">{{ trans('messages.correct_answer') }}</label>
                        <span class="remove-option pull-right" onclick="deleteThisOption(this,{{ isset($answer['id_answer_option']) ? $answer['id_answer_option'] :'' }});" >&times;</span>
                    </div>
                @endforeach
            @else
                @for($nKey=1; $nKey<=2; $nKey++)
                    <div class="single-mcq-answer form-group clearfix pos-relative">
                        <textarea type="text" class="form-control q-question display-ib v-align animated" name="answer_option[{{ $nKey }}]" placeholder="option"></textarea>
                        <input type='hidden' name="id_answer_option[{{ $nKey }}]" >
                        <input type="radio" id="que{{ $nQuestionCount }}_radio{{ $nKey }}" name="correct_answer" value="option_{{ $nKey }}">
                        <label for="que{{ $nQuestionCount }}_radio{{ $nKey }}">{{ trans('messages.correct_answer') }}</label>
                        <span class="remove-option pull-right" onclick="deleteThisOption(this,'');">&times;</span>
                    </div>
                @endfor
            @endif
        </div>
        <div class="answer_type answer_type_O">
        </div>
        <div class="answer_option answer_option_M {{($sQuestionType == config('constants.QUESTIONTYPEOPEN')) ? 'hidden':''}}">
            <span class="add-new-option cursor-pointer" onclick="addOption(this,{{ $aIdQuestion }})">{{ trans('messages.add_option') }}</span>
        </div>
        <input type="hidden" name="id_quiz_question" value="{{ $aIdQuestion }}" />
        <input type="hidden" name="question_count" value="{{ $nQuestionCount }}" />
        <input type="hidden" name="id_quiz" value="{{ $nIdquiz }}" />
        <input type="button" value="save" class="pull-right btn btn-primary" onclick="submitFormQuiz('form_add_question_{{ $nQuestionCount }}', '{{ route("quiz.quiz-question-add") }}', this ,'question');">
    </form>

<script type="text/javascript">
        callAutoSizeTextArea();
        setAutosizeTextAreaHeight(34);
        // display answer options for mcq and short questions
        $(function() {
            $('.question-type').change(function(){
                $(this).parent().parent().siblings(".answer_type").addClass("hidden");
                $(this).parent().parent().siblings(".answer_type_"+$(this).val()).removeClass("hidden");
                //addoption hide and show
                $(this).parent().parent().siblings(".answer_option").addClass("hidden");
                $(this).parent().parent().siblings(".answer_option_"+$(this).val()).removeClass("hidden");
            });
        });
        //remove option onClick 
</script>
</div>