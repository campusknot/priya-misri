@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Quiz List')

@section('content')
    <div class="clearfix faculty-quiz-listing pos-relative">
        <ul class="ck-top-tabs group-tabs nav nav-tabs clearfix">
            <li class="active" id="all_group">
                <a href="javascript:void(0);" onclick="changeQuizList('quiz_card', '{{ route('quiz.quiz-student-list-ajax') }}', '{{ route('quiz.quiz-student-list') }}','all_group');" >{{ trans('messages.scheduled_for_me') }}</a></li>
            <li class="" id="all_group">
                <a href="javascript:void(0);" onclick="changeQuizList('quiz_card', '{{ route('quiz.quiz-student-list-ajax') }}', '{{ route('quiz.quiz-student-list') }}','all_group');" >{{ trans('messages.created_by_me') }}</a>
            </li>
<!--            @foreach($oGroupList as $oGroup)
                <li id='tab_{{ $oGroup->id_group }}'>
                    <a href="javascript:void(0);" onclick="changeQuizList('quiz_card', '{{ route('quiz.quiz-student-list-ajax',['nIdGroup' => $oGroup->id_group]) }}', '{{ route('quiz.quiz-student-list',['nIdGroup' => $oGroup->id_group]) }}','{{ 'tab_'.$oGroup->id_group}}');" >{{ $oGroup->group_name }}</a>
                </li>
            @endforeach-->
        </ul>
    </div>
<div class="ck-white-panel" >
    <div id="quiz_card" class="">
        @if($sQuizListingType != 'completed')
            @include('WebView::quiz._quiz_student_list_ajax')
        @else
            @include('WebView::quiz._quiz_student_completed_list_ajax')
        @endif
    </div>
    <div class="switch-tab-loader hidden">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
</div>
<script type="text/javascript">

</script>
@stop