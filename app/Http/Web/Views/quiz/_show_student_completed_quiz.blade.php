<div class="modal-content add-quiz">
    <!-- Filename:: quiz._show_student_completed_quiz -->
     <div class="modal-header">
        <button type="button" onclick="hideSlider();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        {{ trans('messages.quiz_detail') }}
     </div>
    <div class=" qc-information clearfix" >
    <?php
    $dQuizStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $oQuiz->start_time);
    $dQuizEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $oQuiz->end_time);
    $dQuizStartDate->setTimezone(Auth::user()->timezone);
    $dQuizEndDate->setTimezone(Auth::user()->timezone);
    ?>
    <span class="hidden">{{ $oQuiz->marks_question }}</span>
        <div class="quiz-card-title">
            <span class="">
                {{ $oQuiz->quiz_title }} 
            </span>      
            <span class="pull-right quiz-card-right-section">
                 <span class="quiz-card-points">{{ trans('messages.marks_obtained') }} : {{ !empty($oQuiz->mark_obtain) ? $oQuiz->mark_obtain : 0  }}  / {{ $oQuiz->total_marks }} pt</span>
            </span>
        </div>
        <div class="quiz-card-information">
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/faculty-icon.png')}}"></span>
                {{ $oUser->first_name }} {{ $oUser->last_name }}
            </div>
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-group.png')}}"></span>
                {{ $oQuiz->group_name }}
            </div>
           
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-date.png')}}"></span>
                <span class="text-uppercase">{{ trans('messages.start') }}</span> : {{ $dQuizStartDate->format('l')}} {{$dQuizStartDate->format('M j')}}, {{$dQuizStartDate->format('g:i A')}} |  <span class="text-uppercase">{{ trans('messages.end') }}</span> : {{ $dQuizEndDate->format('l')}} {{$dQuizEndDate->format('M j')}}, {{$dQuizEndDate->format('g:i A')}}
            </div>
            <div>
                <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/duration.png')}}"> </span>{{ trans('messages.quiz_duration') }}: {{ $oQuiz->duration }} {{ trans('messages.mins') }} 
            </div>
            <div class="show-score">
                {{ trans('messages.time_taken') }} - {{ $oQuiz->time_taken }}
            </div>
        </div>
    </div>
</div>
<div id="quiz_questions">
    @include('WebView::quiz._show_student_completed_question')
</div>
<script type="text/javascript">
        var nCurrentPage = <?php echo $oQuestions->currentPage(); ?>;
        var nLastPage = <?php echo $oQuestions->lastPage(); ?>;
        $('#slider_light_box_container').on('scroll', function() {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                nCurrentPage +=1;
                loadNewData('quiz_questions', '<?php echo route('quiz.student-completed-quiz',['nIdQuiz'=> $nIdQuiz,'nIdUser' => $nIdUser] ); ?>', nCurrentPage);
            }
        }
    });
    
    
</script>