<?php $nCount =  isset($nQuestionNo) ? $nQuestionNo : 1;?>
@foreach($oQuestions as $oQuestion)
<div class="quiz-single-question pos-relative disp-single-question" id='show_question_{{ $nCount }}'>
    <span class="pull-left saved-answer-label">{{ trans('messages.saved') }}</span>
    <div class="clearfix text-center question-wrapper">
        <span class="disp-quiz-counter">{{ trans('messages.question') }} <span class="counter_cal">{{ $nCount }}</span></span>
            @if((isset($nEditable) && $nEditable == 1) || !isset($nEditable))
            <span class="pull-right quiz-edit-btn">
                <a href="javascript:void(0);" onclick="addQuizQuestion({{ $nCount }},{{ $oQuestion->id_quiz_question}})">{{ trans('messages.edit') }} </a>| 
                <a href="javascript:void(0);" class="ck-red-color" onclick="deleteQuestion({{ $nCount }},{{ $oQuestion->id_quiz_question}},'{{ trans('messages.delete_question_text_confirm')}}')">{{ trans('messages.delete') }}</a> 
            </span>
            @endif
            <span class="disp-quiz-points  pull-left pos-relative">{{ trans('messages.points') }} : 
                <span id='edit_marks_{{ $oQuestion->id_quiz_question}}'>{{ $oQuestion->marks }}</span>
                <span class="edit-points-label cursor-pointer">
                    {{ trans('messages.edit') }}
                </span>
                <span class="edit-points">
                    <span class="allot-marks-input arrow-up">
                    <form id="question_{{ $oQuestion->id_quiz_question}}">
                        <input type="hidden" name="id_question" value="{{ $oQuestion->id_quiz_question}}">
                        <button type="button" class="close close-allot-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <input type="text" placeholder="Enter marks" name="marks" class="form-control marks">
                        <button class="btn btn-primary" onclick="addQuestionMarks({{ $oQuestion->id_quiz_question }},'{{ route('quiz.quiz-question-edit-point')}}',event);">{{ trans('messages.save') }}</button>
                    </form>
                    </span>
                </span>
            </span>
            
        </div>
    <div class="disp-quiz-question">
        {{ $oQuestion->question_text }}
    </div>
    @if($oQuestion->question_type == config('constants.QUESTIONTYPEMULTIPLE'))
        @foreach($oQuestion->answer_options as $answerOption)
            <div no="{{ $answerOption->id_answer_option }}" class="disp-quiz-answer clearfix {{ ($answerOption->id_answer_option == $oQuestion->correct_answer )?'disp-correct-answer' :'' }}">
                {{ $answerOption->answer_text }}
                @if($answerOption->id_answer_option == $oQuestion->correct_answer )
                    <span class="disp-correct-answer pull-right">
                        <img class="" src="{{asset('assets/web/img/coorect-answer.png')}}">
                    </span>
                @endif
            </div>
        @endforeach
    @else
    <div class="disp-openended-answer">
        {{ trans('messages.question_type_openended') }}
    </div>
    @endif
    <?php $nCount++ ;?>
</div>  
@endforeach
<script type="text/javascript">
    $(".edit-points-label").click(function(){
        $(this).siblings(".edit-points").children(".allot-marks-input").css("display","block");
    });
    $(".close-allot-btn").click(function(){
        $(this).parents(".allot-marks-input").css("display","none");
    });
    
</script>
