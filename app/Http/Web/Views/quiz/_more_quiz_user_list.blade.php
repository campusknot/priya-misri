@foreach($oUserList as $UserList)
    <?php
        $sOnclick = $UserList->completed != 0 ? "openQuizSlider('". route('quiz.student-completed-quiz',['nIdQuiz'=> $UserList->id_quiz,'nIdUser' => $UserList->id_user]) ."');" : '';
    ?>
    <ul class="quiz-user-list clearfix" onclick="{{ $sOnclick }}">
        <li class="display-ib col-lg-5 col-md-5 col-sm-5 col-xs-5 ">
            <span class="icon-image img-35" data-toggle="tooltip" title="">
                {!! setProfileImage("50",$UserList->user_profile_image, $UserList->id_user, $UserList->first_name, $UserList->last_name) !!}
            </span>
            <span class="person-name display-ib v-align-top ">
                <span class="font-w-500">{{ $UserList->first_name.' '.$UserList->last_name }}</span>
                <br>
                <span>({{ $UserList->email}})</span>
            
            </span>
        </li>
        <li class="display-ib col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
            {{ ($UserList->mark_obtain != NULL || $UserList->completed != 0) ?  (!empty($UserList->mark_obtain) ? $UserList->mark_obtain.' pt' : '0 pt' ) : '' }}
        </li>
        @if($UserList->completed != 0)
            <li class="display-ib col-lg-4 col-md-4 col-sm-4 col-xs-4 cursor-pointer text-center pos-relative">
                <span class="">    {{ trans('messages.completed') }}</span>

                @if($UserList->marks_remaining != 0)
                    <span class="show-marks-alert ">
                        <img class="show-alert-img v-align-top" src="{{asset('assets/web/img/yellow-alert.png')}}" alt="" />
                        <span class="show-alert-text arrow-up">{{ trans('messages.allot_marks_for_open_ended_question') }}</span>
                    </span>
                @endif
            </li>
        @else
            <li class="display-ib col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">
                <span>{{ trans('messages.not_attempted') }}</span>
            </li>
        @endif
    </ul>
@endforeach