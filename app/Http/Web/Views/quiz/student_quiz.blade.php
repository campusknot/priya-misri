@extends('layouts.web.quiz_submit_layout')

@section('title', 'Campusknot-Quiz')

@section('content')
<?php 
    $dCurrentTime = Carbon::Now(Auth::user()->timezone);
    $dStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $oQuiz->start_time);
    $dEndTime = Carbon::createFromFormat('Y-m-d H:i:s', $oQuiz->end_time);
    
    $dStartTime->setTimezone(Auth::user()->timezone);
    $dEndTime->setTimezone(Auth::user()->timezone);
?>
    <div class="Col-lg-3 col-md-3 col-sm-3 col-xs-3 ">
        <div class="question-count-list hidden">
            <div class="question-count-label">{{ trans('messages.question') }}</div>
            <ul class="clearfix">
                @for($nQuestionIndex = 1; $nQuestionIndex <= $nQuizQuestionCount; $nQuestionIndex++)
                <li id="index_{{$nQuestionIndex}}" data-id="q_{{$nQuestionIndex}}">
                    {{$nQuestionIndex}}
                </li>
                <!--<li class="attempted">2</li>-->
                @endfor
            </ul>
        </div>
        
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="quiz-submit ck-background-white">
            <div class="qs-title-bar">
                {!! $oQuiz->quiz_title !!}
                <span class="pull-right quiz-card-right-section">
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/duration.png')}}"></span>{!! $oQuiz->duration !!}mins
                    <span class="quiz-card-points qs-points">{!! $oQuiz->total_marks !!}pt</span>
                </span>
            </div>
            <div class="qs-information">
                <p class="center-block">
                    {!! $oQuiz->description !!}
                </p>
                <div class="qs-div-margin">
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/faculty-icon.png')}}"></span>
                    {!! $oQuiz->first_name !!} {!! $oQuiz->last_name !!}
                </div>
                <div class="qs-div-margin">
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-group.png')}}"></span>
                    {!! $oQuiz->group_name !!}
                </div>
                <div class="qs-div-margin">
                    <?php
                            $dQuizStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $oQuiz->start_time);
                            $dQuizEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $oQuiz->end_time);
                            $dQuizStartDate->setTimezone(Auth::user()->timezone);
                            $dQuizEndDate->setTimezone(Auth::user()->timezone);
                        ?>
                    <span class="quiz-card-icon"><img class="" src="{{asset('assets/web/img/event-date.png')}}"></span>
                    <span class="text-uppercase font-w-500">{{ trans('messages.start') }}</span> : {{$dQuizStartDate->format('M j')}}, {{$dQuizStartDate->format('g:i A')}} |  <span class="text-uppercase font-w-500">{{ trans('messages.end') }}</span> : {{$dQuizEndDate->format('M j')}}, {{$dQuizEndDate->format('g:i A')}}
                    
                </div>
                @if($dStartTime < $dCurrentTime && $dEndTime > $dCurrentTime)
                    <button class="btn btn-primary md-center-button qs-div-margin qs-btn-start" id="start_quiz">
                        {{ trans('messages.start_quiz') }}
                    </button>
                @else
                    <button class="btn btn-primary md-center-button qs-div-margin qs-btn-start disabled" disabled="disabled">
                        {{ trans('messages.start_quiz') }}
                    </button>
                @endif
            </div>
            <div class="qs-quiz-list clearfix" id="qs-quiz-list">
            </div>
        </div>
    </div>
    <div class="Col-lg-3 col-md-3 col-sm-3 col-xs-3">
        <div class="quiz-counter-parent">
            <div class="quiz-counter-label">{{ trans('messages.time_left') }}</div>
            <div class="quiz-counter">
                <span id="timeDiffSec" class="hidden">{{ $dTimeDiff }}</span>
                <span class="numbers"><span id="hrs"></span> :<span class="number-label">hrs</span></span>
                <span class="numbers"><span id="mins"></span> :<span class="number-label">mins</span></span>
                <span class="numbers"><span id="sec"></span><span class="number-label">secs</span></span>
            </div>
            <div class="qs-end-time">
                {{ trans('messages.your_quiz_end') }} <span id="qs-end-time" >{{ $dEndtime }}</span>
            </div>
            <button class="btn btn-primary md-center-button qs-div-margin qs-btn-end disabled end_quiz" disabled="disabled">
                {{ trans('messages.end_quiz') }}
            </button>
        </div>
        
    </div>
<script type="text/javascript">
     
<?php if($dStartTime < $dCurrentTime && $dEndTime > $dCurrentTime) { ?>
    var nCurrentPage = 1;
    var quizTimer;
    var nLastPage = <?php echo $aQuizQuestions->lastPage(); ?>;
    
    function autoPaginate()
    {          
        setTimeout(function () {   
            nCurrentPage++;
            loadNewData('qs-quiz-list', '<?php echo route('quiz.quiz-question',['nIdQuizUserInvite'=> $nIdQuizUserInvite] ); ?>', nCurrentPage);
            if (nCurrentPage < nLastPage) autoPaginate(); // iteration counter
        }, 500);
    }
    
    function startQuizTimer(){
        var hrs = $("#hrs").text();
        var mins = $("#mins").text();
        var secs = $("#sec").text();
        quizTimer = setInterval(function() {
            
            document.getElementById("hrs").innerHTML =  ('0' + hrs).slice(-2);
            document.getElementById("mins").innerHTML = ('0' + mins).slice(-2)
            document.getElementById("sec").innerHTML = ('0' + secs).slice(-2)
            secs--;
            if(mins > 0 || secs > 0 || hrs > 0 ){
                if(secs < 1 && mins >= 0){
                    mins--;
                    secs = 59;
                }
                if(mins< 1 && hrs > 0)
                {
                        hrs--;
                    mins=59;
                }
            }
            else{
                //document.getElementById("demo").innerHTML ="Time Yp" ;
                document.getElementById("sec").innerHTML = 00;
                clearInterval(quizTimer); 
                endQuiz();
            }
        }, 1000);
    }
    
    $('#start_quiz').click(function(){
        $.ajax({
            type: "GET",
            url: siteUrl + '/quiz/quiz-start/'+{{ $nIdQuizUserInvite }},
            success: function (data) {
                if(data.success == true)
                {
                    //debugger;
                    $('#qs-quiz-list').append(data.html);
                    var d = data.timeDiff;
                    $('.question-count-list').removeClass('hidden');
                    $('#qs-end-time').text(data.dEndQuizTime);
                    $('.end_quiz').prop('disabled', false);
                    $('#start_quiz').remove();
                    $('.end_quiz').removeClass('disabled');
                    getTimeFromSec(d);
                    autoPaginate();
                    startQuizTimer();
                }
            },
            error: function (data) {
                if(data.status == 401)
                {
                    window.location = siteUrl + '/home';
                }
                console.log('Error:', data);
            }
        });
    });
    $('.end_quiz').click(function(){
        swal({
            title: "",
            text: "{{ trans('messages.confirm_end_quiz')}} ",//"You will not be able to recover this imaginary file!",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: 'Yes',
            cancelButtonText: 'Cancel',
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm)
        {
            if (isConfirm) {
                endQuiz();
            }
        });
    });
    
    function endQuiz()
    {
        
        $.ajax({
            type: "GET",
            url: siteUrl + '/quiz/quiz-end/'+{{ $nIdQuizUserInvite }},
            success: function (data) {
                if(data == 'success')
                {
                    $('#qs-quiz-list').html('');
                    $('#qs-quiz-list').html('Your quiz submitted successfully');
                    $('#qs-quiz-list').addClass("quiz-submit-success");
                    $('.end_quiz').prop('disabled',true);
                    $('.end_quiz').addClass('disabled');
                    $('.end_quiz').removeClass('end_quiz');
                    clearInterval(quizTimer); 
                }
            }
        });
    }
<?php } ?>    
    callAutoSizeTextArea();
    setAutosizeTextAreaHeight(34);
    $(document).ready(function(){
        var diff = $('#timeDiffSec').text();
        getTimeFromSec(diff);
    });
    //scroll to question when clicked on the number
    $(".question-count-list ul li").click(function(){
        var scrollTo = $(this).attr('data-id');
        var targetDiv = '#'+scrollTo;
        $('html, body').animate({
            scrollTop: ($(targetDiv).offset().top ) - 200
        }, 100);
        $(targetDiv).parent().parent().siblings().removeClass("active-question");
        $(targetDiv).parent().parent().addClass("active-question");


    });
    function getTimeFromSec(d){
        var h = ('0' +Math.floor(d / 3600)).slice(-2);
        var m = ('0' +Math.floor(d % 3600 / 60)).slice(-2);
        var s = ('0' + Math.floor(d % 3600 % 60)).slice(-2);
         

        var hDisplay = h > 0 ? h : '00';
        var mDisplay = m > 0 ? m : '00';
        var sDisplay = s > 0 ? s : "00";
        $('#hrs').text(hDisplay);
        $('#mins').text(mDisplay);
        $('#sec').text(sDisplay);
    }
    function addActiveClass(ele){
         $(ele).siblings().removeClass("active-question");
         $(ele).addClass("active-question");
    }
    
      $(".ck-submit-quiz").bind("contextmenu",function(e){
     e.preventDefault();//or return false;
    });
    $('.ck-submit-quiz').bind('copy paste', function(e) {
        e.preventDefault();
    });
    document.onkeydown = function(e) {
        if(event.keyCode == 123) {
           return false;
        }
        if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
           return false;
        }
        if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
           return false;
        }
        if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
           return false;
        }
    }
 
  
</script>
@stop
