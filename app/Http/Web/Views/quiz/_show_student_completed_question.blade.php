<?php $nCount = $oQuestions->firstItem();?>
@foreach($oQuestions as $oQuestion)
<div class="quiz-single-question pos-relative disp-single-question" id='show_question_{{ $nCount }}'>
    <span class="pull-left saved-answer-label">{{ trans('messages.saved') }}</span>
    <div class="clearfix text-center question-wrapper">
            <span class="disp-quiz-counter">{{ trans('messages.question') }} {{ $nCount }}</span>
            <span class="disp-quiz-points  pull-left">{{ trans('messages.points') }} : {{ $oQuestion->marks }} </span>
            <span class="pull-right alloted_marks">{{ $oQuestion->mark_obtain }}</span>
            <span class="pull-right allot-marks">{{ trans('messages.allot_marks')}} : &nbsp;</span>
            @if($oQuestion->question_type == config('constants.QUESTIONTYPEOPEN'))
                <span class="allot-marks-input arrow-up">
                    <form id="form_{{ $nCount }}" onsubmit="addMarks({{ $nCount }},this);" >
                        <input type="hidden" name="id_question" value="{{ $oQuestion->id_quiz_question }}"/>
                        <input type="hidden" name="id_user" value="{{ $nIdUser }}"/>
                        <button type="button" class="close close-allot-btn" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <input type="text" placeholder="Enter marks" name="marks" class="form-control marks"/>
                        <button class="btn btn-primary">{{ trans('messages.save') }}</button>
                    </form>
                </span>
            @endif
        </div>
    <div class="disp-quiz-question">
        {{ $oQuestion->question_text }}
    </div>
    @if($oQuestion->question_type == config('constants.QUESTIONTYPEMULTIPLE'))
        @foreach($oQuestion->answer_options as $answerOption)
            <?php $sClass=''; ?>
            @if($answerOption->id_answer_option == $oQuestion->correct_answer)
                <?php $sClass = 'disp-correct-answer'; ?>
            @elseif($answerOption->id_answer_option == $oQuestion->user_answer)
                <?php $sClass = 'disp-incorrect-answer'; ?>
            @endif
            <div no="{{ $answerOption->id_answer_option }}" class="disp-quiz-answer clearfix {{ $sClass }}">
                {{ $answerOption->answer_text }}
                @if($answerOption->id_answer_option == $oQuestion->correct_answer )
                    <span class="disp-correct-answer pull-right">
                        <img class="" src="{{asset('assets/web/img/coorect-answer.png')}}">
                    </span>
                @elseif($answerOption->id_answer_option == $oQuestion->user_answer)
                    <span class="disp-incorrect-answer pull-right">
                        <img class="" src="{{asset('assets/web/img/incorrect-answer.png')}}">
                    </span>
                @endif
            </div>
        @endforeach
    @else
    <div class="disp-openended-attended-answer">
        {{ $oQuestion->user_answer_description }}
    </div>
    @endif
    <?php $nCount++ ;?>
</div>  
@endforeach
<script type="text/javascript">
    $('.allot-marks').on('click', function(){
        $(this).siblings(".allot-marks-input").css("display","block");
    });
    $('.close-allot-btn').on('click', function(){
        $(this).parent().parent(".allot-marks-input").css("display","none");
    });
</script>