<?php $nCount = $oQuestions->firstItem();?>
<!-- Filename:: quiz._student_completed_question -->
@foreach($oQuestions as $oQuestion)
<div class="quiz-single-question pos-relative disp-single-question" id='show_question_{{ $nCount }}'>
    <div class="clearfix text-center">
            <span class="disp-quiz-counter">{{ trans('messages.question') }} {{ $nCount }}</span>
            <span class="disp-quiz-points  pull-left">{{ trans('messages.points') }} : {{ $oQuestion->marks }} </span>
            <span class="pull-right">{{ $oQuestion->mark_obtain }}</span>
            <span class="pull-right">{{ trans('messages.allot_marks')}} : &nbsp;</span>
        </div>
    <div class="disp-quiz-question">
        {{ $oQuestion->question_text }}
    </div>
    @if($oQuestion->question_type == config('constants.QUESTIONTYPEMULTIPLE'))
        @foreach($oQuestion->answer_options as $answerOption)
            <?php $sClass=''; ?>
            @if($answerOption->id_answer_option == $oQuestion->correct_answer)
                <?php $sClass = 'disp-correct-answer'; ?>
            @elseif($answerOption->id_answer_option == $oQuestion->user_answer)
                <?php $sClass = 'disp-incorrect-answer'; ?>
            @endif
            <div no="{{ $answerOption->id_answer_option }}" class="disp-quiz-answer clearfix {{ $sClass }}">
                {{ $answerOption->answer_text }}
                @if($answerOption->id_answer_option == $oQuestion->correct_answer )
                    <span class="disp-correct-answer pull-right">
                        <img class="" src="{{asset('assets/web/img/coorect-answer.png')}}">
                    </span>
                @elseif($answerOption->id_answer_option == $oQuestion->user_answer)
                    <span class="disp-incorrect-answer pull-right">
                        <img class="" src="{{asset('assets/web/img/incorrect-answer.png')}}">
                    </span>
                @endif
            </div>
        @endforeach
    @else
    <div class="disp-openended-answer">
        {{ $oQuestion->user_answer_description }}
    </div>
    @endif
    <?php $nCount++ ;?>
</div>  
@endforeach
<script type="text/javascript">
    $('.allot-marks').on('click', function(){
        $(this).siblings(".allot-marks-input").css("display","block");
    });
    $('.close-allot-btn').on('click', function(){
        $(this).parent().parent(".allot-marks-input").css("display","none");
    });
</script>