<?php
    $nIdQuiz = isset($oQuiz->id_quiz) ? $oQuiz->id_quiz : old('id_quiz');
    $sQuizTitle = isset($oQuiz->quiz_title) ? $oQuiz->quiz_title : old('quiz_title');
    $sQuizDescription = isset($oQuiz->description) ? $oQuiz->description : old('description');
    $nQuizTotalMark = isset($oQuiz->total_marks) ? $oQuiz->total_marks : old('total_marks');
    $nIdGroup = isset($oQuiz->id_group) ? $oQuiz->id_group : old('group_name');
    $nQuizType = (isset($oQuiz->activated) && $oQuiz->activated == 0) ? 3 : old('quiz_type');
    $nQuizDuration = isset($oQuiz->duration) ? $oQuiz->duration : old('duration');
    $sStartDate = isset($oQuiz->start_time) ? $oQuiz->start_time : (mb_strlen(old('start_date')) ? old('start_date') : '');
    $sEndDate = isset($oQuiz->end_time) ? $oQuiz->end_time : (mb_strlen(old('end_date')) ? old('end_date') : '');
    $sShowMobile = isset($oQuiz->show_in_mobile) ? $oQuiz->show_in_mobile : (mb_strlen(old('show_mobile')) ? old('show_mobile') : '');
    $sDisplayStartDate = '';
    $sStartHours = (mb_strlen(old('start_hour')) ? old('start_hour') : '');
    $sStartMinutes = (mb_strlen(old('start_minute')) ? old('start_minute') : '');
    $sStartAMPM = 'AM';
    $sEndAMPM = 'AM';
    $nEditable = ((!empty($nIdQuiz) && isset($oQuiz) && $oQuiz->start_time > Carbon::Now()) || (isset($oQuiz) && $oQuiz->activated == 0) || empty($nIdQuiz)) ? 1 : 0;

    if(!empty($sStartDate))
    {
        $dStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $sStartDate);
        $dStartDate->setTimezone(Auth::user()->timezone);
        $sDisplayStartDate = $dStartDate->format('m-d-Y');
        $sStartTime = $dStartDate->toTimeString();
        $aStartTime = explode(":", $sStartTime);
        $sStartHours = $aStartTime[0];
        if($sStartHours > 12)
        {
            $sStartHours = $sStartHours - 12;
            $sStartAMPM = 'PM';
        }
        if($sStartHours == 12)
            $sStartAMPM = 'PM';
        elseif($sStartHours == 00)
            $sStartHours=12;
        $sStartMinutes = $aStartTime[1];
    }


    $sDisplayEndDate = '';
    $sEndHours = (mb_strlen(old('end_hour')) ? old('end_hour') : '');
    $sEndMinutes = (mb_strlen(old('end_minute')) ? old('end_minute') : '');

    if(mb_strlen($sEndDate))
    {
        $dEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $sEndDate);
        $dEndDate->setTimezone(Auth::user()->timezone);
        $sDisplayEndDate = $dEndDate->format('m-d-Y');
        $sEndTime = $dEndDate->toTimeString();
        $aEndTime = explode(":", $sEndTime);
        $sEndHours = $aEndTime[0];
        if($sEndHours > 12)
        {
            $sEndHours = $sEndHours - 12;
            $sEndAMPM = 'PM';
        }
        if($sEndHours == 12)
            $sEndAMPM = 'PM';
        elseif($sEndHours == 00)
            $sEndHours=12;
            
        $sEndMinutes = $aEndTime[1];
    }
?>
<span class="pull-left saved-answer-label">{{ trans('messages.saved') }}</span>
<div class="add-quiz-info">  
    <form id="form_add_quiz" class="form-horizontal" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="form-group {{ ($errors->has('quiz_title')) ? 'has-error' : ''}}">
            <input type="text" class="form-control" placeholder="{{ trans('messages.quiz_name') }}" name="quiz_title" title="{{($errors->has('quiz_title')) ? $errors->first('quiz_title') : '' }}" value="{{$sQuizTitle}}"/>
        </div>
        <div class="form-group">
            <textarea class="form-control" placeholder="{{ trans('messages.quiz_information') }}" name="description" >{{ $sQuizDescription }}</textarea>
        </div>
        <div class="form-group">
            <span class="quiz-date label-input-parent {{ ($errors->has('duration')) ? 'error' : ''}} pos-relative">
                <label>{{ trans('messages.quiz_duration') }}</label>
                <input type="text" class=" form-control " placeholder="{{ trans('messages.mins') }}" name="duration" title="{{($errors->has('duration')) ? $errors->first('duration') : '' }}" value="{{$nQuizDuration}}" />
                <span class="quiz-mins-label">{{ trans('messages.mins') }}</span>
            </span>
            <span class="quiz-date pull-right label-input-parent {{ ($errors->has('total_marks')) ? 'error' : ''}}">
                <label>{{ trans('messages.quiz_points') }}</label>
                <input type="text" class=" form-control" placeholder="" name="total_marks" title="{{($errors->has('total_marks')) ? $errors->first('total_marks') : '' }}" value="{{$nQuizTotalMark}}" />
            </span>


        </div> 
        <div class="form-group pos-relative {{ ($errors->has('group_name')) ? 'has-error' : ''}}">
            <span class=" caret caret-right"></span>
            <select class="form-control" name="group_name" id="id_group">
                <option value="">{{ trans('messages.select_group') }}</option>
                @foreach($oGroupList as $oGroup)
                    <?php ($oGroup->id_group == $nIdGroup) ? $selected = "selected = 'selected'" : $selected = ''; ?>
                    <option value="{{ $oGroup->id_group }}" {{ $selected }}>
                        {{ $oGroup->group_name }}
                        @if($oGroup->section)
                            {{ trans('messages.section')}}- {{ $oGroup->section }} ({{ $oGroup->semester }})
                        @endif
                    </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <div class="form-control mobile-quiz-chk no-padding cursor-pointer">
                <input type="checkbox" class=" hidden"  id="mobile_quiz_checkbox"
                       name="show_mobile" value="1" {{ ($sShowMobile && $sShowMobile == 1) ? 'checked' : ''}} />
                <label for="mobile_quiz_checkbox">
                    <img src="{{asset('assets/web/img/mobile_quiz.png')}}" alt=""/>
                    {{ trans('messages.show_in_mobile') }}
                    <img src="{{asset('assets/web/img/check.png')}}" class="pull-right quiz-checkmark" alt=""/>
                    
                </label>
            </div>
                 
            
        </div>
        <div class="form-group pos-relative">
            <span class=" caret caret-right"></span>
            <select class="form-control" name="quiz_type" id="quiz_type" onchange="changeOption('quiz_type');return false;">
                    <option value="1" >{{trans('messages.schedule_quiz') }}</option>
<!--                    <option value="2" {{ ($nQuizType == 2) ? "selected = 'selected'" : '' }} >Current Quiz</option>-->
                    <option value="3" {{ ($nQuizType == 3) ? "selected = 'selected'" : '' }} >Save as Draft</option>
            </select>
        </div>
        
        <div class="form-group clearfix">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding quiz-date {{ ($nQuizType == 2 || $nQuizType == 3) ? 'hidden':''}}" id="start_quiz_date">
                <div class="datepicker {{ ($errors->has('start_date') || $errors->has('start_hour') || $errors->has('start_minute')) ? 'error' : ''}}">
                <input type="text" id="add_start_date" name="start_date" title="{{ ($errors->has('start_date')) ? $errors->first('start_date') : ''}}" placeholder="{{trans('messages.start_date_quiz')}}" value="{{$sDisplayStartDate}}" data-toggle="tooltip" data-placement="top" >
                    <select class="time-hour" name="start_hour" title="{{ ($errors->has('start_hour')) ? $errors->first('start_hour') : ''}}" data-toggle="tooltip" data-placement="top">
                       <option value=''>hh</option>
                        @for($nStartHour = 1 ; $nStartHour <=12 ; $nStartHour++)
                        <option value='{{ $nStartHour }}' {{ $nStartHour == $sStartHours  ? "selected":"" }}>{{ $nStartHour }}</option>
                        @endfor
                    </select>
                    <select class="time-minutes" name="start_minute" title="{{($errors->has('start_minute')) ? $errors->first('start_minute') : '' }}" data-toggle="tooltip" data-placement="top">
                        <option value="">mm</option>
                        <option value='00' <?php echo $sStartMinutes==00?"selected":"" ?>>00</option>
                        <option value='05' <?php echo $sStartMinutes==05?"selected":"" ?>>05</option>
                        @for($nStartMin = 10 ; $nStartMin <=55 ; $nStartMin = $nStartMin+5)
                            <option value='{{ $nStartMin }}' {{ $nStartMin == $sStartMinutes  ? "selected":"" }}>{{ $nStartMin }}</option>
                        @endfor
                    </select>
                    <input id="time_start_am" type="radio" class="hidden" name="start_ampm" value="am" {{ ($sStartAMPM == 'AM') ? 'checked' : '' }} >
                    <input id="time_start_pm" type="radio" class="hidden" name="start_ampm" value="pm" {{ ($sStartAMPM == 'PM') ? 'checked' : '' }} >
                    <label for="time_start_am" class="time-start-am">{{ trans('messages.am') }}</label>
                    <label for="time_start_pm" class="time-start-pm">{{ trans('messages.pm') }}</label>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding quiz-date pull-right {{ ($nQuizType == 3) ? 'hidden':'' }}" id="end_quiz_date">
                <div class="datepicker pos-relative {{ ($errors->has('end_date') || $errors->has('end_hour') || $errors->has('end_minute')) ? 'error' : ''}}">
                    <input id="add_end_date" type="text" title="{{($errors->has('end_date')) ? $errors->first('end_date') : '' }}" name="end_date" placeholder="{{trans('messages.end_date_quiz')}}" data-toggle="tooltip" data-placement="top" value="{{ $sDisplayEndDate }}">
                    <select required class="time-hour" name="end_hour" title="{{ ($errors->has('start_hour')) ? $errors->first('start_hour') : ''}}" data-toggle="tooltip" data-placement="top" >
                        <option value=''>hh</option>
                        @for($nEndHour = 1 ; $nEndHour <=12 ; $nEndHour++)
                        <option value='{{ $nEndHour }}' {{ $nEndHour == $sEndHours  ? "selected":"" }}>{{ $nEndHour }}</option>
                        @endfor
                    </select>
                    <select required class="time-minutes " name="end_minute" title="{{($errors->has('end_minute')) ? $errors->first('end_minute') : '' }}" data-toggle="tooltip" data-placement="top" >
                        <option value=''>mm</option>
                        <option value='00' <?php echo $sEndMinutes==00?"selected":"" ?>>00</option>
                        <option value='05' <?php echo $sEndMinutes==05?"selected":"" ?>>05</option>
                        @for($nEndMin = 10 ; $nEndMin <=55 ; $nEndMin = $nEndMin+5)
                            <option value='{{ $nEndMin }}' {{ $nEndMin == $sEndMinutes  ? "selected":"" }}>{{ $nEndMin }}</option>
                            @endfor
                    </select>
                    <input type="radio" class="hidden" name="end_ampm" value="am" id="time_end_am" checked > 
                    <input type="radio" class="hidden" name="end_ampm" value="pm" id="time_end_pm" {{ ($sEndAMPM == 'PM') ? 'checked' : '' }} > 

                    <label for="time_end_am" class="time-end-am">{{ trans('messages.am') }}</label>
                    <label for="time_end_pm" class="time-end-pm">{{ trans('messages.pm') }}</label>
                </div>
            </div>
        </div>
        <input type="hidden" name="id_quiz" id="id_quiz_add" value="{{$nIdQuiz}}"/>    

        @if($nEditable == 1)
        <div class="">
            <input type="button" class="btn btn-primary lg-center-button" onclick="submitFormQuiz('form_add_quiz', '{{ route('quiz.quiz-add') }}', this ,'quiz')" value="Save" />
        </div>
        @endif
    </form>
    <script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip();   
       
     $("#add_start_date").datepicker({
        dateFormat: 'mm-dd-yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-10:c+3',
        minDate: 0,
        showButtonPanel: false,
        onSelect: function (date) {

            var endDatePicker = $('#add_end_date');
            var startDate = $(this).datepicker('getDate');
            var minDate = $(this).datepicker('getDate');
           // endDatePicker.datepicker('setDate', minDate);
            startDate.setDate(startDate.getDate() + 30);
            //sets endDatePicker maxDate to the last day of 30 days window
            endDatePicker.datepicker('option', 'maxDate', startDate);
            endDatePicker.datepicker('option', 'minDate', minDate);
            $(this).datepicker('option', 'minDate', new Date());
        }
    });

    $( "#add_end_date" ).datepicker({
        dateFormat: 'mm-dd-yy',
        changeYear: true,
        changeMonth: true,
        yearRange: 'c-10:c+3',
        showButtonPanel: false,
        minDate: 0,
    });
    
    function changeOption(nQuizTypeId)
    {
        var nQuizType = $('#'+nQuizTypeId).val();
        $('#end_quiz_date').addClass('pull-right');
        if(nQuizType == 3)
        {
            $('#start_quiz_date').addClass('hidden');
            $('#end_quiz_date').addClass('hidden');
        }
        else if(nQuizType == 2)
        {
            $('#start_quiz_date').addClass('hidden');
            $('#end_quiz_date').removeClass('hidden');
            $('#end_quiz_date').removeClass('pull-right');
        }
        else
        {
            $('#start_quiz_date').removeClass('hidden');
            $('#end_quiz_date').removeClass('hidden');
        }
    }
    </script>
</div>