<div class="clearfix {{ (Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD')) ? 'drop_doc' : '' }}">
    <ul class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix  docs-top-heading-bar ">
        <li> <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.shared-document-ajax') }}','{{ route('documents.shared-document') }}');" >{{ trans('messages.shared_documents') }}</a></li>
        @if(count($aBreadCrumb))
        <?php 
            $isPermission=0; ?>

            @foreach($aBreadCrumb as $breadcrumb)
                @if($isPermission == 1 || $breadcrumb->permission_type != '')
                    <?php 
                    $isPermission=1; 
                    ?>
                <li><a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.shared-document-ajax',$breadcrumb->id_document) }}','{{ route('documents.shared-document',$breadcrumb->id_document) }}');" >{{ $breadcrumb->document_name }}</a></li>
                @endif
            @endforeach
        @endif
    </ul>
    <form name="fileupload" id="fileupload" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <input type="hidden" name="parent_id" id="parent_id" value="{{ $nIdParent }}" />
        <input type="hidden" name="current_permission" value="{{ (Session::has('current_permission')) ? Session::get('current_permission') : 'R'}}" />
        <input type="hidden" name="doc_type" id="doc_type" value="" />
        <input type="hidden" name="folder_name" id="fol_name" value="" />
        <input type="hidden" name="id_document" id="id_document" value="" />
        <input type="file" name="file" id="file_upload" class="hide" /> 
    </form>
    
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="group_search" name="group_search" onsubmit="searchDocs('{{ route('documents.shared-document-ajax',['nDocumentId' => $nIdParent]) }}','{{ route('documents.shared-document',['nDocumentId' => $nIdParent]) }}','search_str','document_list');return false">
            <div class="input-group docs-search">                
                <input id="search_str" type="text" class="form-control" name="search_str"  placeholder="{{ trans('messages.search_document') }}" value="{{ ($sSearchStr) ? $sSearchStr : '' }}" >
                <button type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>              
            </div>
        </form>
    </div>
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 document-list_my-docs">
        
    </div>
    
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 document-list_my-docs">
        <ul class="my-docs_document-title clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                {{ trans('messages.name') }}
                <span class="column-sorting-options document_name" onclick="getDocsSort('document_name','{{ route('documents.shared-document-ajax',['nDocumentId' => $nIdParent]) }}','document_list');">
                    <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/asc.png')}}">
                    </span>
                    <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
                <span>
                    {{ trans('messages.doc-owner') }}
                    <span class="column-sorting-options first_name" onclick="getDocsSort('first_name','{{ route('documents.shared-document-ajax',['nDocumentId' => $nIdParent]) }}','document_list');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
                <span class="pull-right">
                    {{ trans('messages.date') }}
                    <span class="column-sorting-options updated_at" onclick="getDocsSort('updated_at','{{ route('documents.shared-document-ajax',['nDocumentId' => $nIdParent]) }}','document_list');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">{{ trans('messages.share_with') }}</li>
            
            
            <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">{{ trans('messages.size') }}</li>-->
        </ul>
        <div class="document-list-parent clearfix" id="<?php ($nIdParent != '') ? $nIdParent : 'more_document'; ?>">
             <div class="file-upload-loader">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div class="file-upload-error">
                <div class="error-message"></div>
            </div>
            @if(count($oSharedDocument))
                <?php $right=0; ?>
                @foreach($oSharedDocument as $oDocument)

                <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12" id='{{ $oDocument->id_document }}'>
                    <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name clearfix">
                        @if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
                            <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.shared-document-ajax',$oDocument->id_document) }}','{{ route('documents.shared-document',$oDocument->id_document) }}');">                        
                                <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ $oDocument->document_name }}                       
                            </a>
                        @else
                            <a target="_blank" href="{{route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" >                        
                                <?php
                                $aFileNameData = explode('.', $oDocument->file_name);

                                //Div for file icon
                                if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                }
                                elseif (in_array(end($aFileNameData), array('zip','rar')))
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                }
                                elseif (end($aFileNameData) == 'pdf')
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                }
                                else
                                {
                                    $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/image.png').'">';
                                }
                                echo $sHtmlString;
                                ?>
                                {{ $oDocument->document_name }}                       
                            </a>
                        @endif
                    </li>
                     <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
                        <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{  $oDocument->last_name}}">
                            {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
                        </span>
                         <span class=" pull-right">
                            <span class="doc-date">{{ $oDocument->updated_at->format('m/d/Y') }}</span>
                            <span class="folder-action pull-right">
                                @if($oDocument->document_type != config('constants.DOCUMENTTYPEFOLDER'))
                                    <a target="_blank" href="{{route('utility.download-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" > 
                                        <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.download')}}">
                                    </a>
                                @endif
                                @if($oDocument->id_user == Auth::user()->id_user)
                                    @if($oDocument->shared_with != config('constants.DOCUMENTSHAREDWITHGROUP'))
                                        <img class="" src="{{asset('assets/web/img/doc-share-icon.png')}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});" data-toggle="tooltip" title="{{ trans('messages.share')}}">
                                    @endif
                                    <img class="doc-rename" src="{{asset('assets/web/img/doc-rename-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.rename')}}"> 
                                @endif
                                
                            </span>
                         </span>
                    </li>
                    <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        @if($oDocument->count_user > 3)
                        <span class="img-25 pull-right more-shared-with-user" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                            +
                        </span>
                        <ul class="more-shared-with-user-listing">
                        </ul>
                        @endif
                        @if(count($oDocument->group_document))
                            <span class="pull-right doc-admin">
                                <span class="img-25" data-toggle="tooltip" title="{{ $oDocument->group_document->group_name }}">
                                    {!! setGroupImage($oDocument->group_document->group_image_name, $oDocument->group_document->group_category_image) !!}
                                </span>
                            </span>
                        @elseif(count($oDocument->user_list))
                            @foreach($oDocument->user_list as $oUserList)
                            <span class="pull-right doc-admin">
                                <span class="img-25 multiple-user" data-toggle="tooltip" title="{{ $oUserList->first_name }} {{  $oUserList->last_name}}">
                                    {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
                                </span>
                            </span>
                            @endforeach
                        @endif                 
                    </li>
                   
                    <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>-->
                </ul>
                @endforeach
            @else
                <div class="large-padding no-data">
                    <img class="" src="{{asset('assets/web/img/no-documents.png')}}">
                    {{ trans('messages.no_documents') }}

                </div>
            
            @endif
        </div>

        <script type="text/javascript"> 
            
        $(document).ready(function(){
            var nCurrentPage = <?php echo $oSharedDocument->currentPage(); ?>;
            var nLastPage = <?php echo $oSharedDocument->lastPage(); ?>;
            $(window).scroll(function (event) {
                if(window.location.href == '<?php echo route('documents.shared-document',['nDocumentId' => $nIdParent]); ?>')
                {
                    if($(window).scrollTop() + $(window).height() == $(document).height()) {
                        if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                            nCurrentPage +=1;
                            loadNewData('<?php ($nIdParent != '') ? $nIdParent : 'more_document'; ?>', '<?php echo route('documents.shared-document',['nDocumentId' => $nIdParent]); ?>', nCurrentPage, '&order_by=<?php echo $sOrderBy; ?>&order_field=<?php echo $sOrderField; ?>&search_str='+$('#search_str').val());
                        }
                    }
                }
            });
            flagDoc = 0;
            $(document).on('focusout','.new-folder-name',function(e){   
                if(e.type == "focusout"){

                    if(!flagDoc){
                        CreateFolder(this,e,1);
                    }
                    else{
                        flagDoc = 0;
                    }
                }
            });

            $(document).on('keyup','.new-folder-name',function(e){
                e.preventDefault();
                if (e.keyCode == 13) {
                    flagDoc = 1;
                    CreateFolder(this,e,1);
                } 
            });

            $('.drop_doc').dropzone({ 
                headers:
                {
                    'X-CSRF-Token': $('input[name="_token"]').val()
                },
                url: "{{ route('documents.create-folder') }}" ,
                params : { parent_id : $('#parent_id').val(), doc_type : 'F'},
                success: function() {
                    location.reload();
                }
            });
            
            /*$('#search_str').keyup(function(){
                var text = $('#search_str').val();
                if(text.length > 2)
                searchDocs('<?php echo route('documents.shared-document-ajax',['nDocumentId' => $nIdParent]); ?>','<?php echo route('documents.shared-document',['nDocumentId' => $nIdParent]); ?>','search_str','document_list')
            });*/
        });

        showTooltip();
        </script>
    </div>
</div>