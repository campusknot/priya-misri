@if(count($oSharedDocument))
    @foreach($oSharedDocument as $oDocument)
    <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 ui-widget-content" id='{{ $oDocument->id_document }}'>
        <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name clearfix">
            @if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
                <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.shared-document-ajax',$oDocument->id_document) }}','{{ route('documents.shared-document',$oDocument->id_document) }}');">                        
                    <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ $oDocument->document_name }}                       
                </a>
            @else
                <a target="_blank" href="{{route('utility.view-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" >                        
                    <?php
                            $aFileNameData = explode('.', $oDocument->file_name);

                            //Div for file icon
                            if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                            }
                            elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                            }
                            elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                            }
                            elseif (in_array(end($aFileNameData), array('zip','rar')))
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                            }
                            elseif (end($aFileNameData) == 'pdf')
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                            }
                            else
                            {
                                $sHtmlString = '<img src="'.asset('assets/web/img/file-icons/image.png').'">';
                            }
                            echo $sHtmlString;
                            ?>
                    {{ $oDocument->document_name }}                       
                </a>
            @endif
        </li>
        <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
            <span class="img-25 doc-admin" data-toggle="tooltip" title="{{ $oDocument->first_name }} {{  $oDocument->last_name}}">
                {!! setProfileImage("50",$oDocument->user_profile_image, $oDocument->id_user, $oDocument->first_name, $oDocument->last_name) !!}
            </span>
            <span class="pull-right">
                <span class="doc-date">{{ $oDocument->updated_at->format('m/d/Y') }}</span>
                <span class="folder-action pull-right">
                    @if($oDocument->document_type != config('constants.DOCUMENTTYPEFOLDER'))
                    <a target="_blank" href="{{route('utility.download-file',['d_'.$oDocument->id_document,$oDocument->document_name]) }}" > 
                        <img class="doc-delete" src="{{asset('assets/web/img/doc-download-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.download')}}">
                    </a>
                    @endif

                    @if($oDocument->id_user == Auth::user()->id_user)
                        @if($oDocument->shared_with != config('constants.DOCUMENTSHAREDWITHGROUP'))
                            <img class="" src="{{asset('assets/web/img/doc-share-icon.png')}}" onclick="showDocsShareLightBox({{ $oDocument->id_document }});" data-toggle="tooltip" title="{{ trans('messages.share')}}">
                        @endif
                        <img class="doc-rename" src="{{asset('assets/web/img/doc-rename-icon.png')}}" data-toggle="tooltip" title="{{ trans('messages.rename')}}"> 
                    @endif
                </span>
            </span>
        </li>
        <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
            @if($oDocument->count_user > 3)
            <span class="img-25 pull-right more-shared-with-user" onclick="showDocsShareLightBox({{ $oDocument->id_document }});">
                +
            </span>
            <ul class="more-shared-with-user-listing">
            </ul>
            @endif
            @if(count($oDocument->group_document))
                <span class="pull-right doc-admin">
                    <span class="img-25" data-toggle="tooltip" title="{{ $oDocument->group_document->group_name }}">
                        {!! setGroupImage($oDocument->group_document->group_image_name, $oDocument->group_document->group_category_image) !!}
                    </span>
                </span>
            @elseif(count($oDocument->userList))
                @foreach($oDocument->userList as $oUserList)
                <span class="pull-right doc-admin">
                    <span class="img-25 multiple-user" data-toggle="tooltip" title="{{ $oUserList->first_name }} {{  $oUserList->last_name}}">
                        {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
                    </span>
                </span>
                @endforeach
            @endif
        </li> 
        <!--<li class="col-lg-1 col-md-1 col-sm-1 col-xs-1">--</li>-->
    </ul>
    @endforeach
@endif

<script type="text/javascript">
    showTooltip();
</script>