<div class="clearfix {{ (Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD')) ? 'drop_doc' : '' }}">
    <ul class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix  my-docs docs-top-heading-bar ">
        <li><a href="{{ route('documents.document-listing') }}">{{ trans('messages.my_documents') }}</a></li>
        @if(count($aBreadCrumb))
            <?php 
            $isPermission=0; ?>    
            @foreach($aBreadCrumb as $breadcrumb)
                @if($isPermission == 1 || ($breadcrumb->permission_type != '' && $breadcrumb->id_user == Auth::user()->id_user))
                    <?php 
                    $isPermission=1; 
                    ?>
                 <li><a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.document-listing-ajax',$breadcrumb->id_document) }}','{{ route('documents.document-listing',$breadcrumb->id_document) }}');" >{{ $breadcrumb->document_name }}</a></li>
                @endif
            @endforeach
        @endif
    </ul>
    <form name="fileupload" id="fileupload" enctype="multipart/form-data">
        {!! csrf_field() !!}

        <input type="hidden" name="parent_id" id="parent_id" value="{{ $nIdParent }}" />
        <input type="hidden" name="shared_with" id="shared_with" value="{{ $sSharedWith }}" />
        <input type="hidden" name="current_permission" value="{{ (Session::has('current_permission')) ? Session::get('current_permission') : 'R'}}" />
        <input type="hidden" name="doc_type" id="doc_type" value="" />
        <input type="hidden" name="folder_name" id="fol_name" value="" />
        <input type="hidden" name="id_document" id="id_document" value="" />
        <input type="file" name="file" id="file_upload" class="hide" /> 
    </form>
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="group_search" name="group_search" onsubmit="searchDocs('{{ route('documents.document-listing-ajax',['nDocumentId' => $nIdParent]) }}','{{ route('documents.document-listing',['nDocumentId' => $nIdParent]) }}','search_str','document_list');return false">
            <div class="input-group docs-search">                
                <input id="search_str" type="text" class="form-control" name="search_str"  placeholder="{{ trans('messages.search_document') }}" value="{{ ($sSearchStr) ? $sSearchStr : '' }}" required="required">
                <button type="submit">
                    <i class="glyphicon glyphicon-search"></i>
                </button>              
            </div>
        </form>
    </div>
    
    
    <div class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 document-list_my-docs">
         <div class="file-upload-loader">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>
            <div class="file-upload-error">
                <div class="error-message"></div>
            </div>
        <ul class="my-docs_document-title clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12">
            <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                {{ trans('messages.name') }}
                <span class="column-sorting-options document_name" onclick="getDocsSort('document_name','{{ route('documents.document-listing-ajax',['nDocumentId' => $nIdParent]) }}','document_list');">
                    <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/asc.png')}}">
                    </span>
                    <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='document_name') ? 'active': ''}}">
                        <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                    </span>
                </span>
            </li>
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 document-date">
                <span>
                    {{ trans('messages.doc-owner') }}
                    <span class="column-sorting-options first_name" onclick="getDocsSort('first_name','{{ route('documents.document-listing-ajax',['nDocumentId' => $nIdParent]) }}','document_list');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='first_name') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
                <span class="pull-right">
                    {{ trans('messages.date') }}
                    <span class="column-sorting-options updated_at" onclick="getDocsSort('updated_at','{{ route('documents.document-listing-ajax',['nDocumentId' => $nIdParent]) }}','document_list');">
                        <span class="asc {{ ($sOrderBy == 'asc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/asc.png')}}">
                        </span>
                        <span class="desc {{ ($sOrderBy == 'desc' && $sOrderField =='updated_at') ? 'active': ''}}">
                            <img class="" src="{{asset('assets/web/img/dsc.png')}}">
                        </span>
                    </span>
                </span>
            </li>        
            <li class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-right">{{ trans('messages.share_with') }}</li>
        </ul>
        <div class="document-list-parent clearfix" id="{{ 'more_document_'.$nIdParent }}">
            @if(!isset($nDocumentId))
            <ul class="my-docs_document-list clearfix col-lg-12  col-md-12 col-sm-12 col-xs-12 " id=''>
                <li class="col-lg-6 col-md-6 col-sm-6 col-xs-6 document-name clearfix">
                    <a href="javascript:void(0);" onclick="getDocumentData('{{ route('documents.post-document-ajax') }}','{{ route('documents.post-document') }}');">                        
                        <img class="" src="{{asset('assets/web/img/folder-icon.png')}}"> {{ trans('messages.post-document-title') }}                     
                    </a>
                </li>
                <li class="col-lg-4 col-md-4 col-sm-4 col-xs-4"></li>
                <li class="col-lg-2 col-md-2 col-sm-2 col-xs-2 document-date"></li>
            </ul>
            @endif
            @if(count($oDocuments)) 
                @include('WebView::documents._more_document')
            @elseif(count($oDocuments)==0 && $nIdParent!= '')
                <div class="large-padding no-data">
                    <img class="" src="{{asset('assets/web/img/no-documents.png')}}">
                    {{ trans('messages.no_documents') }}

                </div>
            @endif
        </div>
<script type="text/javascript">
        
$(document).ready(function(){
    //toolip show 
    showTooltip();
    callTabDrop('docs-top-heading-bar');
    //Code for pagination
    var nCurrentPage = <?php echo $oDocuments->currentPage(); ?>;
    var nLastPage = <?php echo $oDocuments->lastPage(); ?>;
    $(window).scroll(function (event) {
        if(window.location.href == '<?php echo route('documents.document-listing',['nDocumentId' => $nIdParent]); ?>')
        {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('<?php echo 'more_document_'.$nIdParent; ?>', '<?php echo route('documents.document-listing',['nDocumentId' => $nIdParent]); ?>', nCurrentPage,'&order_by=<?php echo $sOrderBy; ?>&order_field=<?php echo $sOrderField; ?>&search_str='+$('#search_str').val());
                }
            }
        }
    });
    flagDoc = 0;
    $(document).on('focusout','.new-folder-name',function(e){   
        if(e.type == "focusout"){
        
            if(!flagDoc){
                CreateFolder(this,e,1);
            }
            else{
                flagDoc = 0;
            }
        }
    });
    
    $(document).on('keyup','.new-folder-name',function(e){
        e.preventDefault();
        if (e.keyCode == 13) {
             /*$(this).off("focusout");*/
            flagDoc = 1;
            CreateFolder(this,e,1);
            
        } 
    });
    
    $('.drop_doc').dropzone({ 
        headers:
        {
            'X-CSRF-Token': $('input[name="_token"]').val()
        },
        url: "{{ route('documents.create-folder') }}" ,
        params : { parent_id : $('#parent_id').val(), doc_type : 'F',shared_with : $('#shared_with').val()},
        success: function(){
            location.reload();
        }
    });
    $(document).on('focusout','.doc-copy',function(e){ 
        
    });
    
    /*$('#search_str').keyup(function(){
        var text = $('#search_str').val();
        if(text.length > 2)
        searchDocs('<?php echo route('documents.document-listing-ajax',['nDocumentId' => $nIdParent]); ?>','<?php echo route('documents.document-listing',['nDocumentId' => $nIdParent]); ?>','search_str','document_list')
    });*/
}); 

</script>
    </div>
</div>