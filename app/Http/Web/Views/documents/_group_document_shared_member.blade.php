@foreach($oGroupMember as $oUserList)
<span class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix doc-permision-group-single-member ">
    <span class="docs-permission-grp-name">
        <span class="img-25 display-ib v-align" data-toggle="tooltip" title="" data-original-title="Campusknot Designing ">
            {!! setProfileImage("50",$oUserList->user_profile_image, $oUserList->id_user, $oUserList->first_name, $oUserList->last_name) !!}
        </span>
        <span class="doc-group-name">{{ $oUserList->first_name.' '.$oUserList->last_name }}</span>
    </span>
    <span class="pos-relative display-ib v-align pull-right">
        @if($oUserList->id_user == Auth::user()->id_user)
            <span>{{ trans('messages.doc-owner')}}</span>
        @else
            <span class="caret"></span>
            <select class="permission-listing permission_change" data-id="{{ $oUserList->id_document_permission }}">
                <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEREAD') }}" <?php if($oUserList->permission_type== config('constants.DOCUMENTPERMISSIONTYPEREAD')) echo "selected='selected'"; ?> >
                    {{ trans('messages.read_permission_text') }}
                </option>
                <option value="{{ config('constants.DOCUMENTPERMISSIONTYPEWRITE') }}" <?php if($oUserList->permission_type== config('constants.DOCUMENTPERMISSIONTYPEWRITE')) echo "selected='selected'"; ?>>
                    {{ trans('messages.write_permission_text') }}
                </option>
            </select>
        @endif
    </span>
</span>
@endforeach