<div class="modal-content document-copy-popup">
    <div class="modal-header">
        <button type="button" onclick="closeLightBox();" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php
        if($oDocument->document_type == 'F') $sMessage = 'File';
        else $sMessage = 'Folder';
        ?>
        <h4 id="" class="text-left">{{ trans('messages.copy_folder',['folder' => $sMessage ]) }} : {{ $oDocument->document_name }}</h4>
    </div>
    <!-- Start: Model Body -->
    <div class="modal-body">
        <span class="error_message"></span>
        <div class="copy-document-container">
            <ul class="copy-doc-root-parent">
                <li class="has-child">
                    <span class="folder-state-indication">
                        <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                        <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                    </span>
                    <span class="folder-name active" data-id="">
                        <span class="cpy-doc-folder-icon">
                            <img class="" src="{{asset('assets/web/img/folder-icon.png')}}">
                        </span>
                        My document
                    </span>
                    <ul class="clearfix">
                    <?php 
                    foreach($oDocuments as $oDoc)
                    {
                        $oParent = $oDoc[0];
                        $nLevel = 0;
                        foreach($oDoc as $oSingleTree)
                        {
                            if($oSingleTree->rgt-$oSingleTree->lft == 1)
                            {
                            ?>
                                <li class="padding-5-0">
                                    <span class="folder-name active" data-id="{{ $oSingleTree->id_document }}">
                                        <span class="cpy-doc-folder-icon">
                                            <img class="" src="{{asset('assets/web/img/folder-icon.png')}}">
                                        </span>
                                        {{ $oSingleTree->document_name }}
                                    </span>
                                </li>
                                <?php
                                if($oParent->rgt-$oSingleTree->rgt <= 1 && $nLevel > 0)
                                {
                                    echo "</ul></li>";
                                    $nLevel--;
                                }
                                if($nLevel == 0)
                                    $oParent = $oSingleTree;
                            }
                            else 
                            {
                                $oParent = $oSingleTree;
                                ?>
                                <li class="has-child padding-5-0">
                                    <span class="folder-state-indication">
                                        <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                                        <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                                    </span>
                                    <span class="folder-name active" data-id="{{ $oSingleTree->id_document }}">
                                        <span class="cpy-doc-folder-icon">
                                            <img class="" src="{{asset('assets/web/img/folder-icon.png')}}">
                                        </span>
                                        {{ $oSingleTree->document_name }}
                                    </span>

                                    <ul class="clearfix">
                                <?php
                                
                                $nLevel++;
                            }
                            
                        }
                        for($ncount=$nLevel;$ncount>0;$ncount--)
                        { ?>
                            </ul>
                                </li>
                        <?php                        
                        }
                        
                    }
                    ?>
                    </ul>
                </li> 
                <li class=" padding-5-0 has-child">
                    <span class="folder-state-indication">
                        <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                        <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                    </span>
                    <span class="folder-name" data-id="not_valid">
                        <span class="cpy-doc-folder-icon">
                            <img class="" src="{{asset('assets/web/img/folder-icon.png')}}">
                        </span>
                        Group docs
                    </span>
                    <ul class="clearfix">
                    <?php 
                    foreach($oGroupList as $oGroups)
                    { 
                        ?>
                    <li class="has-child">
                    <span class="folder-state-indication">
                        <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                        <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                    </span>
                    <span class="folder-name active" data-id="{{ $oGroups->id_group }}_G">
                        <span class="cpy-doc-folder-icon">
                            <img class="" src="{{asset('assets/web/img/folder-icon.png')}}">
                        </span>
                        {{ $oGroups->group_name }}
                    </span>
                    <ul class="clearfix">
                    <?php
                        foreach($oGroups->group_doc as $oSingleTree)
                        {
                        ?>
                            <li class="has-child">
                                <span class="folder-state-indication group_child_doc" data-id="{{ $oSingleTree->id_document }}" data-tag='g'>
                                    <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                                    <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                                </span>
                                <span class="folder-name {{($oSingleTree->permission_type == config('constants.DOCUMENTPERMISSIONTYPEWRITE')) ? 'active' : 'inactive'}}" data-id="{{ $oSingleTree->id_document }}">
                                    <span class="cpy-doc-folder-icon">
                                        <img class="" src="{{($oSingleTree->permission_type == config('constants.DOCUMENTPERMISSIONTYPEWRITE')) ? asset('assets/web/img/folder-icon.png') : asset('assets/web/img/inactive-folder-icon.png')}}">
                                    </span>
                                    {{ $oSingleTree->document_name }}
                                </span>
                                <ul class="clearfix" id="child_g_{{ $oSingleTree->id_document }}"> </ul>
                            </li>
                        <?php       
                        }
                        ?>
                    </ul>
                    </li>
                    <?php
                    }
                    ?>
                    </ul>
                </li>
                
                <li class=" padding-5-0 has-child">
                    <span class="folder-state-indication">
                        <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                        <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                    </span>
                    <span class="folder-name" data-id="not_valid">
                        <span class="cpy-doc-folder-icon">
                            <img class="" src="{{asset('assets/web/img/folder-icon.png')}}">
                        </span>
                        Shared documents
                    </span>
                    <ul class="clearfix">
                        <?php
                        foreach($oSharedFolder as $oSingleTree)
                        {
                        ?>
                            <li class="has-child">
                                <span class="folder-state-indication group_child_doc" data-id="{{ $oSingleTree->id_document }}" data-tag='s'>
                                    <img class="expand" src="{{asset('assets/web/img/doc-plus.png')}}">
                                    <img class="collapse" src="{{asset('assets/web/img/doc-minus.png')}}">
                                </span>
                                <span class="folder-name {{($oSingleTree->permission_type == config('constants.DOCUMENTPERMISSIONTYPEWRITE')) ? 'active' : 'inactive'}}" data-id="{{ $oSingleTree->id_document }}">
                                    <span class="cpy-doc-folder-icon">
                                        <img class="" src="{{($oSingleTree->permission_type == config('constants.DOCUMENTPERMISSIONTYPEWRITE')) ? asset('assets/web/img/folder-icon.png') : asset('assets/web/img/inactive-folder-icon.png')}}">
                                    </span>
                                    {{ $oSingleTree->document_name }}
                                </span>
                                <ul class="clearfix" id="child_s_{{ $oSingleTree->id_document }}"> </ul>
                            </li>
                        <?php       
                        }
                        ?>
                    </ul>
                </li>
            </ul>
            
        </div>
        <form id="copy_folder_form" class="form-horizontal clearfix" method="post" action="{{ route('documents.document-copy') }}">
            {!! csrf_field() !!}
            <input id="id_document" name="id_document" type="hidden" value="{{ $oDocument->id_document }}" />
            <input id="id_parent" name="id_parent" type="hidden" value="" />

            
            <div class=" submit-options clearfix">
                <button onclick="closeLightBox();" class="btn btn-default pull-right " type="button">{{ trans('messages.cancel') }}</button>
                <input id="copy_button" type="submit" class="btn btn-primary pull-right" value="{{ trans('messages.copy') }}" data-loading-text="{{ trans('messages.copy') }}" disabled="disabled">
            </div>
        </form>  
    </div>
</div>


<script type="text/javascript">
    $('.has-child').on('click','.expand',function(e) {
            $(this).parent("span").siblings("ul").css("height", "auto");
            $(this).siblings().css("display", "inline-block");
            $(this).css("display", "none");
    });
    $('.has-child').on('click','.collapse',function (e){
            $(this).parent("span").siblings("ul").css("height", "0");
            $(this).siblings().css("display", "inline-block");
            $(this).css("display", "none");
    });
    $('.has-child').on('click','.active',function(e) {
    //$('.active').click(function () {
            $(".folder-name").css("background", "transparent");
            var doc_id = $(this).attr('data-id');
            $(this).css("background", "rgba(80, 161, 204, 0.12)");
            $('#id_parent').val(doc_id);
            $('#copy_button').attr('disabled',false);
            
    });
    $('.has-child').on('click','.inactive',function(e) {
            $(".folder-name").css("background", "transparent");
            $(this).css("background", "#fff");
            $('#copy_button').attr('disabled',true);
            $('#id_parent').val('not_valid');
    });

$("#copy_folder_form").on('submit', function(e){
    var isvalidate = $('#id_parent').val();
    if(isvalidate == 'not_valid')
    {
        $('.error_message').text("<?php echo trans('messages.not_valid_copy_folder'); ?>");
        return false;
    }
});
        
$('.has-child').on("click",".group_child_doc",function(e){
    var id_document = $(this).attr('data-id');
    var dtag = $(this).attr('data-tag');
    var isData = $('#child_'+dtag+'_'+id_document).html();
    if(isData.trim() == '' && !nLoadNewDataStatus)
    {
        $.ajax({
            type: "GET",
            url: siteUrl+'/documents/group-child-document/'+id_document ,
            data : {'sDataFrom' : dtag},
            success: function (data) {
                var response = data.html;
                $('#child_'+dtag+'_'+id_document).html(response);            
            },
            beforeSend: function(){
                nLoadNewDataStatus = 1; // status set if ajax call is active
            },
            complete: function(){
                nLoadNewDataStatus = 0; // status set if ajax call is complete
            }
        });
    }
});


</script>