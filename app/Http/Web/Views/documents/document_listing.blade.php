@extends('layouts.web.main_layout_col9')

@section('title', 'Campusknot-Document List')

@section('content')

<div class="clearfix documents">
    @if ($errors->has('group_error'))
        <div class="clearfix alert alert-danger">{{ $errors->first('group_error') }}</div>
    @endif
    <div class="clearfix pos-relative" id="tabs">
        <ul class="document-tabs group-tabs nav nav-tabs clearfix">
            <li id="document-listing" class="{{ ($sDocumentListing == 'DocumentList' || $sDocumentListing == 'PostDocumentList' ) ? 'active' : ''}}" onclick="getDocumentData('{{ route('documents.document-listing-ajax') }}','{{ route('documents.document-listing') }}');"> <a href="#" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap >{{ trans('messages.my_docs') }}</a></li>
            <li id="shared-document" class="{{ ($sDocumentListing == 'ShareDocumentList') ? 'active' : ''}}"  onclick="getDocumentData('{{ route('documents.shared-document-ajax') }}','{{ route('documents.shared-document') }}');"> <a href="#" data-toggle="tab" class="mightOverflow"  data-toggle="tooltip" data-placement="bottom" title="" nowrap >{{ trans('messages.shared_with_me') }}</a></li>
            <li id="group-document" class="group-document-tab dropdown {{ ($sDocumentListing == 'GroupDocumentList' || $sDocumentListing == 'GroupPostDocumentList') ? 'active' : ''}}"> 
                <a href="#"  data-toggle="dropdown"  class="dropdown-link-caret" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                    {{ trans('messages.group_docs') }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                    @foreach($oGroupListTab as $oGroupList)
                    <li id="groupId_{{ $oGroupList->id_group }}" data-toggle="tooltip" data-placement="top" title="{{ $oGroupList->group_name }}" nowrap> 
                        <a onclick="getDocumentData('{{ route('documents.group-document-ajax',$oGroupList->id_group) }}','{{ route('documents.group-document',$oGroupList->id_group) }}');" class="mightOverflow " nowrap>
                            {{ $oGroupList->group_name }}
                            @if($oGroupList->section != '')
                                {{ trans('messages.section') }}- {{$oGroupList->section}} ({{$oGroupList->semester}})
                            @endif
                        </a>
                    </li>
                    @endforeach
                </ul>
            </li>
            <li class="pull-right add-new-document-bar">
                @if(Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD'))
                    @include('WebView::documents._create_folder_tab')
                @endif
            </li>
        </ul>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             
        </div>
        <div class="loader_ajax col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden">
            <div class='spinner'> 
                <div class='bounce1'></div>
                <div class='bounce2'></div>
                <div class='bounce3'></div>
            </div>
            
            <!--<div class="loader-text">
                <img src="{{asset('assets/web/img/loader.gif')}}" >
                <div>{{ trans('messages.loading') }}</div>
            </div> -->
          
        </div>
        
        <div id="document_list" class="document-list clearfix">
            <input type="hidden" id="old_folder_name" />
            @if($sDocumentListing == 'DocumentList')
                @include('WebView::documents._my_document')
            @elseif($sDocumentListing == 'ShareDocumentList')
                @include('WebView::documents._shared_document')
            @elseif($sDocumentListing == 'GroupPostDocumentList' || $sDocumentListing == 'PostDocumentList')
                @include('WebView::documents._post_document')
            @else
                @include('WebView::documents._group_document')
            @endif
        </div>
        <div class="more-data-loader col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden">
            <div class='spinner'> 
                <div class='bounce1'></div>
                <div class='bounce2'></div>
                <div class='bounce3'></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
       setNotClickable();  
    });
    $(document).on('click','.dropdown-menu',function(e){
        $(this).parent('li').siblings().removeClass('active');
        $(this).parent('li').addClass('active');
    });
$(document).on('click','.doc-rename',function(e){
    var folderName = htmlSpecialChars($.trim($(this).parent().parent().parent().siblings('li').children('a').text()));
    var imgSrc = $.trim($(this).parent().parent().parent().siblings('li').find("img").attr('src'));
    $('#fol_name').val(folderName);
    var renameFolder ='<li class="col-lg-6 col-md-6 col-sm-6 col-xs-6"> <img class="" src="'+imgSrc+'"> <input type="text" class="new-folder-name  form-control"  maxlength="250" value ="'+ folderName+ '" autofocus></li>';
    var doc_id=$(this).closest('ul').attr('id');
    $('#id_document').val(doc_id);
    $('#old_folder_name').val(folderName);
    $(this).closest('ul').prepend(renameFolder);
    $(this).closest('ul').addClass('mk-new-folder');
    $('.new-folder-name').trigger('focus');
    $( ".new-folder-name" ).select();
    $(this).parent().parent().parent().siblings('.document-name').remove();
    $(this).parent().remove();
    e.preventDefault();
});

$(window).bind('popstate', function() {
    
    var sPathName = location.pathname;
    var aUrlComponents = sPathName.split('/');
    
    var sLoadUrl = siteUrl + '/' + aUrlComponents[1] + '/' + aUrlComponents[2] + '-ajax';
    
    for(var nUrlIndex = 3; nUrlIndex < aUrlComponents.length; nUrlIndex++)
    {
        sLoadUrl += '/' + aUrlComponents[nUrlIndex];
    }
    getDocumentData(sLoadUrl, location);
    //Make side bar tab selected
    if(aUrlComponents[2] == 'post-document')
        aUrlComponents[2] = 'document-listing';
    else if(aUrlComponents[2] == 'group-post-document')
        aUrlComponents[2] = 'group-document';
    
    makeTabSelected(aUrlComponents[2]);
});

function makeTabSelected(location) {
    var oActiveTab = $('#'+location);
    oActiveTab.siblings().removeClass('active');
    oActiveTab.addClass('active');
}
</script>
@stop