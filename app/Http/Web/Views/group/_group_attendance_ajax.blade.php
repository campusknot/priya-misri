<!-- Filename:: group._group_attendance_ajax -->
@if(count($oAllUniqueLecture))
    @include('WebView::group._group_more_attendance')
@else
    {{trans('messages.no_group_attendance' )}}
@endif
<script type="text/javascript">
    var nCurrentPage = <?php echo $oAllUniqueLecture->currentPage(); ?>;
    var nLastPage = <?php echo $oAllUniqueLecture->lastPage(); ?>;
    $(window).scroll(function (event) {
        if(window.location.href == '<?php echo route('group.attendance'); ?>')
        {
            if($(window).scrollTop() + $(window).height() == $(document).height() ) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('atten_ajax', '<?php echo route('group.attendance'); ?>', nCurrentPage);
                }
            }
        }
    });
</script>