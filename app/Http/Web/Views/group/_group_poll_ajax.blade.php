<!-- Filename:: group._group_poll_ajax -->
@if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
    <div class="ck-background-white">
        <div class="no-data ">
            <img src="{{asset('assets/web/img/private-group.png')}}">
            <div>{{ $oGroupDetails->about_group }}</div>
            <div class="sml-text">
            @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                {{ trans('messages.private_group_member') }}
            @else
                {{ trans('messages.secret_group_member') }}
            @endif
            </div>
        </div>
    </div>
@else
    <div class="">
        <div class="inner-page-heading padding-10 clearfix">
            <h3>{{ trans('messages.polls') }}</h3>
        </div>
        @if(count($oGroupDetails))
        <div id="group_poll" class="poll-list clearfix">
            @if(count($oGroupFeeds))
                @include('WebView::group._more_group_poll')
            @else
            <div class="no-data">
                <img src="{{asset('assets/web/img/no-poll.png')}}">
                {{ trans('messages.no_user_poll') }}
            </div>
            @endif
        </div>
        @else
        <div class="col-lg-12  col-md-12 col-sm-12 col-xs-12 no-data-found m-t-2 background-white round-coreners-8">
            <h2> {{ trans('messages.no_record_found') }} </h2>
        </div>
        @endif
    </div>
@endif
    

<script type="text/javascript">
    var nCurrentPage = <?php echo $aPaginator['current_page']; ?>;
    var nLastPage = <?php echo $aPaginator['last_page']; ?>;
    
    $(document).scroll(function (event) {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                nCurrentPage +=1;
                loadNewData('group_poll', '<?php echo route('group.group-poll', ['nIdGroup' => $oGroupDetails->id_group]); ?>',nCurrentPage);
            }
        }
    });
    
    function showRequestComment(formData, jqForm, options)
    { 
        $("#validation-errors").hide().empty();
        $("#output").css('display','none');
        return true; 
    }
    
    function showResponseComment(response, statusText, xhr, $form)
    { 
        if(response.success == true)
        {
            if(response.type=='poll'){
                $('#'+response.id_post).replaceWith(response.html);
            }
            else
            {
                $('#add_comment_'+ response.id_post)[0].reset();
                $('#comment-box-'+response.id_post).html(response.html);
                removeErrorComment(response.id_post);
            }
//            $('.btn-md').button('reset');
//            $('.animated').css('height','36px');
        }
        else
        { 
            var arr = response;
                $.each(arr, function(index, value)
                {
                        if (value.length != 0)
                        {
                                $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                        }
                });                     
        }
    }
    
    function showErrorComment(xhr, textStatus, errorThrown)
    {
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
            }
        
        var result = $.parseJSON(arr);
        $.each(result, function(key, value)
        {
            key=key.replace('file','comment_text');
            $('.btn-md').button('reset');
            if (key.length != 0)
            {
                    $("#"+key).html('<strong>'+ value +'</strong>');
            }
        });
    }
    
    function removeErrorComment(id)
    {
        $("#comment_text_"+id).html('');
    }
    $(document).ready(function(){
        var options = { 
        beforeSubmit:  showRequestComment,
        success:       showResponseComment,
        error:showErrorComment,
        dataType: 'json' 
        }; 
        
        
        setInterval(function() {
            $(".minutepoll").each(function(){
                var ele=$(this);
                var myTime = ele.val()-1;
                
                if(myTime <= 300 ){
                    var MinSec=getTime(myTime);
                    ele.siblings('span').text(MinSec);
                    ele.siblings('span').removeClass('hidden');
                    ele.siblings('div').addClass('hidden');
                }
                ele.val(myTime);

                if(myTime <= 0){
                    ele.parent().addClass('hidden');
                    var parent=ele.parent().parent().children('form');
                    parent.children('input[name=expired]').val(1);
                    parent.children('button').trigger( "click" );
                    
                }
            });
        },  1000);
        $(document).on('click','.btn-poll',function(e){
            var data = $(this);
            var id_poll = data.attr('data-id');
            $(this).button('loading');
            $('#poll_'+id_poll).ajaxForm(options).submit();
        });
    });
</script>