@if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
    <div class="ck-background-white">
        <div class="no-data ">
            <img src="{{asset('assets/web/img/private-group.png')}}">
            <div>{{ $oGroupDetails->about_group }}</div>
            <div class="sml-text">
            @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                {{ trans('messages.private_group_member') }}
            @else
                {{ trans('messages.secret_group_member') }}
            @endif
            </div>
        </div>
    </div>
@else
    <div class="inner-page-heading padding-10">
        <h3 class=""> {{ trans('messages.events_page_title') }} </h3>
        @if(count($oGroupDetails))
            @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
            <div class="btn btn-primary pull-right">
                <a href="#myModal-add-event" class="mightOverflow" data-toggle="modal" data-toggle="tooltip" data-placement="bottom" onclick="showAddEventLightBox();" title="" nowrap>
                    {{ trans('messages.create_event') }}
                </a>
            </div>
            @endif
            
    </div>
    <div class="clearfix event-list pos-relative">
            <form id="group_search" name="group_search" onsubmit="searchdata('{{ route('group.group-events-ajax',['nIdGroup' => $oGroupDetails->id_group]) }}','{{ route('group.group-events',['nIdGroup' => $oGroupDetails->id_group]) }}','search_event','event_list');return false;">
                <div class="input-group group-search">                
                    <input id="search_event" type="text" class="form-control" name="search_member"  placeholder="{{ trans('messages.search_group_event') }}" value="{{ ($sSeacrhStr) ? $sSeacrhStr : '' }}" >
                    <button type="submit">
                        <i class="glyphicon glyphicon-search"></i>
                    </button>              
                </div>
            </form>
            @if(count($oEventTabs))
                <div class="planner-timeline">

                </div>  
            @endif
            <div id="event_list" class="event-detail">
            <?php
                $sPreviousYear = ($oEventTabs->currentPage() > 1) ? ((Session::has('group_event_list_current_year')) ? Session::get('group_event_list_current_year') : '') : '';
                $sPreviousDate = ($oEventTabs->currentPage() > 1) ? ((Session::has('group_event_list_current_date')) ? Session::get('group_event_list_current_date') : '') : '';

                $nEventCount = 0;
                ?>
                @if(count($oEventTabs))
                @foreach( $oEventTabs as $Event )

                <?php
                    $dEventStartDate = Carbon::createFromFormat('Y-m-d H:i:s', $Event->start_date);
                    $dEventStartDate->setTimezone(Auth::user()->timezone);
                    $dEventEndDate='';
                    if($Event->end_date != null){
                        $dEventEndDate = Carbon::createFromFormat('Y-m-d H:i:s', $Event->end_date);
                        $dEventEndDate->setTimezone(Auth::user()->timezone);
                    }
                    $sEventYear = $dEventStartDate->format('Y');
                    $sEventDate = $dEventStartDate->format('M j');
                    if($sEventDate != $sPreviousDate)
                    {
                        session(['event_list_current_date' => $sEventDate]);
                        $sPreviousDate = $sEventDate;
                        if($nEventCount > 0)
                            echo '</div><div class="event-detail">';

                        if($sEventYear != $sPreviousYear)
                        {
                            session(['event_list_current_year' => $sEventYear]);
                            $sPreviousYear = $sEventYear;
                            echo '<span class="event-day event-year pull-left">'.$sEventYear.'</span>';
                        }
                    ?>
                    <span class="event-day">{{$dEventStartDate->format('M j')}}  | {{$dEventStartDate->format('l')}}</span>
                    <span class="horizontal-line"></span>
                    <?php } ?>
                    @if(count($Event))
                        <ul class="" onclick="getSingleEvent({{$Event->id_event}},'GET');">
                            <li class="center">
                                <span class="event-time">{{$dEventStartDate->format('g:i A')}}</span>
                            </li>
                            <li>
                                <div class="event-name">{{$Event->event_title}}</div><!-- event -->
                                <div class="">{{$dEventStartDate->format('l')}} {{$dEventStartDate->format('M j')}}, {{$dEventStartDate->format('g:i A')}}
                                    @if($dEventEndDate != '')
                                - {{ ($dEventStartDate->toDateString() == $dEventEndDate->toDateString()) ? $dEventEndDate->format('g:i A') : 
                                            $dEventEndDate->format('l').' '.$dEventEndDate->format('M j').', '.$dEventEndDate->format('g:i A') }}
                                @endif
                                </div>
                                @if($Event->latitude != '0.00000000' &&  $Event->longitude != '0.00000000' )
                                    <a class="event-place" target="_blank" href="{{config('constants.GMAPURL')}}?q={{$Event->address}}">{{$Event->address}}:{{$Event->room_number}}</a> <!-- place -->
                                @else
                                    <div>{{$Event->address}}:{{$Event->room_number}}</div>
                                @endif
                            </li>
                        </ul>
                        @else
                        <div class="center">
                                <img src="{{ asset('assets/web/img/no-event.png') }}" />
                            <h4> {{ trans('messages.no_events_found') }} </h4>
                        </div>
                    @endif
                @endforeach
                @else
                   <div class="no-data">
                       <img src="{{asset('assets/web/img/no-events.png')}}">
                       {{ trans('messages.no_group_events') }}
                   </div>
                @endif
            </div>
        @else
        <div class="col-lg-12 no-data-found m-t-2 background-white round-coreners-8">
            <h2> {{ trans('messages.no_record_found') }} </h2>
        </div>
        @endif
    </div>
@endif
<form id="event_filter_form" name="event_filter_form" method="GET" enctype="multipart/form-data">
        <input id="event_filter_id_group" name="id_group" type="hidden" value="{{ $oGroupDetails->id_group }}" />
        <input id="event_filter_start_date" name="start_date" type="hidden" value="{{ old('start_date') }}" />
        <input id="event_filter_search_str" name="search_str" type="hidden" value="{{ old('search_str') }}" />
    </form>
<script type="text/javascript">
    var nCurrentPage = <?php echo $oEventTabs->currentPage(); ?>;
    var nLastPage = <?php echo $oEventTabs->lastPage(); ?>;
    
    $(document).scroll(function (event) {
        if(window.location.href == '<?php echo route('group.group-events', ['nIdGroup' => $oGroupDetails->id_group,'sSeacrhStr' => $sSeacrhStr]); ?>')
        {
            if($(window).scrollTop() + $(window).height()+20 >= $(document).height()) {
                var nIdGroup = $('#event_filter_id_group').val();
                var sStartDate = $('#event_filter_start_date').val();
                var sSearchStr = $('#search_event').val();
                
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    var searchStr=$('#search_event').val();
                    loadNewData('event_list', '<?php echo route('group.group-events', ['nIdGroup' => $oGroupDetails->id_group]); ?>/'+searchStr,nCurrentPage,'&id_group='+nIdGroup+'&start_date='+sStartDate+'&search_str='+sSearchStr);
                }
            }
        }
    });
    
    $('#search_event').keyup(function(){
        var text = $('#search_event').val();
        if(text.length > 0)
        searchdata('<?php echo route('group.group-events-ajax',['nIdGroup' => $oGroupDetails->id_group]); ?>','<?php echo route('group.group-events',['nIdGroup' => $oGroupDetails->id_group]); ?>','search_event','event_list');
    });
</script>
