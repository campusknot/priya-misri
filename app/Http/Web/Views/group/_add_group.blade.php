<!-- Modal content-->
<div class="modal-content add-group-popup">
    <form id="add_group_form" name="add_group_form" method="POST" action="" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="modal-header">
            <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title event-title">{{ trans('messages.add_group') }}</h4>
        </div>
        <div class="modal-body clearfix">
            <div class="form-group clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="text" title="{{ ($errors->has('group_name')) ? $errors->first('group_name') : '' }}" data-toggle="tooltip"  data-placement="top" id="group_name" class="form-control {{ ($errors->has('group_name')) ? 'error' : '' }}" name="group_name" placeholder="{{trans('messages.group_name')}}" value="{{ old('group_name') }}" required="required">
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pos-relative" title="{{($errors->has('group_category')) ? $errors->first('group_category') : '' }}" data-toggle="tooltip"  data-placement="top">
                    <span class="caret"></span>
                    <select id="groupcategory" class="form-control selectpicker {{($errors->has('group_category')) ? 'error' : '' }}" name="group_category" required="required">
                        <option value="">{{ trans('messages.select_group_category') }}</option>
                        @foreach($aGroupCategories as $aGroupCategory)
                            @if($aGroupCategory['id_group_category'] != config('constants.COURSEGROUPCATEGORY') && $aGroupCategory['id_group_category'] != config('constants.UNIVERSITYGROUPCATEGORY'))
                            <option value="{{ $aGroupCategory['id_group_category'] }}" {{ (old('group_category') == $aGroupCategory['id_group_category']) ? 'selected="selected"' : '' }}>{{ $aGroupCategory['group_category_name'] }}</option>
                            @endif
                        @endforeach
                    </select>
                    @if ($errors->has('group_category'))
                        <span class="date-error"><?php echo $errors->first('group_category'); ?></span>
                @endif
                </div>
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-12 col-xs-12 col-sm-10 col-md-10">
                    <div class="group-icons  clearfix">
                        <div class="type-info-icon"> 
                            <img src="{{asset('assets/web/img/info.png')}}" class="" >
                        </div>
                        <div class="type-info-text">
                                
                                {!! trans('messages.group_info_text') !!}
                        </div>
                        <input type="radio" name="group_type" id="public_group" value="{{ config('constants.PUBLICGROUP') }}" required="required" {{ (old('group_type') == config('constants.PUBLICGROUP')) ? 'checked="checked"' : '' }} />
                        <input type="radio" name="group_type" id="private_group" value="{{ config('constants.PRIVATEGROUP') }}" required="required" {{ (old('group_type') == config('constants.PRIVATEGROUP')) ? 'checked="checked"' : '' }} />    
                        <input type="radio"  name="group_type" id="secret_group" value="{{ config('constants.SECRETGROUP') }}" required="required" {{ (old('group_type') == config('constants.SECRETGROUP')) ? 'checked="checked"' : '' }} />       
                        <label for="public_group" class="group-type-radio-buttons group-type-title-public">
                            <div class="group-type-icon-public">
                               <img src="{{ asset('assets/web/img/public_group.png') }}" />
                            </div>
                           {{ trans('messages.PUB') }}
                        </label>
                        <label for="private_group" class="group-type-radio-buttons group-type-title-private">
                           <div class="group-type-icon-private">
                               <img src="{{ asset('assets/web/img/private_group.png') }}" />
                           </div>
                           {{ trans('messages.PVT') }}
                        </label>
                        <label for="secret_group" class="group-type-radio-buttons group-type-title-secret" >
                            <div class="group-type-icon-secret">
                               <img src="{{ asset('assets/web/img/secret_group.png') }}" />
                           </div>
                          {{ trans('messages.SEC') }}
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                @if ($errors->has('group_type'))
                        <span class="date-error"><?php echo $errors->first('group_type'); ?></span>
                    @endif
            </div>
            <div class="form-group clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <textarea id="about_group" class="form-control {{ ($errors->has('about_group')) ? 'error' : '' }}" name="about_group" type="text" title="{{ ($errors->has('about_group')) ? $errors->first('about_group') : '' }}" data-toggle="tooltip"  data-placement="top" placeholder="{{trans('messages.about_group')}}">{{ old('about_group') }}</textarea>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="button" onclick="submitAjaxForm('add_group_form', '{{ route('group.add-group') }}', this )" class="btn btn-primary lg-center-button">{{ trans('messages.create_group') }}</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   

    });    
</script>
<script>
    
    $(document).ready(function(){
        $('.selectpicker').selectmenu({
             
        });
    });
    
</script>