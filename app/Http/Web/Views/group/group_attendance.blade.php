<?php setcookie("attendance",1, 2147483647);?>
@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Attendance')


@section('content')
<!-- Filename:: group.group_attendane -->
<div class="user-attendance">
    <div class="inner-page-heading padding-10">
        <h3>{{ trans('messages.attendance') }}</h3>
        <div onclick="showAttendance(1,'take');" class="btn  pull-right ck-add-btn ml-10" data-toggle="collapse" data-target="#add_photo" aria-expanded="false" >{!! trans('messages.add_attendance') !!}</div>
        <div onclick="showAttendance(1,'submit');" class="btn  pull-right ck-add-btn"   >{!! trans('messages.submit_attendance') !!}</div>
    </div>

    <div class="attendance-panel clearfix">
        <div class="attendance-notation clearfix pos-relative">
            <ul class="pull-right">
                <li class="stu-present-indication">
                    {{ trans('messages.attendance_present') }}
                </li>
                <li class="stu-absent-indication">
                    {{ trans('messages.attendance_absent') }}
                </li>
                <li class="stu-sick-indication">
                    {{ trans('messages.sick_leave') }}
                </li>
            </ul>
        </div>
        <ul class="nav nav-tabs ck-tabs sub-tabs">
            <li class="{{ ($sAttendanceType == 'course') ? 'active':'' }}"><a data-toggle="tab" href="#home" onclick="loadCourseAttendanceData('atten_ajax', '{{ route('group.course-attendance-ajax') }}', '{{ route('group.course-attendance') }}');">Course Attendance</a></li>
            <li class="{{ ($sAttendanceType == 'group') ? 'active':'' }}"><a data-toggle="tab" href="#menu1" onclick="loadGroupAttendanceData('atten_ajax', '{{ route('group.attendance-ajax') }}', '{{ route('group.attendance') }}');">Group Attendance</a></li>
            
          </ul>
        <div class="tab-content ck-tab-content">
            <div id="" class="tab-pane fade in active">
               <div class="atten_ajax" id="atten_ajax">
                    @include('WebView::group._group_attendance_ajax')
                </div>
            </div>
        </div>
    </div>
</div>

@section('footer-scripts')
    
    $(".attendance-panel .nav-tabs li").click(function(){
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
    });
    
@endsection
@stop