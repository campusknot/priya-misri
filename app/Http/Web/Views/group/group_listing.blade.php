@extends('layouts.web.main_layout_1')

@section('title', 'Campusknot-Group List')

@section('content')
<div class="clearfix ">
    @if ($errors->has('group_error'))
        <div class="clearfix alert alert-danger">{{ $errors->first('group_error') }}</div>
    @endif
    
    <div class="clearfix pos-relative" id="tabs">
        <ul class="ck-top-tabs nav nav-tabs padding-right clearfix">
            <li id="active_recommended" class="{{ (isset($oGroupList['recommended_groups'])) ? 'active' : ''}}" onclick="chengeGroupListing('group_list', '{{ route('group.recommended-group-listing-ajax') }}', '{{ route('group.recommended-group-listing') }}');"> 
                <a href="#tab_group_all" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                    {{ trans('messages.recomended') }}
                </a>
            </li>
            <li id="active_member" class="{{ (isset($oGroupList['member_groups'])) ? 'active' : ''}}" onclick="chengeGroupListing('group_list', '{{ route('group.member-group-listing-ajax') }}', '{{ route('group.member-group-listing') }}');"> 
                <a href="#tab_group_member" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                    {{ trans('messages.member') }}
                </a>
            </li>
            <li id="active_admin" class="{{ (isset($oGroupList['admin_groups'])) ? 'active' : ''}}" onclick="chengeGroupListing('group_list', '{{ route('group.admin-group-listing-ajax') }}', '{{ route('group.admin-group-listing') }}');"> 
                <a href="#tab_group_admin" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                    {{ trans('messages.admin') }}
                </a>
            </li>
            @if($oGroupList['deactive_groups_count'] > 0)
                <li class="{{ (isset($oGroupList['deactive_groups'])) ? 'active' : ''}}" onclick="chengeGroupListing('group_list', '{{ route('group.deactive-group-listing-ajax') }}', '{{ route('group.deactive-group-listing') }}');"> 
                    <a href="#tab_group_deactivated" data-toggle="tab" class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                        {{ trans('messages.deactivated') }}
                    </a>
                </li>
            @endif
        </ul>
        <div class="switch-tab-loader hidden">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <div class="pull-right create"> <a href="javascript:void(0);" class="mightOverflow" onclick="showAddGroupLightBox();" title="" nowrap>{{ trans('messages.create_group') }}</a></div>
        <div id="group_list" class="group-list clearfix">
            @if(isset($oGroupList['recommended_groups']))
                @include('WebView::group._recommended_group_listing', array($oGroupList))
            @endif
            
            @if(isset($oGroupList['admin_groups']))
                @include('WebView::group._admin_group_listing', array($oGroupList))
            @endif
            
            @if(isset($oGroupList['member_groups']))
                @include('WebView::group._member_group_listing', array($oGroupList))
            @endif
            
            @if(isset($oGroupList['deactive_groups']))
                @include('WebView::group._deactive_group_listing', array($oGroupList))
            @endif

        </div>
        <div class="more-data-loader hidden">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        
    </div>
</div>
<script type="text/javascript">
    $(window).bind('popstate', function() {
        var sPathName = location.pathname;
        var aUrlComponents = sPathName.split('/');

        var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 2] + '/' + aUrlComponents[aUrlComponents.length - 1] + '-ajax';

        chengeGroupListing('group_list', sLoadUrl, location);
        makeTabSelected();
    });
        
    function makeTabSelected() {
        var sPathName = location.pathname;
        var aUrlComponents = sPathName.split('/');

        var sLoadUrl = siteUrl + '/' + aUrlComponents[aUrlComponents.length - 2] + '/' + aUrlComponents[aUrlComponents.length - 1] + '-ajax';

        //Make side bar tab selected
        var sAction = aUrlComponents[aUrlComponents.length - 1];
        var aActionComponents = sAction.split('-');

        var oActiveTab = $('#active_'+aActionComponents[aActionComponents.length - 3]);
        console.log(oActiveTab);
        oActiveTab.siblings().attr('class', '');
        oActiveTab.attr('class', '');
        oActiveTab.addClass('active');
    }
</script>
@stop