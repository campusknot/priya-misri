<div class="">
   <div id="group_calender" class="event-calender"></div>
</div>

<script>
$( "#group_calender" ).datepicker( {
   dateFormat: 'yy-mm-dd',
   onSelect: function()
   {
        $('#event_filter_start_date').val($(this).val());
        filterEventList();
   }

});
</script>