@foreach($oGroupFeeds as $oFeed)
    <div id="{{ $oFeed->id_post }}" class="post">
        <div class=" clearfix">
            <!-- Div for creator image -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 post-top-section">
                <div class="icon-image img-50">
                    <a href="{{route('user.profile',[$oFeed->id_user])}}">
                        {!! setProfileImage("50",$oFeed->user_profile_image,$oFeed->id_user,$oFeed->first_name,$oFeed->last_name) !!}
                    </a>
                </div>

                <div class="post-person-name">
                    <a href="{{route('user.profile',[$oFeed->id_user])}}">{{ $oFeed->first_name.' '.$oFeed->last_name}}</a>
                    <span class="post-time">{{ secondsToTime($oFeed->created_at) }} @if($oFeed->group_name != NULL) {{ ' | '.$oFeed->group_name }} @endif</span>
                    @if(count($oGroupMemberDetail))
                    <span class="edit-links">
                        <img src="{{asset('assets/web/img/dropdown-icon.png')}}" class="post-dropdown" alt="dropdown icon" data-toggle="dropdown" />
                        <ul class="dropdown-menu report-post">
                            @if($oFeed->id_user == Auth::user()->id_user || ( $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')) )
                                <li onclick="deletePost('{{trans('messages.post_delete_confirmation')}}','{{$oFeed->id_post}}');">{{ trans('messages.delete') }}</li>
                            @endif
                            @if($oFeed->id_user != Auth::user()->id_user)
                                <li class="report">{{ trans('messages.report') }}</li>
                                <li class="report-post-form">
                                    <div class="text-left report-post-header">{{ trans('messages.report_heading') }}</div>
                                    <form id="post_report_{{$oFeed->id_post}}" method="post" onsubmit="submitReport( 'post_report_{{$oFeed->id_post}}','{{ route('post.post-add-report')}}',this);return false;">
                                        <textarea name="report_text" placeholder="{{ trans('messages.report_placeholder') }}"></textarea>
                                        <input type="hidden" name="id_entity" value="{{$oFeed->id_post}}" />
                                        <input type="hidden" name="entity_type" value="{{ config('constants.ENTITYTYPEPOST') }}" />
                                        <input type="button" class="btn btn-primary" value="{{ trans('messages.submit') }}" onclick="submitReport( 'post_report_{{$oFeed->id_post}}','{{ route('post.post-add-report')}}',this)" />
                                        <input type="reset" value="{{ trans('messages.cancel') }}" class="remove-form btn btn-default"/>
                                    </form>
                                </li>
                            @endif
                        </ul>
                    </span>
                    @endif
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding-10-20">
                <div class="post-content">
                    @if(trim($oFeed->post_text)!= '')
                    <div class="post-text">
                        <p class="groupFeedsMore more">{!! setTextHtml($oFeed->post_text) !!}</p>
                    </div>
                    @endif

                    @if($oFeed->file_name)
                    <?php
                            $sHtmlString = '';
                            switch ($oFeed->post_type)
                            {
                                case config('constants.POSTTYPEIMAGE'):
                                    $sHtmlString = '<div class="post-img">';
                                        $sFileName = setPostImage($oFeed->file_name,80);
                                        $sHtmlString .= '<img src="'.$sFileName.'" onclick="showPhotoDetails('. $oFeed->id_post .','.$oGroupDetails->id_group.');">';
                                        //$sHtmlString .= '<img src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oFeed->file_name.'" onclick="showPhotoDetails('. $oFeed->id_post .','.$oGroupDetails->id_group.');">';
                                    $sHtmlString .= '</div>';

                                    echo $sHtmlString;
                                    break;
                                case config('constants.POSTTYPEVIDEO'):

                                    break;
                                case config('constants.POSTTYPEDOCUMENT'):
                                    $sHtmlString = '<div class="post-doc mightOverflow" data-toggle="tooltip" title="">';

                                    $aFileNameData = explode('.', $oFeed->file_name);
                                    

                                    //Div for file icon
                                    if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                    {
                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                    }
                                    elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                    {
                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                    }
                                    elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                    {
                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                    }
                                    elseif (in_array(end($aFileNameData), array('zip','rar')))
                                    {
                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                    }
                                    elseif (end($aFileNameData) == 'pdf')
                                    {
                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                    }
                                    elseif (end($aFileNameData) == 'csv')
                                    {
                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/csv.png').'">';
                                    }

                                    //Div for file details and view/download option
                                    $sHtmlString .= '<span class="">'.(($oFeed->display_file_name) ? $oFeed->display_file_name : $oFeed->file_name);
                                    $sHtmlString .= '<br>';
                                        $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['p_'.$oFeed->id_post,$oFeed->display_file_name]).'">'.trans('messages.view').'</a>';
                                        $sHtmlString .= '<a class="" target="blank" href="'.route('utility.download-file',['p_'.$oFeed->id_post,$oFeed->display_file_name]).'">'.trans('messages.download').'</a>';
                                    //$sHtmlString .= '<a class="" target="blank" href="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oFeed->file_name.'" download="'.$oFeed->display_file_name.'">'.trans('messages.download').'</a>';
                                    $sHtmlString .= '</span>';
                                    $sHtmlString .= '</div>';

                                    echo $sHtmlString;
                                    break;
                                default :
                                    //Code for text/code
                                    echo 'No file found';
                            }
                        ?>
                    @endif
                </div>
                <!-- Div for post like button -->
                <div class="post-likes like_button_{{ $oFeed->id_post }}" >
                    <div class="btn btn-like {{ ($oFeed->id_like) ? "btn-like-user" : "" }}" onclick="<?php echo (count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL) ? "updatePostLike('like_button_".$oFeed->id_post."','".$oFeed->id_post."');" : "displayAlertBox('".trans('messages.non_member_like_alert')."');";?>"></div>
                    @if($oFeed->likes > 0)
                       <a href="javascript:void(0);" onclick="getLikeUserList({{ $oFeed->id_post }});"><span>{{$oFeed->likes}}</span></a>
                    @endif
                </div>
            </div>
        </div>
        
        <!-- Comment listing -->
        <div class="comment-box {{ (count($oFeed->comments) > 0) ? '' : 'hidden' }}" id="comment-box-{{$oFeed->id_post}}">
            <div class="comments-number closed">
                    {!! trans('messages.comments', ['comment_count' => $oFeed->comment_count,'post' => $oFeed->id_post]) !!}
                    <span class="glyphicon glyphicon-chevron-down"></span>
                    <span class="glyphicon glyphicon-chevron-up"></span>
            </div>
            <div id="ext_comments_{{$oFeed->id_post}}" class="comments-parent clearfix">
                @if(count($oFeed->comments) > 0)                
                    <!-- extra comment strats here-->
                    <?php
                        for($nIndex = 0; $nIndex < count($oFeed->comments); $nIndex ++)
                        {
                            $oComment = $oFeed->comments[$nIndex];
                    ?>
                            <div class="row post-comment padding-10">
                                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-1 no-padding">
                                    <div class="icon-image img-35">
                                        {!! setProfileImage("50",$oComment->user_profile_image,$oComment->id_user,$oComment->first_name,$oComment->last_name) !!}
                                    </div>
                                </div>
                                <div class="col-lg-11 col-md-11 col-sm-10 col-xs-11 no-padding clearfix" >
                                    <div class="comment-person-name no-padding">{{ $oComment->first_name.' '.$oComment->last_name }} 
                                        <span class="comment-time">{{ secondsToTime($oComment->created_at) }} ago</span>
                                        <span class="comment-links pull-right">
                                            <img src="{{asset('assets/web/img/dropdown-icon.png')}}" class="post-dropdown" alt="dropdown icon" data-toggle="dropdown" />
                                            <ul class="dropdown-menu report-post">
                                                @if($oComment->id_user == Auth::user()->id_user || (count($oGroupMemberDetail) && $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')) )
                                                    <li onclick="deleteUserProfileDetail('{{ trans('messages.delete_record_alert', ["record_name" => "comment"]) }}','{{route('post.delete-post-comment',[$oComment->id_comment])}}');">{{ trans('messages.delete') }}</li>
                                                @endif
                                                @if($oComment->id_user != Auth::user()->id_user && count($oGroupMemberDetail))
                                                    <li class="report">{{ trans('messages.report') }}</li>
                                                    <li class="report-post-form">
                                                        <div class="text-left report-post-header">{{ trans('messages.report_comment_heading') }}</div>
                                                        <form id="comment_report_{{$oComment->id_comment}}" method="post" onsubmit="submitReport( 'comment_report_{{$oComment->id_comment}}','{{ route('post.post-add-report')}}',this);return false;">
                                                            <textarea name="report_text" placeholder="{{ trans('messages.report_placeholder') }}"></textarea>
                                                            <input type="hidden" name="id_entity" value="{{$oComment->id_comment}}" />
                                                            <input type="hidden" name="entity_type" value="C" />
                                                            <input type="button" class="btn btn-primary" value="{{ trans('messages.submit') }}" onclick="submitReport( 'comment_report_{{$oComment->id_comment}}','{{ route('post.post-add-report')}}',this)" />
                                                            <input type="reset" value="{{ trans('messages.cancel') }}" class="remove-form btn btn-default"/>
                                                        </form>
                                                    </li>
                                                @endif
                                            </ul>
                                        </span>
                                    </div>
                                    <div class="comment-content">
                                        <div class="comment-text">
                                                <p class="more groupCommentMore">{!! setTextHtml($oComment->comment_text) !!}</p>
                                        </div>
                                        @if($oComment->file_name)
                                        <?php
                                            $sHtmlString = '';
                                            switch ($oComment->comment_type)
                                            {
                                                case config('constants.POSTTYPEIMAGE'):
                                                    $sHtmlString = '<div class="comment-img">';
                                                        $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['c_'.$oComment->id_comment,$oComment->display_file_name]).'">';
                                                            $sHtmlString .= '<img src="'.config('constants.MEDIAURL').'/'.config('constants.POSTMEDIAFOLDER').'/'.$oComment->file_name.'">';
                                                        $sHtmlString .= '</a>';
                                                    $sHtmlString .= '</div>';

                                                    echo $sHtmlString;
                                                    break;
                                                case config('constants.POSTTYPEVIDEO'):

                                                    break;
                                                case config('constants.POSTTYPEDOCUMENT'):
                                                    $sHtmlString = '<div class="comment-doc">';

                                                    $aFileNameData = explode('.', $oComment->file_name);

                                                    //Div for file icon
                                                    if(in_array(end($aFileNameData), array('doc','docx','pages','rtf','txt','wp')))
                                                    {
                                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/doc.png').'">';
                                                    }
                                                    elseif (in_array(end($aFileNameData), array('numbers','xls','xlsx')))
                                                    {
                                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/xls.png').'">';
                                                    }
                                                    elseif (in_array(end($aFileNameData), array('key','ppt','pps','pptx')))
                                                    {
                                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/ppt.png').'">';
                                                    }
                                                    elseif (in_array(end($aFileNameData), array('zip','rar')))
                                                    {
                                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/zip.png').'">';
                                                    }
                                                    elseif (end($aFileNameData) == 'pdf')
                                                    {
                                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/pdf.png').'">';
                                                    }
                                                     elseif (end($aFileNameData) == 'csv')
                                                    {
                                                        $sHtmlString .= '<img src="'.asset('assets/web/img/file-icons/csv.png').'">';
                                                    }

                                                    //Div for file details and view/download option
                                                    $sHtmlString .= '<span class="">'.(($oComment->display_file_name) ? $oComment->display_file_name : $oComment->file_name);
                                                    $sHtmlString .= '<br>';
                                                        $sHtmlString .= '<a class="" target="blank" href="'.route('utility.view-file',['c_'.$oComment->id_comment,$oComment->display_file_name]).'">'.trans('messages.view').'</a>';
                                                        $sHtmlString .= '<a class="" target="blank" href="'.route('utility.download-file',['c_'.$oComment->id_comment,$oComment->display_file_name]).'">'.trans('messages.download').'</a>';
                                                    $sHtmlString .= '</span>';
                                                    $sHtmlString .= '</div>';

                                                    echo $sHtmlString;
                                                    break;
                                                default :
                                                    //Code for text/code
                                                    echo 'No file found';
                                            }
                                        ?>
                                        @endif
                                    </div>
                                </div>
                            </div>
                    <?php
                        }
                    ?>
                    <!-- extra comment ends here-->                
                @endif
            </div>
        </div>
        
        @if(count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL)
        <!-- Div for add comment -->
        <div class="user-comment-box">
            <div class="user-comment-box-padding clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding clearfix">
                    <form id="add_comment_{{ $oFeed->id_post }}" name="add_comment" action="{{ route('post.add-comment') }}" method="POST" enctype="multipart/form-data" class="add_comment">
                        {!! csrf_field() !!}
                        <div class="icon-image img-30 display-ib">
                            {!! setProfileImage("50",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                        </div>
                        <input type="hidden" name="id_post" value="{{ $oFeed->id_post }}">
                        <input id="comment_type_{{ $oFeed->id_post }}" name="comment_type" type="hidden" value="{{ (old('comment_type_'.$oFeed->id_post)) ? old('comment_type_'.$oFeed->id_post) : config('constants.POSTTYPETEXT') }}">
                        <div class="input-group">
                            <textarea class="form-control animated" name="comment_text_{{ $oFeed->id_post }}" value="{{old('comment_text_'.$oFeed->id_post)}}" placeholder="{{ trans('messages.add_comment') }}" aria-describedby="basic-addon1"></textarea>
                            <span class="error_message" id="comment_text_{{ $oFeed->id_post }}"></span>
                            <span class="comment-attchment" >
                                <label><img src="{{ asset('assets/web/img/attachment-icon.png') }}"  class="no-border" onclick="setPostCommentType('{{ config('constants.POSTTYPEDOCUMENT') }}','{{ $oFeed->id_post }}');"></label>
                                <label><img src="{{ asset('assets/web/img/camera-icon.png') }}" onclick="setPostCommentType('{{ config('constants.POSTTYPEIMAGE') }}','{{ $oFeed->id_post }}');"></label>
                            </span>
                            <div class="input-group" id="file-input">
                                <label class="input-group-btn file-attachment-btn">
                                    <span class="btn btn-primary">
                                        {{ trans('messages.browse') }} <input type="file" name="file_{{ $oFeed->id_post }}"   multiple="">
                                    </span>
                                </label>
                                <input type="text" class=" file-attachment-name" readonly="">
                                <span class="file-attachment-cancel" onclick="setPostCommentType('{{ config('constants.POSTTYPETEXT')}}','{{ $oFeed->id_post }}');" data-id="{{ $oFeed->id_post }}">{{ trans('messages.cancel') }}</span>
                            </div> 
                            @if($errors->has('file_'.$oFeed->id_post))
                                <span class="error_message">{{ $errors->first('file_'.$oFeed->id_post) }}</span>
                            @endif
                        </div>
                        <button type="submit" class="btn btn-primary btn-md btn-comment" data-id='{{$oFeed->id_post}}' id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>">{{ trans('messages.send') }}</button>
                    </form>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-2 col-xs-1"></div>
            </div>
        </div>
        @endif
    </div>
@endforeach


<script type="text/javascript">
     $(".comments-number").click(function(){
            $(this).toggleClass("open");
            $(this).siblings(".comments-parent").slideDown();
            $(this).siblings(".comments-parent").toggleClass("show-all-child");
        });
        $(function(){
            setAutosizeTextAreaHeight(30);
            callAutoSizeTextArea();
       });
       
       $('.report').on('click', function(evt) {
            evt.stopImmediatePropagation();
            $(this).parent().addClass("containsForm");
            
       });
       $('.remove-form').on('click', function(evt) {
            evt.stopImmediatePropagation();
            $('.report-error').remove();
            $(this).parents(".report-post").removeClass("containsForm");
            $(this).parents(".edit-links").removeClass("open");
            
       });
       /*js forshow less and more image caption*/
        showMoreAmountdata(500,'groupFeedsMore');
        showMoreAmountdata(200,'groupCommentMore');
        $(".groupFeedsMore .morelink").on("click", function(e){
           setMoreAmountDataLabel(this);
           e.preventDefault();
        });
        
        $(".groupCommentMore .morelink").on("click", function(e){
           setMoreAmountDataLabel(this);
           e.preventDefault();
        });

//     $(document).ready(function(){
//        $(".comment-attchment label:first-child").click(function(){
//              $(this).parent().siblings("#file-input").addClass("show-input");
//              $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
//              $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any document ");
//        });
//        $(".comment-attchment label:last-child").click(function(){
//              $(this).parent().siblings("#file-input").addClass("show-input");
//              $(this).parent().siblings("#file-input").children(".file-attachment-name").val('');
//              $(this).parent().siblings("#file-input").children(".file-attachment-name").attr("placeholder", " Share any photo");
//        });
//        $(".file-attachment-cancel").click(function(){
//            var id = $(this).attr('data-id');
//            $('#file_'+id).val('').clone(true);;
//            $(this).parent().removeClass("show-input");
//            input = $(".file-attachment-name").val('');
//        });
//        findUrlThumbnails();
//    });
</script>