<div class="clearfix col-lg-12 background-white round-coreners-8">
    <h3 class="m-t-3">{{ trans('messages.group_categories') }}</h3>
    @foreach($oGroupCategoryList as $oGroupCategory)
    <div class="clearfix m-t-1">
        <a onclick="searchGroup('{{ $oGroupCategory->id_group_category }}');" href="javascript:void(0);">{{ $oGroupCategory->group_category_name }}</a>
    </div>
    @endforeach
</div>
<!-- file not in use -->