<div id="tab_group_admin" class="group-list-bg clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form id="group_search"  class="clearfix" name="group_search" method="POST" enctype="multipart/form-data" onsubmit="searchGroup('/group/admin-group-listing-ajax/','/group/admin-group-listing/');return false;">
            <div class="input-group group-search pull-right">
                <input id="search_group" type="text" class="form-control" name="search_param"  placeholder="{{ trans('messages.search_group') }}" value="{{ $sSeacrhStr }}" required="required">
                <button type="submit" onclick="searchGroup('/group/admin-group-listing-ajax/','/group/admin-group-listing/');return false;">
                    <i class="glyphicon glyphicon-search"></i>
                </button>  
            </div>
        </form>
    </div>
    @if(count($oGroupList['admin_groups']) > 0)
       @include('WebView::group._more_admin_group_listing')
    @else
    <div class="no-data">
          <img src="{{asset('assets/web/img/no-group.png')}}">
          {!! trans('messages.no_admin_group') !!}
    </div>
    @endif
</div>
<script type="text/javascript">
    var nCurrentPage = <?php echo $oGroupList['admin_groups']->currentPage(); ?>;
    var nLastPage = <?php echo $oGroupList['admin_groups']->lastPage(); ?>;

    $(window).scroll(function (event) {
        if ($('#tab_group_admin').length){
            if($(window).scrollTop() + $(window).height()+50 >= $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;
                    loadNewData('tab_group_admin', '<?php echo route('group.admin-group-listing-ajax'); ?>/'+$('#search_group').val(), nCurrentPage);
                }
            }
        }
    });
</script>