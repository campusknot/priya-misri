@extends('layouts.web.main_layout')

@section('title', 'Campusknot-Group Search')

@section('content')
<!-- FileName :: group_search_listing(not in use) -->
<form id="group_search" class="clearfix" name="group_search" action="{{ route('group.group-search') }}" method="GET" enctype="multipart/form-data" onsubmit="searchGroup();">
        <div class="input-group group-search pull-right">
            <input id="search_group" type="text" class="form-control" name="search_param"  placeholder="{{ trans('messages.search_group') }}" >
                
            <button type="submit" onclick="searchGroup();">
                <i class="glyphicon glyphicon-search"></i>
            </button>  
        </div>
    </form>
    <div class="group-list clearfix">
        @if(count($oGroupList) > 0)
            <div class=" clearfix">
            <?php
                $nCount = 0;
                foreach($oGroupList as $oGroupDetails)
                {
                    if($nCount > 0 && $nCount%3 == 0)
                    {
                        echo '</div><div class="row clearfix">';
                    }
            ?>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 group">
                        <a href=" {{ route('group.group-feeds', ['nIdGroup' => $oGroupDetails->id_group]) }}" title="{{ trans('messages.click_here', ['click_hint' => 'to view '.$oGroupDetails->group_name.' group.']) }}">
                            <div class="group-bg">    
                            <div class="group-icon">
                                    <?php $sGroupImgPath = ($oGroupDetails->group_image) ? config('constants.MEDIAURL').'/groups/'.$oGroupDetails->group_image : asset('/assets/web/img/group-categories').'/'.$oGroupDetails->group_category_image; ?>
                                    <img src="{{ $sGroupImgPath }}" alt="{{ $oGroupDetails->group_name }}" >
                                </div>
                                <div class="group-info">
                                    <span class="mightOverflow" data-toggle="tooltip" data-placement="bottom" title="" nowrap>
                                        {{ $oGroupDetails->group_name }}
                                    </span>
                                    {{ $oGroupDetails->group_category_name }}
                                </div>
                            </div> 
                            </a>

                    </div>
            <?php
                    $nCount ++;
                }
            ?>
            </div>
        @else
            <div class="clearfix col-lg-12 background-white round-coreners-8 p-3">{{ trans('messages.no_record_found') }}</div>
        @endif
    </div>

@stop