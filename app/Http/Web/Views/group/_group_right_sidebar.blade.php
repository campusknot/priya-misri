<div class="clearfix">
    <div class="pull-left">
        <h2 class="m-t-3"> {{ trans('messages.about_group') }} </h2>
    </div>
    @if(count($oGroupMemberDetail) && (($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')) || ($oGroupMemberDetail->member_type == config('constants.GROUPADMIN'))))
    <div class="pull-right m-t-3">
        <a href="{{ route('group.group-settings', ['nIdGroup' => $oGroupDetails->id_group]) }}" class="col-lg-1 edit-group-info-button glyphicon glyphicon-edit" onclick="openEditInfo('edit_group_info', 'group_info');"> </a>
    </div>
    @endif
</div>
<div class="m-b-3">
    <span for="group_info">{{ empty($oGroupDetails->about_group)? trans('messages.no_group_info_available') : $oGroupDetails->about_group }}</span>
</div>