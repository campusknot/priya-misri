<div class="clearfix col-lg-12 ck-background-white round-coreners-5 group-type">
    <h3 class="m-t-3">{{ trans('messages.group_categories') }}</h3>
    @foreach($oGroupCategoryList as $oGroupCategory)
    <div class="clearfix m-t-1">
        <a href="{{ route('group.group-listing', ['nIdGroupCategory' => $oGroupCategory->id_group_category]) }}">{{ $oGroupCategory->group_category_name }}</a>
    </div>
    @endforeach
</div>