<div class="clearfix">
    @if(count($oGroupDetails) && $sPage=='setting')
    <div class="inner-page-heading padding-10 pos-relative">
        <h3>{{ trans('messages.group_settings') }}</h3>
        <span class="pull-right more-options" data-toggle="dropdown">
                <img src="{{ asset('assets/web/img/more-option.png') }}" />
            </span>
            <ul class="dropdown-menu group-options arrow-up">
              <li id="active_leave" class="">
                    <span id="leave_group"   title="{{ trans('messages.click_here', ['click_hint' => 'to '.trans('messages.leave_group')]) }}">
                        {{ trans('messages.leave_group') }} 
                    </span>
                </li>
            </ul>
    </div>
    <div class="innerpage">
        <div class="content-area" id="edit_group_info">
            <ul class="nav nav-tabs ck-tabs">
                <li class="active"><a data-toggle="tab" href="#g_setting">{{ trans('messages.about_group') }}</a></li>
                @if($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
                <li><a data-toggle="tab" href="#admin_list">{{trans('messages.admin')}}</a></li>
                @endif
              </ul>

            <div class="tab-content ck-tab-content clearfix">
                <div id="g_setting" class="tab-pane fade in active">
                    <form id="about_group_form" name="about_group_form" method="POST" enctype="multipart/form-data" action="{{ route('group.group-settings', ['nIdGroup' => $oGroupDetails->id_group]) }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id_group" value="{{ $oGroupDetails->id_group }}">
                    <div class="form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @if($oGroupMemberDetail->member_type != config('constants.GROUPCREATOR') || 
                            ($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR') && $oGroupMemberDetail->id_group_category ==  config('constants.COURSEGROUPCATEGORY')))
                                <input type="text" id="group_name" name="group_name" class="form-control"  placeholder="{{trans('messages.group_name')}}" value="{{ (old('group_name')) ? old('group_name') : $oGroupDetails->group_name }}"  readonly="readonly">
                               
                            @elseif($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
                                <input type="text" id="group_name" name="group_name" class="form-control"  placeholder="{{trans('messages.group_name')}}" value="{{ (old('group_name')) ? old('group_name') : $oGroupDetails->group_name }}">
                            @else
                                <input type="text" id="group_name" class="form-control"  placeholder="{{trans('messages.group_name')}}" value="{{ (old('group_name')) ? old('group_name') : $oGroupDetails->group_name }}">
                                <input type="hidden" name="group_name" value="{{ (old('group_name')) ? old('group_name') : $oGroupDetails->group_name }}" >
                            @endif
                            
                            
                            @if ($errors->has('group_name'))
                                <span class="error_message"><?php echo $errors->first('group_name'); ?></span>
                            @endif
                        </div>
                    </div>
                   
                    <div class="form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <select id="groupcategory" class="form-control" name="group_category" disabled="disabled">
                                <option value="">{{ trans('messages.select_group_category') }}</option>
                                @foreach($aGroupCategories as $aGroupCategory)
                                    <option value="{{ $aGroupCategory['id_group_category'] }}" {{ ($aGroupCategory['id_group_category'] == $oGroupDetails->id_group_category) ? "selected='selected'" : '' }}>{{ $aGroupCategory['group_category_name'] }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('group_category'))
                                <span class="error_message"><?php echo $errors->first('group_category'); ?></span>
                            @endif
                        </div>
                    </div>
                     @if($oGroupMemberDetail->id_group_category ==  config('constants.COURSEGROUPCATEGORY'))
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 ">
                             <div class="form-group clearfix label-input-parent disabled">
                                <label>{{trans('messages.section')}}</label>
                                <input type="text" id="" name="" class="form-control"  placeholder="" value="{{ $oGroupDetails->section }}" disabled />
                             </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group clearfix label-input-parent disabled">
                                <label>{{trans('messages.semester')}}</label>
                                <input type="text" id="" name="" class="form-control"  placeholder="" value="{{ $oGroupDetails->semester }}" disabled />
                            </div>
                        </div>
                    @endif
                    <div class="form-group clearfix">
                        <div id="auto-suggetion-div" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <textarea type="text" id="about_group" class="form-control" name="about_group" placeholder="{{trans('messages.about_group') }}">{{ $oGroupDetails->about_group }}</textarea>
                        </div>
                    </div>
                    <div class=" form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php $sDisabled='';
                            if($oGroupMemberDetail->id_group_category ==  config('constants.COURSEGROUPCATEGORY'))
                                $sDisabled = "disabled='disabled'";
                            ?>
                            <div class="group-icons clearfix">
                                <input type="radio" name="group_type" id="public_group" value="{{ config('constants.PUBLICGROUP') }}" {{ (config('constants.PUBLICGROUP') === $oGroupDetails->group_type) ? "checked='checked'" : '' }} {{ $sDisabled }}/>
                                <input type="radio" name="group_type" id="private_group" value="{{ config('constants.PRIVATEGROUP') }}" {{ (config('constants.PRIVATEGROUP') === $oGroupDetails->group_type) ? "checked='checked'" : '' }} {{ $sDisabled }}/>
                                <input type="radio" name="group_type" id="secret_group" value="{{ config('constants.SECRETGROUP') }}" {{ (config('constants.SECRETGROUP') === $oGroupDetails->group_type) ? "checked='checked'" : '' }} {{ $sDisabled }}/>

                                <label for="public_group" class="group-type-radio-buttons group-type-title-public">
                                    <div class="group-type-icon-public">
                                       <img src="{{ asset('assets/web/img/public_group.png') }}" />
                                    </div>
                                    {{ trans('messages.PUB') }}
                               </label>
                               <label for="private_group" class="group-type-radio-buttons group-type-title-private">
                                   <div class="group-type-icon-private">
                                       <img src="{{ asset('assets/web/img/private_group.png') }}" />
                                   </div>
                                   {{ trans('messages.PVT') }}
                               </label>
                               <label for="secret_group" class="group-type-radio-buttons group-type-title-secret" >
                                    <div class="group-type-icon-secret">
                                       <img src="{{ asset('assets/web/img/secret_group.png') }}" />
                                   </div>
                                   {{ trans('messages.SEC') }}
                               </label>
                            </div>
                        </div>
                        @if ($errors->has('group_type'))
                            <span class="error_message"><?php echo $errors->first('group_type'); ?></span>
                        @endif
                    </div>
                    <div class="form-group clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  submit-options ">
                            <button type="submit" name="submit" class="btn btn-primary pull-right" value="{{ trans('messages.save') }}">{{ trans('messages.save') }}</button>
                        </div>
                    </div>
            </form>
                </div>
                @if($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
                <div id="admin_list" class="tab-pane fade">
                    
                    <ul class="Col-lg-12 col-md-12 col-sm-12 col-xs-12 group-admins-list">
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <span class="icon-image img-35 mr-10">
                                {!! setProfileImage("150",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                            </span>
                            <span class="name-width">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
                            <span class="pull-right group-admin-label">{{ trans('messages.group_creator') }}</span>
                        </li>
                    @if(count($oAdminMember) > 0)
                        @foreach($oAdminMember as $oAdmin)
                            <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="icon-image img-35 mr-10">
                                    {!! setProfileImage("50",$oAdmin->user_profile_image,$oAdmin->id_user,$oAdmin->first_name,$oAdmin->last_name) !!}
                                </span>
                                <span class="name-width">{{ $oAdmin->name }}</span>
                                <input type="hidden" class="form-control get_member_id_user" value="{{ $oAdmin->id_user }}"/>
                                <span class="pull-right group-admin-label" onclick="removeAdmin(this,'{{ config('constants.GROUPMEMBER') }}');">Remove</span>
                            </li>
                        @endforeach
                    @endif
                        <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <span class="add-group-admin">{{ trans('messages.add_new_admin') }}</span>
                            <div class="admin-inputs hidden">
                                <input type="text" class="form-control" id="get_member" placeholder="{{ trans('messages.enter_admin_name') }}"/>
                                <input type="hidden" class="form-control get_member_id_user" id="get_member_id_user"/>
                                <button type="button"  class="btn btn-primary" onclick="addAdmin(this,'{{ config('constants.GROUPADMIN') }}');">{{ trans('messages.add') }}</button>
                                <button type="button"  class="btn btn-cancel ck-disable-btn">{{ trans('messages.cancel') }}</button>
                                
                            </div>
                        </li>
                    
                    </ul>
                    
                </div>
                @endif
               
            </div>
           
        </div>
    </div>
    @else
    <div class="inner-page-heading padding-10 pos-relative">
        <h3>{{ trans('messages.about_group') }}</h3>
        @if(count($oGroupMemberDetail) && (($oGroupMemberDetail->member_type == config('constants.GROUPADMIN')) || ($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR')) || ($oGroupMemberDetail->member_type == config('constants.GROUPMEMBER'))))
            <span class="pull-right more-options" data-toggle="dropdown">
                <img src="{{ asset('assets/web/img/more-option.png') }}" />
            </span>
            <ul class="dropdown-menu group-options arrow-up">
              <li id="active_leave" class="">
                    <span id="leave_group"  title="{{ trans('messages.click_here', ['click_hint' => 'to '.trans('messages.leave_group')]) }}">
                        {{ trans('messages.leave_group') }} 
                    </span>
                </li>
            </ul>
        @endif
    </div>
    <div class="innerpage clearfix">
        <div class="col-lg-12 no-data-found  ">
            @if($oGroupDetails->about_group == '')
            <div class="no-data">
                <img src="{{ asset('assets/web/img/no-group.png') }}" />
                {{ trans('messages.about_group_text') }}
            </div>
            @else
            <div class="about-group-text">
                <p> {!! $oGroupDetails->about_group !!} </p>
            </div>
            
            @endif
        </div>
    </div>
    
    @endif
</div>

<script>
    function removeAdmin(ele,sMemberType){
        var user_id = $(ele).siblings('.get_member_id_user').val();
        swal(
        {
            title: "",
            text: "{{ trans('messages.admin_remove_confirmation') }}",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm)
        {
            if (isConfirm) {
                $.ajax({
                    type: "GET",
                    url: "{{ route('group.change-group-member-type',[$oGroupDetails->id_group]) }}?user_id="+user_id+"&member_type="+sMemberType,
                    success: function (data) {
                           $(ele).parent().remove();
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
            }
        });
    }
    /*leave group */
    groupId = <?php echo $oGroupDetails->id_group ?>;
    leaveGroupMessage =  "<?php echo trans('messages.leave_group_confirmation') ?>";
    document.getElementById('leave_group').onclick = function() {
        leaveGroup(leaveGroupMessage,groupId);
      };
    $(".add-group-admin").click(function(){
        $(".admin-inputs").removeClass("hidden");
    });
           
    $(".btn-cancel").click(function(){
        $(".admin-inputs").addClass("hidden");
        $("#get_member").val('');
        $("#get_member_id_user").val('');
    });
    $(function()
    {
        $('#get_member').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('group.member-suggestion',[$oGroupDetails->id_group]) }}"+"?member_str=" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.name,
                            email: value.email,
                            desc: value.id_user
                        };

                    }));
                });
            },
            minLength: 2,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {

            },
            select: function(event, ui) {
                event.stopPropagation();
                $("input#get_member").val(ui.item.label+' ('+ui.item.email+')');
                $("#get_member_id_user").val(ui.item.desc);
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span>" + item.label + " (" + item.email + ")</span>" )
                .appendTo( ul );
            };
    });
    function addAdmin(ele,sMemberType){
        var user_id = $(ele).siblings('.get_member_id_user').val();
        $.ajax({
            type: "GET",
            url: "{{ route('group.change-group-member-type',[$oGroupDetails->id_group]) }}?user_id="+user_id+"&member_type="+sMemberType,
            success: function (data) {
                var html = '<li class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><span class="icon-image img-35 mr-10">';
                    html+=data.profile_img;
                    html+='</span><span class="name-width">'+ data.name +'</span>';
                    html+='<input type="hidden" class="form-control get_member_id_user" value="'+ data.id_user+'"/>';
                    html+="<span class='pull-right group-admin-label' onclick='removeAdmin(this,\'M\');'>Remove</span></li>";
                    $(ele).parent().parent().before(html);
                    $('#get_member').val('');
                    $('#get_member_id_user').val('');
            },
            error: function (data) {
                location.reload();
            }
        });
    }
</script>