@if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
    <div class="ck-background-white">
        <div class="no-data ">
            <img src="{{ asset('assets/web/img/private-group.png') }}">
            <div>{{ $oGroupDetails->about_group }}</div>
            <div class="sml-text">
            @if($oGroupDetails->group_type == config('constants.PRIVATEGROUP'))    
                {{ trans('messages.private_group_member') }}
            @else
                {{ trans('messages.secret_group_member') }}
            @endif
            </div>
        </div>
    </div>
@else
    <div class="clearfix">
        @if(count($oGroupRequest)>0)
            <div class="innerpage  group-member-request clearfix ">
                <div class="inner-page-heading padding-10">
                    <h3>{{ trans ('messages.group_join_request_mail_heading') }}</h3>   
                    
                </div>
                
                <div class="invited-member-status">
                    <div class="request-listing clearfix">
                        <ul class="status-header clearfix">
                            <li class="col-lg-8 col-md-8 col-sm-8 col-xs-8">{{ trans('messages.name') }}</li>
                            <li class="col-lg-4 col-md-4 col-sm-4 col-xs-4">{{ trans('messages.action') }}</li>
                        </ul>
                        <?php 
                            foreach($oGroupRequest as $GroupRequest)
                            {
                        ?>
                                <ul class="status-list col-lg-12 clearfix">
                                    <li class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="icon-image img-35">
                                            {!! setProfileImage("50",$GroupRequest->user_profile_image,$GroupRequest->id_user,$GroupRequest->first_name,$GroupRequest->last_name) !!}
                                        </span>
                                        <span>{{ $GroupRequest->first_name.' '. $GroupRequest->last_name }}</span>
                                    </li>
                                    <li class="col-lg-4 col-md-4 col-sm-4 col-xs-4 request-action" id="group_{{ $GroupRequest->id_group_request }}">
                                        <a href="javascript:void(0)" value="Accept" onclick="UpdateNotificationStatus('A', '{{ $GroupRequest->id_group_request }}' ,'GM_LIST')" class="accept">{{ trans('messages.approve') }}</a>
                                        <a href="javascript:void(0)" value="Decline" onclick="UpdateNotificationStatus('R', '{{ $GroupRequest->id_group_request }}','GM_LIST')" class="decline">{{ trans('messages.dis_approve') }}</a>
                                    </li>
                                </ul>
                        <?php  } ?>
                    </div>   
                </div>
            </div>
        @endif
        @if(count($oGroupDetails))
            <div class="inner-page-heading padding-10 pos-relative">
                <h3>{{trans('messages.group_members_list')}}</h3>
                
                @if(count($oGroupMemberDetail) && ($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR') || $oGroupMemberDetail->member_type == config('constants.GROUPADMIN')))
                     <span class="pull-right more-options">
                        <form method="post" action="{{ route('group.group-member-excel',['nIdGroup' => $oGroupDetails->id_group]) }}" id="excel" class="text-center">
                            {!! csrf_field() !!}
                            <input type="hidden" id="id_group" name="id_group" value="{{ $oGroupDetails->id_group }}"/>
                            <input type="submit" value="{{trans('messages.download_csv')}}" id="excel_download" class="btn btn-primary btn-xs mt-10">
                        </form>
                    </span>
                @endif
            </div>
            <div>
                <div class="clearfix member-list">
                    <form id="group_search" name="group_search" onsubmit="searchdata('{{ route('group.group-member-ajax',['nIdGroup' => $oGroupDetails->id_group]) }}','{{ route('group.group-member',['nIdGroup' => $oGroupDetails->id_group]) }}','search_member','member-list');return false">
                        <div class="input-group group-search">                
                            <input id="search_member" type="text" class="form-control" name="search_member" placeholder="{{ trans('messages.search_group_member') }}" value="{{ ($sSeacrhStr) ? $sSeacrhStr : '' }}" >
                            <button type="submit">
                                <i class="glyphicon glyphicon-search"></i>
                            </button>              
                        </div>
                    </form> 

                    <div id="member-list">
                    @include('WebView::group._more_group_members')
                    </div>
                </div>
            </div>
        @else
        <div class="col-lg-12 no-data-found m-t-2 ck-background-white round-coreners-8">
            <h2> {{ trans('messages.no_record_found') }} </h2>
        </div>
        @endif
    </div>
@endif

<script type="text/javascript">
    var nCurrentPage = <?php echo $oGroupMemberList->currentPage(); ?>;
    var nLastPage = <?php echo $oGroupMemberList->lastPage(); ?>;

    $(document).scroll(function (event) {
        if(window.location.href == '<?php echo route('group.group-member', ['nIdGroup' => $oGroupDetails->id_group,'sSeacrhStr' => $sSeacrhStr]); ?>')
        {
            if($(window).scrollTop() + $(window).height()+20 >= $(document).height()) {
                if(nCurrentPage < nLastPage && !nLoadNewDataStatus) {
                    nCurrentPage +=1;   
                    var searchStr = $('#search_member').val();
                    loadNewData('member-list', '<?php echo route('group.group-member', ['nIdGroup' => $oGroupDetails->id_group]); ?>/'+searchStr,nCurrentPage);
                }
            }
        }
    });
    
    $('#search_member').keyup(function(){
        var text = $('#search_member').val();
        if(text.length > 0)
        searchdata('<?php echo route('group.group-member-ajax',['nIdGroup' => $oGroupDetails->id_group]);?>','<?php echo route('group.group-member',['nIdGroup' => $oGroupDetails->id_group]); ?>','search_member','member-list');
    });
</script>