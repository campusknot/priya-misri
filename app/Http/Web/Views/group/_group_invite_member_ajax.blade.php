<div class="clearfix">
    @if(count($oGroupDetails))
        <div class="inner-page-heading padding-10">
            <h3> {{ trans('messages.invite_users') }} </h3>
        </div>
        <div class="innerpage">
            
            <div class="content-area invite-member">
                @if ($errors->has('invite_to'))
                    <div class="alert alert-error">
                        <strong class="error_message">{!! $errors->first('invite_to') !!}</strong>
                    </div>
                @endif
                <div class="clearfix">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form id="invite_user" name="invite_user" method="POST" enctype="multipart/form-data" action="{{ route('group.invite-member', ['nIdGroup' => $oGroupDetails->id_group]) }}">
                            {!! csrf_field() !!}
                            <input type="hidden" name="id_group" value="{{ $oGroupDetails->id_group }}">
                            <div class="form-group pos-relative">
                                <div class="">
                                    <input type="text" id="user_name" class="form-control" name="invite_to[]" value="{{ old('user_name') }}" placeholder="{{trans('messages.user_name') }}" />
                                </div>
                                <div id="selected_emails" class="auto-suggetion-outer">
                                    @if($errors->has('invite_to') && old('invite_to'))
                                        @foreach(old('invite_to') as $sInvity)
                                            <div id='vR_{{$sInvity}}' class='vR pull-left m-r-1'>
                                                <span email='{{$sInvity}}' class='vN'>
                                                    <div class='vT invalid-email'>{{$sInvity}}</div>
                                                    <div id='{{$sInvity}}' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>
                                                </span>
                                                <input type='hidden' value='{{$sInvity}}' name='invite_to[]'>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div id="auto-suggetion-div"></div>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix no-padding">
                                <div class="input-group pull-right submit-options">
                                    <button onclick="clearInputs('invite_user');" class="btn btn-default pull-right" type="button">{{ trans('messages.cancel') }}</button>
                                    <button type="submit" name="submit" class="btn btn-primary btn-primary-custom pull-right" value="{{ trans('messages.send') }}" id="btn-send">{{ trans('messages.send') }}</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="inner-page-heading padding-10">
            <h3> {{ trans('messages.invite_users_csv') }} </h3>
        </div>
        <div class="innerpage">

                 <div class="content-area">
                     @if ($errors->has('csv_invite_to'))
                         <div class="alert alert-error">
                             <strong class="error_message">{{ $errors->first('csv_invite_to') }}</strong>
                         </div>
                     @endif
                     <div class="clearfix">
                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 invite-member-csv">
                             <form id="invite_user_csv" name="invite_user_csv" method="POST" enctype="multipart/form-data" action="{{ route('group.invite-member-via-csv') }}">
                                 {!! csrf_field() !!}
                                 <input type="hidden" name="id_group" value="{{ $oGroupDetails->id_group }}">
                                 <div class="form-group pos-relative">

                                     <div id="add_file" class="clearfix m-t-3" title="{{ ($errors->has('extention')) ? $errors->first('extention') : '' }}" data-toggle="tooltip"  data-placement="top" >
                                         <div class="form-group">
                                             <a href="{{asset('/assets/sample.csv')}}">{{ trans('messages.download_csv_sample') }}</a>
                                             <div class="input-group {{($errors->has('extention')) ? 'error' : '' }}" id="file-input">
                                                 <label class="input-group-btn file-attachment-btn">
                                                     <span class="btn btn-primary">
                                                         Browse… <input type="file" name="file" accept=".csv" id="csv_file">
                                                     </span>
                                                 </label>
                                                 <input type="text" class="file-attachment-name" readonly="">
                                             </div>
                                             @if ($errors->has('file'))
                                                 <span class="error_message"><?php echo $errors->first('file'); ?></span>
                                             @endif
                                             @if ($errors->has('extention'))
                                                 <span class="error_message"><?php echo $errors->first('extention'); ?></span>
                                             @endif
                                         </div>
                                     </div>
                                 </div>

                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 clearfix no-padding">
                                     <div class="input-group pull-right submit-options">
                                         <button class="btn btn-default pull-right csv_file_cancel" type="button">{{ trans('messages.cancel') }}</button>
                                         <button type="submit" name="submit" class="btn btn-primary btn-primary-custom pull-right" value="{{ trans('messages.send') }}" id="btn-send">{{ trans('messages.send') }}</button>
                                     </div>
                                 </div>
                             </form>
                         </div>
                     </div>
                 </div>

             </div>
        <div class="inner-page-heading padding-10">
            <h3> {{ trans('messages.all_request_status') }} </h3>
        </div>
        <div class="innerpage clearfix">
            
            <div id="invited_member_status" class="invited-member-status"></div>
        </div>
    @else
    <div class="col-lg-12 no-data-found m-t-2 background-white round-coreners-8">
        <h2> {{ trans('messages.no_record_found') }} </h2>
    </div>
    @endif
</div>
<script type="text/javascript">
        $(function()
        {
            $('#user_name').autocomplete({
                source: function (request, response) {
                    $.getJSON("{{ route('group.auto-suggetions-for-invite-user', ['nIdGroup' => $oGroupDetails->id_group]) }}"+"?term=" + request.term, function (data) {
                        response($.map(data, function (value, key) {

                        return {
                                label: value.name,
                                value: value.email,
                                desc: value.id_user
                            };
                            
                        }));
                    });
                },
                minLength: 3,
                appendTo: "#auto-suggetion-div",
                autoFill: false,
                matchContains: false,
                highlightItem: true,
                delay: 500,
                focus: function( event, ui ) {
                    
                },
                select: function(event, ui) {
                    var sHtml = "<div id='vR_"+ui.item.desc+"' class='vR pull-left m-r-1'>";
                        sHtml += "<span email='"+ui.item.value+"' class='vN'>";
                        sHtml += "<div class='vT'>"+ui.item.label+"</div>";
                        sHtml += "<div id='"+ui.item.desc+"' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>";
                        sHtml += "</span>";
                        sHtml += "<input type='hidden' value='"+ ui.item.value + "' name='invite_to[]'>";
                        sHtml += "</div>";
                    $('#selected_emails').append( sHtml );
                    $("#user_name").val('');
                    return false;
                }
            })
            .autocomplete( "instance" )._renderItem = function( ul, item ) {
                return $( "<li class='auto-suggetion'></li>" )
                    .append( "<span>" + item.label + ": &lt;" + item.value + "&gt;</span>" )
                    .appendTo( ul );
                };
        });
        
        $(document).on('keydown', 'input#user_name', function(e) { 
            var keyCode = e.keyCode || e.which; 

            if (keyCode == 9 || keyCode == 188) { 
                e.preventDefault();
                // call custom function here
                if( $(this).val().length !== 0 ) {
                    var sColorClass = (isValidEmailAddress($(this).val())) ? '' : 'invalid-email';
                    
                    var sHtml = "<div id='vR_"+$(this).val()+"' class='vR pull-left m-r-1'>";
                        sHtml += "<span email='"+$(this).val()+"' class='vN'>";
                        sHtml += "<div class='vT "+sColorClass+"'>"+$(this).val()+"</div>";
                        sHtml += "<div id='"+$(this).val()+"' class='vM glyphicon glyphicon-remove' onclick='removeInvity(this);'></div>";
                        sHtml += "</span>";
                        sHtml += "<input type='hidden' value='"+ $(this).val() + "' name='invite_to[]'>";
                        sHtml += "</div>";
                    $('#selected_emails').append( sHtml );
                    $(this).val('');
                }
            } 
        });
        
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
    $(':file').on('fileselect', function(event, numFiles, label) {
        showCustomBrowseButton(event, numFiles, label, this);
    });
    
    $(document).ready(function() {
        $('.csv_file_cancel').click(function(){
            $('#csv_file').val('');
            $('#csv_file').replaceWith($('#csv_file').val('').clone(true));
            $(".file-attachment-name").val('');
        });
        getGroupRequestStatusData(<?php echo $oGroupDetails->id_group; ?>, 1);
    });
</script>