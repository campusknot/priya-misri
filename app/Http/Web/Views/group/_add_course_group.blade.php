<!-- Modal content-->
<div class="modal-content add-new-course-group-popup">
    <form id="add_course_group_form" name="add_course_group_form" method="POST" action="" enctype="multipart/form-data">
        {!! csrf_field() !!}
        <div class="modal-header">
            <button type="button" onclick="closeLightBox();" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title event-title">{{ trans('messages.add_corse_group') }}</h4>
        </div>
        <div class="modal-body clearfix">
            <div class="type-info-icon"> 
                <img src="{{asset('assets/web/img/info.png')}}" class="" >
            </div>
            <div class="type-info-text">
                {{ trans('messages.add_course_group_info') }}
            </div>
            <div class="form-group padding-t-10 clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="text" class="form-control {{ ($errors->has('group_name')) ? 'error' : '' }}"  placeholder="{{trans('messages.group_name')}}" value="" title="{{ ($errors->has('group_name')) ? $errors->first('group_name') : '' }}" data-toggle="tooltip"  data-placement="top" required="required" id="group_name" name="group_name">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div id="auto_suggetion_div" class=""></div>
                    </div>
                </div>
            </div>
            <div class="form-group clearfix Col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                <span class="display-ib v-align">
                    <div class="input-group">
                        <span class="input-group-addon">{{ trans('messages.section') }}</span>
                        <input type="text" class="form-control max-length-four {{ ($errors->has('group_name')) ? 'error' : '' }}" maxlength="4" title="{{ ($errors->has('section_name')) ? $errors->first('section_name') : '' }}" data-toggle="tooltip"  data-placement="top" name="section_name">
                    </div>
                    
                </span>
                <span class="display-ib v-align">
                    <span class="group-attendance-temp pos-relative">
                        <select required class="form-control {{ ($errors->has('semester')) ? 'error' : '' }}" name="semester">
                            <option disabled="disabled" selected="selected" value="" class="display-ib">Semester</option>
                            @if($oCampus->country_name == 'India')
                                <option value="semester-1">Semester-1</option>
                                <option value="semester-2">Semester-2</option>
                                <option value="semester-3">Semester-3</option>
                                <option value="semester-4">Semester-4</option>
                                <option value="semester-5">Semester-5</option>
                                <option value="semester-6">Semester-6</option>
                                <option value="semester-7">Semester-7</option>
                                <option value="semester-8">Semester-8</option>
                            @else
                                <option value="fall">{{ trans('messages.fall') }}</option>
                                <option value="winter">{{ trans('messages.winter') }}</option>
                                <option value="winter-1">{{ trans('messages.winter-1') }}</option>
                                <option value="winter-2">{{ trans('messages.winter-2') }}</option>
                                <option value="spring">{{ trans('messages.spring') }}</option>
                                <option value="summer">{{ trans('messages.summer') }}</option>
                                <option value="summer-1">{{ trans('messages.summer-1') }}</option>
                                <option value="summer-2">{{ trans('messages.summer-2') }}</option>
                            @endif
                        </select>
                        <span class="caret caret-right"></span>
                    </span>
                    
                </span>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <button type="button" onclick="submitAjaxForm('add_course_group_form', '{{ route('group.add-course-group') }}', this )" class="lg-center-button btn btn-primary">{{ trans('messages.create_group') }}</button>
            </div>
        </div>
    </form>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });    
</script>
<script>
    
    $(document).ready(function(){
        $('.selectpicker').selectmenu({
        });
    });

    $(function()
    {
        $('#group_name').autocomplete({
            source: function (request, response) {
                $.getJSON("{{ route('user.course-suggestion') }}"+"?course_name=" + request.term, function (data) {
                    response($.map(data, function (value, key) {

                    return {
                            label: value.course_name,
                            value: value.course_name+'('+value.course_code+')',
                            desc: value.id_course
                        };

                    }));
                });
            },
            minLength: 2,
            appendTo: "#auto_suggetion_div",
            autoFill: false,
            matchContains: false,
            highlightItem: true,
            delay: 500,
            focus: function( event, ui ) {
                //alert('hii');
                $('#group_name').val(  ui.item.value );
            },
            select: function(event, ui) {
                event.stopPropagation();
                $('#group_name').val(  ui.item.value );
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
            return $( "<li class='auto-suggetion'></li>" )
                .append( "<span>" +  item.value +"</span>" )
                .appendTo( ul );
            };
    });
        
    $(document).on('keydown', 'input#group_name', function(e) { 
        var keyCode = e.keyCode || e.which; 
        //alert(keyCode);
        if (keyCode == 9 || keyCode == 188) { 
            e.preventDefault(); 
            // call custom function here
            if( $(this).val().length !== 0 ) {
                
                $('#group_name').val( $(this).val() );
            }
        } 
    });
    
</script>