<div class="share-bg padding-10 clearfix">
    <div class="share clearfix">
         <div id="validation-errors"></div>
        <form id="add_post" name="add_post" action=" {{ route('post.add-post') }}" method="POST" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  clearfix write-post" >
                <div class="icon-image img-50">
                    {!! setProfileImage("50",Auth::user()->user_profile_image,Auth::user()->id_user,Auth::user()->first_name,Auth::user()->last_name) !!}
                </div>
                <input id="post_type" name="post_type" type="hidden" value="{{ config('constants.POSTTYPEPOLL') }}">
                <?php 
                    if(isset($oGroupDetails->group_name))
                        $sPlaceholder= trans('messages.post_placeholder',['name'=>$oGroupDetails->group_name]);
                    else
                        $sPlaceholder= trans('messages.post_placeholder',['name'=>'Followers']);
                    ?>
                @if(isset($oGroupDetails))
                    <input id="id_group" name="id_group" type="hidden" value="{{ $oGroupDetails->id_group }}">
                @endif
                <div class="input-group">
                    <textarea class="form-control" placeholder="{{ $sPlaceholder }}" id="post_text" name="post_text" aria-describedby="basic-addon1">{{ old('post_text') }}</textarea>
                    @if ($errors->has('post_text'))
                       <span class="error_message"><?php echo $errors->first('post_text'); ?></span>
                    @endif
                </div>
                <div id="add_code" class="clearfix m-t-3 {{ ($errors->has('post_textarea')) ? '' : 'hidden' }}">
                    <div class="input-group">
                        <textarea name="post_textarea" id="post_textarea" > </textarea>
                        @if ($errors->has('post_textarea'))
                           <span class="error_message"><?php echo $errors->first('post_textarea'); ?></span>
                        @endif
                    </div>
                </div>
                <div id="add_file" class="clearfix m-t-3">
                    <div class="">
                        <div class="input-group file-attachment" id="file-input">
                            <label class="input-group-btn file-attachment-btn">
                                <span class="btn btn-primary">
                                    Browse <input type="file" name="file" multiple="" accept=".bmp,.jpg,.jpeg,.png,.svg" id="file_form">
                                </span>
                            </label>
                            <input type="text" class=" file-attachment-name" readonly="" placeholder=" Share your pictorial query here ">
                            <span class="file-attachment-cancel" data-toggle="collapse" data-target="#add_document" >Cancel</span>
                        </div>  
                      <!--   <input type="file" name="file" class="form-control" data-input="false" > -->
                        @if ($errors->has('file'))
                            <span class="error_message"><?php echo $errors->first('file'); ?></span>
                        @endif
                    </div>
                </div>
                <div id="add_poll" class="add-poll">
                     <div class="controls" id="profs"> 
                        <div class="input-append">
                            <div id="field">
                                <div class="new-poll-option">
                                    <textarea autocomplete="off" class="input form-control animated" id="field1" name="option[1]" type="text" placeholder="Type your option" data-items="8" maxlength="255" ></textarea>
                                </div>
                                <div class="new-poll-option">
                                    <textarea autocomplete="off" class="input form-control animated" id="field2" name="option[2]" type="text" placeholder="Type your option" data-items="8" maxlength="255"></textarea>
                                </div>
                            </div>
                            <span id="b1" class="add-more" type="button">+ Add more</span>

                        </div>
                        <div class="poll-expire-time">
                            Poll Duration:
                            <span class="poll-expire-day">
                                <span class="caret"></span>
                                <select  class="" name="day">
                                    <option value='0'>Day</option> 
                                    @for ($i = 0; $i < 32; $i++)
                                       <option value='{{ $i }}'>{{ $i }}</option> 
                                    @endfor
                                </select>
                            </span>
                            <span class="poll-expire-hour">
                                <span class="caret"></span>
                                 <select   class="" name="hour">
                                    <option >Hour</option> 
                                    @for ($i = 1; $i < 24; $i++)
                                       <option value='{{ $i }}'>{{ $i }}</option> 
                                    @endfor
                                </select>
                            </span>
                             <span class="poll-expire-min">
                                 <span class="caret"></span>
                                <select  class="" name="minutes">
                                    <option>Min</option> 
                                    @for ($i = 5; $i < 56; $i=$i+5)
                                       <option value='{{ $i }}'>{{ $i }}</option> 
                                    @endfor
                                </select>
                             </span>
                        </div>
                    
                    
                    </div>
                </div>

            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="attachment">
                    <button type="button" class="btn btn-primary btn-lg post-btn" name="save" id="load" data-loading-text="<div class='spinner'> <div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></div>" >{{ trans('messages.post') }}</button>
                </div>
            </div>    
        </form>
    </div>
</div>

<script type="text/javascript">
 $(document).ready(function(){
    var next = 2;
    $(".add-more").click(function(e){
        var count = $("#field").children().length;
        if(count < 10){
            e.preventDefault();
            var addto = "#field";
            //var addRemove = "#field" + (next);
            next = next + 1;
            var newIn = '<div class="new-poll-option"><textarea autocomplete="off" rows="1" placeholder="Type your option" class="input form-control animated" id="field' + next + '" name="option[' + next + ']" type="text" maxlength="255"></textarea><span id="remove' + (next) + '" class="remove-me"  >×</span></div>';
            $(addto).append(newIn);
            /*$('.animated').css('height','36px');*/
//            $('.animated').autosize({append: "\n"});
              callAutoSizeTextArea();

            $("#field" + next).attr('data-source',$(addto).attr('data-source'));
            $("#count").val(next);  
             if(count==9){
                $('.add-more').css('color','#ccc');
            }
        }
       $('.remove-me').on('click',function(e){
                e.preventDefault();
                var fieldNum = this.id.charAt(this.id.length-1);
                var fieldID = "#field" + fieldNum;
                $(this).parent().remove();
                $(fieldID).remove();
                $('.add-more').css('color','#51a2cc');
            
            });
        
    });
    
//    $(function(){
//        $('.animated').css('height','36px');
//        $('.animated').autosize({append: "\n"});
//    });
    /*draggable thing*/
   

    
});

$(document).ready(function($) {
        callAutoSizeTextArea();
    var options = { 
        beforeSubmit:  showRequest,
        success:       showResponse,
        error:showError,
        dataType: 'json' 
        }; 
        $('.post-btn').click(function(e){
            $(this).button('loading');
            $('#add_post').ajaxForm(options).submit();  		
        });
    });		
    function showRequest(formData, jqForm, options) { 
            $("#validation-errors").hide().empty();
            $("#output").css('display','none');
            return true; 
    } 
    function showResponse(response, statusText, xhr, $form)  { 
            if(response.success == true)
            {
                $('#add_post')[0].reset();
                $(".feeds").prepend(response.html);
                $('.post-btn').button('reset');
                location.reload();
                
            } else { 
                var arr = response;
                //alert(arr);
                    $.each(arr, function(index, value)
                    {
                            if (value.length != 0)
                            {
                                    $("#validation-errors").append('<div class="alert alert-error"><strong>'+ value +'</strong><div>');
                            }
                    });
                    $("#validation-errors").show();
                   $('.post-btn').button('reset');
                   //location.reload();
                     
            }
    }
    function showError(xhr, textStatus, errorThrown)  {
        //alert('hii');
        //location.reload();
        //console.log(xhr.responseText);
        var arr = xhr.responseText;
        try {
        $.parseJSON(arr);
           } catch (e) {
               location.reload();
            }
        
        var result = $.parseJSON(arr);
        $("#validation-errors").append('<div class="alert alert-error"><div>');
        $.each(result, function(index, value)
        {
            if (value.length != 0)
            {
                    $("#validation-errors .alert").append('<strong>'+ value +'</strong><br />');
            }
        });
        $("#validation-errors").show();
        $('.post-btn').button('reset');
        
    }
$('.file-attachment-cancel').click(function(){
   $(this).siblings(".file-attachment-name").val(''); 
    //$('#file_form').replaceWith( $("#file_form").clone() );
    $('#file_form').val('');
});
//browse button set file name
    $(':file').on('fileselect', function(event, numFiles, label) {
        showCustomBrowseButton(event, numFiles, label, this);
    });
</script>