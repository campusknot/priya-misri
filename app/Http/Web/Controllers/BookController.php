<?php

namespace App\Http\Web\Controllers;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

class BookController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => [
                                                'callBookListing'
                                            ]
                        ]);
        
        parent::__construct();
        session(['current_page' => 'book']);
    }
    
    public function callBookListing(Request $oRequest)
    {
       return \View::make('WebView::book.book_listing'); 
    }
    
    //book selling popup open
    public function callBookSell(Request $oRequest)
    {
       return \View::make('WebView::book.book_sell'); 
    }
    
    //book user add wishlist
    public function callBookUserWishList(Request $oRequest)
    {
       return \View::make('WebView::book.book_wishlist'); 
    }
}
