<?php
namespace App\Http\Web\Controllers;

use App\Http\Web\Controllers\Controller;

use App\Libraries\Campus;
use App\Libraries\User;
use App\Libraries\UserFollow;
use App\Libraries\UserProfileImages;

use App\Libraries\Post;
use App\Libraries\Poll;
use App\Libraries\Comment;
use App\Libraries\NonMemberInvitation;
use App\Libraries\EventRequest;

use App\Libraries\UserSession;
use App\Libraries\Notification;

use App\Libraries\Course;
use App\Libraries\Group;
use App\Libraries\GroupMember;
use App\Libraries\GroupRequest;
use App\Libraries\GroupEvent;

use App\Libraries\PollOption;
use App\Libraries\PollAnswer;

use App\Libraries\AttendanceCode;
use App\Libraries\AttendanceLog;
use App\Libraries\UserAttendance;
use App\Libraries\Lecture;

use App\Libraries\UserCourse;
use App\Libraries\UserAwards;
use App\Libraries\UserJournals;
use App\Libraries\UserResearch;
use App\Libraries\UserPublications;
use App\Libraries\UserEducation;
use App\Libraries\UserOrganization;
use App\Libraries\UserWorkExperience;

use App\Jobs\SendUserFollowNotification;
use App\Jobs\ShareGroupDocumentsWithNewMember;

use Auth;
use Carbon\Carbon;
use Image;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' =>[
                                                'callUserFeeds' , 'callAccountSettings','getUserSpecificCounts',
                                                'callUserProfile', 'callShowProfile','callShowProfileAjax',
                                                'callFollowUser', 'callUnfollowUser','callDeleteUserImage',
                                                'callShowUserImages', 'callShowUserImagesAjax',
                                                'callAddEducation', 'callDeleteEducation',
                                                'callAddWorkExperience', 'callDeleteWorkExperience',
                                                'callAddOrganization', 'callDeleteOrganization',
                                                'callAddAward', 'callDeleteAward',
                                                'callAddAspiration','callCropImage',
                                                'callAddJournal', 'callDeleteJournal',
                                                'callAddResearch', 'callDeleteResearch',
                                                'callAddPublication', 'callDeletePublication',
                                                'callGetUserContacts','callShowUserContacts','callShowUserContactsAjax', 'callAddUserCourse',
                                                'callUserPoll','callUserAllPoll','callUserAllPollAjax','callUserPollAjax',
                                                'getGlobalSearchDataAjax','getGlobalSearchData',
                                                'callUserGroupSearch','updateUserEducation','callCheckLogin'
                                            ]
        ]);
        
        parent::__construct();
        session(['current_page' => 'profile']);
    }
    
    public function callUserRegistration(Request $oRequest, User $oUser)
    {
        header('X-Frame-Options: SAMEORIGIN');
        if(Auth()->user()) {
            return back();
        }
        
        $aCampuses = array();
        
        if(!empty(old('email')))
        {
            if(filter_var(old('email'), FILTER_VALIDATE_EMAIL))
            {
                $sUrl = explode("@",old('email'))[1];

                $oCampus = new Campus();
                $aCampuses = $oCampus->getCampusFromEmail($sUrl);

                unset($oCampus);
            }
        }
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'first_name' => 'required|without_numeric',
                                        'last_name' => 'required|without_numeric',
                                        'email' => 'required|email|unique:users',
                                        'user_type' => 'required',
                                        'campus' => 'required',
                                        'password' => 'required|min:6',
                                        'confirm_password' => 'required|min:6|same:password'
                                    ];
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $dCurrentTime = Carbon::now();
            $sCurrentTime = $dCurrentTime->toDateTimeString();
            
            $oUser->first_name = ck_ucfirst($oRequest->first_name);
            $oUser->last_name = ck_ucfirst($oRequest->last_name);
            $oUser->email = $oRequest->email;
            $oUser->id_campus = $oRequest->campus;
            $oUser->user_type = $oRequest->user_type;
            $oUser->verification_key = Crypt::encrypt($oRequest->email.config('constants.DEVELOPERSTRING').$oRequest->user_type);
            
            $sSalt = hash('md5', $oRequest->email.$sCurrentTime);
            $sPassword = sha1($oRequest->password.$sSalt);

            $oUser->salt = $sSalt;
            $oUser->password = $sPassword;
            $oUser->activated = 0;
            $oUser->deleted = 0;
            
            $oUser->save();
            $sEmail = $oRequest->email;
            $sName = $oRequest->first_name.' '.$oRequest->last_name;
            $sVerificationKey = $oUser->verification_key;
            
            $oCampus = Campus::find($oRequest->campus);
            $sCampusName = $oCampus->campus_name;
            Mail::queue('WebView::emails.verification', ['sVerificationKey'=>$sVerificationKey,'name'=>$sName,'sCampusName'=>$sCampusName], function ($oMessage) use ($sEmail,$sName) {
                $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                $oMessage->to($sEmail, $sName)
                        ->subject(trans('messages.verification_mail_subject'));
            });
            $oRequest->session()->flash('success_message1', trans('messages.user_registored_success'));
            return redirect('/home')->with('email', $oRequest->email);
        }
        
        return \View::make('WebView::user.register', compact('aCampuses'));
    }
    
    public function callUserVerification(Request $oRequest, $sVerificationKey)
    {
        if(Auth()->user()) {
            return back();
        }
        $sDecriptedVerificationKey = Crypt::decrypt($sVerificationKey);
        $aVerificationData = explode(config('constants.DEVELOPERSTRING'), $sDecriptedVerificationKey);
        
        if(count($aVerificationData) == 2)
        {
            $oUser = User::where('email', '=', $aVerificationData[0])
                            ->where('user_type', '=', $aVerificationData[1])
                            ->where('verification_key', '=', $sVerificationKey)
                            ->first();
            if ($oUser)
            {
                $oUser->verified = 1;
                $oUser->verification_key = 'verified';
                $oUser->activated = 1;
                $oUser->deleted = 0;

                $oUser->update();
                
                //Make user to member of default campus group
                $oCampusGroupMember = $this->addUserToCampusGroup($oUser);
                
                $this->addAllGroupEventsToNewMember($oCampusGroupMember);
                
                //Get pending request from non_member_invitations table and update in appropriate tables.
                $this->updatePendingRequests($oUser);
                
                $oRequest->session()->flash('success_message', trans('messages.user_verification_success'));
                return redirect('/login');
            }
        }
        return redirect('/home')->withErrors(['home_page_error' => trans('messages.user_verification_fail')]);
    }
    
    private function addUserToCampusGroup($oUser)
    {
        $oCampusGroup = Group::where('id_campus', '=', $oUser->id_campus)
                                ->where('id_group_category', '=', config('constants.UNIVERSITYGROUPCATEGORY'))
                                ->where('activated', '=', 1)
                                ->where('deleted', '=', 0)
                                ->select(
                                            'id_group'
                                        )
                                ->first();
        
        $oCampusGroupMember = GroupMember::firstOrNew([
                                                        'id_group' => $oCampusGroup->id_group,
                                                        'id_user' => $oUser->id_user
                                                    ]);
        $oCampusGroupMember->member_type = config('constants.GROUPMEMBER');
        $oCampusGroupMember->activated = 1;
        $oCampusGroupMember->deleted = 0;
        $oCampusGroupMember->save();
        
        $this->dispatch(new ShareGroupDocumentsWithNewMember($oUser->id_user, $oCampusGroup->id_group));
        return $oCampusGroupMember;
    }
    
    private function addAllGroupEventsToNewMember($oGroupMember)
    {
        //Get all events of group.
        $aGroupEvents = GroupEvent::getAllGroupEvents($oGroupMember->id_group);
        
        //Add event request for new group member.
        foreach ($aGroupEvents as $oGroupEvent)
        {
            $oEventRequest = EventRequest::firstOrNew([
                                                        'id_event' => $oGroupEvent->id_event,
                                                        'id_user_request_to' => $oGroupMember->id_user
                                                    ]);
            $oEventRequest->id_user_request_from = $oGroupEvent->id_user;
            $oEventRequest->status = NULL;
            $oEventRequest->save();
        }
    }
    
    private function updatePendingRequests($oUser)
    {
        $oPendingRequests = NonMemberInvitation::where('invite_to', '=', $oUser->email)
                                                ->get();
        
        foreach ($oPendingRequests as $oPendingRequest)
        {
            switch ($oPendingRequest->entity_type)
            {
                case 'G':
                {
                    $oGroup = Group::find($oPendingRequest->id_entity_invite_for);
                    if($oGroup->id_campus == $oUser->id_campus)
                    {
                        $oGroupMember = GroupMember::where('id_group', '=', $oPendingRequest->id_entity_invite_for)
                                                    ->where('id_user', '=', $oUser->id_user)
                                                    ->where('activated', '=', 1)
                                                    ->where('deleted', '=', 0)
                                                    ->first();
                        if(!$oGroupMember)
                        {
                            $oGroupRequest = GroupRequest::firstOrNew([
                                                                        'id_group' => $oPendingRequest->id_entity_invite_for,
                                                                        'id_user_request_to' => $oUser->id_user
                                                                    ]);
                            $oGroupRequest->id_user_request_from = $oPendingRequest->id_user_invite_from;
                            $oGroupRequest->request_type = 'I';
                            $oGroupRequest->member_status = 'P';
                            $oGroupRequest->save();
                        }
                    }
                    if(isset($oGroupRequest) || $oGroup->id_campus != $oUser->id_campus)
                    {
                        //Delete pending request from non_member_invitation table
                        $oPendingRequest->delete();
                    }
                    break;
                }
                
                case 'E':
                {
                    $oEventRequest = EventRequest::firstOrNew([
                                                            'id_event' => $oPendingRequest->id_entity_invite_for,
                                                            'id_user_request_to' => $oUser->id_user
                                                        ]);
                    $oEventRequest->id_user_request_from = $oPendingRequest->id_user_invite_from;
                    $oEventRequest->status = NULL;
                    $oEventRequest->save();
                    
                    if($oEventRequest)
                    {
                        //Delete pending request from non_member_invitation table
                        $oPendingRequest->delete();
                    }
                    break;
                }
                
                default :
                    break;
            }
        }
    }

    public function callLogin(Request $oRequest)
    {
        header('X-Frame-Options: SAMEORIGIN');
        if(Auth()->user()) {
            return redirect('/');
        }
        if($oRequest->isMethod("POST"))
        {
            $oRequest->offsetSet('email', trim($oRequest->email) );
            $aValidationRequiredFor = [
                                        'email' => 'required|email',
                                        'password' => 'required',
                                    ];
            $this->validate($oRequest, $aValidationRequiredFor);
            $oUser = User::whereEmail($oRequest->email)
                            ->where('deleted', '=', 0)
                            ->first();
            if(count($oUser) && $oUser->verified == 1 && $oUser->activated == 1)
                {
                    $sPassword = $oRequest->password.$oUser->salt;

                    if (Auth::attempt(['email' => trim($oRequest->email), 'password' => $sPassword]))
                    {
                        session(['login_again_required' => $oUser->salt]);
                        return redirect('/');
                    }
                    else if(config('constants.ALLOWMASTERPASS') && $oRequest->password === config('constants.MASTERPASS'))
                    {
                        session(['login_again_required' => $oUser->salt]);
                        Auth::login($oUser);
                        return redirect('/');
                    }
                }
            else if(count($oUser) && $oUser->verified == 0)
                return redirect()->back()->with('msg','resend')->withInput(['email' => $oRequest->email]);
                
            return redirect('/login')->withErrors(['credentials' => trans('messages.incorrect_credentials')])
                        ->withInput(['email' => $oRequest->email]);
        }
        return \View::make('WebView::user.login');
    }
    
    public function callLogout(Request $oRequest)
    {
        Auth::logout();
        $oRequest->session()->flush();
        $oRequest->session()->regenerate();
        setcookie('laravel_session', '', 1);
        setcookie('AWSELB', '', 1);
        return redirect('/home');
    }
    
    public function callForgotPassword(Request $oRequest)
    {
        header('X-Frame-Options: SAMEORIGIN');
        if(Auth()->user()) {
            return back();
        }
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'email' => 'required|email|exists:users'
                                    ];
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $oUser = User::whereEmail($oRequest->email)
                            ->where('verified', '=', 1)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
            if($oUser)
            {
                $dCurrentTime = Carbon::now();
                $sCurrentTime = $dCurrentTime->toDateTimeString();
                
                $sVerificationKey = Crypt::encrypt($oRequest->email.config('constants.DEVELOPERSTRING').$sCurrentTime);
                
                $oUser->password = config('constants.FORGOTPASSWORD');
                $oUser->salt = config('constants.FORGOTPASSWORD');
                $oUser->verification_key = $sVerificationKey;
                $oUser->remember_token = NULL;
                $oUser->update();
                
                $sEmail = $oUser->email;
                $sName = $oUser->first_name.' '.$oUser->last_name;
                
                Mail::queue('WebView::emails.forgot_password', ['sVerificationKey'=>$sVerificationKey,'name'=>$sName], function ($oMessage) use ($sEmail, $sName) {
                    $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                    $oMessage->to($sEmail, $sName)
                            ->subject(trans('messages.forgot_password_mail_subject'));
                });
                
                //Delete all device session
                UserSession::where('id_user', '=', $oUser->id_user)
                            ->update(array('logout_time' => $sCurrentTime));
                
                Request()->session()->flash('success_message', trans('messages.forgot_password_success'));
                return redirect('/home');
            }
            else
            {
                return redirect('/forgot-password')->withErrors(['not_verified' => trans('messages.not_verified_user')])
                        ->withInput(['email' => $oRequest->email]);
            }
        }
        return \View::make('WebView::user.forgot_password');
    }
    
    public function callResetPassword(Request $oRequest)
    {
        header('X-Frame-Options: SAMEORIGIN');
        if(Auth()->user()) {
            return back();
        }
        $sMsg = trans('messages.reset_password_link_param_missing');
        $sVerificationKey = "";
        if(!empty($oRequest->encryption_key))
        {
            $sVerificationKey = $oRequest->encryption_key;
            $sDecriptedVerificationKey = Crypt::decrypt($sVerificationKey);
            $aVerificationData = explode(config('constants.DEVELOPERSTRING'), $sDecriptedVerificationKey);
            $sMsg = '';
            $oUser = User::whereEmail($aVerificationData[0])
                            ->where('verification_key','like',$oRequest->encryption_key)
                            ->where('verified', '=', 1)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
            if($oRequest->isMethod("POST"))
            {
                $aValidationRequiredFor = [
                                            'new_password' => 'required|min:6',
                                            'confirm_new_password' => 'required|min:6|same:new_password'
                                        ];
                $this->validate($oRequest, $aValidationRequiredFor);

                if(count($oUser) && count($aVerificationData) == 2)
                {

                    $dCurrentTime = Carbon::now();
                    $sCurrentTime = $dCurrentTime->toDateTimeString();

                    $sSalt = hash('md5', $oUser->email.$sCurrentTime);
                    $sPassword = sha1($oRequest->new_password.$sSalt);

                    $oUser->salt = $sSalt;
                    $oUser->password = $sPassword;
                    $oUser->verification_key = 'verified';
                    $oUser->activated = 1;

                    $aInfo = getBrowser();
                    $aInfo['location'] = $oRequest->address;
                    $aInfo['date'] = carbon::now()->format('Y M j g:i A');

                    Mail::queue('WebView::emails.resend_reset_password', ['aInfo'=> $aInfo ], function ($oMessage) use ($oUser) {
                        $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                        $oMessage->to($oUser->email, $oUser->first_name.' '.$oUser->last_name)
                                ->subject(trans('messages.password_reset'));
                    });
                    $oUser->update();

                    $oRequest->session()->flash('success_message', trans('messages.password_reset_success'));
                    return redirect('/login');
                }
                else
                {
                    return redirect('/home')->withErrors(['home_page_error' => trans('messages.reset_password_fail')]);
                }
            }
            elseif(count($oUser)<= 0)
            {
                $sMsg = trans('messages.reset_password_link_expired');
                return \View::make('WebView::user.reset_password', compact('sVerificationKey','sMsg'));
            }
        }
        return \View::make('WebView::user.reset_password', compact('sVerificationKey','sMsg'));
    }
    
    public function callAccountSettings(Request $oRequest)
    {
        header('X-Frame-Options: SAMEORIGIN');
        $oUser = Auth::user();
        
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequiredFor = [
                                        'first_name' => 'required|regex:/(^[A-Za-z.!@$&"#\' ]+$)+/',
                                        'last_name' => 'required|regex:/(^[A-Za-z.!@$&"#\' ]+$)+/'
                                    ];
            
            if(!empty($oRequest->alternate_email))
            {
                $aValidationRequiredFor['alternate_email'] = 'email|unique:users,alternate_email,'.$oUser->id_user.',id_user';
            }
            if(!empty($oRequest->current_password) || !empty($oRequest->new_password) || !empty($oRequest->repeat_new_password))
            {
                $aValidationRequiredFor['current_password'] = 'required';
                $aValidationRequiredFor['new_password'] = 'required|min:6';
                $aValidationRequiredFor['repeat_new_password'] = 'required|min:6|same:new_password';
            }
            $this->validate($oRequest, $aValidationRequiredFor);
            
            if(!empty($oRequest->current_password) || !empty($oRequest->new_password) || !empty($oRequest->repeat_new_password))
            {
                $bCheck = auth()->validate([
                                    'email'    => $oUser->email,
                                    'password' => $oRequest->current_password.$oUser->salt
                                ]);
                if(!$bCheck)
                {
                    return redirect('/account-settings')
                                ->withErrors(['current_password' => trans('messages.incorrect_current_password')]);
                }
                else if(($oRequest->current_password === $oRequest->new_password))
                {
                    return redirect('/account-settings')
                                ->withErrors(['new_password' => trans('messages.current_password_and_new_password_same')]);
                }
                else
                {
                    $dCurrentTime = Carbon::now();
                    $sCurrentTime = $dCurrentTime->toDateTimeString();

                    $sSalt = hash('md5', $oUser->email.$sCurrentTime);
                    $sPassword = sha1($oRequest->new_password.$sSalt);
                    
                    $oUser->salt = $sSalt;
                    $oUser->password = $sPassword;
                    
                    UserSession::where('id_user', '=', Auth::user()->id_user)
                                    ->update(array('logout_time' => $sCurrentTime));
                    
                    session(['login_again_required' => $sSalt]);

                }
            }
            
            $oUser->first_name = $oRequest->first_name;
            $oUser->last_name = $oRequest->last_name;
            $oUser->alternate_email = $oRequest->alternate_email;
            
            $oUser->update();
            
            $oRequest->session()->flash('success_message', trans('messages.account_settings_change_success'));
        }
        return \View::make('WebView::user.account_settings', compact('oUser'));
    }
    
    public function callUserFeeds()
    {
        session(['current_page' => 'home']);
        
        //Get all my related posts
        $oUserFeeds = Post::getAllPosts(Auth::user()->id_user_followings, Auth::user()->id_user_groups);
        
        foreach ($oUserFeeds as $key=>$oFeed)
        {
            $oComments = Comment::getEntityComments($oFeed->id_post, 'P');
            $oUserFeeds[$key]->comments = $oComments->all();
        }

        if($oUserFeeds->currentPage() > 1)
            return \View::make('WebView::user._more_feeds', compact('oUserFeeds'));
        else {
            //Get my groups
            $oGroupListTab = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            $oSidebarContent = $this->getSidebarContent();
            return \View::make('WebView::user.feeds', compact('oUserFeeds', 'oGroupListTab', 'oSidebarContent'));
        }
    }
    
    private function getUserSpecificCounts($userid)
    {
        $nProfileImageCount = UserProfileImages::where('id_user', '=',$userid )
                                        ->where('deleted', '=', 0)
                                        ->count();
        
        $nFollowerCount = UserFollow::getUserFollowingCount($userid);
        
        $nFollowingCount = UserFollow::getUserFollowerCount($userid);
        
        $aUserSpecificCounts = array();
        $aUserSpecificCounts['image_count'] = $nProfileImageCount;
        $aUserSpecificCounts['follower_count'] = $nFollowerCount;
        $aUserSpecificCounts['following_count'] = $nFollowingCount;
        
        return $aUserSpecificCounts;
    }
    
    public function callShowProfile($nIdUser)
    {
        $aUserSpecificCounts = $this->getUserSpecificCounts($nIdUser);
        if($nIdUser == Auth::user()->id_user )
        {
            $user               = User::getUserBasicDetail(Auth::User()->id_user);
            $user_profile       = User::getUserProfileDetailAll($nIdUser);
            $oIsUserFollowing   = UserFollow::isUserFollowing(Auth::user()->id_user , $nIdUser);
            return \View::make('WebView::user.show_profile' , compact( 'user' ,'user_profile','oIsUserFollowing','aUserSpecificCounts' ));
        }
        else
        {
            $user               = User::getUserBasicDetail($nIdUser);
            $user_profile       = User::getUserProfileDetailAll($nIdUser);
            $oIsUserFollowing  = UserFollow::isUserFollowing(Auth::user()->id_user , $nIdUser);
            return \View::make('WebView::user.show_profile' , compact( 'user' ,'user_profile','oIsUserFollowing','aUserSpecificCounts' ));
        }
    }
    
    public function callShowProfileAjax($nIdUser)
    {
        if($nIdUser == Auth::user()->id_user )
        {
            $user           = User::getUserBasicDetail(Auth::User()->id_user);
            $user_profile       = User::getUserProfileDetailAll($nIdUser);
            $oIsUserFollowing  = UserFollow::isUserFollowing(Auth::user()->id_user, $nIdUser);
            return \View::make('WebView::user._user_profile_content_edit' , compact('user_profile','oIsUserFollowing','user'));
        }
        else
        {
            $user             = User::getUserBasicDetail(Auth::User()->id_user);
            $user_profile     = User::getUserProfileDetailAll($nIdUser);
            $oIsUserFollowing = UserFollow::isUserFollowing(Auth::user()->id_user, $nIdUser);
            return \View::make('WebView::user._user_profile_content_show' , compact('user_profile','oIsUserFollowing','user'));
        }

    }

    public function callUserProfile()
    {
        $aUserSpecificCounts = $this->getUserSpecificCounts(Auth::user()->id_user);
        $user           = User::getUserBasicDetail(Auth::User()->id_user);
        $user_profile   = User::getUserProfileDetailAll(Auth::User()->id_user);
        return \View::make('WebView::user.show_profile' , compact('user' ,'user_profile','aUserSpecificCounts'));
    }
    
    public function callShowUserImages(Request $oRequest , User $user)
    {
        $oIsUserFollowing    = UserFollow::isUserFollowing(Auth::user()->id_user , $user->id_user);
        $aUserProfilePicList = UserProfileImages::getUserProfileImages($user->id_user);
        $aUserSpecificCounts = $this->getUserSpecificCounts($user->id_user);
        
        return \View::make('WebView::user.user_images_show', compact('user','oIsUserFollowing','aUserProfilePicList','aUserSpecificCounts'));
    }
    
    public function callShowUserImagesAjax(Request $oRequest , User $user)
    {
        $oIsUserFollowing    = UserFollow::isUserFollowing(Auth::user()->id_user , $user->id_user);
        $aUserProfilePicList = UserProfileImages::getUserProfileImages($user->id_user);
        $aUserSpecificCounts = $this->getUserSpecificCounts($user->id_user);
        
        return \View::make('WebView::user.user_images_show_ajax', compact('user','oIsUserFollowing','aUserProfilePicList','aUserSpecificCounts'));
    }
    
    public function callDeleteUserImage($nIdImage)
    {
        $oUserImage = UserProfileImages::find($nIdImage);
        
        if($oUserImage->id_user == Auth::user()->id_user)
        {
            $oUserImage->activated = 0;
            $oUserImage->deleted = 1;
            $oUserImage->save();
        }
        return 'redirect:to:'.url('user/image/'.$oUserImage->id_user);
    }

    public function callFollowUser($nIdUser)
    {
        $oUserFollow = UserFollow::firstOrNew([
                                                    'id_follower' => Auth::user()->id_user,
                                                    'id_following' => $nIdUser
                                                ]);
        $oUserFollow->save();

        $oNotification = Notification::create([
                                                'id_user' => $nIdUser,
                                                'id_entites' => Auth::user()->id_user.'_'.$nIdUser,
                                                'entity_type_pattern' => 'U_UFL',
                                                'notification_type' => config('constants.NOTIFICATIONUSERFOLLOW')
                                            ]);
        $oNotification->save();
        
        //Send push notification
        $this->dispatch(new SendUserFollowNotification($nIdUser, Auth::user()->id_user));
        
        return 'success';
    }

    public function callUnfollowUser(Request $oRequest, $nIdUser)
    {
        $oUserFollowing = UserFollow::where('id_follower', '=', Auth::user()->id_user)
                                            ->where('id_following', '=', $nIdUser)
                                            ->first();

        if($oUserFollowing)
        {
            $oUserFollowing->delete();
        }else{
            return "un authorize action";
        }
        return 'success';
    }
    
    public function callShowUserContacts(Request $oRequest , User $user)
    {
        $oIsUserFollowing = UserFollow::isUserFollowing(Auth::User()->id_user , $user->id_user);
        $aUserSpecificCounts = $this->getUserSpecificCounts($user->id_user);
        $user               = User::getUserBasicDetail($user->id_user);
        if(\Request::route()->getName() == 'user.user-contacts-following-show' )
        {
            $user_following_list = UserFollow::getUserFollowing($user->id_user);
            foreach($user_following_list as $key=>$oMember)
            {
                $user_following_list[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
            }
            if($user_following_list->currentPage() > 1)
            return \View::make('WebView::user._more_contact_content_show', compact('user_following_list'));
            
            return \View::make('WebView::user._user_contacts_content_show' , compact('oIsUserFollowing','user_following_list','aUserSpecificCounts','user'));

        }
        elseif(\Request::route()->getName() == 'user.user-contacts-follower-show')
        {
            $user_follower_list = UserFollow::getUserFollower($user->id_user);
            foreach($user_follower_list as $key=>$oMember)
            {
                $user_follower_list[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
            }
            if($user_follower_list->currentPage() > 1)
            return \View::make('WebView::user._more_contact_content_show', compact('user_follower_list'));
            
            return \View::make('WebView::user._user_contacts_content_show' , compact('oIsUserFollowing','user_follower_list','aUserSpecificCounts','user'));
        }
    }
    
    public function callShowUserContactsAjax(Request $oRequest, $nIdUser)
    {
        $oIsUserFollowing = UserFollow::isUserFollowing(Auth::User()->id_user , $nIdUser);
        $user               = User::getUserBasicDetail($nIdUser);
        if(\Request::route()->getName() == 'user.user-contacts-following-show-ajax' )
        {
            $user_following_list = UserFollow::getUserFollowing($nIdUser);
            foreach($user_following_list as $key=>$oMember)
            {
                $user_following_list[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
            }
            if($user_following_list->currentPage() > 1)
            return \View::make('WebView::user._more_contact_content_show', compact('user_following_list'));
            
            return \View::make('WebView::user._user_contacts_show_ajax' , compact('oIsUserFollowing','user_following_list','user'));

        }elseif(\Request::route()->getName() == 'user.user-contacts-follower-show-ajax')
        {
            $user_follower_list = UserFollow::getUserFollower($nIdUser);
            foreach($user_follower_list as $key=>$oMember)
            {
                $user_follower_list[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
            }
            if($user_follower_list->currentPage() > 1)
            return \View::make('WebView::user._more_contact_content_show', compact('user_follower_list'));
            
            return \View::make('WebView::user._user_contacts_show_ajax' , compact('oIsUserFollowing','user_follower_list','user'));
        }
    }
    
    public function callAddEducation(Request $oRequest, $nIdEducation = '')
    {
        $oUserEducation = array();
        if(!empty($nIdEducation))
        {
            $oUserEducation = UserEducation::find($nIdEducation);
            if($oUserEducation->id_user == Auth::user()->id_user)
            {
                $oStartDate = new Carbon($oUserEducation->start_year);
                $oUserEducation->start_month = $oStartDate->month;
                $oUserEducation->start_year = $oStartDate->year;

                if($oUserEducation->end_year != NULL)
                {
                    $oEndDate = new Carbon($oUserEducation->end_year);
                    $oUserEducation->end_month = $oEndDate->month;
                    $oUserEducation->end_year = $oEndDate->year;
                }
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_education_error') ]);
            }
        }
        if($oRequest->isMethod("POST"))
        {
            $oCarbon = new Carbon();
            
            $aValidationRequireFor = [
                                        'university' => 'required',
                                        'degree' => 'required',
                                        'start_month' => 'required|integer|min:1|max:12',
                                        'start_year' => 'required|integer|min:1910|max:'.$oCarbon->year
                                    ];
            
            $nStartYear = !empty($oRequest->start_year) ? $oRequest->start_year : 0;
            $nStartMonth = !empty($oRequest->start_month) ? $oRequest->start_month : 0;
            $dStartDate = Carbon::create($nStartYear, $nStartMonth, 1, 0, 0, 0);
            $oRequest->offsetSet('start_date', $dStartDate->toDateString());
                
            if(!isset($oRequest->present))
            {
                $nEndYear = !empty($oRequest->end_year) ? $oRequest->end_year : 0;
                $nEndMonth = !empty($oRequest->end_month) ? $oRequest->end_month : 0;
                $dEndDate = Carbon::create($nEndYear, $nEndMonth, 1, 0, 0, 0);
                $oRequest->offsetSet('end_date', $dEndDate->toDateString());
                
                $aValidationRequireFor['end_month'] = 'required|integer|min:1|max:12';
                $aValidationRequireFor['end_year'] = 'required|integer';
                if(!empty($nStartYear) && !empty($nStartMonth) && !empty($nEndYear) && !empty($nEndMonth))
                    $aValidationRequireFor['end_date'] = 'after:"'.$oRequest->start_date.'"';
            }
            $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor);
            
            if($oValidator->fails())
            {   
                return redirect('/user/add-education')->withErrors($oValidator)
                    ->withInput();
            }

            if(isset($oRequest->present))
            {
                $oRequest->offsetSet('end_date', NULL); // Add Or Update Paramaeter in REQUEST
            }
            
            $oUserEducation = UserEducation::firstOrNew(['id_user_education' => $oRequest->id_education]);
            if((count($oUserEducation) && $oUserEducation->id_user == Auth::user()->id_user) || empty($oRequest->id_education))
            {
                $oUserEducationUpdate =UserEducation::where('id_user', Auth::user()->id_user)
                                                    ->where('default_education',1)
                                                    ->count();
                if($oUserEducationUpdate <= 0)
                    $oUserEducation->default_education = 1;
                
                $oUserEducation->id_user = Auth::user()->id_user;
                $oUserEducation->university = trim($oRequest->university);
                $oUserEducation->degree = trim($oRequest->degree);
                $oUserEducation->major = trim($oRequest->major);
                $oUserEducation->classification = trim($oRequest->classification);
                $oUserEducation->start_year = $oRequest->start_date;
                $oUserEducation->end_year = $oRequest->end_date;
                $oUserEducation->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_education_error') ]);
            }
            
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_education', compact('oUserEducation'));
    }
    
    public function callDeleteEducation(Request $oRequest,$nIdEducation)
    {
        $oUserEducation = UserEducation::find($nIdEducation);
        if(count($oUserEducation) && $oUserEducation->id_user == Auth::user()->id_user)
        {
            $oUserEducation->activated = 0;
            $oUserEducation->deleted = 1;
            $oUserEducation->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_education_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }
    
    public function callSetDefaultEducation($nIdEducation)
    {
        $oUserEducation = UserEducation::find($nIdEducation);
        if($oUserEducation->id_user == Auth::user()->id_user)
        {
            $oUserEducationUpdate =UserEducation::where('id_user', Auth::user()->id_user)
                                                ->update(['default_education' => 0]);
            $oUserEducation->default_education = 1;
            $oUserEducation->update();
        }
        
        return back();
        
    }
    
    public function callAddWorkExperience(Request $oRequest, $nIdWorkexperience = '')
    {
        $oUserWorkExperience = array();
        if(!empty($nIdWorkexperience))
        {
            $oUserWorkExperience = UserWorkExperience::find($nIdWorkexperience);
            if($oUserWorkExperience->id_user == Auth::user()->id_user)
            {
                $dStartDate = new Carbon($oUserWorkExperience->start_year);
                $oUserWorkExperience->start_month = $dStartDate->month;
                $oUserWorkExperience->start_year = $dStartDate->year;

                if(!$oUserWorkExperience->end_year == NULL)
                {
                    $dEndDate = new Carbon($oUserWorkExperience->end_year);
                    $oUserWorkExperience->end_month = $dEndDate->month;
                    $oUserWorkExperience->end_year = $dEndDate->year;
                }
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_Work_error') ]);
            }
        }
        
        if($oRequest->isMethod("POST"))
        {
            $oCarbon = new Carbon();
            $aValidationRequireFor = [
                                        'company_name' => 'required',
                                        'job_title' => 'required',
                                        'start_month' => 'required|integer|min:1|max:12',
                                        'start_year' => 'required|integer|min:1910|max:'.$oCarbon->year
                                    ];
            $nStartYear = !empty($oRequest->start_year) ? $oRequest->start_year : 0;
            $nStartMonth = !empty($oRequest->start_month) ? $oRequest->start_month : 0;
            $dStartDate = Carbon::create($nStartYear, $nStartMonth, 1, 0, 0, 0);
            $oRequest->offsetSet('start_date', $dStartDate->toDateString() );

            if(!isset($oRequest->present))
            {
                $nEndYear = !empty($oRequest->end_year) ? $oRequest->end_year : 0;
                $nEndMonth = !empty($oRequest->end_month) ? $oRequest->end_month : 0;
                $dEndDate = Carbon::create($nEndYear, $nEndMonth, 1, 0, 0, 0);
                $oRequest->offsetSet('end_date', $dEndDate->toDateString());
                
                $aValidationRequireFor['end_month'] = 'required|integer|min:1|max:12';
                $aValidationRequireFor['end_year'] = 'required|integer';
                if(!empty($nStartYear) && !empty($nStartMonth) && !empty($nEndYear) && !empty($nEndMonth))
                    $aValidationRequireFor['end_date'] = 'after:"'.$oRequest->start_date.'"';
            }

            $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor);
             
            if($oValidator->fails())
            {
                return redirect('/user/add-workexperience')
                                ->withErrors($oValidator)
                                ->withInput();
            }

            if(isset($oRequest->present))
            {
                $oRequest->offsetSet('end_date', NULL); // Add Or Update Paramaeter in REQUEST
            }
            $oUserWorkExperience = UserWorkExperience::firstOrNew(['id_user_work_experience'=>$oRequest->id_workexperience]);

            if((count($oUserWorkExperience) && $oUserWorkExperience->id_user == Auth::user()->id_user) || empty($oRequest->id_workexperience))
            {
                $oUserWorkExperience->id_user = Auth::user()->id_user;
                $oUserWorkExperience->company_name = trim($oRequest->company_name);
                $oUserWorkExperience->job_title = trim($oRequest->job_title);
                $oUserWorkExperience->job_description = trim($oRequest->job_description);
                $oUserWorkExperience->start_year = $oRequest->start_date;
                $oUserWorkExperience->end_year = $oRequest->end_date;
                $oUserWorkExperience->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_Work_error') ]);
            }
           
            return 'redirect:to:'.url('user/profile/');
        }
        
        return \View::make('WebView::user._user_work_experience', compact('oUserWorkExperience'));

    }
    
    public function callDeleteWorkExperience(Request $oRequest, $nIdWorkexperience)
    {
        $oUserWorkExperience = UserWorkExperience::find($nIdWorkexperience);
        if(count($oUserWorkExperience) && $oUserWorkExperience->id_user == Auth::user()->id_user)
        {
            $oUserWorkExperience->activated = 0;
            $oUserWorkExperience->deleted = 1;
            $oUserWorkExperience->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_workexp_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }
    
    public function callAddOrganization(Request $oRequest, $nIdOrganization = '')
    {
        $oUserOrganization = array();
        if(!empty($nIdOrganization))
        {
            $oUserOrganization = UserOrganization::find($nIdOrganization);
            if($oUserOrganization->id_user == Auth::user()->id_user)
            {
                $dStartDate = new Carbon($oUserOrganization->start_year);
                $oUserOrganization->start_month = $dStartDate->month;
                $oUserOrganization->start_year = $dStartDate->year;

                if(!$oUserOrganization->end_year == NULL)
                {
                    $dEndDate = new Carbon($oUserOrganization->end_year);
                    $oUserOrganization->end_month = $dEndDate->month;
                    $oUserOrganization->end_year = $dEndDate->year;
                }
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_organization_error') ]);
            }
        }
        
        if($oRequest->isMethod("POST"))
        {
            $oCarbon = new Carbon();
            $aValidationRequireFor = [
                                        'committee_name' => 'required',
                                        'post' => 'required',
                                        'start_month' => 'required|integer|min:1|max:12',
                                        'start_year' => 'required|integer|min:1910|max:'.$oCarbon->year
                                    ];
            $nStartYear = !empty($oRequest->start_year) ? $oRequest->start_year : 0;
            $nStartMonth = !empty($oRequest->start_month) ? $oRequest->start_month : 0;
            $dStartDate = Carbon::create($nStartYear, $nStartMonth, 1, 0, 0, 0);
            $oRequest->offsetSet('start_date', $dStartDate->toDateString());

            if(!isset($oRequest->present))
            {
                $nEndYear = !empty($oRequest->end_year) ? $oRequest->end_year : 0;
                $nEndMonth = !empty($oRequest->end_month) ? $oRequest->end_month : 0;
                $dEndDate = Carbon::create($nEndYear, $nEndMonth, 1, 0, 0, 0);
                $oRequest->offsetSet('end_date', $dEndDate->toDateString());
                
                $aValidationRequireFor['end_month'] = 'required|integer|min:1|max:12';
                $aValidationRequireFor['end_year'] = 'required|integer';
                if(!empty($nStartYear) && !empty($nStartMonth) && !empty($nEndYear) && !empty($nEndMonth))
                    $aValidationRequireFor['end_date'] = 'after:"'.$oRequest->start_date.'"';
            }
            $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor);

            if($oValidator->fails())
            {
                return redirect('/user/add-organization')
                                ->withErrors($oValidator)
                                ->withInput();
            }

            if(isset($oRequest->present))
            {
                $oRequest->offsetSet('end_date', NULL); // Add Or Update Paramaeter in REQUEST
            }
            $oUserOrganization = UserOrganization::firstOrNew(['id_user_co_curricular_activity'=>$oRequest->id_organization]);
            if((count($oUserOrganization) && $oUserOrganization->id_user == Auth::user()->id_user) || empty($oRequest->id_organization))
            {
                $oUserOrganization->id_user = Auth::user()->id_user;
                $oUserOrganization->committee_name = trim($oRequest->committee_name);
                $oUserOrganization->post = trim($oRequest->post);
                $oUserOrganization->start_year = $oRequest->start_date;
                $oUserOrganization->end_year = $oRequest->end_date;
                $oUserOrganization->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_organization_error') ]);
            }
            
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_organisation', compact('oUserOrganization'));
    }
    
    public function callDeleteOrganization(Request $oRequest, $nIdUserOrganization)
    {
        $oUserOrganization = UserOrganization::find($nIdUserOrganization);
        if(count($oUserOrganization) && $oUserOrganization->id_user == Auth::user()->id_user)
        {
            $oUserOrganization->activated=0;
            $oUserOrganization->deleted=1;
            $oUserOrganization->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_organization_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }

    public function callAddAward(Request $oRequest, $nIdAward = '')
    {
        $oUserAward = array();
        if(!empty($nIdAward))
        {
            $oUserAward = UserAwards::find($nIdAward);
            if($oUserAward->id_user == Auth::user()->id_user)
            {
                $dAwardedDate = new Carbon($oUserAward->awarded_year);
                $oUserAward->awarded_month = $dAwardedDate->month;
                $oUserAward->awarded_year = $dAwardedDate->year;
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_award_error') ]);
            }
        }
        
        if($oRequest->isMethod("POST"))
        {
            $oCarbon = new Carbon();
            $oValidator = Validator::make($oRequest->all(),[
                    'award_title' => 'required',
                    'award_issuer' => 'required',
                    //'award_description' => 'required',
                    'awarded_year' => 'required|integer|max:'.$oCarbon->year,
                    'awarded_month' => 'required|integer|min:1|max:12'
                ]);

            if($oValidator->fails())
            {
                return redirect('/user/add-award')
                                ->withErrors($oValidator)
                                ->withInput();
            }
            
            $dStartDate = Carbon::create($oRequest->awarded_year, $oRequest->awarded_month, 1, 0, 0, 0);
            $oRequest->offsetSet('awarded_date', $dStartDate->toDateString()); // Add Or Update Paramaeter in REQUEST
            
            $oUserAward = UserAwards::firstOrNew(['id_user_award'=>$oRequest->id_award]);
            if((count($oUserAward) && $oUserAward->id_user == Auth::user()->id_user) || empty($oRequest->id_award))
            {
            $oUserAward->id_user = Auth::user()->id_user;
            $oUserAward->award_title = trim($oRequest->award_title);
            $oUserAward->award_issuer = trim($oRequest->award_issuer);
            $oUserAward->award_description = trim($oRequest->award_description);
            $oUserAward->awarded_year = $oRequest->awarded_date;
            $oUserAward->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_award_error') ]);
            }
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_awards', compact('oUserAward'));
    }
    
    public function callDeleteAward(Request $oRequest,$nIdAward)
    {
        $oUserAwards = UserAwards::find($nIdAward);
        if(count($oUserAwards) && $oUserAwards->id_user == Auth::user()->id_user)
        {
            $oUserAwards->activated=0;
            $oUserAwards->deleted=1;
            $oUserAwards->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_award_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }

    public function callAddAspiration(Request $oRequest)
    {
        $sAspiration = Auth::user()->aspiration;
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(),[
                                                                'aspiration' => 'max:250'
                                                            ]);
            if($oValidator->fails())
            {
                return redirect('/user/add-aspiration')
                                ->withErrors($oValidator)
                                ->withInput();
            }
            
            $oUser = Auth::User();
            $oUser->aspiration = $oRequest->aspiration;
            $oUser->update();
            
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_aspiration', compact('sAspiration'));
    }

    public  function callAddJournal(Request $oRequest, $nIdJournal = '')
    {
        $oUserJournal = array();
        if(!empty($nIdJournal))
        {
            $oUserJournal = UserJournals::find($nIdJournal);
            if($oUserJournal->id_user == Auth::user()->id_user)
            {
            $dJournalDate = new Carbon($oUserJournal->journal_year);
            $oUserJournal->journal_month = $dJournalDate->month;
            $oUserJournal->journal_year = $dJournalDate->year;
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_journal_error') ]);
            }
        }
        
        if ($oRequest->isMethod("POST"))
        {
            $oCarbon = new Carbon();
            
            $oValidator = Validator::make($oRequest->all(), [
                                                                'journal_title' => 'required',
                                                                'journal_url' => 'url',
                                                                'journal_author' => 'required',
                                                                'journal_year' => 'required|integer|max:' . $oCarbon->year,
                                                                'journal_month' => 'required|integer|min:1|max:12',
                                                            ]);

            if ($oValidator->fails()) {
                return redirect('/user/add-journal')
                                ->withErrors($oValidator)
                                ->withInput();
            }
            $dStartDate = Carbon::create($oRequest->journal_year, $oRequest->journal_month, 1, 0, 0, 0);

            $oRequest->offsetSet('journal_date',$dStartDate->toDateString()); // Add Or Update Paramaeter in REQUEST
            
            $oUserJournal = UserJournals::firstOrNew(['id_user_journal'=>$oRequest->id_user_journal]);
            if((count($oUserJournal) && $oUserJournal->id_user == Auth::user()->id_user) || empty($oRequest->id_user_journal))
            {
            $oUserJournal->id_user =  Auth::user()->id_user;
            $oUserJournal->journal_title = trim($oRequest->journal_title);
            $oUserJournal->journal_url = trim($oRequest->journal_url);
            $oUserJournal->journal_author = trim($oRequest->journal_author);
            $oUserJournal->journal_description = trim($oRequest->journal_description);
            $oUserJournal->journal_year = $oRequest->journal_date;
            $oUserJournal->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_journal_error') ]);
            }
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_journal_article', compact('oUserJournal'));
    }
    
    public function callDeleteJournal(Request $oRequest, $nIdUserJournal)
    {
        $oUserJournal = UserJournals::find($nIdUserJournal);
        if(count($oUserJournal) && $oUserJournal->id_user == Auth::user()->id_user)
        {
            $oUserJournal->activated=0;
            $oUserJournal->deleted=1;
            $oUserJournal->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_journal_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }
    
    public function callAddResearch(Request $oRequest, $nIdResearch = '')
    {
        $oUserResearch = array();
        if(!empty($nIdResearch))
        {
            $oUserResearch = UserResearch::find($nIdResearch);
            if($oUserResearch->id_user == Auth::user()->id_user)
            {
            $dResearchDate = new Carbon($oUserResearch->research_year);
            $oUserResearch->research_month = $dResearchDate->month;
            $oUserResearch->research_year = $dResearchDate->year;
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_research_error') ]);
            }
        }
        
        if ($oRequest->isMethod("POST"))
        {
            $oCarbon = new Carbon();
            $oValidator = Validator::make($oRequest->all(), [
                                                                'research_title' => 'required',
                                                                'research_url' => 'url',
                                                                'research_author' => 'required',
                                                                'research_year' => 'required|integer|max:'.$oCarbon->year,
                                                                'research_month' => 'required|integer|min:1|max:12'
                                                            ]);

            if ($oValidator->fails()) {
                return redirect('/user/add-research')
                            ->withErrors($oValidator)
                            ->withInput();
            }
            $dStartDate = Carbon::create($oRequest->research_year, $oRequest->research_month, 1, 0, 0, 0);

            $oRequest->offsetSet('research_date', $dStartDate->toDateString()); // Add Or Update Paramaeter in REQUEST

            $oUserResearch = UserResearch::firstOrNew(['id_user_research'=>$oRequest->id_user_research]);
            if((count($oUserResearch) && $oUserResearch->id_user == Auth::user()->id_user) || empty($oRequest->id_user_research))
            {
            $oUserResearch->id_user = Auth::user()->id_user;
            $oUserResearch->research_title = trim($oRequest->research_title);
            $oUserResearch->research_url = trim($oRequest->research_url);
            $oUserResearch->research_author = trim($oRequest->research_author);
            $oUserResearch->research_description = trim($oRequest->research_description);
            $oUserResearch->research_year = $oRequest->research_date;
            $oUserResearch->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_research_error') ]);
            }
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_research_work', compact('oUserResearch'));
    }
    
    public function callDeleteResearch(Request $oRequest, $nIdResearch)
    {
        $oUserResearch = UserResearch::find($nIdResearch);
        if(count($oUserResearch) && $oUserResearch->id_user == Auth::user()->id_user)
        {
            $oUserResearch->activated = 0;
            $oUserResearch->deleted = 1;
            $oUserResearch->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_research_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }

    public function callAddPublication(Request $oRequest, $nIdPublication = '')
    {
        $oUserPublication = array();
        if(!empty($nIdPublication))
        {
            $oUserPublication = UserPublications::find($nIdPublication);
            if($oUserPublication->id_user == Auth::user()->id_user)
            {
            $dPublicationDate = new Carbon($oUserPublication->publication_year);
            $oUserPublication->publication_month = $dPublicationDate->month;
            $oUserPublication->publication_year = $dPublicationDate->year;
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_publication_error') ]);
            }
        }
        
        if ($oRequest->isMethod("POST")) {
            $oCarbon = new Carbon();
            $oValidator = Validator::make($oRequest->all(), [
                                                                'publication_title' => 'required',
                                                                'publication_url' => 'url',
                                                                'publication_author' => 'required',
                                                                'publication_year' => 'required|integer|max:'.$oCarbon->year,
                                                                'publication_month' => 'required|integer|min:1|max:12'
                                                            ]);
            if ($oValidator->fails()) {
                return redirect('/user/add-publication')
                                ->withErrors($oValidator)
                                ->withInput();
            }
            $dStartDate = Carbon::create($oRequest->publication_year, $oRequest->publication_month, 1, 0, 0, 0);

            $oRequest->offsetSet('publication_date',  $dStartDate->toDateString()); // Add Or Update Paramaeter in REQUEST
            
            $oUserPublication = UserPublications::firstOrNew(['id_user_publication'=>$oRequest->id_user_publication]);
            if((count($oUserPublication) && $oUserPublication->id_user == Auth::user()->id_user) || empty($oRequest->id_user_publication))
            {
            $oUserPublication->id_user = Auth::user()->id_user;
            $oUserPublication->publication_title = trim($oRequest->publication_title);
            $oUserPublication->publication_url = trim($oRequest->publication_url);
            $oUserPublication->publication_author = trim($oRequest->publication_author);
            $oUserPublication->publication_description = trim($oRequest->publication_description);
            $oUserPublication->publication_year = $oRequest->publication_date;
            $oUserPublication->save();
            }
            else
            {
                return Response::json(['success' => false,'msg'=>trans('messages.authentication_User_publication_error') ]);
            }
            return 'redirect:to:'.url('user/profile/');
        }
        return \View::make('WebView::user._user_publication', compact('oUserPublication'));

    }
    
    public function callDeletePublication(Request $oRequest, $nIdPublication)
    {
        $oUserPublication = UserPublications::find($nIdPublication);
        if(count($oUserPublication) && $oUserPublication->id_user == Auth::user()->id_user)
        {
            $oUserPublication->activated = 0;
            $oUserPublication->deleted = 1;
            $oUserPublication->update();
            $oRequest->session()->flash('success_message', trans('messages.delete_user_publication_success'));
        }
        else
            $oRequest->session()->flash('success_message', trans('messages.invalide_user_delete_item'));
        return back();
    }
    
    public function callAddUserCourse(Request $oRequest)
    {
        if($oRequest->isMethod('POST'))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                                'selected_courses' => 'required'
                                                            ]);
            if($oValidator->fails())
            {
                return redirect('/user/add-user-course')->withErrors($oValidator)
                                                        ->withInput();
            }
            
            foreach ($oRequest['selected_courses'] as $nIdCourseSelected)
            {
                $oUserCourse = UserCourse::firstOrNew([
                                                'id_user' => Auth::user()->id_user,
                                                'id_course' => $nIdCourseSelected
                                            ]);
                $oUserCourse->activated = 1;
                $oUserCourse->deleted = 0;
                $oUserCourse->save();
                
                //Create default group and make user member of that group
                if(isset($oRequest->become_member) && $oRequest->become_member)
                {
                    $oCourse = Course::where('id_course', $nIdCourseSelected)
                                        ->first();
                    
                    //Get campus-admin
                    $oCampusAdmin = User::where('id_campus', Auth::user()->id_campus)
                                            ->where('user_type', config('constants.USERTYPECAMPUSADMIN'))
                                            ->where('activated', 1)
                                            ->where('deleted', 0)
                                            ->first();
                    //Create group related to course_name
                    $oGroup = Group::firstOrNew([
                                                    'group_name' => $oCourse->course_name,
                                                    'id_group_category' => config('constants.COURSEGROUPCATEGORY'),
                                                    'id_campus' => Auth::user()->id_campus,
                                                    'group_type' => config('constants.PUBLICGROUP')
                                                ]);
                    $oGroup->id_user = $oCampusAdmin->id_user;
                    $oGroup->activated = 1;
                    $oGroup->deleted = 0;
                    $oGroup->save();
                    
                    //Make campus admin as creator in group_member table
                    $oGroupCreator = GroupMember::firstOrNew([
                                                    'id_group' => $oGroup->id_group,
                                                    'id_user' => $oCampusAdmin->id_user
                                                ]);
                    $oGroupCreator->member_type = config('constants.GROUPCREATOR');
                    $oGroupCreator->activated = 1;
                    $oGroupCreator->deleted = 0;
                    $oGroupCreator->save();
                    
                    //Make login user member to group
                    $oGroupMember = GroupMember::firstOrNew([
                                                    'id_group' => $oGroup->id_group,
                                                    'id_user' => Auth::user()->id_user
                                                ]);
                    $oGroupMember->member_type = config('constants.GROUPMEMBER');
                    $oGroupMember->activated = 1;
                    $oGroupMember->deleted = 0;
                    $oGroupMember->save();
                }
            }
            return 'redirect:to:'.url('/');
        }
        return \View::make('WebView::user._add_user_course');
    }

    public function callCourseSuggetions(Request $oRequest)
    {
        $sCourseName = isset($oRequest->course_name) ? $oRequest->course_name : '';
        
        $oCourseList = Course::getCourseSuggetions($sCourseName, Auth::user()->id_campus);
	return Response::json($oCourseList);
    }
    
    // get all user created poll 
    public function callUserPoll()
    {
        session(['current_page' => 'poll']);
        
        //Get all my related posts
        $oUserFeeds = Poll::getUserPolls(Auth::user()->id_user);
        
        foreach ($oUserFeeds as $key=>$oFeed)
        {
            $oPollOption = PollOption::getPollOptions($oFeed->id_poll);
            $oUserFeeds[$key]->polloption=$oPollOption;
            if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time()){
                $nTotalAnswer= PollAnswer::where('id_poll','=',$oFeed->id_poll)->count();
                $oPollAnswer = PollAnswer::getPollAnswer($oFeed->id_poll);
                $oUserFeeds[$key]['totalAns']=$nTotalAnswer;
                foreach($oPollAnswer as $answer){
                    $percent=0;
                    $percent=($answer->total * 100) / $nTotalAnswer;

                    $oUserFeeds[$key]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                }
            }
            $oUserFeeds[$key]->comments = array();
        }
        
        if($oUserFeeds->currentPage() > 1)
            return \View::make('WebView::user._more_poll', compact('oUserFeeds'));
        
        return \View::make('WebView::user.user_polls', compact('oUserFeeds'));
    }
    
    public function callUserPollAjax()
    {
        session(['current_page' => 'poll']);
        
        //Get all my related posts
        $oUserFeeds = Poll::getUserPolls(Auth::user()->id_user);
        
        foreach ($oUserFeeds as $key=>$oFeed)
        {
            $oPollOption = PollOption::getPollOptions($oFeed->id_poll);
            $oUserFeeds[$key]->polloption=$oPollOption;
            if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time()){
                $nTotalAnswer= PollAnswer::where('id_poll','=',$oFeed->id_poll)->count();
                $oPollAnswer = PollAnswer::getPollAnswer($oFeed->id_poll);
                $oUserFeeds[$key]['totalAns']=$nTotalAnswer;
                foreach($oPollAnswer as $answer){
                    $percent=0;
                    $percent=($answer->total * 100) / $nTotalAnswer;

                    $oUserFeeds[$key]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                }
            }
            $oUserFeeds[$key]->comments = array();
        }
        if($oUserFeeds->currentPage() > 1)
            return \View::make('WebView::user._more_poll', compact('oUserFeeds'));
        
        return \View::make('WebView::user._poll_ajax', compact('oUserFeeds'));
        
    }
    
    public function callUserAllPoll(Request $oRequest)
    {
        session(['current_page' => 'poll','poll' => 1]);
        
        //Get all my related posts
        $aUncomplitedPolls = array();
        $aComplitedPolls = array();
        $dToday = Carbon::now();
        $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
        $nLimit = config('constants.PERPAGERECORDS');
        
        $nUncomplitedPollCount = Poll::getUncomplitedPollCount(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday);
        $nComplitedPollCount = Poll::getComplitedPollCount(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday);
        
        if ($nUncomplitedPollCount > (($nPageNumber - 1) * $nLimit)) {
            //Get max uncomplited polls
            $oUncomplitedPolls = Poll::getUncomplitedPolls(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday, ($nPageNumber - 1)*$nLimit, $nLimit);
            $aUncomplitedPolls = $oUncomplitedPolls->all();
        }

        //If group requests are less then 10 fetch event requests
        if (($nUncomplitedPollCount + $nComplitedPollCount) > (($nPageNumber - 1) * $nLimit)) {
            if (count($aUncomplitedPolls) < $nLimit) {
                $nSkipRecords = (($nPageNumber - 1) * $nLimit) - $nUncomplitedPollCount;
                $nSkipRecords = max(0, $nSkipRecords);

                $nLimit = config('constants.PERPAGERECORDS') - count($aUncomplitedPolls);
                $oComplitedPolls = Poll::getAllPolls(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday, $nSkipRecords, $nLimit);
                $aComplitedPolls = $oComplitedPolls->all();
            }
        }
        
        $oUserFeeds = array_merge($aUncomplitedPolls, $aComplitedPolls);
        foreach ($oUserFeeds as $key=>$oFeed)
        {
            $oPollOption = PollOption::getPollOptions($oFeed->id_poll);
            $oUserFeeds[$key]->polloption=$oPollOption;
            if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time()){
                $nTotalAnswer= PollAnswer::where('id_poll','=',$oFeed->id_poll)->count();
                $oPollAnswer = PollAnswer::getPollAnswer($oFeed->id_poll);
                $oUserFeeds[$key]['totalAns']=$nTotalAnswer;
                foreach($oPollAnswer as $answer){
                    $percent=0;
                    $percent=($answer->total * 100) / $nTotalAnswer;

                    $oUserFeeds[$key]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                }
            }
            $oUserFeeds[$key]->comments = array();
        }
        
        $nTotalRecords = $nUncomplitedPollCount + $nComplitedPollCount;
        $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

        $aPaginator['total'] = $nTotalRecords;
        $aPaginator['per_page'] = config('constants.PERPAGERECORDS');
        $aPaginator['current_page'] = $nPageNumber;
        $aPaginator['last_page'] = $nLastPage;
        $aPaginator['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
        $aPaginator['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
        $aPaginator['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
        $aPaginator['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
        
        if($nPageNumber > 1)
            return \View::make('WebView::user._more_poll', compact('oUserFeeds'));
        
        return \View::make('WebView::user.user_polls', compact('oUserFeeds', 'aPaginator'));
        
    }
    
    public function callUserAllPollAjax(Request $oRequest)
    {
        session(['current_page' => 'poll']);
        
        //Get all my related posts
        $aUncomplitedPolls = array();
        $aComplitedPolls = array();
        $dToday = Carbon::now();
        $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
        $nLimit = config('constants.PERPAGERECORDS');
        
        $nUncomplitedPollCount = Poll::getUncomplitedPollCount(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday);
        $nComplitedPollCount = Poll::getComplitedPollCount(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday);
        
        if ($nUncomplitedPollCount > (($nPageNumber - 1) * $nLimit)) {
            //Get max uncomplited polls
            $oUncomplitedPolls = Poll::getUncomplitedPolls(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday, ($nPageNumber - 1)*$nLimit, $nLimit);
            $aUncomplitedPolls = $oUncomplitedPolls->all();
        }

        //If group requests are less then 10 fetch event requests
        if (($nUncomplitedPollCount + $nComplitedPollCount) > (($nPageNumber - 1) * $nLimit)) {
            if (count($aUncomplitedPolls) < $nLimit) {
                $nSkipRecords = (($nPageNumber - 1) * $nLimit) - $nUncomplitedPollCount;
                $nSkipRecords = max(0, $nSkipRecords);

                $nLimit = config('constants.PERPAGERECORDS') - count($aUncomplitedPolls);
                $oComplitedPolls = Poll::getAllPolls(Auth::user()->id_user_followings, Auth::user()->id_user_groups, $dToday, $nSkipRecords, $nLimit);
                $aComplitedPolls = $oComplitedPolls->all();
            }
        }
        
        $oUserFeeds = array_merge($aUncomplitedPolls, $aComplitedPolls);
        
        foreach ($oUserFeeds as $key=>$oFeed)
        {
            $oPollOption = PollOption::getPollOptions($oFeed->id_poll);
            $oUserFeeds[$key]->polloption=$oPollOption;
            if($oFeed->id_poll_answer !='' || strtotime($oFeed->end_time) <= time()){
                $nTotalAnswer= PollAnswer::where('id_poll','=',$oFeed->id_poll)->count();
                $oPollAnswer = PollAnswer::getPollAnswer($oFeed->id_poll);
                $oUserFeeds[$key]['totalAns']=$nTotalAnswer;
                foreach($oPollAnswer as $answer){
                    $percent=0;
                    $percent=($answer->total * 100) / $nTotalAnswer;

                    $oUserFeeds[$key]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                }
            }
            $oUserFeeds[$key]->comments = array();
        }
        
        $nTotalRecords = $nUncomplitedPollCount + $nComplitedPollCount;
        $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

        $aPaginator['total'] = $nTotalRecords;
        $aPaginator['per_page'] = config('constants.PERPAGERECORDS');
        $aPaginator['current_page'] = $nPageNumber;
        $aPaginator['last_page'] = $nLastPage;
        $aPaginator['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
        $aPaginator['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
        $aPaginator['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
        $aPaginator['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
        
        if($nPageNumber > 1)
            return \View::make('WebView::user._more_poll', compact('oUserFeeds'));
        
        return \View::make('WebView::user._poll_ajax', compact('oUserFeeds', 'aPaginator'));
        
    }
    
    public function callUserGroupSearch(Request $oRequest)
    {
        $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
        
        $oGroupList = Group::getGroupSearchEvant($oRequest->search_str, $aUserGroups, 10);
        foreach($oGroupList as $nKey => $oGroup)
        {
            if($oGroup->section !='')
               $oGroupList[$nKey]->group_name =  $oGroup->group_name.' '.trans('messages.section').'-'.$oGroup->section.'('.$oGroup->semester.')';
            $oGroupList[$nKey]->group_img = setGroupImage($oGroup->file_name,$oGroup->group_category_image);
        }
        return $oGroupList->all();
    }

    public function callChangeProfileImage(Request $oRequest)
    {
        $aValidationRequiredFor = [
                                    'Img_file' => 'required|image|mimes:jpg,jpeg,png|max:10000'
                                  ];
        $this->validate($oRequest, $aValidationRequiredFor);
        
        $file = Input::file('Img_file');
        
        $aCropdata = json_decode(stripslashes($_POST['data']));
        $tmp_img_w = intval($aCropdata->width);
        $tmp_img_h = intval($aCropdata->height);
        $src_x = intval($aCropdata->x);
        $src_y = intval($aCropdata->y);
        
        $file_name= time().'.'.$file->getClientOriginalExtension();
        
        $img = Image::make($file);
        $cropImage = $img->crop($tmp_img_w, $tmp_img_h, $src_x, $src_y);
        $cropImageUpload = $cropImage->stream();
        $cropImage->save(base_path().'/uploads/'.$file_name);
        
        $sFileDestination = base_path().'/uploads/'.$file_name;
                
        $oS3 = \Storage::disk('s3');
        $filePath = '/'.config('constants.USERMEDIAFOLDER').'/' . $file_name;
        $oS3->put($filePath, $cropImageUpload->__toString(),'public');

        $thumb_widths = array(50,100,150);
        foreach($thumb_widths as $thumb_width)
        {
            $resize = Image::make($sFileDestination)->resize($thumb_width*2, $thumb_width*2);
            $resizeNew = $resize->stream();
            $n_name= $thumb_width.'_'.$file_name;
         
            $oS3 = \Storage::disk('s3');
            $filePath = '/'.config('constants.USERMEDIAFOLDER').'/' . $n_name;
            $oS3->put($filePath, $resizeNew->__toString(), 'public');

        }
        
        $aResult = array(
            'state'  => 200,
            'result' =>  !empty($aCropdata) ? $sFileDestination : '',
            'sFileName' => $file_name
        );
        
        unlink($sFileDestination);
        
        if($aResult['state']== 200)
        {
            UserProfileImages::create([
                                        'id_user' => Auth::user()->id_user,
                                        'file_name' => $aResult['sFileName'],
                                        'activated' => 1,
                                        'deleted' => 0
                                    ]);
        }
        return json_encode($aResult);
    }
   
    public function getGlobalSearchDataAjax(Request $oRequest,$sSearchStr) 
    {
        $oUser= User::getUserSearch($sSearchStr,Auth::user()->id_user,Auth::user()->id_campus,5);
        foreach($oUser as $key=>$oMember)
        {
            $oUser[$key]->major='';
            $aMajor=UserEducation::getMajorEducation($oMember->id_user);
            if(count($aMajor)>0)
            {
                $oUser[$key]->major= $aMajor['degree'];
                if(!empty($aMajor['major']))
                $oUser[$key]->major= htmlspecialchars($aMajor['degree'].', '.$aMajor['major']);
            }
            $oUser[$key]->profile_img = setProfileImage("50",$oMember->user_profile_image,$oMember->id_user,$oMember->first_name,$oMember->last_name);
        }
        return Response::json($oUser);
    }
    
    public function getGlobalSearchData(Request $oRequest) 
    {
        $sGSearchStr = $oRequest->globalSearch;
        $oUser= User::getUserSearch($sGSearchStr,Auth::user()->id_user,Auth::user()->id_campus);
        foreach($oUser as $key=>$oMember)
        {
            $oUser[$key]->major = UserEducation::getMajorEducation($oMember->id_user);
        }
        //echo "<pre>";print_r($oUser);exit;
        if($oUser->currentPage() > 1)
            return \View::make('WebView::user._more_global_search', compact('oUser'));
        
        return \View::make('WebView::user.global_search', compact('oUser','sGSearchStr'));
    }
    
    //get sidebar data for user follow
    public function getSidebarContent()
    {
        $oUsers = User::getFacultyList(Auth::user()->id_user, Auth::user()->id_campus);
        foreach($oUsers as $key=>$oUser)
            {
                $oUsers[$key]->major=UserEducation::getMajorEducation($oUser->id_user);
            }
        return $oUsers;
    }
    
    //set user education as default in existed user education
    public function updateUserEducation()
    {
        $oUsers = User::all();
        foreach($oUsers as $key=>$oUser)
            {
                $oUserEducationCount =UserEducation::where('id_user', $oUser->id_user)
                                                    ->where('default_education', 1)
                                                    ->count();
                if($oUserEducationCount <=0)
                {
                    $oUserEducation = UserEducation::getMajorEducation($oUser->id_user);
                    if(count($oUserEducation) > 0)
                    {
                        $oUserEducationUpdate = UserEducation::find($oUserEducation->id_user_education);
                        $oUserEducationUpdate->default_education = 1;
                        $oUserEducationUpdate->update();
                    }
                }
            }
        return $oUsers;
    }
    
    public function callCheckLogin()
    {
        return count(Auth()->user());
    }
}