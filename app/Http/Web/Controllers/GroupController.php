<?php
namespace App\Http\Web\Controllers;

use App\Http\Web\Controllers\Controller;

use App\Libraries\User;
use App\Libraries\Campus;
use App\Libraries\Group;
use App\Libraries\GroupCategory;
use App\Libraries\GroupMember;
use App\Libraries\GroupRequest;
use App\Libraries\GroupEvent;
use App\Libraries\GroupPost;
use App\Libraries\Comment;
use App\Libraries\EventRequest;
use App\Libraries\UserEducation;
use App\Libraries\Notification;
use App\Libraries\Poll;
use App\Libraries\PollOption;
use App\Libraries\PollAnswer;
use App\Libraries\Document;
use App\Libraries\DocumentPermisssion;
use App\Libraries\GroupDocument;
use App\Libraries\NonMemberInvitation;
use App\Libraries\QuizUserInvite;
use App\Libraries\QuizGroupInvite;
use App\Libraries\Quiz;

use App\Libraries\AttendanceCode;
use App\Libraries\GroupAttendanceLog;
use App\Libraries\UserGroupAttendance;
use App\Libraries\GroupLecture;
use App\Libraries\AllowedEmail;

use Auth;
use Carbon\Carbon;
use Session;
use Image;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

use App\Jobs\SendGroupInvitation;
use App\Jobs\ShareGroupDocumentsWithNewMember;
use App\Jobs\RevokeGroupDocPermission;
use App\Jobs\SyncUserGroups;
use App\Jobs\AddUserAttendanceForGroup;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', [
                                    'only' => [
                                                'callAddGroup', 'callAddCourseGroup',
                                                'callAutoSuggetionsForInviteUser','callGroupRequestStatus',
                                                'callGroupSearch', 'callGroupSearchAjax',
                                                'callAdminGroupListing', 'callAdminGroupListingAjax',
                                                'callMemberGroupListing', 'callMemberGroupListingAjax',
                                                'callDeactiveGroupListing', 'callDeactiveGroupListingAjax',
                                                'callGroupSettings','callGroupSettingsAjax', 'callJoinGroup',
                                                'callGroupFeeds', 'callGroupFeedsAjax',
                                                'callGroupMembers', 'callGroupMembersAjax',
                                                'callGroupPhotos', 'callGroupPhotosAjax',
                                                'callGroupDocuments', 'callGroupDocumentsAjax','callGroupDocumentsSidebarAjax',
                                                'callGroupPostDocument','callGroupPostDocumentAjax','callGroupDocumentsSidebarAjax',
                                                'callGroupEvents', 'callGroupEventsAjax',
                                                'callGroupPoll', 'callGroupPollAjax',
                                                'callInviteUser', 'callInviteUserAjax',
                                                'callInviteUserViaCsv', 'callInviteUserAgain',
                                                'callLeaveGroup', 'callPassCreatorRights',
                                                'callEditGroupImage', 'callRemoveMember',
                                                'callGetAttendanceExcel','callGetAttendance','callGetAttendanceAjax','callCourseAttendance',
                                                'callAdminGroupSuggetion','callAddAttendance','callSubmitAttendance','callCourseAttendanceAjax',
                                                'callGetAttendanceTable','getStudentSignleAttendace','callGroupMemberAsAdmin','callDeleteGroupLecture'                                           
                                            ]
                            ]
                        );
        parent::__construct();
        session(['current_page' => 'groups']);
    }
    
    /**
     * 
     * @param Request $oRequest
     * @return type
     */
    public function callAddGroup(Request $oRequest)
    {
        $aGroupCategories = GroupCategory::where('activated','=',1)
                                            ->where('deleted','=',0)
                                            ->get();
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                    'group_category' => 'required',
                                                    'group_name' => 'required|max:128',
                                                    'group_type' => 'required'
                                        ]);
            if($oValidator->fails())
            {
                return redirect('/group/add-group')->withErrors($oValidator)
                                                    ->withInput();
            }
            //print_r($oRequest->about_group);exit;
            $oCreatedGroup = Group::create([
                                                'id_group_category' => $oRequest->group_category,
                                                'group_name' => $oRequest->group_name,
                                                'group_type' => $oRequest->group_type,
                                                'about_group' => $oRequest->about_group,
                                                'id_user' => Auth()->user()->id_user,
                                                'id_campus' => Auth()->user()->id_campus
                                            ]);
            GroupMember::create([
                                'id_group' => $oCreatedGroup->id_group,
                                'id_user' => Auth()->user()->id_user,
                                'member_type' => config('constants.GROUPCREATOR'),
                            ]);
            return 'redirect:to:'.url('group/member-invite/'. $oCreatedGroup->id_group);
        }
        return \View::make('WebView::group._add_group', compact('aGroupCategories'));
    }
    
    public function callAddCourseGroup(Request $oRequest)
    {
        $oCampus = Campus::find(Auth()->user()->id_campus);
        if($oRequest->isMethod("POST"))
        {
            $oValidator = Validator::make($oRequest->all(), [
                                                    'group_name' => 'required',
                                                    'section_name' => 'required|max:4',
                                                    'semester' =>'required'
                                        ]);
            if($oValidator->fails())
            {
                return redirect('/group/add-course-group')->withErrors($oValidator)
                                                    ->withInput();
            }
            $oCreatedGroup = Group::create([
                                                'id_group_category' => config('constants.COURSEGROUPCATEGORY'),
                                                'group_name' => $oRequest->group_name,
                                                'group_type' => config('constants.SECRETGROUP'),
                                                'section' => $oRequest->section_name,
                                                'semester' => $oRequest->semester.'-'.Carbon::now()->year,
                                                'id_user' => Auth()->user()->id_user,
                                                'id_campus' => Auth()->user()->id_campus
                                            ]);
            GroupMember::create([
                                'id_group' => $oCreatedGroup->id_group,
                                'id_user' => Auth()->user()->id_user,
                                'member_type' => config('constants.GROUPCREATOR'),
                            ]);
            return 'redirect:to:'.url('group/member-invite/'. $oCreatedGroup->id_group);
        }
        return \View::make('WebView::group._add_course_group',compact('oCampus'));
    }
    
    /**
     * This function will use to give auto suggetions
     * during invite user proccess. Will give auto suggetion of user
     * which are not member of current group
     */
    public function callAutoSuggetionsForInviteUser(Request $oRequest, $nIdGroup)
    {
	
        //Get all member ids for $nIdGroup.
        //Get all pending members for $nIdGroup.
        //Merge id_user for both arrays.
        
        //Get users which are not in above array
        $sUserName = $oRequest->term;
	
        $oUsersList = GroupMember::getGroupMemberSuggetions($sUserName, $nIdGroup, Auth::user()->id_campus);
	return Response::json($oUsersList);
    }
    
    /**
     * 
     * @param Request $oRequest
     */
    public function callInviteUser(Request $oRequest, $nIdGroup)
    {
        //Set variables for sidebar
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        if($oGroupDetails->member == '' || ( count($oGroupMemberDetail) && ($oGroupMemberDetail->member_type != config('constants.GROUPADMIN') && $oGroupMemberDetail->member_type != config('constants.GROUPCREATOR')) && $oGroupDetails->id_group_category != config('constants.UNIVERSITYGROUPCATEGORY')) )
            return redirect(route("group.group-feeds",['nIdGroup'=>$nIdGroup]));
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        if($oRequest->isMethod('POST'))
        {
            $aValidationRequiredFor = [
                                        'invite_to' => 'required_array'
                                    ];
            
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $aErrors = array();
            $aInputs = array();
            
            $aCheckedEmails = $this->checkValidEmails($oRequest->invite_to);
            
            $this->dispatch(new SendGroupInvitation(Auth::user(), $aCheckedEmails['valid_emails'], $oRequest->id_group));
            
            if(count($aCheckedEmails['invalid_email']) || count($aCheckedEmails['out_campus_email']))
            {
                if(count($aCheckedEmails['invalid_email']) || count($aCheckedEmails['out_campus_email']))
                {
                    $aErrors['invite_to'] = trans('messages.invalid_and_out_campus_emails', 
                                                ['in_valid_email' => implode('<br />', $aCheckedEmails['invalid_email']),
                                                    'out_campus_email' => implode('<br />', $aCheckedEmails['out_campus_email'])]
                                            );
                }
                $aInputs['invite_to'] = array_merge($aCheckedEmails['invalid_email'], $aCheckedEmails['out_campus_email']);
            }
            else{
                $oRequest->session()->flash('success_message', trans('messages.invite_group_successful'));
            }
            return redirect(route('group.invite-member', ['nIdGroup' => $oRequest->id_group]))
                    ->withErrors($aErrors)
                    ->withInput($aInputs);
        }
        return \View::make('WebView::group.group_invite_member', compact('nIdGroup', 'oGroupDetails', 'oGroupMemberDetail', 'aGroupSpecificCounts'));
    }
    
    public function callInviteUserViaCsv(Request $oRequest)
    {
        $aValidationRequiredFor = [
                                    'file' => 'required'
                                ];
        
        if($oRequest->hasFile('file'))
        {
            $oRequest->offsetSet('extention', $oRequest->file('file')->getClientOriginalExtension());

            $aValidationRequiredFor['extention'] = 'required|in:csv';
        }
        
        $this->validate($oRequest, $aValidationRequiredFor);
        
        $aErrors = array();
        
        $aEmails = $this->parseCsvFile($oRequest);
        
        $aCheckedEmails = $this->checkValidEmails($aEmails);
        
        $this->dispatch(new SendGroupInvitation(Auth::user(), $aCheckedEmails['valid_emails'], $oRequest->id_group));
        if(count($aCheckedEmails['invalid_email']) || count($aCheckedEmails['out_campus_email']))
        {
            if(count($aCheckedEmails['invalid_email']) && count($aCheckedEmails['out_campus_email']))
            {
                $aErrors['csv_invite_to'] = trans('messages.invalid_and_out_campus_emails', 
                                            ['in_valid_email' => implode(',', $aCheckedEmails['invalid_email']),
                                                'out_campus_email' => implode(',', $aCheckedEmails['out_campus_email'])]
                                        );
            }
            else if(count($aCheckedEmails['invalid_email']))
            {
                $aErrors['csv_invite_to'] = trans('messages.invalid_emails', 
                                            ['in_valid_email' => implode(',', $aCheckedEmails['invalid_email'])]
                                        );
            }
            else
            {
                $aErrors['csv_invite_to'] = trans('messages.out_campus_emails', 
                                            ['out_campus_email' => implode(',', $aCheckedEmails['out_campus_email'])]
                                        );
            }
        }
        else{
            $oRequest->session()->flash('success_message', trans('messages.invite_group_successful'));
        }
        return redirect(route('group.invite-member', ['nIdGroup' => $oRequest->id_group]))
                ->withErrors($aErrors)
                ->withInput(array());
    }
    
    private function parseCsvFile($oRequest)
    {
        $aInvitedEmails = array();
        
        $oUploadedFile = $oRequest->file('file');
        
        $sDestinationPath = base_path().'/uploads/'; // upload path
        $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
        $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
        $oUploadedFile->move($sDestinationPath, $sFileName); // uploading file to given path
        
        $aImportedData = array_map('str_getcsv', file($sDestinationPath.$sFileName));
        
        foreach ($aImportedData as $aItem)
        {
            if(count($aItem) > 2)
                $aInvitedEmails[] = $aItem[2];
        }
        
        //Remove local copy of media
        unlink($sDestinationPath.$sFileName);
        
        return $aInvitedEmails;
    }
    
    private function checkValidEmails($aEmails)
    {
        $aResponse['valid_emails'] = array();
        $aResponse['invalid_email'] = array();
        $aResponse['out_campus_email'] = array();
            
        $aEmailData = explode("@", Auth::user()->email);
        $sEmailCampus = $aEmailData[1];
        
        foreach ($aEmails as $sEmail)
        {
            $sEmail = trim($sEmail);
            if(!empty($sEmail))
            {
                if(filter_var($sEmail, FILTER_VALIDATE_EMAIL))
                {
                    //Check if user exist in database
                    $oUser = User::whereEmail($sEmail)
                                    ->where('id_campus', '=', Auth::user()->id_campus)
                                    ->first();
                    if($oUser)
                    {
                        $aResponse['valid_emails'][] = $sEmail;
                        continue;
                    }
                    
                    //Check if user is from same email
                    $aInputEmailData = explode("@", $sEmail);
                    if((mb_strpos($aInputEmailData[1], $sEmailCampus) !== FALSE) &&
                            (mb_strpos($sEmailCampus, $aInputEmailData[1]) !== FALSE))
                    {
                        $aResponse['valid_emails'][] = $sEmail;
                        continue;
                    }
                    
                    //Check domain is in custom campus url array
                    $aCustomCampusUrls = config('constants.CUSTOMCAMPUSURL');
                    if(array_key_exists($aInputEmailData[1], $aCustomCampusUrls) && 
                        in_array(Auth::user()->id_campus, $aCustomCampusUrls[$aInputEmailData[1]])) {
                        $aResponse['valid_emails'][] = $sEmail;
                        continue;
                    }
                    
                    //Check if user is allowed for campus
                    $oAllowedEmail = AllowedEmail::where('email',$sEmail)
                                                    ->where('id_campus',Auth::user()->id_campus)
                                                    ->first();
                    if($oAllowedEmail) {
                        $aResponse['valid_emails'][] = $sEmail;
                        continue;
                    }
                    
                    //Check for subdomain
                    $bEmailNotFound = TRUE;
                    $aUrlComponents = explode(".",$aInputEmailData[1]);
                    $sUrl = $aInputEmailData[1];
                    foreach($aUrlComponents as $sUrlComponent)
                    {
                        $sUrl = mb_strstr($sUrl, ".");
                        $sUrl = trim($sUrl, ".");
                        
                        $aNewUrlComponents = explode(".",$sUrl);
                        if(count($aNewUrlComponents) >= 2 && ((mb_strpos($sUrl, $sEmailCampus) !== FALSE) ||
                            (mb_strpos($sEmailCampus, $sUrl) !== FALSE)))
                        {
                            $bEmailNotFound = FALSE;
                            $aResponse['valid_emails'][] = $sEmail;
                            break;
                        }
                    }
                    
                    if($bEmailNotFound)
                        $aResponse['out_campus_email'][] = $sEmail;
                }
                else
                {
                    $aResponse['invalid_email'][] = $sEmail;
                }
            }
        }
        return $aResponse;
    }
    
    /**
     * This function will use to show group request and status.
     * @param type $nIdGroup
     */
    public function callGroupRequestStatus(Request $oRequest, $nIdGroup)
    {
        $oGroupRequests = array();
        $oNonMemberRequests = array();
        
        $nPageNumber = isset($oRequest->page) ? intval($oRequest->page) : 1;
        $nLimit = config('constants.PERPAGERECORDS');
        //$nLimit = 5;

        $nGroupRequestCount = GroupRequest::where('id_group', '=', $nIdGroup)
                                           ->where('request_type',config('constants.GROUPREQUESTTYPEINVITE'))
                                            ->count();

        $nNonMemberRequestCount = NonMemberInvitation::where('id_entity_invite_for', '=', $nIdGroup)
                                                        ->where('entity_type', '=', 'G')
                                                        ->count();
        
        if($nGroupRequestCount > (($nPageNumber-1) * $nLimit))
        {
            //Get max 10 group requests
            $oGroupRequests = GroupRequest::getGroupRequests($nIdGroup, (($nPageNumber - 1) * config('constants.PERPAGERECORDS')), $nLimit);
        }
        
        //If group requests are less then 10 fetch non member requests
        if(($nNonMemberRequestCount + $nGroupRequestCount) > (($nPageNumber-1) * $nLimit))
        {
            if(count($oGroupRequests) < config('constants.PERPAGERECORDS'))
            {
                $nSkipRecords = (($nPageNumber-1)*config('constants.PERPAGERECORDS')) - $nGroupRequestCount;
                $nSkipRecords = max(0, $nSkipRecords);

                $nLimit = config('constants.PERPAGERECORDS') - count($oGroupRequests);
                $oNonMemberRequests = NonMemberInvitation::getNonMemberRequests($nIdGroup, "G", $nSkipRecords, $nLimit);
            }
        }
        
        $nTotalRecords = $nGroupRequestCount + $nNonMemberRequestCount;
        $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);
        
        if($nPageNumber > 1)    
        return \View::make('WebView::group._more_group_request_status', compact('nIdGroup', 'oGroupRequests', 'oNonMemberRequests', 'nLastPage', 'nPageNumber'));
        
        return \View::make('WebView::group._group_request_status', compact('nIdGroup', 'oGroupRequests', 'oNonMemberRequests', 'nLastPage', 'nPageNumber'));
    }

    public function callEditGroupImage(Request $oRequest)
    {
        
        $file = Input::file('Img_file');
        $aCropdata = json_decode(stripslashes($_POST['avatar_data']));
        $tmp_img_w = intval($aCropdata->width);
        $tmp_img_h = intval($aCropdata->height);
        $src_x = intval($aCropdata->x);
        $src_y = intval($aCropdata->y);
        
        $file_name= time().'.'.$file->getClientOriginalExtension() ;
        
        $img = Image::make($file);
        $cropImage = $img->crop($tmp_img_w, $tmp_img_h, $src_x, $src_y);
        $cropImageUpload = $cropImage->stream();
        $cropImage->save(base_path().'/uploads/'.$file_name);
        
        $sFileDestination = base_path().'/uploads/'.$file_name;
                
        $oS3 = \Storage::disk('s3');
        $filePath = '/'.config('constants.GROUPMEDIAFOLDER').'/' . $file_name;
        $oS3->put($filePath, $cropImageUpload->__toString(),'public');

        $thumb_widths = array(50,100,150);
        foreach($thumb_widths as $thumb_width)
        {
            $resize = Image::make($sFileDestination)->resize($thumb_width*2, $thumb_width*2);
            $resizeNew = $resize->stream();
            $n_name= $thumb_width.'_'.$file_name;
         
            $oS3 = \Storage::disk('s3');
            $filePath = '/'.config('constants.GROUPMEDIAFOLDER').'/' . $n_name;
            $oS3->put($filePath, $resizeNew->__toString(), 'public');

        }
        $aResult = array(
            'state'  => 200,
            'result' =>  !empty($aCropdata) ? $sFileDestination : '',
            'sFileName' => $file_name
        );
        
       unlink($sFileDestination);
       
        if($aResult['state']== 200)
        {
            $objGroup=Group::find($oRequest->group_id);
            $objGroup->file_name=$aResult['sFileName'];
            $objGroup->update();
        }
        return json_encode($aResult);
        
    }

    public function callGroupSearch(Request $oRequest)
    {
        //Get user groups
        $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
        
        $sSeacrhStr = isset($oRequest->search_param) ? $oRequest->search_param : '';
        $aSearchParam['search_term'] = $sSeacrhStr;
        $aSearchParam['id_group_category'] = isset($oRequest->id_group_category) ? $oRequest->id_group_category : '';
        $aSearchParam['id_campus'] = Auth::user()->id_campus;
        $aSearchParam['user_groups'] = $aUserGroups;
        
        $oGroupList['recommended_groups'] = Group::getGroupSearch($aSearchParam);
        $oGroupList['deactive_groups_count'] = Group::from( 'groups as g' )
                                                    ->leftJoin('group_members as gm', 'g.id_group', '=', 'gm.id_group')
                                                    ->where('gm.id_user', Auth::user()->id_user)
                                                    ->where('gm.activated', 0)
                                                    ->where('gm.deleted', 0)
                                                    ->count();
        
        return \View::make('WebView::group.group_listing', compact('oGroupList','sSeacrhStr'));
    }
    
    public function callGroupSearchAjax(Request $oRequest,$sSeacrhStr ='')
    {
        //Get user groups
        $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
        
        $aSearchParam['search_term'] = $sSeacrhStr;
        $aSearchParam['id_group_category'] = isset($oRequest->id_group_category) ? $oRequest->id_group_category : '';
        $aSearchParam['id_campus'] = Auth::user()->id_campus;
        $aSearchParam['user_groups'] = $aUserGroups;
        
        $oGroupList['recommended_groups'] = Group::getGroupSearch($aSearchParam);
        
        if($oGroupList['recommended_groups']->currentPage() > 1)
            return \View::make('WebView::group._more_recommended_group_listing', compact('oGroupList'));
        return \View::make('WebView::group._recommended_group_listing', compact('oGroupList','sSeacrhStr'));
    }

    public function callAdminGroupListing(Request $oRequest,$sSeacrhStr='')
    {
        $oGroupList['deactive_groups_count'] = Group::from( 'groups as g' )
                                                    ->leftJoin('group_members as gm', 'g.id_group', '=', 'gm.id_group')
                                                    ->where('gm.id_user', Auth::user()->id_user)
                                                    ->where('gm.activated', 0)
                                                    ->where('gm.deleted', 0)
                                                    ->count();
        
        /** Get list of groups in which user is admin **/
        //$sSeacrhStr = isset($oRequest->search_param) ? $oRequest->search_param : '';
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPCREATOR'), config('constants.GROUPADMIN')];
        $oGroupList['admin_groups'] = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions,$sSeacrhStr);
        unset($aOptionalConditions);
        
        return \View::make('WebView::group.group_listing', compact('oGroupList','sSeacrhStr'));
    }
    
    public function callAdminGroupListingAjax(Request $oRequest,$sSeacrhStr='')
    {
        /** Get list of groups in which user is admin **/
        //$sSeacrhStr = isset($oRequest->search_param) ? $oRequest->search_param : '';
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPCREATOR'), config('constants.GROUPADMIN')];
        $oGroupList['admin_groups'] = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions,$sSeacrhStr);
        unset($aOptionalConditions);
        
        if($oGroupList['admin_groups']->currentPage() > 1)
            return \View::make('WebView::group._more_admin_group_listing', compact('oGroupList'));
        return \View::make('WebView::group._admin_group_listing', compact('oGroupList','sSeacrhStr'));
    }

    public function callMemberGroupListing(Request $oRequest)
    {
        $oGroupList['deactive_groups_count'] = Group::from( 'groups as g' )
                                                    ->leftJoin('group_members as gm', 'g.id_group', '=', 'gm.id_group')
                                                    ->where('gm.id_user', Auth::user()->id_user)
                                                    ->where('gm.activated', 0)
                                                    ->where('gm.deleted', 0)
                                                    ->count();
        
        /** Get list of groups in which user is only member **/
        $sSeacrhStr = isset($oRequest->search_param) ? $oRequest->search_param : '';
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPMEMBER')];
        $oGroupList['member_groups'] = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions,$sSeacrhStr);
        unset($aOptionalConditions);
        
        return \View::make('WebView::group.group_listing', compact('oGroupList','sSeacrhStr'));
    }
    
    public function callMemberGroupListingAjax(Request $oRequest,$sSeacrhStr='')
    {
        /** Get list of groups in which user is only member **/
        //$sSeacrhStr = isset($oRequest->search_param) ? $oRequest->search_param : '';
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 1;
        $aOptionalConditions['member_types'] = [config('constants.GROUPMEMBER')];
        $oGroupList['member_groups'] = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions,$sSeacrhStr);
        unset($aOptionalConditions);
        
        if($oGroupList['member_groups']->currentPage() > 1)
            return \View::make('WebView::group._more_member_group_listing', compact('oGroupList'));
        return \View::make('WebView::group._member_group_listing', compact('oGroupList','sSeacrhStr'));
    }
    
    public function callDeactiveGroupListing(Request $oRequest)
    {
        $oGroupList['deactive_groups_count'] = Group::from( 'groups as g' )
                                                    ->leftJoin('group_members as gm', 'g.id_group', '=', 'gm.id_group')
                                                    ->where('gm.id_user', Auth::user()->id_user)
                                                    ->where('gm.activated', 0)
                                                    ->where('gm.deleted', 0)
                                                    ->count();
        
        /** Get list of groups in which user is removed **/
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 0;
        $oGroupList['deactive_groups'] = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions);
        
        return \View::make('WebView::group.group_listing', compact('oGroupList','sSeacrhStr'));
    }
    
    public function callDeactiveGroupListingAjax(Request $oRequest)
    {
        /** Get list of groups in which user is removed **/
        $aOptionalConditions = array();
        $aOptionalConditions['activated'] = 0;
        $oGroupList['deactive_groups'] = Group::getGroupList(Auth::user()->id_user, '', $aOptionalConditions);
        
        if($oGroupList['deactive_groups']->currentPage() > 1)
            return \View::make('WebView::group._more_deactive_group_listing', compact('oGroupList'));
        return \View::make('WebView::group._deactive_group_listing', compact('oGroupList'));
    }
    
    public function callGroupSettings(Request $oRequest, $nIdGroup)
    {
        $aGroupCategories = GroupCategory::where('activated','=',1)
                                            ->where('deleted','=',0)
                                            ->get();
        
        $oGroupDetails = Group::getGroupDetail($nIdGroup);
        if($oGroupDetails->id_user == Auth::user()->id_user || $oGroupDetails->member_type == config('constants.GROUPCREATOR') || $oGroupDetails->member_type == config('constants.GROUPADMIN'))
        {
            $sPage='setting';
            $oAdminMember = GroupMember::getGroupUserSuggetions('',$nIdGroup,config('constants.GROUPADMIN'));
        }
        else
            $sPage='about';

        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        if($oRequest->isMethod("POST"))
        {  
            $aValidationRequiredFor = [
                                        'id_group' => 'required',
                                        'group_name' => 'required|unique:groups,group_name,'.$oRequest->id_group.',id_group',
                                        'group_type' => 'required',
                                    ];
            $this->validate($oRequest, $aValidationRequiredFor);
            
            $oGroup = Group::where('id_group', '=', $oRequest->id_group)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->first();
            if($oGroup->id_user == Auth()->user()->id_user)
                $oGroup->group_name = $oRequest->group_name;
            if( $oGroupDetails->member_type != config('constants.GROUPMEMBER'))
            {
                $oGroup->group_type = $oRequest->group_type;
                $oGroup->about_group = $oRequest->about_group;
                $oGroup->update();
            }
            
            return redirect(route('group.group-settings', ['nIdGroup' => $oRequest->id_group]));
        }
        return \View::make('WebView::group.group_settings', compact(['aGroupCategories', 'oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail','sPage','oAdminMember']));
    }
    
    public function callGroupSettingsAjax(Request $oRequest, $nIdGroup)
    {
        $aGroupCategories = GroupCategory::where('activated','=',1)
                                            ->where('deleted','=',0)
                                            ->get();
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);

        if($oGroupDetails->id_user == Auth()->user()->id_user || $oGroupDetails->member_type == config('constants.GROUPCREATOR') || $oGroupDetails->member_type == config('constants.GROUPADMIN'))
        {
            $sPage='setting';
            $oAdminMember = GroupMember::getGroupUserSuggetions('',$nIdGroup,config('constants.GROUPADMIN'));
        }
        else
            $sPage='about';
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_settings_ajax', compact(['aGroupCategories', 'oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail','sPage','oAdminMember']))->render();
    
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    public function callGroupMemberSuggestion(Request $oRequest,$nIdGroup) 
    {
       
        $sSearchFor = isset($oRequest->member_str) ? $oRequest->member_str : '';
         
        $oGroupList = GroupMember::getGroupUserSuggetions($sSearchFor,$nIdGroup,config('constants.GROUPMEMBER'));
        foreach($oGroupList as $key=>$oMember)
        {
            $oGroupList[$key]->profile_img = setProfileImage("50",$oMember->user_profile_image,$oMember->id_user,$oMember->first_name,$oMember->last_name);
        }
        return Response::json($oGroupList);
    }
    
    public function callGroupMemberAsAdmin(Request $oRequest,$nIdGroup) 
    {
        $oGroupMember = GroupMember::firstOrNew([ 'id_group' => $nIdGroup,'id_user'=>$oRequest->user_id ]);
        $oGroupMember->member_type = $oRequest->member_type;
        $oGroupMember->save();
        
        $oQuizIds = QuizGroupInvite::getGroupQuizIds($nIdGroup);
        $aQuizId = explode(",", $oQuizIds[0]->id_quiz);
        if($oRequest->member_type == 'A')
        {
        QuizUserInvite::whereIn('id_quiz',$aQuizId)
                      ->where('id_user',$oRequest->user_id)
                      ->update(['activated'=>0,'deleted'=>1]);
        }
        else
        {
            $aQuizId = array_filter($aQuizId);
            if(count($aQuizId) > 0)
            {
                foreach($aQuizId as $nQuizId )
                {
                    $oGroupQuiz = QuizGroupInvite::where(['id_group'=> $nIdGroup ,'id_quiz' => $nQuizId])->first();
                    $oQuizUserInvite = QuizUserInvite::firstOrNew(['id_user' => $oRequest->user_id,
                                                                   'id_quiz' => $nQuizId ]);
                    $oQuizUserInvite->start_time = $oGroupQuiz->start_time;
                    $oQuizUserInvite->end_time = $oGroupQuiz->end_time;
                    $oQuizUserInvite->save();
                }
            }
        }
        $oUser = User::getUserBasicDetail($oRequest->user_id);
        $oUser->profile_img = setProfileImage("50",$oUser->user_profile_pic,$oUser->id_user,$oUser->first_name,$oUser->last_name);
        $oUser->name = $oUser->first_name.' '.$oUser->last_name;
        
        $this->dispatch(new SyncUserGroups($oGroupMember->id_user));
        
        return Response::json($oUser);
    }
    private function getGroupSpecificCounts($nIdGroup)
    {
        $nGroupMemberCount = GroupMember::where('id_group', '=', $nIdGroup)
                                        ->where('activated', '=', 1)
                                        ->where('deleted', '=', 0)
                                        ->count();
        
        $nGroupEventsCount = GroupEvent::leftJoin('events', 'group_events.id_event', '=', 'events.id_event')
                                        ->where('group_events.id_group', '=', $nIdGroup)
                                        ->where('events.activated', '=', 1)
                                        ->where('events.deleted', '=', 0)
                                        ->count();
        
        $nGroupPhotosCount = GroupPost::leftJoin('posts', 'group_posts.id_post', '=', 'posts.id_post')
                                        ->where('group_posts.id_group', '=', $nIdGroup)
                                        ->where('posts.post_type', '=', config('constants.POSTTYPEIMAGE'))
                                        ->where('posts.activated', '=', 1)
                                        ->where('posts.deleted', '=', 0)
                                        ->count();
        
        /*$nGroupDocumentsCount = GroupPost::leftJoin('posts', 'group_posts.id_post', '=', 'posts.id_post')
                                        ->where('group_posts.id_group', '=', $nIdGroup)
                                        ->where('posts.post_type', '=', config('constants.POSTTYPEDOCUMENT'))
                                        ->where('posts.activated', '=', 1)
                                        ->where('posts.deleted', '=', 0)
                                        ->count();
        */
        $aGroupSpecificCounts = array();
        $aGroupSpecificCounts['members_count'] = $nGroupMemberCount;
        $aGroupSpecificCounts['events_count'] = $nGroupEventsCount;
        $aGroupSpecificCounts['photos_count'] = $nGroupPhotosCount;
        //$aGroupSpecificCounts['documents_count'] = $nGroupDocumentsCount;
        
        return $aGroupSpecificCounts;
    }
    
    public function callGroupFeeds($nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        $oGroupRequestJoin='';
        if(empty($oGroupDetails))
        {      
            return \View::make('WebView::group.group_feeds', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds','oGroupListTab','oGroupRequestJoin'));
        }
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }

        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oGroupFeeds = GroupPost::getGroupPost($nIdGroup);

        $oGroupListTab    =  GroupMember::getUserGroupsTab(Auth::user()->id_user);
        
        foreach ($oGroupFeeds as $key=>$oGroupFeed)
        {
            $oComments = Comment::getEntityComments($oGroupFeed->id_post, 'P');
            $oGroupFeeds[$key]->comments = $oComments->all();
        }
        if($oGroupFeeds->currentPage() > 1)
            return \View::make('WebView::group._more_group_feeds', compact('oGroupFeeds','oGroupMemberDetail','oGroupDetails'));
        
        return \View::make('WebView::group.group_feeds', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds','oGroupListTab','oGroupRequestJoin'));
    }
    
    public function callGroupFeedsAjax($nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oGroupFeeds = GroupPost::getGroupPost($nIdGroup);
        
        $oGroupListTab    =  GroupMember::getUserGroupsTab(Auth::user()->id_user);
        
        foreach ($oGroupFeeds as $key=>$oGroupFeed)
        {
            $oComments = Comment::getEntityComments($oGroupFeed->id_post, 'P');
            $oGroupFeeds[$key]->comments = $oComments->all();
        }
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_feeds_ajax', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds', 'oGroupListTab'))->render();
    
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    public function callGroupMembers($nIdGroup,$sSeacrhStr ='')
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupRequest=array();
        if($oGroupDetails->group_type==config('constants.PRIVATEGROUP') && Auth()->user()->id_user==$oGroupDetails->id_user){
            $oGroupRequest = GroupRequest::getJoinGroupRequests($nIdGroup); 
        }
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $oGroupMemberList = $oGroupMember->getGroupMembers($nIdGroup,array(),'',$sSeacrhStr);
        foreach($oGroupMemberList as $key=>$oMember)
        {
            $oGroupMemberList[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
        }
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        if($oGroupMemberList->currentPage() > 1 )
            return \View::make('WebView::group._more_group_members', compact('oGroupMemberList','oGroupMemberDetail'));
        
        return \View::make('WebView::group.group_members', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupMemberList','oGroupRequest','oGroupRequestJoin','sSeacrhStr'));
    }

    public function callGroupMembersAjax($nIdGroup,$sSeacrhStr ='')
    {
        //echo $sSeacrhStr;exit;
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequest=array();
        if($oGroupDetails->group_type==config('constants.PRIVATEGROUP') && Auth()->user()->id_user==$oGroupDetails->id_user){
            $oGroupRequest = GroupRequest::getJoinGroupRequests($nIdGroup); 
        }
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        //echo $oGroupMemberList = $oGroupMember->getGroupMembers($nIdGroup,'','',$sSeacrhStr);exit;
        $oGroupMemberList = $oGroupMember->getGroupMembers($nIdGroup,array(),'',$sSeacrhStr);
        //print_r($oGroupMemberList);exit;
        foreach($oGroupMemberList as $key=>$oMember)
        {
            $oGroupMemberList[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
        }
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        $html['right_sidebar'] = '';
        $nCurrentPage = $oGroupMemberList->currentPage();
        $nLastPage = $oGroupMemberList->lastPage();
        if($oGroupMemberList->currentPage() == 1 && $sSeacrhStr != '')
        $html['content'] = \View::make('WebView::group._more_group_members', compact('oGroupMemberDetail', 'oGroupMemberList'))->render();
        else
        $html['content'] = \View::make('WebView::group._group_members_ajax', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupMemberList','oGroupRequest','sSeacrhStr'))->render();
    
        return response()->json( array('success' => true, 'html'=>$html,'nLastPage' => $nLastPage, 'nCurrentPage'=>$nCurrentPage) );
    }
    
    public function callGroupMembersExcel($nIdGroup)
    {
        $oGroupDetails = Group::find($nIdGroup);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        $oGroupMemberList = GroupMember::getGroupMembers($nIdGroup,array(),'','',1);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $aMemberHeading = array("First Name","Last Name","NetID","Email"); 
            $aMembers = array();
            foreach($oGroupMemberList as $oGroupMember)
            {
                $aMember = array();
                array_push($aMember, $oGroupMember->first_name);
                array_push($aMember, $oGroupMember->last_name);
                $aEmail = explode('@', $oGroupMember->email);
                array_push( $aMember, $aEmail[0]);
                array_push($aMember, $oGroupMember->email);
                
                array_push($aMembers,$aMember);
            }
            $sFileName = trim($oGroupDetails->group_name);
            if($oGroupDetails->section != '')
                $sFileName .= ' '.trans('messages.section').'-'.trim($oGroupDetails->section).' ('.trim($oGroupDetails->semester).')';
            Excel::create($sFileName, function($excel) use($aMemberHeading, $aMembers) {
                                // Set the title
                                $excel->setTitle('Member Data');
                                // Chain the setters
                                $excel->setCreator('Campusknot')
                                      ->setCompany('Campusknot.com');
                                
                                $excel->sheet('Attendance', function($sheet) use($aMemberHeading, $aMembers) {
                                                    
                                                    $sheet->appendRow($aMemberHeading);
                                                    $sheet->row($sheet->getHighestRow(), function ($row) {
                                                                    $row->setFontFamily('Arial');
                                                                    $row->setFontSize(12);
                                                                    $row->setFontWeight('bold');
                                                                });
                                                    
                                                    $sheet->rows($aMembers); 
                                                });
                            })->download('xls');
        }
        
    }
    
    /*
    public function callGroupMembersSearch($nIdGroup,$sSeacrhStr ='')
    {
        //echo $sSeacrhStr;exit;
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequest=array();
        if($oGroupDetails->group_type==config('constants.PRIVATEGROUP') && Auth()->user()->id_user==$oGroupDetails->id_user){
            $oGroupRequest = GroupRequest::getJoinGroupRequests($nIdGroup); 
        }
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        //echo $oGroupMemberList = $oGroupMember->getGroupMembers($nIdGroup,'','',$sSeacrhStr);exit;
        $oGroupMemberList = $oGroupMember->getGroupMembers($nIdGroup,array(),'',$sSeacrhStr);
        //print_r($oGroupMemberList);exit;
        foreach($oGroupMemberList as $key=>$oMember)
        {
            $oGroupMemberList[$key]->major=UserEducation::getMajorEducation($oMember->id_user);
        }
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._more_group_members', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupMemberList','oGroupRequest','sSeacrhStr'))->render();
    
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    */
    public function callGroupPhotos($nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oGroupFeeds = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEIMAGE'));
        
        foreach ($oGroupFeeds as $nKey=>$oGroupFeed)
        {
            $oComments = Comment::getEntityComments($oGroupFeed->id_post, 'P');
            $oGroupFeeds[$nKey]->comments = $oComments->all();
        }
        if($oGroupFeeds->currentPage() > 1)
            return \View::make('WebView::group._more_group_photos', compact('oGroupFeeds','oGroupMemberDetail','oGroupDetails'));
        
        return \View::make('WebView::group.group_photos', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds','oGroupRequestJoin'));
    }

    public function callGroupPhotosAjax($nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oGroupFeeds = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEIMAGE'));
        
        foreach ($oGroupFeeds as $nKey=>$oGroupFeed)
        {
            $oComments = Comment::getEntityComments($oGroupFeed->id_post, 'P');
            $oGroupFeeds[$nKey]->comments = $oComments->all();
        }
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_photos_ajax', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds'))->render();
    
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    public function callGroupDocuments(Request $oRequest, $nIdGroup,$nIdDocument =null)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);       
        
        $nIdParent=$nIdDocument;
        $sCurrentPer = (count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL) ? config('constants.DOCUMENTPERMISSIONTYPEWRITE') : config('constants.DOCUMENTPERMISSIONTYPEREAD');
        
        $aBreadCrumb = array();
        if($nIdDocument != '')
        {
            $oDocument = Document::find($nIdDocument);
            $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);
            
            $nCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
            if($nCurrentPer > 0 )
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            }
            else
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');
            }
        }
        session(['current_permission' => $sCurrentPer ]);
        $sDocumentListing = "GroupDocumentList";
        if($nIdDocument == '')
            $oGroupDocument = GroupDocument::getDocumentGroupShared($nIdGroup,$sOrderBy,$sOrderField,$sSearchStr);
        else
            $oGroupDocument = Document::getChildDocument($nIdDocument,$sOrderBy,$sOrderField,$sSearchStr);   
        
        $nLimit = config('constants.PERPAGERECORDS');
        $nPage = $oRequest->has('page') ? $oRequest->page : 1;
        $nTotal = $oGroupDocument->count();
        $oGroupDocument = $oGroupDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

        $oGroupDocument = new \Illuminate\Pagination\LengthAwarePaginator($oGroupDocument, $nTotal, $nLimit, $nPage, [
                                                                        'path' => $oRequest->url(),
                                                                        'pageName' => 'page',
                                                                    ]);
        
        if($oGroupDocument->currentPage() > 1)
            return \View::make('WebView::group._more_group_documents', compact('oGroupDocument','oGroupDetails'));
        
        return \View::make('WebView::group.group_documents', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupDocument','oGroupRequestJoin', 'aBreadCrumb','nIdParent','sDocumentListing','sOrderBy','sOrderField','sSearchStr'));
    }
    
    //make for side bar ajax call
    public function callGroupDocumentsSidebarAjax(Request $oRequest, $nIdGroup, $nIdDocument =null)
    {
        //echo $oRequest->order_by;exit;
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts =  $this->getGroupSpecificCounts($nIdGroup);
        
        Session::forget('current_permission');
        
        $nIdParent=$nIdDocument;
        $sCurrentPer = (count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL) ? config('constants.DOCUMENTPERMISSIONTYPEWRITE') : config('constants.DOCUMENTPERMISSIONTYPEREAD');
        $aBreadCrumb = array();
        if($nIdDocument != '')
        {
            $oDocument = Document::find($nIdDocument);
            $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);
            $nCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
            if($nCurrentPer > 0 )
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            }
            else
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');
            }
        }
        session(['current_permission' => $sCurrentPer ]);
        $oGroupListTab = GroupMember::getUserGroupsTab( Auth::user()->id_user );
        $sDocumentListing = "GroupDocumentList";
        if($nIdDocument == '')
            $oGroupDocument = GroupDocument::getDocumentGroupShared($nIdGroup,$sOrderBy,$sOrderField,$sSearchStr);
        else
            $oGroupDocument = Document::getChildDocument($nIdDocument,$sOrderBy,$sOrderField,$sSearchStr);                
        
        $nLimit = config('constants.PERPAGERECORDS');
        $nPage = $oRequest->has('page') ? $oRequest->page : 1;
        $nTotal = $oGroupDocument->count();
        $oGroupDocument = $oGroupDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);


        $oGroupDocument = new \Illuminate\Pagination\LengthAwarePaginator($oGroupDocument, $nTotal, $nLimit, $nPage, [
                                                                        'path' => $oRequest->url(),
                                                                        'pageName' => 'page',
                                                                    ]);
        if($oGroupDocument->currentPage() > 1)
        {
            return \View::make('WebView::documents._more_group_document', compact('oGroupDocument','oGroup','nIdParent', 'nIdDocument','oGroupDetails'));
        }
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_documents_ajax',compact('oGroupDocument', 'aBreadCrumb', 'nIdParent','oGroupDetails','oGroupMemberDetail','sDocumentListing','sOrderBy','sOrderField','sSearchStr'))->render();
    
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    //made for parent to child and child to parent or breadcrumb ajax call
    public function callGroupDocumentsAjax(Request $oRequest, $nIdGroup, $nIdDocument =null)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        Session::forget('current_permission');
        
        $nIdParent=$nIdDocument;
        $sCurrentPer = (count($oGroupMemberDetail) && $oGroupMemberDetail->member_type != NULL) ? config('constants.DOCUMENTPERMISSIONTYPEWRITE') : config('constants.DOCUMENTPERMISSIONTYPEREAD');
        $aBreadCrumb = array();
        if($nIdDocument != '')
        {
            $oDocument = Document::find($nIdDocument);
            $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);
            $nCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
            if($nCurrentPer > 0 )
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            }
            else
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');
            }
        }
        session(['current_permission' => $sCurrentPer ]);
        $oGroupListTab = GroupMember::getUserGroupsTab( Auth::user()->id_user );
        $sDocumentListing = "GroupDocumentList";
        if($nIdDocument == '')
            $oGroupDocument = GroupDocument::getDocumentGroupShared($nIdGroup,$sOrderBy,$sOrderField,$sSearchStr);
        else
            $oGroupDocument = Document::getChildDocument($nIdDocument,$sOrderBy,$sOrderField,$sSearchStr);                
        
        $nLimit = config('constants.PERPAGERECORDS');
        $nPage = $oRequest->has('page') ? $oRequest->page : 1;
        $nTotal = $oGroupDocument->count();
        $oGroupDocument = $oGroupDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);


        $oGroupDocument = new \Illuminate\Pagination\LengthAwarePaginator($oGroupDocument, $nTotal, $nLimit, $nPage, [
                                                                        'path' => $oRequest->url(),
                                                                        'pageName' => 'page',
                                                                    ]);
        if($oGroupDocument->currentPage() > 1)
        {
            return \View::make('WebView::documents._more_group_document', compact('oGroupDocument','oGroup','nIdParent', 'nIdDocument','oGroupDetails'));  
        }
        $html['CreateFolderHtml'] = '';
            if(Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD'))
            {
                $html['CreateFolderHtml'] = view('WebView::documents._create_folder_tab')->render();
            }
            $html['DocumentList'] = view('WebView::group._group_document',compact('oGroupDocument', 'aBreadCrumb', 'nIdParent','oGroupDetails','oGroupMemberDetail','sOrderBy','sOrderField','sSearchStr'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    //made for post document page refresh
    public function callGroupPostDocument(Request $oRequest,$nIdGroup)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        $aBreadCrumb = array();
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        session(['current_permission' => config('constants.DOCUMENTPERMISSIONTYPEREAD') ]);
        $oPostDocument = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        $sDocumentListing = "GroupPostDocumentList";
        if($oPostDocument->currentPage() > 1)
            return \View::make('WebView::documents._more_post_document', compact('oGroupDocument'));
        
        return \View::make('WebView::group.group_documents', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oPostDocument', 'oGroupRequestJoin', 'sDocumentListing','sOrderBy','sOrderField','sSearchStr'));
    }
    
    //call when get post data from ajax call
    public function callGroupPostDocumentAjax(Request $oRequest,$nIdGroup)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        $aBreadCrumb = array();
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oPostDocument = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        $sDocumentListing = "GroupPostDocumentList";
        if($oPostDocument->currentPage() > 1)
            return \View::make('WebView::documents._more_post_document', compact('oGroupDocument'));
        
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_documents_ajax', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 
                                        'oPostDocument', 'oGroupRequestJoin', 'sDocumentListing','sOrderBy','sOrderField','sSearchStr'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    //call when get post data from ajax call
    public function callGroupPostChildDocumentAjax(Request $oRequest,$nIdGroup)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        $aBreadCrumb = array();
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oPostDocument = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        $sDocumentListing = "GroupPostDocumentList";
        if($oPostDocument->currentPage() > 1)
            return \View::make('WebView::documents._more_post_document', compact('oGroupDocument'));
        
        $html['CreateFolderHtml'] = '';
        $html['DocumentList'] = view('WebView::group._post_document', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oPostDocument', 'oGroupRequestJoin', 'sDocumentListing','aBreadCrumb','sOrderBy','sOrderField','sSearchStr'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }

    public function callGroupEvents($nIdGroup,$sSeacrhStr ='')
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oEventTabs =  GroupEvent::getGroupEvents($nIdGroup,'',$sSeacrhStr); // to show events within group  in json
        
        if($oEventTabs->currentPage() > 1)
            return \View::make('WebView::group._more_group_events', compact('oEventTabs'));

        return \View::make('WebView::group.group_events', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oEventTabs', 'oGroupRequestJoin','sSeacrhStr'));
    }
    
    public function callGroupEventsAjax($nIdGroup,$sSeacrhStr ='')
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $oEventTabs =  GroupEvent::getGroupEvents($nIdGroup,'',$sSeacrhStr);
        //echo count($oEventTabs);exit;
        if((count($oGroupDetails) && ($oGroupDetails->group_type != config('constants.PUBLICGROUP'))) && (!count($oGroupMemberDetail) || $oGroupMemberDetail->member_type == NULL))
            $html['right_sidebar'] = '';
        else
            $html['right_sidebar'] = \View::make('WebView::group._group_event_sidebar')->render();
        
        $nCurrentPage = $oEventTabs->currentPage();
        $nLastPage = $oEventTabs->lastPage();
        if($oEventTabs->currentPage() == 1 && $sSeacrhStr !='')
            $html['content'] = \View::make('WebView::group._more_group_events', compact('oEventTabs'))->render();
        else
            $html['content'] = \View::make('WebView::group._group_events_ajax', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oEventTabs','sSeacrhStr'))->render();
        
        return response()->json( array('success' => true, 'html'=>$html,'nCurrentPage'=>$nCurrentPage,'nLastPage'=>$nLastPage) );
    }
    
    public function callGroupPoll(Request $oRequest, $nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupRequestJoin='';
        if ($oGroupDetails->member == '')
        {    
            $oGroupRequestJoin= GroupRequest::getLoginUserRequest($nIdGroup);
        }
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $aUncomplitedPolls = array();
        $aComplitedPolls = array();
        $dToday = Carbon::now();
        $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
        $nLimit = config('constants.PERPAGERECORDS');
        
        $nUncomplitedPollCount = Poll::getGroupUncomplitedPollCount($nIdGroup, $dToday);
        $nComplitedPollCount = Poll::getGroupComplitedPollCount($nIdGroup, $dToday);
        
        if ($nUncomplitedPollCount > (($nPageNumber - 1) * $nLimit)) {
            //Get max uncomplited polls
            $oUncomplitedPolls = Poll::getGroupUncomplitedPolls($nIdGroup, $dToday, ($nPageNumber - 1)*$nLimit, $nLimit);
            $aUncomplitedPolls = $oUncomplitedPolls->all();
        }

        //If group requests are less then 10 fetch event requests
        if (($nUncomplitedPollCount + $nComplitedPollCount) > (($nPageNumber - 1) * $nLimit)) {
            if (count($aUncomplitedPolls) < $nLimit) {
                $nSkipRecords = (($nPageNumber - 1) * $nLimit) - $nUncomplitedPollCount;
                $nSkipRecords = max(0, $nSkipRecords);

                $nLimit = config('constants.PERPAGERECORDS') - count($aUncomplitedPolls);
                $oComplitedPolls = Poll::getGroupAllPolls($nIdGroup, $dToday, $nSkipRecords, $nLimit);
                $aComplitedPolls = $oComplitedPolls->all();
            }
        }
        
        $oGroupFeeds = array_merge($aUncomplitedPolls, $aComplitedPolls);
        
        foreach ($oGroupFeeds as $key=>$oGroupFeed)
        {
            $oPollOption = PollOption::getPollOptions($oGroupFeed->id_poll);
            $oGroupFeeds[$key]->polloption=$oPollOption;
            if($oGroupFeed->id_poll_answer !='' || strtotime($oGroupFeed->end_time) <= time()){
                $nTotalAnswer= PollAnswer::where('id_poll','=',$oGroupFeed->id_poll)->count();
                $oPollAnswer = PollAnswer::getPollAnswer($oGroupFeed->id_poll);
                $oGroupFeeds[$key]['totalAns']=$nTotalAnswer;
                foreach($oPollAnswer as $answer){
                    $percent=0;
                    $percent=($answer->total * 100) / $nTotalAnswer;
                    
                    $oGroupFeeds[$key]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                }
            }
        }
        
        $nTotalRecords = $nUncomplitedPollCount + $nComplitedPollCount;
        $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

        $aPaginator['total'] = $nTotalRecords;
        $aPaginator['per_page'] = config('constants.PERPAGERECORDS');
        $aPaginator['current_page'] = $nPageNumber;
        $aPaginator['last_page'] = $nLastPage;
        $aPaginator['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
        $aPaginator['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
        $aPaginator['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
        $aPaginator['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
        
        if($nPageNumber > 1)
            return \View::make('WebView::group._more_group_poll', compact('oGroupFeeds'));
        
        return \View::make('WebView::group.group_poll', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds','oGroupRequestJoin', 'aPaginator'));
    }
    
    public function callGroupPollAjax(Request $oRequest, $nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $aUncomplitedPolls = array();
        $aComplitedPolls = array();
        $dToday = Carbon::now();
        $nPageNumber = isset($oRequest->page) ? $oRequest->page : 1;
        $nLimit = config('constants.PERPAGERECORDS');
        
        $nUncomplitedPollCount = Poll::getGroupUncomplitedPollCount($nIdGroup, $dToday);
        $nComplitedPollCount = Poll::getGroupComplitedPollCount($nIdGroup, $dToday);
        
        if ($nUncomplitedPollCount > (($nPageNumber - 1) * $nLimit)) {
            //Get max uncomplited polls
            $oUncomplitedPolls = Poll::getGroupUncomplitedPolls($nIdGroup, $dToday, ($nPageNumber - 1)*$nLimit, $nLimit);
            $aUncomplitedPolls = $oUncomplitedPolls->all();
        }

        //If group requests are less then 10 fetch event requests
        if (($nUncomplitedPollCount + $nComplitedPollCount) > (($nPageNumber - 1) * $nLimit)) {
            if (count($aUncomplitedPolls) < $nLimit) {
                $nSkipRecords = (($nPageNumber - 1) * $nLimit) - $nUncomplitedPollCount;
                $nSkipRecords = max(0, $nSkipRecords);

                $nLimit = config('constants.PERPAGERECORDS') - count($aUncomplitedPolls);
                $oComplitedPolls = Poll::getGroupAllPolls($nIdGroup, $dToday, $nSkipRecords, $nLimit);
                $aComplitedPolls = $oComplitedPolls->all();
            }
        }
        
        $oGroupFeeds = array_merge($aUncomplitedPolls, $aComplitedPolls);
        
        foreach ($oGroupFeeds as $key=>$oGroupFeed)
        {
            $oPollOption = PollOption::getPollOptions($oGroupFeed->id_poll);
            $oGroupFeeds[$key]->polloption=$oPollOption;
            if($oGroupFeed->id_poll_answer !='' || strtotime($oGroupFeed->end_time) <= time()){
                $nTotalAnswer= PollAnswer::where('id_poll','=',$oGroupFeed->id_poll)->count();
                $oPollAnswer = PollAnswer::getPollAnswer($oGroupFeed->id_poll);
                $oGroupFeeds[$key]['totalAns']=$nTotalAnswer;
                foreach($oPollAnswer as $answer){
                    $percent=0;
                    $percent=($answer->total * 100) / $nTotalAnswer;
                    
                    $oGroupFeeds[$key]['pollanswer_'.$answer->id_poll_option]=round($percent, 2);
                }
            }
        }
        
        $nTotalRecords = $nUncomplitedPollCount + $nComplitedPollCount;
        $nLastPage = floor($nTotalRecords / config('constants.PERPAGERECORDS')) + (($nTotalRecords % config('constants.PERPAGERECORDS')) ? 1 : 0);

        $aPaginator['total'] = $nTotalRecords;
        $aPaginator['per_page'] = config('constants.PERPAGERECORDS');
        $aPaginator['current_page'] = $nPageNumber;
        $aPaginator['last_page'] = $nLastPage;
        $aPaginator['next_page_url'] = ($nPageNumber < $nLastPage) ? $oRequest->url() . "?page=" . ($nPageNumber + 1) : null;
        $aPaginator['prev_page_url'] = ($nPageNumber > 1) ? $oRequest->url() . "?page=" . ($nPageNumber - 1) : null;
        $aPaginator['from'] = ($nPageNumber < $nLastPage) ? (($nPageNumber - 1) * config('constants.PERPAGERECORDS')) + 1 : null;
        $aPaginator['to'] = ($nPageNumber < $nLastPage) ? min($nPageNumber * config('constants.PERPAGERECORDS'), $nTotalRecords) : null;
        
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_poll_ajax', compact('oGroupDetails', 'aGroupSpecificCounts', 'oGroupMemberDetail', 'oGroupFeeds', 'aPaginator'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }

    public function callInviteUserAjax($nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup);
        
        $oGroupMember = new GroupMember();
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        if($oGroupDetails->member == '' || ($oGroupMemberDetail->member_type != config('constants.GROUPADMIN') && $oGroupMemberDetail->member_type != config('constants.GROUPCREATOR')) &&
                ($oGroupDetails->id_group_category != config('constants.UNIVERSITYGROUPCATEGORY')) && ($oGroupDetails->id_group_category != config('constants.COURSEGROUPCATEGORY')))
            return redirect(route("group.group-feeds",['nIdGroup'=>$nIdGroup])); 
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_invite_member_ajax', compact('nIdGroup', 'oGroupDetails', 'oGroupMemberDetail', 'aGroupSpecificCounts'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    public function callJoinGroup(Request $oRequest, $nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetail = $oGroup->getGroupDetail($nIdGroup);
        
        if($oGroupDetail)
        {
            if($oGroupDetail->group_type == config('constants.SECRETGROUP'))
                return redirect(route('group.group-listing', ['nIdGroup' => $nIdGroup]))
                        ->withErrors(['group_error' => trans('messages.not_invited_to_join')]);
            if($oGroupDetail->group_type == config('constants.PUBLICGROUP'))
            {
                $oGroupMember = GroupMember::firstOrCreate(['id_group' => $nIdGroup,'id_user'=>Auth::user()->id_user]);
                $oGroupMember->id_group = $nIdGroup;
                $oGroupMember->id_user = Auth::user()->id_user;
                $oGroupMember->member_type = config('constants.GROUPMEMBER');
                $oGroupMember->activated = 1;
                $oGroupMember->deleted = 0;
                $oGroupMember->save();
                
                $this->dispatch(new SyncUserGroups($oGroupMember->id_user));
                $this->addAllGroupEventsToNewMember($oGroupMember);
                $this->dispatch(new ShareGroupDocumentsWithNewMember($oGroupMember->id_user, $nIdGroup));
                
                return redirect(route('group.group-feeds', ['nIdGroup' => $nIdGroup]))
                        ->with(['success_message' => trans('messages.new_member_of_group')]);
            }
            else
            {
                $oGroupRequest=GroupRequest::firstorcreate(['id_group' => $nIdGroup,
                                        'id_user_request_from' => Auth::user()->id_user
                                    ]);
                $oGroupRequest->id_user_request_to =$oGroupDetail->id_user ;
                $oGroupRequest->member_status = config('constants.GROUPMEMBERPENDING');
                $oGroupRequest->request_type = 'J';
                $oGroupRequest->save();
                
                $oAdminUser=User::find($oGroupDetail->id_user);
                $oLoggedinUser=Auth::user();
                
                Mail::queue('WebView::emails.join_group_request', ['sName'=>$oAdminUser->first_name.' '.$oAdminUser->last_name, 'nIdGroup'=>$nIdGroup, 'sRequestBy' => $oLoggedinUser->first_name.' '.$oLoggedinUser->last_name, 'sGroupName' => $oGroupDetail->group_name], function ($oMessage) use ($oLoggedinUser, $oAdminUser) {
                    $oMessage->from(config('mail.from.address'), $oLoggedinUser->first_name.' '.$oLoggedinUser->last_name);

                    $oMessage->to($oAdminUser->email, $oAdminUser->first_name.' '.$oAdminUser->last_name)
                            ->subject(trans('messages.group_join_request_subject'));
                });
                return redirect(route('group.group-feeds', ['nIdGroup' => $nIdGroup]))
                        ->with(['success_message' => trans('messages.request_registored')]);
            }
        }
        return redirect(route('group.group-feeds', ['nIdGroup' => $nIdGroup]))
                        ->withErrors(['group_error' => trans('messages.no_record_found')]);
    }
    
    private function addAllGroupEventsToNewMember($oGroupMember)
    {
        //Get all events of group.
        $aGroupEvents = GroupEvent::getAllGroupEvents($oGroupMember->id_group);
        
        //Add event request for new group member.
        foreach ($aGroupEvents as $oGroupEvent)
        {
            $oEventRequest = EventRequest::firstOrNew([
                                                        'id_event' => $oGroupEvent->id_event,
                                                        'id_user_request_to' => $oGroupMember->id_user
                                                    ]);
            $oEventRequest->id_user_request_from = $oGroupEvent->id_user;
            $oEventRequest->status = NULL;
            $oEventRequest->save();
        }
    }

    public function callLeaveGroup(Request $oRequest, $nIdGroup)
    {
        $oGroupMember = new GroupMember();
        $nGroupMemberCount = $oGroupMember->getMembersCount($nIdGroup);
        
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        //Check if group have other members.
        if($nGroupMemberCount > 1)
        {
            //Check if user is creator
            if($oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
            {
                //Redirect to pass creator rights page.
                $oGroupMembers = $oGroupMember->getGroupMembers($nIdGroup);
                
                return \View::make('WebView::group._creator_leave_group', compact('oGroupMemberDetail', 'oGroupMembers'));
            }
            else {
                $this->removeMemberFromGroup($oGroupMemberDetail);
                $oRequest->session()->flash('success_message', trans('messages.leave_group_success'));
                $this->dispatch(new RevokeGroupDocPermission(Auth::user()->id_user, $nIdGroup));
                $this->removeUpcomingGroupQuiz(Auth::user()->id_user, $nIdGroup);
                
                return 'redirect:to:'.url('group/group-feeds/'. $nIdGroup);
            }
        }
        else {
            // Remove user from group_member
            $this->removeMemberFromGroup($oGroupMemberDetail);
            $oRequest->session()->flash('success_message', trans('messages.leave_group_success'));
            
            //Delete all pending requests
            $oGroupRequests = GroupRequest::where('id_group', $nIdGroup)
                                            ->where('member_status', config('constants.GROUPMEMBERPENDING'));
            foreach($oGroupRequests as $oGroupRequest)
            {
                $oGroupRequest->delete();
            }
            
            //Delete group as there is only one members
            $oGroup = Group::find($nIdGroup);
            
            $oGroup->activated = 0;
            $oGroup->deleted = 1;
            $oGroup->update();
            $this->dispatch(new RevokeGroupDocPermission(Auth::user()->id_user, $nIdGroup));
            $this->removeUpcomingGroupQuiz(Auth::user()->id_user, $nIdGroup);
        }
        return 'redirect:to:'.url('/');
    }
    
    private function removeUpcomingGroupQuiz($nIdUser, $nIdGroup)
    {
        //get all upcoming quiz for this group
        $dTime = Carbon::Now();
        $aWhereData = [
                            ['qgi.end_time', '>', $dTime]
                        ];
        $oQuizList = Quiz::getUpcomingGroupQuiz($aWhereData, $nIdGroup);
        
        //Add event request for new group member.
        foreach ($oQuizList as $oQuiz)
        {
            Notification::where(['id_entites' => Auth::user()->id_user.'_'.$oQuiz->id_quiz.'_'.$nIdGroup,
                                  'entity_type_pattern' => 'U_Q_G',
                                    'id_user' => $nIdUser
                                ])->delete();
            QuizUserInvite::where(['id_quiz'=> $oQuiz->id_quiz,'id_user' => $nIdUser])->update(['activated'=>0,'deleted'=>1]);
        }
    }
    
    public function callInviteUserAgain(Request $oRequest, $sGroupRequestData)
    {
        $aIdGroupRequestData = explode('-', $sGroupRequestData);
        
        if(count($aIdGroupRequestData) == 1)
        {
            $oGroupRequest = GroupRequest::find($aIdGroupRequestData[0]);
            $oGroupRequest->member_status = config('constants.GROUPMEMBERPENDING');
            $oGroupRequest->save();
            
            $oUserDetail = User::find($oGroupRequest->id_user_request_to);
            $sFirstName = $oUserDetail->first_name;
            $sEmail = $oUserDetail->email;
            
            $nIdGroup = $oGroupRequest->id_group;
        }
        else
        {
            $oNonMember = NonMemberInvitation::find($aIdGroupRequestData[0]);
            $nIdGroup = $oNonMember->id_entity_invite_for;
            $sEmail = $oNonMember->invite_to;
            $sFirstName = $sEmail;
        }
        $oLogedinUser = Auth()->user();
        $oGroup = Group::find($nIdGroup);

        Mail::queue('WebView::emails.group_invitation',
                                            ['sFirstName'=>$sFirstName,
                                                'sInvitedBy' => $oLogedinUser->first_name.' '.$oLogedinUser->last_name,
                                                'nIdGroup'=>$oGroup->id_group,
                                                'group_name'=>$oGroup->group_name],
                                                function ($oMessage) use ($oLogedinUser,$sEmail) {
                                                        $oMessage->from(config('mail.from.address'),
                                                            $oLogedinUser->first_name.' '.$oLogedinUser->last_name);
                                                        $oMessage->to($sEmail)->subject(trans('messages.group_invitation_subject'));
                            });
                        
        $oRequest->session()->flash('success_message', trans('messages.invite_again_success', ['name' => $sFirstName]));
        return redirect()->back();
    }

    public function callRemoveMember(Request $oRequest, $nIdGroup, $nIdUser)
    {
        if($nIdUser != Auth::user()->id_user)
        {
            $oGroupMember = new GroupMember();
            $oLoggedinUser = $oGroupMember->getGroupMemberDetail($nIdGroup, Auth::user()->id_user);
            if($oLoggedinUser->member_type == config('constants.GROUPCREATOR'))
            {
                $oGroupMemberDetail = GroupMember::getGroupMemberDetail($nIdGroup, $nIdUser);
                
                $oGroupMember = GroupMember::where(['id_user' => $nIdUser,'id_group'=>$nIdGroup])->first();
                $oGroupMember->member_type = config('constants.GROUPMEMBER');
                $oGroupMember->activated = 0;
                $oGroupMember->deleted = 0;
                $oGroupMember->update();

                //print_r($oGroupMemberDetail);exit;
                $this->dispatch(new SyncUserGroups($oGroupMemberDetail->id_user));
                
                Mail::queue('WebView::emails.remove_group_member', ['sFirstName'=>$oGroupMemberDetail->first_name.' '.$oGroupMemberDetail->last_name, 'nIdGroup'=>$nIdGroup, 'sRemovedBy' => $oLoggedinUser->first_name.' '.$oLoggedinUser->last_name, 'sGroupName' => $oGroupMemberDetail->group_name], function ($oMessage) use ($oLoggedinUser, $oGroupMemberDetail) {
                    $oMessage->from(config('mail.from.address'), $oLoggedinUser->first_name.' '.$oLoggedinUser->last_name);

                    $oMessage->to($oGroupMemberDetail->email, $oGroupMemberDetail->first_name.' '.$oGroupMemberDetail->last_name)
                            ->subject(trans('messages.remove_group_member_subject'));
                });
                $oRequest->session()->flash('success_message', trans('messages.member_removed_success'));
                return 'redirect:to:'.url('group/group-member/'. $nIdGroup);
            }
        }
        $oRequest->session()->flash('error_message', trans('messages.not_allowed_to_member_removed'));
        return 'redirect:to:'.url('group/group-member/'. $nIdGroup);
    }

    private function removeMemberFromGroup($oGroupMemberDetail)
    {
        $oGroupMemberDetail->member_type = config('constants.GROUPMEMBER');
        $oGroupMemberDetail->activated = 0;
        $oGroupMemberDetail->deleted = 1;
        $oGroupMemberDetail->update();
        
        $this->dispatch(new SyncUserGroups($oGroupMemberDetail->id_user));
        return TRUE;
    }

    public function callPassCreatorRights(Request $oRequest)
    {
        $oGroupMember = new GroupMember();
        $oGroupMembers = $oGroupMember->getGroupMembers($oRequest->id_group);
        $oGroupMemberDetail = $oGroupMember->getGroupMemberDetail($oRequest->id_group, $oRequest->id_creator);
        
        if($oRequest->isMethod('POST'))
        {    
            $aValidationRequireFor = [
                                        'leave_option' => 'required'
                                      ];
            if($oRequest->leave_option == 'CM')
                $aValidationRequireFor['existing_member'] =  'required';
            else
                $oRequest->offsetSet('existing_member', $oGroupMembers[0]->id_group_member);
            
             $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor);
            if($oValidator->fails())
            {
                $oGroupMembers = $oGroupMember->getGroupMembers($oRequest->id_group);
                
                return \View::make('WebView::group._creator_leave_group', compact('oGroupMemberDetail', 'oGroupMembers'))
                                ->withErrors($oValidator);
            }
            
            $oExistingMemberDetail = $oGroupMember->find($oRequest->existing_member);
            $oExistingMemberDetail->member_type = config('constants.GROUPCREATOR');
            $nIdCreater=$oExistingMemberDetail->id_user;
            $oExistingMemberDetail->update();

            $this->removeMemberFromGroup($oGroupMemberDetail);
            
            $oGroupDetail = Group::find($oRequest->id_group);
            $nOldCreatorId=$oGroupDetail->id_user;
            $oGroupDetail->id_user = $nIdCreater;
            $oGroupDetail->update();
            $oNewCreator = User::find($nIdCreater);
            $sGroupCreaterName=$oGroupMemberDetail->first_name.' '.$oGroupMemberDetail->last_name;
            
            $oNotification= Notification::create([
                 'id_user'=>$nIdCreater,
                 'id_entites'=>$nOldCreatorId.'_'.$oRequest->id_group,
                 'entity_type_pattern'=>'U_G',
                 'notification_type'=>config('constants.NOTIFICATIONNEWADMIN')
             ]);
           
            /* Group Request update to new creator*/
            GroupRequest::where('id_group',$oRequest->id_group)
                        ->where('request_type',config('constants.GROUPREQUESTTYPEINVITE'))
                        ->update(['id_user_request_from'=>$nIdCreater]);
            GroupRequest::where('id_group',$oRequest->id_group)
                        ->where('request_type',config('constants.GROUPREQUESTTYPEJOIN'))
                        ->update(['id_user_request_to'=>$nIdCreater]);
            $nIdGroup = $oRequest->id_group;
            Mail::queue('WebView::emails.new_group_creator', ['sFirstName'=>$oNewCreator->first_name.' '.$oNewCreator->last_name, 'nIdGroup'=>$nIdGroup, 'sGroupName'=>$oGroupDetail->group_name,'sGroupCreaterName'=>$sGroupCreaterName], function ($oMessage) use ($oGroupMemberDetail, $oNewCreator) {
                $oMessage->from(config('mail.from.address'), $oGroupMemberDetail->first_name.' '.$oGroupMemberDetail->last_name);

                $oMessage->to($oNewCreator->email, $oNewCreator->first_name.' '.$oNewCreator->last_name)
                        ->subject(trans('messages.verification_mail_subject'));
            });
            
            return 'redirect:to:'.url('group/group-feeds/'. $oRequest->id_group);
        }
        
        return \View::make('WebView::group._creator_leave_group', compact('oGroupMemberDetail', 'oGroupMembers'));
    }
    
    //group document database update..
    public function callDatabaseUpdate()
    {
        //echo "asdas";
        $oGroupDocuments = GroupDocument::where(['activated' => 1,'deleted' => 0])
                                        ->select('id_group','id_document','group_document_permission')
                                        ->get();
        foreach($oGroupDocuments as $oGroupDocument)
        {
            $oGroupMember = new GroupMember();
            $oGroupMembers = $oGroupMember->getAllGroupMembers($oGroupDocument->id_group);
            foreach($oGroupMembers as $oGroupMember)
            {
                $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                                    'id_document' => $oGroupDocument->id_document,
                                                    'id_user' => $oGroupMember->id_user,
                                                    'id_group' => $oGroupDocument->id_group
                                                    ]);
                $oDocumentPermisssion->permission_type = $oGroupDocument->group_document_permission;
                $oDocumentPermisssion->activated = 1;
                $oDocumentPermisssion->deleted = 0;
                $oDocumentPermisssion->save();
                
            }
        }
        
    }

    /////////// Group Attendance
    public function callAdminGroupSuggetion(Request $oRequest)
    {
        $sGroupName = isset($oRequest->group_name) ? $oRequest->group_name : '';
        $sSearchFor = isset($oRequest->search_for) ? $oRequest->search_for : '';
        $nGroupCat = isset($oRequest->group_cat) ? $oRequest->group_cat : '';
         
        $oGroupList = Group::getAttendanceGroupSuggestion($sGroupName,Auth::user()->id_user,$sSearchFor,$nGroupCat);
        foreach($oGroupList as $key=>$oGroup)
        {
            if($oGroup->section != NULL)
               $oGroupList[$key]->group_name = $oGroup->group_name.' '.trans('messages.section').'-'.$oGroup->section.'('.$oGroup->semester.')';
            $oGroupList[$key]->group_img = setGroupImage($oGroup->file_name,$oGroup->group_category_image);
        }
        return Response::json($oGroupList);
        
    }
    
    //add attendance for attendance taken by admin
    public function callAddAttendance(Request $oRequest)
    {
        if($oRequest->end != 1)
        {
            $aValidationRequireFor = [
                                        'group_name' => 'required',
                                        'semester' => 'required'
                                    ];
            $this->validate($oRequest, $aValidationRequireFor);
            $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oRequest->id_group, Auth()->user()->id_user);
            
            if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
            { 
                if(isset($oRequest->course_attendance) && $oRequest->course_attendance == 1)
                    $sSemester = $oRequest->semester;
                else
                    $sSemester = $oRequest->semester.'-'.Carbon::now()->year;
                
                $oRequest->offsetSet('semester', $sSemester );
                //$nTodayLecture = GroupLecture::CheckTodayLectureCount($oRequest); //$nTodayLecture <= 0 ||
//                if($nTodayLecture == 0 || !empty($oRequest->id_lecture))
//                {
                    $oGroupLecture= GroupLecture::firstOrNew(['id_group_lecture' => $oRequest->id_lecture]);
                    $oGroupLecture->id_group = $oRequest->id_group;
                    $oGroupLecture->id_user = Auth::user()->id_user;
                    $oGroupLecture->semester = $sSemester;
                    $oGroupLecture->save();

                    $sRandomString = mb_substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 1) . mb_substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 5);


                    $oNewAttendance = AttendanceCode::firstOrNew(['id_lecture' => $oGroupLecture->id_group_lecture,
                                                                  'entity_type' => config('constants.ATTENDANCETYPEGROUP')
                                                                ]);
                    $oNewAttendance->attendance_code = $sRandomString;
                    $oNewAttendance->save();
                    if(!isset($oRequest->course_attendance) && $oRequest->course_attendance != 1)
                    {
                        $oUserGroupAttendance=UserGroupAttendance::firstOrNew([
                                                                        'id_user'=>Auth::user()->id_user,
                                                                        'id_group_lecture'=>$oGroupLecture->id_group_lecture
                                                                        ]);
                        $oUserGroupAttendance->activated = 1;
                        $oUserGroupAttendance->deleted = 0;
                        $oUserGroupAttendance->save();
                    }
            
                    return Response::json(['success' => true, 'nIdGroupLecture' => $oGroupLecture->id_group_lecture,'random'=>$sRandomString,'actionType'=>'start']);
//                }
//                else
//                    return Response::json(['success' => false,'msg'=>trans('messages.today_attendance_taken') ]);
            }
        }
        else
        {
            AttendanceCode::where('id_lecture', $oRequest->id_lecture)
                          ->where('entity_type',config('constants.ATTENDANCETYPEGROUP'))
                          ->delete();
            $this->dispatch(new AddUserAttendanceForGroup($oRequest->id_lecture));
            
            return Response::json(['success' => true, 'actionType'=>'end']);
        }

    }

    //attendance submit for group all member
    public function callSubmitAttendance(Request $oRequest) 
    {
        $aValidationRequireFor = [
                                    'group_id' => 'required',
                                    'attendance_code' => 'required'
                                ];
        $this->validate($oRequest, $aValidationRequireFor);
        $oAttendance=AttendanceCode::getGroupAttendanceCodeDetail($oRequest->attendance_code);
        if(count($oAttendance) && $oAttendance->id_group == $oRequest->group_id){
            $oUserGroupAttendance=UserGroupAttendance::firstOrNew([
                                                            'id_user'=>Auth::user()->id_user,
                                                            'id_group_lecture'=>$oAttendance->id_lecture
                                                            ]);
            $oUserGroupAttendance->save();
        }
        else
            return Response::json(['success' => false,'msg'=>trans('messages.submit_attendance_error')]);
        
        return Response::json(['success' => true,'msg'=>trans('messages.submit_attendance_success')]);
    }
    
    //get all attendance list for single grp in group module tab
    public function callGetGroupAttendance($nIdGroup)
    {
        $sAttendanceType = 'group';
        $oGroupDetails = Group::getGroupDetail($nIdGroup);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);
        
        $aGroupSpecificCounts = $this->getGroupSpecificCounts($nIdGroup);
        
        $aUserGroups = array($nIdGroup);
        
        $oAllUniqueLecture = GroupLecture::getGroupAttendanceList(Auth::user()->id_user,$aUserGroups);
        foreach($oAllUniqueLecture as $nKey => $oGroupLecture)
        {
            $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
            $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
            $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");

            $oAllUniqueLecture[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
            $oAllUniqueLecture[$nKey]['absent_lectures_count'] = $nAllLecturesCount-$nSickLeaveLecturesCount-$nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['all_lectures_count'] = $nAllLecturesCount;
        }

        if($oAllUniqueLecture->currentPage() > 1)
            return \View::make('WebView::group._group_more_attendance', compact('oAllUniqueLecture'));
        
            return \View::make('WebView::group.group_attendance_tab', compact('oAllUniqueLecture','oGroupDetails','oGroupMemberDetail','aGroupSpecificCounts','sAttendanceType'));
    }
    
    public function callGetGroupAttendanceAjax($nIdGroup)
    {
        $oGroupDetails = Group::getGroupDetail($nIdGroup);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($nIdGroup, Auth()->user()->id_user);

        $aUserGroups = array($nIdGroup);
        
        $oAllUniqueLecture = GroupLecture::getGroupAttendanceList(Auth::user()->id_user,$aUserGroups);
        
        foreach($oAllUniqueLecture as $nKey => $oGroupLecture)
        {
            $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
            $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
            $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");

            $oAllUniqueLecture[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
            $oAllUniqueLecture[$nKey]['absent_lectures_count'] = $nAllLecturesCount-$nSickLeaveLecturesCount-$nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['all_lectures_count'] = $nAllLecturesCount;
        }

        if($oAllUniqueLecture->currentPage() > 1)
            return \View::make('WebView::group._group_more_attendance', compact('oAllUniqueLecture'));
        
        $html['right_sidebar'] = '';
        $html['content'] = \View::make('WebView::group._group_single_attendance_ajax', compact('oAllUniqueLecture','oGroupDetails','oGroupMemberDetail'))->render();
    
        return response()->json( array('success' => true, 'html'=>$html) );
            //return \View::make('WebView::group._group_single_attendance_ajax', compact('oAllUniqueLecture'));
    }
    
    //get all attendance list for all group in attendance module
    public function callGetAttendance()
    {
        $sAttendanceType = 'group';
        $oCourseGroups = GroupMember::getUserSelectiveGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'!=');
        $aUserGroups = explode(",", $oCourseGroups[0]->id_groups);
        
        $oAllUniqueLecture = GroupLecture::getGroupAttendanceList(Auth::user()->id_user,$aUserGroups);
        
        foreach($oAllUniqueLecture as $nKey => $oGroupLecture)
        {
            $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
            $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
            $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");

            $oAllUniqueLecture[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
            $oAllUniqueLecture[$nKey]['absent_lectures_count'] = $nAllLecturesCount-$nSickLeaveLecturesCount-$nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['all_lectures_count'] = $nAllLecturesCount;
        }
        //set cookie for notify new functionality
        $aCustomCookies['atten'] = TRUE;
        Cookie::queue(Cookie::forever('attendance', json_encode($aCustomCookies)));
        session(['current_page' => 'attendance','attendance' => 1]);

        if($oAllUniqueLecture->currentPage() > 1)
            return \View::make('WebView::group._group_more_attendance', compact('oAllUniqueLecture','sAttendanceType'));
        
            return \View::make('WebView::group.group_attendance', compact('oAllUniqueLecture','sAttendanceType'));
    }
    
    public function callGetAttendanceAjax(Request $oRequest)
    {
        $sAttendanceType = 'group';
        $oCourseGroups = GroupMember::getUserSelectiveGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'!=');
        $aUserGroups = explode(",", $oCourseGroups[0]->id_groups);
        
        $oAllUniqueLecture = GroupLecture::getGroupAttendanceList(Auth::user()->id_user,$aUserGroups);
        foreach($oAllUniqueLecture as $nKey => $oGroupLecture)
        {
            $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
            $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
            $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");

            $oAllUniqueLecture[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
            $oAllUniqueLecture[$nKey]['absent_lectures_count'] = $nAllLecturesCount-$nSickLeaveLecturesCount-$nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['all_lectures_count'] = $nAllLecturesCount;
        }
        //set cookie for notify new functionality
        $aCustomCookies['atten'] = TRUE;
        Cookie::queue(Cookie::forever('attendance', json_encode($aCustomCookies)));
        session(['current_page' => 'attendance','attendance' => 1]);

        if($oAllUniqueLecture->currentPage() > 1)
            return \View::make('WebView::group._group_attendance_ajax', compact('oAllUniqueLecture','sAttendanceType'));

        $view=  \View::make('WebView::group._group_attendance_ajax', compact('oAllUniqueLecture','sAttendanceType'));
        $html = $view->render();
        return Response::json(['success' => true, 'html' => $html ]);
        
    }
    
    //get all attendance list for all group in attendance module
    public function callCourseAttendance()
    {
        $sAttendanceType = 'course';
        $oCourseGroups = GroupMember::getUserSelectiveGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'=');
        $aUserGroups = explode(",", $oCourseGroups[0]->id_groups);
        
        $oAllUniqueLecture = GroupLecture::getGroupAttendanceList(Auth::user()->id_user,$aUserGroups);
        
        foreach($oAllUniqueLecture as $nKey => $oGroupLecture)
        {
            $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
            $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
            $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");

            $oAllUniqueLecture[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
            $oAllUniqueLecture[$nKey]['absent_lectures_count'] = $nAllLecturesCount-$nSickLeaveLecturesCount-$nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['all_lectures_count'] = $nAllLecturesCount;
        }
        //set cookie for notify new functionality
        $aCustomCookies['atten'] = TRUE;
        Cookie::queue(Cookie::forever('attendance', json_encode($aCustomCookies)));
        session(['current_page' => 'attendance','attendance' => 1]);

        if($oAllUniqueLecture->currentPage() > 1)
            return \View::make('WebView::group._group_more_attendance', compact('oAllUniqueLecture','sAttendanceType'));
        
            return \View::make('WebView::group.group_attendance', compact('oAllUniqueLecture','sAttendanceType'));
    }
    
    public function callCourseAttendanceAjax(Request $oRequest)
    {
        $sAttendanceType = 'course';
        $oCourseGroups = GroupMember::getUserSelectiveGroupIds(Auth::user()->id_user, config('constants.COURSEGROUPCATEGORY'),'=');
        $aUserGroups = explode(",", $oCourseGroups[0]->id_groups);
        
        $oAllUniqueLecture = GroupLecture::getGroupAttendanceList(Auth::user()->id_user,$aUserGroups);
        foreach($oAllUniqueLecture as $nKey => $oGroupLecture)
        {
            $nPresentLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPEPRESENT'));
            $nSickLeaveLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, config('constants.ATTENDANCETYPESICKLEAVE'));
            $nAllLecturesCount = GroupLecture::getGroupLecturesCount($oGroupLecture, Auth::user()->id_user, "");

            $oAllUniqueLecture[$nKey]['present_lectures_count'] = $nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['sick_leave_lectures_count'] = $nSickLeaveLecturesCount;
            $oAllUniqueLecture[$nKey]['absent_lectures_count'] = $nAllLecturesCount-$nSickLeaveLecturesCount-$nPresentLecturesCount;
            $oAllUniqueLecture[$nKey]['all_lectures_count'] = $nAllLecturesCount;
        }
        //set cookie for notify new functionality
        $aCustomCookies['atten'] = TRUE;
        Cookie::queue(Cookie::forever('attendance', json_encode($aCustomCookies)));
        session(['current_page' => 'attendance','attendance' => 1]);

        if($oAllUniqueLecture->currentPage() > 1)
            return \View::make('WebView::group._group_attendance_ajax', compact('oAllUniqueLecture','sAttendanceType'));

        $view=  \View::make('WebView::group._group_attendance_ajax', compact('oAllUniqueLecture','sAttendanceType'));
        $html = $view->render();
        return Response::json(['success' => true, 'html' => $html ]);
        
    }
    
    //get all student attendance for faculty account
    function callGetAttendanceExcel(Request $oRequest)
    {
        $oGroupLecture = GroupLecture::find($oRequest->id_lecture_excel);
        $dStartDate = GroupLecture::getLecture($oGroupLecture->id_group,Auth::user()->id_user,$oGroupLecture->semester,'asc');
        $dEndDate = GroupLecture::getLecture($oGroupLecture->id_group,Auth::user()->id_user,$oGroupLecture->semester,'desc');
        $oRequest->offsetSet('from', Carbon::createFromFormat('Y-m-d H:i:s', $dStartDate->created_at));
        $oRequest->offsetSet('to', Carbon::createFromFormat('Y-m-d H:i:s', $dEndDate->created_at));
        
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oRequest->id_group, Auth()->user()->id_user);
            
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            $aRequestData = array();
            $aRequestData['id_group']=$oGroupLecture->id_group;
            $aRequestData['semester']=$oGroupLecture->semester;
            $aRequestData['from']=$oRequest->from;
            $aRequestData['to']=$oRequest->to;
            $oGroupLecture = GroupLecture::getLectureDetail($aRequestData, Auth::user()->id_user);
                    
            $oCourse = Group::find($oRequest->id_group);
            $sFileName = str_replace(' ', '-', $oGroupLecture[0]->group_name).'-'.$oGroupLecture[0]->semester;
            
            $aLectureData = $oUserAttendance = array();
            if(count($oGroupLecture))
            {
                $aLectureData=$oGroupLecture->first();
                $nExcelFlag = 1;
                $oUserAttendance= UserGroupAttendance::getAllUserAttendanceForGroup($aLectureData,$nExcelFlag);
            }
            
            $aAttendaceContent = array();
            $aLectureheading = array("First Name","Last Name","NetID","Email","Total",trans('messages.present'),trans('messages.absent'),trans('messages.sick_leave'));
            $aLectureTotal = array("","","","","","","","Total");

            $aLecture=array(); $record=0;
            foreach($oGroupLecture as $Lecture)
            {
                $aLecture[$record]=$Lecture->id_group_lecture;
                $record++;
                $dLectureDate = Carbon::createFromFormat('Y-m-d H:i:s', $Lecture->created_at);
                $dLectureDate = $dLectureDate->setTimezone(Auth::user()->timezone);
                array_push( $aLectureheading,mb_substr($dLectureDate->format('F'),0,3).' '.$dLectureDate->format('d').' '.mb_substr($dLectureDate->format('l'),0,3).' '.$dLectureDate->format('h:i A'));
                array_push($aLectureTotal, $Lecture->present_student.'/'. count($oUserAttendance));
            }

            foreach($oUserAttendance as $oUserData)
            {
                $aLectureDate =$aIdPresentLecture= $aIdSickLecture= array();
                array_push( $aLectureDate,$oUserData->first_name);
                array_push( $aLectureDate,$oUserData->last_name);
                $aEmail = explode('@', $oUserData->email);
                array_push( $aLectureDate, $aEmail[0]);
                array_push( $aLectureDate, $oUserData->email);
                $aIdPresentLecture = explode(',',$oUserData->id_lecture_present); 
                $nPresent = count(array_intersect(array_unique($aIdPresentLecture), $aLecture));
                $aIdSickLecture = explode(',',$oUserData->id_lecture_sick); 
                $nSick = count(array_intersect(array_unique($aIdSickLecture), $aLecture));
                $nAbsent = count($aLecture)-$nPresent-$nSick;
                
                array_push( $aLectureDate,count($aLecture));
                array_push( $aLectureDate,$nPresent);
                array_push( $aLectureDate,$nAbsent);
                array_push( $aLectureDate,$nSick);
                foreach($aLecture as $rec)
                {
                    if(in_array($rec,$aIdPresentLecture)) 
                        array_push( $aLectureDate,'P');
                    elseif(in_array($rec,$aIdSickLecture)) 
                        array_push( $aLectureDate,'EA');
                    else 
                        array_push( $aLectureDate,'A');
                }
                array_push($aAttendaceContent, $aLectureDate);
            }
            ob_end_clean();
            Excel::create($sFileName, function($excel) use($aAttendaceContent, $aLectureheading, $aLectureTotal) {
                                // Set the title
                                $excel->setTitle('Our new awesome title');
                                // Chain the setters
                                $excel->setCreator('Campusknot')
                                      ->setCompany('Campusknot.com');
                                
                                $excel->sheet('Attendance', function($sheet) use($aAttendaceContent, $aLectureheading, $aLectureTotal) {
                                                    
                                                    $sheet->appendRow($aLectureheading);
                                                    $sheet->row($sheet->getHighestRow(), function ($row) {
                                                                    $row->setFontFamily('Arial');
                                                                    $row->setFontSize(12);
                                                                    $row->setFontWeight('bold');
                                                                });
                                                    
                                                    $sheet->rows($aAttendaceContent); 
                                                    
                                                     $sheet->appendRow($aLectureTotal);
                                                    $sheet->row($sheet->getHighestRow(), function ($row) {
                                                                    $row->setFontFamily('Arial');
                                                                    $row->setFontSize(12);
                                                                    $row->setFontWeight('bold');
                                                                });
                                                });
                            })->download('xls');
            ob_end_clean();
        }
    }
    
    public function callGetAttendanceTable(Request $oRequest, $nIdLecture='') 
    {
        $nIdLecture = (!empty($nIdLecture)) ? $nIdLecture : $oRequest->id_lecture;
        $oLectures = GroupLecture::find($nIdLecture);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oLectures->id_group, Auth()->user()->id_user);
            
        if($oGroupMemberDetail->member_type != config('constants.GROUPMEMBER'))
        {
        
            $aCustomCookies['atten'] = TRUE;
            Cookie::queue(Cookie::forever('attendance', json_encode($aCustomCookies)));
            session(['current_page' => 'attendance','attendance' => 1]);

            if(isset($oRequest->from) && $oRequest->from !='')
            {
                //for all lecture date from to to
                $dStartdate=Carbon::createFromFormat('m-d-Y', $oRequest->from)->toDateString();
                $dEnddate=Carbon::createFromFormat('m-d-Y', $oRequest->to)->toDateString();
                $aRequestData['id_group']=$oLectures->id_group;
                $aRequestData['semester']=$oLectures->semester;
                
                $dFromDate = Carbon::createFromFormat('m-d-Y', $oRequest->from)->setTimezone(Auth::user()->timezone)->format('m-d-Y');
                $dToDate = Carbon::createFromFormat('m-d-Y', $oRequest->to)->setTimezone(Auth::user()->timezone)->format('m-d-Y');
                $aFromDate = explode('-', $dFromDate);
                $aToDate = explode('-', $dToDate);
                $aRequestData['to'] = Carbon::create($aToDate[2], $aToDate[0], $aToDate[1], 23, 59, 59, Auth::user()->timezone)->setTimezone('UTC')->toDateTimeString();
                $aRequestData['from']=Carbon::create($aFromDate[2], $aFromDate[0], $aFromDate[1], 00, 00, 00, Auth::user()->timezone)->setTimezone('UTC')->toDateTimeString();
//                $aRequestData['from']=$oRequest->from;
//                $aRequestData['to']=$oRequest->to;
                $aRequestData['search_str']=$oRequest->search_student;
                $oLecture = GroupLecture::getLectureDetail($aRequestData, Auth::user()->id_user);
                $oUserAttendance= UserGroupAttendance::getAllUserAttendanceForGroup($aRequestData);
            }
            else 
            {
                //for only last lecture
                if($oLectures->activated == 0)
                {
                    //if deleted lecture id get then new function
                    $oGroupLecture = GroupLecture::where(['id_group' => $oLectures->id_group,'semester'=>$oLectures->semester,'activated'=>1,'deleted'=>0])
                                                  ->orderBy('id_group_lecture','desc')
                                                  ->first();
                    if(count($oGroupLecture))
                    {
                        $nIdLecture = $oGroupLecture->id_group_lecture;
                        $oLectures = $oGroupLecture;
                    }
                }
                $aRequestData['id_group']=$oLectures->id_group;
                $aRequestData['semester']=$oLectures->semester;
                $dLectureDate = Carbon::createFromFormat('Y-m-d H:i:s', $oLectures->created_at)->setTimezone(Auth::user()->timezone);
                $dDate = $dLectureDate->format('m-d-Y');
                $aDate = explode('-', $dDate);
                $aRequestData['to'] = Carbon::create($aDate[2], $aDate[0], $aDate[1], 23, 59, 59, Auth::user()->timezone)->setTimezone('UTC')->toDateTimeString();
                $aRequestData['from']=Carbon::create($aDate[2], $aDate[0], $aDate[1], 00, 00, 00, Auth::user()->timezone)->setTimezone('UTC')->toDateTimeString();
                $aRequestData['search_str']='';
                $oLecture = GroupLecture::getLectureDetail($aRequestData, Auth::user()->id_user);
                $oUserAttendance= UserGroupAttendance::getAllUserAttendanceForGroup($oLectures);
            }
            foreach($oUserAttendance as $nKey => $oUserGroupAttendance)
            {                
                $nAllLecturesCount = GroupLecture::getAllGroupLectureCount($oLectures);
                $oUserAttendance[$nKey]['present_lectures_count'] = GroupLecture::getGroupLecturesCount($oLectures, $oUserGroupAttendance->id_user, config('constants.ATTENDANCETYPEPRESENT'));
                $oUserAttendance[$nKey]['sick_leave_lectures_count'] = GroupLecture::getGroupLecturesCount($oLectures, $oUserGroupAttendance->id_user,config('constants.ATTENDANCETYPESICKLEAVE'));
                $oUserAttendance[$nKey]['absent_lectures_count'] =  $nAllLecturesCount- $oUserAttendance[$nKey]['present_lectures_count']- $oUserAttendance[$nKey]['sick_leave_lectures_count'];
                $oUserAttendance[$nKey]['all_lectures_count'] = $nAllLecturesCount;
            }
            $oAttendace = 1;
            if($oUserAttendance->currentPage() > 1)
            {            
                $html=  \View::make('WebView::group._more_attendance_group_admin', compact('oLecture', 'oUserAttendance','oAttendace'))->render();
                $oAttendace = 0;
                $RowView=  \View::make('WebView::group._more_attendance_group_admin', compact('oLecture', 'oUserAttendance','oAttendace'))->render();

                return Response::json(['success' => true, 'html' => $html,'rowview' => $RowView ]);
            }
            $view=  \View::make('WebView::group._attendance_group_admin', compact('oLecture', 'oUserAttendance','oRequest'));
            $html = $view->render();
            return Response::json(['success' => true, 'html' => $html ]);
        }
    }
    
    //add member in attendance from faculty side 
    function callAddMemberInList(Request $oRequest)
    {
        $aValidationRequireFor = [
                                        'id_user' => 'required',
                                        'id_group_lecture' => 'required',
                                    ];
        $this->validate($oRequest, $aValidationRequireFor);
        $oLecture = GroupLecture::find($oRequest->id_group_lecture);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oLecture->id_group, Auth()->user()->id_user);
            
        if($oGroupMemberDetail->member_type != config('constants.GROUPMEMBER'))
        {
            
            $nUserExist = GroupLecture::checkMemberExists($oLecture,$oRequest->id_user);
            if($nUserExist <= 0 && count($oLecture) > 0){
                $oUserAttendance= UserGroupAttendance::firstOrNew(['id_group_lecture' => $oRequest->id_group_lecture,
                                                        'id_user' => $oRequest->id_user]);
                $oUserAttendance->attendance_type = 'A';
                $oUserAttendance->activated = 1;
                $oUserAttendance->deleted = 0;
                $oUserAttendance->save();
                return Response::json(['success' => true, 'msg' => trans('messages.member_exists_list_successfully')]);
            }
            else
                return Response::json(['success' => false, 'msg' => trans('messages.member_exists_list')]);
        }
    }
    
    function getStudentSignleAttendace($nIdLecture)
    {
        $nIdLecture = (!empty($nIdLecture)) ? $nIdLecture : $oRequest->id_lecture;
         //if student is absent dont get that lacture so find unique lacture
        $oLecture = GroupLecture::getLectureData($nIdLecture);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oLecture->id_group, Auth()->user()->id_user);
            
        if($oGroupMemberDetail->member_type == config('constants.GROUPMEMBER'))
        {
            session(['current_page' => 'attendance','attendance' => 1]);
            
            $oAllLacture['lecture'] = GroupLecture::getAllSameLecture($oLecture, Auth::user()->id_user);
            $aAllLacture = $oAllLacture['lecture']->toArray();
            $nIdAttendanceLog = array_column($aAllLacture, 'count_id_attendance_log');
            $nIdLectures = array_column($aAllLacture, 'id_group_lecture');
            if(array_sum($nIdAttendanceLog) > 0)
                $oLecture->isLog = 1;
            else
                $oLecture->isLog = 0;

            $oAllLacture['group'] = Group::find($oLecture->id_group);
            $oAllLacture['lecture_ids'] = implode(',', $nIdLectures);
            //set cookie for notify new functionality
            $aCustomCookies['atten'] = TRUE;
            Cookie::queue(Cookie::forever('attendance', json_encode($aCustomCookies)));
            $view=  \View::make('WebView::group._attendance_group_student_detail', compact('oLecture', 'oAllLacture'));
            $html = $view->render();
            return Response::json(['success' => true, 'html' => $html ]);
        }
        else
        {
            return redirect('/group/attendance');
        }
    }
    
    public function callAttendaceLog(Request $oRequest)
    {
        $sIdAttendanceLog = $oRequest->sIds;
        $aIdAttendaceLog = explode(',',$sIdAttendanceLog);
        $oUserAttendaceLog = GroupAttendanceLog::getAdminAttendaceLog($aIdAttendaceLog);
        return \View::make('WebView::group._group_attendance_log',compact('oUserAttendaceLog'));      
    }
    
    public function getMemberAttendaceLog(Request $oRequest) 
    {
        $sIdLecture = $oRequest->sIds;
        $aIdLecture = explode(',',$sIdLecture);
        $oUserAttendaceLog = GroupAttendanceLog::getMemberAttendaceLog($aIdLecture);
        return \View::make('WebView::group._group_attendance_log',compact('oUserAttendaceLog'));
    }
    
    //delete lecture from faculty account
    public function callDeleteGroupLecture($nIdLecture)
    {
        $oLecture = GroupLecture::find($nIdLecture);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oLecture->id_group, Auth::user()->id_user);
        
        if($oGroupMemberDetail->member_type == config('constants.GROUPADMIN') || $oGroupMemberDetail->member_type == config('constants.GROUPCREATOR'))
        {
            GroupLecture::where('id_group_lecture',$nIdLecture)
                        ->update(array('activated' => 0));
            return "success";
        }
        else
            return "fail";
    }
    
    //edit attendace from admin side
    public function callChangeStudentAttendance(Request $oRequest)
    {
        $nIdLecture = $oRequest->id_lecture;
        $nIdUser = $oRequest->id_user;
        $oLecture = GroupLecture::find($nIdLecture);
        $oGroupMemberDetail = GroupMember::getGroupMemberDetail($oLecture->id_group, Auth()->user()->id_user);
        
        if($oGroupMemberDetail->member_type != config('constants.GROUPMEMBER'))
        {
            //echo 'asdasd';exit;
            $oUserAttendance = UserGroupAttendance::firstOrNew([
                                                        'id_user'=>$nIdUser,
                                                        'id_group_lecture'=>$nIdLecture
                                                        ]);
            $sChangeFrom = ($oUserAttendance->attendance_type != '') ? $oUserAttendance->attendance_type : config('constants.ATTENDANCETYPEABSENT');
            //print_r($oUserAttendance);exit;
            if($oRequest->sAttenText == config('constants.ATTENDANCETYPEABSENT'))
            {
                $oUserAttendance->attendance_type = config('constants.ATTENDANCETYPEABSENT');
                $oUserAttendance->save();
                $html='
                    <span class="attendance-record absent pos-relative">
                    <span>'.trans('messages.absent_short').'</span>
                    <span class="edit-attendance-text">
                        <span>
                            <input class="" type="radio" name="attendance" value="'.config('constants.ATTENDANCETYPEPRESENT').'"> 
                            '.trans('messages.mark_as_paresent').'
                        </span>
                        <span>
                            <input class="" type="radio" name="attendance" value="'.config('constants.ATTENDANCETYPESICKLEAVE').'"> 
                            '.trans('messages.mark_as_sick_leave').'
                        </span>
                        <span>
                            <input type="button" onclick="changeStudentAttendance("'.$nIdUser.'",'.$nIdLecture.'",\'group\');" value="'. trans('messages.save').'" class="link-button pull-right"> 
                        </span>
                    </span>
                </span>';
                $oAttendanceLog = GroupAttendanceLog::create([
                                                        'id_user' => Auth::user()->id_user,
                                                        'id_user_group_attendance' => $oUserAttendance->id_user_group_attendance,
                                                        'change_from' => $sChangeFrom,
                                                        'change_to' => config('constants.ATTENDANCETYPEABSENT')
                                                    ]);
                $oAttendanceLog->save();
            }
            else if($oRequest->sAttenText == config('constants.ATTENDANCETYPESICKLEAVE'))
            {
                $oUserAttendance->attendance_type = config('constants.ATTENDANCETYPESICKLEAVE');
                $oUserAttendance->save();
                $html='
                    <span class="attendance-record sick-leave pos-relative">
                    <span>'.trans('messages.sick_leave_short').'</span>
                    <span class="edit-attendance-text">
                        <span>
                            <input class="" type="radio" name="attendance" value="'.config('constants.ATTENDANCETYPEPRESENT').'"> 
                            '.trans('messages.mark_as_paresent').'
                        </span>
                        <span>
                            <input class="" type="radio" name="attendance" value="'.config('constants.ATTENDANCETYPEABSENT').'"> 
                            '.trans('messages.mark_as_absent').'
                        </span>
                        <span>
                            <input type="button" onclick="changeStudentAttendance("'.$nIdUser.'",'.$nIdLecture.'",\'group\');" value="'. trans('messages.save').'" class="link-button pull-right"> 
                        </span>
                    </span>
                </span>';
                $oAttendanceLog = GroupAttendanceLog::create([
                                                            'id_user' => Auth::user()->id_user,
                                                            'id_user_group_attendance' => $oUserAttendance->id_user_group_attendance,
                                                            'change_from' => $sChangeFrom,
                                                            'change_to' => config('constants.ATTENDANCETYPESICKLEAVE')
                                                        ]);
                $oAttendanceLog->save();
            }
            else if($oRequest->sAttenText == config('constants.ATTENDANCETYPEPRESENT'))
            {
                $oUserAttendance->attendance_type = config('constants.ATTENDANCETYPEPRESENT');
                $oUserAttendance->save();
                $html='
                    <span class="attendance-record present pos-relative">
                    <span>'.trans('messages.present_short').'</span>
                    <span class="edit-attendance-text">
                        <span>
                            <input class="" type="radio" name="attendance" value="'.config('constants.ATTENDANCETYPEABSENT').'"> 
                           '.trans('messages.mark_as_absent').'
                        </span>
                        <span>
                            <input class="" type="radio" name="attendance" value="'.config('constants.ATTENDANCETYPESICKLEAVE').'"> 
                            '.trans('messages.mark_as_sick_leave').'
                        </span>
                        <span>
                            <input type="button" onclick="changeStudentAttendance("'.$nIdUser.'",'.$nIdLecture.'",\'group\');" value="'. trans('messages.save').'" class="link-button pull-right"> 
                        </span>
                    </span>
                </span>';
                $oAttendanceLog = GroupAttendanceLog::create([
                                                        'id_user' => Auth::user()->id_user,
                                                        'id_user_group_attendance' => $oUserAttendance->id_user_group_attendance,
                                                        'change_from' => $sChangeFrom,
                                                        'change_to' => config('constants.ATTENDANCETYPEPRESENT')
                                                    ]);
                $oAttendanceLog->save();
            }
            $oNotification = Notification::create([
                                                'id_user' => $nIdUser,
                                                'id_entites' => Auth::user()->id_user.'_'.$oAttendanceLog->id_group_attendance_log.'_'.$oLecture->id_group,
                                                'entity_type_pattern' => 'U_GAL_G',
                                                'notification_type' => config('constants.NOTIFICATIONGROUPATTENDANCECHANGE')
                                            ]);
            $oNotification->save();
        }
        return Response::json(['success' => true, 'html' => $html,'message' => trans('messages.success_change_attendace')]);
    }
}