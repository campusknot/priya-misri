<?php

namespace App\Http\Web\Controllers;

use Auth;
use Session;

use Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Cookie;

use App\Libraries\GroupMember;
use App\Libraries\SharedDocument;
use App\Libraries\GroupDocument;
use App\Libraries\DocumentPermisssion;
use App\Libraries\Document;
use App\Libraries\Group;
use App\Libraries\GroupPost;
use App\Libraries\Post;

use App\Jobs\SendDocumentSharedNotification;
use App\Jobs\SendNewCreatedDocumentNotification;

class DocumentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => [
                                                'callCreateDocument','copyFolder','addGroupDocument','addUserDocument',
                                                'callDocumentListing', 'callDocumentListingAjax',
                                                'callPostDocument','callPostDocumentAjax',
                                                'callSharedDocument', 'callSharedDocumentAjax',
                                                'callGroupDocument', 'callGroupDocumentAjax',
                                                'callGroupPostDocument','callGroupPostDocumentAjax',
                                                'callGetSearchForShare', 'callDocumentShare',
                                                'callDeleteDocument','getGroupChildDocument',
                                                'callDocumentPermissionChange','getGroupDocumentMember','callDocumentShareUserList'
                                            ]
                        ]);
        
        parent::__construct();
        session(['current_page' => 'document']);
        $aCustomCookies['document'] = TRUE;
        Cookie::queue(Cookie::forever('document', json_encode($aCustomCookies)));
    }
    
    //create folder in document
    public function callCreateDocument(Request $oRequest)
    {
        if(Session::get('current_permission') == config('constants.DOCUMENTPERMISSIONTYPEWRITE'))
        {
            if($oRequest->parent_id != '' && $oRequest->parent_id != 'undefined') 
                $nParentId = $oRequest->parent_id;
            else
                $nParentId = NULL;

            $oDocument= Document::find($oRequest->parent_id);
            $sFileName='';
            if($oRequest->doc_type == config('constants.DOCUMENTTYPEFILE'))
            {
                $aValidationRequiredFor = [
                                            'file' => 'required|max:40000'
                                        ];
                if ($oRequest->hasFile('file')) 
                {
                    $oRequest->offsetSet('extention', mb_strtolower($oRequest->file('file')->getClientOriginalExtension()));
                    $aValidationRequiredFor['extention'] = 'required|in:bmp,jpg,jpeg,png,svg,doc,docx,pages,rtf,txt,wp,numbers,xls,xlsx,csv,key,ppt,pptx,pps,mdb,acc,accdb,pdf,zip,rar';
                } 
                $this->validate($oRequest, $aValidationRequiredFor);              
            }
            else
            {
                $aValidationRequiredFor = [
                                            'folder_name' => 'required'
                                        ];
            }
            $this->validate($oRequest, $aValidationRequiredFor);
            
            if($oRequest->doc_type == config('constants.DOCUMENTTYPEFILE'))
            {
                $oUploadedFile = $oRequest->file('file');
                if($oUploadedFile != ''){
                    $sOriginalName = $oUploadedFile->getClientOriginalName();
                    $oRequest->folder_name = $sOriginalName;
                    $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
                    $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image

                    $oS3 = \Storage::disk('s3');
                    $filePath = '/'.config('constants.POSTMEDIAFOLDER').'/' . $sFileName;
                    $oS3->put($filePath, fopen($oUploadedFile, 'r+'), 'public');

                }

            }
            //rename folder or file
            if($oRequest->id_document!='')
            {
                $oDocument = Document::find($oRequest->id_document);
                if($oDocument->id_user == Auth::user()->id_user){
                    if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                    {
                        $aExt = explode('.',$oDocument->file_name);
                        $sExtension = $aExt[count($aExt)-1];
                        $aFileNewName = explode('.',$oRequest->folder_name);
                        $sFileNewName = $aFileNewName[count($aFileNewName)-1];
                        if($sExtension == $sFileNewName)
                            $sFileName= $oRequest->folder_name;
                        else
                           $sFileName= $oRequest->folder_name.'.'.$sExtension;
                    }
                    else 
                    {
                        $sFileName= $oRequest->folder_name;
                    }
                $oDocument->document_name = $sFileName;
                $oDocument->save();

                }
                else
                {
                    $oRequest->session()->flash('success_message', trans('messages.authentication_document_error'));
                }
            }
            else
            {
                $oDocument = Document::create(['document_name' => $oRequest->folder_name,
                                            'id_parent' => $nParentId,
                                            'document_type' => $oRequest->doc_type,
                                            'id_user' => Auth::user()->id_user,
                                            'shared_with' => $oRequest->shared_with,
                                            'file_name' => $sFileName
                                        ]);
                $oDocument->save();
                
                DocumentPermisssion::create([
                                            'id_document' => $oDocument->id_document,
                                            'id_user' => Auth::user()->id_user,
                                            'permission_type' => config('constants.DOCUMENTPERMISSIONTYPEWRITE')
                                        ]); 
                if($nParentId != ''){
                    $this->dispatch(new SendNewCreatedDocumentNotification($oDocument->id_document));
                 }
                
                if($oRequest->id_group && $oRequest->id_group != '' && $nParentId == '')
                {
                    $this->addGroupDocument($oDocument->id_document, $oRequest->id_group, Auth::user()->id_user,config('constants.DOCUMENTPERMISSIONTYPEREAD'));
                }
            }
        }
    }
    
    //share document with group
    public function addGroupDocument( $nIdDocument ,$nIdGroup, $nIdUserFrom,$sPermission)
    {
        $oGroupDocument = GroupDocument::firstOrCreate([
                                                        'id_document' => $nIdDocument,
                                                        'id_group' => $nIdGroup
                                                    ]);
        $oGroupDocument->group_document_permission = $sPermission;
        $oGroupDocument->save();
        
        $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                            'id_document' => $nIdDocument,
                                            'id_user' => Auth::user()->id_user,
                                            'id_group' => $nIdGroup
                                        ]);
        $oDocumentPermisssion->permission_type = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
        $oDocumentPermisssion->activated = 1;
        $oDocumentPermisssion->deleted = 0;
        $oDocumentPermisssion->save();
                
        $aGroupMembers = GroupMember::getAllGroupMembers($nIdGroup)->all();
        $aGroupMemberIds = array_map(function($oGroupMember){
                                    return intval($oGroupMember->id_user);
                                },$aGroupMembers);
        $this->dispatch(new SendDocumentSharedNotification($nIdDocument, Auth::user()->id_user, $sPermission, $aGroupMemberIds, $nIdGroup));
            
        return true;
    }
    
    //share document with user one to one
    public function addUserDocument( $nIdDocument ,$nIdUserTo, $nIdUserFrom,$sPermission )
    { 
        //one to one share  //      
        if($nIdUserFrom != $nIdUserTo)
        {
            $oSharedDocument = SharedDocument::firstOrNew([
                                                                'id_document' => $nIdDocument,
                                                                'id_user' => $nIdUserFrom,
                                                                'shared_with' => $nIdUserTo
                                                            ]);
            $oSharedDocument->activated = 1;
            $oSharedDocument->deleted = 0;
            $oSharedDocument->save();
            
            $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                                                            'id_document' => $nIdDocument,
                                                                            'id_user' => $nIdUserTo,
                                                                            'id_group' => NULL
                                                                        ]);
            $oDocumentPermisssion->permission_type = $sPermission;
            $oDocumentPermisssion->activated = 1;
            $oDocumentPermisssion->deleted = 0;
            $oDocumentPermisssion->save();
        }
        return true;
    }

    public function copyFolder(Request $oRequest,$nIdDocument='')
    {
        $nIdDocument = (!empty($nIdDocument)) ? $nIdDocument : $oRequest->id_document;
        if($this->CheckDocumentAccessRights($nIdDocument,config('constants.DOCUMENTPERMISSIONTYPEWRITE')) > 0)
        {    
            $oDocument = Document::find($nIdDocument);
            if($oRequest->isMethod('POST'))
            {
                $aParentId = explode('_', $oRequest->id_parent);
                if(in_array('G', $aParentId) || $aParentId[0] == '')
                    $nParentId = NULL;
                else
                    $nParentId = $aParentId[0];

                $oDuplicateDocument = $oDocument->replicate();
                if($oDocument->document_type == config('constants.DOCUMENTTYPEFOLDER'))
                {
                    $oDuplicateDocument->document_name = $oDocument->document_name.'-copy';
                }
                else
                {
                    $sName = explode('.',$oDocument->document_name);
                    $last = array_pop($sName);
                    $parts = array(implode('_', $sName), $last);
                    $oDuplicateDocument->document_name = $parts[0].'-copy.'.$last;

                }
                $oDuplicateDocument->copy_from = $oDocument->id_document;
                $oDuplicateDocument->id_parent = $nParentId;
                $oDuplicateDocument->save();
                
                $nIdParent=$oDuplicateDocument->id_document;
                
                if(in_array('G', $aParentId))
                {
                    $this->addGroupDocument($nIdParent, $aParentId[0], Auth::user()->id_user,config('constants.DOCUMENTPERMISSIONTYPEREAD'));
                }
                else
                {
                    DocumentPermisssion::create([
                                                    'id_document' => $nIdParent,
                                                    'id_user' => Auth::user()->id_user,
                                                    'permission_type' => config('constants.DOCUMENTPERMISSIONTYPEWRITE')
                                                ]);
                }
                $oDocumentChild= $oDocument->getDescendants()->toHierarchy();
                //create new tree for group document
                $aCopyRecursive = $this->callCopyRecursive($oDocumentChild, $nIdParent );
                
                $oRequest->session()->flash('success_message', trans('messages.copy_folder_success'));
                if(in_array('G', $aParentId))
                {
                    return Redirect::route('documents.group-document',array('nIdGroup' => $aParentId[0]));
                }
                return Redirect::route('documents.document-listing',array('nIdDocument' => $oRequest->id_parent));

            }
            $oDocumentsList = Document::getAllMyDocuments(Auth::user()->id_user);
            $oDocuments = array();
            $nCount = 0;
            foreach ($oDocumentsList as $oDoc) 
            {
                $oDocuments[$nCount] = Document::getAllChildDocument($oDoc);
                $nCount++;
            }
            $oGroupList = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            foreach ($oGroupList as $key=>$oGroups) 
            {
                $oGroupList[$key]->group_doc = GroupDocument::getGroupDocuments($oGroups->id_group,Auth::user()->id_user);
            }
            $oSharedFolder = SharedDocument::getSharedDocumentList(Auth::user()->id_user);
            //print_r($oSharedFolder);exit;
            $view = \View::make('WebView::documents._copy_document', compact('oDocuments','oDocument','oGroupList','oSharedFolder'));
            $html = $view->render();
            return Response::json(['success' => true, 'html' => $html]);
        }
        else
             return response()->view('errors.access_denied');
    }
    
    private function callCopyRecursive($oChildDocuments,$nIdParent) 
    {
        foreach($oChildDocuments as $oDocument)
        {
            $oDoc = Document::find($oDocument->id_document);
            $aDocument = $oDoc->replicate();
            $aDocument->id_parent = $nIdParent;
            $aDocument->copy_from = $oDocument->id_document;
            $aDocument->save();
            if(count($oDocument->children))
            {
                $this->callCopyRecursive($oDocument->children,$aDocument->id_document);
            }
        }
    }
    //All document data which is created by me
    public function callDocumentListing(Request $oRequest,$nDocumentId= null)
    {
        if($this->CheckDocumentAccessRights($nDocumentId) > 0)
        {
            $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
            $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
            $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';


            $nIdParent=$nDocumentId;
            $sSharedWith=null;
            $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');        
            $aBreadCrumb = array();
            if($nDocumentId != '')
            {
                $aDocument = Document::find($nDocumentId);
                $sSharedWith= $aDocument->shared_with;
                $oCurrentPer = Document::countAncestorsWithWritePermission($aDocument, Auth::user()->id_user);
                if($oCurrentPer > 0 )
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                }
                $aBreadCrumb = Document::getAncestorsWithPermission($aDocument, Auth::user()->id_user);
                $oDocuments = Document::getChildDocument($nDocumentId,$sOrderBy,$sOrderField,$sSearchStr);
            }
            else
                $oDocuments = Document::getDocumentList(Auth::user()->id_user,$sOrderBy,$sOrderField,$sSearchStr);
            //echo $oDocuments;exit;
            session(['current_permission' => $sCurrentPer ]);

            $oGroupListTab = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oDocuments->count();
            $oDocuments = $oDocuments->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oDocuments = new \Illuminate\Pagination\LengthAwarePaginator($oDocuments, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);

            $sDocumentListing = "DocumentList";

            foreach($oDocuments as $Key=>$oDocument)
            {
                $oDocuments[$Key]['can_delete'] = !Document::isChildShareorNot($oDocument);
                $oDocuments[$Key]['count_group'] = GroupDocument::getCountGroupDocument($oDocument->id_document);
                $oDocuments[$Key]['group_document'] = GroupDocument::getGroupDocument($oDocument->id_document,0,3);
                $oDocuments[$Key]['count_user'] = SharedDocument::countSharedUserList($oDocument->id_document);           
                if(count($oDocuments[$Key]['group_document']) < 3 )
                {
                    $nUserTake = 3-$oDocuments[$Key]['count_group'];
                    //$nUserTake = $oDocuments[$Key]['count_user']-$nUserSkip;
                    $oDocuments[$Key]['userList'] = SharedDocument::sharedUserList($oDocument->id_document,0,$nUserTake);
                }
            }        
            if($oDocuments->currentPage() > 1)
            {
                return \View::make('WebView::documents._more_document', compact('oDocuments'));
            }

            return \View::make('WebView::documents.document_listing',compact('oGroupListTab', 'oDocuments', 'aBreadCrumb','nIdParent','sSharedWith',
                                                                        'sDocumentListing', 'nDocumentId','sOrderBy','sOrderField','sSearchStr')); 
        }
        else
             return response()->view('errors.access_denied');
    }
    
    public function callDocumentListingAjax(Request $oRequest,$nDocumentId= null)
    {
        if($this->CheckDocumentAccessRights($nDocumentId) > 0)
        {
            $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
            $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
            $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';

            $nIdParent=$nDocumentId;
            $sSharedWith= null;
            $sPermission = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            session(['current_permission' => $sPermission ]);
            $html['CreateFolderHtml'] = '';

            $oCurrentPer = $aBreadCrumb = array();
            if($nDocumentId != '')
            {
                $aDocument = Document::find($nDocumentId);
                $sSharedWith= $aDocument->shared_with;
                $aBreadCrumb = Document::getAncestorsWithPermission($aDocument, Auth::user()->id_user);

                $oCurrentPer = Document::countAncestorsWithWritePermission($aDocument, Auth::user()->id_user);
                if($oCurrentPer > 0 )
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                }
                $oDocuments = Document::getChildDocument($nDocumentId,$sOrderBy,$sOrderField,$sSearchStr);
            }
            else
                $oDocuments = Document::getDocumentList(Auth::user()->id_user,$sOrderBy,$sOrderField,$sSearchStr);

            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oDocuments->count();
            $oDocuments = $oDocuments->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oDocuments = new \Illuminate\Pagination\LengthAwarePaginator($oDocuments, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);
            foreach($oDocuments as $Key=>$oDocument)
            {

                $oDocuments[$Key]['can_delete'] = !Document::isChildShareorNot($oDocument);
                $oDocuments[$Key]['count_group'] = GroupDocument::getCountGroupDocument($oDocument->id_document);
                $oDocuments[$Key]['group_document'] = GroupDocument::getGroupDocument($oDocument->id_document,0,3);
                $oDocuments[$Key]['count_user'] = SharedDocument::countSharedUserList($oDocument->id_document);           
                if(count($oDocuments[$Key]['group_document']) < 3 )
                {
                    $nUserTake = 3-$oDocuments[$Key]['count_group'];
                    //$nUserTake = $oDocuments[$Key]['count_user']-$nUserSkip;
                    $oDocuments[$Key]['userList'] = SharedDocument::sharedUserList($oDocument->id_document,0,$nUserTake);
                }
            }

            if(Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD'))
            {
                $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                $html['CreateFolderHtml'] = view('WebView::documents._create_folder_tab')->render();
            }
            //echo $oDocuments->currentPage();
            if($oDocuments->currentPage() > 1)
                return \View::make('WebView::documents._more_document', compact('oDocuments'));

            $html['DocumentList'] = view('WebView::documents._my_document',compact('oDocuments','aBreadCrumb','nIdParent', 'sSharedWith',
                                                                                    'nDocumentId','sOrderBy','sOrderField','sSearchStr'))->render();
            return response()->json( array('success' => true, 'html'=>$html) );
        }    
        else
            return redirect()->back();
        
    }
    
    //get data of shared document with me
    public function callSharedDocument(Request $oRequest, $nDocumentId = '')
    {
        if($this->CheckDocumentAccessRights($nDocumentId) > 0)
        {
            $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
            $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
            $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';

            $nIdParent=$nDocumentId;
            $sSharedWith= null;
            $aBreadCrumb = array();
            $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');

            if($nDocumentId != '')
            { 
                $oDocument = Document::find($nDocumentId);
                $sSharedWith= $oDocument->shared_with;
                $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);
                //echo "<pre>";print_r($aBreadCrumb);exit;
                $oCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
                if($oCurrentPer > 0 )
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                }
            }
            else
            {
                Session::forget('current_permission');
            }
            session(['current_permission' => $sCurrentPer ]);
            $sDocumentListing = "ShareDocumentList";
            $oGroupListTab = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            if($nDocumentId == '')
                $oSharedDocument = Document::getDocumentSharedList(Auth::user()->id_user,$sOrderBy,$sOrderField,$sSearchStr);
            else
                $oSharedDocument = Document::getChildDocument($nDocumentId,$sOrderBy,$sOrderField,$sSearchStr);

            foreach($oSharedDocument as $Key=>$oDocument)
            {
                //$oSharedDocument[$Key]['user_list'] = SharedDocument::sharedUserList($oDocument->id_document);
                $oSharedDocument[$Key]['count_user'] = SharedDocument::countSharedUserList($oDocument->id_document);
                $oSharedDocument[$Key]['user_list'] = SharedDocument::sharedUserList($oDocument->id_document,3);
            }

            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oSharedDocument->count();
            $oSharedDocument = $oSharedDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oSharedDocument = new \Illuminate\Pagination\LengthAwarePaginator($oSharedDocument, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);
            //print_r($oSharedDocument);
            if($oSharedDocument->currentPage() > 1)
            {
                return \View::make('WebView::documents._more_shared_document', compact('oSharedDocument'));
            }
            return \View::make('WebView::documents.document_listing',compact('oSharedDocument', 'aBreadCrumb','nIdParent','sSharedWith',
                                                                        'oGroupListTab','sDocumentListing','sOrderBy','sOrderField','sSearchStr'));
        }
        else
            return response()->view('errors.access_denied');
            
    }
    
    public function callSharedDocumentAjax(Request $oRequest, $nDocumentId = '')
    {
        if($this->CheckDocumentAccessRights($nDocumentId) > 0)
        {
            $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
            $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
            $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';

            $nIdParent=$nDocumentId;
            $sSharedWith= null;
            $aBreadCrumb = array();
            $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');

            if($nDocumentId != '')
            { 
                $oDocument = Document::find($nDocumentId);
                $sSharedWith= $oDocument->shared_with;
                $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);
                $oCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
                if($oCurrentPer > 0 )
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                }
            }
            else
            {
                Session::forget('current_permission');
            }
            session(['current_permission' => $sCurrentPer ]);

            $oGroupListTab = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            if($nDocumentId == '')
                $oSharedDocument = Document::getDocumentSharedList(Auth::user()->id_user,$sOrderBy,$sOrderField,$sSearchStr);
            else
                $oSharedDocument = Document::getChildDocument($nDocumentId,$sOrderBy,$sOrderField,$sSearchStr);

            foreach($oSharedDocument as $Key=>$oDocument)
            {
                $oSharedDocument[$Key]['count_user'] = SharedDocument::countSharedUserList($oDocument->id_document);
                $oSharedDocument[$Key]['user_list'] = SharedDocument::sharedUserList($oDocument->id_document,3);
            }

            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oSharedDocument->count();
            $oSharedDocument = $oSharedDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oSharedDocument = new \Illuminate\Pagination\LengthAwarePaginator($oSharedDocument, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);

            if($oSharedDocument->currentPage() > 1)
            {
                return \View::make('WebView::documents._more_shared_document', compact('oSharedDocument'));
            }
            $html['CreateFolderHtml'] = '';
            if(Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD'))
            {
                $html['CreateFolderHtml'] = view('WebView::documents._create_folder_tab')->render();
            }

            $html['DocumentList'] = view('WebView::documents._shared_document',compact('oSharedDocument', 'oCurrentPer', 'aBreadCrumb','nIdParent',
                                                                                        'sSharedWith','sOrderBy','sOrderField','sSearchStr'))->render();

            return response()->json( array('success' => true, 'html'=>$html) );
        }
        else
            return redirect()->back();
    }

    //GET group document
    public function callGroupDocument(Request $oRequest, $nIdGroup, $nIdDocument=null) 
    {
        if($this->CheckDocumentAccessRights($nIdDocument) > 0)
        {
            $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
            $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
            $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';

            Session::forget('current_permission');

            $nIdParent=$nIdDocument;
            $sSharedWith= config('constants.DOCUMENTSHAREDWITHGROUP');
            $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            $oGroup = Group::find($nIdGroup);
            $aBreadCrumb = array();
            if($nIdDocument != '')
            {
                $oDocument = Document::find($nIdDocument);
                $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);
                $nCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
                if($nCurrentPer > 0 )
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                }
                else
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');
                }
            }
            session(['current_permission' => $sCurrentPer ]);
            $oGroupListTab = GroupMember::getUserGroupsTab( Auth::user()->id_user );
            $sDocumentListing = "GroupDocumentList";
            if($nIdDocument == '')
                $oGroupDocument = GroupDocument::getDocumentGroupShared($nIdGroup,$sOrderBy,$sOrderField,$sSearchStr);
            else
                $oGroupDocument = Document::getChildDocument($nIdDocument,$sOrderBy,$sOrderField,$sSearchStr);                

            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oGroupDocument->count();
            $oGroupDocument = $oGroupDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oGroupDocument = new \Illuminate\Pagination\LengthAwarePaginator($oGroupDocument, $nTotal, $nLimit, $nPage, [
                                                                        'path' => $oRequest->url(),
                                                                        'pageName' => 'page',
                                                                ]);
            foreach($oGroupDocument as $Key=>$oDocument)
            {
                $oGroupDocument[$Key]['can_delete'] = !Document::isChildShareorNot($oDocument);
                $oGroupDocument[$Key]['count_group'] = GroupDocument::getCountGroupDocument($oDocument->id_document);
                $oGroupDocument[$Key]['group_document'] = GroupDocument::getGroupDocument($oDocument->id_document,0,3);
                $oGroupDocument[$Key]['count_user'] = SharedDocument::countSharedUserList($oDocument->id_document);           
                if(count($oGroupDocument[$Key]['group_document']) < 3 )
                {
                    $nUserTake = 3-$oGroupDocument[$Key]['count_group'];
                    //$nUserTake = $oDocuments[$Key]['count_user']-$nUserSkip;
                    $oGroupDocument[$Key]['userList'] = SharedDocument::sharedUserList($oDocument->id_document,0,$nUserTake);
                }
            }       

            if($oGroupDocument->currentPage() > 1)
            {
                return \View::make('WebView::documents._more_group_document', compact('oGroupDocument','oGroup','nIdParent', 'nIdDocument'));
            }
            return \View::make('WebView::documents.document_listing',compact('oGroupDocument', 'aBreadCrumb','nIdParent','sSharedWith','oGroupListTab',
                                                                            'oGroup','sDocumentListing', 'nIdDocument','sOrderBy','sOrderField','sSearchStr'));
        }
        else
             return response()->view('errors.access_denied');
    }

    public function callGroupDocumentAjax(Request $oRequest, $nIdGroup, $nIdDocument=null)
    {
        if($this->CheckDocumentAccessRights($nIdDocument) > 0)
        {
            $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
            $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
            $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';

            $nIdParent=$nIdDocument;
            $sSharedWith= config('constants.DOCUMENTSHAREDWITHGROUP');
            $aBreadCrumb = array();
            $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            $oGroup = Group::find($nIdGroup);
            if($nIdDocument != '')
            { 
                $oDocument = Document::find($nIdDocument);
                $aBreadCrumb = Document::getAncestorsWithPermission($oDocument,Auth::user()->id_user);

                $nCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
                if($nCurrentPer > 0 )
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
                }
                else
                {
                    $sCurrentPer = config('constants.DOCUMENTPERMISSIONTYPEREAD');
                }
            }
            else
            {
                Session::forget('current_permission');
            }
            session(['current_permission' => $sCurrentPer]);

            $oGroupListTab = GroupMember::getUserGroupsTab(Auth::user()->id_user);
            if($nIdDocument == '')      
                $oGroupDocument = GroupDocument::getDocumentGroupShared($nIdGroup,$sOrderBy,$sOrderField,$sSearchStr);
            else
                $oGroupDocument = Document::getChildDocument($nIdDocument,$sOrderBy,$sOrderField,$sSearchStr);

            $nLimit = config('constants.PERPAGERECORDS');
            $nPage = $oRequest->has('page') ? $oRequest->page : 1;
            $nTotal = $oGroupDocument->count();
            $oGroupDocument = $oGroupDocument->slice(max(array($nPage - 1, 0)) * $nLimit, $nLimit);

            $oGroupDocument = new \Illuminate\Pagination\LengthAwarePaginator($oGroupDocument, $nTotal, $nLimit, $nPage, [
                                                                            'path' => $oRequest->url(),
                                                                            'pageName' => 'page',
                                                                        ]);

            foreach($oGroupDocument as $Key=>$oDocument)
            {
                $oGroupDocument[$Key]['can_delete'] = !Document::isChildShareorNot($oDocument);
                $oGroupDocument[$Key]['count_group'] = GroupDocument::getCountGroupDocument($oDocument->id_document);
                $oGroupDocument[$Key]['group_document'] = GroupDocument::getGroupDocument($oDocument->id_document,0,3);
                $oGroupDocument[$Key]['count_user'] = SharedDocument::countSharedUserList($oDocument->id_document);           
                if(count($oGroupDocument[$Key]['group_document']) < 3 )
                {
                    $nUserTake = 3-$oGroupDocument[$Key]['count_group'];
                    //$nUserTake = $oDocuments[$Key]['count_user']-$nUserSkip;
                    $oGroupDocument[$Key]['userList'] = SharedDocument::sharedUserList($oDocument->id_document,0,$nUserTake);
                }
            }

            if($oGroupDocument->currentPage() > 1)
            {
                return \View::make('WebView::documents._more_group_document', compact('oGroupDocument','oGroup','nIdParent'));
            }

            $html['CreateFolderHtml'] = '';
            if(Session::get('current_permission') != config('constants.DOCUMENTPERMISSIONTYPEREAD'))
            {
                $html['CreateFolderHtml'] = view('WebView::documents._create_folder_tab')->render();
            }

            $html['DocumentList'] = view('WebView::documents._group_document',compact('oGroupDocument', 'aBreadCrumb', 'nIdParent','sSharedWith','oGroup',
                                                                                    'sOrderBy','sOrderField','sSearchStr'))->render();

            return response()->json( array('success' => true, 'html'=>$html) );
        }
        else
            return redirect()->back();
    }

    //document share with user and group
    public function callDocumentShare(Request $oRequest)
    {        
        $nIdDoc = isset($oRequest->id_document) ? $oRequest->id_document : '';
        $sPermission = isset($oRequest->permission) ? $oRequest->permission : config('constants.DOCUMENTPERMISSIONTYPEREAD');
        $oDocument = Document::find($nIdDoc);
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequireFor = [
                                        'share_member_list' => 'required'
                                    ];
            $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor);
            if($oValidator->fails() == 1)
            {
                return redirect('documents/document-share?id_document='.$oRequest->id_document)->withErrors($oValidator)->withInput();
            }
            $aUserList = array();
            if(count($oRequest->share_member_list))
            {
                foreach($oRequest->share_member_list as $sInviteMember)
                {
                    $aInviteMember = explode('_',$sInviteMember);
                    switch ($aInviteMember[0])
                    {
                        case 'GM':
                            $this->addGroupDocument($oRequest->id_document, $aInviteMember[1], Auth::user()->id_user,$sPermission);
                            break;
                        case 'U' :
                            $this->addUserDocument($oRequest->id_document, $aInviteMember[1], Auth::user()->id_user,$sPermission);
                            array_push($aUserList, $aInviteMember[1]);
                            break;
                        default :
                            break;
                    }
                }

                if(count($aUserList))
                {
                    $this->dispatch(new SendDocumentSharedNotification($oRequest->id_document,Auth::user()->id_user,$sPermission, $aUserList ));
                }
                
                return 'redirect:to:'.url('documents/document-listing/'.$oDocument->id_parent);
            }
        }
        $oSharedDocumentUserList='';
        
            $oCountGroup = GroupDocument::getCountGroupDocument($nIdDoc);
            $oSharedGroupList = GroupDocument::getGroupDocument($nIdDoc,0,$oCountGroup);
            foreach($oSharedGroupList as $key=>$oSharedGroup){
                $sGroupName = $oSharedGroup->group_name.' ';
                if($oSharedGroup->section != '')
                $sGroupName .= trans('messages.section').'-'.$oSharedGroup->section.'('.$oSharedGroup->semester.')';
                
                $oSharedGroupList[$key]->group_name = $sGroupName;
                $oSharedGroupList[$key]->member_count = GroupMember::where('id_group', '=', $oSharedGroup->id_group)
                                                                    ->where('activated', '=', 1)
                                                                    ->where('deleted', '=', 0)
                                                                    ->count();
            }
            $oSharedDocumentUserList = SharedDocument::getShareDocUser($nIdDoc);
        return \View::make('WebView::documents.document_share', compact('oDocument','oSharedDocumentUserList','oSharedGroupList')); 
    }
    
    public function callGroupPostDocument(Request $oRequest,$nIdGroup)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = Group::find($nIdGroup);
        $oGroupListTab = GroupMember::getUserGroupsTab( Auth::user()->id_user );
        $oPostDocument = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        $sDocumentListing = "GroupPostDocumentList";
        if($oPostDocument->currentPage() > 1)
            return \View::make('WebView::documents._more_post_document', compact('oGroupDocument'));
        
        return \View::make('WebView::documents.document_listing', compact('oPostDocument', 'oGroup', 'oGroupListTab','sDocumentListing',
                                                                            'sOrderBy','sOrderField','sSearchStr'));
    }
    
    public function callGroupPostDocumentAjax(Request $oRequest,$nIdGroup)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroup = Group::find($nIdGroup);
        $oPostDocument = GroupPost::getGroupPost($nIdGroup, config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        
        $html['CreateFolderHtml'] = '';
        $html['DocumentList'] = view('WebView::documents._post_document', compact('oPostDocument', 'oGroup','sOrderBy','sOrderField','sSearchStr'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    public function callPostDocument(Request $oRequest)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oGroupListTab = GroupMember::getUserGroupsTab( Auth::user()->id_user );
        $oPostDocument = Post::getUserPostDocument(Auth::user()->id_user,config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        $sDocumentListing = "PostDocumentList";
        if($oPostDocument->currentPage() > 1)
            return \View::make('WebView::documents._more_post_document', compact('oGroupDocument'));
        
        return \View::make('WebView::documents.document_listing', compact('oPostDocument', 'oGroupListTab','sDocumentListing','sOrderBy','sOrderField','sSearchStr'));
    }
    
    public function callPostDocumentAjax(Request $oRequest)
    {
        $sOrderBy = (isset($oRequest->order_by)) ? $oRequest->order_by : 'desc';
        $sOrderField = (isset($oRequest->order_field)) ? $oRequest->order_field : 'updated_at';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';
        
        $oPostDocument = Post::getUserPostDocument(Auth::user()->id_user,config('constants.POSTTYPEDOCUMENT'),$sOrderBy,$sOrderField,$sSearchStr);
        
        $html['CreateFolderHtml'] = '';
        $html['DocumentList'] = view('WebView::documents._post_document', compact('oPostDocument','sOrderBy','sOrderField','sSearchStr'))->render();
        return response()->json( array('success' => true, 'html'=>$html) );
    }
    
    // share with user or group list suggestion 
    public function callGetSearchForShare(Request $oRequest)
    {
        $aResult = array();
        $oGroupList = $this->getGroupList($oRequest->search_str,$oRequest->doc_id);
        foreach($oGroupList as $nKey =>$oGroup)
        {
            if($oGroup->section !='')
               $oGroupList[$nKey]->group_name = $oGroup->group_name.' '.trans('messages.section').'-'.$oGroup->section.'('.$oGroup->semester.')';
            
            $oGroupList[$nKey]->group_img = setGroupImage($oGroup->file_name,$oGroup->group_category_image);
        }
        $aResult['group_list']=$oGroupList;
        $oUserList = SharedDocument::getUserSuggetionsForShare($oRequest->search_str,$oRequest->doc_id,Auth::user()->id_campus);
        foreach($oUserList as $nKey=>$oUser)
        {
            $oUserList[$nKey]->user_img = setProfileImage("50",$oUser->user_profile_image,$oUser->id_user,$oUser->first_name,$oUser->last_name); 
        }
        $aResult['user_connected_list'] = $oUserList;
        return array('status'=>'1' ,'msg'=>'' ,'data'=>$aResult);
    }
    
    // group suggestion for share document
    function getGroupList($sSearchStr,$nDocId)
    {
        $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
        
        $oGroupList = Group::getGroupShareSearch($sSearchStr, $aUserGroups, 10,$nDocId);
        return $oGroupList->all();
    }
    
    public function callDocumentPermissionChange(Request $oRequest) 
    {
        if($oRequest->isMethod("POST"))
        {
            $IdDocumentPermission = explode('_',$oRequest->id_doc_per);
            if(in_array('G', $IdDocumentPermission)){
                $oGroupDocument = GroupDocument::find($IdDocumentPermission[0]);
                $oGroupDocument->group_document_permission = $oRequest->permission;
                $oGroupDocument->save();
                $oDocumentPermisssion = DocumentPermisssion::where('id_group','=', $oGroupDocument->id_group)
                                                            ->where('id_document','=', $oGroupDocument->id_document)
                                                            ->update(['permission_type' => $oRequest->permission]);
            }
            else
            {
                $oDocumentPermisssion = DocumentPermisssion::find($IdDocumentPermission[0]);
                $oDocumentPermisssion->permission_type = $oRequest->permission;
                $oDocumentPermisssion->save();
            }
            
        }
    }
    
    public function callDeleteDocument($nIdDocument)
    {
        $oDocument = Document::find($nIdDocument);
        if($oDocument->id_user == Auth::user()->id_user)
        {        
            Document::where('lft' ,'>=', $oDocument->lft)
                    ->where('rgt' ,'<=', $oDocument->rgt)
                    ->update(['deleted' => 1, 'activated' => 0]);
        }
    }
    
    public function getGroupDocumentMember($nIdGroupDocument)
    {
        $oGroupDocument = GroupDocument::find($nIdGroupDocument);
        $oGroupMember = GroupDocument::getAllGroupMemberPermission($oGroupDocument->id_group,$oGroupDocument->id_document,Auth::user()->id_user);
        
        $view = \View::make('WebView::documents._group_document_shared_member', compact('oGroupMember'));
        $html = $view->render();
        return Response::json(['success' => true, 'html' => $html]);
                                            
    }
    
    public function getGroupChildDocument(Request $oRequest,$nIdDocument)
    {
        $sDataFrom = $oRequest->sDataFrom;
        $oGroupDocument = Document::getGroupChildDocument($nIdDocument);
        
        foreach($oGroupDocument as $key=>$oDocument)
        {
            $oCurrentPer = Document::countAncestorsWithWritePermission($oDocument,Auth::user()->id_user);
            if($oCurrentPer > 0 )
            {
                $oGroupDocument[$key]->permission_type = config('constants.DOCUMENTPERMISSIONTYPEWRITE');
            }
        }
        $view = \View::make('WebView::documents._copy_document_in_group', compact('oGroupDocument','sDataFrom'));
        $html = $view->render();
        return Response::json(['success' => true, 'html' => $html]);
    }
    
    public function CheckDocumentAccessRights($nIdDocument,$sPermission='')
    {   
        if( $nIdDocument =='')
        {
            return 1;
        }
        else
        {
            $oDocument = Document::find($nIdDocument);
            $nCount = Document::checkDocumentPermission($oDocument, Auth::user()->id_user, $sPermission);
            return $nCount;
        }
    }

    //delete document access rights
    public function callRevokeDocumentAccess(Request $oRequest)
    {
        if($oRequest->entity_type == 'G')
        {
            GroupDocument::where('id_group',$oRequest->id_entity)
                          ->where('id_document',$oRequest->id_document)
                          ->update(['activated' => 0,'deleted' => 1]);
            DocumentPermisssion::where('id_group',$oRequest->id_entity)
                                ->where('id_document',$oRequest->id_document)
                                ->update(['activated' => 0,'deleted' => 1]);
            
        }
        else if($oRequest->entity_type == 'U')
        {
            SharedDocument::where('shared_with',$oRequest->id_entity)
                          ->where('id_document',$oRequest->id_document)
                          ->update(['activated' => 0,'deleted' => 1]);
            DocumentPermisssion::whereNull('id_group')
                                ->where('id_user',$oRequest->id_entity)
                                ->where('id_document',$oRequest->id_document)
                                ->update(['activated' => 0,'deleted' => 1]);
            
        }
        $oRequest->session()->flash('success_message', trans('messages.success_revoke_permission'));
        return 'success';
    }
}