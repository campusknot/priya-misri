<?php 

namespace App\Http\Web\Controllers;

use App\Libraries\EventFeed;
use App\Libraries\User;
use App\Libraries\Event;
use App\Libraries\EventRequest;
use App\Libraries\GroupEvent;
use App\Libraries\GroupMember;
use App\Libraries\Group;
use App\Libraries\GoogleApiAccessToken;

use App\Http\Web\Controllers\Controller;
use App\Http\Web\Requests\AddEventFeedRequest;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Response;

use App\Jobs\SendEditEventNotification;
use App\Jobs\SyncGoogleCalendarData;
use App\Jobs\SendEventInvitation;
use App\Jobs\SendCancelEventNotification;
use App\Jobs\DisconnectGoogleCalendar;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => [
                                                'callAddEvent', 'callEventListing',
                                                'callFilterEventListing',
                                                'callEventDetail', 'callSetEventStatus',
                                                'callEventMembersByStatus',
                                                'callDeleteEvent',
                                                'callGetSearchForInvite','callEventInviteMember','callEventInviteMemberSearch',
                                                'callEventInviteMemberRequest','callPostEventFeed','callAddGoogleCredential',
                                                'callGoogleDisconnect','getGoogleEvents','callGoogleCronRun'
                                            ]
                        ]);
        
        parent::__construct();
        session(['current_page' => 'events']);
        $aCustomCookies['event'] = TRUE;
        Cookie::queue(Cookie::forever('event', json_encode($aCustomCookies)));
    }
    
    //POST Event Add New  Function
    public function callAddEvent(Request $oRequest)
    {
        $oGroup = '';
        if(mb_strpos($_SERVER['HTTP_REFERER'],'group-events') > 0)
        {
            $aUrl = explode('/', $_SERVER['HTTP_REFERER']);
            $nIdGroup = $aUrl[count($aUrl)-1];
            $oGroupMember = GroupMember::getGroupMemberDetail($nIdGroup,Auth::user()->id_user);
            if(count($oGroupMember))
            $oGroup = Group::find($nIdGroup);
        }
        $oEvent = (isset($oRequest->id_event)) ? Event::find($oRequest->id_event) : new Event();
        if((count($oEvent) && $oEvent->id_user == Auth::user()->id_user) || empty($oRequest->id_event))
        {
            if ($oRequest->isMethod("POST")) 
            {
                $dToday = Carbon::now();
                $sToday = $dToday->toDateTimeString();
                
                //this condition add for time like 00:15
                if($oRequest->start_ampm == 'am' && $oRequest->start_hour == 12){
                    $oRequest->offsetSet('start_hour', '00' );
                }
                if($oRequest->end_ampm == 'am' && $oRequest->end_hour == 12){
                    $oRequest->offsetSet('end_hour', '00' );
                }
                    
                $sStartDate = $oRequest->start_date;
                $aStartDateData = explode('-',$sStartDate);
                if(count($aStartDateData) && $sStartDate!='')
                {
                    $dStartDate = Carbon::create($aStartDateData[2], $aStartDateData[0], $aStartDateData[1], 
                                                    !empty($oRequest->start_hour) ? $oRequest->start_hour : '00',
                                                    !empty($oRequest->start_minute) ? $oRequest->start_minute : '00','00',Auth::user()->timezone);
                    if($oRequest->start_ampm == 'pm' && $oRequest->start_hour != 12){
                        $dStartDate->addHour(12);
                    }
                    $dStartDate->setTimezone('UTC');
                    $oRequest->offsetSet('start_date', $dStartDate->toDateTimeString() );
                }
                $dEndDate = NULL;
                $sEndDate = $oRequest->end_date;
                $aEndDateData = explode('-',$sEndDate);

                if(count($aEndDateData)>1)
                {
                    $dEndDate = Carbon::create($aEndDateData[2], $aEndDateData[0], $aEndDateData[1],
                                                    !empty($oRequest->end_hour) ? $oRequest->end_hour : '00',
                                                    !empty($oRequest->end_minute) ? $oRequest->end_minute : '00','00',Auth::user()->timezone);

                    if($oRequest->end_ampm == 'pm' && $oRequest->end_hour != 12){
                        $dEndDate->addHour(12);
                    }
                    $dEndDate->setTimezone('UTC');
                    $dEndDate->toDateTimeString();
                }
                $oRequest->offsetSet('end_date', $dEndDate );
                $aValidationRequireFor = [
                                            'event_title' => 'required|max:128',
                                            'address' => 'required',
                                            'start_date' => 'required|date',
                                            'start_hour' => 'required',
                                            'start_minute' => 'required',
                                            'end_date' => 'sometimes|after:"'.$oRequest->start_date.'"',
                                            'end_hour' => 'required_with:end_date',
                                            'end_minute' => 'required_with:end_date'
                                        ];

                $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor );
                if ($oValidator->fails()) {
                    return redirect('/event/add-event')->withErrors($oValidator)->withInput();
                }

                $oCreatedEvent = Event::firstOrNew([
                                                        'id_event' => $oRequest->id_event
                                                    ]);
                
                $oCreatedEvent->id_user = Auth::user()->id_user;
                $oCreatedEvent->event_title = $oRequest->event_title;
                $oCreatedEvent->event_description = $oRequest->event_description;
                $oCreatedEvent->start_date = $oRequest->start_date;
                $oCreatedEvent->end_date = $oRequest->end_date;
                $oCreatedEvent->room_number = $oRequest->room_number;
                $oCreatedEvent->address = $oRequest->address;
                $oCreatedEvent->latitude = $oRequest->latitude;
                $oCreatedEvent->longitude = $oRequest->longitude;
                $oCreatedEvent->save();

                if($oCreatedEvent)
                {
                    if(!empty($oRequest->id_event))
                    {
                        $dToday = Carbon::today();
                        if($oCreatedEvent->start_date > $dToday)
                        {
                            $oEventMembers = EventRequest::getAllInvitedUserList($oRequest->id_event);
                            $this->dispatch(new SendEditEventNotification(Auth::User(), $oCreatedEvent, $oEventMembers));
                        }
                    }
                    else
                    {
                        $this->addEventRequest($oCreatedEvent->id_event, Auth::user()->id_user, Auth::user()->id_user, 'G');
                    }
                    if( $oRequest->invite_member_list)
                    {
                        $this->addGroupEvent($oCreatedEvent->id_event, $oRequest->invite_member_list);
                        $this->dispatch(new SendEventInvitation($oRequest->invite_member_list, $oCreatedEvent->id_event, Auth::user()->id_user));
                    }
                }
                if(mb_strpos($_SERVER['HTTP_REFERER'],'group-events') > 0){
                    return 'redirect:to:'.$_SERVER['HTTP_REFERER'];
                }
                else
                {            
                    return 'redirect:to:'.url('event/event-listing/');
                }
            }
            return \View::make('WebView::event._add_event_model', compact('oEvent','oGroup'));
        }
        else
        {
            return Response::json(['success' => false,'msg'=>trans('messages.authentication_event_error') ]);
        }
        
    }
    
    private function addGroupEvent($nIdEvent, $aInviteMemberList)
    {
        foreach($aInviteMemberList as $sInviteMember)
        {
            $aInviteMember = explode('_',$sInviteMember);
            if ($aInviteMember[0] == 'GM')
            {
                $aGroupEvent = array();
                $aGroupEvent['id_event'] = $nIdEvent;
                $aGroupEvent['id_group'] = $aInviteMember[1];

                GroupEvent::firstOrCreate($aGroupEvent);
            }
        }
    }
    
    private function addEventRequest($nIdEvent, $nIdUserTo, $nIdUserFrom, $sStatus=NULL)
    {
        $oUser = new User();
        $oInvitedUserDetail = $oUser->getUserDetail('id_user', $nIdUserTo);
        $oInvityUserDetail = $oUser->getUserDetail('id_user', $nIdUserFrom);
        
        $oInvitedUserName = $oInvitedUserDetail->first_name.' '.$oInvitedUserDetail->last_name;
        $oInvitedUserEmail = $oInvitedUserDetail->email;
        $oInvityUserName = $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name;
        
        $oEvent= Event::find($nIdEvent);
        
        if($nIdUserFrom != $nIdUserTo && $oInvitedUserDetail != null && $oInvityUserDetail != null)
        {
            Mail::queue('WebView::emails.event_invitation', ['sFirstName'=>$oInvitedUserDetail->first_name, 'sInvitedBy' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name, 'sEventName'=>$oEvent->event_title], function ($oMessage) use ($oInvityUserName, $oInvitedUserName,$oInvitedUserEmail) {
                $oMessage->from(config('mail.from.address'),$oInvityUserName);

                $oMessage->to($oInvitedUserEmail, $oInvitedUserName)
                        ->subject(trans('messages.event_invitation_subject'));
            });
        }
        $oEventRequest = EventRequest::firstOrNew([
                                                    'id_event' => $nIdEvent,
                                                    'id_user_request_to' => $nIdUserTo
                                                ]);
        $oEventRequest->id_user_request_from = $nIdUserFrom;
        $oEventRequest->status = ($oEventRequest->status != NULL) ? $oEventRequest->status : $sStatus;
        $oEventRequest->save();
    }
    
    // Event Home Page  Function
    public function callEventListing( Request $oRequest, $sSearchParam='' )
    {
        // Google calendar data api
        $oGoogleApiAccessTocken = GoogleApiAccessToken::where('id_user' , Auth()->user()->id_user)
                                                        ->first();
        if(count($oGoogleApiAccessTocken))
        { 
            $this->getGoogleEvents();
            $dTimeMin = Carbon::today();
            $sTimeMin = $dTimeMin->toAtomString(); //'2016-11-12T00:00:00-00:00'

            $dTimeMax = Carbon::now()->addMonths(12);
            $sTimeMax = $dTimeMax->toAtomString(); //'2017-01-11T00:00:00-00:00'
            
            
            $this->dispatch(new SyncGoogleCalendarData(Auth::User()->id_user,$sTimeMin,$sTimeMax));
        }
        else
        {
            $oClient = new \Google_Client();
            $oClient->setAuthConfig( config('constants.client_secret_path') );
            $oClient->setApplicationName(config('constants.GOOGLECALENDARAPPNAME'));
            $oClient->setApprovalPrompt('force');
            $oClient->setAccessType('offline');
            $oClient->setRedirectUri(url('/event/google-credential'));
            $oClient->setDeveloperKey(config('constants.GOOGLEDEVELOPERKEY'));
            $oClient->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
            $gAuthUrl = $oClient->createAuthUrl();
        }
         // End Google calendar data api
        $oGroupList = GroupMember::getUserGroupsTab(Auth::user()->id_user);
        $oGoogleCalendarList=Event::getGoogleCalendarList(Auth::user()->id_user);
        
        $aGroupIds = array();
        foreach ($oGroupList as $oGroup)
        {
            $aGroupIds[] = $oGroup->id_group;
        }
        
        if( isset($oRequest->fromDate) && !empty($oRequest->fromDate))
        {
            $dStartDate = Carbon::createFromFormat("Y-m-d H:i:s", $oRequest->fromDate, Auth::user()->timezone);
            $dStartDate->setTimezone('UTC');
            $sStartDate = $dStartDate->toDateTimeString();
        }
        else
        {
            $sStartDate = Carbon::today(Auth::user()->timezone);
            $sStartDate->setTimezone('UTC');
        }
        $oUserEventList = Event::getUserEventList(Auth::user()->id_user, $sStartDate);
        
        $nCount = 0;
        foreach ($oUserEventList as $oUserEvent)
        {
            $oEventGroups = GroupEvent::getEventGroups($oUserEvent->id_event, $aGroupIds);
            $oUserEvent->id_groups = $oEventGroups[0]->id_groups;
            $oUserEvent->group_names = $oEventGroups[0]->group_names;
            
            $oUserEventList[$nCount] = $oUserEvent;
            $nCount ++;
        }
        
        return \View::make('WebView::event.event_list', compact('oUserEventList','oGroupList','gAuthUrl','oGoogleCalendarList'));
    }

    public function callFilterEventListing(Request $oRequest)
    {        
        $nIdGroup = (isset($oRequest->id_group)) ? $oRequest->id_group : '';
        $sSearchStr = (isset($oRequest->search_str)) ? $oRequest->search_str : '';                
        
        $oGroupIds = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aGroupIds = explode(",", $oGroupIds[0]->id_groups);
        
        if( isset($oRequest->start_date) && !empty($oRequest->start_date))
        {
            $dStartDate = Carbon::createFromFormat("Y-m-d", $oRequest->start_date, Auth::user()->timezone);
            $dStartDate->setTimezone('UTC');
            $sStartDate = $dStartDate->toDateTimeString();
        }
        else
        {
            $sStartDate = Carbon::today(Auth::user()->timezone);
            $sStartDate->setTimezone('UTC');
        }
        $oUserEventList = Event::getUserEventList(Auth::user()->id_user, $sStartDate, $nIdGroup,$sSearchStr);
        $nCount = 0;

        foreach ($oUserEventList as $oUserEvent)
        {
            $oEventGroups = GroupEvent::getEventGroups($oUserEvent->id_event, $aGroupIds);
            $oUserEvent->id_groups = $oEventGroups[0]->id_groups;
            $oUserEvent->group_names = $oEventGroups[0]->group_names;

            $oUserEventList[$nCount] = $oUserEvent;
            $nCount ++;
        }
        // google evet api job call
        $oGoogleApiAccessTocken = GoogleApiAccessToken::where('id_user' , Auth()->user()->id_user)
                                          ->first();
        if(count($oGoogleApiAccessTocken))
        {
            if($sStartDate != '')
            {                
                $dTimeMin = Carbon::createFromFormat('Y-m-d H:i:s', $sStartDate);

                $sTimeMin = $dTimeMin->toAtomString(); //'2016-11-12T00:00:00-00:00'

                $dTimeMax = Carbon::now()->addMonths(12);
                $sTimeMax = $dTimeMax->toAtomString(); //'2017-01-11T00:00:00-00:00' 
                $this->dispatch(new SyncGoogleCalendarData(Auth::User()->id_user,$sTimeMin,$sTimeMax) );
            } 
        }
        return \View::make('WebView::event._event_list', compact('oUserEventList'));
    }

    //AJAX Event Detail Function
    public function callEventDetail(Request $oRequest , Event $oEvent)
    {
        $oGroupIds = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aGroupIds = explode(",", $oGroupIds[0]->id_groups);
        
        $oEventDetail = Event::getEventDetail($oEvent->id_event);
        if(count($oEventDetail))
        {
        $oEventGroups = GroupEvent::getEventGroups($oEventDetail->id_event, $aGroupIds);
        $oEventDetail->id_groups = $oEventGroups[0]->id_groups;
        $oEventDetail->group_names = $oEventGroups[0]->group_names;
        
        $oEventDetail->is_admin = Event::isUserEventAdmin(Auth::user()->id_user, $oEventDetail->id_user );
        }
        return \View::make('WebView::event._event_detail', compact('oEventDetail'));
    }

    //AJAX Event Request Status Change Function
    public function callSetEventStatus(Request $oRequest , EventRequest $oEventRequest)
    {
        if($oEventRequest->id_user == Auth::User()->id_user)
        {
            $oEventRequest->status = $oRequest->status;
            $oEventRequest->update();
            return array('status'=>'1' ,'msg'=>'status has been set');
        }else{
            return array('status'=>'0' ,'msg'=>'un authorize call');
        }
    }
    
    //AJAX Event Remove Function
    public function callDeleteEvent(Request $oRequest , $nIdEvent)
    {
        $oEvent = Event::getEventDetail($nIdEvent);
        
        if($oEvent->id_user == Auth::User()->id_user)
        {
            $oEvent->activated = 0;
            $oEvent->deleted = 1;
            $oEvent->update();
            
            $oRequest->session()->flash('success_message', trans('messages.event_delete'));
            
            $oEventMembers = EventRequest::getAllInvitedUserList($nIdEvent);
            
            $this->dispatch(new SendCancelEventNotification(Auth::User(), $oEvent, $oEventMembers));
            return 'redirect:to:'.url('event/event-listing');
        }
        else {
            return trans('messages.not_allow_to_delete_event');
        }
        return $oEvent;
    }

    //AJAX Event Going / NotGoing User List
    public function callEventMembersByStatus($nIdEvent, $sStatus)
    {
        $oEventMembers = EventRequest::getRespondedUserList($nIdEvent, array($sStatus));
        if($oEventMembers->currentPage() > 1)
            return \View::make('WebView::event._more_member_list', compact('oEventMembers'));
        
        return \View::make('WebView::event._event_members_list', compact('oEventMembers'));
    }

    //AJAX Start:Search Group/User For Invite
    public function callGetSearchForInvite(Request $oRequest)
    {
        $aResult = array();
        $aResult['group_list'] = $this->getGroupList($oRequest->search_str);
        $aResult['user_connected_list'] = $this->getUserConnectedList($oRequest->search_str);
        $aResult['user_other_list'] = $this->getUserOtherList($oRequest->search_str);

        return array('status'=>'1' ,'msg'=>'' ,'data'=>$aResult);
    }

    private function getGroupList($sSearchStr)
    {
        $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
        $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
        
        $oGroupList = Group::getGroupSearchEvant($sSearchStr, $aUserGroups, 10);
        foreach($oGroupList as $nKey => $oGroup)
        {
            if($oGroup->section !='')
               $oGroupList[$nKey]->group_name =  $oGroup->group_name.' '.trans('messages.section').'-'.$oGroup->section.'('.$oGroup->semester.')';
            $oGroupList[$nKey]->group_img = setGroupImage($oGroup->file_name,$oGroup->group_category_image);
        }
        return $oGroupList->all();
    }

    private function getUserConnectedList($sSearchStr)
    {
        $oUserList = User::getConnectedUserSearch($sSearchStr , Auth::user()->id_user , 10);
        foreach($oUserList as $nKey=>$oUser)
        {
            $oUserList[$nKey]->user_img = setProfileImage("50",$oUser->user_profile_image,$oUser->id_user,$oUser->first_name,$oUser->last_name); 
        }
        return $oUserList->all();
    }

    private function getUserOtherList($sSearchStr)
    {
        $oUserList = User::getNotConnectedUserSearch($sSearchStr , Auth::user()->id_user , 10);
        foreach($oUserList as $nKey=>$oUser)
        {
            $oUserList[$nKey]->user_img = setProfileImage("50",$oUser->user_profile_image,$oUser->id_user,$oUser->first_name,$oUser->last_name); 
        }
        return $oUserList->all();
    }
    // End:Search Group/User For Invite
    
    /* invite member lightbox ajax*/
    public function callEventInviteMember(Request $oRequest) 
    {        
        $IdEvent = isset($oRequest->id_event) ? $oRequest->id_event : '';
        $oEvent = Event::find($IdEvent);
        if($oRequest->isMethod("POST"))
        {
            $aValidationRequireFor = [
                                        'invite_member_list' => 'required'
                                    ];
            
            $oValidator = Validator::make($oRequest->all(), $aValidationRequireFor );
            if ($oValidator->fails())
            {
                return redirect('/event/invite-member?id_event='.$oRequest->id_event)->withErrors($oValidator)->withInput($oRequest->all());
            }
            $this->dispatch(new SendEventInvitation($oRequest->invite_member_list, $oRequest->id_event, Auth::user()->id_user));
            
            $oRequest->session()->flash('success_message', trans('messages.event_invite_member'));
            return 'redirect:to:'.url()->previous();
        }
        return \View::make('WebView::event._invite_member', compact('oEvent'));
    }

    public function callEventInviteMemberSearch(Request $oRequest)
    {
        $aResult['user_connected_list'] = $this->getUserConnectedList($oRequest->search_str);
        $aResult['user_other_list'] = $this->getUserOtherList($oRequest->search_str);
        return array('status'=>'1' ,'msg'=>'' ,'data'=>$aResult);
    }

    public function callEventInviteMemberRequest(Request $oRequest)
    {
        if( $oRequest->invite_member_list)
            $this->doEventRequest($oRequest->invite_member_list, $oRequest->id_event, Auth::user()->id_user );
        return redirect()->back()->with('success_message', trans('messages.event_invite_member'));
    }
    
    /* ajax invite member */
    public function callPostEventFeed( AddEventFeedRequest $oRequest )
    {
        $sFileName = '';
        if(isset($oRequest->post_type) && $oRequest->post_type == 'I') {
            $oUploadedFile = $oRequest->file('file');
            $sOriginalName = $oUploadedFile->getClientOriginalName();
            $sDestinationPath = base_path().'/uploads/'; // upload path
            $sExtension = $oUploadedFile->getClientOriginalExtension(); // getting image extension
            $sFileName = str_random(8).'_'.time().'.'.$sExtension; // renameing image
            $oUploadedFile->move($sDestinationPath, $sFileName); // uploading file to given path
        }
        $oCreatedFeed = EventFeed::create([
                    'id_entity' => $oRequest->id_event,
                    'id_user' => Auth()->user()->id_user,
                    'entity_type'=>'E',
                    'comment_text' => $oRequest->post_text,
                    'comment_type' => $oRequest->post_type,
                    'file_name'=> $sFileName
                ]);
        return Redirect::to('event/event-listing');
    }
    
    public function callAddGoogleCredential() 
    {
        if (isset($_GET['code'])) 
        {
            $oClient = new \Google_Client();
            $oClient->setAuthConfig( config('constants.client_secret_path') );
            $oClient->setApplicationName(config('constants.GOOGLECALENDARAPPNAME'));

            $oClient->setApprovalPrompt('force');
            $oClient->setAccessType('offline');
            $oClient->setRedirectUri(url('/event/google-credential'));
            $oClient->setDeveloperKey(config('constants.GOOGLEDEVELOPERKEY'));
            
                        
            $oClient->authenticate($_GET['code']);
            $aToken = $oClient->getAccessToken();
            $oGoogleApiAccessTocken = GoogleApiAccessToken::firstOrNew([
                                                        'id_user' => Auth()->user()->id_user
                                                    ]);
            $oGoogleApiAccessTocken->access_token=$aToken['access_token'];
            $oGoogleApiAccessTocken->refresh_token=$aToken['refresh_token'];
            $oGoogleApiAccessTocken->token_type=$aToken['token_type'];
            $oGoogleApiAccessTocken->created=$aToken['created'];
            $oGoogleApiAccessTocken->expires_in=$aToken['expires_in'];
            $oGoogleApiAccessTocken->save();
            
            $dTimeMin = Carbon::today();
            $sTimeMin = $dTimeMin->toAtomString(); //'2016-11-12T00:00:00-00:00'

            $dTimeMax = Carbon::now()->addMonths(6);
            $sTimeMax = $dTimeMax->toAtomString(); //'2017-01-11T00:00:00-00:00'
            
            
            $this->dispatch(new SyncGoogleCalendarData(Auth::User()->id_user,$sTimeMin,$sTimeMax) );
            
            return redirect('/event/event-listing');
        }
    }
    
    public function callGoogleDisconnect()
    {
        GoogleApiAccessToken::where([
                                        'id_user' => Auth()->user()->id_user
                                    ])->delete();
        Event::where([ 'id_user' => Auth::User()->id_user,
                        'event_from' => config('constants.EVENTTYPEGOOGLE')
                    ])->update(['deleted' => 1,'activated' => 0]);
        $this->dispatch( new DisconnectGoogleCalendar(Auth::User()->id_user ));
        return 'redirect:to:'.url('/event/event-listing');
        
    }
    
    private function getGoogleEvents()
    {
    
        $oGoogle = new GoogleApiAccessToken();
        $oGoogleApiAccessTocken = $oGoogle->where('id_user' , Auth()->user()->id_user)
                                          ->first();
        $oClient = new \Google_Client();
        $oClient->setAuthConfig( config('constants.client_secret_path') );
        $oClient->setApplicationName(config('constants.GOOGLECALENDARAPPNAME'));
        $oClient->setApprovalPrompt('force');
        $oClient->setAccessType('offline');
        $oClient->setRedirectUri(url('/event/google-credential'));
        $oClient->setDeveloperKey(config('constants.GOOGLEDEVELOPERKEY'));
        $oClient->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
        if(count($oGoogleApiAccessTocken))
        { 
            $aGData['access_token'] = $oGoogleApiAccessTocken['access_token'];
            $aGData['refresh_token'] = $oGoogleApiAccessTocken['refresh_token'];
            $aGData['created'] = $oGoogleApiAccessTocken['created'];
            $aGData['token_type'] = $oGoogleApiAccessTocken['token_type'];
            $aGData['expires_in'] = $oGoogleApiAccessTocken['expires_in'];
            $oClient->setAccessToken($aGData);
            
            if ($oClient->isAccessTokenExpired() == 1 && isset($aGData)) 
            {
                $oClient->refreshToken($aGData['refresh_token']);
            }
            $aToken = $oClient->getAccessToken();
            if(!isset($aToken['refresh_token']))
            {
                $aToken['refresh_token'] = $aGData['refresh_token'];
            }

            if (count($aToken) && isset($aGData)) 
            {
                $oCalendar = new \Google_Service_Calendar($oClient);
                $oCalList = $oCalendar->calendarList->listCalendarList();
                
               
                foreach($oCalList->getItems() as $calendar)
                {
                    if(mb_strpos( $calendar['id'],'holiday') === false && mb_strpos( $calendar['id'], 'contacts') === false)
                    {
                    $dTimeMin = Carbon::today();
                    $sTimeMin = $dTimeMin->toAtomString(); //'2016-11-12T00:00:00-00:00'
                    $events = $oCalendar->events->listEvents($calendar['id'],array('timeMin'=>$sTimeMin,'orderBy'=>'updated','maxResults'=>15,'singleEvents'=> 'true') );
                    
                    foreach($events->getItems() as $event)
                    {                        
                        if($event['recurringEventId'])
                                Event::where('id_external_event',$event['recurringEventId'])->update(['deleted' => 1,'activated' => 0]);
                        
                        $external_event_id = $event->getId();
                        $dStartTime = $event->getStart()->getDateTime();
                        if($dStartTime == '') 
                            $dStartTime = $event->getStart()->getDate();
                        $dEndTime = $event->getEnd()->getDateTime();
                        if($dEndTime == '') 
                            $dEndTime = $event->getEnd()->getDate();
                        
                        $dstart = Carbon::parse($dStartTime);
                        $dEnd = Carbon::parse($dEndTime);
                        $sEventTitle = $event->getSummary();
                        $sEventDesc = $event->getDescription();
                        $sCalendarId = $calendar->getId();
                        $sCalendarName = $calendar->getSummary();
                        $sEventAddress=$event->getLocation();
                        $latitude=$longitude='';
                        if($sEventAddress!='')
                        {
                            $sPrepAddr = str_replace(' ','+',$sEventAddress);
                            $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$sPrepAddr.'&sensor=false');
                            $aGeocode= json_decode($geocode);
                            if(isset($aGeocode->results) && count($aGeocode->results))
                            {
                            $latitude = $aGeocode->results[0]->geometry->location->lat;
                            $longitude = $aGeocode->results[0]->geometry->location->lng;
                            }
                        }
                        else
                            $sEventAddress=' ';
                        if($event->getStatus() == 'cancelled')
                        {
                            Event::where('id_external_event', $external_event_id )->update(['deleted' => 1]);
                        }
                        elseif($sEventTitle != '')
                        {
                            $oCreatedEvent = Event::firstOrNew([
                                                            'id_external_event' =>$external_event_id,
                                                            'id_user' => Auth::user()->id_user
                                                        ]);
                            $oCreatedEvent->event_title = $sEventTitle;
                            $oCreatedEvent->event_description = $sEventDesc;
                            $oCreatedEvent->start_date = $dstart->toDateTimeString();
                            $oCreatedEvent->end_date = $dEnd->toDateTimeString();
                            $oCreatedEvent->address = $sEventAddress;
                            $oCreatedEvent->latitude = $latitude;
                            $oCreatedEvent->longitude = $longitude;
                            $oCreatedEvent->event_from = config('constants.EVENTTYPEGOOGLE');
                            $oCreatedEvent->id_calendar = $sCalendarId;
                            $oCreatedEvent->calendar_name = $sCalendarName;
                            $oCreatedEvent->activated = 1;
                            $oCreatedEvent->deleted = 0;
                            $oCreatedEvent->save();

                            if($oCreatedEvent)
                            {
                                $this->addEventRequest($oCreatedEvent->id_event, Auth::user()->id_user, Auth::user()->id_user, 'G');
                                
                            }
                        }
                    }
                             
                    }
                }
            }
            $oGoogleApiAccessTocken = GoogleApiAccessToken::firstOrNew([
                                                        'id_user' => Auth()->user()->id_user
                                                    ]);
            $oGoogleApiAccessTocken->access_token=$aToken['access_token'];
            $oGoogleApiAccessTocken->refresh_token=$aToken['refresh_token'];
            $oGoogleApiAccessTocken->token_type=$aToken['token_type'];
            $oGoogleApiAccessTocken->created=$aToken['created'];
            $oGoogleApiAccessTocken->expires_in=$aToken['expires_in'];
            $oGoogleApiAccessTocken->save();
        }
    }
    
    public function callGoogleCronRun($nPageId)
    {
        $oGoogleApiAccessTocken = GoogleApiAccessToken::where('id_user' , Auth()->user()->id_user)
                                          ->first();
        $oEvent = Event::where('event_from','=',config('constants.EVENTTYPEGOOGLE'))
                        ->where('id_user','=',Auth()->user()->id_user)
                        ->orderBy('start_date','desc')
                        ->first();
        if(count($oGoogleApiAccessTocken) && count($oEvent)){
            $dLastDate=Carbon::createFromFormat('Y-m-d H:i:s', $oEvent->start_date);

            $dTimeMin = Carbon::today()->addMonths(12);
            if($dLastDate > $dTimeMin)
                $dTimeMin = $dLastDate;

            $sTimeMin = $dTimeMin->toAtomString(); //'2016-11-12T00:00:00-00:00'

            $dTimeMax = $dTimeMin->addMonths(4);
            $sTimeMax = $dTimeMax->toAtomString(); //'2017-03-12T00:00:00-00:00'


            $this->dispatch(new SyncGoogleCalendarData(Auth::User()->id_user,$sTimeMin,$sTimeMax) );
        }
    }
}