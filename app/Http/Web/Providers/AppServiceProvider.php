<?php

namespace App\Http\Web\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::addNamespace('WebView', __DIR__.'/../Views/');
        
        Validator::extend('without_numeric', function($attribute, $value, $parameters) {
            $matches = NULL;
            preg_match_all('!\d+!', $value, $matches);
            
            if(empty($matches[0])) {
                return TRUE;
            }
            return FALSE;
        });
        
        Validator::extend('required_array', function($attribute, $value, $parameters) {
            if(is_array($value)) {
                foreach ($value as $item) {
                    if(mb_strlen(trim($item))) {
                        return TRUE;
                    }
                }
            }
            return FALSE;
        });
        
        Validator::extend('required_array_without', function($attribute, $value, $parameters, $validator) {
            $bValid = FALSE;
            $data = $validator->getData();
            foreach ($parameters as $parameter) {
                if(!empty($data[$parameter])) {
                    $bValid = TRUE;
                    break;
                }
            }
            
            if(!$bValid)
            {
                foreach ($value as $item) {
                    if(!empty(trim($item))) {
                        $bValid = TRUE;
                        break;
                    }
                }
            }
            
            return $bValid;
        });
        
        Validator::extend('array_min_size', function($attribute, $value, $parameters, $validator) {
            $aValue = array_filter($value, 'strlen' );
            if(count($aValue) >= $parameters[0]){
                return TRUE;
            }   
            $validator->addReplacer('array_min_size', function ($message, $attribute, $rule, $parameters) use ($value) {
                                return str_replace(':array_min_size', $parameters[0], $message);
                            });
        });
        
        Validator::extend('unique_array', function($attribute, $value, $parameters, $validator) {
            $aUniqueValue = array_unique($value);
            if(count($aUniqueValue) == count($value)){
                return TRUE;
            }
            return FALSE;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
