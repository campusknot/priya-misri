<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\MailQueue;

use Illuminate\Support\Facades\DB;
use App\Libraries\Document;
use App\Libraries\Group;
use App\Libraries\Notification;
use App\Libraries\DocumentPermisssion;

class SendNewCreatedDocumentNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected  $nIdDocument;
    /**
     * Create a new job instance.
     *
     * @return void
     *  If user create folder in a folder there is no entry in shared_document.
     *  send notification to all user where Document parent shared with.
     */
    public function __construct($nIdDocument)
    {
        $this->nIdDocument = $nIdDocument;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailQueue $oMailer)
    {
        $oDocument = Document::from( 'documents as d' )
                            ->leftJoin('users as u', 'd.id_user', '=', 'u.id_user')
                            ->where('id_document', '=', $this->nIdDocument)
                            ->select(
                                    'd.lft as lft',
                                    'd.rgt as rgt',
                                    'd.id_user as id_user',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    'd.shared_with as shared_with'
                                    )
                            ->first();
        
        $oIdDocuments = Document::from( 'documents as d' )
                                ->where('d.lft', '<', $oDocument->lft )
                                ->where('d.rgt', '>', $oDocument->rgt )
                                ->where('d.activated', '=', 1)
                                ->where('d.deleted', '=', 0)
                                ->select(
                                            DB::raw('GROUP_CONCAT(id_document) as id_documents')
                                        )
                                ->get();
        
        $oSharedUsers = DocumentPermisssion::from( 'document_permissions as dp' )
                              ->leftJoin('users as u', 'dp.id_user', '=', 'u.id_user')
                              ->whereIn('dp.id_document',explode(',', $oIdDocuments[0]->id_documents))
                              ->select(
                                    'dp.id_user as id_user',
                                    'dp.id_document as id_document',
                                    'dp.id_group as id_group',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    'u.email as email'
                                    )
                              ->groupBy('dp.id_user')
                              ->get();

        foreach($oSharedUsers as $oSharedUser)
        {
            if($oDocument->id_user != $oSharedUser->id_user && $oSharedUser->id_group == NULL)
            {
                Notification::firstOrCreate([
                                                    'id_user' => $oSharedUser->id_user,
                                                    'id_entites' =>  $oDocument->id_user.'_'.$this->nIdDocument,
                                                    'entity_type_pattern' =>  'U_D' , //U = user, G = Group, D = Document
                                                    'notification_type' =>  config('constants.NOTIFICATIONUSERDOCUMENT')
                                                ]);
//                $aMailParams = [
//                                'sFirstName' => $oSharedUser->first_name, 
//                                'sSharedBy' => $oDocument->first_name.' '.$oDocument->last_name, 
//                                'sSharedWith' => 'you'
//                            ];
                $sNotificationMessage = trans('messages.document_share_mail_body',['shared_by' => $oDocument->first_name.' '.$oDocument->last_name,'shared_with'=>'you']);

//                $oMailer->later(10,'WebView::emails.share_document', $aMailParams, 
//                                function ($oMessage) use ($oDocument, $oSharedUser) 
//                                {
//                                    $oMessage->from(config('mail.from.address'), $oDocument->first_name.' '.$oDocument->last_name);
//
//                                    $oMessage->to($oSharedUser->email, $oSharedUser->first_name.' '.$oSharedUser->last_name)
//                                            ->subject(trans('messages.document_share_subject'));
//                                });
            }
            else if($oDocument->id_user != $oSharedUser->id_user)
            {
                $oGroup = Group::find($oSharedUser->id_group);
                if($oGroup->id_group_category != config('constants.UNIVERSITYGROUPCATEGORY') )
                {
                    Notification::firstOrCreate([
                                                        'id_user' => $oSharedUser->id_user,
                                                        'id_entites' =>  $oDocument->id_user.'_'.$oSharedUser->id_group.'_'.$this->nIdDocument,
                                                        'entity_type_pattern' =>  'U_G_D' , //U = user, G = Group, D = Document
                                                        'notification_type' =>  config('constants.NOTIFICATIONGROUPDOCUMENT')
                                                    ]);
//                    $aMailParams = [
//                                    'sFirstName' => $oSharedUser->first_name, 
//                                    'sSharedBy' => $oDocument->first_name.' '.$oDocument->last_name, 
//                                    'sSharedWith' => 'you'
//                                ];
                    $sNotificationMessage = trans('messages.document_share_mail_body',['shared_by' => $oDocument->first_name.' '.$oDocument->last_name,'shared_with'=>$oGroup->group_name]);

//                    $oMailer->later(10,'WebView::emails.share_document', $aMailParams, 
//                                                function ($oMessage) use ($oDocument, $oSharedUser) 
//                                                {
//                                                    $oMessage->from(config('mail.from.address'), $oDocument->first_name.' '.$oDocument->last_name);
//
//                                                    $oMessage->to($oSharedUser->email, $oSharedUser->first_name.' '.$oSharedUser->last_name)
//                                                            ->subject(trans('messages.document_share_subject'));
//                                                });
                }
            }
        }
    }
}