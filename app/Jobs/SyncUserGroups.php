<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\UserSession;
use Davibennun\LaravelPushNotification\Facades\PushNotification;

class SyncUserGroups extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $nIdUser;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nIdUser)
    {
        $this->nIdUser = $nIdUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $aUserSessions = UserSession::where('id_user', '=', $this->nIdUser)
                                    ->where('logout_time', '=', '0000-00-00 00:00:00')
                                    ->get();
        
        foreach ($aUserSessions as $oUserSession)
        {
            if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
            {
                PushNotification::app($oUserSession->device_type.'OS')
                                    ->to($oUserSession->device_token)
                                    ->send("",array('badge'=>0,'sound'=>'','content-available'=>1,'custom'=>array('type'=>'sync_group')));
            }
        }
    }
}
