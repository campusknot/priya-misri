<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\DocumentPermisssion;
use App\Libraries\GroupDocument;

class ShareGroupDocumentsWithNewMember extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $oLoggedinUser, $nIdGroup;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser, $nIdGroup)
    {
        $this->oLoggedinUser = $oLoggedinUser;
        $this->nIdGroup = $nIdGroup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $aWhereParams = [
                            'd.activated' => 1,
                            'd.deleted' => 0,  
                            'gd.id_group' => $this->nIdGroup,
                        ];
        
        $oGroupDocuments = GroupDocument::from('group_documents as gd')      
                                ->join( 'documents as d','gd.id_document','=','d.id_document' )
                                ->where($aWhereParams)
                                ->select(
                                             'd.id_document as id_document',   
                                             'd.id_user as id_user',
                                             'gd.group_document_permission as group_document_permission'
                                        )
                                ->get();
        
        foreach($oGroupDocuments as $aGroupDocument)
        {
            $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                                                        'id_document' => $aGroupDocument->id_document,
                                                                        'id_user' => $this->oLoggedinUser,
                                                                        'id_group' =>$this->nIdGroup
                                                                    ]);
            if((isset($oDocumentPermisssion->activated) && isset($oDocumentPermisssion->deleted))
                    && ($oDocumentPermisssion->activated == 1 && $oDocumentPermisssion->deleted == 0))
            {
                $oDocumentPermisssion->permission_type = isset($oDocumentPermisssion->permission_type) ? $oDocumentPermisssion->permission_type : $aGroupDocument->group_document_permission;
            }
            else
            {
                $oDocumentPermisssion->permission_type = $aGroupDocument->group_document_permission;
            }
            $oDocumentPermisssion->activated = 1;
            $oDocumentPermisssion->deleted = 0;
            $oDocumentPermisssion->save();
        }
    }
}
