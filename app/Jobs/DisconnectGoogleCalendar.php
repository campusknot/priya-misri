<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\Event;

class DisconnectGoogleCalendar extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $oLoggedinUser;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser)
    {
        $this->oLoggedinUser = $oLoggedinUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Event::where([ 'id_user' => $this->oLoggedinUser,
                        'event_from' => config('constants.EVENTTYPEGOOGLE')
                    ])->update(['deleted' => 1,'activated' => 0]);
    }
}
