<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\Post;
use App\Libraries\Comment;
use App\Libraries\Like;
use App\Libraries\Notification;

use App\Libraries\UserSession;
use Davibennun\LaravelPushNotification\Facades\PushNotification;

class SendCommentOnPostNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $nIdPost, $oLoggedinUser;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser, $nIdPost)
    {
        $this->nIdPost = $nIdPost;
        $this->oLoggedinUser = $oLoggedinUser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Add notification for post creator, post commenter, and who likes the post
        $aSendNotificationsTo = array();

        $oPostComments = Comment::getEntityComments($this->nIdPost, 'P');
        foreach ($oPostComments as $oPostCommentElement)
        {
            if(!in_array($oPostCommentElement->id_user, $aSendNotificationsTo))
                $aSendNotificationsTo[] = $oPostCommentElement->id_user;
        }

        $oPostLikes = Like::getEntityLikes($this->nIdPost, 'P');
        foreach ($oPostLikes as $oPostLike)
        {
            if(!in_array($oPostLike->id_user, $aSendNotificationsTo))
                $aSendNotificationsTo[] = $oPostLike->id_user;
        }

        $oPost = Post::find($this->nIdPost);
        if(!in_array($oPost->id_user, $aSendNotificationsTo))
            $aSendNotificationsTo[] = $oPost->id_user;

        foreach ($aSendNotificationsTo as $nSendNotificationTo)
        {
            if($this->oLoggedinUser->id_user == $nSendNotificationTo)
                continue;

            Notification::create([
                                'id_user' => $nSendNotificationTo,
                                'id_entites' => $this->oLoggedinUser->id_user.'_'.$this->nIdPost,
                                'entity_type_pattern' => 'U_P', //U = user, P = post
                                'notification_type' => config('constants.NOTIFICATIONCOMMENTONPOST')
                            ]);
            $this->sendPushNotification($nSendNotificationTo, $oPost);
        }
    }
    
    private function sendPushNotification($nIdUser, $oPost)
    {
        $aUserSessions = UserSession::where('id_user', '=', $nIdUser)
                                    ->where('logout_time', '=', '0000-00-00 00:00:00')
                                    ->get();

        $sNotificationMessage = trans('messages.comment_on_post', ['user' => $this->oLoggedinUser->first_name.' '.$this->oLoggedinUser->last_name, 'post' => applySubStr($oPost->post_text, 35)]);

        foreach ($aUserSessions as $oUserSession)
        {
            //Update badge_count
            $oUserSession->badge_count = $oUserSession->badge_count + 1;
            $oUserSession->save();
            
            if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
            {
                PushNotification::app($oUserSession->device_type.'OS')
                                    ->to($oUserSession->device_token)
                                    ->send($sNotificationMessage,array('badge'=>$oUserSession->badge_count,'content-available'=>1,'custom'=>array('type'=>'O')));
            }
        }
    }
}