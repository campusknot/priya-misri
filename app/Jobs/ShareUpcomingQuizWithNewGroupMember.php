<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\User;
use App\Libraries\Quiz;
use App\Libraries\QuizUserInvite;
use App\Libraries\Notification;

use Carbon\Carbon;

class ShareUpcomingQuizWithNewGroupMember extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $nIdUser, $nIdGroup;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nIdUser, $nIdGroup)
    {
        $this->nIdUser = $nIdUser;
        $this->nIdGroup = $nIdGroup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oUser = User::find($this->nIdUser);
        if($oUser->user_type != config('constants.USERTYPEFACULTY') )
        {
            $dTime = Carbon::Now();
            $aWhereData = [
                                ['qgi.end_time', '>', $dTime]
                            ];
            $oQuizList = Quiz::getUpcomingGroupQuiz($aWhereData,$this->nIdGroup);

            //Add event request for new group member.
            foreach ($oQuizList as $oQuiz)
            {
                $oQuizUserInvite = QuizUserInvite::firstOrNew(['id_user' => $this->nIdUser,
                                                               'id_quiz' => $oQuiz->id_quiz ]);
                $oQuizUserInvite->start_time = $oQuiz->start_time;
                $oQuizUserInvite->end_time = $oQuiz->end_time;
                $oQuizUserInvite->save();
                Notification::create([  'id_user' => $this->nIdUser,
                                        'id_entites' => $oQuiz->id_user.'_'.$oQuiz->id_quiz.'_'.$this->nIdGroup,
                                        'entity_type_pattern' => 'U_Q_G',
                                        'notification_type' => config('constants.NOTIFICATIONGROUPQUIZ')
                                      ]);
            }
        }
    }
}
