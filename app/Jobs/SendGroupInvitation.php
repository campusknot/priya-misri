<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Validator;

use Illuminate\Contracts\Mail\MailQueue;

use App\Libraries\User;
use App\Libraries\UserSession;
use App\Libraries\Group;
use App\Libraries\GroupMember;
use App\Libraries\GroupRequest;
use App\Libraries\NonMemberInvitation;

use Davibennun\LaravelPushNotification\Facades\PushNotification;

class SendGroupInvitation extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $oLogedinUser, $aValidEmails, $nIdGroup;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLogedinUser, $aValidEmails, $nIdGroup)
    {
        $this->oLogedinUser = $oLogedinUser;
        $this->aValidEmails = $aValidEmails;
        $this->nIdGroup = $nIdGroup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailQueue $oMailer)
    {
        $this->proccessValidEmails($oMailer, $this->oLogedinUser, $this->aValidEmails, $this->nIdGroup);
    }
    
    protected function proccessValidEmails($oMailer, $oLogedinUser, $aValidEmails, $nIdGroup)
    {
        $oGroup = new Group();
        $oGroupDetails = $oGroup->getGroupDetail($nIdGroup, $oLogedinUser->id_user);
        
        foreach ($aValidEmails as $sEmail)
        {
            $sEmail = trim($sEmail);

            //Get user detail
            $oUser = new User();
            $oUserDetail = $oUser->getUserDetail('email', $sEmail);
            
            $bResponse = FALSE;
            //Check if user exist in campusknot
            if($oUserDetail)
            {
                $bResponse = $this->inviteCampusMember($oUserDetail, $nIdGroup, $oLogedinUser, $oGroupDetails);
            }
            else
            {
                $oValidator = Validator::make(['unique_combination_key' => $oLogedinUser->id_user.'_'.$sEmail.'_'.$nIdGroup.'_G'],
                                                [
                                                    'unique_combination_key' => 'required|unique:non_member_invitations'
                                                ]);
                if(!$oValidator->fails())
                {
                    NonMemberInvitation::create([
                                                'id_user_invite_from' => $oLogedinUser->id_user,
                                                'invite_to' => $sEmail,
                                                'id_entity_invite_for' => $nIdGroup,
                                                'entity_type' => 'G',
                                                'unique_combination_key' => $oLogedinUser->id_user.'_'.$sEmail.'_'.$nIdGroup.'_G'
                                            ]);
                    $bResponse = TRUE;
                }
            }
            
            if($bResponse)
            {
                //Send mail to user
                $aMailData = [
                                'sFirstName'=>($oUserDetail) ? $oUserDetail->first_name :'',
                                'nIdGroup'=>$nIdGroup,
                                'sInvitedBy' => $oLogedinUser->first_name.' '.$oLogedinUser->last_name,
                                'group_name'=>$oGroupDetails->group_name
                            ];
                $oMailer->later(10, 'WebView::emails.group_invitation', $aMailData, function ($oMessage) use ($oLogedinUser, $oUserDetail, $sEmail) {
                    $oMessage->from(config('mail.from.address'), $oLogedinUser->first_name.' '.$oLogedinUser->last_name);

                    $oMessage->to($sEmail, ($oUserDetail)?$oUserDetail->first_name.' '.$oUserDetail->last_name : '')
                            ->subject(trans('messages.group_invitation_subject'));
                });
            }
        }
    }
    
    private function inviteCampusMember($oUserDetail, $nIdGroup, $oLogedinUser, $oGroupDetails)
    {
        $oGroupMember = new GroupMember();
        $oGroup = Group::find($nIdGroup);
        
        //  Check if not already member of group
        $aGroupMemberIds = array();
        $oGroupMembersList = $oGroupMember->getAllGroupMembers($nIdGroup, array(), $oLogedinUser->id_user)
                                            ->all();
        
        $aGroupMemberIds = array_map(function($oGroupMember){
                                    return intval($oGroupMember->id_user);
                                },$oGroupMembersList);
        
        if(in_array($oUserDetail->id_user, $aGroupMemberIds) || $oGroup->id_campus != $oUserDetail->id_campus)
        {
            //---YES
            //Do not procces this record
            return FALSE;
        }
        //---NO
        //   Check if request pending
        $oGroupRequest = new GroupRequest();
        
        $aGroupPendingMemberIds = array();
        $oGroupPendingMembersList = $oGroupRequest->getGroupPendingRequests($nIdGroup);
        
        foreach($oGroupPendingMembersList as $oGroupPendingMemberItem)
        {
            $aGroupPendingMemberIds[] = $oGroupPendingMemberItem->id_user_request_to;
        }
        
        if(in_array($oUserDetail->id_user, $aGroupPendingMemberIds))
        {
            //---YES
            //Do not make new entry in database but send mail as reminder
            
            return FALSE;
        }
        //--------NO
        //      Make entry in group_request table and Send email
        
        $oGroupRequest=GroupRequest::firstOrCreate(['id_group' => $nIdGroup,'id_user_request_to'=>$oUserDetail->id_user]);
        
        $oGroupRequest->id_group = $nIdGroup;
        $oGroupRequest->id_user_request_from = $oLogedinUser->id_user;
        $oGroupRequest->id_user_request_to = $oUserDetail->id_user;
        $oGroupRequest->request_type = 'I';
        $oGroupRequest->member_status = 'P';
        $oGroupRequest->save();
        
        //Send push notification
        $aUserSessions = UserSession::where('id_user', '=', $oUserDetail->id_user)
                                    ->where('logout_time', '=', '0000-00-00 00:00:00')
                                    ->get();

        $sNotificationMessage = $oLogedinUser->first_name.' '.$oLogedinUser->last_name.' '.trans('messages.group_invitation_text').' '.$oGroupDetails->group_name;

        foreach ($aUserSessions as $oUserSession)
        {
            //Update badge_count
            $oUserSession->badge_count = $oUserSession->badge_count + 1;
            $oUserSession->save();
            
            if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
            {
                PushNotification::app($oUserSession->device_type.'OS')
                                    ->to($oUserSession->device_token)
                                    ->send($sNotificationMessage, array('badge'=>$oUserSession->badge_count,'content-available'=>1,'custom'=>array('type'=>'G')));
            }
        }
                    
        return TRUE;
    }
}