<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\MailQueue;
use App\Libraries\User;

class SendNewsletter extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $nIdCampus , $sSubject,$sUserType;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id_campus,$sSubject,$user_type)
    {
        $this->nIdCampus = $id_campus;
        $this->sSubject = $sSubject;
        $this->sUserType = $user_type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailQueue $oMailer)
    {
        $sSubject = $this->sSubject;
        $oUsers = User::where([ 'id_campus'=>$this->nIdCampus,
                                'user_type' => $this->sUserType,
                                'activated'=> 1, 
                                'deleted'=> 0 
                            ])->get();
        
        if($this->sUserType == 'S')
        {
            foreach( $oUsers as $oUser )
            {
                $oMailer->later(10,'WebView::emails.send_newsletter', ['sName'=>$oUser->first_name.' '.$oUser->last_name], function ($oMessage) use ($oUser,$sSubject) {
                        $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                        $oMessage->to($oUser->email, $oUser->first_name.' '.$oUser->last_name)
                                ->subject($sSubject);
                    });
            }
        }
        else
        {
            foreach( $oUsers as $oUser )
            {
                $oMailer->later(10,'WebView::emails.send_newsletter_faculty', ['sName'=>$oUser->first_name.' '.$oUser->last_name], function ($oMessage) use ($oUser,$sSubject) {
                        $oMessage->from(config('mail.from.address'), config('mail.from.name'));

                        $oMessage->to($oUser->email, $oUser->first_name.' '.$oUser->last_name)
                                ->subject($sSubject);
                    });
            }
        }
    }
}
