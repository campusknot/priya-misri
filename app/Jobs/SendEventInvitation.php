<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\MailQueue;

use App\Libraries\User;
use App\Libraries\UserSession;
use App\Libraries\Event;
use App\Libraries\EventRequest;
use App\Libraries\GroupMember;
use App\Libraries\GroupEvent;
use App\Libraries\Group;

use Davibennun\LaravelPushNotification\Facades\PushNotification;

class SendEventInvitation extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $aInviteMembers, $nIdEvent, $nIdUserFrom;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($aInviteMembers, $nIdEvent, $nIdUserFrom)
    {
        $this->aInviteMembers = $aInviteMembers;
        $this->nIdEvent = $nIdEvent;
        $this->nIdUserFrom = $nIdUserFrom;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailQueue $oMailer)
    {
        $this->doEventRequest($oMailer, $this->aInviteMembers, $this->nIdEvent, $this->nIdUserFrom);
    }
    
    private function doEventRequest($oMailer, $aInviteMemberList , $nIdEvent, $nIdUserFrom)
    {
        foreach($aInviteMemberList as $sInviteMember)
        {
            $aInviteMember = explode('_',$sInviteMember);
            switch ($aInviteMember[0])
            {
                case 'GM':
                    $this->addGroupEvent($oMailer, $nIdEvent, $aInviteMember[1], $nIdUserFrom);
                    break;
                case 'CU':
                case 'U' :
                    $this->addEventRequest($oMailer, $nIdEvent, $aInviteMember[1], $nIdUserFrom);
                    break;
                default :
                    break;
            }
        }
    }

    private function addEventRequest($oMailer, $nIdEvent, $nIdUserTo, $nIdUserFrom, $sStatus=NULL,$nIdGroupCategory=NULL)
    {
        $oEventRequest = EventRequest::firstOrNew([
                                                    'id_event' => $nIdEvent,
                                                    'id_user_request_to' => $nIdUserTo
                                                ]);
        $oEventRequest->id_user_request_from = $nIdUserFrom;
        $oEventRequest->status = ($oEventRequest->status != NULL &&$oEventRequest->status !='') ? $oEventRequest->status : $sStatus;
        $oEventRequest->save();
        
        $oUser = new User();
        $oInvitedUserDetail = $oUser->getUserDetail('id_user', $nIdUserTo);
        $oInvityUserDetail = $oUser->getUserDetail('id_user', $nIdUserFrom);
        $oEvent = Event::find($nIdEvent);
        
        if(($nIdUserFrom != $nIdUserTo) && ($oInvitedUserDetail != NULL) && ($oInvityUserDetail != NULL))
        {
            if($nIdGroupCategory != config('constants.UNIVERSITYGROUPCATEGORY') )
            {
//                $oMailer->later(10,'WebView::emails.event_invitation', ['sFirstName'=>$oInvitedUserDetail->first_name, 'sInvitedBy' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name, 'sEventName'=>$oEvent->event_title], function ($oMessage) use ($oInvityUserDetail, $oInvitedUserDetail) {
//                    $oMessage->from(config('mail.from.address'), $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name);
//
//                    $oMessage->to($oInvitedUserDetail->email, $oInvitedUserDetail->first_name.' '.$oInvitedUserDetail->last_name)
//                            ->subject(trans('messages.event_invitation_subject'));
//                });
            }
            
            //Send push notification
            $aUserSessions = UserSession::where('id_user', '=', $nIdUserTo)
                                        ->where('logout_time', '=', '0000-00-00 00:00:00')
                                        ->get();

            $sNotificationMessage = trans('messages.event_invite_mail_body',['invite_by' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name,'event_name'=>$oEvent->event_title]);

            foreach ($aUserSessions as $oUserSession)
            {
                //Update badge_count
                $oUserSession->badge_count = $oUserSession->badge_count + 1;
                $oUserSession->save();
                
                if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
                {
                    PushNotification::app($oUserSession->device_type.'OS')
                                        ->to($oUserSession->device_token)
                                        ->send($sNotificationMessage,array('badge'=>$oUserSession->badge_count,'content-available'=>1,'custom'=>array('type'=>'E')));
                }
            }
        }
    }

    private function addGroupEvent($oMailer, $nIdEvent ,$nIdGroup, $nIdUserFrom )
    {
        $aGroupEvent = array();
        $aGroupEvent['id_event'] = $nIdEvent;
        $aGroupEvent['id_group'] = $nIdGroup;
        
        GroupEvent::firstOrCreate($aGroupEvent);
        $oGroup = Group::find($nIdGroup);
        $aGroupMembers = GroupMember::getAllGroupMembers($nIdGroup);
        foreach( $aGroupMembers as $oGroupMember )
        {
            $this->addEventRequest($oMailer, $nIdEvent, $oGroupMember->id_user, $nIdUserFrom,NULL,$oGroup->id_group_category);
        }
    }
}