<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\MailQueue;
use Davibennun\LaravelPushNotification\Facades\PushNotification;

use App\Libraries\Notification;
use App\Libraries\UserSession;

class SendCancelEventNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $oLoggedinUser, $oEvent, $oEventMembers;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser, $oEvent, $oEventMembers)
    {
        $this->oLoggedinUser = $oLoggedinUser;
        $this->oEvent = $oEvent;
        $this->oEventMembers = $oEventMembers;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailQueue $oMailer)
    {
        foreach ($this->oEventMembers as $oEventMember)
        {
            if($oEventMember->id_user != $this->oLoggedinUser->id_user)
            {
                Notification::create([
                                        'id_user' => $oEventMember->id_user,
                                        'id_entites' => $this->oLoggedinUser->id_user.'_'.$this->oEvent->id_event,
                                        'entity_type_pattern' => 'U_E', //U = user, E = event
                                        'notification_type' => config('constants.NOTIFICATIONCANCELEVENT')
                                    ]);
                
//                $oMailer->later(10,'WebView::emails.event_cancelation', ['sFirstName'=>$oEventMember->first_name, 'sCancelBy' => $this->oLoggedinUser->first_name.' '.$this->oLoggedinUser->last_name, 'sEventName' => $this->oEvent->event_title], function ($oMessage) use ($oEventMember) {
//                    $oMessage->from(config('mail.from.address'), config('mail.from.name'));
//
//                    $oMessage->to($oEventMember->email, $oEventMember->first_name.' '.$oEventMember->last_name)
//                            ->subject(trans('messages.event_cancelation_subject'));
//                });
                
                $this->sendPushNotification($oEventMember->id_user);
            }
        }
    }
    
    private function sendPushNotification($nIdUser)
    {
        $aUserSessions = UserSession::where('id_user', '=', $nIdUser)
                                    ->where('logout_time', '=', '0000-00-00 00:00:00')
                                    ->get();

        $sNotificationMessage = trans('messages.event_cancel_mail_body', ['cancel_by' => $this->oLoggedinUser->first_name.' '.$this->oLoggedinUser->last_name, 'event_name' => $this->oEvent->event_title]);

        foreach ($aUserSessions as $oUserSession)
        {
            //Update badge_count
            $oUserSession->badge_count = $oUserSession->badge_count + 1;
            $oUserSession->save();
            
            if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
            {
                PushNotification::app($oUserSession->device_type.'OS')
                                    ->to($oUserSession->device_token)
                                    ->send($sNotificationMessage,array('badge'=>$oUserSession->badge_count,'content-available'=>1,'custom'=>array('type'=>'O')));
            }
        }
    }
}