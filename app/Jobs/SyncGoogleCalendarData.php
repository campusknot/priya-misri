<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Carbon\Carbon;
use App\Libraries\Event;
use App\Libraries\EventRequest;
use App\Libraries\GoogleApiAccessToken;

class SyncGoogleCalendarData extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $oLoggedinUser, $sTimeMin, $sTimeMax;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser,$sTimeMin,$sTimeMax)
    {
        $this->oLoggedinUser = $oLoggedinUser;
        $this->sTimeMin = $sTimeMin;
        $this->sTimeMax = $sTimeMax;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oGoogleApiAccessTocken = GoogleApiAccessToken::where('id_user' , $this->oLoggedinUser)
                                                        ->first();
        if(count($oGoogleApiAccessTocken))
        {
            $aGData['access_token'] = $oGoogleApiAccessTocken->access_token;
            $aGData['refresh_token'] = $oGoogleApiAccessTocken->refresh_token;
            $aGData['created'] = $oGoogleApiAccessTocken->created;
            $aGData['token_type'] = $oGoogleApiAccessTocken->token_type;
            $aGData['expires_in'] = $oGoogleApiAccessTocken->expires_in;

            $oClient = new \Google_Client();
            $oClient->setAuthConfig( config('constants.client_secret_path') );
            $oClient->setApplicationName(config('constants.GOOGLECALENDARAPPNAME'));
            $oClient->setApprovalPrompt('force');
            $oClient->setAccessType('offline');
            $oClient->setRedirectUri(url('/event/google-credential'));
            $oClient->setDeveloperKey(config('constants.GOOGLEDEVELOPERKEY'));
            $oClient->addScope(\Google_Service_Calendar::CALENDAR_READONLY);
            $oClient->setAccessToken($aGData);

            if ($oClient->isAccessTokenExpired() == 1 && isset($aGData)) 
            {
                $oClient->refreshToken($aGData['refresh_token']);
            }
            $aToken = $oClient->getAccessToken();
            if(!isset($aToken['refresh_token']))
            {
                $aToken['refresh_token'] = $aGData['refresh_token'];
            }

            if (count($aToken) && isset($aGData)) 
            {
                $oGoogleServiceCalendar = new \Google_Service_Calendar($oClient);
                $aCalendarList = $oGoogleServiceCalendar->calendarList->listCalendarList()->getItems();

                foreach($aCalendarList as $oCalendar)
                {
                    if(mb_strpos( $oCalendar['id'],'holiday') === false && mb_strpos( $oCalendar['id'], 'contacts') === false)
                    {
                    $oEvents = $oGoogleServiceCalendar->events->listEvents($oCalendar->getId(),array('timeMin'=>$this->sTimeMin, 'timeMax'=>$this->sTimeMax,
                                                            'showDeleted'=>'true','singleEvents'=> 'true','orderBy'=>'startTime') );
                    $sCalendarId = $oCalendar->getId();
                    $sCalendarName = $oCalendar->getSummary();
                    while(count($oEvents) > 0)
                    {
                        $aEvents = $oEvents->getItems();
                        foreach($aEvents as $oEvent)
                        {
                            if($oEvent['recurringEventId'])
                                Event::where('id_external_event',$oEvent['recurringEventId'])->update(['deleted' => 1,'activated' => 0]);

                            $sIdExternalEvent = $oEvent->getId();
                            $dStartTime = $oEvent->getStart()->getDateTime();
                            if($dStartTime == '') 
                                $dStartTime = $oEvent->getStart()->getDate();
                            $dEndTime = $oEvent->getEnd()->getDateTime();
                            if($dEndTime == '') 
                                $dEndTime = $oEvent->getEnd()->getDate();

                            $dstart = Carbon::parse($dStartTime);
                            $dEnd = Carbon::parse($dEndTime);
                            $sEventTitle = $oEvent->getSummary();
                            $sEventDesc = $oEvent->getDescription();
                            $sEventAddress=$oEvent->getLocation();
                            $latitude=$longitude='';
                            if(trim($sEventAddress) !='')
                            {
                                $sPrepAddr = str_replace(' ','+',$sEventAddress);
                                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$sPrepAddr.'&sensor=false');
                                $aGeocode= json_decode($geocode);
                                if(isset($aGeocode->results) && count($aGeocode->results))
                                {
                                    $latitude = $aGeocode->results[0]->geometry->location->lat;
                                    $longitude = $aGeocode->results[0]->geometry->location->lng;
                                }
                            }
                            else
                                $sEventAddress=' ';

                            if($oEvent->getStatus() == 'cancelled')
                            {
                                Event::where('id_external_event', $sIdExternalEvent )->update(['deleted' => 1,'activated' => 0]);
                            }
                            elseif($sEventTitle != '')
                            {
                                $oCreatedEvent = Event::firstOrNew([
                                                                'id_external_event' =>$sIdExternalEvent,
                                                                'id_user' => $this->oLoggedinUser
                                                            ]);
                                $oCreatedEvent->event_title = $sEventTitle;
                                $oCreatedEvent->event_description = $sEventDesc;
                                $oCreatedEvent->start_date = $dstart->toDateTimeString();
                                $oCreatedEvent->end_date = $dEnd->toDateTimeString();
                                $oCreatedEvent->address = $sEventAddress;
                                $oCreatedEvent->latitude = $latitude;
                                $oCreatedEvent->longitude = $longitude;
                                $oCreatedEvent->event_from = config('constants.EVENTTYPEGOOGLE');
                                $oCreatedEvent->id_calendar = $sCalendarId;
                                $oCreatedEvent->calendar_name = $sCalendarName;
                                $oCreatedEvent->activated = 1;
                                $oCreatedEvent->deleted = 0;
                                $oCreatedEvent->save();

                                if($oCreatedEvent)
                                {
                                    $this->addEventRequest($oCreatedEvent->id_event,$this->oLoggedinUser, $this->oLoggedinUser, 'G');
                                }
                            }
                        }
                        $pageToken = $oEvents->getNextPageToken();
                        $oEvents = array();
                        if ($pageToken != '') {
                          $optParams = array('pageToken' => $pageToken,'timeMin'=>$this->sTimeMin, 'timeMax'=>$this->sTimeMax,
                                            'showDeleted'=>'true','singleEvents'=> 'true','orderBy'=>'startTime');
                          $oEvents = $oGoogleServiceCalendar->events->listEvents($oCalendar->getId(), $optParams);
                        }
                    }

                    }
                }
                $oGoogleApiAccessToken = GoogleApiAccessToken::where(
                                                                    'id_user','=', $this->oLoggedinUser
                                                                )
                                                              ->update([
                                                                  'access_token' => $aToken['access_token'],
                                                                  'refresh_token' => $aToken['refresh_token'],
                                                                  'token_type' => $aToken['token_type'],
                                                                  'created' => $aToken['created'],
                                                                  'expires_in' => $aToken['expires_in']
                                                                  ]);

            }
        }
    }
    
    private function addEventRequest( $nIdEvent, $nIdUserTo, $nIdUserFrom, $sStatus=NULL)
    {
        /*request for me to me to get event data */
        $oEventRequest = EventRequest::firstOrNew([
                                                    'id_event' => $nIdEvent,
                                                    'id_user_request_to' => $nIdUserTo
                                                ]);
        $oEventRequest->id_user_request_from = $nIdUserFrom;
        $oEventRequest->status = ($oEventRequest->status != NULL) ? $oEventRequest->status : $sStatus;
        $oEventRequest->save();

    }
}
