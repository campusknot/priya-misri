<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\DocumentPermisssion;

class RevokeGroupDocPermission extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $oLoggedinUser, $nIdGroup;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser, $nIdGroup)
    {
        $this->oLoggedinUser = $oLoggedinUser;
        $this->nIdGroup = $nIdGroup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $nIdGroup=$this->nIdGroup;
        $oDocumentPermisssion= DocumentPermisssion::from( 'document_permissions as dp')
                                ->where('dp.id_user', '=', $this->oLoggedinUser)
                                ->where('dp.id_group', '=', $nIdGroup)
                                ->whereIn('dp.id_document', function($query) use ($nIdGroup){
                                            $query->select('id_document')
                                                    ->from('group_documents')
                                                    ->where('id_group','=', $nIdGroup);
                                        })
                                ->update([ 
                                            'dp.activated' => 0,
                                            'dp.deleted' => 1,
                                        ]);        
    }
}
