<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Libraries\User;
use App\Libraries\QuizGroupInvite;
use App\Libraries\QuizUserInvite;
use App\Libraries\GroupMember;
use App\Libraries\Notification;

class SendQuizGroupInvite extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $dStartdate,$dEnddate,$nIdQuiz,$nIdGroup,$nLoginIdUser,$nQuizType;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($nIdUser, $dStartdate,$dEnddate,$nIdQuiz,$nIdGroup,$nQuizType)
    {
        $this->dStartdate =$dStartdate;
        $this->dEnddate =$dEnddate;
        $this->nIdQuiz =$nIdQuiz;
        $this->nIdGroup =$nIdGroup;
        $this->nLoginIdUser =$nIdUser;
        $this->nQuizType =$nQuizType;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dStartdate = $this->dStartdate;
        $dEnddate = $this->dEnddate;
        $nIdQuiz = $this->nIdQuiz;
        $nIdUser = $this->nLoginIdUser;
        $nIdGroup = $this->nIdGroup;
        $nQuizType = $this->nQuizType;
        
        $oQuizGroupInvite = QuizGroupInvite::firstOrNew(['id_quiz' => $nIdQuiz ]);
        $nIdGroupOld='';
        if(count($oQuizGroupInvite))
        {
            $nIdGroupOld = $oQuizGroupInvite->id_group;
            $dStartdateOld = $oQuizGroupInvite->start_time;
            $dEnddateOld = $oQuizGroupInvite->end_time;
        }
        $oQuizGroupInvite->id_group = $nIdGroup;
        $oQuizGroupInvite->start_time = $dStartdate;
        $oQuizGroupInvite->end_time = $dEnddate;
        $oQuizGroupInvite->save();
            
        if(!empty($nIdGroupOld) && $nIdGroupOld != $nIdGroup)
        {
            Notification::where(['id_entites' => $nIdUser.'_'.$nIdQuiz.'_'.$nIdGroupOld,
                                  'entity_type_pattern' => 'U_Q_G'])->delete();
            QuizUserInvite::where('id_quiz', $nIdQuiz)->delete();
            $oOldGroupMembersList = GroupMember::getAllGroupMembers($nIdGroupOld);
            //for delete quiz from old group/change group notification
            foreach($oOldGroupMembersList as $oGroupMember)
            {
                if($oGroupMember->id_user != $nIdUser)
                {
                    Notification::create(['id_user' => $oGroupMember->id_user,
                                          'id_entites' => $nIdUser.'_'.$nIdQuiz.'_'.$nIdGroupOld,
                                          'entity_type_pattern' => 'U_Q_G',
                                          'notification_type' => config('constants.NOTIFICATIONGROUPQUIZDELETE')
                                        ]);
                }
            }
        }
        
        $oGroupMembersList = GroupMember::getAllGroupMembers($nIdGroup);
        foreach($oGroupMembersList as $oGroupMember)
        {
            $oUser = User::find($oGroupMember->id_user);
            if($oGroupMember->member_type == config('constants.GROUPMEMBER'))
            {
                $oQuizUserInvite = QuizUserInvite::firstOrNew(['id_user' => $oGroupMember->id_user,
                                                               'id_quiz' => $nIdQuiz ]);
                $oQuizUserInvite->start_time = $dStartdate;
                $oQuizUserInvite->end_time = $dEnddate;
                $oQuizUserInvite->save();
                if((!empty($dStartdate) && $dStartdate != $dStartdateOld) || (!empty($dEnddate) && $dEnddateOld != $dEnddate))
                {
                    if($nQuizType != 3)
                    {
                    Notification::create(['id_user' => $oGroupMember->id_user,
                                      'id_entites' => $nIdUser.'_'.$nIdQuiz.'_'.$nIdGroup,
                                      'entity_type_pattern' => 'U_Q_G',
                                      'notification_type' => config('constants.NOTIFICATIONGROUPQUIZ')
                                    ]);
                    }
                }
            }
        }
    }
}