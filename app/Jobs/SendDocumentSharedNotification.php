<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Illuminate\Contracts\Mail\MailQueue;

use App\Libraries\User;
use App\Libraries\UserSession;
use App\Libraries\Group;
use App\Libraries\Document;
use App\Libraries\SharedDocument;
use App\Libraries\DocumentPermisssion;
use App\Libraries\Notification;

use Davibennun\LaravelPushNotification\Facades\PushNotification;

class SendDocumentSharedNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $nIdDocument, $nIdUserFrom, $sPermission, $aUsers, $nIdGroup;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct( $nIdDocument, $nIdUserFrom, $sPermission, $aUsers, $nIdGroup = NULL )
    {
        $this->nIdDocument = $nIdDocument;
        $this->nIdUserFrom = $nIdUserFrom;
        $this->sPermission = $sPermission;
        $this->aUsers = $aUsers;
        $this->nIdGroup = $nIdGroup;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MailQueue $oMailer)
    {
        foreach( $this->aUsers as $nIdUser )
        {
            if($nIdUser != $this->nIdUserFrom)
            {
                if($this->nIdGroup == '')
                {
                    $oSharedDocument = SharedDocument::firstOrNew([
                                                                    'id_document' => $this->nIdDocument,
                                                                    'id_user' => $this->nIdUserFrom,
                                                                    'shared_with' => $nIdUser
                                                                ]);
                    $oSharedDocument->activated = 1;
                    $oSharedDocument->deleted = 0;
                    $oSharedDocument->save();
                }
                else
                    $oGroup = Group::find($this->nIdGroup);
                
                $oDocumentPermisssion = DocumentPermisssion::firstOrCreate([
                                                                            'id_document' => $this->nIdDocument,
                                                                            'id_user' => $nIdUser,
                                                                            'id_group' => $this->nIdGroup
                                                                        ]);
                
                $oDocumentPermisssion->permission_type = $this->sPermission;
                $oDocumentPermisssion->activated = 1;
                $oDocumentPermisssion->deleted = 0;
                $oDocumentPermisssion->save();
                if(empty($this->nIdGroup) || (isset($oGroup) && $oGroup->id_group_category != config('constants.UNIVERSITYGROUPCATEGORY')) )
                Notification::firstOrCreate([
                                                'id_user' => $nIdUser,
                                                'id_entites' => empty($this->nIdGroup) ? $this->nIdUserFrom.'_'.$this->nIdDocument : $this->nIdUserFrom.'_'.$this->nIdGroup.'_'.$this->nIdDocument,
                                                'entity_type_pattern' => empty($this->nIdGroup) ? 'U_D' : 'U_G_D', //U = user, G = Group, D = Document
                                                'notification_type' => empty($this->nIdGroup) ? config('constants.NOTIFICATIONUSERDOCUMENT') : config('constants.NOTIFICATIONGROUPDOCUMENT')
                                            ]);
                
                $oInvityUserDetail = User::find($this->nIdUserFrom);
                $oInvitedUserDetail = User::find($nIdUser);
                
                $aMailParams = array();
                $sNotificationMessage = '';
                
                $oDocument = Document::find($this->nIdDocument);
                
                if(empty($this->nIdGroup))
                {
                    $sNotificationMessage = trans('messages.'.config('constants.NOTIFICATIONUSERDOCUMENT').'_folder', ['user' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name, 'document' => $oDocument->document_name]);
                    if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                    {
                        $sNotificationMessage = trans('messages.'.config('constants.NOTIFICATIONUSERDOCUMENT'), ['user' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name, 'document' => $oDocument->document_name]);
                    }
                }
                else
                {
                    if($oGroup->id_group_category != config('constants.UNIVERSITYGROUPCATEGORY') )
                    {
                    
                        $sNotificationMessage = trans('messages.'.config('constants.NOTIFICATIONGROUPDOCUMENT').'_folder', ['user' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name, 'document' => $oDocument->document_name, 'group' => $oGroup->group_name]);
                        if($oDocument->document_type == config('constants.DOCUMENTTYPEFILE'))
                        {
                            $sNotificationMessage = trans('messages.'.config('constants.NOTIFICATIONGROUPDOCUMENT'), ['user' => $oInvityUserDetail->first_name.' '.$oInvityUserDetail->last_name, 'document' => $oDocument->document_name, 'group' => $oGroup->group_name]);
                        }
                    }
                }
                if(!empty($sNotificationMessage))
                    $this->sendPushNotification($nIdUser, $sNotificationMessage);
            }
        }
    }
    
    private function sendPushNotification($nIdUserTo, $sNotificationMessage)
    {
        //Send push notification
        $aUserSessions = UserSession::where('id_user', '=', $nIdUserTo)
                                        ->where('logout_time', '=', '0000-00-00 00:00:00')
                                        ->get();
        
        foreach ($aUserSessions as $oUserSession)
        {
            if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
            {
                //Update badge_count
                $oUserSession->badge_count = $oUserSession->badge_count + 1;
                $oUserSession->save();
                
                PushNotification::app($oUserSession->device_type.'OS')
                                    ->to($oUserSession->device_token)
                                    ->send($sNotificationMessage, array('badge'=>$oUserSession->badge_count,'content-available'=>1,'custom'=>array('type'=>'O')));
            }
        }
    }
}