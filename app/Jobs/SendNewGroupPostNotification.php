<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Davibennun\LaravelPushNotification\Facades\PushNotification;

use App\Libraries\Post;
use App\Libraries\Group;
use App\Libraries\GroupMember;
use App\Libraries\Notification;
use App\Libraries\UserSession;

class SendNewGroupPostNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $oLoggedinUser, $nIdGroup, $nIdPost;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($oLoggedinUser, $nIdGroup, $nIdPost)
    {
        $this->oLoggedinUser = $oLoggedinUser;
        $this->nIdGroup = $nIdGroup;
        $this->nIdPost = $nIdPost;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $oGroup = Group::find($this->nIdGroup);
        $oPost = Post::find($this->nIdPost);
        $aUserIosDevices = array();
        $aUserAndroidDevices = array();
        
        //Send notification to all group members
        $oGroupMembersList = GroupMember::getAllGroupMembers($this->nIdGroup)->all();
        
        foreach ($oGroupMembersList as $oGroupMember)
        {
            if($oGroupMember->id_user == $this->oLoggedinUser->id_user)
                continue;

            Notification::create([
                                    'id_user' => $oGroupMember->id_user,
                                    'id_entites' => $this->oLoggedinUser->id_user.'_'.$this->nIdPost.'_'.$this->nIdGroup,
                                    'entity_type_pattern' => 'U_P_G', //U = user, P = post, G = group
                                    'notification_type' => config('constants.NOTIFICATIONNEWGROUPPOST')
                                ]);
            if($oGroup->id_group_category != config('constants.UNIVERSITYGROUPCATEGORY') )
            {
                $aUserSessions = UserSession::where('id_user', '=', $oGroupMember->id_user)
                                                ->where('logout_time', '=', '0000-00-00 00:00:00')
                                                ->get();
                
                foreach ($aUserSessions as $oUserSession)
                {
                    //Update badge_count
                    $oUserSession->badge_count = $oUserSession->badge_count + 1;
                    $oUserSession->save();

                    if(!empty($oUserSession->device_token) && $oUserSession->device_token != "(null)")
                    {
                        if($oUserSession->device_type == "I")
                            $aUserIosDevices[] = PushNotification::Device($oUserSession->device_token);
                        else
                            $aUserAndroidDevices[] = PushNotification::Device($oUserSession->device_token);
                    }
                }
            }
        }
        
        if(count($aUserIosDevices) || count($aUserAndroidDevices))
        {
            $sNotificationMessage = trans('messages.new_group_post', ['user' => $this->oLoggedinUser->first_name.' '.$this->oLoggedinUser->last_name, 'group' => $oGroup->group_name]);
            if($oPost->post_type == config('constants.POSTTYPEPOLL'))
            {
                $sNotificationMessage = trans('messages.new_group_poll', ['user' => $this->oLoggedinUser->first_name.' '.$this->oLoggedinUser->last_name, 'group' => $oGroup->group_name]);
            }

            $oMessage = PushNotification::Message($sNotificationMessage,array('badge'=>1,'content-available'=>1,'custom'=>array('type'=>'O')));
            $this->sendPushNotification($oMessage, $aUserIosDevices, $aUserAndroidDevices);
        }
    }
    
    private function sendPushNotification($oMessage, $aUserIosDevices, $aUserAndroidDevices)
    {
        if(count($aUserIosDevices)) {
            $aIosDevices = PushNotification::DeviceCollection($aUserIosDevices);
            PushNotification::app('IOS')
                            ->to($aIosDevices)
                            ->send($oMessage);
        }
        if(count($aUserAndroidDevices)) {
            $aAndroidDevices = PushNotification::DeviceCollection($aUserAndroidDevices);
            PushNotification::app('AOS')
                            ->to($aAndroidDevices)
                            ->send($oMessage);
        }
    }
}