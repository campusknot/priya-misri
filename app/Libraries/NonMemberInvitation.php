<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class NonMemberInvitation extends Model
{
    protected $table = 'non_member_invitations';
    protected $primaryKey = 'id_non_member_invitation';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user_invite_from', 'invite_to', 'id_entity_invite_for', 'entity_type', 'unique_combination_key'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public static function getNonMemberRequests($nIdEntity, $sEntityType, $nSkipRecords, $nLimit)
    {
        return GroupRequest::from('non_member_invitations as nmi')
                            ->where('nmi.id_entity_invite_for', '=', $nIdEntity)
                            ->where('nmi.entity_type', '=', $sEntityType)
                            ->select(
                                        'nmi.id_non_member_invitation as id_non_member_invitation',
                                        'nmi.invite_to as invite_to',
                                        'nmi.created_at as created_at',
                                        'nmi.updated_at as updated_at'
                                    )
                            ->orderBy('nmi.updated_at', 'desc')
                            ->skip($nSkipRecords)
                            ->take($nLimit)
                            ->get();
    }
}
