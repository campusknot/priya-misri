<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class GroupEvent extends Model
{
    protected $table = 'group_events';
    protected $primaryKey = 'id_group_event';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_group', 'id_event'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * This function will return all group events.
     * @param type $nIdGroup
     * @return type
     */
    public static function getAllGroupEvents($nIdGroup)
    {
        return  GroupEvent::from('group_events as ge')
                            ->leftJoin('events as e', 'ge.id_event', '=', 'e.id_event')
                            ->where('ge.id_group', '=', $nIdGroup)
                            ->where('e.activated', '=', 1)
                            ->where('e.deleted', '=', 0)
                            ->select(
                                        'ge.id_group_event as id_group_event',
                                        'ge.id_group as id_group',
                                        'ge.id_event as id_event',
                                        'e.id_user as id_user'
                                    )
                            ->get();
    }
    
    /**
     * This function will return group events and other data with pagination
     * @param type $nIdGroup
     * @param type $dStartDate
     * @return type Array
     */
    public static function getGroupEvents($nIdGroup, $dStartDate = '',$sSeacrhStr='')
    {
        if(empty($dStartDate))
        {
            $dCurrentdate = Carbon::now();
            $dStartDate = $dCurrentdate->toDateString();
        }
        
        $query =  GroupEvent::from('group_events as ge')
                            ->leftJoin('events as e', 'ge.id_event', '=', 'e.id_event')
                            ->where('ge.id_group', '=', $nIdGroup)
                            ->where('e.activated', '=', 1)
                            ->where('e.deleted', '=', 0)
                            ->where(function ($query) use($dStartDate) {
                                $query->where('e.start_date', '>=', $dStartDate." 00:00:00")
                                      ->orWhere('e.end_date', '>=', $dStartDate." 00:00:00");
                            })
                            ->select(
                                        'ge.id_group_event as id_group_event',
                                        'ge.id_group as id_group',
                                        'ge.id_event as id_event',
                                        'e.id_user as id_user',
                                        'e.event_title as event_title',
                                        'e.event_description as event_description',
                                        'e.start_date as start_date',
                                        'e.end_date as end_date',
                                        'e.room_number as room_number',
                                        'e.address as address',
                                        'e.latitude as latitude',
                                        'e.longitude as longitude',
                                        'e.activated as activated',
                                        'e.deleted as deleted',
                                        'e.created_at as created_at',
                                        'e.updated_at as updated_at'
                                    )
                            ->orderBy('e.start_date', 'asc');
        if($sSeacrhStr != '')
            $query->where(DB::raw('e.event_title'), 'like', '%'.$sSeacrhStr.'%' );
        
        return $query->paginate(config('constants.PERPAGERECORDS'));
                            
    }
    
    /**
     * This function returns all group names related to event.
     * @param type $nIdEvent
     * @param type $aGroupIds
     * @return Object
     */
    public static function getEventGroups($nIdEvent, $aGroupIds = array())
    {
        return GroupEvent::from('group_events as ge')
                            ->leftJoin('groups as g', 'ge.id_group', '=', 'g.id_group')
                            ->where('ge.id_event', '=', $nIdEvent)
                            ->whereIn('ge.id_group', $aGroupIds)
                            ->select(
                                        DB::raw('group_concat(g.id_group SEPARATOR " | ") as id_groups'),
                                        DB::raw('group_concat(g.group_name SEPARATOR " | ") as group_names')
                                    )
                            ->get();
    }
}
