<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserPublications extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'publication_title', 
        'publication_url','publication_author',
        'publication_description','publication_year'  
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_publications';
    protected $primaryKey = 'id_user_publication';
    
    public function getRouteKeyName()
    {
        return 'id_user_publication';
    }


	/**
     * Get the user that owns the Organization.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserPublication($nUserId, $nLimit = 0)
    {
        return UserPublications::where('id_user', $nUserId)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->orderBy('publication_year', 'desc')
                                ->when($nLimit > 0, function ($query) use ($nLimit) {
                                    return $query->take($nLimit);
                                })
                                ->get();
    }

}
