<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Poll extends Model
{
    protected $table = 'polls';
    protected $primaryKey = 'id_poll';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_entity', 'entity_type', 'poll_text', 'file_name', 'start_time', 'end_time', 'allow_multiple_answers', 'poll_type', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    public static function getUncomplitedPollCount($aUserFollowIds, $aUserGroupIds = array(), $dToday)
    {
        if(!count($aUserFollowIds))
            return FALSE;
        
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('polls as po',function($join) {
                        $join->on('p.id_post', '=', 'po.id_entity')
                             ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                    })
                    ->leftJoin('poll_answers as pa',function($join) {
                        $join->on('po.id_poll', '=', 'pa.id_poll')
                             ->where('pa.id_user','=',Auth::user()->id_user);
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->where('po.activated', '=', 1)
                    ->where('po.deleted', '=', 0)
                    ->where(function ($query) use($aUserFollowIds, $aUserGroupIds) {
                        $query->whereIn('p.id_user', $aUserFollowIds)
                              ->where('gp.id_post', NULL)
                              ->when(count($aUserGroupIds), function ($query) use ($aUserGroupIds) {
                                    return $query->orWhereIn('gp.id_group', $aUserGroupIds);
                                });
                    })
                    ->where('p.post_type', '=', config('constants.POSTTYPEPOLL'))
                    ->where('po.start_time','<=',$dToday)
                    ->where('po.end_time','>',$dToday)
                    ->where('pa.id_poll_answer',NULL)
                    ->count();
    }
    
    public static function getComplitedPollCount($aUserFollowIds, $aUserGroupIds = array(), $dToday)
    {
        if(!count($aUserFollowIds))
            return FALSE;
        
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('polls as po',function($join) {
                        $join->on('p.id_post', '=', 'po.id_entity')
                             ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                    })
                    ->leftJoin('poll_answers as pa',function($join) {
                        $join->on('po.id_poll', '=', 'pa.id_poll')
                             ->where('pa.id_user','=',Auth::user()->id_user);
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->where('po.activated', '=', 1)
                    ->where('po.deleted', '=', 0)
                    ->where(function ($query) use($aUserFollowIds, $aUserGroupIds) {
                        $query->whereIn('p.id_user', $aUserFollowIds)
                              ->where('gp.id_post', NULL)
                              ->when(count($aUserGroupIds), function ($query) use ($aUserGroupIds) {
                                    return $query->orWhereIn('gp.id_group', $aUserGroupIds);
                                });
                    })
                    ->where('po.start_time','<=',$dToday)
                    ->where(function ($query) use($dToday) {
                        $query->where('po.end_time','<',$dToday)
                              ->orWhere('pa.id_poll_answer','!=', NULL);
                    })
                    ->count();
    }
    
    public static function getUncomplitedPolls($aUserFollowIds, $aUserGroupIds = array(), $dToday, $nSkipRecords, $nLimit)
    {
        if(!count($aUserFollowIds))
            return FALSE;
        
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('polls as po',function($join) {
                        $join->on('p.id_post', '=', 'po.id_entity')
                             ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                    })
                    ->leftJoin('poll_answers as pa',function($join) {
                        $join->on('po.id_poll', '=', 'pa.id_poll')
                             ->where('pa.id_user','=',Auth::user()->id_user);
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->leftJoin('groups as g', 'gp.id_group', '=', 'g.id_group')
                    ->leftJoin('group_members as gm', function($join) {
                                        $join->on('gm.id_group', '=', 'g.id_group')
                                             ->where('gm.id_user','=',Auth::user()->id_user);
                                })
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->where('po.activated', '=', 1)
                    ->where('po.deleted', '=', 0)
                    ->where(function ($query) use($aUserFollowIds, $aUserGroupIds) {
                        $query->whereIn('p.id_user', $aUserFollowIds)
                              ->where('gp.id_post', NULL)
                              ->when(count($aUserGroupIds), function ($query) use ($aUserGroupIds) {
                                    return $query->orWhereIn('gp.id_group', $aUserGroupIds);
                                });
                    })
                    ->where('p.post_type', '=', config('constants.POSTTYPEPOLL'))
                    ->where('po.start_time','<=',$dToday)
                    ->where('po.end_time','>',$dToday)
                    ->where('pa.id_poll_answer',NULL)
                    ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'p.post_text as post_text',
                                'p.post_type as post_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'gp.id_group_post as id_group_post',
                                'g.id_group as id_group',
                                'g.group_name as group_name',
                                'g.section as section',
                                'g.semester as semester',
                                'gm.member_type as member_type',
                                'po.id_poll as id_poll',
                                'po.poll_text as poll_text',
                                'po.file_name as poll_file_name',
                                'po.start_time as start_time',
                                'po.end_time as end_time',
                                'po.allow_multiple_answers as allow_multiple_answers',
                                'po.poll_type as poll_type',
                                'po.activated as activated',
                                'pa.id_poll_answer as id_poll_answer',
                                'pa.id_poll_option as id_poll_option',
                                'pa.poll_answer_description as poll_answer_description',
                                'p.created_at as created_at',
                                'p.updated_at as updated_at'
                            )
                    ->orderBy(DB::raw('TIMEDIFF(po.end_time, NOW())'), 'asc')
                    ->skip($nSkipRecords)
                    ->take($nLimit)
                    ->get();
    }
    
    public static function getAllPolls($aUserFollowIds, $aUserGroupIds = array(), $dToday, $nSkipRecords, $nLimit)
    {
        if(!count($aUserFollowIds))
            return FALSE;
        
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('polls as po',function($join) {
                        $join->on('p.id_post', '=', 'po.id_entity')
                             ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                    })
                    ->leftJoin('poll_answers as pa',function($join) {
                        $join->on('po.id_poll', '=', 'pa.id_poll')
                             ->where('pa.id_user','=',Auth::user()->id_user);
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->leftJoin('groups as g', 'gp.id_group', '=', 'g.id_group')
                    ->leftJoin('group_members as gm', function($join) {
                                        $join->on('gm.id_group', '=', 'g.id_group')
                                             ->where('gm.id_user','=',Auth::user()->id_user);
                                })
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->where('po.activated', '=', 1)
                    ->where('po.deleted', '=', 0)
                    ->where('p.post_type', '=', config('constants.POSTTYPEPOLL'))
                    ->where(function ($query) use($aUserFollowIds, $aUserGroupIds) {
                        $query->whereIn('p.id_user', $aUserFollowIds)
                              ->where('gp.id_post', NULL)
                              ->when(count($aUserGroupIds), function ($query) use ($aUserGroupIds) {
                                    return $query->orWhereIn('gp.id_group', $aUserGroupIds);
                                });
                    })
                    ->where('po.start_time','<=',$dToday)
                    ->where(function ($query) use($dToday) {
                        $query->where('po.end_time','<',$dToday)
                              ->orWhere('pa.id_poll_answer','!=', NULL);
                    })
                    ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'p.post_text as post_text',
                                'p.post_type as post_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'gp.id_group_post as id_group_post',
                                'g.id_group as id_group',
                                'g.group_name as group_name',
                                'g.section as section',
                                'g.semester as semester',
                                'gm.member_type as member_type',
                                'po.id_poll as id_poll',
                                'po.poll_text as poll_text',
                                'po.file_name as poll_file_name',
                                'po.start_time as start_time',
                                'po.end_time as end_time',
                                'po.allow_multiple_answers as allow_multiple_answers',
                                'po.poll_type as poll_type',
                                'po.activated as activated',
                                'pa.id_poll_answer as id_poll_answer',
                                'pa.id_poll_option as id_poll_option',
                                'pa.poll_answer_description as poll_answer_description',
                                'p.created_at as created_at',
                                'p.updated_at as updated_at'
                            )
                    ->orderBy('po.end_time', 'desc')
                    ->skip($nSkipRecords)
                    ->take($nLimit)
                    ->get();
    }
    
    public static function getUserPolls($nIdUser)
    {
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('polls as po',function($join) {
                        $join->on('p.id_post', '=', 'po.id_entity')
                             ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                    })
                    ->leftJoin('poll_answers as pa',function($join) {
                        $join->on('po.id_poll', '=', 'pa.id_poll')
                             ->where('pa.id_user','=',Auth::user()->id_user);
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->leftJoin('groups as g', 'gp.id_group', '=', 'g.id_group')
                    ->leftJoin('group_members as gm', function($join) {
                                $join->on('gm.id_group', '=', 'g.id_group')
                                     ->where('gm.id_user','=',Auth::user()->id_user);
                            })
                    ->where('p.id_user', '=', $nIdUser)
                    ->where('p.post_type', '=', config('constants.POSTTYPEPOLL'))
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'p.post_text as post_text',
                                'p.post_type as post_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'gp.id_group_post as id_group_post',
                                'g.id_group as id_group',
                                'g.group_name as group_name',
                                'g.section as section',
                                'g.semester as semester',
                                'gm.member_type as member_type',
                                'po.id_poll as id_poll',
                                'po.poll_text as poll_text',
                                'po.file_name as poll_file_name',
                                'po.start_time as start_time',
                                'po.end_time as end_time',
                                'po.allow_multiple_answers as allow_multiple_answers',
                                'po.poll_type as poll_type',
                                'po.activated as activated',
                                'pa.id_poll_answer as id_poll_answer',
                                'pa.id_poll_option as id_poll_option',
                                'pa.poll_answer_description as poll_answer_description',
                                'p.created_at as created_at',
                                'p.updated_at as updated_at'
                            )
                    ->orderBy('p.updated_at', 'desc')
                    ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getGroupUncomplitedPollCount($nIdGroup, $dToday)
    {
        if(!$nIdGroup)
            return FALSE;
        
        $aWhereParams = [
                            'gp.id_group' => $nIdGroup,
                            'p.post_type' => config('constants.POSTTYPEPOLL'),
                            'p.activated' => 1,
                            'p.deleted' => 0,
                            'po.activated' => 1,
                            'po.deleted' => 0
                        ];
        return Post::from( 'group_posts as gp' )
                        ->leftJoin('posts as p', 'gp.id_post', '=', 'p.id_post')
                        ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                        ->leftJoin('polls as po',function($join) {
                            $join->on('p.id_post', '=', 'po.id_entity')
                                 ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                        })
                        ->leftJoin('poll_answers as pa',function($join) {
                            $join->on('po.id_poll', '=', 'pa.id_poll')
                                 ->where('pa.id_user','=',Auth::user()->id_user);
                        })
                        ->where($aWhereParams)
                        ->where('po.start_time','<=',$dToday)
                        ->where('po.end_time','>',$dToday)
                        ->where('pa.id_poll_answer',NULL)
                        ->count();
    }
    
    public static function getGroupComplitedPollCount($nIdGroup, $dToday)
    {
        if(!$nIdGroup)
            return FALSE;
        
        $aWhereParams = [
                            'gp.id_group' => $nIdGroup,
                            'p.post_type' => config('constants.POSTTYPEPOLL'),
                            'p.activated' => 1,
                            'p.deleted' => 0,
                            'po.activated' => 1,
                            'po.deleted' => 0
                        ];
        return Post::from( 'group_posts as gp' )
                        ->leftJoin('posts as p', 'gp.id_post', '=', 'p.id_post')
                        ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                        ->leftJoin('polls as po',function($join) {
                            $join->on('p.id_post', '=', 'po.id_entity')
                                 ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                        })
                        ->leftJoin('poll_answers as pa',function($join) {
                            $join->on('po.id_poll', '=', 'pa.id_poll')
                                 ->where('pa.id_user','=',Auth::user()->id_user);
                        })
                        ->where($aWhereParams)
                        ->where('po.start_time','<=',$dToday)
                        ->where(function ($query) use($dToday) {
                            $query->where('po.end_time','<',$dToday)
                                  ->orWhere('pa.id_poll_answer','!=', NULL);
                        })
                        ->count();
    }
    
    public static function getGroupUncomplitedPolls($nIdGroup, $dToday, $nSkipRecords, $nLimit)
    {
        if(!$nIdGroup)
            return FALSE;
        
        $aWhereParams = [
                            'gp.id_group' => $nIdGroup,
                            'p.post_type' => config('constants.POSTTYPEPOLL'),
                            'p.activated' => 1,
                            'p.deleted' => 0,
                            'po.activated' => 1,
                            'po.deleted' => 0
                        ];
        return Post::from( 'group_posts as gp' )
                        ->leftJoin('posts as p', 'gp.id_post', '=', 'p.id_post')
                        ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                        ->leftJoin('polls as po',function($join) {
                            $join->on('p.id_post', '=', 'po.id_entity')
                                 ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                        })
                        ->leftJoin('poll_answers as pa',function($join) {
                            $join->on('po.id_poll', '=', 'pa.id_poll')
                                 ->where('pa.id_user','=',Auth::user()->id_user);
                        })
                        ->where($aWhereParams)
                        ->where('po.start_time','<=',$dToday)
                        ->where('po.end_time','>',$dToday)
                        ->where('pa.id_poll_answer',NULL)
                        ->select(
                                    'p.id_post as id_post',
                                    'p.id_user as id_user',
                                    'p.post_text as post_text',
                                    'p.post_type as post_type',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'gp.id_group_post as id_group_post',
                                    'po.id_poll as id_poll',
                                    'po.poll_text as poll_text',
                                    'po.file_name as poll_file_name',
                                    'po.start_time as start_time',
                                    'po.end_time as end_time',
                                    'po.allow_multiple_answers as allow_multiple_answers',
                                    'po.poll_type as poll_type',
                                    'po.activated as activated',
                                    'pa.id_poll_answer as id_poll_answer',
                                    'pa.id_poll_option as id_poll_option',
                                    'pa.poll_answer_description as poll_answer_description',
                                    'p.created_at as created_at',
                                    'p.updated_at as updated_at'
                                )
                        ->orderBy(DB::raw('TIMEDIFF(po.end_time, NOW())'), 'asc')
                        ->skip($nSkipRecords)
                        ->take($nLimit)
                        ->get();
    }
    
    public static function getGroupAllPolls($nIdGroup, $dToday, $nSkipRecords, $nLimit)
    {
        if(!$nIdGroup)
            return FALSE;
        
        $aWhereParams = [
                            'gp.id_group' => $nIdGroup,
                            'p.post_type' => config('constants.POSTTYPEPOLL'),
                            'p.activated' => 1,
                            'p.deleted' => 0,
                            'po.activated' => 1,
                            'po.deleted' => 0
                        ];
        return Post::from( 'group_posts as gp' )
                        ->leftJoin('posts as p', 'gp.id_post', '=', 'p.id_post')
                        ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                        ->leftJoin('polls as po',function($join) {
                            $join->on('p.id_post', '=', 'po.id_entity')
                                 ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                        })
                        ->leftJoin('poll_answers as pa',function($join) {
                            $join->on('po.id_poll', '=', 'pa.id_poll')
                                 ->where('pa.id_user','=',Auth::user()->id_user);
                        })
                        ->where($aWhereParams)
                        ->where('po.start_time','<=',$dToday)
                        ->where(function ($query) use($dToday) {
                            $query->where('po.end_time','<',$dToday)
                                  ->orWhere('pa.id_poll_answer','!=', NULL);
                        })
                        ->select(
                                    'p.id_post as id_post',
                                    'p.id_user as id_user',
                                    'p.post_text as post_text',
                                    'p.post_type as post_type',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'gp.id_group_post as id_group_post',
                                    'po.id_poll as id_poll',
                                    'po.poll_text as poll_text',
                                    'po.file_name as poll_file_name',
                                    'po.start_time as start_time',
                                    'po.end_time as end_time',
                                    'po.allow_multiple_answers as allow_multiple_answers',
                                    'po.poll_type as poll_type',
                                    'po.activated as activated',
                                    'pa.id_poll_answer as id_poll_answer',
                                    'pa.id_poll_option as id_poll_option',
                                    'pa.poll_answer_description as poll_answer_description',
                                    'p.created_at as created_at',
                                    'p.updated_at as updated_at'
                                )
                        ->orderBy('po.end_time', 'desc')
                        ->skip($nSkipRecords)
                        ->take($nLimit)
                        ->get();
    }
}