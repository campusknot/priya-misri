<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class ReadNotification extends Model
{
    protected $table = 'read_notifications';
    protected $primaryKey = 'id_read_notification';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_notification'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
