<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Libraries\Poll;
use Auth;

class PollAnswer extends Model
{
    protected $table = 'poll_answers';
    protected $primaryKey = 'id_poll_answers';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_poll', 'id_user', 'id_poll_option', 'poll_answer_description'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function getPollAnswer($nIdPoll) {
        return PollAnswer::where('id_poll', $nIdPoll)
                            ->select(
                                    'id_poll_option as id_poll_option',
                                    DB::raw('count(*) as total')
                                    )
                            ->groupBy('id_poll_option')
                            ->get(); 
    }
    
    public static function getPollAllAnswer($nIdPoll) {
        return PollAnswer::from( 'poll_answers as pa' )
                            ->leftJoin('users as u', 'pa.id_user', '=', 'u.id_user')
                            ->where('pa.id_poll', $nIdPoll)
                            ->select(
                                    'pa.id_poll_option as id_poll_option',
                                    'pa.id_user as id_user',
                                    'pa.poll_answer_description as poll_answer_description',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    'u.email as email'
                                    )
                            ->orderBy('u.first_name','asc')
                            ->get(); 
    }
    
    public static function getPollGroupMemberAnswer($nIdPoll,$nIdGroup)
    {
        $oPoll = Poll::find($nIdPoll);
        $dEndDate = $oPoll->end_time;
        $dStartDate = $oPoll->start_time;
        $oMembers = PollAnswer::from( 'group_members as gm' )
                            ->leftJoin('poll_answers as pa', function($query) use($nIdPoll){
                                        return $query->on('gm.id_user', '=', 'pa.id_user')
                                                    ->where('pa.id_poll','=', $nIdPoll);
                                        })
                            ->leftJoin('users as u', 'gm.id_user', '=', 'u.id_user')
                            ->where(function($query) use($dEndDate,$dStartDate){
                                return $query->whereNotNull('pa.id_poll_answer')
                                            ->orWhere(function($query) use($dStartDate){
                                                            $query->where('gm.updated_at','>',$dStartDate)
                                                                   ->where('gm.activated','=',0)
                                                                   ->where('gm.deleted','=',1);
                                                        })
                                            ->orWhere(function($query) use($dEndDate){      
                                                        return $query->where('gm.activated', '=', 1)
                                                                      ->where('gm.deleted', '=', 0)
                                                                      ->where('gm.updated_at','<',$dEndDate);
                                                });
                                    })
                            ->where('gm.id_group','=',$nIdGroup)
                            ->select(
                                    'pa.id_poll_option as id_poll_option',
                                    'pa.id_user as id_user',
                                    'pa.poll_answer_description as poll_answer_description',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    'u.email as email'
                                    )
                            ->orderBy('u.first_name','asc');
//        dd($oMembers);
//       $qry = str_replace(array('%', '?'), array('%%', '%s'), $oMembers->toSql());
//       $qry = vsprintf($qry, $oMembers->getBindings());
//       dd($qry);
        return $oMembers->get();
    }
}
