<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserFollows extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_follower', 'id_following'
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_follows';
    protected $primaryKey = 'id_user_follow';


    public function getRouteKeyName()
    {
        return 'id_user_follow';
    }

    /**
     * Get the user_following that owns the following user.
     */
    public function user_following()
    {
        return $this->hasOne(User::class , "id_user" ,"id_following")
            ->select('id_user' ,'first_name' ,'last_name' ,'id_campus');
    }


    /**
     * Get the user_follower that owns the following user.
     */
    public function user_follower()
    {
        return $this->hasOne(User::class , "id_user" , "id_follower");
    }

    public static function isUserFollowing($nIdFollower , $nIdFollowing)
    {
        return UserFollows::where('id_follower','=' , $nIdFollower)
            ->where('id_following','=' , $nIdFollowing)
            ->get();
    }

    public static function getUserFollower($nUserId)
    {
        return UserFollows::with('user_follower.profile_image')
            ->where('id_following','=' , $nUserId)
            ->get();
    }

    public static function getUserFollowing($nUserId)
    {
        return UserFollows::with('user_following.profile_image')
            ->where('id_follower','=',$nUserId)
            ->get();
    }
}
