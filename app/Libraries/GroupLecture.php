<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;

class GroupLecture extends Model
{
    protected $table = 'group_lectures';
    protected $primaryKey = 'id_group_lecture';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_group', 'semester', 'activated', 'deleted'
    ];
    //not in use 
    public static function CheckTodayLectureCount($oRequest) 
    {
        $oGroupLecture= GroupLecture::where('id_group', $oRequest->id_group)
                        ->where('semester', $oRequest->semester)
                        ->where('activated', 1)
                        ->where('deleted', 0)
                        ->orderBy('id_group_lecture', 'desc')
                        ->select('created_at as created_at')
                        ->first();
        $dLectureDate = '';
        if(count($oGroupLecture))
        {
            $dLectureDate = Carbon::createFromFormat('Y-m-d H:i:s', $oGroupLecture->created_at);
            $dLectureDate = $dLectureDate->setTimezone(Auth::user()->timezone)->toDateString();
        }
        if($dLectureDate == Carbon::now(Auth::user()->timezone)->toDateString())
            return 1;
        else
            return 0;
    }
    
    //list of lecture with diffrent semster in different semester
    public static function getGroupAttendanceList($nUserId, $aUserGroupIds)
    {
        $iQuery = GroupLecture::from('group_lectures as gl')
                        ->leftJoin('groups as g', 'gl.id_group', '=', 'g.id_group')
                        ->leftJoin('group_members as gm',function ($query) use ($nUserId) {
                                                        $query->on('gm.id_group' ,'=', 'g.id_group')
                                                              ->where('gm.id_user' ,'=', $nUserId)
                                                              ->where('gm.activated' ,'=', 1)
                                                              ->where('gm.deleted' ,'=', 0);
                                                        })
                        ->leftJoin('users as u', 'g.id_user', '=', 'u.id_user')
                        ->whereIn('gl.id_group', $aUserGroupIds)
                        ->where('gl.activated',1)
                        ->where('gl.deleted',0)
                        ->select
                                (
                                    //DB::raw('max(gl.created_at) as created_at'),
                                    'gl.id_group_lecture as id_group_lecture',
                                    'gl.id_user as id_user',
                                    'gl.id_group as id_group',
                                    'g.group_name as group_name',
                                    'g.section as section',
                                    'gl.semester as semester',
                                    'gm.member_type as member_type',
                                    'u.id_user as id_creator',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name'
                                )
                        ->orderBy('gl.created_at','desc');
                                                        
        return GroupLecture::from(DB::raw("({$iQuery->toSql()}) as lecture"))
                        ->select('*')
                        ->mergeBindings( $iQuery->getQuery() )
                        ->orderBy('lecture.id_group_lecture','desc')
                        ->groupBy('lecture.id_group','lecture.semester')
                        ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getLectureDetail($aData,$nUserId) 
    {
        $aWhereParams = [
                            'gl.activated' => 1,
                            'gl.deleted' => 0
                        ];
        
        $oLecture= GroupLecture::from( 'group_lectures as gl' )
                        ->leftJoin('groups as g', 'gl.id_group', '=', 'g.id_group')
                        ->where($aWhereParams)
                        ->select(
                                'g.id_group as id_group',
                                'g.group_name as group_name',
                                'g.id_group_category as id_group_category',
                                'g.section as section',
                                'gl.semester as semester',
                                'gl.id_group_lecture as id_group_lecture',
                                'gl.created_at as created_at',
                                DB::raw('(SELECT count(*) FROM user_group_attendance as ua WHERE ua.id_group_lecture = gl.id_group_lecture AND ua.attendance_type = "P" AND ua.activated = 1 AND ua.deleted = 0 ) as present_student')
                              );
       
        if(empty($aData['from']))
        {
            $oLecture->where('gl.id_group_lecture',$aData['id_lecture']);
            $oLecture->limit(1);
        }
        else
        {
            //$dStartdate=Carbon::createFromFormat('m-d-Y', $aData['from'])->toDateString();
            //$dEnddate=Carbon::createFromFormat('m-d-Y', $aData['to'])->toDateString();
            $semester=$aData['semester'];
            $oLecture->where('gl.created_at', '>=',$aData['from']);
            $oLecture->where('gl.created_at', '<=',$aData['to']);
            $oLecture->where('gl.semester', '=',$semester);
            $oLecture->where('gl.id_group', '=',$aData['id_group']);
            $oLecture->orderBy('gl.created_at', 'asc');
        }
        return $oLecture->get();
    }
    
    public static function getLectureData($nIdLecture)
    {
        return GroupLecture::from( 'group_lectures as gl' )
                                ->leftJoin('groups as g', 'gl.id_group', '=', 'g.id_group')
                                ->leftJoin('users as u', 'g.id_user', '=', 'u.id_user')
                                ->where('gl.id_group_lecture',$nIdLecture)  
                                ->where('gl.activated',1) 
                                ->where('gl.deleted',0)
                                ->select(
                                            'gl.id_group_lecture as id_group_lecture',
                                            'gl.id_user as id_user',
                                            'gl.id_group as id_group',
                                            'gl.semester as semester',
                                            'g.group_name as group_name',
                                            'g.section as section',
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            'gl.created_at as created_at'
                                        )
                                ->first();
    }
    
    public static function getAllSameLecture($oLacture,$nUserId)
    { 
        return Lecture::from( 'group_lectures as gl' )
                      ->leftJoin('user_group_attendance as uga',function($join) use ($nUserId){ 
                                    $join->on('gl.id_group_lecture', '=', 'uga.id_group_lecture')      
                                         ->where('uga.id_user','=', $nUserId );      
                                })
                      ->where('gl.id_group',$oLacture->id_group)  
                      ->where('gl.semester',$oLacture->semester)
                      ->where('gl.activated',1) 
                      ->where('gl.deleted',0) 
                      ->select
                            (
                                'gl.id_group_lecture as id_group_lecture',
                                'gl.id_user as id_user',
                                'gl.id_group as id_group',
                                'gl.semester as semester',
                                'gl.created_at as created_at',
                                'uga.id_user_group_attendance as id_user_group_attendance',
                                'uga.attendance_type as attendance_type',
                                DB::raw('(SELECT count(gal.id_group_attendance_log) FROM group_attendance_logs as gal WHERE uga.id_user_group_attendance = gal.id_user_group_attendance) as count_id_attendance_log')
                            )
                      ->get();
    }
    
    //for excel getting start date and date of semester attendance
    public static function getLecture($id_group,$userid,$semester,$orderby)
    {
        return GroupLecture::from( 'group_lectures as gl' )
                        ->where('gl.id_group',$id_group) 
                        ->where('gl.semester',$semester)
                        ->where('gl.activated',1) 
                        ->where('gl.deleted',0)
                        ->orderBy('gl.id_group_lecture',$orderby)
                        ->select(
                                    'gl.created_at as created_at',
                                    'gl.id_group_lecture as id_group_lecture'
                                )
                        ->first();
    }
    
     public static function checkMemberExists($oLecture,$nIdUser) 
    {
        return GroupLecture::from( 'group_lectures as gl' )
                    ->join('user_group_attendance as uga', 'uga.id_group_lecture', '=', 'gl.id_group_lecture')
                    ->where('gl.id_group',$oLecture->id_group)
                    ->where('gl.semester',$oLecture->semester)
                    ->where('uga.id_user',$nIdUser)
                    ->where('gl.activated',1) 
                    ->where('gl.deleted',0)
                    ->count();
    }
    
    public static function getGroupLecturesCount($oGroupLacture,$nUserId,$sAttendanceType="")
    { 
        return Lecture::from( 'group_lectures as gl' )
                        ->leftJoin('user_group_attendance as uga',function($join) use ($nUserId){ 
                                      $join->on('gl.id_group_lecture', '=', 'uga.id_group_lecture')      
                                           ->where('uga.id_user','=', $nUserId );      
                                  })
                        ->where('gl.id_group',$oGroupLacture->id_group)  
                        ->where('gl.semester',$oGroupLacture->semester)  
                        ->where('gl.activated',1) 
                        ->where('gl.deleted',0)
                        ->when($sAttendanceType, function($query) use ($sAttendanceType) {
                            return $query->where('uga.id_user_group_attendance', '!=', NULL)
                                        ->where('uga.attendance_type', '=', $sAttendanceType);
                        })
                        ->select
                            (
                                'gl.id_group_lecture as id_group_lecture'
                            )
                        ->count();
    }
    public static function getGroupLecturesIds($oGroupLacture,$nUserId,$sAttendanceType="")
    { 
        $query= Lecture::from( 'group_lectures as gl' )
                        ->leftJoin('user_group_attendance as uga',function($join) use ($nUserId){ 
                                      $join->on('gl.id_group_lecture', '=', 'uga.id_group_lecture')      
                                           ->where('uga.id_user','=', $nUserId );      
                                  })
                        ->where('gl.id_group',$oGroupLacture->id_group)  
                        ->where('gl.semester',$oGroupLacture->semester)  
                        ->where('gl.activated',1) 
                        ->where('gl.deleted',0)
                        ->where('uga.id_user_group_attendance', '!=', NULL)
                        ->when(!empty($sAttendanceType), function($query) use ($sAttendanceType) {
                            return $query->where('uga.attendance_type', '=', $sAttendanceType);
                        })
                        ->select
                            (
                                DB::raw("GROUP_CONCAT(uga.id_group_lecture) as id_lectures"),
                                DB::raw("GROUP_CONCAT(uga.id_user_group_attendance) as ids_user_group_attendance")
                            );
        return $query->get();
    }
    
    public static function getAllGroupLectureCount($oGroupLacture)
    {
        return Lecture::from( 'group_lectures as gl' )
                        ->where('gl.id_group',$oGroupLacture->id_group)  
                        ->where('gl.semester',$oGroupLacture->semester)  
                        ->where('gl.activated',1) 
                        ->where('gl.deleted',0)
                        ->count();
    }
    
    public static function getGroupLectureData($nIdLecture)
    {
        //if lecture is deleted than also get data of lecture
        return GroupLecture::from( 'group_lectures as gl' )
                                ->leftJoin('groups as g', 'gl.id_group', '=', 'g.id_group')
                                ->leftJoin('users as u', 'g.id_user', '=', 'u.id_user')
                                ->where('gl.id_group_lecture',$nIdLecture)
                                ->select(
                                            'gl.id_group_lecture as id_group_lecture',
                                            'gl.id_user as id_user',
                                            'gl.id_group as id_group',
                                            'gl.semester as semester',
                                            'gl.activated as activated',
                                            'g.group_name as group_name',
                                            'g.section as section',
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            'gl.created_at as created_at'
                                        )
                                ->first();
    }
}