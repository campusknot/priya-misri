<?php

namespace App\Libraries;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class CorrectAnswer extends Model
{
    /**
     * For define table name
     */
    protected $table = 'correct_answers';
    protected $primaryKey = 'id_correct_answer';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_quiz_question', 'id_answer_option', 'answer_description', 'activated', 'deleted'
                        ];
    
}