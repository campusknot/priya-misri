<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class DocumentPermisssion extends Model
{
    /**
     * For define table name
     */
    protected $table = 'document_permissions';
    protected $primaryKey = 'id_document_permission';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_document', 'id_user', 'id_group', 'permission_type', 'activated', 'deleted' ];
   
}