<?php

namespace App\Libraries;

use App\Libraries\Campus;
use App\Libraries\UserCourse;
use App\Libraries\UserProfileImages;
use App\Libraries\UserPaymentDetail;
use App\Libraries\CampusContract;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Auth;
use Illuminate\Support\Facades\DB;
class User extends Authenticatable
{
    protected $table = 'users';
    protected $primaryKey = 'id_user';
    protected $appends = array(
        'id_university', 'course_count', 'user_profile_image', 'id_user_followings',
        'id_user_groups', 'timezone', 'is_paid_user'
        );
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'first_name', 'last_name', 'email', 'alternate_email', 'password', 'salt', 'id_campus', 'user_type', 'verified', 'verification_key', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];
    
    //id_university attribute acceser
    public function getIdUniversityAttribute()
    {
        $oCampus = Campus::where('id_campus', $this->id_campus)
        ->select('id_university')
        ->first();
        return (isset($oCampus) && $oCampus->id_university != NULL) ? $oCampus->id_university : '';
    }
    
    //course_count attribute acceser
    public function getCourseCountAttribute()
    {
        return UserCourse::where('id_user', $this->id_user)
        ->count();
    }
    
    //user_profile_image attribute acceser
    public function getUserProfileImageAttribute()
    {
        $oUserProfileImage = UserProfileImages::where('id_user', $this->id_user)
        ->where('activated', 1)
        ->where('deleted', 0)
        ->select('file_name')
        ->orderBy('updated_at', 'desc')
        ->first();
        return (isset($oUserProfileImage) && $oUserProfileImage->file_name != NULL) ? $oUserProfileImage->file_name : '';
    }
    
    //id_paid_user attribute acceser
    public function getIsPaidUserAttribute()
    {
        if($this->user_type == config('constants.USERTYPEFACULTY'))
            return TRUE;
        
        $nCampusContract = CampusContract::where('id_campus', $this->id_campus)
        ->where('contract_end_on','>', date('Y-m-d H:i:s'))
        ->where('activated', 1)
        ->where('deleted', 0)
        ->count();
        if($nCampusContract > 0)
            return TRUE;
        
        $nPaidUser = UserPaymentDetail::where('id_user', $this->id_user)
        ->where('contract_end_on','>', date('Y-m-d H:i:s'))
        ->where('activated', 1)
        ->where('deleted', 0)
        ->count();
        if($nPaidUser > 0)
            return TRUE;
        return FALSE;
    }
    
    //user_profile_image attribute acceser
    public function getTimezoneAttribute()
    {
        $oCampusTimeZone = Campus::where('id_campus', $this->id_campus)
        ->select('timezone')
        ->first();
        return (!empty($oCampusTimeZone) && $oCampusTimeZone->timezone != NULL) ? $oCampusTimeZone->timezone : '';
    }
    
    public function getIdUserFollowingsAttribute()
    {
        if(!Auth::guard('admin')->check()){
            $oUserFollowings = UserFollow::getUserFollowIds(Auth::user()->id_user);
            $aUserFollowings = !empty($oUserFollowings[0]->id_followings) ? explode(",", $oUserFollowings[0]->id_followings) : array();
            array_push($aUserFollowings, Auth::user()->id_user);
            return $aUserFollowings;
        }
    }
    
    public function getIdUserGroupsAttribute()
    {
        if(!Auth::guard('admin')->check()){
            $oUserGroups = GroupMember::getUserGroupIds(Auth::user()->id_user);
            $aUserGroups = explode(",", $oUserGroups[0]->id_groups);
            return $aUserGroups;
        }
    }

    public function getUserDetail($sField, $sFieldValue)
    {
        return $this->where($sField, '=', $sFieldValue)
        ->where('activated', '=', 1)
        ->where('deleted', '=', 0)
        ->first();
    }

    public static function getUserBasicDetail($nUserId)
    {
        return User::with('campus')
        ->where('id_user','=' , $nUserId)
        ->select(
            'id_user' ,
            'first_name',
            'last_name',
            'id_campus',
            'aspiration',
            DB::raw('(SELECT file_name FROM user_profile_images as up WHERE up.id_user = users.id_user AND up.activated = 1 AND up.deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_pic')
            )
        ->first();
    }

    public static function getUserProfileDetailAll($nUserId)
    {
        return $aUserProfileDetail = array(
            'education_list'         => UserEducation::getUserEducation($nUserId),
            'work_experience_list'   => UserWorkExperience::getUserWorkExperience($nUserId),
            'organization_list'      => UserOrganization::getUserOrganization($nUserId),
            'award_list'             => UserAwards::getUserAwards($nUserId),
            'journal_list'           => UserJournals::getUserJournal($nUserId),
            'research_list'          => UserResearch::getUserResearch($nUserId),
            'publication_list'       => UserPublications::getUserPublication($nUserId)
            );
    }

    /**
     * Get all of the profile_images for the user.
     */
    public function profile_images()
    {
        return $this->hasMany(UserProfileImages::class , 'id_user')
        ->where('activated' , 1)
        ->where('deleted' , 0)
        ->select(['id_user_profile_image' ,'file_name' ,'id_user']);
    }
    public function profile_image()
    {
        return $this->hasOne(UserProfileImages::class , 'id_user')
        ->where('activated'  , 1)
        ->where('deleted' , 0)
        ->orderBy('updated_at', 'DESC')
        ->select('file_name as file_name')
        ->limit(1);
    }

    /**
     * Get all of the campus for the user.
     */
    public function campus()
    {
        return $this->belongsTo(Campus::class ,'id_campus' , 'id_campus')
        ->select(['id_campus' , 'campus_name' ,'country_name' ,'id_university']);
    }


    /**
     * Get all of the education for the user.
     */
    public function education()
    {
        return $this->hasMany(UserEducation::class , 'id_user');
    }

    /**
     * Get all of the work experience for the user.
     */
    public function work_experience()
    {
        return $this->hasMany(UserWorkExperience::class , 'id_user');
    }

    /**
     * Get all of the  Organization for the user.
     */
    public function organization()
    {
        return $this->hasMany(UserOrganization::class , 'id_user');
    }

    /**
     * Get all of the  Award for the user.
     */
    public function award()
    {
        return $this->hasMany(UserAwards::class , 'id_user');
    }

    /**
     * Get all of the  Journal for the user.
     */
    public function journal()
    {
        return $this->hasMany(UserJournals::class , 'id_user');
    }

    /**
     * Get all of the  Research for the user.
     */
    public function research()
    {
        return $this->hasMany(UserResearch::class , 'id_user');
    }

    /**
     * Get all of the  Notification for the user.
     */
    public function notification()
    {
        return $this->hasMany(Notification::class , 'id_user');
    }
    /**
     * Get all of the  Publications for the user.
     */
    public function publication()
    {
        return $this->hasMany(UserPublications::class , 'id_user');
    }

    /**
     * Get all of the  Event for the user.
     */
    public function event()
    {
        return $this->hasMany(Event::class , 'id_user');
    }
    public function event_user()
    {
        return $this->belongsToMany(User::class , "event_requests","id_user" ,"id_event");
    }

    public function event_feed_user()
    {
        return $this->belongsToMany(User::class , "event_feeds","id_user" ,"id_event");
    }
    
    public static function getConnectedUserSearch($sSearchParam , $nIdUser ,$nLimit)
    {
        $oConnectedUserList = User::getConnectedUser($nIdUser);
        $aConnectedUserList = explode(",", $oConnectedUserList[0]->id_users);
        
        return User::from('users as u')
        ->whereIn('u.id_user', $aConnectedUserList)
        ->where('u.activated', '=', 1)
        ->where('u.deleted', '=', 0)
        ->where(function($query) use($sSearchParam){
            $query->where('u.first_name', 'LIKE', $sSearchParam.'%')
            ->orWhere('u.last_name', 'LIKE', $sSearchParam.'%')
            ->orWhere('u.email', 'LIKE', $sSearchParam.'%');
        })
        ->select(
            'u.id_user as id_user',
            'u.first_name as first_name',
            'u.last_name as last_name',
            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
            )
        ->paginate($nLimit);
    }

    public static function getNotConnectedUserSearch($sSearchParam ,$nIdUser ,$nLimit)
    {
        $oConnectedUserList = User::getConnectedUser($nIdUser);
        $aConnectedUserList = explode(",", $oConnectedUserList[0]->id_users);
        
        return User::from('users as u')
        ->where('u.id_user', '!=', $nIdUser)
        ->where('u.id_campus', '=', Auth::user()->id_campus)
        ->where('u.activated', '=', 1)
        ->where('u.deleted', '=', 0)
        ->whereNotIn('u.id_user' , $aConnectedUserList)
        ->where(function($query) use($sSearchParam){
            $query->where('u.first_name', 'LIKE', $sSearchParam.'%')
            ->orWhere('u.last_name', 'LIKE', $sSearchParam.'%')
            ->orWhere('u.email', 'LIKE', $sSearchParam.'%');
        })
        ->select(
            'u.id_user as id_user',
            'u.first_name as first_name',
            'u.last_name as last_name',
            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
            )
        ->paginate($nLimit);
    }

    public static function getConnectedUser($nIdUser)
    {
        $aWhereParams[] = array('u.id_user', '!=', $nIdUser);
        $aWhereParams[] = array('u.activated', '=', 1);
        $aWhereParams[] = array('u.deleted', '!=', 0);
        
        return User::from( 'user_follows as uf' )
        ->leftJoin('users as u', function($join) {
            $join->on('uf.id_follower', '=','u.id_user' )
            ->orOn('uf.id_following','=','u.id_user');
        })
        ->where($aWhereParams)
        ->where(function($query) use($nIdUser){
            $query->where('uf.id_follower', '=', $nIdUser)
            ->orWhere('uf.id_following', '=', $nIdUser);
        })
        ->select(DB::raw('GROUP_CONCAT(DISTINCT u.id_user) as id_users'))
        ->get();
    }
    
    public static function getUsersFromCampus($nIdCampus, $dUpdatedAt)
    {
        if(empty($nIdCampus))
            return FALSE;
        
        $aWhereParams[] = array('u.id_campus', '=', $nIdCampus);
        $aWhereParams[] = array('u.updated_at', '>', $dUpdatedAt);
        
        return User::from( 'users as u' )
        ->where($aWhereParams)
        ->select(
            'u.id_user as id_user',
            'u.first_name as first_name',
            'u.last_name as last_name',
            'u.email as u.email',
            'u.alternate_email as alternate_email',
            'u.id_campus as id_campus',
            'u.user_type as user_type',
            'u.verified as verified',
            'u.activated as activated',
            'u.deleted as deleted',
            'u.created_at as created_at',
            'u.updated_at as updated_at'
            )
        ->get();
    }
    
    //check function
    public static function getUserSearch($sSearchStr,$nIdUser,$nIdCampus,$limit='')
    {
        $sSearchStr = trim($sSearchStr);
        $sSearchStr = preg_replace('/\s\s+/', ' ', $sSearchStr);
        $iQuery= User::from( 'users as u' )
                    ->where(function($query) use($sSearchStr){
                            $query->where('u.first_name', 'like', $sSearchStr.'%')
                                  ->orWhere('u.last_name', 'like', $sSearchStr.'%')
                                  ->orWhere('u.email', 'like', '%'.$sSearchStr.'%')
                                  ->orWhere(DB::raw('CONCAT_WS(" ", first_name, last_name)'), 'like', '%'.$sSearchStr.'%');
                })
                    ->leftJoin('user_follows as uf', function($join) use($nIdUser){
                        $join->on('u.id_user' ,'=', 'uf.id_following')
                             ->where('uf.id_follower','=',$nIdUser);
                    })
                    ->where('u.id_user','!=',$nIdUser)
                    ->where('u.id_campus','=',$nIdCampus)
                    ->where('u.activated',1)
                    ->where('u.deleted',0)
                    ->where('u.verified',1)
                    ->select(
                                'u.id_user as id_user',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                'u.user_type as user_type',
                                'u.email as email',
                                'uf.id_user_follow as id_user_follow',
                                DB::raw('(SELECT file_name FROM user_profile_images as up WHERE up.id_user = u.id_user AND up.activated = 1 AND up.deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_pic')
                            )
                    ->groupBy('u.id_user')
                    ->orderBy('u.first_name','asc');
        if($limit == '')
            return $iQuery->paginate(config('constants.PERPAGERECORDS'));
        else
        {
            return $iQuery->take($limit)
                          ->get();
        }
    }
    
    public static function getFacultyList($nIdUser, $nIdCampus)
    {
        return User::from( 'users as u' )
        ->leftJoin('user_follows as uf', function($join) use($nIdUser){
            $join->on('u.id_user' ,'=', 'uf.id_following')
            ->where('uf.id_follower', '=', $nIdUser);
        })
        ->where('u.id_user', '!=', $nIdUser)
        ->where('u.id_campus', '=', $nIdCampus)
        ->where('u.user_type', '=', config('constants.USERTYPEFACULTY'))
        ->where('u.activated',1)
        ->where('u.deleted',0 )
        ->whereNull('uf.id_user_follow')
        ->select(
            'u.id_user as id_user',
            'u.first_name as first_name',
            'u.last_name as last_name',
            'u.user_type as user_type',
            'u.email as email',
            'uf.id_user_follow as id_user_follow',
            DB::raw('(SELECT file_name FROM user_profile_images as up WHERE up.id_user = u.id_user AND up.activated = 1 AND up.deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_pic')
            )
        ->get();
    }
    
    /* admin functions */
    
    public static function getCampusUser($nIdCampus, $nSearchStr, $nOrderField, $nOrderBy){
        return User::from( 'users as u' )
        ->leftJoin('campuses as c', 'u.id_campus','=','c.id_campus')
        ->where('u.id_campus', '=', $nIdCampus)
        ->where('u.deleted', '=', FALSE)
        ->when($nSearchStr != "" || $nSearchStr != NULL, function ($query) use ($nSearchStr) {
                return $query->where(function ($query) use ($nSearchStr) {
                $query->where('u.email', 'like', '%'.$nSearchStr.'%')
                ->orWhere(DB::raw('CONCAT(first_name, " ", last_name)'), 'like', '%'.$nSearchStr.'%');
                if(preg_match("/^".$nSearchStr."/i", trans('messages.verified'), $match)){
                    $query->orWhere('u.verified', '=', true);
                }else if(preg_match("/^".$nSearchStr."/i", trans('messages.pending'), $match)){
                    $query->orWhere('u.verified', '=', false);
                }
            });
        })
        ->select(
            'u.id_user as id_user',
            'u.first_name as first_name',
            'u.last_name as last_name',
            'u.email as email',
            'u.alternate_email as alternate_email',
            'u.user_type as user_type',
            'u.verified as verified',
            'u.activated as activated',
            'u.deleted as deleted',
            'u.created_at as created_at',
            'u.updated_at as updated_at',
            'c.campus_name as campus_name',
            'c.id_campus as id_campus'
            )->orderBy($nOrderField, $nOrderBy)->paginate(config('constants.ADMIN_PERPAGERECORDS'));
    }
}
