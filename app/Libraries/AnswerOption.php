<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class AnswerOption extends Model
{
    /**
     * For define table name
     */
    protected $table = 'answer_options';
    protected $primaryKey = 'id_answer_option';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_quiz_question', 'answer_text', 'file_name', 'activated', 'deleted'
                        ];
    
    public static function checkQuizCreator($nIdAnswerOption) 
    {
        return  AnswerOption::from('answer_options as ao')
                            ->leftJoin('quiz_questions as qq','qq.id_quiz_question','=','ao.id_quiz_question')
                            ->leftjoin('quizzes as q','qq.id_quiz','=','q.id_quiz')
                            ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                            ->where('id_answer_option',$nIdAnswerOption)
                            ->select(
                                    'q.id_user as id_user',
                                    'qgi.id_group as id_group'
                                    )
                            ->first();
    }
    
}