<?php
namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class GoogleApiAccessToken extends Model
{
    protected $table = 'google_api_access_tokens';
    protected $primaryKey = 'id_google_api_access_token';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'access_token', 'refresh_token', 'token_type', 'created','expires_in'
    ];
}