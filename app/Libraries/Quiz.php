<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Quiz extends Model
{
    /**
     * For define table name
     */
    protected $table = 'quizzes';
    protected $primaryKey = 'id_quiz';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_user', 'quiz_title', 'description', 'duration',
                            'total_marks','show_in_mobile', 'activated', 'deleted'
                        ];
    
    public static function getFacultyCompletedQuizList($nIdUser ,$aCondition,$nIdGroup='',$sOrderBy) 
    {
        $aCondition['q.activated'] = 1;
        $aCondition['q.deleted'] = 0;
        //print_r($nIdGroup);
        return Quiz::from('quizzes as q')
                   ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                   ->leftJoin('groups as g','qgi.id_group','=','g.id_group')
                   ->where($aCondition)
                   ->whereIn('qgi.id_group',$nIdGroup)
                   ->select(
                           'q.id_quiz as id_quiz',
                           'q.quiz_title as quiz_title',
                           'q.duration as duration',
                           'q.total_marks as total_marks',
                           'q.activated as activated',
                           'g.group_name as group_name',
                           'qgi.start_time as start_time',
                           'qgi.end_time as end_time',
                           DB::raw('(SELECT count(*) as total_user FROM quiz_user_invite AS qui WHERE qui.id_quiz = q.id_quiz AND activated = 1 AND deleted = 0) as total_user'),
                           DB::raw('(SELECT SUM(qui.completed = 1) FROM quiz_user_invite AS qui WHERE qui.id_quiz = q.id_quiz AND activated = 1 AND deleted = 0) as completed_users'),
                           DB::raw('(SELECT SUM(qq.marks) FROM quiz_questions AS qq WHERE qq.id_quiz = q.id_quiz AND activated = 1 AND deleted = 0) as marks_question')
                           )
                   ->orderby('qgi.start_time',$sOrderBy)
                   ->paginate(config('constants.PERPAGERECORDS'));
    }
    public static function getFacultyUpcommingQuizList($nIdUser,$nIdGroup='',$sOrderBy,$dTime) 
    {
        return Quiz::from('quizzes as q')
                   ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                   ->leftJoin('groups as g','qgi.id_group','=','g.id_group')
                   ->where(function($query) use($dTime){
                            return $query->where('qgi.end_time', '>', $dTime)
                                         ->orWhere('q.activated',0);
                   })
                   ->where('q.deleted',0)
                   ->whereIn('qgi.id_group',$nIdGroup)
                   ->select(
                           'q.id_quiz as id_quiz',
                           'q.quiz_title as quiz_title',
                           'q.duration as duration',
                           'q.total_marks as total_marks',
                           'q.activated as activated',
                           'g.group_name as group_name',
                           'g.section as section',
                           'g.semester as semester',
                           'qgi.start_time as start_time',
                           'qgi.end_time as end_time',
                           DB::raw('(SELECT count(*) as total_user FROM quiz_user_invite AS qui WHERE qui.id_quiz = q.id_quiz AND activated = 1 AND deleted = 0) as total_user'),
                           DB::raw('(SELECT SUM(qui.completed = 1) FROM quiz_user_invite AS qui WHERE qui.id_quiz = q.id_quiz AND activated = 1 AND deleted = 0) as completed_users'),
                           DB::raw('(SELECT SUM(qq.marks) FROM quiz_questions AS qq WHERE qq.id_quiz = q.id_quiz AND activated = 1 AND deleted = 0) as marks_question')
                           )
                   ->orderby('qgi.start_time',$sOrderBy)
                   ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getFacultyQuizCount($nIdUser,$nIdGroup='') 
    {
        //$aCondition['q.activated'] = 1;
        $aCondition['q.deleted'] = 0;
        $dTime = Carbon::Now();
        $dTime->toDateTimeString();
        return Quiz::from('quizzes as q')
                   ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                   ->leftJoin('groups as g','qgi.id_group','=','g.id_group')
                   ->where(function($query) use($dTime){
                            return $query->where('qgi.end_time', '>', $dTime)
                                         ->orWhere('q.activated',0);
                   })
                   ->where('q.deleted',0)
                   ->whereIn('qgi.id_group',$nIdGroup)
                   ->count();
    }
    
    public static function getQuizDetail($nIdQuiz) 
    {
        $aCondition['q.deleted'] = 0;
        $aCondition['q.id_quiz'] = $nIdQuiz;
        return Quiz::from('quizzes as q')
                        ->leftJoin('users as u', 'q.id_user', '=', 'u.id_user')
                        ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                        ->leftJoin('groups as g', 'qgi.id_group', '=', 'g.id_group')
                        ->where($aCondition)
                        ->select(
                                    'q.id_quiz as id_quiz',
                                    'q.quiz_title as quiz_title',
                                    'q.description as description',
                                    'q.duration as duration',
                                    'q.total_marks as total_marks',
                                    'q.show_in_mobile as show_in_mobile',
                                    'q.activated as activated',
                                    'qgi.id_group as id_group',
                                    'qgi.start_time as start_time',
                                    'qgi.end_time as end_time',
                                    'g.group_name as group_name',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name'
                                )
                        ->first();
    }
    
    public static function getUpcomingUserQuizDetail($nIdUser, $nIdQuiz)
    {
        $aWhereConditions = [
                                'qui.id_user' => $nIdUser,
                                'qui.id_quiz' => $nIdQuiz,
                                'qui.completed' => 0,
                                'qui.activated' => 1,
                                'qui.deleted' => 0
                            ];
        return QuizUserInvite::from('quizzes as q')
                                ->leftJoin('users as u', 'q.id_user', '=', 'u.id_user')
                                ->leftJoin('quiz_group_invite as qgi', 'q.id_quiz', '=', 'qgi.id_quiz')
                                ->leftJoin('groups as g', 'qgi.id_group', '=', 'g.id_group')
                                ->where($aWhereConditions)
                                ->select(
                                            'q.id_quiz as id_quiz',
                                            'q.quiz_title as quiz_title',
                                            'q.duration as duration',
                                            'q.total_marks as total_marks',
                                            'g.group_name as group_name',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name'
                                        )
                                ->first();
    }
    
    public static function getUpcomingGroupQuiz($aCondition,$nIdGroup='') 
    {
        //$aCondition['q.activated'] = 1;
        $aCondition['q.deleted'] = 0;
        if(!empty($nIdGroup))
            $aCondition['qgi.id_group'] = $nIdGroup;
        return Quiz::from('quizzes as q')
                   ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                   ->where($aCondition)
                   ->select(
                           'q.id_quiz as id_quiz',
                           'q.id_user as id_user',
                           'qgi.start_time as start_time',
                           'qgi.end_time as end_time'
                          )
                   ->get();
    }
    
}