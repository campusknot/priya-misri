<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class AttendanceCode extends Model
{
    protected $table = 'attendance_codes';
    protected $primaryKey = 'id_attendance_code';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_lecture', 'attendance_code','entity_type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function getAttendanceCodeDetail($sAttendanceCode) 
    {
        return AttendanceCode::from( 'attendance_codes as ac' )
                      ->leftJoin('lectures as l', 'l.id_lecture', '=', 'ac.id_lecture')
                      ->where('attendance_code' , $sAttendanceCode)
                      ->where('entity_type' , config('constants.ATTENDANCETYPECOURSE'))
                      ->select(
                                'ac.attendance_code as attendance_code',
                                'l.section as section',
                                'l.id_course as id_course',
                                'l.id_lecture as id_lecture'
                              )
                      ->first();
    }
    public static function getGroupAttendanceCodeDetail($sAttendanceCode) 
    {
        return AttendanceCode::from( 'attendance_codes as ac' )
                      ->leftJoin('group_lectures as gl', 'gl.id_group_lecture', '=', 'ac.id_lecture')
                      ->where('attendance_code' , $sAttendanceCode)
                      ->where('entity_type' , config('constants.ATTENDANCETYPEGROUP'))
                      ->select(
                                'ac.attendance_code as attendance_code',
                                'gl.id_group as id_group',
                                'gl.id_group_lecture as id_lecture'
                              )
                      ->first();
    }
}