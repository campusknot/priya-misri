<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserPaymentDetail extends Model
{
    protected $table = 'user_payment_details';
    protected $primaryKey = 'id_user_payment_detail';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_transaction', 'device_type', 'contract_start_on', 'contract_end_on', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
