<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuizQuestion extends Model
{
    /**
     * For define table name
     */
    protected $table = 'quiz_questions';
    protected $primaryKey = 'id_quiz_question';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_quiz', 'question_text', 'question_type', 'file_name', 'marks', 'activated', 'deleted'
                        ];
    
    public static function getQuizQuestionCount($nIdQuiz) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                           ->where('qq.id_quiz',$nIdQuiz)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->count();
    }
    
    public static function getQuizQuestions($nIdQuiz) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                           ->leftJoin('correct_answers as ca','qq.id_quiz_question','=','ca.id_quiz_question')
                           ->where('qq.id_quiz',$nIdQuiz)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->select(
                                   'qq.id_quiz_question as id_quiz_question',
                                   'qq.question_text as question_text',
                                   'qq.question_type as question_type',
                                   'qq.file_name as file_name',
                                   'qq.marks as marks',
                                   'ca.id_answer_option as correct_answer'
                                   )
                           ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getQuizQuestionDetail($nIdQuizQuestion) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                           ->leftJoin('correct_answers as ca','qq.id_quiz_question','=','ca.id_quiz_question')
                           ->where('qq.id_quiz_question',$nIdQuizQuestion)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->select(
                                   'qq.id_quiz_question as id_quiz_question',
                                   'qq.question_text as question_text',
                                   'qq.question_type as question_type',
                                   'qq.file_name as file_name',
                                   'qq.marks as marks',
                                   'ca.id_answer_option as correct_answer'
                                   )
                           ->first();
    }
    
    public static function getUserQuizQuestions($nIdQuiz,$nIdUser) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                           ->leftJoin('correct_answers as ca','qq.id_quiz_question','=','ca.id_quiz_question')
                           ->leftJoin('user_answers as ua',function($join) use ($nIdUser){
                                        $join->on('qq.id_quiz_question','=','ua.id_quiz_question')
                                             ->where('ua.id_user','=',$nIdUser);
                           })
                           ->where('qq.id_quiz',$nIdQuiz)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->select(
                                   'qq.id_quiz_question as id_quiz_question',
                                   'qq.id_quiz as id_quiz',
                                   'qq.question_text as question_text',
                                   'qq.question_type as question_type',
                                   'qq.file_name as file_name',
                                   'qq.marks as marks',
                                   'ca.id_answer_option as correct_answer',
                                   'ua.id_answer_option as user_answer',
                                   'ua.user_answer_description as user_answer_description',
                                   'ua.mark_obtain as mark_obtain'
                                   )
                           ->paginate(config('constants.PERPAGERECORDS'));
    }
    public static function getQuizQuestionsMarksTotal($nIdQuiz) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                           ->where('qq.id_quiz',$nIdQuiz)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->sum('marks');
    }
    
    public static function getStudentTotalMarks($nIdQuiz,$nIdUser) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                            ->leftJoin('user_answers as ua','qq.id_quiz_question','=','ua.id_quiz_question')
                           ->where('qq.id_quiz',$nIdQuiz)
                            ->where('ua.id_user','=',$nIdUser)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->sum('mark_obtain');
    }
    
    public static function getQuizQuestion($nIdQuiz)
    {
        $aCondiation = ['id_quiz'=>$nIdQuiz,
                        'question_type' => config('constants.QUESTIONTYPEOPEN'),
                        'activated'=>1,
                        'deleted'=>0
                        ];
        return QuizQuestion::where($aCondiation)
                           ->select(DB::raw('GROUP_CONCAT(id_quiz_question) as id_quiz_question'))
                           ->get();
    }
    
    public static function getQuestionDetail($nIdQuestion) 
    {
        return QuizQuestion::from('quiz_questions as qq')
                            ->leftJoin('quizzes as q','qq.id_quiz','=','q.id_quiz')
                            ->leftJoin('quiz_group_invite as qgi','q.id_quiz','=','qgi.id_quiz')
                            ->where('qq.id_quiz_question',$nIdQuestion)
                           ->where('qq.activated',1)
                           ->where('qq.deleted',0)
                           ->select(
                                   'q.id_user as id_user',
                                   'qq.id_quiz as id_quiz',
                                   'qq.marks as marks',
                                   'qgi.id_group as id_group'
                                   )
                           ->first();
    }
}