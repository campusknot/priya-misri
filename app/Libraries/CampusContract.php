<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class CampusContract extends Model
{
    protected $table = 'campus_contracts';
    protected $primaryKey = 'id_campus_contract';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_campus', 'id_university', 'contract_start_on', 'contract_end_on', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
