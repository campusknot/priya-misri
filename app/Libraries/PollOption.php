<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class PollOption extends Model
{
    protected $table = 'poll_options';
    protected $primaryKey = 'id_poll_option';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_poll', 'option_text', 'rank', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function getPollOptions($nIdPoll)
    {
        return PollOption::where('id_poll', $nIdPoll)
                            ->where('activated', 1)
                            ->where('deleted', 0)
                            ->select(
                                        'id_poll_option as id_poll_option',
                                        'option_text as option_text',
                                        'rank as rank'
                                    )
                            ->orderBy('rank', 'asc')
                            ->get(); 
        
    }
}
