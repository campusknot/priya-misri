<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupMember extends Model
{
    protected $table = 'group_members';
    protected $primaryKey = 'id_group_member';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_group', 'member_type', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * 
     * @param type $nIdGroup
     * @return boolean
     */
    public function getMembersCount($nIdGroup)
    {
        if (!$nIdGroup)
            return FALSE;
        
        return $this->where('id_group', '=', $nIdGroup)
                    ->where('activated', '=', 1)
                    ->where('deleted', '=', 0)
                    ->count();
    }
    
    /**
     * This function gives all member ids of group and currently being use in event invite.
     * @param type $nIdGroup
     * @return boolean
     * @author Jay Pandya <jay.pandya@campusknot.com>
     */
    public static function getAllGroupMembers($nIdGroup)
    {
        if(empty($nIdGroup))
            return FALSE;
        
        return GroupMember::where('id_group', '=', $nIdGroup)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->select(
                                        'id_group_member',
                                        'id_group',
                                        'id_user',
                                        'member_type'
                                    )
                            ->get();
    }

    /**
     * 
     * @param type $nIdGroup
     * @return type
     */
    public static  function getGroupMembers($nIdGroup, $aMemberType = array(), $nIdUserLogedin = '',$sSearchStr='',$nExcel = '')
    {
        if(empty($nIdGroup))
            return FALSE;
        
        if(empty($nIdUserLogedin))
            $nIdUserLogedin = Auth()->user()->id_user;
        
        $aWhereParams = [
                            'gm.id_group' => $nIdGroup,
                            'gm.activated' => 1,
                            'gm.deleted' => 0
                        ];
        $sSearchStr = trim($sSearchStr);
        $sSearchStr = preg_replace('/\s\s+/', ' ', $sSearchStr);
        
        $iQuery= GroupMember::from('group_members as gm')
                    ->leftJoin('users as u',function($join) {
                        $join->on('gm.id_user', '=', 'u.id_user');
                    })
                    ->leftJoin('user_follows as uf', function($join) use($nIdUserLogedin){
                        $join->on('gm.id_user' ,'=', 'uf.id_following')
                             ->where('uf.id_follower','=',$nIdUserLogedin);
                    })
                    ->when(count($aMemberType), function ($query) use ($aMemberType) {
                        return $query->whereIn('gm.member_type', $aMemberType);
                    })
                    ->where($aWhereParams)
                    ->select(
                                'gm.id_group_member as id_group_member',
                                'gm.id_group as id_group',
                                'gm.id_user as id_user',
                                'gm.member_type as member_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                'u.user_type as user_type',
                                'u.email as email',
                                DB::raw('CONCAT(u.first_name, " ", u.last_name) as name'),
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'uf.id_user_follow as id_user_follow',
                                'gm.created_at as created_at',
                                'gm.updated_at as updated_at'
                            )
                    ->orderBy('u.first_name', 'asc')
                    ->groupBy('gm.id_user');
                    //->toSql();
                    
        if($sSearchStr != '')
        {
            $iQuery->where(function( $query ) use($sSearchStr) {
                                        $query->where('u.first_name', 'like', $sSearchStr.'%')
                                              ->orWhere('u.last_name', 'like', $sSearchStr.'%')
                                              ->orWhere('u.email', 'like', '%'.$sSearchStr.'%')
                                              ->orWhere(DB::raw('CONCAT_WS(" ", u.first_name, u.last_name)'), 'like', '%'.$sSearchStr.'%');
                                        });
        }
        if($nExcel =='')
            return $iQuery->paginate(config('constants.PERPAGERECORDS'));
        else
            return $iQuery->get();
    }
    
    /**
     * 
     * @param type $nIdGroup
     * @param type $nIdUser
     * @return boolean
     */
    public static function getGroupMemberDetail($nIdGroup = '', $nIdUser = '')
    {
        if(!strlen($nIdGroup) || !strlen($nIdUser))
            return FALSE;
        
        return GroupMember::from('group_members as gm')
                        ->leftJoin('groups as g', 'gm.id_group' ,'=', 'g.id_group')
                        ->leftJoin('users as u', 'gm.id_user' ,'=', 'u.id_user')
                        ->where('gm.id_group', '=', $nIdGroup)
                        ->where('gm.id_user', '=', $nIdUser)
                        ->where('gm.activated', '=', 1)
                        ->where('gm.deleted', '=', 0)
                        ->select(
                                    'gm.id_group_member as id_group_member',
                                    'gm.id_group as id_group',
                                    'gm.id_user as id_user',
                                    'gm.member_type as member_type',
                                    'g.group_name as group_name',
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.id_group_category as id_group_category',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    'u.email as email'
                                )
                        ->first();
    }
    
    /**
     * This function generaly use for get loggedin user's group ids and pass them into agregate post function.
     * @param type $nIdUser
     * @return type
     */
    public static function getUserGroupIds($nIdUser)
    {
        return GroupMember::where('id_user', '=', $nIdUser)
                            ->where('activated', '=', 1)
                            ->where('deleted', '=', 0)
                            ->select(DB::raw('GROUP_CONCAT(id_group) as id_groups'))
                            ->get();
    }
    
    public static function getUserSelectiveGroupIds($nIdUser, $nIdGroupCat, $sComparision = '=')
    {
        return GroupMember::from('group_members as gm')
                            ->leftJoin('groups as g', 'gm.id_group' ,'=', 'g.id_group')
                            ->where('g.id_group_category', $sComparision, $nIdGroupCat)
                            ->where('gm.id_user', '=', $nIdUser)
                            ->where('gm.activated', '=', 1)
                            ->where('gm.deleted', '=', 0)
                            ->select(DB::raw('GROUP_CONCAT(g.id_group) as id_groups'))
                            ->get();
    }
    
    public static function getGroupMemberSuggetions($sUserName, $nIdGroup, $nIdCampus)
    {
        return GroupMember::from('users as u')
                        ->leftJoin('group_members as gm', function($join) use($nIdGroup){
                            $join->on('u.id_user', '=', 'gm.id_user');
                            $join->on('gm.id_group', '=', DB::raw($nIdGroup));
                        })
                        ->leftJoin('group_requests as gr', function($join) use($nIdGroup){
                            $join->on('u.id_user', '=', 'gr.id_user_request_to');
                            $join->on('gr.id_group', '=', DB::raw($nIdGroup));
                        })
                        ->where(function ($query) use($sUserName) {
                            $query->where('u.email', 'LIKE', $sUserName.'%')
                                  ->orWhere('u.first_name', 'LIKE', $sUserName.'%')
                                  ->orWhere('u.last_name', 'LIKE', $sUserName.'%');
                        })
                        ->where('u.id_campus', '=', $nIdCampus)
                        ->where('u.activated', '=', 1)
                        ->where('u.deleted', '=', 0)
                        ->where('gm.id_group_member', '=', NULL)
                        ->where(function ($query) {
                            $query->where('gr.id_group_request', '=', NULL)
                                  ->orWhere('gr.member_status', '=', 'R');
                        })
                        ->select(
                                    'u.id_user as id_user',
                                    DB::raw('CONCAT(u.first_name, " ", u.last_name) as name'),
                                    'u.email as email'
                                )
                        ->groupBy('u.id_user')
                        ->take(5)->get();
    }

    public static function  getUserGroupsTab($nIdUser)
    {
        return GroupMember::from('group_members as gm')
                            ->leftJoin('groups as g', 'gm.id_group' ,'=', 'g.id_group')
                            ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                            ->where('gm.id_user', '=', $nIdUser)
                            ->where('g.activated', '=', 1)
                            ->where('g.deleted', '=', 0)
                            ->where('gm.activated', '=', 1)
                            ->where('gm.deleted', '=', 0)
                            ->select(
                                        'gm.id_group_member as id_group_member',
                                        'gm.id_group as id_group',
                                        DB::raw('IF(gm.member_type = "A" OR gm.member_type = "C", 1, 0) as is_admin'),
                                        'gm.member_type as member_type',
                                        'g.group_name as group_name',
                                        'g.section as section',
                                        'g.semester as semester',
                                        'g.group_type as group_type',
                                        'g.file_name as group_image',
                                        'gc.id_group_category as id_group_category',
                                        'gc.group_category_name as group_category_name',
                                        'gc.file_name as group_category_image',
                                        'gm.activated as member_activated',
                                        'gm.deleted as member_deleted',
                                        'g.activated as activated',
                                        'g.deleted as deleted',
                                        'g.created_at as created_at',
                                        'g.updated_at as updated_at'
                                    )
                            ->orderBy('g.group_name', 'ASC')
                            ->get();
    }
    
    public static function getGroupUserSuggetions($sUserName, $nIdGroup, $nMemberType)
    {
        return GroupMember::from('group_members as gm')
                            ->leftJoin('users as u', 'gm.id_user' ,'=', 'u.id_user')
                            ->where('gm.id_group', '=', $nIdGroup)
                            ->where('gm.member_type', '=', $nMemberType)
                            ->where('gm.activated', '=', 1)
                            ->where('gm.deleted', '=', 0)
                            ->where(function ($query) use($sUserName) {
                                                    $query->where('u.email', 'LIKE', $sUserName.'%')
                                                          ->orWhere('u.first_name', 'LIKE', $sUserName.'%')
                                                          ->orWhere('u.last_name', 'LIKE', $sUserName.'%');
                                                })
                            ->select(
                                        'gm.id_group_member as id_group_member',
                                        'gm.id_group as id_group',
                                        'gm.member_type as member_type',
                                        'u.id_user as id_user',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('CONCAT(u.first_name, " ", u.last_name) as name'),
                                        'u.email as email',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                    )
                            ->orderBy('u.first_name', 'ASC')
                            ->get();
    }
    
    public static function getMemberTypeGroupIds($nIdUser, $nIdGroupCat, $sComparision = '=',$sSearchFor = '')
    {
        $aWhere ='';
        if($sSearchFor == 'admin')
            $aWhere = 1;
        return GroupMember::from('group_members as gm')
                            ->leftJoin('groups as g', 'gm.id_group' ,'=', 'g.id_group')
                            ->where('g.id_group_category', $sComparision, $nIdGroupCat)
                            ->where('gm.id_user', '=', $nIdUser)
                            ->where(function ($query) use($aWhere){
                                    if( $aWhere == 1){
                                        $query->where('gm.member_type', '=', config('constants.GROUPADMIN'));
                                        $query->orWhere('gm.member_type', '=', config('constants.GROUPCREATOR'));
                                    }else
                                        $query->orWhere('gm.member_type', '=', config('constants.GROUPMEMBER'));
                                })
                            ->where('gm.activated', '=', 1)
                            ->where('gm.deleted', '=', 0)
                            ->select(DB::raw('GROUP_CONCAT(g.id_group) as id_groups'))
                            ->get();
    }
    
    public static  function getGroupMembersForAttendance($nIdGroup, $aMemberType = array(),$sSearchStr='',$nExcel='')
    {
        
        $aWhereParams = [
                            'gm.id_group' => $nIdGroup
                        ];
        
        $Query =GroupMember::from('group_members as gm')
                    ->leftJoin('users as u','gm.id_user', '=', 'u.id_user')
                    ->when(count($aMemberType), function ($query) use ($aMemberType) {
                        return $query->whereIn('gm.member_type', $aMemberType);
                    })
                    ->where($aWhereParams)
                    ->when(strlen($sSearchStr), function ($query) use ($sSearchStr) {
                        return $query->where(function( $query ) use($sSearchStr) {
                                    $query->where('u.first_name', 'like', $sSearchStr.'%')
                                            ->orWhere('u.last_name', 'like', $sSearchStr.'%')
                                            ->orWhere('u.email', 'like', '%'.$sSearchStr.'%')
                                            ->orWhere(DB::raw('CONCAT_WS(" ", u.first_name, u.last_name)'), 'like', '%'.$sSearchStr.'%');
                                });
                    })
                    ->select(
                                'gm.id_group_member as id_group_member',
                                'gm.id_group as id_group',
                                'gm.id_user as id_user',
                                'gm.member_type as member_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                'u.user_type as user_type',
                                'u.email as email',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'gm.activated as activated',
                                'gm.deleted as deleted'
                            )
                    ->orderBy('u.first_name', 'asc');
        if($nExcel == '')
            return $Query->paginate(config('constants.PERPAGERECORDS'));
        else
            return $Query->get();
        
    }

}
