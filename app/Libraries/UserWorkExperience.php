<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserWorkExperience extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'company_name', 'job_title',
        'job_description' , 'start_year' ,
        'end_year'  
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_work_experience';
    protected $primaryKey = 'id_user_work_experience';

     public function getRouteKeyName()
    {
        return 'id_user_work_experience';
    }


	/**
     * Get the user that owns the education.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserWorkExperience($nUserId, $nLimit = 0)
    {
        return  UserWorkExperience::where('id_user', $nUserId)
                                    ->where('activated', 1)
                                    ->where('deleted', 0)
                                    ->orderBy('start_year', 'desc')
                                    ->when($nLimit > 0, function ($query) use ($nLimit) {
                                        return $query->take($nLimit);
                                    })
                                    ->get();
    }


}
