<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class GroupCategory extends Model
{
    protected $table = 'group_categories';
    protected $primaryKey = 'id_group_category';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_category_name', 'file_name'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}
