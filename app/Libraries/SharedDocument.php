<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class SharedDocument extends Model
{
    /**
     * For define table name
     */
    protected $table = 'shared_documents';
    protected $primaryKey = 'id_shared_document';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_document', 'id_user', 'shared_with', 'activated', 'deleted' ];
    
    /**
     * 
     * @param type $sSearchTerm
     * @param type $nIdDocument
     * @param type $nIdCampus
     * @return type
     */
    public static function getUserSuggetionsForShare($sSearchTerm, $nIdDocument, $nIdCampus)
    {
        return SharedDocument::from( 'users as u' )
                                ->leftJoin('shared_documents as sd',function($join) use($nIdDocument){
                                    $join->on('u.id_user', '=', 'sd.shared_with');
                                    $join->on('sd.id_document', '=', DB::raw($nIdDocument));
                                })
                                ->where(function ($query) use($sSearchTerm) {
                                    $query->where('u.email', 'LIKE', $sSearchTerm.'%')
                                          ->orWhere('u.first_name', 'LIKE', $sSearchTerm.'%')
                                          ->orWhere('u.last_name', 'LIKE', $sSearchTerm.'%');
                                })
                                ->where('sd.id_shared_document', '=', NULL)
                                ->where('u.id_campus', '=', $nIdCampus)
                                ->where('u.activated', '=', 1)
                                ->where('u.deleted', '=', 0)
                                ->select(
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            DB::raw('CONCAT(u.first_name, " ", u.last_name) as name'),
                                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                        )
                                ->groupBy('u.id_user')
                                ->take(5)->get();
    }
    
    public static function getShareDocUser($nIdDocument) 
    {
        return SharedDocument::from( 'shared_documents as sd' )
                             ->leftJoin('document_permissions as dp',function ($query) use($nIdDocument){
                                        $query->on('sd.shared_with','=','dp.id_user')
                                              ->Where('dp.id_document','=',$nIdDocument);
                                        })
                             ->leftJoin('users as u' ,'dp.id_user', '=', 'u.id_user')
                             ->where('sd.id_document', '=', $nIdDocument)
                             ->where('dp.id_group', '=', NULL)
                             ->where('sd.activated', '=', 1)
                             ->where('sd.deleted', '=', 0)
                             ->select(
                                     'u.id_user as id_user',                                     
                                     DB::raw('CONCAT(u.first_name, " ", u.last_name) as name'),
                                     'u.first_name as first_name',
                                     'u.last_name as last_name',
                                     'dp.permission_type as permission_type',
                                     'dp.id_document_permission as id_document_permission'   
                                     )
                             ->groupby('dp.id_user')
                             ->orderBy('u.first_name','asc')
                             ->orderBy('dp.permission_type','desc')
                             ->get();
    }
    
    public static function sharedUserList($nIdDocument,$nSkip=0,$nTake=3) {
        return SharedDocument::from( 'shared_documents as sd' )
                             ->join('users as u' ,'u.id_user', '=', 'sd.shared_with')
                             ->where('sd.id_document', '=', $nIdDocument)
                             ->where('sd.activated', '=', 1)
                             ->where('sd.deleted', '=', 0)
                             ->select(
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                    )
                             ->take($nTake)
                             ->skip($nSkip)
                             ->get();
        
    }
    
    public static function countSharedUserList($nIdDocument) {
        return SharedDocument::from( 'shared_documents as sd' )
                             ->where('sd.id_document', '=', $nIdDocument)
                             ->where('sd.activated', '=', 1)
                             ->where('sd.deleted', '=', 0)
                             ->count();
    }
    
    public static function allSharedUserList($nIdDocument) {
        return SharedDocument::from( 'shared_documents as sd' )
                             ->join('users as u' ,'u.id_user', '=', 'sd.shared_with')
                             ->where('sd.id_document', '=', $nIdDocument)
                             ->where('sd.activated', '=', 1)
                             ->where('sd.deleted', '=', 0)
                             ->select(
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                        )
                             ->skip(3)
                             ->take(50)
                             ->get();
        
    }
    
    public static function getSharedDocumentList($nIdUser) 
    {
        $aWhereParams = [
                            'sd.shared_with' => $nIdUser,
                            'd.activated' => 1,
                            'd.deleted' => 0,                         
                            'd.document_type' => config('constants.DOCUMENTTYPEFOLDER'),
                        ];
        
        DB::statement(DB::raw('SET @previous_lft = 0'));
        DB::statement(DB::raw('SET @previous_rgt = 0'));
        
        $iQuery = SharedDocument::from( 'shared_documents as sd' )
                            ->leftJoin( 'documents as d', 'sd.id_document', '=', 'd.id_document' )
                            ->leftJoin('document_permissions as dp',function ($query) use($nIdUser){
                                            $query->on('dp.id_document','=','sd.id_document' )
                                                  ->where('dp.permission_type','=',config('constants.DOCUMENTPERMISSIONTYPEWRITE'))
                                                  ->Where('dp.id_user','=',$nIdUser);
                                        })
                            ->where($aWhereParams)
                            ->select(
                                        'd.id_document as id_document',
                                        'd.document_type as document_type',
                                        'd.id_parent as id_parent',
                                        'd.document_name as document_name',
                                        'd.depth as depth',
                                        'd.lft as lft',
                                        'd.rgt as rgt',
                                        DB::raw('@previous_lft := d.lft'),
                                        DB::raw('@previous_rgt := d.rgt'),
                                        'd.created_at as created_at',
                                        'd.updated_at as updated_at',
                                        'dp.permission_type as permission_type'
                                    )
                            ->having('d.lft', 'NOT BETWEEN', DB::raw(DB::raw('@previous_lft') .' AND '. DB::raw(DB::raw('@previous_rgt'))))
                            ->orderBy('sd.id_document','asc');
        
        return SharedDocument::from(DB::raw("({$iQuery->toSql()}) as docs"))
                        ->select('*')
                        ->mergeBindings( $iQuery->getQuery() )
                        ->orderBy('docs.document_name','asc')
                        ->get();
    }
}