<?php

namespace App\Libraries;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserAnswer extends Model
{
    /**
     * For define table name
     */
    protected $table = 'user_answers';
    protected $primaryKey = 'id_user_answer';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_user', 'id_answer_option', 'id_quiz_question', 'user_answer_description',
                            'mark_obtain', 'activated', 'deleted'
                        ];
    
    public static function getUserMarkObtain($nIdQuiz,$nIdUser) 
    {
        return UserAnswer::from('user_answers as ua')
                         ->join('quiz_questions as qq','ua.id_quiz_question','=','qq.id_quiz_question')
                         ->where('qq.id_quiz',$nIdQuiz)
                         ->where('ua.id_user',$nIdUser)
                         ->where('qq.activated',1)
                         ->where('qq.deleted',0)
                         ->select(
                                  DB::Raw('SUM(ua.mark_obtain) as mark_obtain')
                                 )
                         ->get();
    }
    
    //check faculty gives marks to student is remaining or not
    public static function checkQuizMarksRemaining($sIdQuestion,$nIdUser = '')
    {
        return UserAnswer::from('user_answers as ua')
                         ->whereIn('ua.id_quiz_question',$sIdQuestion)
                         ->where('ua.mark_obtain','=',NULL)
                         ->when(!empty($nIdUser), function ($query) use($nIdUser) {
                                            return $query->where('ua.id_user','=',$nIdUser);
                                })
                         ->where('ua.id_user','=',$nIdUser)
                         ->count();
    }
}