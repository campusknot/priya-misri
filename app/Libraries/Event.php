<?php

namespace App\Libraries;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Event extends Model
{
    /**
     * For define table name
     */
    protected $table = 'events';
    protected $primaryKey = 'id_event';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_user', 'event_title', 'event_description', 'start_date',
                            'end_date', 'address', 'latitude', 'longitude', 'room_number',
                            'calendar_name','id_calendar','event_from','id_external_event'
                        ];

    public function getRouteKeyName()
    {
        return 'id_event';
    }

    /**
     * Get the user that owns the Event.
     */

    public function user()
    {
        return $this->hasOne(User::class , "id_user" ,"id_user");
    }

    public function user_event()
    {
        return $this->belongsToMany(User::class , "event_requests","id_event" ,"id_user");
    }

    public function user_event_feed()
    {
        return $this->belongsToMany(User::class , "event_feeds","id_event" ,"id_user");
    }

    /**
     * This function will fetch data of user's active and in-active events.
     * List will be first order by start_date
     * So active events will come first with start_date accending order.
     * @param type $nIdUser
     * @param type $dStartDate
     * @param type $nIdGroup
     * @return type $oEventList
     */
    
    public static function getUserEventList($nIdUser, $dStartDate = '', $nIdGroup = '', $sSearchParam='')
    {
        if(empty($dStartDate))
        {
            $dCurrentdate = Carbon::now();
            $dStartDate = $dCurrentdate->toDateTimeString();
        }
        
        $query =  Event::from('events as e')
                        ->leftJoin('event_requests as er', 'e.id_event', '=', 'er.id_event')
                        ->leftJoin('users as u', 'e.id_user', '=', 'u.id_user')
                        ->where('e.activated', '=', 1)
                        ->where('e.deleted', '=', 0)
                        ->where('er.id_user_request_to', '=', $nIdUser)
                        ->where(function ($query) use($dStartDate) {
                            $query->where('e.start_date', '>=', $dStartDate)
                                  ->orWhere('e.end_date', '>=', $dStartDate);
                        })
                        ->when(!empty($nIdGroup), function ($query) use($nIdGroup) {
                            return $query->leftJoin('group_events as ge', 'e.id_event', '=', 'ge.id_event')
                                            ->where('ge.id_group', '=', $nIdGroup);
                        })
                        ->select(
                                    'e.id_event as id_event',
                                    'e.id_user as id_user',
                                    'e.event_title as event_title',
                                    'e.event_description as event_description',
                                    'e.start_date as start_date',
                                    'e.end_date as end_date',
                                    'e.room_number as room_number',
                                    'e.address as address',
                                    'e.latitude as latitude',
                                    'e.longitude as longitude',
                                    'e.event_from as event_from',
                                    'e.calendar_name as calendar_name',
                                    'e.activated as activated',
                                    'e.deleted as deleted',
                                    'e.created_at as created_at',
                                    'e.updated_at as updated_at',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name'
                                )
                        ->groupBy('e.id_event')
                        ->orderBy('e.start_date', 'asc');
                        
        if($sSearchParam != '')
            $query->where(DB::raw('e.event_title'), 'like', $sSearchParam.'%' );
        
        return $query->paginate(config('constants.PERPAGERECORDS'));
    }
    
    /**
     * 
     * @param type $nIdEvent
     * @return $oEventDetail Description
     */
    public static function getEventDetail($nIdEvent)
    {
        return Event::from('events as e')
                    ->leftJoin('event_requests as er', function($join) {
                        $join->on('e.id_event', '=', 'er.id_event');
                        $join->on('er.id_user_request_to', '=', DB::raw(Auth::user()->id_user));
                    })
                    ->leftJoin('users as u', 'e.id_user', '=', 'u.id_user')
                    ->where('e.id_event', '=', $nIdEvent)
                    ->where('e.activated', '=', 1)
                    ->where('e.deleted', '=', 0)
                    ->select(
                                'e.id_event as id_event',
                                'e.id_user as id_user',
                                'e.event_title as event_title',
                                'e.event_description as event_description',
                                'e.start_date as start_date',
                                'e.end_date as end_date',
                                'e.room_number as room_number',
                                'e.address as address',
                                'e.latitude as latitude',
                                'e.longitude as longitude',
                                'e.event_from as event_from',
                                'e.calendar_name as calendar_name',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'er.id_event_request as id_event_request',
                                'er.status as user_response',
                                'e.activated as activated',
                                'e.deleted as deleted',
                                'e.created_at as created_at',
                                'e.updated_at as updated_at'
                            )
                    ->first();
    }
    
    public static function isUserEventAdmin($nUserId , $nAdminId)
    {
        if($nUserId == $nAdminId)
        {
            return true;
        }else{
            return false;
        }
    }
    
    public static function getGoogleCalendarList($nIdUser)
    {
        return Event::from('events as e')
                     ->where('e.id_user', '=', $nIdUser)
                     ->where('e.activated', '=', 1)
                     ->where('e.deleted', '=', 0)
                     ->groupBy('e.id_calendar')
                     ->select(
                             'e.calendar_name as calendar_name'
                             )
                     ->get();
                
    }

}