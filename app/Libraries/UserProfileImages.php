<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserProfileImages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'file_name'
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_profile_images';
    protected $primaryKey = 'id_user_profile_image';

     public function getRouteKeyName()
    {
        return 'id_user_profile_image';
    }

    /**
     * Get the user that owns the UserImages.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public static function getUserCurrentProfileImage($nUserId)
    {
        return UserProfileImages::where('id_user', $nUserId)
                                    ->where('activated', 1)
                                    ->where('deleted', 0)
                                    ->orderBy('updated_at', 'desc')
                                    ->first();
    }
    
    public static function getUserProfileImages($nUserId)
    {
        return UserProfileImages::where('id_user', $nUserId)
                                    ->where('deleted', 0)
                                    ->orderBy('created_at', 'desc')
                                    ->get();
    }
}
