<?php
namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class UserAttendance extends Model
{
    protected $table = 'user_attendance';
    protected $primaryKey = 'id_user_attendance';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_lecture', 'attendance_type', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function getAllUserForCourse($oLecture,$nExcelFlag='') {
        $oUserAttendance= UserAttendance::from( 'user_attendance as ua' )
                             ->join('lectures as l', 'ua.id_lecture', '=', 'l.id_lecture')
                             ->leftjoin('users as u', 'ua.id_user', '=', 'u.id_user')
                             ->leftjoin('attendance_log as al', 'ua.id_user_attendance', '=', 'al.id_user_attendance')
                             ->where('l.id_course',$oLecture['id_course'])
                             ->where('l.section',$oLecture['section'])
                             ->where('l.semester',$oLecture['semester'])                                                      
                             ->where('l.id_faculty',Auth::user()->id_user)                                                      
                             ->where('ua.activated', 1)                                                      
                             ->where('ua.deleted',0)                                                      
                             ->select(
                                     'u.first_name as first_name',
                                     'u.last_name as last_name',
                                     'u.id_user as id_user',
                                     'u.email as email',
                                     'l.semester as semester',
                                     DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                     DB::raw("GROUP_CONCAT( CASE WHEN ua.attendance_type = 'P' THEN ua.id_lecture ELSE NULL END SEPARATOR ',') as `id_lecture_present`"),
                                     DB::raw("GROUP_CONCAT( CASE WHEN ua.attendance_type = 'SL' THEN ua.id_lecture ELSE NULL END SEPARATOR ',') as `id_lecture_sick`"),
                                     DB::raw("GROUP_CONCAT( CASE WHEN al.id_user_attendance != '' THEN al.id_attendance_log ELSE NULL END SEPARATOR ',') as `id_attendance_log`")
                                     )
                             ->groupBy('ua.id_user')
                             ->orderBy('u.first_name');
        
        if(isset($oLecture['search_str']) && $oLecture['search_str'] != ''){
            $sSearchStr= $oLecture['search_str'];
            $oUserAttendance->where( function( $query ) use( $sSearchStr) {
                                        $query->where('u.first_name', 'like', $sSearchStr.'%')
                                              ->orWhere('u.last_name', 'like', $sSearchStr.'%');
                                        });
        }
        if($nExcelFlag == 1)
            return $oUserAttendance->get();
        else
            return $oUserAttendance->paginate(config('constants.PERPAGERECORDS'));
    }
}