<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserJournals extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'journal_title', 
        'journal_url','journal_author',
        'journal_description','journal_year'  
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_journals';
    protected $primaryKey = 'id_user_journal';

     public function getRouteKeyName()
    {
        return 'id_user_journal';
    }

	/**
     * Get the user that owns the Organization.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserJournal($nUserId, $nLimit = 0)
    {
        return UserJournals::where('id_user', $nUserId)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->orderBy('journal_year', 'desc')
                                ->when($nLimit > 0, function ($query) use ($nLimit) {
                                    return $query->take($nLimit);
                                })
                                ->get();
    }
}
