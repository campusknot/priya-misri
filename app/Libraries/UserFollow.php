<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserFollow extends Model
{
    protected $table = 'user_follows';
    protected $primaryKey = 'id_user_follow';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_follower', 'id_following'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * 
     * @param type $nIdUser
     * @return type
     */
    public static function getUserFollowIds($nIdUser)
    {
        return UserFollow::where('id_follower', '=', $nIdUser)
                            ->select(DB::raw('GROUP_CONCAT(id_following) as id_followings'))
                            ->get();
    }
    
    public static function getUserFollowingCount($nIdUser)
    {
        return UserFollow::where('id_follower', '=', $nIdUser)
                            ->count();
    }
    
    public static function getUserFollowerCount($nIdUser)
    {
        return UserFollow::where('id_following', '=', $nIdUser)
                            ->count();
    }
    public static function getUserFollower($nUserId)
    {
        return UserFollow::from('user_follows as uf')
                            ->leftjoin('users as u','uf.id_follower','=','u.id_user')
                            ->leftJoin('user_follows as uf1', function($join){
                                $join->on('uf.id_follower' ,'=', 'uf1.id_following')
                                     ->where('uf1.id_follower','=',Auth()->user()->id_user);

                            })
                            ->where('uf.id_following','=' , $nUserId)
                            ->where('u.activated','=',1)
                            ->where('u.deleted','=',0)
                            ->select
                                   (
                                        'uf.id_user_follow as id_user_follow',
                                        'uf1.id_user_follow as id_follow',
                                        'u.id_user as id_user' ,
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        'u.user_type as user_type',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = uf.id_follower AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                    )
                            ->orderBy('u.first_name','asc')
                            ->paginate(config('constants.PERPAGERECORDS'));
        
    }

    public static function getUserFollowing($nUserId)
    {
        return UserFollow::from('user_follows as uf')
                            ->leftjoin('users as u','uf.id_following','=','u.id_user')
                            ->leftJoin('user_follows as uf1', function($join){
                                $join->on('uf.id_following' ,'=', 'uf1.id_following')
                                     ->where('uf1.id_follower','=',Auth()->user()->id_user);

                            })
                            ->where('uf.id_follower','=',$nUserId)
                            ->where('u.activated','=',1)
                            ->where('u.deleted','=',0)
                            ->select
                                   (
                                        'uf.id_user_follow as id_user_follow',
                                        'uf1.id_user_follow as id_follow',
                                        'u.id_user as id_user' ,
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        'u.user_type as user_type',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = uf.id_following AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                    )
                            ->orderBy('u.first_name','asc')
                            ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function isUserFollowing($nIdFollower, $nIdFollowing)
    {
        return UserFollow::where('id_follower', '=', $nIdFollower)
                            ->where('id_following', '=', $nIdFollowing)
                            ->get();
    }
}
