<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Post extends Model
{
    protected $table = 'posts';
    protected $primaryKey = 'id_post';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'post_text', 'post_type', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public static function getAllPosts($aUserFollowIds, $aUserGroupIds = array(),$aWhereCond = array())
    {
        if(!count($aUserFollowIds))
            return FALSE;
        
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('post_media as pm', 'p.id_post', '=', 'pm.id_post')
                    ->leftJoin('likes as l', function($join) {
                        $join->on('l.id_user', '=', DB::raw("'".Auth::user()->id_user."'"));
                        $join->on('p.id_post', '=', 'l.id_entity');
                        $join->on('l.entity_type','=',DB::raw("'P'"));
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->leftJoin('groups as g', 'gp.id_group', '=', 'g.id_group')
                    ->leftJoin('group_members as gm', function($join) {
                                $join->on('gm.id_group', '=', 'g.id_group')
                                     ->where('gm.id_user','=',Auth::user()->id_user);
                        })
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->where(function ($query) use($aUserFollowIds, $aUserGroupIds) {
                        $query->whereIn('p.id_user', $aUserFollowIds)
                              ->where('gp.id_post', NULL)
                              ->when(count($aUserGroupIds), function ($query) use ($aUserGroupIds) {
                                    return $query->orWhereIn('gp.id_group', $aUserGroupIds);
                                });
                    })
                    ->where($aWhereCond)
                    ->where('p.post_type', '!=', config('constants.POSTTYPEPOLL'))
                    ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'p.post_text as post_text',
                                'p.post_type as post_type',
                                'pm.display_file_name as display_file_name',
                                'pm.file_name as file_name',
                                'pm.media_type as media_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'l.id_like as id_like',
                                DB::raw('(SELECT COUNT(id_like) FROM likes WHERE id_entity = p.id_post AND entity_type = "P") as likes'),
                                DB::raw('(SELECT COUNT(id_comment) FROM comments WHERE id_entity = p.id_post AND entity_type = "P" AND activated = 1 AND deleted = 0) as comment_count'),
                                'gp.id_group_post as id_group_post',
                                'g.id_group as id_group',
                                'g.group_name as group_name',
                                'g.section as section',
                                'g.semester as semester',
                                'gm.member_type as member_type',
                                'p.created_at as created_at',
                                'p.updated_at as updated_at'
                            )
                    ->orderBy('p.updated_at', 'desc')
                    ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getPostDetail($nIdPost = '')
    {
        if(empty($nIdPost))
            return FALSE;
        
        return Post::from( 'posts as p' )
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->leftJoin('post_media as pm', 'p.id_post', '=', 'pm.id_post')
                    ->leftJoin('polls as po',function($join) {
                            $join->on('p.id_post', '=', 'po.id_entity')
                                 ->where('po.entity_type','=',config('constants.ENTITYTYPEPOST'));
                        })
                    ->leftJoin('poll_answers as pa',function($join) {
                        $join->on('po.id_poll', '=', 'pa.id_poll')
                             ->where('pa.id_user','=',Auth::user()->id_user);
                    })
                    ->leftJoin('likes as l', function($join) {
                        $join->on('l.id_user', '=', DB::raw("'".Auth::user()->id_user."'"));
                        $join->on('p.id_post', '=', 'l.id_entity');
                        $join->on('l.entity_type','=',DB::raw("'P'"));
                    })
                    ->leftJoin('group_posts as gp', 'gp.id_post', '=', 'p.id_post')
                    ->leftJoin('groups as g', 'gp.id_group', '=', 'g.id_group')
                    ->leftJoin('group_members as gm', function($join) {
                                $join->on('gm.id_group', '=', 'g.id_group')
                                     ->where('gm.id_user','=',Auth::user()->id_user);
                        })
                    ->where('p.id_post', '=', $nIdPost)
                    ->where('p.activated', '=', 1)
                    ->where('p.deleted', '=', 0)
                    ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'p.post_text as post_text',
                                'p.post_type as post_type',
                                'pm.display_file_name as display_file_name',
                                'pm.file_name as file_name',
                                'pm.media_type as media_type',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'l.id_like as id_like',
                                DB::raw('(SELECT COUNT(id_like) FROM likes WHERE id_entity = p.id_post AND entity_type = "P") as likes'),
                                DB::raw('(SELECT COUNT(id_comment) FROM comments WHERE id_entity = p.id_post AND entity_type = "P" AND activated = 1 AND deleted = 0) as comment_count'),
                                'gp.id_group_post as id_group_post',
                                'g.id_group as id_group',
                                'g.group_name as group_name',
                                'g.section as section',
                                'g.semester as semester',
                                'gm.member_type as member_type',
                                'p.created_at as created_at',
                                'p.updated_at as updated_at',
                                'po.id_poll as id_poll',
                                'po.poll_text as poll_text',
                                'po.file_name as poll_file_name',
                                'po.start_time as start_time',
                                'po.end_time as end_time',
                                'po.allow_multiple_answers as allow_multiple_answers',
                                'po.poll_type as poll_type',
                                'po.activated as activated',
                                'pa.id_poll_answer as id_poll_answer',
                                'pa.id_poll_option as id_poll_option',
                                'pa.poll_answer_description as poll_answer_description'
                            )
                    ->first();
    }
    
    public static function getUserPostImages($nIdUser = '')
    {
        if(empty($nIdUser))
            return FALSE;
        
        return Post::from( 'posts as p' )
                ->leftJoin('post_media as pm', function($join){
                    $join->on('p.id_post', '=', 'pm.id_post');   
                })
                ->where('p.post_type','=','I')
                ->where('p.id_user', '=', $nIdUser)
                ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'pm.file_nam as file_name'
                        )
                ->orderBy('p.updated_at', 'desc');
    }
    
    public static function getUserPostDocument($nIdUser, $sPostType,$sOrderBy='desc',$sOrderField='updated_at')
    {
        if(empty($sOrderField) || $sOrderField == 'updated_at')
            $sOrderField = 'p.updated_at';
        
        $aWhereParams = [
                            'p.activated' => 1,
                            'p.deleted' => 0,
                            'p.post_type' => $sPostType,
                            'p.id_user' => $nIdUser
                        ];
        
        
        return Post::from('posts as p')
                    ->leftJoin('post_media as pm', 'p.id_post', '=', 'pm.id_post')
                    ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                    ->where($aWhereParams)
                    ->select(
                                'p.id_post as id_post',
                                'p.id_user as id_user',
                                'p.post_text as post_text',
                                'p.post_type as post_type',
                                'p.created_at as created_at',
                                'p.updated_at as updated_at',
                                'pm.display_file_name as display_file_name',
                                'pm.file_name as file_name',
                                'pm.media_type as media_type',
                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                'u.first_name as first_name',   
                                'u.last_name as last_name'
                            )
                    ->orderBy($sOrderField, $sOrderBy)
                    ->paginate(config('constants.PERPAGERECORDS'));
    }
    
}