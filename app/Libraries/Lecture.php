<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;

class Lecture extends Model
{
    protected $table = 'lectures';
    protected $primaryKey = 'id_lecture';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_faculty', 'id_course', 'section', 'semester', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    public static function getLectureDetail($aData,$nUserId) 
    {
        //echo $aData['from'] ;exit;
        
        $aWhereParams = [
                            'l.id_faculty' => $nUserId,
                            'l.activated' => 1,
                            'l.deleted' => 0
                        ];
        
        $oLecture= Lecture::from( 'lectures as l' )
                        ->leftJoin('courses as c', 'l.id_course', '=', 'c.id_course')
                        ->where($aWhereParams)
                        ->select(
                                'c.id_course as id_course',
                                'c.course_name as course_name',
                                'c.course_code as course_code',
                                'l.section as section',
                                'l.semester as semester',
                                'l.id_lecture as id_lecture',
                                'l.created_at as created_at',
                                DB::raw('(SELECT count(*) FROM user_attendance as ua WHERE ua.id_lecture = l.id_lecture AND ua.activated = 1 AND ua.deleted = 0 ) as present_student')
                              );
       
        if(empty($aData['from']))
        {
            $oLecture->where('l.id_lecture',$aData['id_lecture']);
            $oLecture->limit(1);
        }
        else
        {
            $dStartdate=Carbon::createFromFormat('m-d-Y', $aData['from'])->toDateString();
            $dEnddate=Carbon::createFromFormat('m-d-Y', $aData['to'])->toDateString();
            $semester=$aData['semester'];
            $oLecture->whereDate('l.created_at', '>=',$dStartdate);
            $oLecture->whereDate('l.created_at', '<=',$dEnddate);
            $oLecture->where('l.semester', '=',$semester);
            $oLecture->where('l.section', '=',$aData['section']);
            $oLecture->where('l.id_course', '=',$aData['id_course']);
        }
        return $oLecture->get();
    }
    
    public static function getStudentUniqueLecture($nUserId)
    {
        return Lecture::from( 'user_attendance as ua' )
                        ->leftJoin('lectures as l', 'l.id_lecture', '=', 'ua.id_lecture')
                        ->leftJoin('courses as c', 'l.id_course', '=', 'c.id_course')
                        ->leftJoin('users as u', 'l.id_faculty', '=', 'u.id_user')
                        ->where('ua.id_user',$nUserId)
                        ->where('ua.activated',1)
                        ->where('ua.deleted',0)
                        ->select
                                (
                                    DB::raw('max(l.id_lecture) as id_lecture'),
                                    'l.id_faculty as id_faculty',
                                    'l.id_course as id_course',
                                    'l.section as section',
                                    'l.semester as semester',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code',
                                    'u.id_user as id_user',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name'
                                )
                        ->orderBy('id_lecture','desc')
                        ->groupBy('l.id_faculty','l.id_course','l.section','l.semester')
                        ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    //faculty list of lecture with diffrent section in different semester
    public static function getFacultyUniqueLecture($nUserId)
    {
        return Lecture::from('lectures as l')
                        ->leftJoin('courses as c', 'l.id_course', '=', 'c.id_course')
                        ->where('l.id_faculty',$nUserId)
                        ->where('l.activated',1)
                        ->where('l.deleted',0)
                        ->select
                                (
                                    DB::raw('max(l.id_lecture) as id_lecture'),
                                    'l.id_faculty as id_faculty',
                                    'l.id_course as id_course',
                                    'l.section as section',
                                    'l.semester as semester',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code'
                                )
                        ->orderBy('l.id_lecture','desc')
                        ->groupBy('l.id_course','l.section','l.semester')
                        ->paginate(config('constants.PERPAGERECORDS'));
    }

    public static function getAllSameLecture($oLacture,$nUserId)
    { 
        return Lecture::from( 'lectures as l' )
                      ->leftJoin('user_attendance as ua',function($join) use ($nUserId){ 
                                    $join->on('l.id_lecture', '=', 'ua.id_lecture')      
                                         ->where('ua.id_user','=', $nUserId );      
                                })
                      ->where('l.id_course',$oLacture->id_course)  
                      ->where('l.section',$oLacture->section)  
                      ->where('l.semester',$oLacture->semester)  
                      ->where('l.id_faculty',$oLacture->id_faculty)
                      ->where('l.activated',1) 
                      ->where('l.deleted',0) 
                      ->select
                            (
                                'l.id_lecture as id_lecture',
                                'l.id_faculty as id_faculty',
                                'l.id_course as id_course',
                                'l.section as section',
                                'l.semester as semester',
                                'l.created_at as created_at',
                                'ua.id_user_attendance as id_user_attendance',
                                'ua.attendance_type as attendance_type',
                                DB::raw('(SELECT count(al.id_attendance_log) FROM attendance_log as al WHERE ua.id_user_attendance = al.id_user_attendance) as count_id_attendance_log')
                            )
                      ->get();
    }
    
    public static function getLecturesCount($oLacture,$nUserId,$sAttendanceType='')
    { 
        return Lecture::from( 'lectures as l' )
                        ->leftJoin('user_attendance as ua',function($join) use ($nUserId){ 
                                      $join->on('l.id_lecture', '=', 'ua.id_lecture')      
                                           ->where('ua.id_user','=', $nUserId );      
                                  })
                        ->where('l.id_course',$oLacture->id_course)  
                        ->where('l.section',$oLacture->section)  
                        ->where('l.semester',$oLacture->semester)  
                        ->where('l.id_faculty',$oLacture->id_faculty)
                        ->where('l.activated',1) 
                        ->where('l.deleted',0)
                        ->when($sAttendanceType, function($query) use ($sAttendanceType) {
                            return $query->where('ua.id_user_attendance', '!=', NULL)
                                            ->where('ua.attendance_type', '=', $sAttendanceType);
                        })
                        ->select
                            (
                                'l.id_lecture as id_lecture'
                            )
                        ->count();
    }
    
    public static function CheckTodayLectureCount($oRequest,$nUserId) 
    {
        return Lecture::where('id_faculty', $nUserId)
                        ->where('id_course', $oRequest->course_id)
                        ->where('semester', $oRequest->semester.'-'.Carbon::now()->year)
                        ->where('section', $oRequest->section)
                        ->where('activated', 1)
                        ->where('deleted', 0)
                        ->whereDate('created_at', '=',Carbon::now()->toDateString())
                        ->count();

    }
    public static function getCourseSuggetions($nUserId) 
    {
        return Lecture::from( 'lectures as l' )
                        ->join('courses as c', 'l.id_course', '=', 'c.id_course')
                        ->where('l.id_faculty' , $nUserId)
                        ->where('l.activated' , 1)
                        ->where('l.deleted' , 0)
                        ->where('c.activated','=',1)
                        ->where('c.deleted','=',0)
                        ->select(
                                    'l.id_course as id_course',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code'
                                )
                        ->groupBy('l.id_course')
                        ->get();
    }
    //for excel getting start date and date of semester attendance
    public static function getLecture($id_course,$section,$userid,$semester,$orderby)
    {
        return Lecture::from( 'lectures as l' )
                        ->where('l.id_course',$id_course)  
                        ->where('l.section',$section)  
                        ->where('l.semester',$semester)  
                        ->where('l.id_faculty',$userid)
                        ->where('l.activated',1) 
                        ->where('l.deleted',0)
                        ->orderBy('l.id_lecture',$orderby)
                        ->select(
                                    'l.created_at as created_at',
                                    'l.id_lecture as id_lecture'
                                )
                        ->first();
    }
    
    public static function getLectureData($nIdLecture)
    {
        return Lecture::from( 'lectures as l' )
                        ->leftJoin('courses as c', 'l.id_course', '=', 'c.id_course')
                        ->leftJoin('users as u', 'l.id_faculty', '=', 'u.id_user')
                        ->where('l.id_lecture',$nIdLecture)  
                        ->where('l.activated',1) 
                        ->where('l.deleted',0)
                        ->select(
                                    'l.created_at as created_at',
                                    'l.id_lecture as id_lecture',
                                    'l.id_faculty as id_faculty',
                                    'l.id_course as id_course',
                                    'l.section as section',
                                    'l.semester as semester',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code',
                                    'u.id_user as id_user',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name'
                                )
                        ->first();
    }
    
    public static function checkStudentExists($oLecture,$nIdUser) 
    {
        return Lecture::from( 'lectures as l' )
                    ->join('user_attendance as ua', 'ua.id_lecture', '=', 'l.id_lecture')
                    ->where('l.id_course',$oLecture->id_course)  
                    ->where('l.section',$oLecture->section)  
                    ->where('l.semester',$oLecture->semester)  
                    ->where('l.id_faculty',$oLecture->id_faculty)
                    ->where('ua.id_user',$nIdUser)
                    ->where('ua.activated',1) 
                    ->where('ua.deleted',0)
                    ->count();
    }
}