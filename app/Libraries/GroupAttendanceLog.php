<?php
namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class GroupAttendanceLog extends Model
{
    protected $table = 'group_attendance_logs';
    protected $primaryKey = 'id_group_attendance_log';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_user_group_attendance', 'change_from', 'change_to'
    ];
    
    public static function getAdminAttendaceLog($aIdAttendanceLog)
    {
        return GroupAttendanceLog::from('group_attendance_logs as gal')
                            ->leftJoin('user_group_attendance as uga', 'gal.id_user_group_attendance', '=', 'uga.id_user_group_attendance')
                            ->leftJoin('group_lectures as gl', 'uga.id_group_lecture', '=', 'gl.id_group_lecture')
                            ->leftJoin('groups as g', 'gl.id_group', '=', 'g.id_group')
                            ->whereIn('gal.id_group_attendance_log',$aIdAttendanceLog)
                            ->select(
                                    
                                     'gal.created_at as created_at',
                                     'gal.change_to as change_to',
                                     'gal.change_from as change_from',
                                     'g.group_name as group_name',
                                     'gl.semester as semester',
                                      'gl.created_at as lecture_date'
                                    )
                            ->orderBy('uga.created_at','asc')
                            ->orderBy('gal.id_group_attendance_log','desc')
                            ->get();
    }
    
    public static function getMemberAttendaceLog($aIdLecture) 
    {
        return GroupAttendanceLog::from('group_attendance_logs as gal')
                            ->leftJoin('user_group_attendance as uga', 'gal.id_user_group_attendance', '=', 'uga.id_user_group_attendance')
                            ->leftJoin('group_lectures as gl', 'uga.id_group_lecture', '=', 'gl.id_group_lecture')
                            ->leftJoin('groups as g', 'gl.id_group', '=', 'g.id_group')
                            ->whereIn('gl.id_group_lecture',$aIdLecture)
                            ->where('uga.id_user', Auth::user()->id_user)
                            ->select(
                                    'gal.created_at as created_at',
                                     'gal.change_to as change_to',
                                     'gal.change_from as change_from',
                                     'g.group_name as group_name',
                                     'gl.semester as semester',
                                      'gl.created_at as lecture_date'
                                    )
                            ->orderBy('uga.created_at','asc')
                            ->orderBy('gal.id_group_attendance_log','desc')
                            ->get();
    }
    
    public static function getAttendanceLogIds($aLectureIds) 
    {
        
        return GroupAttendanceLog::from('group_attendance_logs as gal')
                                 ->whereIn('gal.id_user_group_attendance',$aLectureIds)
                                 ->select(
                                         DB::raw('GROUP_CONCAT(gal.id_group_attendance_log) as id_group_attendance_logs')
                                         )
                                 ->get();
    }
    
}