<?php
namespace App\Libraries;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*require_once base_path('vendor/aws/S3.php');
require_once base_path('vendor/aws/aws-autoloader.php');

use S3;*/

class ImageCropper
{
    protected $oSource,$ImgType,$extension,$sFileDestination,$aCropdata,$sMsg;
    protected $aThumbImage=array(50,100,150);
    protected $sDestinationPath,$sBucket;
    
    public function getCropImage($sSrc,$sData,$FILES,$sDestination,$sBucket) 
    {
        
        $src=isset($sSrc) ? $sSrc : null;
        $data=isset($sData) ? $sData : null;
        $oFile=isset($FILES['Img_file']) ? $FILES['Img_file'] : null;
        $this->sDestinationPath=$sDestination;
        $this->sBucket=$sBucket;
        
        if (!empty($src)) {
            $type = exif_imagetype($src);

            if ($type) {
                $this->oSource =$src;
                $this->ImgType =$type;
                $this->extension = image_type_to_extension($type);
                $this->sFileDestination=$sDestination.date('YmdHis') . '.png';
            }
        }
       
        if (!empty($data)) {
            $this->aCropdata=json_decode(stripslashes($data));
        }
        $this->setFile($oFile);
        
        if(isset($this->oSource))
        $sFileName=$this->callCropImg($this->oSource, $this->sFileDestination, $this->aCropdata);
        
        $response = array(
            'state'  => 200,
            'message' => isset($this->sMsg) ? $this->sMsg : '',
            'result' =>  !empty($this->aCropdata) ? $this->sFileDestination : $this->oSource,
            'sFileName' => $sFileName
        );
        return $response;
        
    }
    
    public function setFile($oFile) 
    {
        $errorCode = $oFile['error'];

        if ($errorCode === UPLOAD_ERR_OK)
        {
            $type = exif_imagetype($oFile['tmp_name']);

            if ($type)
            {
                $extension = image_type_to_extension($type);
                $file_name= date('YmdHis') . '_original' . $extension;
                $src =   $this->sDestinationPath.$file_name;

                if ($type == IMAGETYPE_GIF || $type == IMAGETYPE_JPEG || $type == IMAGETYPE_PNG)
                {
                    if (file_exists($src))
                    {
                        unlink($src);
                    }

                    $result = move_uploaded_file($oFile['tmp_name'], $src);
                    
                    $oS3 = \Storage::disk('s3');
                    $filePath = '/'.$this->sBucket.'/' . $file_name;
                    $oS3->put($filePath, fopen($src, 'r+'), 'public');

                    if ($result)
                    {
                        $this->oSource = $src;
                        $this->ImgType = $type;
                        $this->extension = $extension;
                        $this->sFileDestination=  $this->sDestinationPath. date('YmdHis') . '.png';
                    }
                    else
                    {
                       $this->sMsg = 'Failed to save file';
                    }
                }
                else
                {
                    $this->sMsg = 'Please upload image with the following types: JPG, PNG, GIF';
                }
            }
            else
            {
                $this->sMsg = 'Please upload image file';
            }
        }
    }
    
    public function callCropImg($sImgPath, $sFileDestination, $aCropdata)
    {
        if (!empty($sImgPath) && !empty($sFileDestination) && !empty($aCropdata))
        {
            switch ($this->ImgType)
            {
                case IMAGETYPE_GIF:
                  $src_img = imagecreatefromgif($sImgPath);
                  break;

                case IMAGETYPE_JPEG:
                  $src_img = imagecreatefromjpeg($sImgPath);
                  break;

                case IMAGETYPE_PNG:
                  $src_img = imagecreatefrompng($sImgPath);
                  break;
            }

            if (!$src_img)
            {
                $this->sMsg = "Failed to read the image file";
                return;
            }

            $size = getimagesize($sImgPath);
            $size_w = $size[0]; // natural width
            $size_h = $size[1]; // natural height

            $src_img_w = $size_w;
            $src_img_h = $size_h;

            $degrees = $aCropdata -> rotate;

            // Rotate the source image
            if (is_numeric($degrees) && $degrees != 0)
            {
                // PHP's degrees is opposite to CSS's degrees
                $new_img = imagerotate( $src_img, -$degrees, imagecolorallocatealpha($src_img, 0, 0, 0, 127) );

                imagedestroy($src_img);
                $src_img = $new_img;

                $deg = abs($degrees) % 180;
                $arc = ($deg > 90 ? (180 - $deg) : $deg) * M_PI / 180;

                $src_img_w = $size_w * cos($arc) + $size_h * sin($arc);
                $src_img_h = $size_w * sin($arc) + $size_h * cos($arc);

                // Fix rotated image miss 1px issue when degrees < 0
                $src_img_w -= 1;
                $src_img_h -= 1;
            }

            $tmp_img_w = $aCropdata -> width;
            $tmp_img_h = $aCropdata -> height;
            $dst_img_w = 220;
            $dst_img_h = 220;

            $src_x = $aCropdata -> x;
            $src_y = $aCropdata -> y;

            if ($src_x <= -$tmp_img_w || $src_x > $src_img_w) {
              $src_x = $src_w = $dst_x = $dst_w = 0;
            } else if ($src_x <= 0) {
              $dst_x = -$src_x;
              $src_x = 0;
              $src_w = $dst_w = min($src_img_w, $tmp_img_w + $src_x);
            } else if ($src_x <= $src_img_w) {
              $dst_x = 0;
              $src_w = $dst_w = min($tmp_img_w, $src_img_w - $src_x);
            }

            if ($src_w <= 0 || $src_y <= -$tmp_img_h || $src_y > $src_img_h) {
              $src_y = $src_h = $dst_y = $dst_h = 0;
            } else if ($src_y <= 0) {
              $dst_y = -$src_y;
              $src_y = 0;
              $src_h = $dst_h = min($src_img_h, $tmp_img_h + $src_y);
            } else if ($src_y <= $src_img_h) {
              $dst_y = 0;
              $src_h = $dst_h = min($tmp_img_h, $src_img_h - $src_y);
            }

            // Scale to destination position and size
            $ratio = $tmp_img_w / $dst_img_w;
            $dst_x /= $ratio;
            $dst_y /= $ratio;
            $dst_w /= $ratio;
            $dst_h /= $ratio;

            $dst_img = imagecreatetruecolor($dst_img_w, $dst_img_h);

            // Add transparent background to destination image
            imagefill($dst_img, 0, 0, imagecolorallocatealpha($dst_img, 0, 0, 0, 127));
            imagesavealpha($dst_img, true);

            $result = imagecopyresampled($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h);
            
            if ($result) {
              if (!imagepng($dst_img, $sFileDestination)) {
                $this->sMsg = "Failed to save the cropped image file";
              }
            } else {
              $this->sMsg = "Failed to crop the image file";
            }
            $file_name= date('YmdHis').'.png' ;
            
            $oS3 = \Storage::disk('s3');
            $filePath = '/'.$this->sBucket.'/' . $file_name;
            $oS3->put($filePath, fopen($sFileDestination, 'r+'), 'public');
                    
            $cropped = $this->CreateThumbImg($sFileDestination,$file_name);
            unlink($this->oSource);
            imagedestroy($src_img);
            imagedestroy($dst_img);
            return $file_name;
        }
    }

    /*generate different size thumnail after image crop */
    public function CreateThumbImg($thumb_image_name,$file_name)
    {
	list($imagewidth, $imageheight, $imageType) = getimagesize($thumb_image_name);
	$filename_err = explode(".",$thumb_image_name);
        $filename_err_count = count($filename_err);
        $file_ext = $filename_err[$filename_err_count-1];
        $thumb_widths=$this->aThumbImage;
        foreach($thumb_widths as $thumb_width)
        {
            $thumbnail = $this->sDestinationPath.$thumb_width.'_'.$file_name;

            $thumb_create = imagecreatetruecolor($thumb_width,floor( $imageheight * ( $thumb_width / $imagewidth)));
            list($N_width,$N_height) = getimagesize($thumb_image_name);
            switch($file_ext){
                case 'jpg':
                    $source = imagecreatefromjpeg($thumb_image_name);
                    break;
                case 'jpeg':
                    $source = imagecreatefromjpeg($thumb_image_name);
                    break;

                case 'png':
                    $source = imagecreatefrompng($thumb_image_name);
                    break;
                case 'gif':
                    $source = imagecreatefromgif($thumb_image_name);
                    break;
                default:
                    $source = imagecreatefromjpeg($thumb_image_name);
            }
            imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,floor( $imageheight * ( $thumb_width / $imagewidth)),$N_width,$N_height);
            switch($file_ext){
                case 'jpg' || 'jpeg':
                    imagejpeg($thumb_create,$thumbnail);
                    break;
                case 'png':
                    imagepng($thumb_create,$thumbnail);
                    break;

                case 'gif':
                    imagegif($thumb_create,$thumbnail);
                    break;
                default:
                    imagejpeg($thumb_create,$thumbnail);
            }
            chmod($thumbnail, 0777);
            
            $oS3 = \Storage::disk('s3');
            $filePath = '/'.$this->sBucket.'/' . $thumb_width.'_'.$file_name;
            $oS3->put($filePath, fopen($thumbnail, 'r+'), 'public');

            unlink($thumbnail);
	}
        unlink($thumb_image_name);
    }
    
    public static function ImageResize($oFile,$sNameOfFile, $nWidth, $nHeight, $sDestination,$sBucket,$sFileName)
    {
        $file = $oFile[$sNameOfFile]['tmp_name'];
        $sSize = $oFile[$sNameOfFile]['size'];
        list($width, $height) = getimagesize($file);
        
        if($width > 800 && $sSize > 1048576)
        {
            $r = $width / $height;

            if ($nWidth/$nHeight > $r) {
                $newwidth = $nHeight*$r;
                $newheight = $nHeight;
            } else {
                $newheight = $nWidth/$r;
                $newwidth = $nWidth;
            }

            //Get file extension
            $exploding = explode(".",$file);
            $ext = end($exploding);

            switch($ext){
                case "png":
                    $src = imagecreatefrompng($file);
                break;
                case "jpeg":
                case "jpg":
                    $src = imagecreatefromjpeg($file);
                break;
                case "gif":
                    $src = imagecreatefromgif($file);
                break;
                default:
                    $src = imagecreatefromjpeg($file);
                break;
            }

            $dst = imagecreatetruecolor($newwidth, $newheight);
            imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

            imagepng($dst, $sDestination, 2);

            $oS3 = \Storage::disk('s3');
            $filePath = '/'.$sBucket.'/80_' . $sFileName;
            $oS3->put($filePath, fopen($sDestination, 'r+'), 'public');
            unlink($sDestination);
        }
        else
        {
            $oS3 = \Storage::disk('s3');
            $filePath = '/'.$sBucket.'/80_' . $sFileName;
            $oS3->put($filePath, fopen($file, 'r+'), 'public');
        }
        return $filePath;
    }
}