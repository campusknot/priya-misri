<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class Group extends Model
{
    protected $table = 'groups';
    protected $primaryKey = 'id_group';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_campus', 'group_name', 'about_group', 'id_group_category', 'group_type','section','semester', 'file_name', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public static function getGroupList($nIdUser = '', $nIdGroupCategory = '', $aOptionalConditions = array(),$sSearchStr = '')
    {
        if(empty($nIdUser))
            return FALSE;
        
        $aWhereParams = [
                            'gm.id_user' => $nIdUser,
                            'g.activated' => 1,
                            'g.deleted' => 0,
                            'gm.deleted' => 0
                        ];
        $aWhereParams['gm.activated'] = isset($aOptionalConditions['activated']) ? $aOptionalConditions['activated'] : 1;
        
        if(!empty($nIdGroupCategory))
            $aWhereParams['gc.id_group_category'] = $nIdGroupCategory;
        
        return Group::from( 'groups as g' )
                        ->leftJoin('users as u', 'g.id_user', '=', 'u.id_user')
                        ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                        ->leftJoin('group_members as gm', 'g.id_group', '=', 'gm.id_group')
                        ->where($aWhereParams)
                        ->when(isset($aOptionalConditions['member_types']), function ($query) use ($aOptionalConditions) {
                            return $query->whereIn('gm.member_type', $aOptionalConditions['member_types']);
                        })
                        ->when(!empty($sSearchStr), function ($query) use($sSearchStr) {
                            return $query->where('g.group_name', 'like', '%'.$sSearchStr.'%');
                        })
                        ->select(
                                    'g.id_group as id_group',
                                    'g.group_name as group_name',
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.group_type as group_type',
                                    'g.about_group as about_group',
                                    'g.file_name as group_image',
                                    'u.id_user as id_user',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    'gc.id_group_category as id_group_category',
                                    'gc.group_category_name as group_category_name',
                                    'gc.file_name as group_category_image',
                                    DB::raw('(SELECT COUNT(id_group_member) FROM group_members WHERE id_group = g.id_group AND activated = 1 AND deleted = 0) as member_count'),
                                    'g.created_at as created_at',
                                    'g.updated_at as updated_at'
                                )
                        ->orderBy('g.group_name', 'ASC')
                        ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getGroupDetail($nIdGroup = '', $nIdUserLogedin = '')
    {
        if(empty($nIdGroup))
            return FALSE;
        
        if(empty($nIdUserLogedin))
            $nIdUserLogedin = Auth()->user()->id_user;
        
        return Group::from( 'groups as g' )
                ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                ->leftJoin('users as u', 'g.id_user', '=', 'u.id_user')
                ->leftJoin('group_members as gm', function($join) use($nIdUserLogedin){
                    $join->on('gm.id_group', '=', 'g.id_group')
                        ->where('gm.id_user','=',$nIdUserLogedin)
                        ->where('gm.activated','=',1)
                        ->where('gm.deleted','=',0);
                })
                ->where('g.id_group', '=', $nIdGroup)
                ->where('g.activated', '=', 1)
                ->where('g.deleted', '=', 0)
                ->select(
                            'g.id_group as id_group',
                            'g.group_name as group_name',
                            'g.section as section',
                            'g.semester as semester',
                            'g.group_type as group_type',
                            'g.about_group as about_group',
                            'g.file_name as group_image',
                            'g.created_at as created_at',
                            'g.updated_at as updated_at',
                            'gc.id_group_category as id_group_category',
                            'gc.group_category_name as group_category_name',
                            'gc.file_name as group_category_image',
                            'u.id_user as id_user',
                            'u.first_name as first_name',
                            'u.last_name as last_name',
                            'u.email as email',
                            'u.user_type as user_type',
                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                            'gm.id_user as member',
                            'gm.member_type as member_type'
                        )
                ->first();
    }
    
    public static function getGroupSearch($aSearchParam = array())
    {
        $nIdUserLogin = Auth()->user()->id_user;
        
        $aWhereParams[] = array('g.group_name', 'like', '%'.$aSearchParam['search_term'].'%');
        $aWhereParams[] = array('g.group_type', '!=', config('constants.SECRETGROUP'));
        $aWhereParams[] = array('g.id_campus', '=', $aSearchParam['id_campus']);
        $aWhereParams[] = array('g.activated', '=', 1);
        $aWhereParams[] = array('g.deleted', '=', 0);
        
        if(!empty($aSearchParam['id_group_category']))
            $aWhereParams[] = array('gc.id_group_category', '=',$aSearchParam['id_group_category']);
        
        return Group::from( 'groups as g' )
                    ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                    ->leftJoin('users as u', 'g.id_user', '=', 'u.id_user')
                    ->where($aWhereParams)
                    ->whereNotIn('g.id_group', $aSearchParam['user_groups'])
                    ->select(
                                'g.id_group as id_group',
                                'g.id_user as id_user',
                                'g.group_name as group_name',
                                'g.section as section',
                                'g.semester as semester',
                                'g.group_type as group_type',
                                'g.about_group as about_group',
                                'g.file_name as group_image',
                                'g.created_at as created_at',
                                'g.updated_at as updated_at',
                                'u.first_name as first_name',
                                'u.last_name as last_name',
                                'gc.id_group_category as id_group_category',
                                'gc.group_category_name as group_category_name',
                                'gc.file_name as group_category_image',
                                DB::raw('(SELECT COUNT(id_group_member) FROM group_members WHERE id_group = g.id_group AND activated = 1 AND deleted = 0) as member_count'),
                                DB::raw('IF((SELECT COUNT(id_group_request) FROM group_requests WHERE id_group = g.id_group AND ((id_user_request_to = '.$nIdUserLogin.' AND request_type = "I") OR (id_user_request_from = '.$nIdUserLogin.' AND request_type = "J")) AND member_status = "P"), 1, 0) as pending_request')
                            )
                    ->orderBy('g.group_name', 'asc')
                    ->paginate(config('constants.PERPAGERECORDS'));
    }

    public static function getGroupSearchEvant($sSearchParam = '', $aUserGroups = array() ,$nLimit)
    {
        $aWhereParams[] = array('g.group_name', 'like', $sSearchParam.'%');
        $aWhereParams[] = array('g.activated', '=', 1);
        $aWhereParams[] = array('g.deleted', '=', 0);
        
        return Group::from( 'groups as g' )
                        ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                        ->where($aWhereParams)
                        ->whereIn('g.id_group', $aUserGroups)
                        ->select(
                                    'g.id_group as id_group',
                                    'g.group_name as group_name',
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.group_type as group_type',
                                    'g.about_group as about_group',
                                    'g.file_name as file_name',
                                    'g.created_at as created_at',
                                    'g.updated_at as updated_at',
                                    'gc.id_group_category as id_group_category',
                                    'gc.group_category_name as group_category_name',
                                    'gc.file_name as group_category_image'
                                )
                        ->paginate($nLimit);
    }

    
    public static function getSyncGroups($nIdUser = '', $sLastUpdatedAt = '', $aOptionalConditions = array())
    {
        if(empty($nIdUser))
            return FALSE;
        
        return Group::from('groups as g')
                        ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                        ->leftJoin('group_members as gm', function($join) use($nIdUser){
                            $join->on('gm.id_group', '=', 'g.id_group')
                            ->where('gm.id_user','=',$nIdUser);
                        })
                        ->whereIn('gm.member_type', $aOptionalConditions['member_types'])
                        ->where(function ($query) use($sLastUpdatedAt) {
                            $query->where('g.updated_at', '>', $sLastUpdatedAt)
                                  ->orWhere('gm.updated_at', '>', $sLastUpdatedAt);
                        })
                        ->select(
                                    'g.id_group as id_group',
                                    'g.group_name as group_name',
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.group_type as group_type',
                                    'g.about_group as about_group',
                                    'g.file_name as file_name',
                                    'g.activated as activated',
                                    'g.deleted as deleted',
                                    'g.created_at as created_at',
                                    'g.updated_at as updated_at',
                                    'gc.id_group_category as id_group_category',
                                    'gc.group_category_name as group_category_name',
                                    'gc.file_name as group_category_image',
                                    'gm.id_group_member as id_group_member',
                                    'gm.member_type as member_type',
                                    'gm.activated as member_activated',
                                    'gm.deleted as member_deleted',
                                    'gm.created_at as member_created_at',
                                    'gm.updated_at as member_updated_at'
                                )
                        ->get();
    }

    public function group_event()
    {
        return $this->hasMany(GroupEvent::class , "id_group" ,"id_group");
    }
    
    public static function getGroupShareSearch($sSearchParam = '', $aUserGroups = array() ,$nLimit,$nDocId)
    {
        $aWhereParams[] = array('g.group_name', 'like', $sSearchParam.'%');
        $aWhereParams[] = array('g.activated', '=', 1);
        $aWhereParams[] = array('g.deleted', '=', 0);
        
        return Group::from( 'groups as g' )
                        ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                        ->leftJoin('group_documents as gd', function($join) use($nDocId){
                            $join->on('g.id_group', '=', 'gd.id_group')
                                 ->where('gd.id_document','=',$nDocId);   
                        })
                        ->where($aWhereParams)
                        ->whereNull('gd.id_group')                        
                        ->whereIn('g.id_group', $aUserGroups)
                        ->select(
                                    'g.id_group as id_group',
                                    'g.group_name as group_name' ,
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.file_name as file_name',
                                    'gc.file_name as group_category_image'
                                )
                        ->paginate($nLimit);
    }
    //attendace admin group for take attendace ..use also in quiz for group list
    public static function getAttendanceGroupSuggestion($sSearchStr,$nIdUser,$sSearchFor,$nGroupCat='')
    {
        $aWhereParams[] = array('g.group_name', 'like', '%'.$sSearchStr.'%');
        $aWhereParams[] = array('g.activated', '=', 1);
        $aWhereParams[] = array('g.deleted', '=', 0);
        if($nGroupCat != '')
            $aWhereParams[] = array('g.id_group_category', '=', $nGroupCat);
        else
            $aWhereParams[] = array('g.id_group_category', '!=', 10);
        
        $aWhere ='';
        if($sSearchFor != 'admin')
            $aWhere = 1;

        return Group::from( 'groups as g' )
                        ->join('group_members as gm', function($join) use($nIdUser){
                            $join->on('gm.id_group', '=', 'g.id_group')
                                 ->where('gm.id_user','=',$nIdUser);   
                        })
                        ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                        ->where(function ($query) use($aWhere){
                            $query->where('gm.member_type', '=', config('constants.GROUPADMIN'));
                            $query->orWhere('gm.member_type', '=', config('constants.GROUPCREATOR'));
                            if($aWhere !='')
                                $query->orWhere('gm.member_type', '=', config('constants.GROUPMEMBER'));
                        })
                        ->where($aWhereParams)
                        ->where('g.id_group_category', '!=', config('constants.UNIVERSITYGROUPCATEGORY'))
                        ->select(
                                    'g.id_group as id_group',
                                    'g.group_name as group_name',
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.file_name as file_name',
                                    'g.section as section',
                                    'g.semester as semester',
                                    'g.id_group_category as id_group_category',
                                    'gc.file_name as group_category_image'
                                )
                        ->orderBy('g.group_name','asc')
                        ->get();
    }
}