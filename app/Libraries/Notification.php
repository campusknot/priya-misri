<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notification extends Model
{
    protected $table = 'notifications';
    protected $primaryKey = 'id_notification';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_entites', 'entity_type_pattern', 'notification_type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUnreadNotifications($nIdUser, $nSkipRecords, $nLimit)
    {
        if(!$nIdUser)
            return FALSE;
        
        return Notification::from('notifications as n')
                            ->leftJoin('read_notifications as rn', 'n.id_notification', '=', 'rn.id_notification')
                            ->where('n.id_user', '=', $nIdUser)
                            ->where('rn.id_notification', NULL)
                            ->select(
                                        'n.id_notification as id_notification',
                                        'n.id_user as id_user',
                                        'n.id_entites as id_entites',
                                        'n.entity_type_pattern as entity_type_pattern',
                                        'n.notification_type as notification_type',
                                        'n.created_at as created_at',
                                        'n.updated_at as updated_at'
                                    )
                            ->skip($nSkipRecords)
                            ->take($nLimit)
                            ->get();
    }

    public static function getGroupRequest( $toUserId )
    {
        if(!$toUserId)
            return FALSE;

            return Notification::from('group_requests as gs')
                                    ->leftjoin('users as us', 'us.id_user', '=', 'gs.id_user_request_from')
                                    ->leftjoin('users as u', 'u.id_user', '=', 'gs.id_user_request_to')
                                    ->leftjoin('groups as g', 'g.id_group', '=', 'gs.id_group')
                                    -> where('id_user_request_to', '=', $toUserId )
                                    -> where('member_status', '=', 'P' )
                                    ->select('id_group_request', 'gs.id_group', 'g.group_name','id_user_request_from', 'id_user_request_to', 'member_status', 'us.first_name as FromName', 'us.last_name as LastName', 'gs.created_at')
                                    ->get();
    }

    public static function ChangeNotificationStatus( $Status, $NotificationId )
    {
        DB::table('group_requests')
            ->where('id_group_request', $NotificationId )  // find your user by their email
            ->update(array('member_status' => $Status));  // update the record in the DB. 
    }
    
    public static function ChangeEventStatus( $Status, $NotificationId )
    {
        DB::table('event_requests')
            ->where('id_event_request', $NotificationId )  // find your user by their email
            ->update(array('status' => $Status));  // update the record in the DB.
    }

    public static function getUserInformativeNotification($nUserId, $nSkipRecords, $nLimit)
    {
        return Notification::where('id_user', '=', $nUserId)
                            ->select(
                                        'id_notification',
                                        'id_user',
                                        'id_entites',
                                        'entity_type_pattern',
                                        'notification_type',
                                        'created_at',
                                        'updated_at'
                                    )    
                            ->orderBy('updated_at','DESC')
                            ->skip($nSkipRecords)
                            ->take($nLimit)
                            ->get();
    }

    public static function getEntitiesFromPattern($sEntityTypePattern, $sIdEntites)
    {
        $aIdEntities = explode('_', $sIdEntites);
        $aEntityPattern = explode('_', $sEntityTypePattern);
        
        $aEntityDetails['user'] = array();
        $aEntityDetails['group'] = array();
        $aEntityDetails['event'] = array();
        $aEntityDetails['post'] = array();
        $aEntityDetails['document'] = array();
        $aEntityDetails['user_following'] = array();
        
        $nCount = 0;
        foreach ($aIdEntities as $nIdEntity)
        {
            switch ($aEntityPattern[$nCount])
            {
                case 'U':
                    $oUser = DB::table('users')
                                    ->where('id_user', $nIdEntity)
                                    ->first();
                    $oUserProfile = DB::table('user_profile_images')
                                        ->where('id_user', $nIdEntity)
                                        ->where('activated',1)
                                        ->where('deleted',0)
                                        ->orderBy('updated_at','DESC')
                                        ->first();
                    $oUser->file_name = isset($oUserProfile->file_name) ? $oUserProfile->file_name : null;
                    $aEntityDetails['user'] = $oUser;
                    break;
                
                case 'G':
                    $oGroup = DB::table('groups')
                                    ->where('id_group', $nIdEntity)
                                    ->first();
                    $aEntityDetails['group'] = $oGroup;
                    break;
                
                case 'E':
                    $oEvent = DB::table('events')
                                    ->where('id_event', $nIdEntity)
                                    ->first();
                    $aEntityDetails['event'] = $oEvent;
                    break;
                case 'D':
                    $oDocument = DB::table('documents')
                                    ->where('id_document', $nIdEntity)
                                    ->first();
                    $aEntityDetails['document'] = $oDocument;
                    break;
                
                case 'P':
                    $oPost = DB::table('posts')
                                    ->where('id_post', $nIdEntity)
                                    ->first();
                    $aEntityDetails['post'] = $oPost;
                    break;
                
                case 'AL':
                    $oAttendanceLog = DB::table('attendance_log')
                                    ->where('id_attendance_log', $nIdEntity)
                                    ->first();
                    $aEntityDetails['attendance_log'] = $oAttendanceLog;
                    break;
                case 'GAL':
                    $oGroupAttendanceLog = DB::table('group_attendance_logs')
                                    ->where('id_group_attendance_log', $nIdEntity)
                                    ->first();
                    $aEntityDetails['group_attendance_log'] = $oGroupAttendanceLog;
                    break;
                
                case 'C':
                    $oCourse = DB::table('courses')
                                    ->where('id_course', $nIdEntity)
                                    ->first();
                    $aEntityDetails['course'] = $oCourse;
                    break;
                
                case 'UFL':
                    $oUser = DB::table('users')
                                    ->where('id_user', $nIdEntity)
                                    ->first();
                    $oUserProfile = DB::table('user_profile_images')
                                        ->where('id_user', $nIdEntity)
                                        ->orderBy('updated_at','DESC')
                                        ->first();
                    $oUser->file_name = isset($oUserProfile->file_name) ? $oUserProfile->file_name : null;
                    $aEntityDetails['user_following'] = $oUser;
                    break;
                case 'Q':
                    $oQuiz = DB::table('quizzes')
                                ->where('id_quiz', $nIdEntity)
                                ->first();
                    $aEntityDetails['quiz'] = $oQuiz;
                    break;
                default :
                    break;
            }
            $nCount ++;
        }
        
        return $aEntityDetails;
    }
    
    public static function getUnreadNotificationCount($nIdUser)
    {
        return Notification::from('notifications as n')
                                ->leftJoin('read_notifications as rn', 'n.id_notification', '=', 'rn.id_notification')
                                ->where('n.id_user', '=', $nIdUser)
                                ->where('rn.id_notification', NULL)
                                ->count();
    }
}
