<?php

namespace App\Libraries;

use Baum\Node;
use Illuminate\Support\Facades\DB;
use Auth;

class Document extends Node
{
    /**
     * For define table name
     */
    protected $table = 'documents';
    protected $primaryKey = 'id_document';
    protected $parentColumn = 'id_parent';

    // 'lft' column name
    protected $leftColumn = 'lft';

    // 'rgt' column name
    protected $rightColumn = 'rgt';

    // 'depth' column name
    protected $depthColumn = 'depth';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // guard attributes from mass-assignment
    protected $guarded = array('id_document', 'id_parent', 'lft', 'rgt', 'depth');
  
    protected $fillable = [ 
                            'id_user', 'document_name', 'file_name', 'lft', 'rgt', 'depth', 'shared_with',
                            'id_parent', 'document_type', 'copy_from', 'activated', 'deleted'
                        ];
    
    public static function getDocumentList($nIdUser,$sOrder='desc',$sOrderField='updated_at',$sSearchStr='')
    {
        $aWhereParams = [
                            'd.id_user' => $nIdUser,
                            'd.activated' => 1,
                            'd.deleted' => 0,                         
                        ];
        
        DB::statement(DB::raw('SET @previous_lft = 0'));
        DB::statement(DB::raw('SET @previous_rgt = 0'));
        
        $iQuery = Document::from( 'documents as d' )
                            ->leftJoin('users as u', 'd.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                        'd.id_document as id_document',
                                        'd.id_user as id_user',
                                        'd.document_type as document_type',
                                        'd.id_parent as id_parent',
                                        'd.document_name as document_name',
                                        'd.file_name as file_name',
                                        'd.depth as depth',
                                        'd.shared_with as shared_with',
                                        'd.lft as lft',
                                        'd.rgt as rgt',
                                        DB::raw('@previous_lft := d.lft'),
                                        DB::raw('@previous_rgt := d.rgt'),
                                        'd.created_at as created_at',
                                        'd.updated_at as updated_at',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'u.first_name as first_name',   
                                        'u.last_name as last_name'
                                    )
                            ->having('d.lft', 'NOT BETWEEN', DB::raw(DB::raw('@previous_lft') .' AND '. DB::raw(DB::raw('@previous_rgt'))))
                            ->orderBy('d.lft','asc');
        if($sSearchStr != '')
            $iQuery->where(DB::raw('d.document_name'), 'like', $sSearchStr.'%' );
        
        return Document::from(DB::raw("({$iQuery->toSql()}) as docs"))
                        ->select('*')
                        ->mergeBindings( $iQuery->getQuery() )
                        ->orderBy('docs.'.$sOrderField,$sOrder)
                        ->get();
    }
    
    //unwanted check the function is used or not.
    public static function getDocumentDetail($nIdUser,$nDocumentId= '')
    {
        $aWhereParams = [
                            'd.activated' => 1,
                            'd.deleted' => 0,                         
                        ];
        if($nDocumentId != '')
            $aWhereParams['d.id_document'] = $nDocumentId;
        
        return Document::from( 'documents as d' )
                       ->leftJoin('document_permissions as dp',function($join) use($nIdUser){
                           $join->on('d.id_document', '=', 'dp.id_document')
                                ->where('dp.id_user','=',$nIdUser);   
                            })
                       ->where($aWhereParams)
                       ->select(
                                    'd.id_document as id_document',                                    
                                    'd.document_type as document_type',
                                    'd.id_parent as id_parent',
                                    'dp.permission_type as permission_type'
                               )
                       ->first();
    }
    
    public static function getDocumentSharedList($nIdUser,$sOrderBy='desc',$sOrderField='updated_at',$sSearchStr='') 
    {
        $aWhereParams = [
                            'sd.shared_with' => $nIdUser,
                            'd.activated' => 1,
                            'd.deleted' => 0,                         
                            'sd.activated' => 1,
                            'sd.deleted' => 0,                         
                        ];
        
        DB::statement(DB::raw('SET @previous_lft = 0'));
        DB::statement(DB::raw('SET @previous_rgt = 0'));
        
        $iQuery = Document::from( 'shared_documents as sd' )
                            ->leftJoin( 'documents as d', 'sd.id_document', '=', 'd.id_document' )
                            ->leftJoin('users as u', 'd.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                        'd.id_document as id_document',
                                        'd.id_user as id_user',
                                        'd.document_type as document_type',
                                        'd.id_parent as id_parent',
                                        'd.document_name as document_name',
                                        'd.file_name as file_name',
                                        'd.depth as depth',
                                        'd.lft as lft',
                                        'd.rgt as rgt',
                                        DB::raw('@previous_lft := d.lft'),
                                        DB::raw('@previous_rgt := d.rgt'),
                                        'd.created_at as created_at',
                                        'd.updated_at as updated_at',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'u.first_name as first_name',   
                                        'u.last_name as last_name'
                                    )
                            ->having('d.lft', 'NOT BETWEEN', DB::raw(DB::raw('@previous_lft') .' AND '. DB::raw(DB::raw('@previous_rgt'))))
                            ->orderBy('sd.id_document','asc');
        
        if($sSearchStr != '')
            $iQuery->where(DB::raw('d.document_name'), 'like', $sSearchStr.'%' );
        
        return Document::from(DB::raw("({$iQuery->toSql()}) as docs"))
                        ->select('*')
                        ->mergeBindings( $iQuery->getQuery() )
                        ->orderBy('docs.'.$sOrderField,$sOrderBy)
                        ->get();
    }
    
    public static function getAncestorsWithPermission($oDocument,$nIdUser)
    {
        $oDocument = Document::from( 'documents as d' )
                                ->leftJoin('document_permissions as dp',function($join) use($nIdUser){
                                    $join->on('d.id_document', '=', 'dp.id_document')
                                         ->where('dp.id_user', '=', $nIdUser)
                                         ->where('dp.activated','=',1)
                                         ->where('dp.deleted','=',0);
                                })
                                ->where('d.lft', '<=', $oDocument->lft)
                                ->where('d.rgt', '>=', $oDocument->rgt)
                                ->orderBy('d.lft')
                                ->groupBy('d.id_document')
                                ->select(
                                             'd.id_document as id_document',
                                             'd.id_user as id_user',
                                             'd.document_name as document_name',
                                             'dp.permission_type as permission_type'
                                        )
                               ->get();
        return $oDocument;
    }
    
    public static function countAncestorsWithWritePermission($oDocument,$nIdUser)
    {
        $aWhereParams = [
                            'd.activated' => 1,
                            'd.deleted' => 0,                         
                        ];
        
        return Document::from( 'documents as d' )
                       ->leftJoin('document_permissions as dp',function($join) use($nIdUser){
                           $join->on('d.id_document', '=', 'dp.id_document')
                                ->where('dp.id_user', '=', $nIdUser);
                       })
                       ->where('d.lft', '<=', $oDocument->lft)
                       ->where('d.rgt', '>=', $oDocument->rgt)
                       ->where('dp.permission_type', '=', config('constants.DOCUMENTPERMISSIONTYPEWRITE')) 
                       ->where($aWhereParams)
                      ->count();
    }
    
    public static function isChildShareorNot($oDocument)
    {
        $nSharedCount = Document::from( 'documents as d' )
                                    ->join('document_permissions as dp',function($join){
                                        $join->on('d.id_document', '=', 'dp.id_document');
                                    })
                                    ->where('dp.id_user', '!=',Auth::user()->id_user)
                                    ->where(function( $query ) use($oDocument) {
                                        $query->where(function( $query ) use($oDocument) {
                                            $query->where('d.lft', '>=', $oDocument->lft)
                                                    ->where('d.rgt', '<=', $oDocument->rgt);
                                        });
                                        $query->orWhere(function( $query ) use($oDocument) {
                                            $query->where('d.lft', '<=', $oDocument->lft)
                                                    ->where('d.rgt', '>=', $oDocument->rgt);
                                        });
                                    })
                                    ->count();
        return ($nSharedCount > 0) ? 1 : 0;
    }
    
    public static function getChildDocument($nIdDocument,$sOrderBy='desc',$sOrderField='updated_at',$sSearchStr='')
    {
        $aWhereParams = [
                            'd.activated' => 1,
                            'd.deleted' => 0, 
                            'd.id_parent' => $nIdDocument
                        ];
        if($sOrderField == 'updated_at')
            $sOrderField == 'd.updated_at';
        
        $iQuery = Document::from( 'documents as d' )
                            ->leftJoin('users as u', 'd.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                    'd.id_document as id_document',
                                    'd.id_user as id_user',
                                    'd.document_type as document_type',
                                    'd.id_parent as id_parent',
                                    'd.document_name as document_name',
                                    'd.file_name as file_name',
                                    'd.created_at as created_at',
                                    'd.updated_at as updated_at',
                                    'd.shared_with as shared_with',
                                    'd.lft as lft',
                                    'd.rgt as rgt',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'u.first_name as first_name',   
                                    'u.last_name as last_name'
                                )
                            ->orderBy($sOrderField,$sOrderBy);
        if($sSearchStr != '')
            $iQuery->where(DB::raw('d.document_name'), 'like', $sSearchStr.'%' );
                            
        return $iQuery->get();
    }
    
    public static function getSingleDocument($nDocumentId)
    {
        $aWhereParams['d.id_document'] = $nDocumentId;
        
        return Document::from( 'documents as d' )
                            ->leftJoin('users as u', 'd.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                        'd.id_document as id_document',
                                        'd.id_user as id_user',
                                        'd.document_type as document_type',
                                        'd.id_parent as id_parent',
                                        'd.document_name as document_name',
                                        'd.file_name as file_name',
                                        'd.depth as depth',
                                        'd.shared_with as shared_with',
                                        'd.lft as lft',
                                        'd.rgt as rgt',
                                        'd.created_at as created_at',
                                        'd.updated_at as updated_at',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'u.first_name as first_name',   
                                        'u.last_name as last_name'
                                    )
                            ->first();
    }
    
    public static function getAllMyDocuments($nIdUser)
    {
        
        $aWhereParams = [
                            'd.id_user' => $nIdUser,
                            'd.activated' => 1,
                            'd.deleted' => 0,   
                            'd.document_type' => config('constants.DOCUMENTTYPEFOLDER')
                        ];
        
        DB::statement(DB::raw('SET @previous_lft = 0'));
        DB::statement(DB::raw('SET @previous_rgt = 0'));
        
        $iQuery = Document::from( 'documents as d' )
                            ->where($aWhereParams)
                            ->select(
                                        'd.id_document as id_document',
                                        'd.id_user as id_user',
                                        'd.document_type as document_type',
                                        'd.id_parent as id_parent',
                                        'd.document_name as document_name',
                                        'd.file_name as file_name',
                                        'd.depth as depth',
                                        'd.shared_with as shared_with',
                                        'd.lft as lft',
                                        'd.rgt as rgt',
                                        DB::raw('@previous_lft := d.lft'),
                                        DB::raw('@previous_rgt := d.rgt'),
                                        'd.created_at as created_at',
                                        'd.updated_at as updated_at'
                                    )
                            ->having('d.lft', 'NOT BETWEEN', DB::raw(DB::raw('@previous_lft') .' AND '. DB::raw(DB::raw('@previous_rgt'))))
                            ->orderBy('d.lft','asc');
        
        return Document::from(DB::raw("({$iQuery->toSql()}) as docs"))
                        ->select('*')
                        ->mergeBindings( $iQuery->getQuery() )
                        ->orderBy('docs.document_name','asc')
                        ->get();
    }
    
    public static function getAllChildDocument($oDocument) 
    {
        return  Document::from( 'documents as d' )
                        ->where(function( $query ) use($oDocument) {
                            $query->where('d.lft', '>=', $oDocument->lft)
                                    ->where('d.rgt', '<=', $oDocument->rgt);
                        })
                        ->where('d.document_type', '=', config('constants.DOCUMENTTYPEFOLDER'))
                        ->where('d.activated', '=', 1)
                        ->where('d.deleted', '=', 0)
                        ->select(
                                'd.id_document as id_document',
                                'd.id_user as id_user',
                                'd.document_type as document_type',
                                'd.id_parent as id_parent',
                                'd.document_name as document_name',
                                'd.file_name as file_name',
                                'd.depth as depth',
                                'd.shared_with as shared_with',
                                'd.lft as lft',
                                'd.rgt as rgt',
                                'd.created_at as created_at',
                                'd.updated_at as updated_at'
                            )
                        
                        ->orderBy('d.lft','asc')
                        ->orderBy('d.document_name','asc')
                        ->get();
    }
    
    public static function getGroupChildDocument($nIdDocument)
    {
        $aWhereParams = [
                            'd.activated' => 1,
                            'd.deleted' => 0, 
                            'd.id_parent' => $nIdDocument,
                            'd.document_type' => config('constants.DOCUMENTTYPEFOLDER')
                        ];
        
        $iQuery = Document::from( 'documents as d' )
                            ->where($aWhereParams)
                            ->select(
                                    'd.id_document as id_document',
                                    'd.document_type as document_type',
                                    'd.document_name as document_name' ,
                                    'd.lft as lft',
                                    'd.rgt as rgt'
                                )
                            ->orderBy('d.document_name','asc');
                            
        return $iQuery->get();
    }
    
    public static function checkDocumentPermission($oDocument,$nIdUser,$sPermissionType='')
    {
        $aWhereParams = [
                            'd.activated' => 1,
                            'd.deleted' => 0,                         
                        ];
        if(!empty($sPermissionType))
            $aWhereParams['dp.permission_type'] = $sPermissionType;
        return Document::from( 'document_permissions as dp' )
                       ->Join('documents as d',function($join) use($nIdUser){
                            $join->on('d.id_document', '=', 'dp.id_document')
                                 ->where('dp.id_user', '=', $nIdUser);
                       })
                       ->where('d.lft', '<=', $oDocument->lft)
                       ->where('d.rgt', '>=', $oDocument->rgt)
                       ->where($aWhereParams)
                      ->count();
    }
}