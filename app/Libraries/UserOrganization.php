<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserOrganization extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'committee_name', 'post',
        'start_year' , 'end_year'  
    ];

    /**
     *   for define table name
     */
    protected $table = 'user_co_curricular_activities';
    protected $primaryKey = 'id_user_co_curricular_activity';

     public function getRouteKeyName()
    {
        return 'id_user_co_curricular_activity';
    }

	/**
     * Get the user that owns the Organization.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserOrganization($nUserId, $nLimit = 0)
    {
        return UserOrganization::where('id_user', $nUserId)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->orderBy('start_year', 'desc')
                                ->when($nLimit > 0, function ($query) use ($nLimit) {
                                    return $query->take($nLimit);
                                })
                                ->get();
    }

}
