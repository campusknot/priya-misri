<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupDocument extends Model
{
    /**
     * For define table name
     */
    protected $table = 'group_documents';
    protected $primaryKey = 'id_group_document';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
                            'id_group', 'id_document', 'group_document_permission', 'activated', 'deleted'
                        ];
    
    public static function getDocumentGroupShared($nIdGroup,$sOrderBy='desc',$sOrderField='updated_at',$sSearchStr='')
    {
        if($sOrderField == 'updated_at')
            $sOrderField == 'd.updated_at';
        
        $aWhereParams = [
                            'gd.id_group' => $nIdGroup,
                            'd.activated' => 1,
                            'd.deleted' => 0
                        ];
        
        $iQuery = Document::from( 'group_documents as gd' )
                        ->leftJoin('documents as d', 'gd.id_document','=','d.id_document' )
                        ->leftJoin('users as u', 'd.id_user', '=', 'u.id_user')
                        ->where($aWhereParams)
                        ->where(function ($query) {
                            $query->where('d.depth', '=', 0)
                                  ->orWhere('d.document_type', '=', 'F');
                        })
                        ->where('gd.activated',1)
                        ->where('gd.deleted',0)
                        ->select(
                                    'd.id_document as id_document',
                                    'd.id_user as id_user',
                                    'd.document_type as document_type',
                                    'd.id_parent as id_parent',
                                    'd.document_name as document_name',
                                    'd.file_name as file_name',
                                    'd.depth as depth',
                                    'd.lft as lft',
                                    'd.rgt as rgt',
                                    'd.created_at as created_at',
                                    'd.updated_at as updated_at',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'u.first_name as first_name',   
                                    'u.last_name as last_name'
                                )
                        ->orderBy($sOrderField,$sOrderBy);
        if($sSearchStr != '')
            $iQuery->where(DB::raw('d.document_name'), 'like', $sSearchStr.'%' );
                            
        return $iQuery->get();
    }
    
    public static function getGroupDocument($nIdDocument,$nSkip=0,$nTake=3)
    {
        $aWhereParams = [
                            'gd.id_document' => $nIdDocument,
                            'gd.activated' => 1,
                            'gd.deleted' => 0,                         
                        ];
        return  GroupDocument::from( 'group_documents as gd' )
                            ->leftJoin( 'groups as g','gd.id_group','=','g.id_group' )
                            ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                            ->where($aWhereParams)
                            ->select(
                                        'g.id_group as id_group',
                                        'g.group_name as group_name',
                                        'g.section as section',
                                        'g.semester as semester',
                                        'g.file_name as group_image_name',
                                        'gc.file_name as group_category_image',
                                        'gd.group_document_permission as group_document_permission',
                                        'gd.id_group_document as id_group_document'
                                    )
                            ->skip($nSkip)
                            ->take($nTake)
                            ->get();
    }
    
    public static function getCountGroupDocument($nIdDocument)
    {
        $aWhereParams = [
                            'gd.id_document' => $nIdDocument,
                            'gd.activated' => 1,
                            'gd.deleted' => 0,                         
                        ];
        return  GroupDocument::from( 'group_documents as gd' )
                            ->leftJoin( 'groups as g','gd.id_group','=','g.id_group' )
                            ->leftJoin('group_categories as gc', 'g.id_group_category', '=', 'gc.id_group_category')
                            ->where($aWhereParams)
                            ->count();
    }
    
    public static function getAllGroupMemberPermission($nIdGroup,$nIdDocument,$nIdUser)
    {
        return  DocumentPermisssion::from('document_permissions as dp')
                                    ->leftJoin('users as u', 'dp.id_user', '=', 'u.id_user')
                                    ->where('dp.id_group','=', $nIdGroup)
                                    ->where('dp.id_document','=', $nIdDocument)
                                    ->where('dp.deleted','=', 0)
                                    ->where('dp.activated','=', 1)
                                    ->select(
                                            'dp.permission_type as permission_type',
                                            'dp.id_document_permission as id_document_permission',
                                            'u.id_user as id_user',
                                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                            'u.first_name as first_name',   
                                            'u.last_name as last_name'
                                            )
                                    ->get();
    }
    
    public static function getGroupDocuments($nIdGroup,$nIdUser)
    {
        $aWhereParams = [
                            'gd.id_group' => $nIdGroup,
                            'd.activated' => 1,
                            'd.deleted' => 0,
                            'd.document_type' => config('constants.DOCUMENTTYPEFOLDER')
                        ];
        
        $iQuery = Document::from( 'group_documents as gd' )
                        ->leftJoin('documents as d', 'gd.id_document','=','d.id_document' )
                        ->leftJoin('document_permissions as dp',function ($query) use($nIdUser){
                            $query->on('dp.id_document','=','gd.id_document' )
                                  ->where('dp.permission_type','=',config('constants.DOCUMENTPERMISSIONTYPEWRITE'))
                                  ->Where('dp.id_user','=',$nIdUser);
                        })
                        ->where($aWhereParams)
                        ->select(
                                    'd.id_document as id_document',
                                    'd.id_user as id_user',
                                    'd.document_type as document_type',
                                    'd.id_parent as id_parent',
                                    'd.document_name as document_name',
                                    'd.lft as lft',
                                    'd.rgt as rgt',
                                    'd.created_at as created_at',
                                    'd.updated_at as updated_at',
                                    'dp.permission_type as permission_type'
                                )
                        ->orderBy('d.document_name','asc')
                        ->groupBy('d.id_document');
        
        return $iQuery->get();
    }
}