<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'id_comment';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_entity', 'entity_type', 'comment_text', 'comment_type', 'display_file_name', 'file_name', 'file_type', 'activated', 'deleted'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    /**
     * 
     * @param type $nIdEntity
     * @param type $sEntityType
     * @return boolean
     */
    public static function getEntityComments($nIdEntity, $sEntityType)
    {
        if(!$nIdEntity || empty($sEntityType))
            return FALSE;
        
        $aWhereParams = [
                            'c.id_entity' => $nIdEntity,
                            'c.entity_type' => $sEntityType,
                            'c.activated' => 1,
                            'c.deleted' => 0
                        ];
        
        return Comment::from( 'comments as c' )
                            ->leftJoin('users as u', 'c.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                        'c.id_comment as id_comment',
                                        'c.id_user as id_user',
                                        'c.comment_text as comment_text',
                                        'c.comment_type as comment_type',
                                        'c.display_file_name as display_file_name',
                                        'c.file_name as file_name',
                                        'c.file_type as file_type',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = c.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'c.created_at as created_at',
                                        'c.updated_at as updated_at'
                                    )
                            ->orderBy('c.updated_at', 'asc')
                            ->get();
    }
    
    /**
     * 
     * @param type $nIdEntity
     * @param type $sEntityType
     * @return boolean
     */
    public static function getEntityCommentsWithPagination($nIdEntity, $sEntityType)
    {
        if(!$nIdEntity || empty($sEntityType))
            return FALSE;
        
        $aWhereParams = [
                            'c.id_entity' => $nIdEntity,
                            'c.entity_type' => $sEntityType,
                            'c.activated' => 1,
                            'c.deleted' => 0
                        ];
        
        return Comment::from( 'comments as c' )
                            ->leftJoin('users as u', 'c.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                        'c.id_comment as id_comment',
                                        'c.id_user as id_user',
                                        'c.comment_text as comment_text',
                                        'c.comment_type as comment_type',
                                        'c.display_file_name as display_file_name',
                                        'c.file_name as file_name',
                                        'c.file_type as file_type',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = c.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'c.created_at as created_at',
                                        'c.updated_at as updated_at'
                                    )
                            ->orderBy('c.updated_at', 'desc')
                            ->paginate(config('constants.PERPAGERECORDS'));
    }
    /*
     *Comment for ajax add Comment.
     */
    public static function getSingleComment($nIdComment) {
        
       $aWhereParams = [
                            'c.id_comment' => $nIdComment,
                            'c.entity_type' => 'P',
                            'c.activated' => 1,
                            'c.deleted' => 0
                        ];
        
        return Comment::from( 'comments as c' )
                            ->leftJoin('users as u', 'c.id_user', '=', 'u.id_user')
                            ->where($aWhereParams)
                            ->select(
                                        'c.id_comment as id_comment',
                                        'c.id_user as id_user',
                                        'c.comment_text as comment_text',
                                        'c.comment_type as comment_type',
                                        'c.display_file_name as display_file_name',
                                        'c.file_name as file_name',
                                        'c.file_type as file_type',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = c.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'c.created_at as created_at',
                                        'c.updated_at as updated_at'
                                    )
                            ->orderBy('c.updated_at', 'desc')
                            ->first();
    }
}
