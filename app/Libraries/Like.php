<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Like extends Model
{
    protected $table = 'likes';
    protected $primaryKey = 'id_like';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_entity', 'entity_type'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public static function getEntityLikes($nIdEntity, $sEntityType, $nIdUserLogedin='')
    {
        if(!$nIdEntity || empty($sEntityType))
            return FALSE;
        
        return  Like::from('likes as l')
                        ->leftJoin('users as u', 'l.id_user', '=', 'u.id_user')
                        ->where('l.id_entity', '=', $nIdEntity)
                        ->where('l.entity_type', '=', $sEntityType)
                        ->select(
                                    'l.id_like as id_like',
                                    'l.id_user as id_user',
                                    'u.first_name as first_name',
                                    'u.last_name as last_name',
                                    DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                    'l.created_at as created_at',
                                    'l.updated_at as updated_at'
                                )
                                //write this code at last bcz addSelect work only after select
                        ->when(!empty($nIdUserLogedin), function ($query) use($nIdUserLogedin) {
                            return $query->leftJoin('user_follows as uf',function($join) use($nIdUserLogedin){
                                            $join->on('l.id_user' ,'=', 'uf.id_following')
                                                 ->where('uf.id_follower','=',$nIdUserLogedin);
                                            })
                                        ->addSelect('uf.id_user_follow as id_user_follow');
                        })
                        ->get();
    }
}
