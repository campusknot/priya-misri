<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    protected $table = 'universities';
    protected $primaryKey = 'id_university';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['university_name', 'university_url'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];


    /* Admin Functions */
    public static function getUniversities(){
        return University::from('universities as u')
                            ->select(
                                'u.id_university as id_university',
                                'u.university_name as university_name',
                                'u.university_url as university_url'
                                )->get();
    }
}
