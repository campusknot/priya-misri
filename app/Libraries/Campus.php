<?php

namespace App\Libraries;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Campus extends Authenticatable {

    protected $table = 'campuses';
    protected $primaryKey = 'id_campus';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_university', 'campus_name', 'campus_url', 'country_name', 'timezone'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * This function will use to fetch campus list related to user provided email at registration time.
     * @param string $sCampusUrl
     * @return JSONArray $aCampuses
     */
    public function getCampusFromEmail($sCampusUrl) {
        $aCampuses = Campus::from('campuses as c')
        ->leftJoin('universities as uni', 'c.id_university', '=', 'uni.id_university')
        ->where('c.campus_url', 'like', '%' . $sCampusUrl . '%')
        ->get();
        return $aCampuses;
    }
    
    public function getCampusFromId($nIdCampus)
    {
        $aCampuses = Campus::from( 'campuses as c' )
        ->leftJoin('universities as uni', 'c.id_university', '=', 'uni.id_university')
        ->whereIn('c.id_campus', $nIdCampus)
        ->get();
        return $aCampuses;
    }
    
    public static function getCampusSuggetions($sCampusName)
    {
        $aCampuses = Campus::from( 'campuses as c' )
        ->where('c.campus_name', 'like', '%'.$sCampusName.'%')
        ->get();
        return $aCampuses;
    }

    public function users() {
        return $this->hasMany(User::class, "id_campus", "id_campus");
    }

    /* Admin Functions */

    public static function getCampusList($nSearchStr, $nOrderField, $nOrderBy) {
        return Campus::from('campuses as c')
        ->leftJoin('universities as u', 'c.id_university', '=', 'u.id_university')
        ->when($nSearchStr != "" || $nSearchStr != NULL, function ($query) use ($nSearchStr) {
            return $query->where(function ($query) use ($nSearchStr) {
                $query->where('c.campus_name', 'like', '%'.$nSearchStr.'%')
                ->orWhere('u.university_name', 'like', '%'.$nSearchStr.'%');
            });
        })
        ->select(
            'c.id_campus as id_campus', 
            'c.campus_name as campus_name', 
            'c.updated_at as updated_at',
            'c.campus_url as campus_url', 
            'u.id_university as id_university', 
            'u.university_name as university_name'
            )->orderBy($nOrderField, $nOrderBy)->paginate(config('constants.ADMIN_PERPAGERECORDS'));
    }

}
