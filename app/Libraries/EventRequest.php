<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class EventRequest extends Model
{
    /*
     * for define table name
     */
    protected $table = 'event_requests';
    protected $primaryKey = 'id_event_request';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_event_request', 'id_event', 'id_user_request_from', 'id_user_request_to', 'status'
    ];
    
    public function getRouteKeyName()
    {
        return 'id_event_request';
    }

    /**
     * Get the event request that owns event.
     */
    public function user()
    {
        return $this->hasOne(User::class , "id_user" ,"id_user")
                    ->select(
                                'first_name',
                                'last_name',
                                'id_campus'
                            );
    }


    /**
     * Get the user event that owns the event user.
     */
    public function event()
    {
        return $this->hasOne(Event::class , "id_event" ,"id_event")
                    ->select(
                                "id_event",
                                "event_title",
                                "event_description",
                                "start_date",
                                "end_date",
                                "room_number",
                                "address",
                                "latitude",
                                "longitude",
                                "activated",
                                "deleted"
                            )
                    ->where('activated', '=', 1)
                    ->where('deleted', '=', 0);
    }
    
    public static function getAllInvitedUserList($nIdEvent)
    {
        if(empty($nIdEvent))
            return FALSE;
        
        return EventRequest::from('event_requests as er')
                            ->leftJoin('users as u', 'er.id_user_request_to', '=', 'u.id_user')
                            ->where('er.id_event', '=', $nIdEvent)
                            ->where('u.activated', '=', 1)
                            ->where('u.deleted', '=', 0)
                            ->select(
                                        'er.id_event_request as id_event_request',
                                        'er.id_event as id_event',
                                        'u.id_user as id_user',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        'u.email as email'
                                    )
                            ->get();
    }
    
    public static function getRespondedUserList($nIdEvent, $aStatus, $nLimit = '')
    {
        if(empty($nIdEvent) || empty($aStatus))
            return FALSE;
        
        $nLimit = empty($nLimit) ? config('constants.PERPAGERECORDS') : $nLimit;
        
        return EventRequest::from('event_requests as er')
                            ->leftJoin('users as u', 'er.id_user_request_to', '=', 'u.id_user')
                            ->where('er.id_event', '=', $nIdEvent)
                            ->whereIn('er.status', $aStatus)
                            ->where('u.activated', '=', 1)
                            ->where('u.deleted', '=', 0)
                            ->select(
                                        'er.id_event_request as id_event_request',
                                        'er.id_event as id_event',
                                        'u.id_user as id_user',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                    )
                            ->paginate($nLimit);
    }
    
    public static function getEventRequest($nIdUser, $nSkipRecords, $nLimit)
    {
        if(!$nIdUser)
            return FALSE;
        
        $dCurrentdate = Carbon::now();
        $dStartDate = $dCurrentdate->toDateString();
            
        return EventRequest::from('event_requests as er')
                            ->leftjoin('users as u', 'er.id_user_request_from', '=', 'u.id_user')
                            ->leftjoin('events as e', 'er.id_event', '=', 'e.id_event')
                            ->where('e.activated', '=', 1)
                            ->where('e.deleted', '=', 0)
                            ->where('er.id_user_request_to', '=', $nIdUser)
                            ->where(function($query) use($dStartDate){
                                $query->where('e.end_date', '>=', $dStartDate." 00:00:00")
                                      ->orWhere('e.start_date', '>=', $dStartDate." 00:00:00");
                                })
                            ->whereNull('er.status')
                            ->select(
                                        'er.id_event_request as id_event_request',
                                        'er.id_event as id_event',
                                        'e.event_title as event_title',
                                        'er.status as status',
                                        'u.id_user as id_user',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'er.created_at as created_at',
                                        'er.updated_at as updated_at'
                                    )
                            ->orderBy('er.updated_at', 'desc')
                            ->skip($nSkipRecords)
                            ->take($nLimit)
                            ->get();
    }
    
    public static function getEventPendingRequestCount($nIdUser)
    {
        $dCurrentdate = Carbon::now();
        $dStartDate = $dCurrentdate->toDateString();
        
        return EventRequest::from('event_requests as er')
                                ->leftJoin('events as e', 'er.id_event', '=', 'e.id_event')
                                ->where('er.id_user_request_to', '=', $nIdUser)
                                ->whereNull('er.status')
                                ->where(function($query) use($dStartDate){
                                    $query->where('e.end_date', '>=', $dStartDate." 00:00:00")
                                            ->orWhere('e.start_date', '>=', $dStartDate." 00:00:00");
                                })
                                ->where('e.activated', '=', 1)
                                ->where('e.deleted', '=', 0)
                                ->count();
    }
}
