<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class QuizUserInvite extends Model
{
    /**
     * For define table name
     */
    protected $table = 'quiz_user_invite';
    protected $primaryKey = 'id_quiz_user_invite';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_quiz', 'id_user', 'start_time', 'end_time', 'completed', 'activated', 'deleted'
                        ];
    
    public static function getUpcomingUserQuizCount($nIdUser, $dDate,$nIdGroup='')
    {
        $aWhereConditions = [
                                'qui.id_user' => $nIdUser,
                                'qui.completed' => 0,
                                'qui.activated' => 1,
                                'qui.deleted' => 0,
                                'q.activated' => 1,
                                'q.deleted' => 0
                            ];
        if(!empty($nIdGroup))
            $aWhereConditions['qgi.id_group'] = $nIdGroup;
        
        return QuizUserInvite::from('quiz_user_invite as qui')
                                ->leftJoin('quizzes as q', 'qui.id_quiz', '=', 'q.id_quiz')
                                ->leftJoin('quiz_group_invite as qgi', 'qui.id_quiz', '=', 'qgi.id_quiz')
                                ->leftJoin('user_attempts as ua', function($query) use($nIdUser){
                                        return $query->on('qui.id_quiz', '=', 'ua.id_quiz')
                                                    ->where('ua.id_user','=',$nIdUser);
                                })
                                ->where($aWhereConditions)
                                ->where('qui.end_time', '>', $dDate)
                                ->where(function($query) use($dDate){
                                        return $query->where('ua.end_time','>',$dDate)
                                                     ->orWhereNull('ua.id_user_attempt');
                                        })
                                ->count();
    }
    public static function getUpcomingUserQuizList($nIdUser,$dDate,$nShowInMobile=0,$nIdGroup='')
    {
        $aWhereConditions = [
                                'qui.id_user' => $nIdUser,
                                'qui.completed' => 0,
                                'q.activated' => 1,
                                'q.deleted' => 0
                            ];
        if(!empty($nShowInMobile))
            $aWhereConditions['q.show_in_mobile'] = $nShowInMobile;
        if(!empty($nIdGroup))
            $aWhereConditions['g.id_group'] = $nIdGroup;
        return QuizUserInvite::from('quiz_user_invite as qui')
                                ->leftJoin('quizzes as q', 'qui.id_quiz', '=', 'q.id_quiz')
                                ->leftJoin('user_attempts as ua', function($query) use($nIdUser){
                                        return $query->on('qui.id_quiz', '=', 'ua.id_quiz')
                                                    ->where('ua.id_user','=',$nIdUser);
                                })
                                ->leftJoin('users as u', 'q.id_user', '=', 'u.id_user')
                                ->leftJoin('quiz_group_invite as qgi', 'q.id_quiz', '=', 'qgi.id_quiz')
                                ->leftJoin('groups as g', 'qgi.id_group', '=', 'g.id_group')
                                ->where($aWhereConditions)
                                ->where('qui.end_time', '>', $dDate)
                                ->where(function($query) use($dDate){
                                        return $query->where('ua.end_time','>',$dDate)
                                                     ->orWhereNull('ua.id_user_attempt');
                                        })
                                ->select(
                                            'qui.id_quiz_user_invite as id_quiz_user_invite',
                                            'q.id_quiz as id_quiz',
                                            'q.quiz_title as quiz_title',
                                            'q.duration as duration',
                                            'q.total_marks as total_marks',
                                            'g.group_name as group_name',
                                            'g.section as section',
                                            'g.semester as semester',
                                            'qui.start_time as start_time',
                                            'qui.end_time as end_time',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name'
                                        )
                                ->orderBy('qui.end_time', 'asc')
                                ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function getCompletedUserQuizList($nIdUser, $dDate,$nIdGroup)
    {
        $aWhereConditions = [
                                'qui.id_user' => $nIdUser,
                                'qui.activated' => 1,
                                'qui.deleted' => 0,
                                'q.activated' => 1,
                                'q.deleted' => 0
                            ];
        if(!empty($nIdGroup))
            $aWhereConditions['g.id_group'] = $nIdGroup;
        return QuizUserInvite::from('quiz_user_invite as qui')
                                ->leftJoin('user_attempts as ua', function($query) use($nIdUser){
                                        return $query->on('qui.id_quiz', '=', 'ua.id_quiz')
                                                    ->where('ua.id_user','=',$nIdUser);
                                })
                                ->leftJoin('quizzes as q', 'qui.id_quiz', '=', 'q.id_quiz')
                                ->leftJoin('users as u', 'q.id_user', '=', 'u.id_user')
                                ->leftJoin('quiz_group_invite as qgi', 'q.id_quiz', '=', 'qgi.id_quiz')
                                ->leftJoin('groups as g', 'qgi.id_group', '=', 'g.id_group')
                                ->where($aWhereConditions)
                                ->where(function($query) use($dDate){
                                    $query->where('qui.end_time', '<=', $dDate)
                                          ->orWhere('ua.end_time', '<=', $dDate);
                                })
                                ->select(
                                            'qui.id_quiz_user_invite as id_quiz_user_invite',
                                            'ua.id_user_attempt as id_user_attempt',
                                            'q.id_quiz as id_quiz',
                                            'q.quiz_title as quiz_title',
                                            'q.duration as duration',
                                            'q.total_marks as total_marks',
                                            'g.group_name as group_name',
                                            'g.section as section',
                                            'g.semester as semester',
                                            'qui.completed as completed',
                                            'qui.start_time as start_time',
                                            'qui.end_time as end_time',
                                            'qui.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name'
                                        )
                                ->orderBy('ua.end_time', 'desc')
                                ->orderBy('qui.end_time', 'desc')
                                ->paginate(config('constants.PERPAGERECORDS'));
    }
    
    public static function checkQuizBelogsToUser($nIdQuizUserInvite, $nIdUser, $dDate)
    {
        $aWhereConditions = [
                                'id_quiz_user_invite' => $nIdQuizUserInvite,
                                'id_user' => $nIdUser,
                                'completed' => 0,
                                'activated' => 1,
                                'deleted' => 0
                            ];
        return QuizUserInvite::where($aWhereConditions)
                                ->where('end_time', '>', $dDate)
                                ->first();
    }
    
    public static function getUserInvitedList($nIdQuiz)
    {
        $aWhereConditions = [
                                'qui.id_quiz' => $nIdQuiz,
                                'qui.activated' => 1,
                                'qui.deleted' => 0
                            ];
        
        return QuizUserInvite::from('quiz_user_invite as qui')
                                ->leftJoin('users as u', 'qui.id_user', '=', 'u.id_user')
                                ->where($aWhereConditions)
                                ->select(
                                            'qui.id_quiz_user_invite as id_quiz_user_invite',
                                            'qui.id_quiz as id_quiz',
                                            'qui.start_time as start_time',
                                            'qui.end_time as end_time',
                                            'qui.completed as completed',
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            'u.email as email',
                                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image')
                                        )
                                ->orderBy('u.first_name', 'asc')
                                ->paginate(config('constants.PERPAGERECORDS'));
    }
}