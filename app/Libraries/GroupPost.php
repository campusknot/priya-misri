<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class GroupPost extends Model
{
    protected $table = 'group_posts';
    protected $primaryKey = 'id_group_post';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_group', 'id_post'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public static function getGroupPost($nIdGroup, $sPostType='',$sOrderBy='',$sOrderField='',$sSearchStr='')
    {
        if(!$nIdGroup)
            return FALSE;
        
        $aWhereParams = [
                            'gp.id_group' => $nIdGroup,
                            'p.activated' => 1,
                            'p.deleted' => 0
                        ];
        if(!empty($sPostType))
            $aWhereParams['p.post_type'] = $sPostType;
        
        if(empty($sOrderBy))
            $sOrderBy = 'desc';
        
        if(empty($sOrderField) || $sOrderField == 'updated_at')
            $sOrderField = 'p.updated_at';
        
        return GroupPost::from( 'group_posts as gp' )
                            ->leftJoin('posts as p', 'gp.id_post', '=', 'p.id_post')
                            ->leftJoin('users as u', 'p.id_user', '=', 'u.id_user')
                            ->leftJoin('post_media as pm', 'p.id_post', '=', 'pm.id_post')
                            ->leftJoin('likes as l', function($join)
                                        {
                                            $join->on('l.id_user', '=', DB::raw("'".Auth::user()->id_user."'"));
                                            $join->on('p.id_post', '=', 'l.id_entity');
                                            $join->on('l.entity_type','=',DB::raw("'P'"));
                                        })
                            ->where('p.post_type', '!=', config('constants.POSTTYPEPOLL'))
                            ->where($aWhereParams)
                            ->when(!empty($sSearchStr), function ($query) use ($sSearchStr) {
                                    return $query->where('pm.display_file_name', 'like', $sSearchStr.'%');
                                })
                            ->select(
                                        'gp.id_group_post as id_group_post',
                                        'p.id_post as id_post',
                                        'p.id_user as id_user',
                                        'p.post_text as post_text',
                                        'p.post_type as post_type',
                                        'pm.display_file_name as display_file_name',
                                        'pm.file_name as file_name',
                                        'pm.media_type as media_type',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'l.id_like as id_like',
                                        DB::raw('(SELECT COUNT(id_like) FROM likes WHERE id_entity = p.id_post AND entity_type = "P") as likes'),
                                        DB::raw('(SELECT COUNT(id_comment) FROM comments WHERE id_entity = p.id_post AND entity_type = "P" AND activated = 1 AND deleted = 0) as comment_count'),
                                        'p.created_at as created_at',
                                        'p.updated_at as updated_at'
                                    )
                            ->orderBy($sOrderField,$sOrderBy)
                            ->groupBy('p.id_post')
                            ->paginate(config('constants.PERPAGERECORDS'));
    }
}
