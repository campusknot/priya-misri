<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class GroupRequest extends Model
{
    protected $table = 'group_requests';
    protected $primaryKey = 'id_group_request';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_group', 'id_user_request_from', 'id_user_request_to', 'request_type', 'member_status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public function getGroupPendingRequests($nIdGroup)
    {
        return $this->where('id_group', '=', $nIdGroup)
                    ->where('member_status', '=', 'P')
                    ->get();
    }
    
    public static function getLoginUserGroupRequests($nIdUser, $nSkipRecords, $nLimit)
    {
        return GroupRequest::from('group_requests as gr')
                            ->leftjoin('users as u', 'gr.id_user_request_from', '=', 'u.id_user')
                            ->leftjoin('groups as g', 'gr.id_group', '=', 'g.id_group')
                            ->where('g.activated', '=', 1)
                            ->where('g.deleted', '=', 0)
                            ->where('gr.id_user_request_to', '=', $nIdUser)
                            ->where('gr.member_status', '=', config('constants.GROUPMEMBERPENDING'))
                            ->select(
                                        'gr.id_group_request as id_group_request',
                                        'gr.request_type as request_type',
                                        'g.id_group as id_group',
                                        'g.group_name as group_name',
                                        'u.id_user as id_user',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'gr.created_at as created_at',
                                        'gr.updated_at as updated_at'
                                    )
                            ->orderBy('gr.updated_at', 'desc')
                            ->skip($nSkipRecords)
                            ->take($nLimit)
                            ->get();
    }
    
    public static function getGroupRequests($nIdGroup, $nSkipRecords, $nLimit)
    {
        return GroupRequest::from('group_requests as gr')
                            ->leftjoin('users as u', 'gr.id_user_request_to', '=', 'u.id_user')
                            ->leftjoin('groups as g', 'gr.id_group', '=', 'g.id_group')
                            ->where('gr.id_group', '=', $nIdGroup)
                            ->where('gr.request_type','=',config('constants.GROUPREQUESTTYPEINVITE'))
                            ->select(
                                        'gr.id_group_request as id_group_request',
                                        'gr.member_status as member_status',
                                        'g.id_group as id_group',
                                        'g.group_name as group_name',
                                        'u.id_user as id_user',
                                        'u.first_name as first_name',
                                        'u.last_name as last_name',
                                        DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                        'gr.created_at as created_at',
                                        'gr.updated_at as updated_at'
                                    )
                            ->orderBy('gr.updated_at', 'desc')
                            ->skip($nSkipRecords)
                            ->take($nLimit)
                            ->get();
    }
    
    /* In pvt group all requested member for join :priya*/
    public static function getJoinGroupRequests($nIdGroup)
    {
        return GroupRequest::from('group_requests as gr')
                                ->leftjoin('users as u', 'gr.id_user_request_from', '=', 'u.id_user')
                                ->where('gr.id_user_request_to','=',Auth()->user()->id_user)
                                ->where('gr.id_group','=',$nIdGroup)
                                ->where('gr.request_type','=',config('constants.GROUPREQUESTTYPEJOIN'))
                                ->where('gr.member_status','=',config('constants.GROUPMEMBERPENDING'))
                                ->orderBy('gr.updated_at', 'desc')
                                ->select(
                                            'gr.id_group_request as id_group_request',
                                            'gr.member_status as member_status',
                                            'gr.id_group as id_group',
                                            'u.id_user as id_user',
                                            'u.first_name as first_name',
                                            'u.last_name as last_name',
                                            DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                            'gr.created_at as created_at',
                                            'gr.updated_at as updated_at'
                                        )
                                ->get();
    }
    
    public static function getLoginUserRequest($nIdGroup)
    {
        $aGroupInviteRequestConditions = ['id_user_request_to'=>Auth()->user()->id_user,'request_type'=>config('constants.GROUPREQUESTTYPEINVITE')];
        
        $aGroupJoinRequestConditions = ['id_user_request_from'=>Auth()->user()->id_user, 'request_type'=>config('constants.GROUPREQUESTTYPEJOIN')];
        
        return GroupRequest::where('id_group','=',$nIdGroup)
                                ->where('member_status','=','P')
                                ->where(function($query) use ($aGroupInviteRequestConditions,$aGroupJoinRequestConditions) {
                                        $query->where($aGroupInviteRequestConditions)
                                                ->orWhere($aGroupJoinRequestConditions);
                                })
                                ->select(
                                            'id_group_request as id_group_request',
                                            'request_type as request_type'
                                        )
                                ->first();        
        
    }
    
    public static function getGroupPendingRequestCount($nIdUser)
    {
        return GroupRequest::from('group_requests as gr')
                                ->leftJoin('groups as g', 'gr.id_group', '=', 'g.id_group')
                                ->where('gr.id_user_request_to', '=', $nIdUser)
                                ->where('gr.member_status', '=', 'P')
                                ->where('g.activated', '=', 1)
                                ->where('g.deleted', '=', 0)
                                ->count();
    }
}
