<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;


class UserEducation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'university', 'degree',
        'major' , 'classification' ,
        'start_year' , 'end_year' 
    ];

    /*
	*   for define table name
    */
    protected $table = 'user_education';
    protected $primaryKey = 'id_user_education';


    public function getRouteKeyName()
    {
        return 'id_user_education';
    }

	/**
     * Get the user that owns the education.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserEducation($nUserId, $nLimit = 0)
    {
        return UserEducation::where('id_user', $nUserId)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->orderBy('start_year', 'desc')
                                ->when($nLimit > 0, function ($query) use ($nLimit) {
                                    return $query->take($nLimit);
                                })
                                ->get();
    }
    
    public static function getMajorEducation($nIdUser) 
    {
        $oUserEducation = UserEducation::where('id_user', $nIdUser)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->where(function( $query ) {
                                        $query->whereNull('end_year')
                                              ->orWhere('default_education', 1);
                                        })
                                ->select('id_user_education',
                                         'university',
                                         'degree',
                                         'major',
                                         'classification'
                                        )
                                ->first();
        if(count($oUserEducation)<=0){
            $oUserEducation= UserEducation::where('id_user', $nIdUser)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->orderBy('end_year','desc')
                                ->select('id_user_education',
                                         'university',
                                         'degree',
                                         'major',
                                         'classification'
                                        )
                                ->first();
            
        }
        return $oUserEducation;
        
    }
}
