<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';
    protected $primaryKey = 'id_course';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'id_course', 'id_university', 'course_name', 'course_code', 'id_user', 'activated', 'deleted'
        ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
    
    public static function getCourseListing()
    {
        return Course::from('courses as c')
                        ->leftJoin('universities as u', 'u.id_university', '=', 'c.id_university')
                        ->where('c.activated','=',1)
                        ->where('c.deleted','=',0)
                        ->select(
                                    'c.id_course as id_course',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code',
                                    'u.id_university as id_university',
                                    'u.university_name as university_name'
                                )
                        ->orderBy('c.updated_at', 'DESC')
                        ->paginate(50);
    }
    
    public static function getCourseListingForSync($nIdUniversity = '', $sCourseUpdatedAt = '')
    {
        if(empty($nIdUniversity) || empty($sCourseUpdatedAt))
            return FALSE;
        
        return Course::from('courses as c')
                        ->leftJoin('universities as u', 'u.id_university', '=', 'c.id_university')
                        ->where('c.updated_at', '>', $sCourseUpdatedAt)
                        ->where('u.id_university', '=', $nIdUniversity)
                        ->select(
                                    'c.id_course as id_course',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code',
                                    'u.id_university as id_university',
                                    'u.university_name as university_name',
                                    'c.activated as activated',
                                    'c.deleted as deleted',
                                    'c.created_at as created_at',
                                    'c.updated_at as updated_at'
                                )
                        ->paginate(50);
    }
    
    public static function getCourseSuggetions($sCourseTerm, $nIdCampus)
    {
        return Course::from('courses as c')
                        ->leftJoin('universities as u', 'c.id_university', '=', 'u.id_university')
                        ->leftJoin('campuses as cu', 'u.id_university', '=', 'cu.id_university')
                        ->where(function( $query ) use($sCourseTerm) {
                                $query->where('c.course_name','LIKE', '%'.$sCourseTerm.'%')
                                    ->orWhere('c.course_code','LIKE', '%'.$sCourseTerm.'%');
                        })
                        ->where('cu.id_campus','=', $nIdCampus)
                        ->where('c.activated','=',1)
                        ->where('c.deleted','=',0)
                        ->select(
                                    'c.id_course as id_course',
                                    'c.course_name as course_name',
                                    'c.course_code as course_code',
                                    'u.university_name as university_name'
                                )
                        ->groupBy('c.id_course')
                        ->get();
    }
}
