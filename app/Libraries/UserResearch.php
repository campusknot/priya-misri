<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserResearch extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'research_title', 
        'research_url','research_author',
        'research_description','research_year'  
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_research';
    protected $primaryKey = 'id_user_research';

     public function getRouteKeyName()
    {
        return 'id_user_research';
    }


	/**
     * Get the user that owns the Organization.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserResearch($nUserId, $nLimit = 0)
    {
        return UserResearch::where('id_user', $nUserId)
                                ->where('activated', 1)
                                ->where('deleted', 0)
                                ->orderBy('research_year', 'desc')
                                ->when($nLimit > 0, function ($query) use ($nLimit) {
                                    return $query->take($nLimit);
                                })
                                ->get();
    }


}
