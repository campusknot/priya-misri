<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class AllowedEmail extends Model
{
    /**
     * For define table name
     */
    protected $table = 'allowed_emails';
    protected $primaryKey = 'id_allowed_email';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_campus', 'email' ];
}
