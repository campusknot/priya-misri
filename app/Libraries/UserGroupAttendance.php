<?php
namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class UserGroupAttendance extends Model
{
    protected $table = 'user_group_attendance';
    protected $primaryKey = 'id_user_group_attendance';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_group_lecture', 'attendance_type', 'activated', 'deleted'
    ];
    
    public static function getAllUserAttendanceForGroup($aFilterParams,$nExcelFlag='') 
    {
        $oUserGroupAttendance = UserGroupAttendance::from( 'user_group_attendance as uga' )
                                                        ->join('group_lectures as gl', 'uga.id_group_lecture', '=', 'gl.id_group_lecture')
                                                        ->leftjoin('users as u', 'uga.id_user', '=', 'u.id_user')
                                                        ->leftjoin('group_attendance_logs as gal', 'uga.id_user_group_attendance', '=', 'gal.id_user_group_attendance')
                                                        ->where('gl.id_group',$aFilterParams['id_group'])
                                                        ->where('gl.semester',$aFilterParams['semester'])
                                                        ->where('gl.activated',1)
                                                        ->where('gl.deleted',0)
                                                        ->where('uga.activated', 1)                                                      
                                                        ->where('uga.deleted',0)                                                      
                                                        ->select(
                                                                'u.first_name as first_name',
                                                                'u.last_name as last_name',
                                                                'u.id_user as id_user',
                                                                'u.email as email',
                                                                'gl.semester as semester',
                                                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                                                DB::raw("GROUP_CONCAT( CASE WHEN uga.attendance_type = 'P' THEN uga.id_group_lecture ELSE NULL END SEPARATOR ',') as `id_lecture_present`"),
                                                                DB::raw("GROUP_CONCAT( CASE WHEN uga.attendance_type = 'SL' THEN uga.id_group_lecture ELSE NULL END SEPARATOR ',') as `id_lecture_sick`"),
                                                                DB::raw("GROUP_CONCAT( CASE WHEN gal.id_user_group_attendance != '' THEN gal.id_group_attendance_log ELSE NULL END SEPARATOR ',') as `id_group_attendance_log`")
                                                                )
                                                        ->groupBy('uga.id_user')
                                                        ->orderBy('u.first_name');
        
        if(isset($aFilterParams['search_str']) && $aFilterParams['search_str'] != ''){
            $sSearchStr= $aFilterParams['search_str'];
            $oUserGroupAttendance->where( function( $query ) use( $sSearchStr) {
                                        $query->where('u.first_name', 'like', $sSearchStr.'%')
                                              ->orWhere('u.last_name', 'like', $sSearchStr.'%');
                                        });
        }
        if($nExcelFlag == 1)
            return $oUserGroupAttendance->get();
        else
            return $oUserGroupAttendance->paginate(config('constants.PERPAGERECORDS'));
    }
    public static function getUserAttendanceForCourse($aFilterParams,$nExcelFlag='') 
    {
        $oUserGroupAttendance = UserGroupAttendance::from( 'group_members as gm' )
                                                        ->leftJoin('user_group_attendance as uga', 'uga.id_user', '=', 'gm.id_user')
                                                        ->leftJoin('group_lectures as gl', 'gm.id_group', '=', 'gl.id_group')
                                                        ->leftjoin('users as u', 'uga.id_user', '=', 'u.id_user')
                                                        ->leftjoin('group_attendance_logs as gal', 'uga.id_user_group_attendance', '=', 'gal.id_user_group_attendance')
                                                        ->where('gl.id_group',$aFilterParams['id_group'])
                                                        ->where('gl.semester',$aFilterParams['semester'])
                                                        ->where(function( $query ){
                                                            $query->where(function( $query1 ){
                                                                $query1->where('gm.activated',1)
                                                                        ->where('gm.deleted',0)
                                                                        ->orWhere('uga.id_user_group_attendance',NULL);
                                                            });
                                                            $query->orWhere(function( $query1 ){
                                                                $query1->where('gm.activated',0)
                                                                        ->where('gm.deleted',1)
                                                                        ->whereNotNull('uga.id_user_group_attendance');
                                                            });
                                                        })
                                                        ->where('gm.member_type', config('constants.GROUPMEMBER'))
                                                        ->where('uga.activated', 1)                                                      
                                                        ->where('uga.deleted',0)                                                      
                                                        ->select(
                                                                'u.first_name as first_name',
                                                                'u.last_name as last_name',
                                                                'u.id_user as id_user',
                                                                'u.email as email',
                                                                'gl.semester as semester',
                                                                DB::raw('(SELECT file_name FROM user_profile_images WHERE id_user = u.id_user AND activated = 1 AND deleted = 0 ORDER BY updated_at desc limit 1) as user_profile_image'),
                                                                DB::raw("GROUP_CONCAT( CASE WHEN uga.attendance_type = 'P' THEN uga.id_group_lecture ELSE NULL END SEPARATOR ',') as `id_lecture_present`"),
                                                                DB::raw("GROUP_CONCAT( CASE WHEN uga.attendance_type = 'SL' THEN uga.id_group_lecture ELSE NULL END SEPARATOR ',') as `id_lecture_sick`"),
                                                                DB::raw("GROUP_CONCAT( CASE WHEN gal.id_user_group_attendance != '' THEN gal.id_group_attendance_log ELSE NULL END SEPARATOR ',') as `id_group_attendance_log`")
                                                                )
                                                        ->groupBy('uga.id_user')
                                                        ->orderBy('u.first_name');
        
        if(isset($aFilterParams['search_str']) && $aFilterParams['search_str'] != ''){
            $sSearchStr= $aFilterParams['search_str'];
            $oUserGroupAttendance->where( function( $query ) use( $sSearchStr) {
                                        $query->where('u.first_name', 'like', $sSearchStr.'%')
                                              ->orWhere('u.last_name', 'like', $sSearchStr.'%');
                                        });
        }
        if($nExcelFlag == 1)
            return $oUserGroupAttendance->get();
        else
            return $oUserGroupAttendance->paginate(config('constants.PERPAGERECORDS'));
    }
}