<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserAwards extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'award_title', 'award_issuer','award_description',
        'awarded_year'  
    ];

    /*
	*   for define table name
    */
	protected $table = 'user_awards';
    protected $primaryKey = 'id_user_award';

     public function getRouteKeyName()
    {
        return 'id_user_award';
    }

	/**
     * Get the user that owns the Organization.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public static function getUserAwards($nUserId, $nLimit = 0)
    {
        return  UserAwards::where('id_user', $nUserId)
                            ->where('activated', 1)
                            ->where('deleted', 0)
                            ->orderBy('awarded_year', 'desc')
                            ->when($nLimit > 0, function ($query) use ($nLimit) {
                                return $query->take($nLimit);
                            })
                            ->get();
    }



}
