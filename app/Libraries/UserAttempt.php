<?php

namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;

class UserAttempt extends Model
{
    /**
     * For define table name
     */
    protected $table = 'user_attempts';
    protected $primaryKey = 'id_user_attempt';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_quiz', 'id_user', 'start_time', 'end_time', 'activated', 'deleted'
                        ];
}