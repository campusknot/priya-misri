<?php

namespace App\Libraries;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class QuizGroupInvite extends Model
{
    /**
     * For define table name
     */
    protected $table = 'quiz_group_invite';
    protected $primaryKey = 'id_quiz_group_invite';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id_quiz', 'id_group', 'start_time', 'end_time', 'activated', 'deleted'
                        ];
    
    public static function getGroupQuizIds($nIdGroup) 
    {
        $aCondition['qgi.activated'] = 1;
        $aCondition['qgi.deleted'] = 0;
        $dTime = Carbon::Now();
        return QuizGroupinvite::from('quiz_group_invite as qgi')
                      ->where($aCondition)
                      ->where('qgi.id_group',$nIdGroup)
                      ->where('qgi.end_time','>',$dTime)
                      ->select(DB::raw('GROUP_CONCAT(qgi.id_quiz) as id_quiz'))
                      ->get();  
    }
}