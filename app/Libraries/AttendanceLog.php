<?php
namespace App\Libraries;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Auth;

class AttendanceLog extends Model
{
    protected $table = 'attendance_log';
    protected $primaryKey = 'id_attendance_log';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_user_attendance', 'change_from', 'change_to'
    ];
    
    public static function getFacultyAttendaceLog($aIdAttendanceLog)
    {
        return AttendanceLog::from('attendance_log as al')
                            ->leftJoin('user_attendance as ua', 'al.id_user_attendance', '=', 'ua.id_user_attendance')
                            ->leftJoin('lectures as l', 'ua.id_lecture', '=', 'l.id_lecture')
                            ->leftJoin('courses as c', 'l.id_course', '=', 'c.id_course')
                            ->whereIn('id_attendance_log',$aIdAttendanceLog)
                            ->select(
                                     'l.created_at as lecture_date',
                                     'al.created_at as created_at',
                                     'al.change_to as change_to',
                                     'al.change_from as change_from',
                                     'c.course_name as course_name',
                                     'l.section as section',
                                     'l.semester as semester'
                                    )
                            ->orderBy('ua.created_at','asc')
                            ->orderBy('al.id_attendance_log','desc')
                            ->get();
    }
    
    public static function getStudentAttendaceLog($aIdLecture) 
    {
        return AttendanceLog::from('attendance_log as al')
                            ->leftJoin('user_attendance as ua', 'al.id_user_attendance', '=', 'ua.id_user_attendance')
                            ->leftJoin('lectures as l', 'ua.id_lecture', '=', 'l.id_lecture')
                            ->leftJoin('courses as c', 'l.id_course', '=', 'c.id_course')
                            ->whereIn('ua.id_lecture',$aIdLecture)
                            ->where('ua.id_user', Auth::user()->id_user)
                            ->select(
                                     'l.created_at as lecture_date',
                                     'al.created_at as created_at',
                                     'al.change_to as change_to',
                                     'al.change_from as change_from',
                                     'c.course_name as course_name',
                                     'l.section as section',
                                     'l.semester as semester'
                                    )
                            ->orderBy('ua.created_at','asc')
                            ->orderBy('al.id_attendance_log','desc')
                            ->get();
    }
}