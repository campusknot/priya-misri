<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        'App\Libraries\UserEducation' => 'App\Policies\UserEducationPolicy',
        'App\Libraries\UserWorkExperience' => 'App\Policies\UserWorkExperiencePolicy',
        'App\Libraries\UserOrganization' => 'App\Policies\UserOrganizationPolicy',
        'App\Libraries\UserAwards' => 'App\Policies\UserAwardPolicy',
        'App\Libraries\UserJournals' => 'App\Policies\UserJournalPolicy',
        'App\Libraries\UserResearch' => 'App\Policies\UserResearchPolicy',
        'App\Libraries\UserPublications' => 'App\Policies\UserPublicationPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        //
    }
}
