<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $adminNamespace = 'App\Http\Admin\Controllers';
    protected $vueNamespace = 'App\Http\Vue\Controllers';
    protected $webNamespace = 'App\Http\Web\Controllers';
    protected $apiNamespace = 'App\Http\Api\Controllers';
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == preg_replace('#^http(s)?://#', '', env('API_URL'))) {
            $router->group(['namespace' => $this->apiNamespace], function ($router) {
                require app_path('Http/Api/routes.php');
            });
        }
        else if(isset ($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == preg_replace('#^http(s)?://#', '', env('APP_URL'))) {
            $router->group(['namespace' => $this->webNamespace], function ($router) {
                require app_path('Http/Web/routes.php');
            });
        }
        else if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == preg_replace('#^http(s)?://#', '', env('ADMIN_URL'))) {
            $router->group(['namespace' => $this->adminNamespace], function ($router) {
                require app_path('Http/Admin/routes.php');
            });
        }
        else if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == preg_replace('#^http(s)?://#', '', env('VUE_URL'))) {
            $router->group(['namespace' => $this->vueNamespace], function ($router) {
                require app_path('Http/Vue/routes.php');
            });
        }
        else {
            $router->group(['namespace' => $this->namespace], function ($router) {
                require app_path('Http/routes.php');
            });
        }
    }
}
