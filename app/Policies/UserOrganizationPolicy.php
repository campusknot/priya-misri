<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserOrganization;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserOrganizationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

     /**
     * Determine if the given user can delete the given organization.
     *
     * @param  User  $user
     * @param  UserOrganization  $organization
     * @return bool
     */
    public function destroy(User $user, UserOrganization $organization)
    {
        return $user->id_user === $organization->id_user;
    }
}
