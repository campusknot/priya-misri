<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserWorkExperience;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserWorkExperiencePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can delete the given workexperience.
     *
     * @param  User  $user
     * @param  UserWorkExperience  $workexperience
     * @return bool
     */
    public function destroy(User $user, UserWorkExperience $workexperience)
    {
        return $user->id_user === $workexperience->id_user;
    }
}
