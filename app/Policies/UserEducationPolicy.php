<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserEducation;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserEducationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can delete the given Education.
     *
     * @param  User  $user
     * @param  UserEducation  $Education
     * @return bool
     */
    public function destroy(User $user, UserEducation $education)
    {
        return $user->id_user === $education->id_user;
    }
    
}
