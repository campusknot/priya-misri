<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserPublications;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserPublicationPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

     /**
     * Determine if the given user can delete the given publication.
     *
     * @param  User  $user
     * @param  UserPublications  $publication
     * @return bool
     */
    public function destroy(User $user, UserPublications $publication)
    {
        return $user->id_user === $publication->id_user;
    }

}
