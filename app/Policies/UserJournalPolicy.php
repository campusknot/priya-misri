<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserJournals;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserJournalPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can delete the given journal.
     *
     * @param  User  $user
     * @param  UserJournals  $journal
     * @return bool
     */
    public function destroy(User $user, UserJournals $journal)
    {
        return $user->id_user === $journal->id_user;
    }

}
