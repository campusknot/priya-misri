<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserAwards;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserAwardPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the given user can delete the given UserAwards.
     *
     * @param  User  $user
     * @param  UserAwards  $UserAwards
     * @return bool
     */
    public function destroy(User $user, UserAwards $award)
    {
        return $user->id_user === $award->id_user;
    }
}
