<?php

namespace App\Policies;

use App\Libraries\User;
use App\Libraries\UserResearch;

use Illuminate\Auth\Access\HandlesAuthorization;

class UserResearchPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    
    /**
     * Determine if the given user can delete the given research.
     *
     * @param  User  $user
     * @param  UserResearch  $research
     * @return bool
     */
    public function destroy(User $user, UserResearch $research)
    {
        return $user->id_user === $research->id_user;
    }

}
