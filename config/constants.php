<?php

return [
    'DEVELOPERSTRING' => '|][=\/',
    'APIDEVELOPERSTRING' => '|][=|-|',
    'MEDIAURL' => env('MEDIAURL', 'https://s3.amazonaws.com/campusknot-local'), //http://campusknot-media.s3-accelerate.amazonaws.com
    'GROUPMEDIAFOLDER' => env('GROUPMEDIAFOLDER', 'group-media'), //group-media
    'POSTMEDIAFOLDER' => env('POSTMEDIAFOLDER', 'post-media'),    //post-media
    'USERMEDIAFOLDER' => env('USERMEDIAFOLDER', 'user-media'),    //user-media
    'QUESTIONMEDIAFOLDER' => env('QUESTIONMEDIAFOLDER', 'question-media'),//question-media
    'GMAPURL' => 'http://maps.google.com/',
    'FORGOTPASSWORD' => 'user_forgot_password',
    'USERTYPESTUDENT' => 'S',
    'USERTYPEFACULTY' => 'F',
    'USERTYPECAMPUSADMIN' => 'CA',
    'PUBLICGROUP' => 'PUB',
    'PRIVATEGROUP' => 'PVT',
    'SECRETGROUP' => 'SEC',
    'GROUPCREATOR' => 'C',
    'GROUPADMIN' => 'A',
    'GROUPMEMBER' => 'M',
    'GROUPREQUESTTYPEINVITE' => 'I',
    'GROUPREQUESTTYPEJOIN' => 'J',
    'GROUPMEMBERPENDING' => 'P',
    'GROUPMEMBERACCEPTED' => 'A',
    'GROUPMEMBERREJECTED' => 'R',
    'UNIVERSITYGROUPCATEGORY' => 8,
    'COURSEGROUPCATEGORY' => 10,
    'EVENTSTATUSGOING' => 'G',
    'EVENTSTATUSMAYBE' => 'M',
    'EVENTSTATUSNOTGOING' => 'NG',
    'EVENTTYPEGOOGLE' => 'g',
    'EVENTTYPECK' => 'ck',
    'POSTTYPETEXT' => 'T',
    'POSTTYPEIMAGE' => 'I',
    'POSTTYPEVIDEO' => 'V',
    'POSTTYPEDOCUMENT' => 'D',
    'POSTTYPEPOLL' => 'P',
    'ENTITYTYPEPOST' => 'P',
    'POSTTYPECODE' => 'C',
    'PERPAGERECORDS' => 20,
    'ADMIN_PERPAGERECORDS' => 10,
    'COLORCOUNT' => 25,
    'GOOGLEDEVELOPERKEY' => env('GOOGLE_API_DEV_KEY', ''),
    'client_secret_path' => (env('APNS_ENV', 'development') == 'production') ? resource_path('batch/client_secret_live.json') : resource_path('batch/client_secret_local.json'),
    'ga_script_type' => (env('APNS_ENV', 'development') == 'production') ? 'ga_script_production' : 'ga_script_local',
    'GOOGLECALENDARAPPNAME' => 'Google Calendar Application',
    
    //Poll types
    'POLLTYPEMULTIPLE' => 'M',
    'POLLTYPEOPEN' => 'O',
    
    'MAXALLOWEDREPORTS' => 10,
    
    //Notification types
    'NOTIFICATIONUSERFOLLOW' => 'user_follow',
    'NOTIFICATIONCOMMENTONEVENT' => 'comment_on_event',
    'NOTIFICATIONEDITEVENT' => 'edit_event',
    'NOTIFICATIONCANCELEVENT' => 'cancel_event',
    'NOTIFICATIONNEWGROUPPOST' => 'new_group_post',
    'NOTIFICATIONCOMMENTONPOST' => 'comment_on_post',
    'NOTIFICATIONLIKEONPOST' => 'like_on_post',
    'NOTIFICATIONNEWADMIN' => 'new_group_admin',
    'NOTIFICATIONGROUPDOCUMENT' => 'new_group_document',
    'NOTIFICATIONUSERDOCUMENT' => 'new_user_document',
    'NOTIFICATIONUSERATTENDANCECHANGE' => 'user_attendance_change',
    'NOTIFICATIONGROUPATTENDANCECHANGE' => 'group_attendance_change',
    'NOTIFICATIONGROUPQUIZ' =>'group_quiz',
    'NOTIFICATIONGROUPQUIZDELETE' => 'group_quiz_delete',
    
    'DOCUMENTTYPEFOLDER' => 'FO',
    'DOCUMENTTYPEFILE' => 'F',
    'DOCUMENTPERMISSIONTYPEWRITE' => 'W',
    'DOCUMENTPERMISSIONTYPEREAD' => 'R',
    'DOCUMENTSHAREDWITHGROUP' => 'G',
    
    //attendance
    'ATTENDANCETYPECOURSE' => 'C',
    'ATTENDANCETYPEGROUP' => 'G',
    'ATTENDANCETYPEPRESENT' => 'P',
    'ATTENDANCETYPESICKLEAVE' => 'SL',
    'ATTENDANCETYPEABSENT' => 'A',
    
    //quiz
    'QUESTIONTYPEOPEN' => 'O',
    'QUESTIONTYPEMULTIPLE' => 'M',
    
    'ALLOWMASTERPASS' => 0,
    'MASTERPASS' => 'Come#!S@@n$K',
    
    //Api response statuses
    'APIFAIL' => -1,
    'APIERROR' => 0,
    'APISUCCESS' => 1,
    'THUMBNAILSIZEARRAY'=>array('50X50', '100X100'),
    
    'DEFAULTPASSWORD' => '123456',
    'DATEDISPALYFORMAT' => 'd M Y',
    'TIMEDISPALYFORMAT' => 'h:i A',
    'DATEMODIFIEDFORMAT' => 'd M Y \a\t h:i A',
    
    //Custom campus URLs
    'CUSTOMCAMPUSURL' => array(
                                "k12.ms.us" => array(7)
                            ),
];