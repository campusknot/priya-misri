<?php

return array(

    'IOS' => array(
        'environment' => env('APNS_ENV', 'development'),
        'certificate' => (env('APNS_ENV', 'development') == 'production') ? resource_path('batch/aps_distribution.pem') : resource_path('batch/aps_development.pem'),
        'passPhrase'  => 'ck@123#@!',
        'service'     => 'apns'
    ),
    'AOS' => array(
        'environment' =>'production',
        'apiKey'      =>'AIzaSyCgMi24wnZRG3kbBBKGEDCP-WMeM_Ulj-Q',
        'service'     =>'gcm'
    )
);